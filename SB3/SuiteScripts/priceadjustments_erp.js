/**
 *@NApiVersion 2.x
 *@NScriptType MapReduceScript
 */

//File: ERP_priceadjustment_MR.js

define(['N/search','N/log','N/record'],

function (search, log, record){

	function getInputData()
    {
		log.debug('Getting Input Data...');

        var priceAdjustmentSearch = search.load({
            id: 'customsearch_priceadjustmentSearch' //Item price adjustment list Search
        });

		return priceAdjustmentSearch;
    }

    function map(context)
    {
      var searchResult = JSON.parse(context.value);
    	var priceId = searchResult.id;
      var priceName = searchResult.values.tranid;

    	log.debug('Mapping) priceID: ' + priceId + ', PriceAdjName: ' + priceName);

    	context.write(priceId, priceName); //Key-Value pair
    }

    function reduce(context)
    {


    }

    function summarize(summary)
    {
    	log.debug('Summarizing...');
        var type = summary.toString();
        log.debug(type + ' Usage Consumed', summary.usage);
        log.debug(type + ' Number of Queues', summary.concurrency);
        log.debug(type + ' Number of Yields', summary.yields);
        log.debug('...Done!');
    }

    return {
        getInputData: getInputData,
        summarize: summarize
    };
});
