/**
 * Company            Explore Consulting
 * Copyright          2015 Explore Consulting, LLC
 * Type               NetSuite EC_Get_Ship_Country_List
 * Version            1.0.0.0
 **/


// Function to get an item's field values based on the internal id of each field
function getShipCountryList(request, response)
{
    nlapiLogExecution('debug', 'getItemFields', 'start');

    try
    {
        var params = request.getAllParameters();

        var shipping_countries = [];
        var results = nlapiSearchRecord('customlist_shipping_country', null, null, [new nlobjSearchColumn('name'), new nlobjSearchColumn('internalid').setSort(false)]);
        if(results && results.length > 0){
            for(var i = 0; i < results.length; i++){

                shipping_countries.push({id:results[i].getValue('internalid') ,name:results[i].getValue('name')});
            }
        }

        nlapiLogExecution('AUDIT', 'shipping_countries', JSON.stringify(shipping_countries));
        
        response.write(JSON.stringify(shipping_countries));
    }
    catch(e)
    {
        nlapiLogExecution('DEBUG', 'getItemFields', 'Unexpected Error occurred while processing:  ' + e);
        
    }
}