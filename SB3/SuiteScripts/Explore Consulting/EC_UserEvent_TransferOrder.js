/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_UserEvent_TransferOrder
 * Version            1.0.0.0
 **/

// Custom List called Set Transfer Price Status
var TO_STATUS = {
    Pending: 1,
    Completed: 2,
    IssuesFound: 3
};

if ( nlapiGetContext().getExecutionContext() == "PRODUCTION" )
    var SendErrorEmailsFrom = 125247;
else
    var SendErrorEmailsFrom = -5;


function onBeforeLoad(type, form, request) {

}

function onBeforeSubmit(type) {

}

function onAfterSubmit(type) {

	
	var execution = nlapiGetContext().getExecutionContext();
	var user = nlapiGetContext().getUser();
	Log.a("afterSubmit", " Execution:  " + execution  + " User: "  + user );
	
    if ( type != 'delete' && nlapiGetRecordId() != 273617 && execution != 'scheduled') EC.PopulateIntraCompanyTransferPricing_OnSave();


}


/**
 * Function to validate From Location field on the Transfer Order form.  This field must always be populated before any
 * lines are added to the order.  If populated, then a Suitelet is called to get the Transfer Pricing for the line.
 * @constructor
 */
EC.PopulateIntraCompanyTransferPricing_OnSave = function(){

    try
    {

        var transferOrder = nsdal.loadObject(nlapiGetRecordType(), nlapiGetRecordId(),         	
        		["id", "location", "custbody_set_transfer_price_status", "orderstatus", "subsidiary", "tranid",
                 "transferlocation", "trandate", "custbody_erp_main_transferorder_number", "custbody_erp_backorder" ])
                 
            .withSublist("item", ["item","description", "itemtype", "rate", "custcol_erp_backorder", "custcol_erp_orig_qty", "quantitycommitted", "quantity", "custcol_erp_to_memo", "custcol_erp_override_rate"]);  // ERP EDIT - IS20150824


        ////////////////////////////////////////////////////////////////////////////////////////// RETRIEVE ITEMS//////////////////////////////////////////////////////////////////////////////////////////
		var arrTOItemId = new Array();
    	_.each(transferOrder.item, function(line, index){
    		  var itemType = getItemType(line.itemtype);
    		  if ( itemType == "inventoryitem") {    	
    			  if(arrTOItemId.indexOf(line.item) < 0 ){
    				  arrTOItemId.push(line.item);
    			  }
    		  }
    	});    	 

	    
		var arSavedSearchResultsItm = null;

    	var arSaveSearchFiltersItm = new Array();
    	var arSavedSearchColumnsItm = new Array();

    	var resultsArray = new Array(); // important array
    	var idArray = new Array();  // important array


    	var strSavedSearchIDItm = null;

    	arSaveSearchFiltersItm.push(new nlobjSearchFilter('inventorylocation', null , 'is', transferOrder.location ));
    	arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null , 'anyof', arrTOItemId ));

    	arSavedSearchColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
    	arSavedSearchColumnsItm.push(new nlobjSearchColumn('name'));
    	arSavedSearchColumnsItm.push(new nlobjSearchColumn('inventorylocation'));
    	arSavedSearchColumnsItm.push(new nlobjSearchColumn('locationaveragecost'));


    	arSavedSearchResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchColumnsItm);

    	if (!isNullOrEmpty(arSavedSearchResultsItm)){
    		 
    			for(var i = 0 ;  i < arSavedSearchResultsItm.length; i++ ){

    				var obj = new Object();
    				obj.id = arSavedSearchResultsItm[i].getValue('internalid');
    				obj.name = arSavedSearchResultsItm[i].getValue('name');
    				obj.location = arSavedSearchResultsItm[i].getValue('inventorylocation');
    				obj.locationname = arSavedSearchResultsItm[i].getText('inventorylocation');
    				obj.locavecost = arSavedSearchResultsItm[i].getValue('locationaveragecost');

    				resultsArray.push(obj);
    				idArray.push(obj.id);

    			}
    	}
       
		////////////////////////////////////////////////////////////////////////////////////////// FROM LOCATION PRICING//////////////////////////////////////////////////////////////////////////////////////////

        var arrZeroAvgItemObj = new Array();
        var arrZeroAvgItemIds = new Array();
        var backorderTOlines = new Array();
        var removeLines  = new Array();

        var errorMsg = false;          // Error Message variable that will help us

        _.each(transferOrder.item, function(line, index){

            var lineNumber = index+1;            
            var itemType = getItemType(line.itemtype);         

            if ( itemType == "inventoryitem" ) {

                var avgCost =  0.00;               

            	var intArrayPosition = idArray.indexOf(line.item); var itemObject = resultsArray[intArrayPosition];

            	if(!isNullOrEmpty(itemObject)){
            		avgCost = itemObject.locavecost;
            	}else{}
            	          
            	
            	//	var ordstatus2 =   transferOrder.orderstatus;
            	//	if(ordstatus2 == "B" ){
            	//		
	            //		if(line.quantitycommitted > 0){
				//							// line.commitinventory;
                //	
				//							// Log.a("Committed", " Line: orig:" +
				//							// line.custcol_erp_orig_qty + "
				//							// committed:" + line.quantitycommitted
				//							// + " bo: " +
				//							// (line.custcol_erp_orig_qty -
				//							// line.quantitycommitted) );
                //	
				//							// Log.a("Committed", " Line: " +
				//							// lineNumber + " committed: " );
	            //			
	            //		        	if(line.custcol_erp_orig_qty == null){
	            //	        			line.custcol_erp_orig_qty = line.quantity;
	            //	        		}
	            //	        		           
	            //	        		line.quantity = line.quantitycommitted;            		            		            			                    		
	            //	        		line.custcol_erp_backorder = (line.custcol_erp_orig_qty - line.quantitycommitted);
	            //		}
	            //		else{
	            //			
	            //			var lineObject = new Object;
	            //			lineObject.item = line.item;
	            //			lineObject.quantity = line.quantity;
	            //			lineObject.quantitycommitted = line.quantitycommitted;
	            //			lineObject.cost = line.rate;
	            //			lineObject.memo = line.custcol_erp_to_memo;
	            //				            	            	
	            //			backorderTOlines.push(lineObject);	     			            		            		
	            //		}
            	//	
            	//	}
            	            	            	
            	if ( avgCost && avgCost != "NOTFOUND" )
                {

if(line.custcol_erp_override_rate == false){
                    line.rate = avgCost;
                    line.custcol_erp_to_memo = "loc: " + itemObject.locationname + " line: " + lineNumber  + " avg: "+ avgCost;                                     
}else{
                       line.custcol_erp_to_memo = "loc: " + itemObject.locationname + " line: " + lineNumber  + " avg: "+ avgCost + " : override";            
}

                }
                else
                {
                    var findItemObj = new Object();
                    findItemObj.linenum = lineNumber;
                    findItemObj.itemid = line.item;

                    arrZeroAvgItemIds.push(line.item);
                    arrZeroAvgItemObj.push(findItemObj);
                                                        
                    
                    if(line.custcol_erp_override_rate == false){  // ERP ADD- IS20150824
	                    //errorMsg = true; //ERP Fix Eric's issue 225?
	             
                    }  
                }                               
                
            }
            else{}               
        });
        
        
		////////////////////////////////////////////////////////////////////////////////////////// MAIN LOCATION PRICING//////////////////////////////////////////////////////////////////////////////////////////
        if(!isNullOrEmpty(arrZeroAvgItemIds)){
        	            
        	var idSubsidiary = transferOrder.subsidiary; 
           				
    	    var searchFiltersMainLocation = [];
    	    searchFiltersMainLocation[0] =  new nlobjSearchFilter('subsidiary', null, 'is', idSubsidiary);
    		searchFiltersMainLocation[1] =  new nlobjSearchFilter('custrecord_ka_main_warehouse', null, 'is', 'T');    	    
    	    var searchColumnsMainLocation = [new nlobjSearchColumn('internalid')];    	    
    	    var resultsMainLocation = nlapiSearchRecord('location', null, searchFiltersMainLocation, searchColumnsMainLocation);
    	    
    	    if (!isNullOrEmpty(resultsMainLocation)){
				
    	    	var idLocation = resultsMainLocation[0].getValue('internalid');    	    	    	    	    		    		
            	var arSavedSearchItemMainResultsItm = null;
                var arSaveSearchFiltersItm = new Array();
                var arSavedSearchItemMainColumnsItm = new Array();
                var zeroIdCostArray = new Array(); // important array
                var zeroObjCostArray = new Array();  // important array
                var strSavedSearchIDItm = null;

                arSaveSearchFiltersItm.push(new nlobjSearchFilter('inventorylocation', null , 'is', idLocation ));
                arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null , 'anyof', arrZeroAvgItemIds ));
                arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
                arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('name'));
                arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('inventorylocation'));
                arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('locationaveragecost'));
                arSavedSearchItemMainResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchItemMainColumnsItm);
                                                

                if (!isNullOrEmpty(arSavedSearchItemMainResultsItm)){

                   

                    for(var i = 0 ;  i < arSavedSearchItemMainResultsItm.length; i++ ){

                      var obj = new Object();
                      obj.id = arSavedSearchItemMainResultsItm[i].getValue('internalid');
                      obj.name = arSavedSearchItemMainResultsItm[i].getValue('name');
                      obj.location = arSavedSearchItemMainResultsItm[i].getValue('inventorylocation');
                      obj.locationname = arSavedSearchItemMainResultsItm[i].getText('inventorylocation');
                      obj.locavecost = arSavedSearchItemMainResultsItm[i].getValue('locationaveragecost');

                      zeroObjCostArray.push(obj);
                      zeroIdCostArray.push(obj.id);

                    }
                }

             
              for(var m = 0; m < arrZeroAvgItemIds.length; m++ ){

                var lineid = arrZeroAvgItemObj[m].linenum; 
                var itemid = arrZeroAvgItemObj[m].itemid;
                
                var avgCost =  0.00;
                var intArrayPosition = zeroIdCostArray.indexOf(itemid); var zeroCostItemObject = zeroObjCostArray[intArrayPosition];
                
               
            	if(!isNullOrEmpty(zeroCostItemObject)){
            		
            		if(!isNullOrEmpty(zeroCostItemObject.locavecost)){
            			avgCost = zeroCostItemObject.locavecost;
            		}
            		
            		
            	}
            	            	
            	
				var memoText = '';
            	              var bOverride = transferOrder.item[lineid-1].custcol_erp_override_rate;
					
				if(bOverride == false){
						transferOrder.item[lineid-1].rate = avgCost;
				}																
				
            	transferOrder.item[lineid-1].custcol_erp_to_memo = " loc: " + zeroCostItemObject.locationname + " line: " + lineid + " avg: "+ avgCost;
				memoText = transferOrder.item[lineid-1].custcol_erp_to_memo ;
            	
				
				Log.a("Transfer Order", lineid + " bOverride " + bOverride + " avgCost " + avgCost);
            	
            	if(bOverride == false && avgCost == 0.0){
            		 errorMsg = true; 
            	}
				
				if(bOverride == true){
						memoText =  memoText + " :overriden" 
 errorMsg = false;  //Fix for Nick's issue 0225
				}
				
				transferOrder.item[lineid-1].custcol_erp_to_memo  = memoText; 
              }   			
    		}
        }



        // If an error was encountered for any line, set the custom status field to reflect this
        // Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Show Error?  " + errorMsg);
        if (errorMsg) {
            transferOrder.custbody_set_transfer_price_status = TO_STATUS.IssuesFound;
            transferOrder.orderstatus = "A";        // A = Pending Approval
        } else {
            transferOrder.custbody_set_transfer_price_status = TO_STATUS.Completed;
            transferOrder.orderstatus = "B";        // B = Pending Fulfillment
        }
        
        transferOrder.custbody_erp_main_transferorder_number = transferOrder.id;
        

         Log.a("Transfer Order", "ID: " + transferOrder.id  + ' errorStatus: ' + errorMsg + ' transferOrder.orderstatus: ' +  transferOrder.orderstatus );
		
        var ordstatus =   transferOrder.orderstatus;
        
        if(ordstatus == "B"){
        
	        if(isNullOrEmpty(transferOrder.custbody_erp_backorder)){
	        	        	
	        	if(!isNullOrEmpty(backorderTOlines)){
	        		var backorderid = createBackorderTransferOrder(transferOrder, backorderTOlines);
	        		transferOrder.custbody_erp_backorder =  backorderid;
	        	}
	        	  
	        }else{        	
	        	
	        	if(!isNullOrEmpty(backorderTOlines)){
	        		updateBackorderTransferOrder(transferOrder, backorderTOlines);
	        	}
	        }
        
        }
        
                
       

        var id = transferOrder.save(true, true);       
    }
    catch(e)
    {
        // Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Unexpected Error Occurred:  " + e);
        nlapiSendEmail(SendErrorEmailsFrom, "rhackey@exploreconsulting.com", "Kit & Ace:  Error in EC_UserEvent_TransferOrder script", "Error Details:  " + e)
    }
};


function isNullOrEmpty(valueStr){
    return(valueStr == null || valueStr == "" || valueStr == undefined);
}

function updateBackorderTransferOrder(transferOrderObject, backOrderArray){

	Log.a('updateBackorderTransferOrder TO OBJ ', JSON.stringify(transferOrderObject));
	
	
	
	try{		
		if(!isNullOrEmpty(backOrderArray))	{
			
			var transBack = nlapiLoadRecord('transferorder', transferOrderObject.custbody_erp_backorder); 
			
			for(var i=0; i < backOrderArray.length; i++){
				
				var backOrder = backOrderArray[i];	
					
				transBack.selectNewLineItem("item");
				transBack.setCurrentLineItemValue("item", "item", backOrder.item);
				transBack.setCurrentLineItemValue("item", "quantity", backOrder.quantity);
				transBack.setCurrentLineItemValue("item", "rate", backOrder.cost); 
				transBack.setCurrentLineItemValue("item", "custcol_erp_to_memo", backOrder.memo);
				transBack.setCurrentLineItemValue("item", "custcol_erp_backorder", backOrder.quantity);
				transBack.setCurrentLineItemValue("item", "custcol_erp_orig_qty", backOrder.quantity);
				transBack.setCurrentLineItemValue("item", "isclosed", 'T');
				
								
				transBack.commitLineItem("item");			        					
			}						
			
			var id = nlapiSubmitRecord(transBack); 	
			
			
		}
	}
	catch(ex){
		Log.e('createBackorderTransferOrder TO OBJ ', ex);
	}
}


function createBackorderTransferOrder(transferOrderObject, backOrderArray){
	
	Log.a('createBackorderTransferOrder TO OBJ ', JSON.stringify(transferOrderObject));
	
	try{
		
		if(!isNullOrEmpty(backOrderArray))	{	
			
			Log.a('createBackorderTransferOrder backOrderArary ',JSON.stringify(backOrderArray));Log.a('createBackorderTransferOrder backOrderArary ',JSON.stringify(backOrderArray));
			
			var transBack = nlapiCreateRecord('transferorder');
					
			var thisid = transBack.getId();
                       
			transBack.setFieldValue('location', transferOrderObject.location);
			transBack.setFieldValue('orderstatus', 'A');
			transBack.setFieldValue('subsidiary', transferOrderObject.subsidiary);
			transBack.setFieldValue('transferlocation', transferOrderObject.transferlocation);
		        transBack.setFieldValue('custbody_erp_is_backorder', 'T');	
			transBack.setFieldValue('custbody_erp_main_transferorder_number', transferOrderObject.id);
			

			
			var newDate = nlapiDateToString(new Date(transferOrderObject.trandate));
			
			transBack.setFieldValue('trandate', newDate);	
			
			for(var i=0; i < backOrderArray.length; i++){
				
				var backOrder = backOrderArray[i];	
					
				transBack.selectNewLineItem("item");
				transBack.setCurrentLineItemValue("item", "item", backOrder.item);
				transBack.setCurrentLineItemValue("item", "quantity", backOrder.quantity);
				transBack.setCurrentLineItemValue("item", "rate", backOrder.cost); 
				transBack.setCurrentLineItemValue("item", "custcol_erp_to_memo", backOrder.memo);
				transBack.setCurrentLineItemValue("item", "custcol_erp_backorder", backOrder.quantity);
				transBack.setCurrentLineItemValue("item", "custcol_erp_orig_qty", backOrder.quantity);
				transBack.setCurrentLineItemValue("item", "isclosed", 'T');
				
				
				
				transBack.commitLineItem("item");			        					
			}						
			
			var id = nlapiSubmitRecord(transBack); 	
			
			return id; 
		    Log.a("createBackorderTransferOrder", "Transfer Order Backorder:  " + id);
		    
		}else{
			
			return null; 
		}
		
		
		
	}catch(ex){
		
		
		Log.e('createBackorderTransferOrder TO OBJ ', ex);
		return null; 
	}
	
	
	
}