/**
 * Company            Explore Consulting
 * Copyright          2014 Explore Consulting, LLC
 * Type               NetSuite EC_UserEvent_PurchaseOrder.js
 * Version            1.0.0.0
 **/

//var PO_SEARCH_ID_SB = 'customsearch204'
var PO_SEARCH_ID_SB = 'customsearch_product_po_qty_totals'
    ,	PO_SEARCH_ID_PROD = 'customsearch_product_po_qty_totals';

function onAfterSubmit(type) {

    /* 2016-03-16 RMH:  Released to Production */
    if ( type == 'create' || type == 'edit' ) {

        EC.ValidateIntercompanyPOTransferPrices();
    }

/*    var poSearchId
        ,	currPoId
        ,	filters = []
        ,	poSearchResults
        ,	currResult
        ,	color
        ,	quantity
        ,	totalUnitCount = 0
        ,	colorUnitText = '';

    if (nlapiGetContext().getEnvironment() == "SANDBOX") {

        poSearchId = PO_SEARCH_ID_SB;

    } else {

        poSearchId = PO_SEARCH_ID_PROD;
    }

    currPoId = nlapiGetRecordId();

    /!*******************************
     * Search for color item groups
     ******************************!/
    filters.push(new nlobjSearchFilter('internalid', null, 'is', currPoId));

    try {

        poSearchResults = nlapiSearchRecord(null, poSearchId, filters, null);
    } catch(e) {

        nlapiLogExecution('ERROR', 'Unable to retrieve purchase orders', e);
        return;
    }

    if (poSearchResults && poSearchResults.length) {

        /!*******************************
         * Add new color group line items
         ******************************!/
        // Iterate over search results 
        for (var i = 0, poSearchLength = poSearchResults.length; i < poSearchLength; i++) {

            // Get current color and quantity values
            currResult = poSearchResults[i];
            color = currResult.getText('custitem3', 'item', 'group');
            quantity = currResult.getValue('quantity', null, 'sum');

            // Add to total item count
            totalUnitCount += parseInt(quantity);

            // Add to color unit text
            colorUnitText += createUnitString(quantity, color);

        }

        // Add color unit total to text
        colorUnitText += createUnitString(totalUnitCount);

        try {

            nlapiSubmitField('purchaseorder', currPoId, 'custbody_po_quantity', colorUnitText);
        } catch(e) {

            nlapiLogExecution('ERROR', 'Unable to save record after adding color item count text', e);
        }
    }


    function createUnitString(unitCount, unitColor) {

        var	retString;

        if (typeof unitColor != undefined && unitColor) {

            retString = 'Unit Count - ';
            retString += unitColor + ': ' + unitCount + '\n';
        } else {

            retString = 'Total Unit Count: ' + unitCount;
        }

        return retString;
    }*/
}

// Custom List called Set Transfer Price Status
var PO_STATUS = {
    Pending: 1,
    Completed: 2,
    IssuesFound: 3
};

/**
 * Function that will validate that any transfer pricing has been populated correctly on this Purchase Order.
 */
EC.ValidateIntercompanyPOTransferPrices = function(){

    try
    {
        var PONetSuiteProperties = ["id", "customform", "custbody_set_transfer_price_status"];
        var record = nsdal.loadObject(nlapiGetRecordType(), nlapiGetRecordId(), PONetSuiteProperties)
            .withSublist("item", ["item", "rate"]);

        // Only execute logic on Intercompany Purchase Order form
        if ( record.customform == 159 ) {      // 159 = K&A Intercompany Purchase Order
            // Loop through the line items and see if any rates are zero
            var errorsFound = false;
            _.each(record.item, function (line) {
                if (line.rate == 0) errorsFound = true;
            });

            // If any lines have a zero rate, then set the status to alert for review
            if (errorsFound)
                record.custbody_set_transfer_price_status = PO_STATUS.IssuesFound;
            else
                record.custbody_set_transfer_price_status = PO_STATUS.Completed;

            var id = record.save(true, true);
            Log.d("ValidateIntercompanyPOTransferPrices", "Record Re-submitted:  " + id);
        }
    }
    catch(e)
    {
        Log.d('ValidateIntercompanyPOTransferPrices()', "An Unexpected Error Occurred: " + e);
        var emailBody = "Script:  EC_UserEvent_PurchaseOrder.js\nFunction: ValidateIntercompanyPOTransferPrices\nError: " + e;
        nlapiSendEmail(-5, "kasupport@exploreconsulting.com", 'Error Occurred in Kit & Ace - EC_UserEvent_PurchaseOrder script', emailBody);
    }
};

Log.AutoLogMethodEntryExit(null, true, true, true);