/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_Client_TransferOrder
 * Version            1.0.0.0
 **/
function onInit(type) {

}

function onSave() {

    Log.d("STARTING");
    //EC.PopulateIntraCompanyTransferPricing_OnSave();           // This was moved to server-side scripting to ensure no governance issues
    Log.d("ENDING");
    return true;
}

function onValidateField(type, fld) {

    return true;
}

function onFieldChanged(type, fld) {

    // If the From Location changes, we need to circle through the lines and set the Intra-Company Transfer Pricing for each line
    if ( fld == "location" )
    {
        var lc = nlapiGetLineItemCount("item");
        for ( var i=1; i<=lc; i++ )
        {
            nlapiSelectLineItem("item", i);
            if ( EC.PopulateIntraCompanyTransferPricing() )
                nlapiCommitLineItem("item");
            else
                return false;
        }
    }

    return true;
}

function onPostSourcing(type, fld) {

}

function onLineSelect(type) {

}

function onValidateLine(type) {

    if ( type != 'item' ) return true;

    if ( !EC.PopulateIntraCompanyTransferPricing() )
        return false;
    else
        return true;
}

function onLineRecalc(type) {

}

/**
 * Function to validate From Location field on the Transfer Order form.  This field must always be populated before any
 * lines are added to the order.  If populated, then a Suitelet is called to get the Transfer Pricing for the line.
 * @constructor
 */
EC.PopulateIntraCompanyTransferPricing = function(){

    var fromLocation = nlapiGetFieldValue("location");
    // Ensure that From Location on this form has been populated.
    if ( !fromLocation )
    {
        alert("Please populate the From Location field before adding lines to this record.");
        return false;
    }
    else    // From Location is populated - required for next logic
    {
        // Pull the item from the current line
        var itemid = nlapiGetCurrentLineItemValue("item", "item");
        var itemname = nlapiGetCurrentLineItemText("item", "item");
        var itemType = getItemType(nlapiGetCurrentLineItemValue("item", "itemtype"));
		
		var override_price = nlapiGetCurrentLineItemValue("item", "custcol_erp_override_rate"); //IDS Get the Override Checkbox

        if ( itemType != "inventoryitem" ) return true;
						
		if(override_price == "F"){ //IDS Get the Override Checkbox - 20150824 ADD

			// Call to Suitelet to access the Item Record and retrieve the Average Cost for the given location
			var url = nlapiResolveURL('SUITELET', 'customscript_ec_slt_item_getavgcost', 'customdeploy_ec_slt_item_getavgcost');
			url += '&location=' + fromLocation + '&item=' + itemid + '&itemtype=' + itemType;
			var response = nlapiRequestURL(url);

			Log.d("SUITELET RESPONSE", response);

			if ( response )
			{
				nlapiLogExecution("DEBUG", "SUITELET RESPONSE", response.getBody());
				var responseObj = JSON.parse(response.getBody());
				if ( responseObj.cost && responseObj.cost != "NOTFOUND" ) {     // Ensure a cost was received from the Item Record
					nlapiSetCurrentLineItemValue("item", "rate", responseObj.cost, false, true);
					return true;
				} else {
					alert("Average Cost for " + itemname + " could not be retrieved from the Item Record. Please ensure this is populated on the related item record.");
					return false;
				}
			}
			else
			{
				alert("Average Cost for this item could not be retrieved from the Item Record.  Please contact Administrator for further assistance.");
				return false;
			}
		
		} else{        			//IDS Get the Override Checkbox - 20150824 ADD
			return true;        //IDS Get the Override Checkbox - 20150824 ADD	 	
		}						//IDS Get the Override Checkbox - 20150824 ADD	
    }
};

/**
 * Function to validate From Location field on the Transfer Order form.  This field must always be populated before any
 * lines are added to the order.  If populated, then a Suitelet is called to get the Transfer Pricing for the line.
 * @constructor
 */
EC.PopulateIntraCompanyTransferPricing_OnSave = function(){

    var fromLocation = nlapiGetFieldValue("location");
    Log.d("PopulateIntraCompanyTransferPricing_OnSave", "fromLocation:  " + fromLocation);
    // Ensure that From Location on this form has been populated.
    if ( !fromLocation )
    {
        alert("Please populate the From Location field before adding lines to this record.");
        //return false;
    }
    else    // From Location is populated - required for next logic
    {
        var errorMsg = "";          // Error Message variable that will help us

        // Go through each line, and populate the
        var lc = nlapiGetLineItemCount("item");
        Log.d("PopulateIntraCompanyTransferPricing_OnSave", "lineCount:  " + lc);
        for ( var i=1; i<=lc; i++ )
        {
            // Pull the item from the current line
            var itemid = nlapiGetLineItemValue("item", "item", i);
            var itemname = nlapiGetLineItemText("item", "item", i);
            var itemType = getItemType(nlapiGetLineItemValue("item", "itemtype", i));

            Log.d("PopulateIntraCompanyTransferPricing_OnSave", "itemid:  " + itemid);
            Log.d("PopulateIntraCompanyTransferPricing_OnSave", "itemname:  " + itemname);
            Log.d("PopulateIntraCompanyTransferPricing_OnSave", "itemType:  " + itemType);
            if ( itemType == "inventoryitem" )
            {
                // Call to Suitelet to access the Item Record and retrieve the Average Cost for the given location
                var url = nlapiResolveURL('SUITELET', 'customscript_ec_slt_item_getavgcost', 'customdeploy_ec_slt_item_getavgcost');
                url += '&location=' + fromLocation + '&item=' + itemid + '&itemtype=' + itemType;
                var response = nlapiRequestURL(url);

                Log.d("PopulateIntraCompanyTransferPricing_OnSave", "response:  " + response);

                if ( response )
                {
                    Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Response Body:  " + response.getBody());
                    /*var responseObj = JSON.parse(response.getBody());
                    Log.d("PopulateIntraCompanyTransferPricing_OnSave", "responseObj.cost:  " + responseObj.cost);
                    if ( responseObj.cost && responseObj.cost != "NOTFOUND" ) {     // Ensure a cost was received from the Item Record
                        nlapiSetLineItemValue("item", "rate", i, responseObj.cost);
                        //return true;
                    } else {
                        //alert("Average Cost for " + itemname + " could not be retrieved from the Item Record. Please ensure this is populated on the related item record.");
                        //return false;
                        errorMsg += itemname + "  [Line " + i + "]\n";
                    }
                    */
                    var responseObj = JSON.parse(response.getBody());
                    Log.d("PopulateIntraCompanyTransferPricing_OnSave", "response body:  " + responseObj);
                    Log.d("PopulateIntraCompanyTransferPricing_OnSave", "responseObj.cost:  " + responseObj.cost);
                    if ( responseObj.cost && responseObj.cost != "NOTFOUND" ) {     // Ensure a cost was received from the Item Record
                        //nlapiSetLineItemValue("item", "rate", i, responseObj.cost);

                        nlapiSelectLineItem('item', i);
                        nlapiSetCurrentLineItemValue('item', 'rate', responseObj.cost, false, true);
                        nlapiCommitLineItem('item');

                        Log.d("PopulateIntraCompanyTransferPricing_OnSave - LINE " + i + " SET", "responseObj.cost:  " + responseObj.cost);
                        //return true;
                    } else {
                        //alert("Average Cost for " + itemname + " could not be retrieved from the Item Record. Please ensure this is populated on the related item record.");
                        //return false;
                        errorMsg += itemname + "  [Line " + i + "]\n";
                    }
                }
                else
                {
                    //alert("Average Cost for this item could not be retrieved from the Item Record.  Please contact Administrator for further assistance.");
                    //return false;
                    errorMsg += itemname + "  [Line " + i + "]\n";
                }
            }   // END IF inventory items

        }  // Done processing through line items

        // If there are any error items we encountered, we will display these to the user
        Log.d("PopulateIntraCompanyTransferPricing_OnSave", "errorMsg:  " + errorMsg);
        if ( errorMsg )
        {
            errorMsg = "The below items will need to be manually updated because not location average cost could be found in the system:\n\n" + errorMsg;
            alert(errorMsg);
        }
    }
};


