 /**
 *	File Name		:	WTKA_GiftCard_Fix.js
 *  Script Type		: 	Scheduled
 *	Schedule		: 	Every day at <TIME>	
 *	Function		:	Script to correct invoices with incorrect Authorization Code for Gift Certificates
 * 	Authors			:	Rini Thomas
 *	Company			:	Wipro Limited
 *	Release Dates	:	DD-MMMM-2016 (v1.0)
 * 	Current Version	:	1.0
**/
 
{
	var recordList 		= new Array(), 	script_entry;
	var governanceMinUsage = 200,			retryCount = 0,	scriptMaxTimeout = 3500; //3600 - is max
}
 
function fixIncorrectGiftCards() //Scheduled script
{
	script_entry = new Date(); //Script Start Time
	try
	{
		var recordedIndex  	= nlapiLookupField('customrecordwtka_giftcard_counter', 1, 'custrecordwtka_giftcard_index');
		var index 			= parseInt(recordedIndex);
		if(index >= 0) //Records to be processed
		{
			/* Find all Invoice records with Gift Cards in Line*/
			var filters = new Array();
			filters.push(['mainline', 	'is', 	'F']); 				// Line level
			filters.push('AND');
			filters.push(['type', 		'is', 	'CustInvc']);		// Invoice
			filters.push('AND');
			filters.push(['item', 		'is', 	3048]); 			// Gift Card item
			if(index > 0)
			{
				filters.push("AND");
				filters.push(['formulanumeric: ABS(CONCAT({internalid}, {line}))', "greaterthanorequalto", index, null]);  // Lookup unprocessed records
			}
			
			var formulaString = generateFormula();
			if(formulaString != '')
			{
				filters.push("AND");
				filters.push([String(formulaString), "greaterthan", 0, null]);  // Lookup unprocessed records
			}
			
			var FinalString = new nlobjSearchColumn('formulanumeric').setFormula('ABS(CONCAT({internalid}, {line}))');
			var columns = new Array();
			columns.push(new nlobjSearchColumn('internalid'));
			columns.push(new nlobjSearchColumn('line'));
			columns.push(new nlobjSearchColumn('item'));		//GiftCert
			columns.push(new nlobjSearchColumn('memo'));		//Card Number
			columns.push(new nlobjSearchColumn('giftcert'));	// Auth Code
			columns.push(FinalString.setSort()); 
			
			var recList = nlapiSearchRecord('transaction', null, filters, columns);
			if(recList != null)
			{
				var i=0;
				for(; i < recList.length; i++)
				{
					var recId 			= recList[i].getId();
					var line 	 		= recList[i].getValue('line');
					var processString 	= String(recId) + String(line);
					lastProcessed = parseInt(processString);
					if(getScriptTimeConsumed() < scriptMaxTimeout && nlapiGetContext().getRemainingUsage() > governanceMinUsage)
					{
						if(checkExisting(recId)) continue; // find unique records
						recordList.push(recId);
						try
						{
							fixRecord(recId);
							recordList.push(recId);
							// updateIndex(lastProcessed);
						}
						catch(err_fix)
						{
							var errorRef = err_fix;
							if(errorRef.code == 'SSS_SEARCH_TIMEOUT')
							{
								nlapiLogExecution('DEBUG', 'Error in Processing request', recId);
								retryCount++;
								if(retryCount > 3) 
								{
									nlapiLogExecution('DEBUG', 'Process Aborted', err_fix);
									updateIndex(-1); //Abort process
									break;
								}
								else // Retry 3 times
								{
									updateIndex(lastProcessed);
									nlapiLogExecution('DEBUG', 'Rescheduling script', 'Process timed out and rescheduling script');
									var status = nlapiScheduleScript('customscript_wtka_giftcard_fix', 'customdeploy_wtka_giftcard_fix'); // RescheduleScript
									nlapiLogExecution('debug', 'Script', status); 
								}
							}
							else
							{
								nlapiLogExecution('debug', 'Error in Processing request', 	recId);
								nlapiLogExecution('debug', 'Error Details', 				err_fix);
								updateIndex(-1); //Abort process
								break;
							}
						}
					}
					else
					{
						updateIndex(lastProcessed);
						var status 	  = nlapiScheduleScript('customscript_wtka_giftcard_fix', 'customdeploy_wtka_giftcard_fix'); // RescheduleScript
						nlapiLogExecution('debug', 'Script', status); 
						// break;
					}
				}
				if(i == recList.length)
				{
					if(index > 0)	updateIndex(0); // Reset Counters
				}
			}
			else
			{
				//DO NOTHING
				if(index < 0)	updateIndex(0); //Reset Counters
			}
		}
	}
	catch(Err_fixIncorrectGiftCards)
	{
		//ERROR HANDLING
		nlapiLogExecution('debug', 'Err_fixIncorrectGiftCards', Err_fixIncorrectGiftCards);
	}
}

function generateFormula()
{
	var formulaString = 'formulanumeric: CASE';
	var columns = new Array();
	columns.push(new nlobjSearchColumn('name'));
	columns.push(new nlobjSearchColumn('custrecord_wtka_giftlist_authcode'));

	var searchResult = nlapiSearchRecord('customrecord_wtka_giftcard_list', null, null, columns);
	for(var s=0; searchResult != null && s<searchResult.length; s++)
	{
		formulaString += ' WHEN DECODE({memo}, \'' + searchResult[s].getValue('name') + '\', \'1\', \'0\') = 1 AND DECODE ({giftcertificate}, \'' + searchResult[s].getValue('custrecord_wtka_giftlist_authcode') + '\', \'1\', \'0\') = 0 THEN 1';
	}
	formulaString += ' ELSE 0 END';

	if(searchResult == null)	formulaString = '';
	nlapiLogExecution('debug', 'formulaString', formulaString);
	return formulaString;
}

function checkExisting(recId)
{
	for(var r=0; r<recordList.length; r++)
	{
		if(recId == recordList[r])	return true;
	}
	return false;
}

function fixRecord(recId)
{
	var change = false;
	try
	{
		// var giftCertRecord = new Array();
		var record = nlapiLoadRecord('invoice', recId); //INVOICE RECORD ONLY
		for(var l=0; l<record.getLineItemCount('item'); l++)
		{
			var itemType = record.getLineItemValue('item', 'itemtype', l+1);
			if(itemType != 'GiftCert') continue; //Skip if not Gift Certificate
			
			var authCode = record.getLineItemValue('item', 'giftcertnumber', l+1); //Gift Cert Auth Code
			var cardNo	 = record.getLineItemValue('item', 'description', 	 l+1); //Card Number
			
			var validAuthCode = lookupAuthCode(cardNo, authCode);
			switch(validAuthCode.status)
			{
				case 'ERROR': 
					//ERROR HANDLING ?? Break process?
					nlapiLogExecution('debug', 'lookupAuthCode', validAuthCode.value);
					break;
				case 'VALID':
					//DO NOTHING - VALID AUTH CODE
					continue; // Skip line
				default : //SUCCESS
					
					// Add new line with correct Gift Card Auth Code
					record.selectNewLineItem('item');
					record.setCurrentLineItemValue('item', 'item', 						record.getLineItemValue('item', 'item', 					l+1)); 
					record.setCurrentLineItemValue('item', 'description', 				cardNo); 
					record.setCurrentLineItemValue('item', 'giftcertfrom', 				record.getLineItemValue('item', 'giftcertfrom', 			l+1)); 
					record.setCurrentLineItemValue('item', 'giftcertrecipientname', 	record.getLineItemValue('item', 'giftcertrecipientname', 	l+1)); 
					record.setCurrentLineItemValue('item', 'giftcertrecipientemail', 	record.getLineItemValue('item', 'giftcertrecipientemail', 	l+1)); 
					record.setCurrentLineItemValue('item', 'giftcertmessage', 			record.getLineItemValue('item', 'giftcertmessage', 			l+1)); 
					record.setCurrentLineItemValue('item', 'giftcertnumber', 			validAuthCode.value); 
					record.setCurrentLineItemValue('item', 'quantity', 					record.getLineItemValue('item', 'quantity', 				l+1)); 
					record.setCurrentLineItemValue('item', 'price', 					record.getLineItemValue('item', 'price', 					l+1)); 
					record.setCurrentLineItemValue('item', 'amount', 					record.getLineItemValue('item', 'amount', 					l+1)); 
					record.commitLineItem('item');
					
					// giftCertRecord.push(record.getLineItemValue('item', 'giftcertkey', l+1)); 
					
					// Remove line with incorrect Gift Card Auth Code
					record.removeLineItem('item', l+1);
					change = true;
			}
		}
		if(change)
		{
			nlapiSubmitRecord(record);
		}
	}
	catch(Err_fixRecord)
	{
		//ERROR HANDLING
		nlapiLogExecution('debug', 'Err_fixRecord', Err_fixRecord);
	}
}

function lookupAuthCode(cardNo, authCode)
{
	var resp = new Object();
	try
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('name', 	null, 'is', cardNo));
		// filters.push(new nlobjSearchFilter('custrecord_wtka_giftlist_cardnumber', 	null, 'is', cardNo));
		// filters.push(new nlobjSearchFilter('custrecord_wtka_giftlist_authcode', 	null, 'is', authCode));
		
		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_wtka_giftlist_authcode'));
		// columns.push(new nlobjSearchColumn('custrecord_wtka_giftlist_giftcard'));
		
		var recList = nlapiSearchRecord('customrecord_wtka_giftcard_list', null, filters, columns);
		if(recList == null) 
		{
			resp.status = 'ERROR';
			resp.value  = 'NOT FOUND';
		}
		else
		{
			if(recList.length > 1)
			{
				resp.status = 'ERROR';
				resp.value  = 'DUPLICATES || ' + JSON.stringify(recList);
			}
			else
			{
				if(authCode == recList[0].getValue('custrecord_wtka_giftlist_authcode'))
				{
					resp.status = 'VALID';
					resp.value  = 'VALID AUTH CODE';
				}
				else
				{
					resp.status = 'SUCCESS';
					// resp.value  = recList[0].getValue('custrecord_wtka_giftlist_giftcard');
					resp.value  = recList[0].getValue('custrecord_wtka_giftlist_authcode');
				}
			}
		}
	}
	catch(Err_lookupAuthCode)
	{
		resp.status = 'ERROR';
		resp.value  = Err_lookupAuthCode;
	}
	return resp;
}

function getScriptTimeConsumed()
{
	var currentTime = new Date();
	var timeDiff 	= (currentTime - script_entry)/1000; //in seconds
	return timeDiff;
}

function updateIndex(index)
{
	//UPDATE COUNTER
	var fields = new Array();
	var values = new Array();
	fields.push('custrecordwtka_giftcard_index');
	values.push(index);
	nlapiSubmitField('customrecordwtka_giftcard_counter', 1, fields, values, false);
}

