/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 */

    	 
define(['N/record',
        'N/url',
        'N/http',
        'N/https',
        'N/search',
        'N/error',
        '../../ERP20/Closing Notes/moment+localejs'],
		mainClosingNotesCS
    );


function mainClosingNotesCS(record, url, http, https, search, error) {
	
    function alertPageLoaded(context) {
           	
    	var currentRecord = context.currentRecord;    
    	var customForm = currentRecord.getValue('customform');
    	
    	if(customForm != 124){
    	
	    	var locId = currentRecord.getValue('custrecord_erp_dclose_shop_name');
	    	var dateCreated = currentRecord.getValue('custrecord_erp_date_created'); 
	    	console.log('alertPageLoaded: this date ' + dateCreated);
	 
	    	
	    	//DATE && SALES
	    	var datesObject = setAllDates(currentRecord, url, https);
	    	
	    	//UPTS
	    	//setUPTs(currentRecord, locId ,datesObject.today, datesObject.today); 
	    	
	    	//Other Stuff
	    	//setShopClosing(currentRecord, locId ,datesObject.today, datesObject.today);
	    	
	    	setMiscInfo(currentRecord, url, https, error, locId ,datesObject.today, datesObject.today);
	    	setMiscInfo2(currentRecord, url, https, error, locId ,datesObject.weekstart, datesObject.weekend);
    	    	
    	}
    	    	
    };
    
 function fieldChanged(context) {
    	
    	var currentRecord = context.currentRecord;    
    	var customForm = currentRecord.getValue('customform');
    	
    	if(customForm != 124){
    		
//		    	console.log('fieldChanged: change: ' + context.fieldId);
		        var currentRecord = context.currentRecord;
		        
		        var name = context.fieldId; var periodType = '';
		        
		        if(name == 'custrecord_erp_dclose_daily_goal' || name == 'custrecord_erp_dclose_daily_actual'){			 
		        	periodType =  'daily';
		        	
		        	var  dailyActual = currentRecord.getValue('custrecord_erp_dclose_daily_actual');
	 				var  dailyActualHours = currentRecord.getValue('custrecord_erp_dclose_actual_hours');
	 				var salesLabHour = dailyActual / dailyActualHours;
	 				
	 				if(dailyActualHours ==  0 || dailyActualHours == '' ){
	 					currentRecord.setValue('custrecord_erp_dclose_saleslabhour', 0);
					}else{
						currentRecord.setValue('custrecord_erp_dclose_saleslabhour', salesLabHour);
					}
				}
		        else if(name == 'custrecord_erp_dclose_weekly_goal' || name == 'custrecord_erp_dclose_weekly_actual'){			 
		        	periodType =  'weekly';
				}
		        else if(name == 'custrecord_erp_dclose_period_goal' || name == 'custrecord_erp_dclose_period_actual'){			 
		        	periodType =  'period';
				}
		        else if(name == 'custrecord_erp_dclose_qtr_goal' || name == 'custrecord_erp_dclose_qtr_actual'){			 
		        	periodType =  'qtr';
				}
		        else if(name == 'custrecord_erp_date_created' || name == 'custrecord_erp_dclose_shop_name'){
		        	
		        	var locId = currentRecord.getValue('custrecord_erp_dclose_shop_name');
		        	var dateCreated = currentRecord.getValue('custrecord_erp_date_created'); 
		        	
		        	if(!isNullOrEmpty(locId) && !isNullOrEmpty(dateCreated)){
		        		var datesObject = setAllDates(currentRecord, url, https);
		        		setMiscInfo(currentRecord, url, https, error, locId, datesObject.today,  datesObject.today)
//			        	setUPTs(currentRecord,locId,datesObject.today,datesObject.today);
//			        	setShopClosing(currentRecord, locId ,datesObject.today, datesObject.today);
		        	}
		        }else if(name == 'custrecord_erp_dclose_actual_hours'){

	 				var  dailyActual = currentRecord.getValue('custrecord_erp_dclose_daily_actual');
	 				var  dailyActualHours = currentRecord.getValue('custrecord_erp_dclose_actual_hours');
	 				var salesLabHour = dailyActual / dailyActualHours;
	 				
	 				if(dailyActualHours ==  0 || dailyActualHours == '' ){
	 					currentRecord.setValue('custrecord_erp_dclose_saleslabhour', 0);
					}else{
						currentRecord.setValue('custrecord_erp_dclose_saleslabhour', salesLabHour);
					}
		        
		        }
		        
		        else if(name == 'custrecord_erp_dclose_returns' || name == 'custrecord_erp_dclose_ecomm_returns'){

		        		 var regular = currentRecord.getValue('custrecord_erp_dclose_ecomm_returns');
						 var total = currentRecord.getValue('custrecord_erp_dclose_returns');
						 var exEcomm = total - regular;

						 currentRecord.setValue('custrecord_erp_dclose_regreturn', exEcomm);


		        }
		        
		        else if(name == 'custrecord_erp_dclose_netsuite_closing' || name == 'custrecord_erp_dclose_payment_terminal' ){

					var nsClosing = currentRecord.getValue('custrecord_erp_dclose_netsuite_closing');
		 			var terminal = currentRecord.getValue('custrecord_erp_dclose_payment_terminal');

		 			var varianceNSTerminal =  terminal - nsClosing;
		 			currentRecord.setValue('custrecord_erp_dclose_variance', varianceNSTerminal );
				}
		        
		        
		        else if(name == 'custrecord_erp_dclose_day_footfall' || name == 'custrecord_erp_dclose_day_transactioncnt' ){

					var ffall = currentRecord.getValue('custrecord_erp_dclose_day_footfall');
		 			var trancnt = currentRecord.getValue('custrecord_erp_dclose_day_transactioncnt');
		 			
		 			if(!ffall){ ffall = 1; 
		 				currentRecord.setValue('custrecord_erp_dclose_day_conversion', '' );
		 			}else{
		 				var conversionday = trancnt / ffall;
		 				currentRecord.setValue('custrecord_erp_dclose_day_conversion', conversionday );
		 			}

		 			
		 			
				}
		        
		        else if(name == 'custrecord_erp_dclose_wtd_footfall' || name == 'custrecord_erp_dclose_wtd_transactioncnt' ){

		        	var ffall = currentRecord.getValue('custrecord_erp_dclose_wtd_footfall');
		 			var trancnt = currentRecord.getValue('custrecord_erp_dclose_wtd_transactioncnt');
		 			
		 			if(!ffall){ ffall = 1; 
		 				currentRecord.setValue('custrecord_erp_dclose_wtd_conversion', '' );
		 			}else{
		 				var conversionday = trancnt / ffall;
		 				currentRecord.setValue('custrecord_erp_dclose_wtd_conversion', conversionday );
		 			}
	
		        	
				}
		        

		        //Unknown?
		        
		        
		        if(!isNullOrEmpty(periodType)){
		        	calculateToGoals(currentRecord, periodType);
		        	
		        	if(periodType == 'daily'){
		        		//additional stuff //TODO
		        	}
		        	
		        }
		        
		        
		        
		        
    	}
        
        
        
    }
    
    

    function isNullOrEmpty(valueStr){
        return(valueStr == null || valueStr == "" || valueStr == undefined );
    }

    function setUPTs(currentRecord,locId,date1,date2){
    	    			
                var mySearch = search.load({
                    id: 'customsearch1188',                   
                });

    			mySearch.filterExpression = [['location', 'anyof' , locId],
                 							 'and',
                 							 ['datecreated', 'within', [date1, date2]]
                 							];
    			
    			var searchResult = mySearch.run().getRange({
                  start: 0,
                  end: 100
                  })
                  
            	  console.log('setUPTs: searching:' + date1 + ' : ' +date2 + " result:" + searchResult.length); 
                  
                  if(!isNullOrEmpty(searchResult)){

      	                var columns = searchResult[0].columns;
      	                var objResult = searchResult[0];
      	
      	    			var objUPTSPT = new Object();
      	
      	    			objUPTSPT.location = objResult.getValue(columns[0]);
      	    			objUPTSPT.upt = objResult.getValue(columns[1]);
      	    			objUPTSPT.spt = objResult.getValue(columns[2]);
      	    			objUPTSPT.aur = objResult.getValue(columns[3]); 
      	    			
      	    		 
      	    			var upt = objResult.getValue(columns[1]);
	      	  			upt = Number(upt).toFixed(1);
	      	  			var spt = objResult.getValue(columns[2]);
	      	  			spt = Number(spt).toFixed(0);
	
	      	  			var aur = objResult.getValue(columns[3]);
	      	  			aur = currentRecord.getValue('custrecord_erp_dclose_daily_actual')/aur;
	      	  			aur = Number(aur).toFixed(0);
	
	      	  		
		      	  		currentRecord.setValue('custrecord_erp_dclose_upt_units_pertrans', upt);
			      	  	currentRecord.setValue('custrecord_erp_dclose_sptdpt', spt);
			      	  	currentRecord.setValue('custrecorderp_dclose_aur', aur);
	      	  			
      	    			console.log('setUPTs: upts are here: ' + JSON.stringify(objUPTSPT));
          			
                  }                                                 
    	
    }
    
    function setShopClosing(currentRecord,locId,date1,date2){
		
    	
        var searchShopClosing = search.load({
            id: 'customsearch_erp_script_cn_closing',                   
        });

        searchShopClosing.filterExpression = [['location', 'anyof' , [locId]],
         							 'and',
         							 ['trandate', 'within', [date1, date2]]
         							];
		
		var searchResult = searchShopClosing.run().getRange({
          start: 0,
          end: 100
          })
          
    	  console.log('setShopClosing: searching:' + date1 + ' : ' +date2 + " result:" + searchResult.length); 
          
          if(!isNullOrEmpty(searchResult)){

	                var columns = searchResult[0].columns;
	                var objResult = searchResult[0];
	    			
	    			var objClosingSales = new Object();

	    			objClosingSales.location = objResult.getValue(columns[0]);
	    			objClosingSales.closingsales = objResult.getValue(columns[1]);
	    			objClosingSales.col2 = objResult.getValue(columns[2]);
	    			objClosingSales.col3 = objResult.getValue(columns[3]);
	    			objClosingSales.col4 = objResult.getValue(columns[4]);
//	    			objClosingSales.col5 = objResult.getValue(columns[5]);

	    			currentRecord.setValue('custrecord_erp_dclose_netsuite_closing',
	    					objClosingSales.closingsales);

  	  		      	  		
  	  			
	    			console.log('setShopClosing: upts are here: ' + JSON.stringify(objClosingSales));
  			
          }                                                 

}
    
    function setMiscInfo(currentRecord, url, https, error, locationId, tranDate1, tranDate2){
    	
    	//console.log('LOG2: get here if you can');
    	try{
    		
    		var locationId = currentRecord.getValue('custrecord_erp_dclose_shop_name');
        
//        	console.log('this date: ' + todayDate);
        	
        	var disposalLink = url.resolveScript({
                scriptId            : 'customscript_erp_sdc_sl_misc', //'customscript_erp_shopclosing_sltest', //
                deploymentId        : 'customdeploy_erp_sdc_sl_misc', //'customdeploy_erp_shopclosing_sl', //
                returnExternalUrl   : false,
                params              : null
            });
        	
        	disposalLink = disposalLink +
        					'&location=' + locationId + 
        					'&trandate1=' + tranDate1 + 
        					'&trandate2=' + tranDate2;
        	
        	console.log('LOG2: disposalLink' + disposalLink);
        	
    		var response = https.get.promise({url:disposalLink}).then(
	    			
	    			function(result){
	    				
	    				//console.log("this result via the new url resolution: " + result.body);
	    	    		var miscObj  = JSON.parse(result.body);
	    	    		
	    	    		if(!isNullOrEmpty(miscObj)){	    	    				    	    			
							currentRecord.setValue('custrecord_erp_dclose_netsuite_closing', miscObj.closing.closingsales);	
							
							currentRecord.setValue('custrecord_erp_dclose_upt_units_pertrans', miscObj.upt.upt);
							currentRecord.setValue('custrecord_erp_dclose_sptdpt',  miscObj.upt.spt);


							var actual = currentRecord.getValue('custrecord_erp_dclose_daily_actual');  
							
							if(actual && miscObj.upt.aur > 0){  
								
								console.log('aur this' + miscObj.upt.aur);
								currentRecord.setValue('custrecorderp_dclose_aur',  actual/miscObj.upt.aur);													
								
														
														
							}	
							
							currentRecord.setValue('custrecord_erp_dclose_day_transactioncnt',  miscObj.upt.trans);
							
							currentRecord.setValue('custrecord_erp_dclose_peak_times', miscObj.peaktime);
							
							currentRecord.setValue('custrecord_erp_dclose_fd',Math.abs(miscObj.posdiscount.fd));
							currentRecord.setValue('custrecord_erp_dclose_employee_discounts',Math.abs(miscObj.posdiscount.ed));
							
	    	    		}
	
	    	    	}
	    			
	    	).catch(	    	
	    		function onRejected(reason) {
	    			console.log('misc error:' + reason);
	    			
//	    			function(error) {
//	    		        function createError() {
//	    		            var errorObj = 
//	    		            throw error.create({
//	    		                name: 'MY_CODE',
//	    		                message: 'my error details - ' + reason,
//	    		                notifyOff: false
//	    		            });
	    		            	
	    		            
	    		            
	    		            
//	    		        }
//	    		        createError();
//	    		    }
	    		}
	    	);
    		
    		
    		
    		
    		
    		console.log('LOG2: ' + response.body);
    		
    		return response; 
    		
        	}catch(ex){ return null;}
    }
    
    
    
    function setMiscInfo2(currentRecord, url, https, error, locationId, tranDate1, tranDate2){
    	
    	try{
    		
    		var locationId = currentRecord.getValue('custrecord_erp_dclose_shop_name');
        

        	
        	var disposalLink = url.resolveScript({
                scriptId            : 'customscript_erp_sdc_sl_misc', //'customscript_erp_shopclosing_sltest', //
                deploymentId        : 'customdeploy_erp_sdc_sl_misc', //'customdeploy_erp_shopclosing_sl', //
                returnExternalUrl   : false,
                params              : null
            });
        	
        	disposalLink = disposalLink +
        					'&location=' + locationId + 
        					'&trandate1=' + tranDate1 + 
        					'&trandate2=' + tranDate2;
        	
        	console.log('LOG2: disposalLink' + disposalLink);
        	
    		var response = https.get.promise({url:disposalLink}).then(
	    			
	    			function(result){
	    					    				
	    	    		var miscObj  = JSON.parse(result.body);
	    	    		
	    	    		if(!isNullOrEmpty(miscObj)){	    	    				    	    			
						
	    	    			currentRecord.setValue('custrecord_erp_dclose_wtd_transactioncnt',  miscObj.upt.trans);
	    	    			
	    	    		}
	
	    	    	}
	    			
	    	).catch(	    	
	    		function onRejected(reason) {
	    			console.log('misc error:' + reason);	    			
	    		}
	    	);    		    		    		    		    
    		
    		return response; 
    		
        }catch(ex){ return null;}
    	
    }

    
    function setAllDates(currentRecord, url, https){
    	
    	var datesObject = new Object();
    	
    		
    	var inputDate = currentRecord.getValue('custrecord_erp_date_created');
    	//console.log('this input date: ' + inputDate);
    	
    	var formatDate = moment(inputDate); //,'MM/DD/YYYY');
    	var todayDate = formatDate.format('L');

    	var accountPeriodObject = getAccountingPeriod(url,https, todayDate); 
    	
    	if(!isNullOrEmpty(accountPeriodObject)){
    		
	    	var accountPeriodObject = JSON.parse(accountPeriodObject.body);
	    	var dateRange = accountPeriodObject;
	
	    	var weekStart = formatDate.startOf('isoweek').format('L');
	    	var weekEnd = formatDate.endOf('isoweek').format('L');
	
	    	var locId = currentRecord.getValue('custrecord_erp_dclose_shop_name');
    	
    		currentRecord.setValue('custrecord_day_daterange', todayDate); 
    		getSalesRangeSL(locId, "daily", todayDate, todayDate, currentRecord, url, https);
    		getSalesRangeSL(locId, 'dailycredit', todayDate, todayDate, currentRecord, url, https) 
    		
    		
    		currentRecord.setValue('custrecord_week_daterange', weekStart + ' - ' + weekEnd); 
    		getSalesRangeSL(locId, "weekly", weekStart, weekEnd, currentRecord, url, https);
    		
    		currentRecord.setValue('custrecord_period_daterange', accountPeriodObject.period.start + "  - " + accountPeriodObject.period.end);
    		getSalesRangeSL(locId, "period", accountPeriodObject.period.start,  accountPeriodObject.period.end, currentRecord, url, https);
    		
    		currentRecord.setValue('custrecord_quarter_daterange', accountPeriodObject.quarter.start + "  - " + accountPeriodObject.quarter.end);
    		getSalesRangeSL(locId, "qtr", accountPeriodObject.quarter.start, accountPeriodObject.quarter.end, currentRecord, url, https);
    		    		


    	}
    	
    	datesObject.today = todayDate;
    	datesObject.weekstart = weekStart; 
    	datesObject.weekend = weekEnd; 
    	
    	return datesObject;
    }
 
    
    function getSalesRangeSL(locId, mode, date1, date2, currentRecord, url, https) {

    	try{
	    	//TODO
	    	var disposalLink = url.resolveScript({
	            scriptId            : 'customscript_erp_shop_daily_closing_sl', //'customscript_erp_shopclosing_sltest', //
	            deploymentId        : 'customdeploy_erp_shop_daily_closing_sl', //'customdeploy_erp_shopclosing_sl', //
	            returnExternalUrl   : false,
	            params              : null
	        });
	    	
	    	
	    	var modeTxt =mode;
	
	    	if(mode == 'daily' ||
	    	   mode == 'weekly'||
	    	   mode == 'period'||
	    	   mode == 'qtr' ){
	
	    	   mode = 'salesinfo';
	
	    	}else if('dailycredit'){
	
	    	   mode = 'creditinfo';
	
	    	}else{ mode = ''; }
	
	
	    	var params = new Array();
	    	params['locationId'] = locId;
	    	params['transactionDate1'] = date1;
	    	params['transactionDate2'] = date2;
	    	params['mode'] = mode; //'salesinfo';
	    	
	    	
	    	disposalLink = disposalLink + 
	    								'&locationId=' + locId +
								    	'&transactionDate1=' + date1 +
								    	'&transactionDate2=' + date2 +
								    	'&mode=' + mode;
	    	
	    	//console.log('SL' + disposalLink);
	    	
	
	    	https.get.promise({url:disposalLink}).then(
	    			
	    			function(result){
	    				
	    				//console.log("this result via the new url resolution: " + result.body);
	    	    		var salesObj = JSON.parse(result.body);
	    	    		
	    	    		    	    		
	    	    		var salesArrIds = new Array();
	    				var salesArrObj = new Array();
	
	    				salesArrIds = salesObj.ids;
	    				salesArrObj = salesObj.obj;
	    				
	    	    		var salesIndex = salesArrIds.indexOf("213213");
						if (salesIndex > -1) {
							var obj = salesArrObj[salesIndex];
							var netSales = obj.netsales_fx;
							var netSalesIncTax = obj.netsales_inctax_fx;
	
							var currency = salesArrObj[salesIndex].currency;
							//nlapiSetFieldValue('custrecord_erp_dclose_currency', currency);
	
							if(modeTxt != 'dailycredit'){
								currentRecord.setValue('custrecord_erp_dclose_' + modeTxt + '_actual', netSales);
								
								if(modeTxt == 'daily'){
									
									currentRecord.setValue('custrecord_erp_dclose_daily_inctax', netSalesIncTax);
								}
								
							}else{
								currentRecord.setValue('custrecord_erp_dclose_returns', netSales);	
							}
						}else{
							currentRecord.setValue('custrecord_erp_dclose_' + modeTxt + '_actual', 0);
							
						}
	
	    	    	}
	    	).catch(function onRejected(reason) {
	    		currentRecord.setValue('custrecord_erp_dclose_' + modeTxt + '_actual', 0);
	          });
	    	
    	}catch(ex){}
	    	//return slResponse;
    }
    
    function getAccountingPeriod(url,https, todayDate){
    	
    	try{
    	var paramsarr = new Array();
    	paramsarr['transactionDate'] = todayDate;
    	
    	//console.log('this date: ' + todayDate);
    	
    	var disposalLink = url.resolveScript({
            scriptId            : 'customscript_erp_shop_daily_closing_sl', //'customscript_erp_shopclosing_sltest', //
            deploymentId        : 'customdeploy_erp_shop_daily_closing_sl', //'customdeploy_erp_shopclosing_sl', //
            returnExternalUrl   : false,
            params              : null
        });
    	
    	disposalLink = disposalLink + '&transactionDate=' + todayDate;
    	
		var response = https.get({url:disposalLink});
		console.log('LOG1: ' + response.body);
		
		return response; 
		
    	}catch(ex){ return null;}
   
    }
 
    
    function getAjax(){
    	
		var promise = new Promise(function(resolve, reject){

			var url = 'https://system.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=550&deploy=1',
			    isAsync = true,
			    xhr = new XMLHttpRequest();
			
			xhr.addEventListener('load', function (event) {
			    if (xhr.readyState === 4) {
			        if (xhr.status === 200) {
			            resolve(xhr.responseText);			    
			        	
			        	console.log(  new Date()); 
			        } 
			        else {
			            reject( xhr.statusText );
			        	
			        	console.log( xhr.statusText ); 
			        }
			     }
			});
			xhr.addEventListener('error', function (event) {
			    reject( xhr.statusText );
			});
			xhr.open('GET', url, isAsync);
			    xhr.send();
			    
		})

        	return promise;
        	
    }
    
    
   
    function calculateToGoals(currentRecord, periodType){
    	
    	 var sales 		= 	currentRecord.getValue('custrecord_erp_dclose_'+ periodType +'_actual');
		 var goal		= 	currentRecord.getValue('custrecord_erp_dclose_'+ periodType +'_goal');
		 var fSales 	= 	0; fSales = Math.round(sales);
		 var fGoal		= 	0; fGoal = Math.round(goal);
		 
		 var diff = fSales - fGoal;
	     var perc = 0;
	     
	    // console.log('fsale and fgoal' + fSales + ' ' + fGoal); 
	     
	     if (!isNullOrEmpty(fGoal) && (fGoal > 0)) {
	    	 
	    	 if(!isNaN(fGoal)){
	    		 	perc = (fSales / fGoal) * 100;
		    		perc = Math.round(perc);
	    	 }
	    		

	    }
		 
		 currentRecord.setValue('custrecord_erp_dclose_'+ periodType +'_togoal',diff);
		 currentRecord.setValue('custrecord_erp_dclose_'+ periodType +'_ptogoal',perc);
    	
    }
    
    

    
    function auditRecordChanges(type, id) {
//        var logRecord = record.create({type: 'customrecord_log', isDynamic: true});
//        logRecord.setValue('custrecord_recordType', type);
//        logRecord.setValue('custrecord_recordId', id);
//        logRecord.setValue('custrecord_changedDate', new Date());
//        logRecord.save();
    }
    
    
    function updateMemoFieldWhenItemFieldIsChanged(context) {
        if(context.fieldId === 'item') {
//            context.currentRecord.setValue('memo', 'item field is changed');
        	console.log('track this please'); 
        }
    }
    
    
    
    return({
        pageInit: alertPageLoaded,
        fieldChanged: fieldChanged
    });
}