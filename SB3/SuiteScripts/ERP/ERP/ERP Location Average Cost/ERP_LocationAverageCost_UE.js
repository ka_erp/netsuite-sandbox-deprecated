

function main_SetLocationAverageCost_BS(type){
		
		//if(type == 'create'){
						
			var idLocation = nlapiGetFieldValue('location');
			
			var arrItems = getRecordItems();
						
			var arrAverageCost = getLocationAverageCost(arrItems, idLocation);
			setLocationAverageCost(arrAverageCost);						
		
		//}
			
}


function setLocationAverageCost(arrAverageCost){
	
	//var intItemCount = nlapiGetLineItemCount('item');
	
	if(!isNullOrEmpty(arrAverageCost)){
		
		for(var i = 0 ; i < arrAverageCost.length; i++){
			
			var objAverageCost = arrAverageCost[i];
			
			nlapiSetLineItemValue('item', 'custcol_erp_loc_average_cost', objAverageCost.line , objAverageCost.aveCost );
			
		}
	
	}
	
	
	
	
	
}

function getRecordItems(){
	
	var intItemCount = nlapiGetLineItemCount('item');
	
	var objRecordItems = new Object();
		
	var arrResultObjectArray = new Array();		
	var arrIdObjectArray = new Array();
	
	if(!isNullOrEmpty(intItemCount)){
		
		for(var i = 1 ; i <= intItemCount; i++){
						
			 var objTemp = new Object(); 
			 var itemId = nlapiGetLineItemValue('item', 'item', i);
			 
			 
			 objTemp.id = itemId;
			 objTemp.line = i; 
			 
			 arrIdObjectArray.push(objTemp.id);
			 arrResultObjectArray.push(objTemp);
			
		}
	
	}
	
	objRecordItems.ids = arrIdObjectArray;  
	objRecordItems.obj = arrResultObjectArray;
	
	return objRecordItems; 
	

}

function getLocationAverageCost(arrItems, idLocation){		
	
	if (!isNullOrEmpty(idLocation) && !isNullOrEmpty(arrItems)){

		
		var arrItemIds = arrItems.ids;
		var arrItemObj = arrItems.obj;
		
		var arrLocationAverageCost = new Array();
		
		nlapiLogExecution('AUDIT', 'TEST THE ARR ITEMS',  arrItemIds); 
		
		var arSavedSearchItemMainResultsItm = null;
	    var arSaveSearchFiltersItm = new Array();
	    var arSavedSearchItemMainColumnsItm = new Array();
	    var strSavedSearchIDItm = null;

	    arSaveSearchFiltersItm.push(new nlobjSearchFilter('inventorylocation', null , 'is', idLocation ));
	    
	    arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null , 'anyof', arrItemIds));

	    arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
	    arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('name'));
	    arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('inventorylocation'));
	    arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('locationaveragecost')); 
	    
	    
	    arSavedSearchItemMainResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchItemMainColumnsItm);
	    
	    
	    if (!isNullOrEmpty(arSavedSearchItemMainResultsItm)){
	    	
	        for(var i = 0 ;  i < arSavedSearchItemMainResultsItm.length; i++ ){	         	
	        		        		        		        
	        	var internalId = arSavedSearchItemMainResultsItm[i].getValue('internalid');	        	
	        	var position = arrItemIds.indexOf(internalId);
	        	var line = arrItemObj[position].line;	        		        
	        	var aveCost = arSavedSearchItemMainResultsItm[i].getValue('locationaveragecost');
	        	var name = arSavedSearchItemMainResultsItm[i].getValue('name');	        		        
	        	var objAveCost = new Object();
	        	
	        	objAveCost.id = internalId;
	        	objAveCost.line = line;
	        	objAveCost.aveCost = aveCost;
	        	objAveCost.name = name;
	        	
	        	nlapiLogExecution('AUDIT', 'TEST THE AVE COST LeNGTH',  ' >>>>>> ' + JSON.stringify(objAveCost)); 
	        	
	        	arrLocationAverageCost.push(objAveCost); 
	        	
	        }
	        
	        
	        nlapiLogExecution('AUDIT', 'TEST THE AVE COST LeNGTH',  arSavedSearchItemMainResultsItm.length);
	        
	        return arrLocationAverageCost; 
	        
	    }else{
	    	
	    	return null; 
	    }
	    	    
		
	}
	
	
}



function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}

