/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Aug 2015     casual
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */

function shopMaintenanceBeforeLoad(type){
	
	nlapiLogExecution('DEBUG', 'shopMaintenanceBeforeLoad ', 'type');
	
//	var intLocationId = nlapiGetFieldValue('custrecord_svm_location');
//	
//	nlapiLogExecution('DEBUG', 'before load', 'this is the initial locaiton ' + intLocationId);
//	var objLocationRecord = nlapiLoadRecord('location', intLocationId);
//
////	nlapiSetFieldValue('custrecord_svm_internet_provider', objLocationRecord.getFieldValue('custrecord_ka_internet_provider')); 
//	
	
	if(type == 'create'){
		nlapiGetField('custrecord_svm_location').setDisplayType('normal');
	}
	else if(type == 'edit' || type != 'xedit'){
		nlapiGetField('custrecord_svm_location').setDisplayType('disabled');
	}
	
}

function shopMaintenanceBeforeSubmit(type){
 
       nlapiLogExecution('DEBUG', 'shopMaintenanceBeforeSubmit ', type);


        if(type == 'xedit'){        	
			nlapiLogExecution('DEBUG', 'xedit ', 'type');
			
			var oldRec = nlapiGetOldRecord();						
			var oldLocId = oldRec.getFieldValue('custrecord_svm_location'); 
			
			
			var newRec = nlapiGetNewRecord();  
			   
// 			var recordID = nlapiGetNewRecord.getId();  
						
//			var objSM = new Object(); 
//			objSM.id = newRec.getFieldValue('id'); 			
//			var rec = nlapiLoadRecord(  nlapiGetRecordType(), objSM.id ); 
			
			var idLocRecId = nlapiLookupField(nlapiGetRecordType(),  newRec.getFieldValue('id') ,  'custrecord_svm_location'); 
			
			
//			nlapiLogExecution('DEBUG', 'xedit ', JSON.stringify(objSM));
//			nlapiLogExecution('DEBUG', 'xedit record field',fields[5] );
//			nlapiLogExecution('DEBUG', 'xedit record rec', JSON.stringify(rec));
			
			
			
			var arFields = nlapiGetNewRecord().getAllFields();
			
			for(var i in arFields)
			{						
				nlapiLogExecution('DEBUG', 'xedit new record array ' + i, arFields[i]);
				
				if(arFields[i] == 'custrecord_svm_shop_phone'){
					nlapiSubmitField('location', idLocRecId,'custrecord_ka_store_phone', nlapiGetFieldValue('custrecord_svm_shop_phone'));										
				}else if(arFields[i] =='custrecord_svm_shop_director'){				
					nlapiSubmitField('location', idLocRecId,'custrecord_ka_shop_director', nlapiGetFieldValue('custrecord_svm_shop_director'));
				}else if(arFields[i] =='custrecord_svm_shop_director_phone'){					
					nlapiSubmitField('location', idLocRecId,'custrecord_ka_shop_director_contact', nlapiGetFieldValue('custrecord_svm_shop_director_phone'));
				}
				
												
				else if(arFields[i] =='custrecord_svm_internet_provider'){										
					nlapiSubmitField('location', idLocRecId,'custrecord_ka_internet_provider', nlapiGetFieldValue('custrecord_svm_internet_provider'));
				}else if(arFields[i] =='custrecord_svm_phone_provider'){															
					nlapiSubmitField('location', idLocRecId,'custrecord_ka_phone_provider', nlapiGetFieldValue('custrecord_svm_phone_provider'));
				}else if(arFields[i] =='custrecord_svm_payment_provider'){			
					nlapiSubmitField('location', idLocRecId,'custrecord_ka_payment_processor', nlapiGetFieldValue('custrecord_svm_payment_provider'));
				}else if(arFields[i] =='custrecord_svm_music_provider'){										
					nlapiSubmitField('location', idLocRecId,'custrecord_ka_music_provider', nlapiGetFieldValue('custrecord_svm_music_provider'));
				}else if(arFields[i] =='custrecord_svm_sensormatic_provider'){										
					nlapiSubmitField('location', idLocRecId,'custrecord_ka_sensormatic_provider', nlapiGetFieldValue('custrecord_svm_sensormatic_provider'));
				}else if(arFields[i] =='custrecord_svm_shop_email'){					
					nlapiSubmitField('location', idLocRecId,'custrecord_ka_shop_email', nlapiGetFieldValue('custrecord_svm_shop_email'));
				}else if(arFields[i] =='custrecord_svm_merchant_id'){
					nlapiSubmitField('location', idLocRecId,'custrecord_ka_merchant_id', nlapiGetFieldValue('custrecord_svm_merchant_id'));
				}else if(arFields[i] =='custrecord_svm_retail_shop_type'){
					nlapiSubmitField('location', idLocRecId,'custrecord_ka_retail_shop_type', nlapiGetFieldValue('custrecord_svm_retail_shop_type'));
				}else if(arFields[i] ==	'custrecord_svm_ip_address'){	
					nlapiSubmitField('location', idLocRecId,'custrecord_ka_ip_addresss', nlapiGetFieldValue('custrecord_svm_ip_address'));
				}
				    																										
				
			}
									
//			nlapiLogExecution('DEBUG', 'xedit record shop phone', phoneShop); 
			nlapiLogExecution('DEBUG', 'xedit record id',  idLocRecId);
			nlapiLogExecution('DEBUG', 'xedit record loc', oldLocId);
			
		
       }
          
	if(type != 'delete'  && type != 'xedit'){
	
		var intLocationId = nlapiGetFieldValue('custrecord_svm_location'); 
		var strLocation = nlapiGetFieldText('custrecord_svm_location');
		
		nlapiLogExecution('DEBUG', 'type', type);
		nlapiLogExecution('DEBUG', 'Location', intLocationId);
		
	    var searchFilters = [];
	    searchFilters[0] =  new nlobjSearchFilter('custrecord_svm_location', null, 'is', intLocationId);
	    
	    var searchColumns = [new nlobjSearchColumn('internalid')];
	    
	    var results = nlapiSearchRecord('customrecord_shop_vendor_maintenance', null, searchFilters, searchColumns);
	    
	    if (!isNullOrEmpty(results)){
	    	
	    	if(results.length > 0 && type == 'create'){
	    		nlapiLogExecution('DEBUG', 'result length', results.length);
	    		throw nlapiCreateError('SCRIPT_ERROR',strLocation + ' shop already exist');
	    		
	    	}else{
	    		nlapiLogExecution('DEBUG', 'result length', results.length);
	    			 			    	
	 			var objLocationRecord = nlapiLoadRecord('location', intLocationId);
	    		
	    		if(type == 'edit'){
	    			
	    			
	    			var idInternetProvider = nlapiGetFieldValue('custrecord_svm_internet_provider');
		    		var idPhoneProvider = nlapiGetFieldValue('custrecord_svm_phone_provider');
		    		var idPaymentProcessor = nlapiGetFieldValue('custrecord_svm_payment_provider');
		    		var idMusicProvider = nlapiGetFieldValue('custrecord_svm_music_provider');
		    		var idSensormaticProvider = nlapiGetFieldValue('custrecord_svm_sensormatic_provider');
		    		
		    				   			  		
					var idShopDirector = nlapiGetFieldValue('custrecord_svm_shop_director'); 
					var strShopDirectorPhone = nlapiGetFieldValue('custrecord_svm_shop_director_phone');
					var strShopPhone =nlapiGetFieldValue('custrecord_svm_shop_phone');
					var strShopEmail =nlapiGetFieldValue('custrecord_svm_shop_email');
						 
					var strMerchantID = nlapiGetFieldValue('custrecord_svm_merchant_id');
					var idRetailShopType = nlapiGetFieldValue('custrecord_svm_retail_shop_type');
					var strIpAddress = nlapiGetFieldValue('custrecord_svm_ip_address');		
		    		
		    		
		    		
		    		
//		    		 idPhoneProvider = objLocationRecord.getFieldValue('custrecord_ka_phone_provider');
//		    		 idPaymentProcessor = objLocationRecord.getFieldValue('custrecord_ka_payment_processor');
//		    		 idMusicProvider = objLocationRecord.getFieldValue('custrecord_ka_music_provider');
//		    		 idSensormaticProvider = objLocationRecord.getFieldValue('custrecord_ka_sensormatic_provider');
//		    		 idMerchId = objLocationRecord.getFieldValue('custrecord_ka_merchant_id');
//		    		 strIPAddress = objLocationRecord.getFieldValue('custrecord_ka_ip_addresss'); 
	    			
	    			objLocationRecord.setFieldValue('custrecord_ka_phone_provider', idPhoneProvider); 
	    			objLocationRecord.setFieldValue('custrecord_ka_payment_processor', idPaymentProcessor); 
	    			objLocationRecord.setFieldValue('custrecord_ka_music_provider', idMusicProvider); 
	    			objLocationRecord.setFieldValue('custrecord_ka_sensormatic_provider', idSensormaticProvider); 	    			 	    				    			
					objLocationRecord.setFieldValue('custrecord_ka_internet_provider', idInternetProvider);
					
					objLocationRecord.setFieldValue('custrecord_ka_merchant_id', strMerchantID);
		    		                  
		    		objLocationRecord.setFieldValue('custrecord_ka_retail_shop_type', idRetailShopType);	    			    			    			    			    			    		    			    	    		 	    		
		    		objLocationRecord.setFieldValue('custrecord_ka_shop_director', idShopDirector);
		    		                  
		    		objLocationRecord.setFieldValue('custrecord_ka_ip_addresss', strIpAddress);
		    		objLocationRecord.setFieldValue('custrecord_ka_store_phone',strShopPhone); 
		    		objLocationRecord.setFieldValue('custrecord_ka_shop_email', strShopEmail);	    		
		    		objLocationRecord.setFieldValue('custrecord_ka_shop_director_contact', strShopDirectorPhone);	   		    					   			    		
	    				    			
	    			var idLocation = nlapiSubmitRecord(objLocationRecord); nlapiLogExecution('DEBUG', 'submittin', idLocation); 		    				    		
	    			
	    		}
	    		
	    	}    	
	    	
	    }else if(type == 'create'){
	    		
	    		nlapiGetField('custrecord_svm_location').setDisplayType('normal');
	    		var objLocationRecord = nlapiLoadRecord('location', intLocationId);
	    	
				var idInternetProvider = objLocationRecord.getFieldValue('custrecord_ka_internet_provider');
	    		var idPhoneProvider = objLocationRecord.getFieldValue('custrecord_ka_phone_provider');
	    		var idPaymentProcessor = objLocationRecord.getFieldValue('custrecord_ka_payment_processor');
	    		var idMusicProvider = objLocationRecord.getFieldValue('custrecord_ka_music_provider');
	    		var idSensormaticProvider = objLocationRecord.getFieldValue('custrecord_ka_sensormatic_provider');
	    		var idMerchId = objLocationRecord.getFieldValue('custrecord_ka_merchant_id');
	    		 
	    		var idRetailShopType = objLocationRecord.getFieldValue('custrecord_ka_retail_shop_type');	    			    			    			    			    			    		    			    	    		 	    		
	    		var idShopDirector = objLocationRecord.getFieldValue('custrecord_ka_shop_director');
	    		 
	    		var strIPAddress = objLocationRecord.getFieldValue('custrecord_ka_ip_addresss');
	    		var strShopPhone = objLocationRecord.getFieldValue('custrecord_ka_store_phone'); 
	    		var strShopEmail = objLocationRecord.getFieldValue('custrecord_ka_shop_email');	    		
	    		var strShopDirectorPhone = objLocationRecord.getFieldValue('custrecord_ka_shop_director_contact');	    		
		   		var strPaymentProcessorContact = objLocationRecord.getFieldValue('custrecord_ka_payment_processor_phone'); 
		   		var strPhoneProviderContact = objLocationRecord.getFieldValue('custrecord_ka_phone_contact');
		   		var strInternetProviderContact = objLocationRecord.getFieldValue('custrecord_ka_internet_contact');
		   		var strSensormaticProviderConact = objLocationRecord.getFieldValue('custrecord_sensormatic_contact');
		   		var strMusicProviderContact = objLocationRecord.getFieldValue('custrecord_ka_music_provider_contact');
		   		
		   		
//		   		strMerchandiseID = objLocationRecord.getFieldValue('custrecord_ka_merchant_id'); 
		   		
		   		nlapiSetFieldValue('custrecord_svm_phone_provider', idPhoneProvider);
		   		nlapiSetFieldValue('custrecord_svm_internet_provider', idInternetProvider);
		   		nlapiSetFieldValue('custrecord_svm_payment_provider', idPaymentProcessor);
		   		nlapiSetFieldValue('custrecord_svm_sensormatic_provider', idSensormaticProvider);
		   		nlapiSetFieldValue('custrecord_svm_music_provider', idMusicProvider);
		   				   		
				nlapiSetFieldValue('custrecord_svm_shop_director',idShopDirector); 
				nlapiSetFieldValue('custrecord_svm_shop_director_phone',strShopDirectorPhone);
				nlapiSetFieldValue('custrecord_svm_shop_phone',strShopPhone);
				nlapiSetFieldValue('custrecord_svm_shop_email',strShopEmail);
						
				nlapiSetFieldValue('custrecord_svm_merchant_id',idMerchId);
				nlapiSetFieldValue('custrecord_svm_retail_shop_type',idRetailShopType);
				nlapiSetFieldValue('custrecord_svm_ip_address',strIPAddress);																								
						
    		}
                 	
	    }
	    
	    
	    
	}



function isNullOrEmpty(valueStr){
    return(valueStr == null || valueStr == "" || valueStr == undefined);
}



