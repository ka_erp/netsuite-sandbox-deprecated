/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       10 Aug 2015     ivan.sioson
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */


//FLD_TICKBOX = 'custpage_tickbox';

function main_suitelet(request, response){

	processMssg = 'main_suitelet';

	//Display projects per customer
	if( request.getMethod() == 'GET' )
	{
		
		 var currLocation = request.getParameter('param_location');					
		 nlapiLogExecution('DEBUG', 'Location from the SL Params', currLocation); 
		
		 var form = nlapiCreateForm('Shop Information' );
				 
		 //Retrieve Fields
		 var intUserId =  nlapiGetUser(); 

		 
		 //Load the Location Record
		 var idEmpLocation = null; 
		 
		 
		 //Find out the Locatation
		 
		 if(!isNullOrEmpty(currLocation)){
			 idEmpLocation = currLocation;			 
			 
		 }else{
			 
			 //Get Stuff from the Employee Record			 			 			 
			 idEmpLocation = nlapiLookupField('employee', intUserId, 'location' );
			 idEmpSubsidiary = nlapiLookupField('employee', intUserId, 'subsidiary' );
			 
			 nlapiLogExecution('DEBUG', 'Location from the User Location', idEmpLocation);
			 			 			 
			 
			 var bRunLocationDefaulLookup = false; 
			 			 			 			 
			 if(isNullOrEmpty(idEmpLocation)){	
				 bRunLocationDefaulLookup = true;				 				 
			 }
			 else{
				 
				//Not Done yet. Get Stuff from the Location Record
				 var locLookType = nlapiLookupField('location', idEmpLocation, 'custrecord_ra_loctype' );
				 if(locLookType != 1){
					 bRunLocationDefaulLookup = true; 
				 }				 				 
			 }
			 
			 
			 nlapiLogExecution('DEBUG', 'are we doing a lookup since the user location is not good?', bRunLocationDefaulLookup);
			 if(bRunLocationDefaulLookup){	
				 
				 	var arSavedSearchResultsLoc = null;    
					var arSaveSearchFiltersLoc = new Array();
					var arSavedSearchColumnsLoc = new Array();
					var strSavedSearchIDLoc = null;
					
					arSaveSearchFiltersLoc.push(new nlobjSearchFilter('subsidiary', null , 'is', idEmpSubsidiary));
					arSaveSearchFiltersLoc.push(new nlobjSearchFilter('custrecord_ra_loctype', null , 'is', 1));		//This is Hardcoded due to the Store Type = Retail - 1
					
					arSavedSearchColumnsLoc.push(new nlobjSearchColumn('internalid'));
					arSavedSearchColumnsLoc.push(new nlobjSearchColumn('namenohierarchy').setSort());
					
					arSavedSearchResultsLoc = nlapiSearchRecord('location', strSavedSearchIDLoc, arSaveSearchFiltersLoc, arSavedSearchColumnsLoc);
				 
					if (!isNullOrEmpty(arSavedSearchResultsLoc)){														
							var invid = arSavedSearchResultsLoc[0].getValue('internalid');	
							idEmpLocation = invid; 
					}	
					
					 nlapiLogExecution('DEBUG', 'Location from the User Location is not valid. Defaulting to the first location up from list', idEmpLocation);
			 }

		}
			 
			 
		
		 
		 
		 if(isNullOrEmpty(idEmpLocation)){			 
			 var fldError = form.addField( 'custpage_error', 'text', 'Error' ).setDisplayType("inline").setLayoutType('startrow').setDefaultValue("Your user does not have a location record. Please contact your administrator") ;			 
		 }
		 else
			 {
				 
			 nlapiLogExecution('DEBUG', 'Im building the form', 'wait for the form for location ' + idEmpLocation);	 
				 
			 var objLocationRecord = nlapiLoadRecord('location', idEmpLocation);
			 
			 //Get fields from Location Record
			 
			 var strLocId = objLocationRecord.getId(); 
			 var strStorePhone = objLocationRecord.getFieldValue('custrecord_ka_store_phone'); 
			 var strShopDirector = objLocationRecord.getFieldValue('custrecord_ka_shop_director'); 
			 var strRetailShopType = objLocationRecord.getFieldValue('custrecord_ka_retail_shop_type'); 
			 
			 		 
			 var emailShopEmailAddress = objLocationRecord.getFieldValue('custrecord_ka_shop_email');
			 var txtAreaShopAddress = objLocationRecord.getFieldValue('mainaddress_text');		 				 		
			 var phnShopDirectorPhone = objLocationRecord.getFieldValue('custrecord_ka_shop_director_contact'); 
			 		 
	         var strPaymentProcessor = objLocationRecord.getFieldText('custrecord_ka_payment_processor');
			 var strPaymentProcessorContact = objLocationRecord.getFieldValue('custrecord_ka_payment_processor_phone');
			 var strMerchId = objLocationRecord.getFieldValue('custrecord_ka_merchant_id');
			 var strPhoneProvider = objLocationRecord.getFieldText('custrecord_ka_phone_provider');
			 var strPhoneProviderContact = objLocationRecord.getFieldValue('custrecord_ka_phone_contact');
			 var strInternetProvider = objLocationRecord.getFieldText('custrecord_ka_internet_provider');
			 var strInternetProviderContact = objLocationRecord.getFieldValue('custrecord_ka_internet_contact');
			 var strSensormaticProvider = objLocationRecord.getFieldText('custrecord_ka_sensormatic_provider');
			 var strSensormaticProviderConact = objLocationRecord.getFieldValue('custrecord_sensormatic_contact');
			 var strMusicProvider = objLocationRecord.getFieldText('custrecord_ka_music_provider');
			 var strMusicProviderConact = objLocationRecord.getFieldValue('custrecord_ka_music_provider_contact');
			 var strIPAddress = objLocationRecord.getFieldValue('custrecord_ka_ip_addresss'); 
			 var strMerchandiseID = objLocationRecord.getFieldValue('custrecord_ka_merchant_id'); 
			 
			 //		 var strDayMonday = ((objLocationRecord.getFieldValue('custrecord_ka_day_mon')) == "T" ? true : false);
			 var strDayMonday = objLocationRecord.getFieldValue('custrecord_ka_day_mon'); 
			 var strDayTuesday = objLocationRecord.getFieldValue('custrecord_ka_day_tue');
			 var strDayWednesday = objLocationRecord.getFieldValue('custrecord_ka_day_wed');
			 var strDayThursday = objLocationRecord.getFieldValue('custrecord_ka_day_thu');
			 var strDayFriday = objLocationRecord.getFieldValue('custrecord_ka_day_fri');
			 var strDaySaturday = objLocationRecord.getFieldValue('custrecord_ka_day_sat');
			 var strDaySunday = objLocationRecord.getFieldValue('custrecord_ka_day_sun');
			 
			 
			 nlapiLogExecution('DEBUG', 'Location JSON >>', objLocationRecord); 
			 
			 nlapiLogExecution('DEBUG', 'Location strLocId >>', strLocId);
			 nlapiLogExecution('DEBUG', 'Location emailShopEmailAddress >>', emailShopEmailAddress);
			 nlapiLogExecution('DEBUG', 'Location txtAreaShopAddress >>', txtAreaShopAddress);
			 nlapiLogExecution('DEBUG', 'Location phnShopDirectorPhone >>', phnShopDirectorPhone);
			 
			 
			 nlapiSetFieldValue('emailShopEmailAddress', emailShopEmailAddress);
			 
			 
			 var idSubsidiary = nlapiGetSubsidiary(); //objLocationRecord.getFieldValue('subsidiary');
			 
			 
			 //Add fields to the form				 
			 
			 var fldLocation = form.addField( 'custpage_location_name', 'select', 'Location' );
			 var fldRetailShopType = form.addField( 'custpage_retail_shop_type', 'select', 'Retail Shop Type' );
			 var fldShopEmailAddress = form.addField( 'custpage_email_address', 'email', 'Shop Email Address' );
			 var fldShopAddress = form.addField( 'custpage_shop_address', 'textarea', 'Shop Address' );		 		
			 var fldShopDirector = form.addField( 'custpage_shop_director', 'select', 'Shop Director' );
			 
			 var fldShopDirectorPhone = form.addField( 'custpage_shop_director_phone', 'phone', 'Shop Director Phone' );		 		 
			 var fldStorePhone = form.addField( 'custpage_store_phone', 'phone', 'Store Phone' );
			 
			 //Providers
			 
			 var fldPaymentProvider	= form.addField('custpage_payment_provider', 'text', 'Payment Provider' ); 	 
			 var fldPaymentProviderPhone	= form.addField('custpage_payment_provider_phone', 'textarea', 'Payment Provider Phone' );
			 var fldPaymentProviderMerchID	= form.addField('custpage_payment_provider_merchid', 'text', 'Merchandize ID' );
			 var fldPaymentIPAddress	= form.addField('custpage_ip_address', 'text', 'IP Address' );
			 
			 
			 
			 var fldPhoneProvider = form.addField( 'custpage_phone_provider', 'text', 'Phone Provider' );
			 var fldPhoneProviderPhone	= form.addField('custpage_phone_provider_phone', 'textarea', 'Phone Provider Phone' );
			 
			 var fldInternetProvider = form.addField( 'custpage_internet_provider', 'text', 'Internet Provider' );
			 var fldInternetProviderPhone = form.addField( 'custpage_internet_provider_phone', 'textarea', 'Internet Provider Phone' );
			 		 
			 var fldSersormaticProvider =form.addField( 'custpage_sensormatic_provider', 'text', 'Sensormatic Provider' );
			 var fldSersormaticProviderPhone =form.addField( 'custpage_sensormatic_provider_phone', 'textarea', 'Sensormatic Provider Phone'  );
			 		 
			 var fldMusicProvider = form.addField( 'custpage_music_provider', 'text', 'Music Provider' );
			 var fldMusicProviderPhone = form.addField( 'custpage_music_provider_phone', 'textarea', 'Music Provider Phone' );
			 
			 
			 var fldDayAll = form.addField( 'custpage_day_all', 'checkbox', 'All' );
			 var fldDayMonday = form.addField( 'custpage_day_monday', 'checkbox', 'M' );
			 var fldDayTuesday = form.addField( 'custpage_day_tuesday', 'checkbox', 'T' );
			 var fldDayWednesday = form.addField( 'custpage_day_wednesday', 'checkbox', 'W' );
			 var fldDayThursday = form.addField( 'custpage_day_thursday', 'checkbox', 'TH' );
			 var fldDayFriday = form.addField( 'custpage_day_friday', 'checkbox', 'F' );
			 var fldDaySaturday = form.addField( 'custpage_day_saturday', 'checkbox', 'SA' );
			 var fldDaySunday = form.addField( 'custpage_day_sunday', 'checkbox', 'SU' );
			 
			 
			 
			 		 		 
	 
	 //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	 //																									SELECT OPTIONS
	 //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	 
			 // Populate the Shop Director field
			 		 
				var arSavedSearchResults = null;    
				var arSaveSearchFilters = new Array();
				var arSavedSearchColumns = new Array();
				var strSavedSearchID = null;
		
				
				arSaveSearchFilters.push(new nlobjSearchFilter('subsidiary', null , 'is', idSubsidiary));			
				arSavedSearchColumns.push(new nlobjSearchColumn('firstname').setSort());
				arSavedSearchColumns.push(new nlobjSearchColumn('lastname'));
				arSavedSearchColumns.push(new nlobjSearchColumn('internalid'));    
		
				arSavedSearchResults = nlapiSearchRecord('employee', strSavedSearchID, arSaveSearchFilters, arSavedSearchColumns);
					
				fldShopDirector.addSelectOption('0', ' ', true);	
				
				if (!isNullOrEmpty(arSavedSearchResults)){
					
					for (var i=0; i < arSavedSearchResults.length; i++){				
						var invid = arSavedSearchResults[i].getValue('internalid');
						var invname = arSavedSearchResults[i].getValue('firstname');
						var invlastname = arSavedSearchResults[i].getValue('lastname');
						
						fldShopDirector.addSelectOption(invid, invname + " " + invlastname);							
					}
					
				}	
				
			// Populate the Location field					
				
				var arSavedSearchResultsLoc = null;    
				var arSaveSearchFiltersLoc = new Array();
				var arSavedSearchColumnsLoc = new Array();
				var strSavedSearchIDLoc = null;
		
				
				arSaveSearchFiltersLoc.push(new nlobjSearchFilter('subsidiary', null , 'is', idSubsidiary));
				arSaveSearchFiltersLoc.push(new nlobjSearchFilter('custrecord_ra_loctype', null , 'is', 1));		//This is Hardcoded due to the Store Type = Retail - 1
				
				arSavedSearchColumnsLoc.push(new nlobjSearchColumn('internalid'));
				arSavedSearchColumnsLoc.push(new nlobjSearchColumn('namenohierarchy').setSort());
		
				arSavedSearchResultsLoc = nlapiSearchRecord('location', strSavedSearchIDLoc, arSaveSearchFiltersLoc, arSavedSearchColumnsLoc);
				
				
				if (!isNullOrEmpty(arSavedSearchResultsLoc)){
				
					for (var i=0; i < arSavedSearchResultsLoc.length; i++){				
						var invid2 = arSavedSearchResultsLoc[i].getValue('internalid');
						var invname2 = arSavedSearchResultsLoc[i].getValue('namenohierarchy');
		
						
						fldLocation.addSelectOption(invid2, invname2);							
					}				
				}	
			
			// Populate the Retail Shop Field field
			
				var arSavedSearchResultsRST = null;    
				var arSaveSearchFiltersRST = new Array();
				var arSavedSearchColumnsRST = new Array();
				var strSavedSearchIDRST = null;
			
				arSavedSearchColumnsRST.push(new nlobjSearchColumn('internalid'));
				arSavedSearchColumnsRST.push(new nlobjSearchColumn('name').setSort());
									
				arSavedSearchResultsRST = nlapiSearchRecord('customlist_ka_retail_shop_type', strSavedSearchIDRST, arSaveSearchFiltersRST, arSavedSearchColumnsRST);
				
	//			console.log(arSavedSearchResultsLoc.length);
				
				fldRetailShopType.addSelectOption('0', ' ', true);
				
				if (!isNullOrEmpty(arSavedSearchResultsRST)){
					
					for (var i=0; i < arSavedSearchResultsRST.length; i++){				
						var invid2 = arSavedSearchResultsRST[i].getValue('internalid');
						var invname2 = arSavedSearchResultsRST[i].getValue('name');
		
						
						fldRetailShopType.addSelectOption(invid2, invname2);							
					}				
				}	
			
				 
	 //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	 //																									Making fields Inline
	 //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	 	
			var viewRoleId = nlapiGetContext().getSetting('SCRIPT', 'custscript_ka_erp_roleview');
				
			if(nlapiGetRole() == viewRoleId){
				
				fldShopDirector.setDisplayType("inline");
				fldShopDirectorPhone.setDisplayType("inline");
				fldStorePhone.setDisplayType("inline");
				fldDayAll.setDisplayType("inline");
				fldDayMonday.setDisplayType("inline");
				fldDayTuesday.setDisplayType("inline");
				fldDayWednesday.setDisplayType("inline");
				fldDayThursday.setDisplayType("inline");
				fldDayFriday.setDisplayType("inline");
				fldDaySaturday.setDisplayType("inline");
				fldDaySunday.setDisplayType("inline");
				fldRetailShopType.setDisplayType("inline");
			}
			
	
			 // Set Field Display Type
			 fldLocation.setLayoutType('startrow'); 
			 
			 fldPaymentProviderPhone.setDisplayType("inline");
			 fldPaymentProvider.setDisplayType("inline");
			 fldPhoneProvider.setDisplayType("inline");
			 fldPhoneProviderPhone.setDisplayType("inline");
			 fldInternetProvider.setDisplayType("inline"); 
			 fldInternetProviderPhone.setDisplayType("inline"); 
			 fldSersormaticProvider.setDisplayType("inline");
			 fldSersormaticProviderPhone.setDisplayType("inline"); 
			 fldMusicProvider.setDisplayType("inline");
			 fldMusicProviderPhone.setDisplayType("inline");
			 fldShopEmailAddress.setDisplayType("inline");
			 fldShopAddress.setDisplayType("inline");
			 fldPaymentProviderMerchID.setDisplayType("inline");
			 fldPaymentIPAddress.setDisplayType("inline");
					 
			 
			 
			 nlapiLogExecution("DEBUG", "Moday " + (objLocationRecord.getFieldValue('custrecord_ka_day_mon')) , strDayMonday); 
			 
	//		 ((city.getName() == null) ? "N/A" : city.getName());
			
			 // Set Values to the fields in the Suitelet
			 
	//		 emailShopEmailAddress
	//		 txtAreaShopAddress
	//		 phnShopDirectorPhone
			 
	//		 nlapiSetFieldValue('custpage_day_monday', "T");
			 
			 
			 fldDayMonday.setDefaultValue(strDayMonday);		
			 fldDayTuesday.setDefaultValue(strDayTuesday);
			 fldDayWednesday.setDefaultValue(strDayWednesday);		 
			 fldDayThursday.setDefaultValue(strDayThursday);
			 fldDayFriday.setDefaultValue(strDayFriday);
			 fldDaySaturday.setDefaultValue(strDaySaturday);
			 fldDaySunday.setDefaultValue(strDaySunday);
			 
			 
	//		 fldDayMonday.setDefaultValue(strMerchandiseID);
	//		 fldDayTuesday.setDefaultValue(strMerchandiseID);
	//		 fldDayWednesday.setDefaultValue(strMerchandiseID);
	//		 fldDayThursday.setDefaultValue(strMerchandiseID);
	//		 fldDayFriday.setDefaultValue(strMerchandiseID);
	//		 fldDaySaturday.setDefaultValue(strMerchandiseID);
	//		 fldDaySunday.setDefaultValue(strMerchandiseID);
			 
			 fldPaymentProviderMerchID.setDefaultValue(strMerchandiseID);
			 fldPaymentIPAddress.setDefaultValue(strIPAddress);
			 fldMusicProvider.setDefaultValue(strMusicProvider);
			 fldMusicProviderPhone.setDefaultValue(strMusicProviderConact);
			 
			 
			 fldShopEmailAddress.setDefaultValue(emailShopEmailAddress);  
			 fldShopAddress.setDefaultValue(txtAreaShopAddress);  	
			 
			 		 
			   
			 fldStorePhone.setDefaultValue(strStorePhone);
			 
			 if(!isNullOrEmpty(strShopDirector)){
				 fldShopDirector.setDefaultValue(strShopDirector);
			 }else{
				 fldShopDirector.setDefaultValue("0");
			 }
			 
			 if(!isNullOrEmpty(strRetailShopType)){
				 fldRetailShopType.setDefaultValue(strRetailShopType);			 
			 }else{
				 fldRetailShopType.setDefaultValue("0");
			 }
			 
			 
			 fldShopDirectorPhone.setDefaultValue(phnShopDirectorPhone); 
			 		 
			 
			 fldPaymentProviderPhone.setDefaultValue(strPaymentProcessorContact);
			 fldPaymentProvider.setDefaultValue(strPaymentProcessor);
			 
			 fldPhoneProvider.setDefaultValue(strPhoneProvider);
			 fldPhoneProviderPhone.setDefaultValue(strPhoneProviderContact);
			 
			 fldInternetProvider.setDefaultValue(strInternetProvider); 
			 fldInternetProviderPhone.setDefaultValue(strInternetProviderContact);
			 
			 fldSersormaticProvider.setDefaultValue(strSensormaticProvider);
			 fldSersormaticProviderPhone.setDefaultValue(strSensormaticProviderConact);
			 
			 fldLocation.setDefaultValue(strLocId);
			 
	//		 fldMusicProvider.setDefaultValue();
	//		 fldMusicProviderPhone.setDefaultValue();
			 
			 form.addSubmitButton( 'Submit' );
			 form.addResetButton( 'Reset' );
			 
		}
		 
		 //Logging				 
		 nlapiLogExecution('DEBUG', 'User', intUserId); 
		 nlapiLogExecution('DEBUG', 'Shop Director', strShopDirector); 
		 nlapiLogExecution('DEBUG', 'Location', strLocId); 
//		 nlapiLogExecution('DEBUG', 'Location JSON', JSON.stringify(objLocationRecord)); 
		 
		 
		 //Attach Client Script
		 form.setScript('customscript_erp_location_extension_cs');
		 

		 //Write the view to the form
		 response.writePage( form );
		 
		 		 				 
	}else{

//		var newDirector = request.getFieldValue( 'custpage_shop_director' );
		
		nlapiLogExecution('DEBUG','Testing','I need to know if this goes through');
		nlapiLogExecution('DEBUG','Store Director', request.getParameter('custpage_shop_director'));
		
		var idLocationID = request.getParameter('custpage_location_name');
		
		
//		var intUserId =  nlapiGetUser(); 
//		var idEmpLocation = nlapiLookupField('employee', intUserId, 'location' ); 	
		
		var idShopDirectorNew = request.getParameter('custpage_shop_director');
		var phnShopPhone = request.getParameter('custpage_store_phone');
		var phnShopDirectorPhone = request.getParameter('custpage_shop_director_phone');
		var idRetailShopType = request.getParameter('custpage_retail_shop_type');	
		
		
		var bMonday = request.getParameter('custpage_day_monday');
		var bTuesday = request.getParameter('custpage_day_tuesday');
		var bWednesday = request.getParameter('custpage_day_wednesday');
		var bThursday = request.getParameter('custpage_day_thursday');
		var bFriday = request.getParameter('custpage_day_friday');
		var bSaturday = request.getParameter('custpage_day_saturday');
		var bSunday = request.getParameter('custpage_day_sunday');	
		
		
		
		
		if(idShopDirectorNew != "0" || idShopDirectorNew != 0){
			nlapiSubmitField('location', idLocationID, "custrecord_ka_shop_director", idShopDirectorNew); // custrecord_ka_shop_director
		}else{
			nlapiSubmitField('location', idLocationID, "custrecord_ka_shop_director", ""); // custrecord_ka_shop_director
		}
		
		
		nlapiSubmitField('location', idLocationID, "custrecord_ka_store_phone", phnShopPhone); // custrecord_ka_shop_director		
		nlapiSubmitField('location', idLocationID, "custrecord_ka_shop_director_contact", phnShopDirectorPhone); // custrecord_ka_shop_director
		
		
		
		//Update shop
		
		nlapiSubmitField('location', idLocationID, 'custrecord_ka_day_mon', bMonday);
		nlapiSubmitField('location', idLocationID, 'custrecord_ka_day_tue', bTuesday); 
		nlapiSubmitField('location', idLocationID, 'custrecord_ka_day_wed', bWednesday);
		nlapiSubmitField('location', idLocationID, 'custrecord_ka_day_thu', bThursday); 
		nlapiSubmitField('location', idLocationID, 'custrecord_ka_day_fri', bFriday); 
		nlapiSubmitField('location', idLocationID, 'custrecord_ka_day_sat', bSaturday); 
		nlapiSubmitField('location', idLocationID, 'custrecord_ka_day_sun', bSunday);
		
		nlapiSubmitField('location', idLocationID, 'custrecord_ka_day_sun', bSunday); 
		nlapiSubmitField('location', idLocationID, 'custrecord_ka_retail_shop_type', idRetailShopType); 
		
		
		
		
		
		var params = new Array();
		params['param_location'] = idLocationID;		
							
		nlapiSetRedirectURL( 'SUITELET', 'customscript_erp_location_extension', 'customdeploy_erp_location_extension', null, params);
	
	}

}



function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}