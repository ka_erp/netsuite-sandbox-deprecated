/**
 *	File Name		:	ERP_WPORT_HYB_ExternalOrder_002_UE.js
 *	Function		:	Order Process - Step 2 (Decides which restlet to call for further processing)
 * 	Remarks			:	Relevant scripts isolated from WTKA_NetSuite_ICTO_Process.js
 *	Prepared by		:	A.Villanueva
 *	Release Dates	:	31-May-2016
 * 	Current Version	:	1.0
**/

function AfterSubmitICTO(type)
{

	// var exec = nlapiGetExecution();
	// var context = exec.getContext();
	//if type == xedit
	nlapiLogExecution('debug', 'type', type);

	if(type == 'create')
	{


		var tranId = nlapiGetFieldValue('custrecord_wtka_orderid');
		try
		{
			checkInventoryTransfer(nlapiGetRecordId());
		}
		catch(ue_err)
		{
			nlapiLogExecution('debug', 'checkInventoryTransfer Error', ue_err);
			var message = '<br><b>Error Details: </b><br>' + JSON.stringify(ue_err);
			sendOrderMail('Inventory', message, dataIn);
			
			createTransactionLog(null, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 12, SYNC_STATUS.ERROR, JSON.stringify(ue_err), null, 20); //Library call to WTKA_Library.js //Record Type 12 is Inventory Transfer
		}
	}
	else if(type == 'edit' || type =='xedit')
	{
		var rollback = nlapiGetFieldValue('custrecord_wtka_rollback');	
		nlapiLogExecution('debug', 'rollback', rollback);
		if(rollback == 'T')
		{
			/*try
			{
				// Trigger ROLLBACK RESTlet
				var orderRequest 			= new Object();
				orderRequest.id  			= nlapiGetRecordId();
				var processed_ids			= nlapiGetFieldValue('custrecord_wtka_processed_ids');
				orderRequest.orderDetails  	= (processed_ids != null || processed_ids != '') ? JSON.parse(processed_ids) : processed_ids;
				nlapiLogExecution('debug', 'Rollback - Request', JSON.stringify(orderRequest));
				var restletResponse 		= nlapiRequestURL(RollbkURL, JSON.stringify(orderRequest), headers);
				if(restletResponse.code == 200)
				{
					var respBody  = JSON.parse(restletResponse.body);
					var resp 	  = nlapiGetFieldValue('custrecord_wtka_final_response');
					if(resp != null)	finalResponse = JSON.parse(resp);
					finalResponse.records.push(respBody);
					respBody = JSON.stringify(finalResponse);
					
					setRecordFieldValues(orderRequest.id, null, finalResponse, 'F', 6);
				}
				nlapiLogExecution('debug', 'Rollback RESTLET Call', restletResponse.getBody());
			}
			catch(rollback_err)
			{
				nlapiLogExecution('debug', 'Rollback exception', rollback_err);
				var message = '<br><b>Error Details: </b><br>' + JSON.stringify(rollback_err);
				sendOrderMail('Order', message, dataIn);
			}*/

			var tranId = nlapiGetFieldValue('custrecord_wtka_orderid');
			var rollbackMessage = 'Rollback is TRUE. The record(s) will not be processed.';
			createTransactionLog(null, tranId, null, null, null, null, SYNC_STATUS.ERROR, rollbackMessage, null, 700); //Library call to WTKA_Library.js
		}
		else
		{
			decideFlow(nlapiGetRecordId());
		}
		linkTransactionsToExternalWTKA(nlapiGetRecordId()); //Call to WTKA_Library.js
	}
}

function checkInventoryTransfer(recID)
{
	//Invoke Inventory Transfers RESTLET for further processing 
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wtka_order_status', 	null, 'anyof', 1));
	filters.push(new nlobjSearchFilter('internalid', 					null, 'anyof', recID));

	var cols = new Array();
	cols.push(new nlobjSearchColumn('custrecord_wtka_orderid'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_inbound_request'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_request_counter'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_inventorytransfers'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_final_response'));
	
	var searchRecord = nlapiSearchRecord('customrecord_wtka_external_orders', null, filters, cols);
	if(searchRecord != null)
	{
		for(var i=0; i < searchRecord.length; i++)
		{
			var recordOrderId = searchRecord[i].getValue('custrecord_wtka_orderid');
			var recordFileId  = searchRecord[i].getValue('custrecord_wtka_inbound_request');
			var recordCounter = searchRecord[i].getValue('custrecord_wtka_request_counter');
			var recordId	  = searchRecord[i].getId();
			var recordResp	  = searchRecord[i].getValue('custrecord_wtka_final_response');
			var recordRequest = fetchRequestFromFile(recordFileId, recordCounter);
			var recordTrnfs	  = searchRecord[i].getValue('custrecord_wtka_inventorytransfers');
			if(recordRequest != 0)
			{
				nlapiLogExecution('DEBUG', 'Initiating Inventory Transfers RESTLET');
				//var response = nlapiRequestURL(InvTrfURL, JSON.stringify(recordRequest), headers);
				
				//APPLE V. 2016/10/17 : Operational Issue : Call common function for triggering Restlet
				var response = callRestlet(InvTrfURL, JSON.stringify(recordRequest), recordOrderId, 'Inventory Transfer');								
				nlapiLogExecution('DEBUG', 'RESPONSE', response.code);

				if(response.code == 200)
				{
					nlapiLogExecution('DEBUG', 'body', response.body);
					
					/* COMMENTED OUT : APPLE V. 2016/10/17 : Operational Issue : Moved 200 Response processing to IT_003_RL script
					var fields 	  = new Array();
					var values    = new Array();
					var respBody  = JSON.parse(response.body); //respBody example: {"records":[{"status":"Success","transactionType":"Inventory Transfer","transactionId":"3429731","transactionNumber":"3082"}],"orderNumber":"00017097_chris13","status":"Success"}
					if(respBody.status == "Success")
					{
						var ordStatus 	= 2;
						var recResp 	= new Object();
						nlapiLogExecution('DEBUG', 'recordResp', recordResp);
						if(recordResp != null && recordResp != '')
						{
							recResp 		= JSON.parse(recordResp);
							nlapiLogExecution('DEBUG', 'recResp', JSON.stringify(recResp));
						}
						else
						{
							recResp.records = new Array();
						}
						recResp.status 	= 'Success';
						for(var r in respBody.records)		recResp.records.push(respBody.records[r]);
						
						var resp = JSON.stringify(recResp);
							
						//Fields to update on the WTKA_External_Order record					
						fields.push('custrecord_wtka_final_response');
						fields.push('custrecord_wtka_inventorytransfers');
						fields.push('custrecord_wtka_order_status');
						
						var invTrfVal    = JSON.parse(response.body);
						var invTrfValues = (recordTrnfs != null) ? recordTrnfs : "";

						nlapiLogExecution('DEBUG',  'invTrfVal', JSON.stringify(invTrfVal));
						nlapiLogExecution('DEBUG',  'invTrfVal.records', JSON.stringify(invTrfVal.records));
						for(var j in invTrfVal.records)
						{
							//put together string for field: INVENTORY TRANSFERS
							if(invTrfValues == ""){
								invTrfValues = invTrfVal.records[j].transactionId;
							} else {
								invTrfValues += ',' + invTrfVal.records[j].transactionId;
							} 	
						}
						
						//set values into the fields.
						values.push(resp);
						values.push(invTrfValues);						
						values.push(ordStatus); //Inventory Transfer Processed
						
						nlapiSubmitField('customrecord_wtka_external_orders', recordId, fields, values, false);	

						linkTransactionsToExternalWTKA(nlapiGetRecordId()); //Call to WTKA_Library.js
						
						if(recordTrnfs == null || recordTrnfs == "" || (recordTrnfs.length != invTrfValues.length))
						{
							var ediStat = InvokeCloudhub('salesorder', 0, recordOrderId, 'Released', 'inventoryFlow'); //call to WTKA_Library.js
							if(ediStat != 'T')
							{
								// Log error response from ESB 
								nlapiLogExecution('debug', 'ediStat', ediStat);
								var fields 	  = new Array();
								var values    = new Array();
								var ordStatus 	= 6; //Error
								if (recResp != null && recResp != '')
								{
									var respObj 	= new Object();
									respObj 		= "940 ESB call post Inventory Reservation failed for order " + recordOrderId;
									recResp.status 	= "Error";
									recResp.records.push(respObj);
								}
								var resp = JSON.stringify(recResp);
														
								fields.push('custrecord_wtka_final_response');
								fields.push('custrecord_wtka_order_status');
								
								values.push(resp);
								values.push(ordStatus); //Error
								
								nlapiSubmitField('customrecord_wtka_external_orders', recordId, fields, values, false);	
							}
						}
					}
					else
					{
						if(respBody.status == "Duplicate")
						{
							var ordStatus 	= 2; //Reset Status
							fields.push('custrecord_wtka_order_status');
							values.push(ordStatus); //Inventory Transfer Processed
							nlapiLogExecution('debug', 'Duplicate Inventory Transfer Request');
							nlapiSubmitField('customrecord_wtka_external_orders', recordId, fields, values, false);	
						}
						else
						{
							var ordStatus 	= 6; //Error
							var recResp 	= new Object();
							if (recordResp != null && recordResp != '')
							{
								recResp 		= JSON.parse(recordResp);
								recResp.status 	= respBody.status;
								recResp.records.push(respBody);
							}
							else
							{
								recResp = respBody;
							}
							var resp = JSON.stringify(recResp);
													
							fields.push('custrecord_wtka_final_response');
							fields.push('custrecord_wtka_order_status');
							
							values.push(resp);
							values.push(ordStatus); //Error
							
							nlapiSubmitField('customrecord_wtka_external_orders', recordId, fields, values, false);	
						}
					}
					*/
				}
				else
				{
					//APPLE V. 2016/10/19 : Operational Issue : Add checking for response.body to prevent exception
					if(response.body){	
						var message = '<br><b>Error Details: </b><br>' + JSON.stringify(response.body);
						sendOrderMail('Inventory', message, dataIn);
						
						/* Log error response from Restlet */
						var fields 	  = new Array();
						var values    = new Array();
						var respBody  = JSON.parse(response.body);
						var ordStatus 	= 6; //Error
						var recResp 	= new Object();
						if (recordResp != null && recordResp != '')
						{
							recResp 		= JSON.parse(recordResp);
							recResp.status 	= respBody ? respBody.status : '';
							recResp.records.push(respBody);
						}
						else
						{
							recResp = respBody;
						}
						var resp = JSON.stringify(recResp);
												
						fields.push('custrecord_wtka_final_response');
						fields.push('custrecord_wtka_order_status');
						
						values.push(resp);
						values.push(ordStatus); //Error
						
						nlapiSubmitField('customrecord_wtka_external_orders', recordId, fields, values, false);	
					}
				}
			}
		}
	}
	else
	{
		nlapiLogExecution('DEBUG', 'No records to process');
	}
}

function decideFlow(recID)
{
	var custRecord 	  = nlapiLoadRecord('customrecord_wtka_external_orders', recID);
	var resp 		  = custRecord.getFieldValue('custrecord_wtka_final_response');

	var tranId = custRecord.getFieldValue('custrecord_wtka_orderid');

	nlapiLogExecution('DEBUG', 'resp', resp);

	if(resp != null)	finalResponse = JSON.parse(resp);
	var completedFlag = custRecord.getFieldValue('custrecord_wtka_completed_request');
	
	try
	{
		var status = custRecord.getFieldValue('custrecord_wtka_order_status');
		nlapiLogExecution('debug', 'status', custRecord.getFieldText('custrecord_wtka_order_status'));
		if(status == 1)	checkInventoryTransfer(recID); // Inventory Reservation
		if(status == 3) // Completed Order
		{
			/* Trigger RESTlet */
			var orderRequest = new Object();
			orderRequest.id  = nlapiGetRecordId();
			
			//var restletResponse = nlapiRequestURL(OrderURL, JSON.stringify(orderRequest), headers);

			//APPLE V. 2016/10/17 : Operational Issue : Call common function for triggering Restlet
			nlapiLogExecution('DEBUG', 'Initiating Sales Order RESTLET');
			var restletResponse = callRestlet(OrderURL, JSON.stringify(orderRequest), tranId, 'Sales Order');
			nlapiLogExecution('DEBUG', 'RESPONSE', restletResponse.code);
			
			if(restletResponse.code == 200)
			{
				/* COMMENTED OUT : APPLE V. 2016/10/18 : Operational Issue : Moved 200 Response processing to SO_003_RL script
				var fields 	  = new Array();
				var values    = new Array();
				var respBody  = JSON.parse(restletResponse.body);

				if(respBody.status != "Success")
				{
					setRecordFieldValues(recID, null, null, null, null, null, null, null, false); //Reset completed flag
				} else {

					nlapiLogExecution('DEBUG', 'Setting Order Logs', 'testing');
					nlapiLogExecution('DEBUG', 'recordId', nlapiGetRecordId());

					var orderDetails = nlapiGetFieldValue('custrecord_wtka_processed_ids');
					nlapiLogExecution('DEBUG', 'orderDetails', orderDetails);

					var custrecord_wtka_inventorytransfers = nlapiGetFieldValue('custrecord_wtka_inventorytransfers');
					nlapiLogExecution('DEBUG', 'custrecord_wtka_inventorytransfers', custrecord_wtka_inventorytransfers);

					linkTransactionsToExternalWTKA(nlapiGetRecordId()); //Call to WTKA_Library.js
				}
				*/
			}
			else
			{
				//APPLE V. 2016/10/19 : Operational Issue : Add checking for restletResponse.body to prevent exception
				if(restletResponse.body){				
					var message = '<br><b>Error Details: </b><br>' + JSON.stringify(restletResponse.body);
					sendOrderMail('Order', message, dataIn);
				}
			}
		}
		if(status == 6) //Send Error Email
		{
			var InboundOrderNumber = custRecord.getFieldValue('custrecord_wtka_orderid');
			var message = '<br><b>Error Details: </b><br>' + JSON.stringify(finalResponse);
			message += 'Process against <b>' + InboundOrderNumber + '</b> failed in NetSuite';
			sendOrderMail('Order', message, dataIn);
			createTransactionLog(null, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, null, SYNC_STATUS.ERROR, JSON.stringify(finalResponse), null, 22); //Library call to WTKA_Library.js
		}
	}
	catch(err_create)
	{
		/* COMMENTED OUT : APPLE V. 2016/10/18 : Operational Issue
		// SSS_REQUEST_TIME_EXCEEDED will be caught by the catch block in callRestlet function
		if(err_create.code == 'SSS_REQUEST_TIME_EXCEEDED') //re-trigger script
		{
			nlapiLogExecution('debug', 'Retriggering...');
			var orderRequest 	= new Object();
			orderRequest.id  	= nlapiGetRecordId();
			var restletResponse = nlapiRequestURL(OrderURL, JSON.stringify(orderRequest), headers);
			nlapiLogExecution('debug', 'Retriggered RESTLET', restletResponse.getBody());
			var errorMessage = 'SSS_REQUEST_TIME_EXCEEDED - Retriggered RESTLET';
			createTransactionLog(null, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 12, SYNC_STATUS.ERROR, errorMessage, null, 23); //Library call to WTKA_Library.js //Record Type 12 is Inventory Transfer
		}
		else
		{*/
			var errObject 		  	= new Object();
			errObject.status 	 	= "Exception";
			errObject.message 	 	= "Order cannot be processed. Error:" + err_create;
			finalResponse.status 	= "Error";
			finalResponse.records.push(errObject);
			completedFlag = (completedFlag == 'T') ? false : null;
			setRecordFieldValues(recID, null, finalResponse, null, 6, null, null, null, completedFlag);
			nlapiLogExecution('debug', 'UE-catch', JSON.stringify(errObject));
			createTransactionLog(null, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, null, SYNC_STATUS.ERROR, JSON.stringify(errObject), null, 24); //Library call to WTKA_Library.js
		//}	
	}
}

//Triggers a Restlet
var INT_RESTLET_CALL_COUNT_MAX = 5;
function callRestlet(stUrl, stPostData, tranId, stTranType, intRsCallCount){
	
	var response = {};
	
	try{
		nlapiLogExecution('debug', 'Call RESTLET Url', stUrl);
		nlapiLogExecution('debug', 'Call RESTLET Post data', stPostData);
      
		response = nlapiRequestURL(stUrl, stPostData, headers);
		
	} catch(error){
		
		intRsCallCount = (intRsCallCount ? intRsCallCount : 0);
		
		nlapiLogExecution('debug', 'Call RESTLET error', error.toString());
		nlapiLogExecution('debug', 'Call RESTLET error.code', error.code);
		nlapiLogExecution('debug', 'Call Restlet call count', intRsCallCount + 1);
		
		//Do not retrigger if error = SSS_REQUEST_TIME_EXCEEDED as it only means the Restlet is still processing
		//Just log the error
		var intTranType = null;
		if(stTranType == 'Inventory Transfer'){
			intTranType = 12;
		} else if(stTranType == 'Sales Order'){
			intTranType = 31;
		}
		createTransactionLog(null, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, intTranType, SYNC_STATUS.ERROR, error.toString(), null, 701); //Library call to WTKA_Library.js
		
		//But if error = SSS_CONNECTION_CLOSED, retrigger the Restlet
		if(intRsCallCount < INT_RESTLET_CALL_COUNT_MAX && error.code == 'SSS_CONNECTION_CLOSED'){
			
			var stRetriggerMsg = 'SSS_CONNECTION_CLOSED - Retriggering Restlet';
			nlapiLogExecution('debug', 'callRestlet', stRetriggerMsg);
			
			//Logging
			createTransactionLog(null, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, intTranType, SYNC_STATUS.ERROR, stRetriggerMsg, null, 702); //Library call to WTKA_Library.js
			
			//Retrigger
			response = callRestlet(stUrl, stPostData, tranId, stTranType, intRsCallCount + 1);		
		}
	}
	
	return response;
}