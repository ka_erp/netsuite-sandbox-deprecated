/**
 *	File Name		:	WTKA_MulesofttoNetSuite.js
 *	Function		:	Sales Orders(945, 214), Intercompany Orders(940, 945), Transfer Orders(940, 945)
 * 	Authors			:	Rini Thomas, Vinod Kumar
 *	Company			:	Wipro Limited
 *	Release Dates	:	20-Jan-2016 (v1.0), 27-Jan-2016 (v1.1), 5-May-2016 (v2.0)
 * 	Current Version	:	2.0
**/

{
	var totalquantity 		= 0, 	totalQuantityReceived   = 0;
	var OrderId 			= 0, 	OrderNumber 			= 0;
	var ItemFulfillrecord 	= 0, 	InboundOrderId 			= 0;
	var invalid_orderid 	= 'F', 	DeliveryDate	  		= '';	
	
	var ErrorObj 			= new Object();
	ErrorObj.status 	 	= "Error";
	ErrorObj.editype 	 	= 0;
	ErrorObj.orderid 		= 0;
	ErrorObj.messages 	 	= new Array();
	
	var sendEmail 			= true; //false;
	
	var FinalShipArray, FinalOrderIdArray, 	 FinalShipQty , 	 FinalUPCCodes, 	InboundOrderNumber, 	ShipmentStatusCode;
	var shipmentLines, 	itemfulfillmentflag, finalTransformType, transformID, 		greturnreferencenumber, gtrackingnumber, 	Trantype945;
	var ccList = null; //['']; //Enter CC Addresses as array
	var toList = ['3PL_eComm_Integration@kitandace.com'];
}
 
function getRESTlet(dataIn) 
{
	return '{"Success" : "Call received"}';
}
 
function postRESTlet945(dataIn) // 945 inbound call for ecomm orders
{
	nlapiLogExecution('DEBUG', 'DATAIN', JSON.stringify(dataIn));
	Trantype945 			= 'salesorder';
	var InBoundData 		= dataIn;
	var PreValidationChecks = Validate945InBoundData(dataIn);
	if(PreValidationChecks)
	{
		var tranformProcess = FinalTransformProcess(dataIn);
		nlapiLogExecution('DEBUG', 'tranformProcess', JSON.stringify(tranformProcess));
		return tranformProcess;
	}
	else
	{
		var errorProcess = ErrorProcess(dataIn);
		return ErrorObj;
	}
}

function Validate945InBoundData(dataIn) //validations for 945 JSON REQUESTS
{
	ErrorObj.EdiType 	 	= 945;
	ErrorObj.orderid 	  	= 0;
	ErrorObj.ordernumber  	= 0;	
	ErrorObj.shipmentdate 	= '';
	ErrorObj.messages 	 	= new Array();
	ErrorObj.messages[0] 	= new Object();
	
	//1. Check if dataIn is empty
	if(!emptyInbound(dataIn)) return false; 
	
	//2. Check if dataIn has header level objects values set
	if(!emptyObject(dataIn.shipmentconfirmation.orderid, 	  "Orderid")) 		return false; 
	if(!emptyObject(dataIn.shipmentconfirmation.shipmentdate, "Shipmentdate"))  return false; 
	
	ErrorObj.orderid 	  = dataIn.shipmentconfirmation.orderid;
	ErrorObj.ordernumber  = dataIn.shipmentconfirmation.ordernumber;	
	ErrorObj.shipmentdate = dataIn.shipmentconfirmation.shipmentdate;
	
	//3. Check if dataIn has line items
	if(!containLines(dataIn.shipmentconfirmation.items, "items present")) 		return false; 
	
	//4. Check if dataIn has line item references
	for(var i in dataIn.shipmentconfirmation.items)
	{
		if(!containLines(dataIn.shipmentconfirmation.items[i].references, "item references exist")) return false; 
	}
	
	//5. Check if references has Tracking Number and Original Request Number
	var shipmentLines   = new Object();
	shipmentLines.items = new Array();
	
	for(var i in dataIn.shipmentconfirmation.items)
	{
		shipmentLines.items[i] 					= new Object();
		shipmentLines.items[i].itemcode			= dataIn.shipmentconfirmation.items[i].itemcode;
		shipmentLines.items[i].quantityordered  = dataIn.shipmentconfirmation.items[i].quantityordered;
		shipmentLines.items[i].quantityshipped  = dataIn.shipmentconfirmation.items[i].quantityshipped;
		shipmentLines.items[i].references 		= new Array();
		
		var ReturnReqRefNumber, TrackingNumber; 
		for(var j in dataIn.shipmentconfirmation.items[i].references)
		{
			shipmentLines.items[i].references[j] 					= new Object();
			var Qualifier 											= dataIn.shipmentconfirmation.items[i].references[j].referencequalifier;
			shipmentLines.items[i].references[j].referencequalifier = Qualifier;
			if(!emptyObject(Qualifier, "Reference Qualifier")) 		return false; 
			
			switch(Qualifier)
			{
				case 'OD':
					ReturnReqRefNumber 										= dataIn.shipmentconfirmation.items[i].references[j].referencenumber;
					shipmentLines.items[i].references[j].referencenumber 	= ReturnReqRefNumber;
					greturnreferencenumber 									= ReturnReqRefNumber;
					if(!emptyObject(ReturnReqRefNumber, "Return Reference Request Number")) 		return false; 
					break;
					
				case '2I':
					TrackingNumber 											= dataIn.shipmentconfirmation.items[i].references[j].referencenumber;
					shipmentLines.items[i].references[j].referencenumber 	= TrackingNumber;
					gtrackingnumber 										= TrackingNumber;
					if(!emptyObject(TrackingNumber, 	"Tracking Number")) 		return false; 
					break;
				
				default:
					break;
			}
		}
	}
	
	//6. Check if dataIn has Quantity Ordered == Quantity Shipped
	for(var i in dataIn.shipmentconfirmation.items)
	{
		var QtyOrdered = parseInt(dataIn.shipmentconfirmation.items[i].quantityordered);
		var QtyShipped = parseInt(dataIn.shipmentconfirmation.items[i].quantityshipped);
		if(QtyOrdered != QtyShipped)
		{
			ErrorObj.messages[0].messagetype = "Quantity Mismatch";
			ErrorObj.messages[0].message = "Order cannot be processed as quantity ordered does not match quantity shipped";
			return false;
		}
	}
	
	//7. Check if salesorder is Valid and in Pending Fulfillment status
	try
	{
		var record 			= nlapiLoadRecord('salesorder', dataIn.shipmentconfirmation.orderid);
		var recordStatus 	= record.getFieldValue('statusRef');
		var recterms 		= record.getFieldValue('terms');
		var recpaymeth 		= record.getFieldValue('paymentmethod');
		var salesItemCount 	= record.getLineItemCount('item');	

		if((recterms != null && recterms.length > 0) || (recpaymeth != null && recpaymeth.length > 0))		finalTransformType = 'cashsale';
		else	finalTransformType = 'invoice';

		if(recordStatus == 'pendingBilling' || recordStatus == 'cancelled' || recordStatus == 'fullyBilled' || recordStatus == 'closed' || recordStatus == 'partiallyFulfilled' || recordStatus == 'pendingBillingPartFulfilled')		
		{
			ErrorObj.messages[0].messagetype = "Invalid order";
			ErrorObj.messages[0].message = "Order cannot be fulfilled as it is already in " + recordStatus + " status.";
			return false;
		}

		//Check if fulfillment can be processed
		try
		{
			var itemFulfillment = nlapiTransformRecord('salesorder', dataIn.shipmentconfirmation.orderid, 'itemfulfillment');
			var itemFulfillItemCount = itemFulfillment.getLineItemCount('item');
			var inventoryItemFulfillmentCount = 0;

			for (var i=1; i<=salesItemCount; i++)
			{
				itemType = record.getLineItemValue('item','itemtype',i);
				nlapiLogExecution('DEBUG','Item Type', itemType);
				if(itemType != 'GiftCert')
				{
					inventoryItemFulfillmentCount += 1;
				}
			}

			if(itemFulfillItemCount == 0)
			{
				ErrorObj.messages[0].messagetype = "Item Fulfillment Failure";
				ErrorObj.messages[0].message = "Order cannot be processed as items are backordered and there are no items available to fulfill";
				return false;
			}
			else
			{
				nlapiLogExecution('DEBUG','Compare amounts', 'inventoryItemFulfillmentCount = ' + inventoryItemFulfillmentCount + ' itemFulfillItemCount is ' + itemFulfillItemCount);
				if(inventoryItemFulfillmentCount != itemFulfillItemCount)
				{
					ErrorObj.messages[0].messagetype = "Item Fulfillment Failure";
					ErrorObj.messages[0].message = "Order cannot be processed as item count on fulfillment process does not match item count on salesorder";
					return false;
				}
			}
		}
		catch(err1)
		{
            nlapiLogExecution('DEBUG', 'UNEXPECTED ERROR',  err1);

			ErrorObj.status = "Exception";
			ErrorObj.messages[0].messagetype = "Item Fulfillment Failure";
			ErrorObj.messages[0].message = "Order cannot be fulfilled as items are backordered and there are no items available to fulfill.";
			return false;
		}
	}
	catch(err)
	{
		return (invalidOrder());
	}
	return true;
}

function postRestlet214(dataIn) //214 inbound calls for ecomm orders
{
	nlapiLogExecution('DEBUG', 'DATAIN', JSON.stringify(dataIn));
	var PreValidationChecks = Validate214InBoundData(dataIn);
	if(PreValidationChecks)
	{ 
		if(ShipmentStatusCode == 'D1' && ShipmentStatusReasonCode == 'NS')
		{
			var DlvyDate 	= new Date (DeliveryDate);
			DlvyDate 		= nlapiDateToString(DlvyDate, 'date');
			
			var finalmessage 			= new Object();
			finalmessage.status 		= "Success";
			finalmessage.orderid 		= OrderId;
			finalmessage.ordernumber 	= OrderNumber;
			finalmessage.deliverydate 	= DeliveryDate;
			finalmessage.records 		= new Array();
			
			try
			{
				var ItemFullFilmentid = nlapiLoadRecord('itemfulfillment', ItemFulfillrecord);
				ItemFullFilmentid.setFieldValue('custbody_ka_shipment_delivery_date', DlvyDate);
				var recordId = nlapiSubmitRecord(ItemFullFilmentid, true);

				finalmessage.records[0] 				= new Object();
				finalmessage.records[0].transactiontype = "Item Fulfillment";
				finalmessage.records[0].transactionid 	= recordId;
				
				var createLogs = LogCreation(1, 3, OrderId, recordId, dataIn, 1, finalmessage, 'T'); //0 - Outbound Call, 1 - Inbound call
					
				nlapiLogExecution('DEBUG', 'Final Message', JSON.stringify(finalmessage));
				return finalmessage;
			}
			catch(err)
			{
				var i = (itemfulfillmentflag == 'T') ? 1 : 0;
				finalmessage.records[i] 		= new Object();
				finalmessage.records[i].message = 'Item Fulfillment record could not be created. Details as below.'; 
				finalmessage.records[i].details = err.getCode() + ' : ' + err.getDetails();
				
				var subject = 'Inbound 214 call processing failure';
				var body = 'Hello,<br><br>';
				body += 'Delivery Date update against order <b>' + OrderNumber + '</b> failed in NetSuite due to below error.<br>';
				body += '<br><b>Error Details: </b><br>' + err.getCode() + ' : ' + err.getDetails() ;
				body += '<br><br>Process needs to be resubmitted or handled manually through user interface after corrections.';
				body += '<br><br><br><br><br><br><br>Thanks';
				body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
				
				nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
				if(sendEmail)	nlapiSendEmail(243423, toList, subject, body, ccList);
				return finalmessage;
			}
		}
		else
		{
			var finalmessage 			= new Object();
			finalmessage.status 		= "Success";
			finalmessage.orderid 		= OrderId;
			finalmessage.ordernumber 	= OrderNumber;
			finalmessage.records 		= new Array();
			finalmessage.records[0] 	= new Object();
			finalmessage.records[0].message = "Delivery date could not be updated against Item Fulfillment record for order - " + OrderNumber
			finalmessage.records[0].details = "ShipmentStatus is '" + ShipmentStatusCode + "' and ShipmentStatusReasonCode is '" + ShipmentStatusReasonCode + "'";
		
			nlapiLogExecution('DEBUG', 'Final Message', JSON.stringify(finalmessage));
			return finalmessage;
		}
	}
	else
	{
		var errorStatus = (ErrorObj.status == "Error") ? 2 : 3; // 2- Error, 3-Exception
		if(invalid_orderid == 'F')	var ErrorLogs = LogCreation(1, 3, OrderId, 0, dataIn, errorStatus, ErrorObj, 'F'); //0 - Outbound Call, 1 - Inbound call
		nlapiLogExecution('DEBUG', 'Error Object', JSON.stringify(ErrorObj));
		return ErrorObj;
	}
}

function Validate214InBoundData(dataIn) //validations for 214 JSON REQUESTS
{
	//Pre-Validation checks on DataIn
	ErrorObj.EdiType 	 	= 214;
	ErrorObj.messages 	 	= new Array();
	ErrorObj.messages[0] 	= new Object();
	
	//1. Check if dataIn is empty
	
	if(!emptyInbound(dataIn))	return false;

	//2. Check if dataIn has deliveryconfirmations object
	if(!emptyObject(dataIn.deliveryconfirmations, 	"deliveryconfirmations"))	return false;
	if(!containLines(dataIn.deliveryconfirmations, 	"data present"))			return false;
	
	//3. Check if dataIn has references, datetimes and shipmentstatus object
	for(var i in dataIn.deliveryconfirmations)
	{
		if(!containLines(dataIn.deliveryconfirmations[i].references, "References available"))					return false;
		
		//3.1 Check if dataIn has datetimes object
		if(!containLines(dataIn.deliveryconfirmations[i].datetimes, "Datetime object available"))				return false;
		
		//3.2 Check if dataIn has shipmentstatus object
		if(!containLines(dataIn.deliveryconfirmations[i].shipmentstatus, "Shipmentstatus object available"))	return false;
	}

	//4. Check if references has referencequalifier and referencenumber
	for(var dc in dataIn.deliveryconfirmations)
	{ 
		for(var rr in dataIn.deliveryconfirmations[dc].references)
		{
			var Qualifier = dataIn.deliveryconfirmations[dc].references[rr].referencequalifier;
			if(!emptyObject(Qualifier, "Reference Qualifier"))	return false;

			switch(Qualifier)
			{
				case 'PO':
					OrderId = dataIn.deliveryconfirmations[dc].references[rr].referencenumber;
					if(!emptyObject(OrderId, "Orderid Reference"))	return false;
					break;
					
				case 'DE':
					OrderNumber = dataIn.deliveryconfirmations[dc].references[rr].referencenumber;
					if(!emptyObject(OrderNumber, "OrderNumber Reference"))	return false;
					break;
				
				default:
					break;
			}
		}

		//4.1 Check if datetimes object has deliverydate
		for(var dt in dataIn.deliveryconfirmations[dc].datetimes)
		{
			DeliveryDate = dataIn.deliveryconfirmations[dc].datetimes[dt].deliverydate;
			if(!emptyObject(DeliveryDate, "DeliveryDate"))	return false;
		}

		//4.2 Check if shipmentstatus object has deliverydate
		for(var ss in dataIn.deliveryconfirmations[dc].shipmentstatus)
		{
			ShipmentStatusCode = dataIn.deliveryconfirmations[dc].shipmentstatus[ss].shipmentstatuscode;
			if(!emptyObject(ShipmentStatusCode, "ShipmentStatusCode"))	return false;

			ShipmentStatusReasonCode = dataIn.deliveryconfirmations[dc].shipmentstatus[ss].shipmentstatusreasoncode;
			if(!emptyObject(ShipmentStatusReasonCode, "ShipmentStatusReasonCode"))	return false;
		}
	}
	
	//5. Check if orderid is Valid
	try
	{
		ItemFulfillrecord = 0;
		var record = nlapiLoadRecord('salesorder', OrderId);

		for(var i=0; i < record.getLineItemCount('links'); i++)
		{
			if(record.getLineItemValue('links', 'type', i+1) == 'Item Fulfillment')		ItemFulfillrecord = record.getLineItemValue('links','id', i+1);
		}
		
		if(ItemFulfillrecord == 0)
		{
			ErrorObj.messages[0].messagetype 	= "Invalid Order";
			ErrorObj.messages[0].message 		= "Delivery date cannot be updated as no Item Fulfillment record(s) exist against order " + OrderNumber;
			return false;
		}
	}
	catch(err)
	{
		return (invalidOrder());
	}
	return true;
}

function getSalesOrderObject_940(dataIn) // REST API Inbound call for generating SO Canonical data object model
{
	nlapiLogExecution('DEBUG', 'dataIn', JSON.stringify(dataIn));
	var type 			= 'salesorder';
	InboundOrderNumber 	= dataIn.id;
	InboundOrderId 		= FetchOrderId(type, InboundOrderNumber);
	var status 			= Validate940(type, InboundOrderId); 
	if(status)
	{
		var record 	 	= nlapiLoadRecord(type, InboundOrderId);
		var CanonObject = generateSOCanonObject(record, InboundOrderNumber);
		nlapiLogExecution('DEBUG', 'Object', JSON.stringify(CanonObject));
		var salesorderLogs 	= LogCreation(2, 1, InboundOrderId, 0, dataIn, 1, CanonObject, 'T'); //0 - Outbound Call, 1 - Inbound call
		return CanonObject;
	}
	else
	{
		var errorStatus = (ErrorObj.status == "Error") ? 2 : 3; // 2- Error, 3-Exception
		if(invalid_orderid == 'F')
		{
			var orderid 	= (InboundOrderId != null) ? InboundOrderId : 0;
			var ErrorLogs 	= LogCreation(2, 1, orderid, 0, dataIn, errorStatus, ErrorObj, 'F'); //0 - Outbound Call, 1 - Inbound call
			nlapiLogExecution('DEBUG', 'Error Object', JSON.stringify(ErrorObj));
		}
		var subject = 'Outbound 940 call processing failure';
		var body 	= 'Hello,<br><br>';
		if(invalid_orderid == 'T')	body += 'Process failed in NetSuite. <br><b>Data Received from Mulesoft: </b>><br>';
		else	body += 'Process against <b>' + InboundOrderNumber + '</b> failed in NetSuite. <br><b>Data Received from Mulesoft: </b><br>';
		body += '<br>' + JSON.stringify(dataIn) + '<br><br><br>';
		body += '<b>NetSuite Response: </b><br>';
		body += JSON.stringify(ErrorObj);
		body += '<br><br><br><br><br><br><br>Thanks';
		body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
		nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
		if(sendEmail)	nlapiSendEmail(243423, toList, subject, body, ccList);
		nlapiLogExecution('DEBUG', 'ErrorObject', JSON.stringify(ErrorObj)); 
		return ErrorObj;
	}
}

function Validate940(type, OrderId) // Validations routines to check inbound request from REST API
{
	//Pre-Validation checks on DataIn
	ErrorObj.editype 	 	= 940;
	ErrorObj.orderid 		= OrderId;
	ErrorObj.messages[0] 	= new Object();

	//Check if Order is Valid and in Pending Fulfillment status
	try
	{
		var record = nlapiLoadRecord(type, OrderId);
		for(var i=0; i < record.getLineItemCount('item'); i++)
		{
			totalquantity 		  += parseInt(record.getLineItemValue('item',   'quantity',			  i+1));
			totalQuantityReceived += parseInt(record.getLineItemValue('item',   'quantityreceived',   i+1));
		}
	}
	catch(err)
	{
        nlapiLogExecution('DEBUG','error message',err.message);
		return (invalidOrder());
	}
	return true;
}

function generateSOCanonObject(record, InboundOrderNumber) //Generation of Canonical data object model for SO
{
	var salesOrderSchema = new Object();
	try
	{
		var recordId 	= record.getId();
		var customerId 	= record.getFieldValue('entity');

		var carriercode = (record.getFieldValue('shipcarrier') 	!= null) ? 	record.getFieldValue('shipcarrier') : record.getFieldValue('carrier');
                if (carriercode == 'nonups') { carriercode = 'UPS' };
		var carriername = record.getFieldText('shipmethod');
		carriername 	= (carriername.match(/FedEx/gi) != null) ? 'FedEx' : (carriername.match(/UPS/gi) != null) ? 'UPS' : 'FedEx'; //Default to FedEx if Ship method is not set

		var fromLocationData  	= (record.getFieldValue('location') != null) ? getLocationData(record.getFieldValue('location')) : "";
		var customerData 	  	= getCustomerData(customerId);
		var shipData 	      	= getShippingData(record);
		var billData          	= getBillingData(record);

		var interco				= record.getFieldValue('intercotransaction');
		var icto				= (interco != null && interco.length > 0) 	? true : false;
		var locale 			  	= (readValue(fromLocationData.country) != "") ? readValue(fromLocationData.country).toLowerCase() : "";
		var customerEmail	  	= record.getFieldValue('email');
		
		if(!icto)
		{
			var paymentid 			= "";
			var authUrl 			= "";
			var paymentmethod 		= "";
			for(var k=0; k < record.getLineItemCount('paymentevent'); k++)
			{
				paymentid = (paymentid == "") ? record.getLineItemValue('paymentevent', 'nseqnum', 	k+1) : paymentid + ', ' + record.getLineItemValue('paymentevent', 'nseqnum', 	k+1);
				authUrl = record.getLineItemValue('paymentevent', 'viewurl', 	k+1);
			}

			if(readValue(record.getFieldValue('paymentmethod')) != "")
			{
				paymentmethod = record.getFieldText('paymentmethod');
				if(Math.abs(record.getFieldValue('giftcertapplied')) > 0)
				{
					paymentmethod += '/Gift Card';
				}
			}
			else if(Math.abs(record.getFieldValue('giftcertapplied')) > 0)
			{
				paymentmethod = 'Gift Card';
			}
		}
		
		var ordStatus = record.getFieldValue('status');
		var orderStatus;
		switch(ordStatus)
		{
			case 'Pending Fulfillment':
				orderStatus = "Created";
				break;
			case 'Pending Billing':
				orderStatus = "Shipped";
				break;
			case 'Billed':
				orderStatus = "Completed";
				break;
			case 'Partially Fulfilled':
				orderStatus = "Released";
				break;
			default:
				orderStatus = "";
		}
		
		salesOrderSchema.order									= new Object();
		salesOrderSchema.order.orderHeader 						= new Object();
		salesOrderSchema.order.orderHeader.orderId 				= readValue(record.getFieldValue('tranid'));
		salesOrderSchema.order.orderHeader.parentOrderNumber 	= "";
		salesOrderSchema.order.orderHeader.childOrderNumber 	= "";
		salesOrderSchema.order.orderHeader.childSourceSystem 	= (icto) ? "Netsuite" 	: "Hybris";
		salesOrderSchema.order.orderHeader.orderTransactionDt 	= readValue(record.getFieldValue('trandate'));
		salesOrderSchema.order.orderHeader.orderStatus 			= (icto) ? orderStatus 	: "Completed";
		salesOrderSchema.order.orderHeader.orderClosedDt 		= "";
		salesOrderSchema.order.orderHeader.orderCreateTs 		= readValue(record.getFieldValue('createddate'));
		salesOrderSchema.order.orderHeader.orderLastModifiedTs 	= readValue(record.getFieldValue('lastmodifieddate'));
		salesOrderSchema.order.orderHeader.transactionType 		= "Transfer Order"; 
		salesOrderSchema.order.orderHeader.exchangeRate 		= parseFloat(record.getFieldValue('exchangerate'));
		salesOrderSchema.order.orderHeader.isGiftOrder 			= false;
		salesOrderSchema.order.orderHeader.giftMessage 			= "";
		salesOrderSchema.order.orderHeader.orderHeaderMemo 		= readValue(record.getFieldValue('memo'));
		salesOrderSchema.order.orderHeader.paymentStatus 		= "Pending";
		salesOrderSchema.order.orderHeader.isFulfilled 			= false;
		salesOrderSchema.order.orderHeader.totalQuantityOrdered = totalquantity;
		salesOrderSchema.order.orderHeader.totalAmountOrdered 	= parseFloat(record.getFieldValue('total'));
		
		salesOrderSchema.order.orderHeader.sellingLocation 				= new Object();
		salesOrderSchema.order.orderHeader.sellingLocation.locationCode = readValue(fromLocationData.externalid);
		salesOrderSchema.order.orderHeader.sellingLocation.locationName = readValue(fromLocationData.name);
		salesOrderSchema.order.orderHeader.sellingLocation.addressLine1 = readValue(fromLocationData.address1); 
		salesOrderSchema.order.orderHeader.sellingLocation.addressLine2 = readValue(fromLocationData.address2); 
		salesOrderSchema.order.orderHeader.sellingLocation.city 		= readValue(fromLocationData.city);
		salesOrderSchema.order.orderHeader.sellingLocation.province 	= readValue(fromLocationData.state);
		salesOrderSchema.order.orderHeader.sellingLocation.country 		= readValue(fromLocationData.country);
		salesOrderSchema.order.orderHeader.sellingLocation.mailingCode 	= readValue(fromLocationData.zip);
		salesOrderSchema.order.orderHeader.sellingLocation.phoneNumber 	= readValue(fromLocationData.addrphone); 
		salesOrderSchema.order.orderHeader.sellingLocation.latitude 	= 0.00000; 
		salesOrderSchema.order.orderHeader.sellingLocation.longitude 	= 0.00000; 
		salesOrderSchema.order.orderHeader.sellingLocation.openingDate 	= readValue(fromLocationData.custrecord_ka_store_opening_date);
		salesOrderSchema.order.orderHeader.sellingLocation.closedDate 	= "";
		salesOrderSchema.order.orderHeader.sellingLocation.locationType = readValue(fromLocationData.custrecord_ra_loctype);
		salesOrderSchema.order.orderHeader.sellingLocation.storeHours 	= "";
		salesOrderSchema.order.orderHeader.sellingLocation.locStatus 	= (fromLocationData.isinactive == 'F') ? 'Open' : 'Closed'; 
		salesOrderSchema.order.orderHeader.sellingLocation.DCLocCode 	= fromLocationData.externalid;

		salesOrderSchema.order.orderHeader.billingCustomer 					  	= new Object();
		salesOrderSchema.order.orderHeader.billingCustomer.customerId 		  	= customerId;
		salesOrderSchema.order.orderHeader.billingCustomer.customerEmail 	  	= readValue(customerEmail);
		salesOrderSchema.order.orderHeader.billingCustomer.honorific 		  	= readValue(customerData.salutation);
		salesOrderSchema.order.orderHeader.billingCustomer.firstName 		  	= readValue(customerData.firstname);
		salesOrderSchema.order.orderHeader.billingCustomer.lastName 			= readValue(customerData.lastname);
		salesOrderSchema.order.orderHeader.billingCustomer.customerPhoneNumber 	= readValue(customerData.phone);
		salesOrderSchema.order.orderHeader.billingCustomer.gender 				= "";
		salesOrderSchema.order.orderHeader.billingCustomer.birthDt 				= "";
		salesOrderSchema.order.orderHeader.billingCustomer.locale 				= locale;
		
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress 					 	= new Object();
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.billingAddressId 	 	= record.getFieldValue('billingaddress_key');
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.billingFirstName	 	= readValue(customerData.firstname);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.billingLastName 	 	= readValue(customerData.lastname);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.billingEmail 		 	= readValue(customerEmail);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.billingAltEmail 	 	= "";
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.billingAddressLine1 	= readValue(billData.billaddr1);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.billingAddressLine2 	= readValue(billData.billaddr2);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.poBox 				= "";
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.cityName				= readValue(billData.billcity);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.postalCode 		 	= readValue(billData.billzip);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.provinceName 		 	= readValue(billData.billstate);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.provinceAltName 	 	= "";
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.countryName 		 	= readValue(billData.billcountry);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.countryAltName 	 	= "";
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.inActiveDate 		 	= "";
		
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress 						= new Object();
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.shippingAddressId 	= record.getFieldValue('shippingaddress_key');
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.recipientFirstName 	= readValue(customerData.firstname);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.recipientLastName 	= readValue(customerData.lastname);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.recipientEmail 		= readValue(customerEmail);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.recipientAltEmail 	= "";
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.recipientPhoneNumber = readValue(shipData.shipphone);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.shippingAddressLine1 = readValue(shipData.shipaddr1);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.shippingAddressLine2 = readValue(shipData.shipaddr2);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.poBox 				= ""
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.cityName 			= readValue(shipData.shipcity);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.postalCode 			= readValue(shipData.shipzip);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.provinceName 		= readValue(shipData.shipstate);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.provinceAltName 		= "";
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.countryName 			= readValue(shipData.shipcountry);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.countryAltName 		= "";
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.inActiveDate 		= "";
		
		if(!icto)
		{
			salesOrderSchema.order.orderHeader.paymentInfo 				= new Object();
			salesOrderSchema.order.orderHeader.paymentInfo.paymentId 	= (paymentid != "") 	? paymentid 	: null;
			salesOrderSchema.order.orderHeader.paymentInfo.tenderType 	= (paymentmethod != "") ? paymentmethod : "";
			salesOrderSchema.order.orderHeader.paymentInfo.currencyCode = readValue(record.getFieldValue('currencyname'));
			salesOrderSchema.order.orderHeader.paymentInfo.authUrl 		= (authUrl != "") 		? authUrl 		: null;
		}
		else
		{
			salesOrderSchema.order.orderHeader.paymentInfo 				= new Object();
			salesOrderSchema.order.orderHeader.paymentInfo.paymentId 	= null;
			salesOrderSchema.order.orderHeader.paymentInfo.tenderType 	= "Promise To Pay";
			salesOrderSchema.order.orderHeader.paymentInfo.currencyCode = readValue(record.getFieldValue('currencyname'));
			salesOrderSchema.order.orderHeader.paymentInfo.authUrl 		= null;
		}
		salesOrderSchema.order.orderDetail = new Array();
		var i=0;
		var totalItemsShipped = 0;
		for(var j=0; j<record.getLineItemCount('item'); j++)
		{
			var itemType = record.getLineItemValue('item', 'itemtype', i+1);
			if(itemType == 'InvtPart' || itemType == 'NonInvtPart')
			{
				var amount  	= parseFloat(record.getLineItemValue('item', 'amount', 	i+1));
				var dualTax 	= false;
				var TaxRate1 	= record.getLineItemValue('item', 'taxrate1', i+1);
				TaxRate1 		= (TaxRate1 != null && TaxRate1.indexOf('%') != -1) ? TaxRate1.substring(0, TaxRate1.indexOf('%'))/100 : 0.0;
				var TaxRate2 	= record.getLineItemValue('item', 'taxrate2', i+1);
				if(TaxRate2 != null)	dualTax = true;
				TaxRate2 		= (TaxRate2 != null && TaxRate2.indexOf('%') != -1) ? TaxRate2.substring(0, TaxRate2.indexOf('%'))/100 : 0.0;
				
				newOrderDetail 						= new Object();
				newOrderDetail.orderId 				= recordId;
				newOrderDetail.orderLineNumber 		= record.getLineItemValue('item', 'id', i+1);
				newOrderDetail.countItemOrdered 	= parseInt(record.getLineItemValue('item', 	'quantity', i+1));
				newOrderDetail.amtExtended 			= amount;
				newOrderDetail.amtExtendedCost 		= 0.00; 
				newOrderDetail.orderDetailMemo 		= "";
				newOrderDetail.isTaxable 			= (TaxRate1 > 0 || TaxRate2 > 0) ? true : false;
			
				newOrderDetail.item 				= new Object();
				newOrderDetail.item.SKUId 			= record.getLineItemValue('item', 'custcol_wtka_upccode', 	i+1);
				newOrderDetail.item.SKUCode 		= record.getLineItemValue('item', 'item_display', 			i+1);
				newOrderDetail.item.SKUName 		= record.getLineItemValue('item', 'description', 			i+1);
		
				newOrderDetail.amtItemPrice							= new Object();
				newOrderDetail.amtItemPrice.priceId 				= "";
				newOrderDetail.amtItemPrice.currencyCode 			= readValue(record.getFieldValue('currencyname'));
				newOrderDetail.amtItemPrice.amtCurrentPrice 		= parseFloat(record.getLineItemValue('item', 'rate', i+1));
				newOrderDetail.amtItemPrice.amtPromoPrice 			= parseFloat(record.getLineItemValue('item', 'rate', i+1));
				newOrderDetail.amtItemPrice.priceEffectiveStartDt 	= null;
				newOrderDetail.amtItemPrice.priceEffectiveEndDt 	= null;
				newOrderDetail.amtItemPrice.lastModifiedByUser 		= "";
				newOrderDetail.amtItemPrice.lastModifiedTimestamp 	= "";
				
				newOrderDetail.tax 					= new Array();
				newOrderDetail.tax[0] 				= new Object();
				newOrderDetail.tax[0].taxId 		= record.getLineItemValue('item', 'taxcode', 	 		 i+1);
				newOrderDetail.tax[0].taxName 		= record.getLineItemValue('item', 'taxcode_display', 	 i+1);
				newOrderDetail.tax[0].taxAltName 	= "";
				newOrderDetail.tax[0].taxRate 		= parseFloat(record.getLineItemValue('item', 'taxrate1', i+1));
				newOrderDetail.tax[0].taxCity 		= "";
				newOrderDetail.tax[0].taxCounty 	= "";
				newOrderDetail.tax[0].taxState 		= "";
				newOrderDetail.tax[0].taxCountry 	= "";
				newOrderDetail.tax[0].amtTaxApplied	= TaxRate1 * amount;
				
				if(dualTax)
				{
					newOrderDetail.tax[1] 				= new Object();
					newOrderDetail.tax[1].taxId 		= record.getLineItemValue('item', 'taxcode', 	 		 i+1);
					newOrderDetail.tax[1].taxName 		= record.getLineItemValue('item', 'taxcode_display', 	 i+1);
					newOrderDetail.tax[1].taxAltName 	= "";
					newOrderDetail.tax[1].taxRate 		= parseFloat(record.getLineItemValue('item', 'taxrate2', i+1));
					newOrderDetail.tax[1].taxCity 		= "";
					newOrderDetail.tax[1].taxCounty 	= "";
					newOrderDetail.tax[1].taxState 		= "";
					newOrderDetail.tax[1].taxCountry 	= "";
					newOrderDetail.tax[1].amtTaxApplied	= TaxRate2 * amount;
				}
				
				salesOrderSchema.order.orderDetail.push(newOrderDetail);
				totalItemsShipped += parseInt(record.getLineItemValue('item', 'quantityfulfilled', i+1));
			}
			i++;
		}
		
		salesOrderSchema.order.shipment			 										= new Array();
		salesOrderSchema.order.shipment[0]												= new Object();
		salesOrderSchema.order.shipment[0].shipmentHeader 								= new Object();
		salesOrderSchema.order.shipment[0].shipmentHeader.shipmentId 					= readValue(record.getFieldValue('tranid'));
		salesOrderSchema.order.shipment[0].shipmentHeader.shipmentCode 					= carriername;
		salesOrderSchema.order.shipment[0].shipmentHeader.shippingMethod 				= carriercode;
		salesOrderSchema.order.shipment[0].shipmentHeader.trackingNumber 				= "";
		//salesOrderSchema.order.shipment[0].shipmentHeader.RMA 							= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.shipmentStatus 				= "CL";
		salesOrderSchema.order.shipment[0].shipmentHeader.countTotalShipped 			= totalItemsShipped;
		salesOrderSchema.order.shipment[0].shipmentHeader.countTotalReceivedInShipment 	= parseInt(record.getLineItemValue('item', 'quantityreceived', i+1));
		if(!salesOrderSchema.order.shipment[0].shipmentHeader.countTotalReceivedInShipment)
		{
			salesOrderSchema.order.shipment[0].shipmentHeader.countTotalReceivedInShipment = 0;
		}
		salesOrderSchema.order.shipment[0].shipmentHeader.shipDt 						= readValue(record.getFieldValue('shipdate'));
		salesOrderSchema.order.shipment[0].shipmentHeader.shipReceivedDt 				= readValue(record.getFieldValue('custbody_ka_to_expected_recv_date'));  
		salesOrderSchema.order.shipment[0].shipmentHeader.expectedShipDate 				= readValue(record.getFieldValue('custbody_ka_to_expected_ship_date'));
		salesOrderSchema.order.shipment[0].shipmentHeader.handlingInstructions 			= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.carrierName 					= carriername;
		
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient 					= new Object();
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.customerId 			= customerId;
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.customerEmail 		= readValue(customerEmail);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.honorific 			= readValue(customerData.salutation);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.firstName 			= readValue(customerData.firstname);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.lastName 			= readValue(customerData.lastname);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.customerPhoneNumber = readValue(customerData.phone);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.gender 				= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.birthDt 			= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.locale 				= locale;
		
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress 						= new Object();
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.billingAddressId 	= record.getFieldValue('billingaddress_key');
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.billingFirstName	 	= readValue(customerData.firstname);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.billingLastName 	 	= readValue(customerData.lastname);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.billingEmail 		= readValue(customerEmail);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.billingAltEmail 		= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.billingAddressLine1 	= readValue(billData.billaddr1);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.billingAddressLine2	= readValue(billData.billaddr2);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.poBox 				= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.cityName				= readValue(billData.billcity);			
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.postalCode 		 	= readValue(billData.billzip);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.provinceName 		= readValue(billData.billstate);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.provinceAltName 	 	= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.countryName 		 	= readValue(billData.billcountry);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.countryAltName 	 	= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.inActiveDate 		= "";

		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress 						= new Object();
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.shippingAddressId 		= record.getFieldValue('shippingaddress_key');
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.recipientFirstName		= readValue(customerData.firstname);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.recipientLastName 		= readValue(customerData.lastname);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.recipientEmail 			= readValue(customerEmail);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.recipientAltEmail 	 	= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.recipientPhoneNumber 	= readValue(shipData.shipphone);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.shippingAddressLine1 	= readValue(shipData.shipaddr1);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.shippingAddressLine2 	= readValue(shipData.shipaddr2);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.poBox 				 	= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.cityName 		 		= readValue(shipData.shipcity);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.postalCode 		 		= readValue(shipData.shipzip);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.provinceName 	 		= readValue(shipData.shipstate);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.provinceAltName 		= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.countryName 		 	= readValue(shipData.shipcountry);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.countryAltName 	 		= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.inActiveDate 	 		= "";

		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation				= new Object();
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.locationCode 	= readValue(fromLocationData.externalid); 
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.locationName 	= readValue(fromLocationData.name);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.addressLine1 	= readValue(fromLocationData.address1);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.addressLine2 	= readValue(fromLocationData.address2);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.city 			= readValue(fromLocationData.city);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.province 		= readValue(fromLocationData.state);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.country 		= readValue(fromLocationData.country);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.mailingCode 	= readValue(fromLocationData.zip);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.phoneNumber 	= readValue(fromLocationData.addrphone);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.latitude 		= 0.00000; 
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.longitude 		= 0.00000; 
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.openingDate 	= readValue(fromLocationData.custrecord_ka_store_opening_date);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.closedDate 	= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.locationType 	= readValue(fromLocationData.custrecord_ra_loctype);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.storeHours 	= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.locStatus 		= (fromLocationData.isinactive == 'F') ? 'Open' : 'Closed';
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.DCLocCode 		= fromLocationData.externalid;

		salesOrderSchema.order.shipment[0].shipmentDetail 	= new Array();		
		for(var i=0; i < record.getLineItemCount('item'); i++)
		{
			var itemType = record.getLineItemValue('item', 'itemtype', i+1);
			if(itemType == 'InvtPart' || itemType == 'NonInvtPart')
			{
				newDetail 					= new Object();
				newDetail.orderDetailId 	= readValue(record.getLineItemValue('item', 'id', 					i+1));
				newDetail.quantityOrdered 	= parseInt(record.getLineItemValue('item', 	'quantity',				i+1));
				newDetail.quantityShipped 	= parseInt(record.getLineItemValue('item', 	'quantityfulfilled', 	i+1));
				salesOrderSchema.order.shipment[0].shipmentDetail.push(newDetail);
			}
		}
	}
	catch(so_error)
	{
        nlapiLogExecution('DEBUG','error message',so_error.message);
		ErrorObj.editype 	 				= 940;
		ErrorObj.orderid 					= record.getId();
		ErrorObj.messages[0] 				= new Object();
		ErrorObj.messages[0].messagetype 	= "Sales Order Error";
		ErrorObj.messages[0].message 	 	= "Error Details: " + so_error.message;
		salesOrderSchema 					= ErrorObj;
		var subject = 'Outbound 940 call processing failure';
		var body 	= 'Hello,<br><br>';
		body += 'Process against <b>' + InboundOrderNumber + '</b> failed in NetSuite.';
		body += '<b>NetSuite Response: </b><br>';
		body += JSON.stringify(ErrorObj);
		body += '<br><br><br><br><br><br><br>Thanks';
		body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
		nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
		if(sendEmail)	nlapiSendEmail(243423, toList, subject, body, ccList);
	}
	return salesOrderSchema;
}

function getTransferOrderObject_940(dataIn)  // REST API Inbound call for generating TO Canonical data object model
{
	nlapiLogExecution('DEBUG', 'dataIn', JSON.stringify(dataIn));
	var type 			= 'transferOrder';
	InboundOrderNumber  = dataIn.id;
	InboundOrderId 		= FetchOrderId(type, InboundOrderNumber);
	var status 			= Validate940(type, InboundOrderId); 
	if(status)
	{
		var record 	 	= nlapiLoadRecord(type, InboundOrderId);
		var CanonObject = generateTOCanonObject(record, InboundOrderNumber);
		nlapiLogExecution('DEBUG', 'Canonical Object', JSON.stringify(CanonObject)); 
		var transferOrderLogs 	= LogCreation(2, 1, InboundOrderId, 0, dataIn, 1, CanonObject, 'T'); //0 - Outbound Call, 1 - Inbound call
		return CanonObject;
	}
	else
	{
		var errorStatus = (ErrorObj.status == "Error") ? 2 : 3; // 2- Error, 3-Exception
		if(invalid_orderid == 'F')
		{
			var orderid 	= (InboundOrderId != null) ? InboundOrderId : 0;
			var ErrorLogs 	= LogCreation(2, 1, orderid, 0, dataIn, errorStatus, ErrorObj, 'F'); //0 - Outbound Call, 1 - Inbound call
			nlapiLogExecution('DEBUG', 'Error Object', JSON.stringify(ErrorObj));
		}
		var subject = 'Outbound 940 call processing failure';
		var body = 'Hello,<br><br>';
		if(invalid_orderid == 'T')	body += 'Process failed in NetSuite. <br><b>Data Received from Mulesoft: </b><br>';
		else	body += 'Process against <b>' + InboundOrderNumber + '</b> failed in NetSuite. <br><b>Data Received from Mulesoft: </b><br>';
		body += '<br>' + JSON.stringify(dataIn) + '<br><br><br>';
		body += '<b>NetSuite Response: </b><br>';
		body += JSON.stringify(ErrorObj);
		body += '<br><br><br><br><br><br><br>Thanks';
		body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
		nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
		if(sendEmail)	nlapiSendEmail(243423, toList, subject, body, ccList);
		return ErrorObj;
	}
}

function generateTOCanonObject(record, InboundOrderNumber) //Generation of Canonical data object model for TO
{
	var transferOrderSchema = new Object();
	try
	{
		var recordId 	= record.getId();
		var carriercode = (record.getFieldValue('shipcarrier') 	!= null) ? 	record.getFieldValue('shipcarrier') : record.getFieldValue('carrier');
		var carriername = record.getFieldText('shipmethod');
		carriername 	= (carriername.match(/FedEx/gi) != null) ? 'FedEx' : (carriername.match(/UPS/gi) != null) ? 'UPS' : 'FedEx'; //Default to FedEx if Ship method is not set
		
		var fromLocationData  	= getLocationData(record.getFieldValue('location'));
		var toLocationData  	= getLocationData(record.getFieldValue('transferlocation'));
				
		transferOrderSchema.transferOrder 		  									= new Object();
		transferOrderSchema.transferOrder.transferHeader  							= new Object();
		transferOrderSchema.transferOrder.transferHeader.transferId 				= readValue(record.getFieldValue('tranid'));
		transferOrderSchema.transferOrder.transferHeader.transferNumber 			= "";
		transferOrderSchema.transferOrder.transferHeader.fromLocation 		  		= new Object();
		transferOrderSchema.transferOrder.transferHeader.fromLocation.locationCode 	= readValue(fromLocationData.externalid); 
		transferOrderSchema.transferOrder.transferHeader.fromLocation.locationName 	= readValue(fromLocationData.name);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.addressLine1 	= readValue(fromLocationData.address1);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.addressLine2 	= readValue(fromLocationData.address2);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.city 			= readValue(fromLocationData.city);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.province 		= readValue(fromLocationData.state);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.country 		= readValue(fromLocationData.country);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.mailingCode 	= readValue(fromLocationData.zip);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.phoneNumber 	= readValue(fromLocationData.addrphone); 
		transferOrderSchema.transferOrder.transferHeader.fromLocation.latitude 		= 0.00000; 
		transferOrderSchema.transferOrder.transferHeader.fromLocation.longitude 	= 0.00000; 
		transferOrderSchema.transferOrder.transferHeader.fromLocation.openingDate 	= readValue(fromLocationData.custrecord_ka_store_opening_date);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.closedDate 	= "";
		transferOrderSchema.transferOrder.transferHeader.fromLocation.locationType 	= readValue(fromLocationData.custrecord_ra_loctype);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.storeHours 	= "";
		transferOrderSchema.transferOrder.transferHeader.fromLocation.locStatus 	= (fromLocationData.isinactive == 'F') ? 'Open' : 'Closed';
		transferOrderSchema.transferOrder.transferHeader.fromLocation.DCLocCode 	= readValue(fromLocationData.externalid); 
		
		transferOrderSchema.transferOrder.transferHeader.toLocation 		  		= new Object();
		transferOrderSchema.transferOrder.transferHeader.toLocation.locationCode 	= readValue(toLocationData.externalid); 
		transferOrderSchema.transferOrder.transferHeader.toLocation.locationName 	= readValue(toLocationData.name);
		transferOrderSchema.transferOrder.transferHeader.toLocation.addressLine1 	= readValue(toLocationData.address1);
		transferOrderSchema.transferOrder.transferHeader.toLocation.addressLine2 	= readValue(toLocationData.address2);
		transferOrderSchema.transferOrder.transferHeader.toLocation.city 			= readValue(toLocationData.city);
		transferOrderSchema.transferOrder.transferHeader.toLocation.province 		= readValue(toLocationData.state);
		transferOrderSchema.transferOrder.transferHeader.toLocation.country 		= readValue(toLocationData.country);
		transferOrderSchema.transferOrder.transferHeader.toLocation.mailingCode 	= readValue(toLocationData.zip);
		transferOrderSchema.transferOrder.transferHeader.toLocation.phoneNumber 	= readValue(toLocationData.addrphone);
		transferOrderSchema.transferOrder.transferHeader.toLocation.latitude 		= 0.00000;
		transferOrderSchema.transferOrder.transferHeader.toLocation.longitude 		= 0.00000;
		transferOrderSchema.transferOrder.transferHeader.toLocation.openingDate 	= readValue(toLocationData.custrecord_ka_store_opening_date);
		transferOrderSchema.transferOrder.transferHeader.toLocation.closedDate 		= "";
		transferOrderSchema.transferOrder.transferHeader.toLocation.locationType 	= readValue(toLocationData.custrecord_ra_loctype);
		transferOrderSchema.transferOrder.transferHeader.toLocation.storeHours 		= "";
		transferOrderSchema.transferOrder.transferHeader.toLocation.locStatus 		= (toLocationData.isinactive == 'F') ? 'Open' : 'Closed';
		transferOrderSchema.transferOrder.transferHeader.toLocation.DCLocCode 		= readValue(toLocationData.externalid); 
		
		transferOrderSchema.transferOrder.transferHeader.transactionDt 		  	= readValue(record.getFieldValue('trandate'));
		transferOrderSchema.transferOrder.transferHeader.transferStatus 	 	= readValue(record.getFieldValue('status'));
		transferOrderSchema.transferOrder.transferHeader.transferClosedDt 	 	= "";
		transferOrderSchema.transferOrder.transferHeader.createTS 			  	= readValue(record.getFieldValue('createddate'));
		transferOrderSchema.transferOrder.transferHeader.lastModifiedTs 	  	= readValue(record.getFieldValue('lastmodifieddate'));
		transferOrderSchema.transferOrder.transferHeader.transferType 		  	= record.getFieldText('custbody_ka_order_type');
		transferOrderSchema.transferOrder.transferHeader.transferHeaderMemo   	= readValue(record.getFieldValue('memo'));
		transferOrderSchema.transferOrder.transferHeader.totalQuantityOrdered 	= parseInt(totalquantity);
		transferOrderSchema.transferOrder.transferHeader.totalAmountOrdered 	= parseFloat(record.getFieldValue('total')); 
		
		transferOrderSchema.transferOrder.transferDetail  = new Array();

		var totalItemsShipped = 0;
		var j=0;
		for(var i=0; i<record.getLineItemCount('item'); i++)
		{
			var itemType = record.getLineItemValue('item', 'itemtype', i+1);
			if(itemType == 'InvtPart' || itemType == 'NonInvtPart')
			{
				transferOrderSchema.transferOrder.transferDetail[j] 					= new Object();
				transferOrderSchema.transferOrder.transferDetail[j].transferId 			= readValue(recordId);
				transferOrderSchema.transferOrder.transferDetail[j].transferLineNumber	= record.getLineItemValue('item', 'id', 				  i+1);
				
				transferOrderSchema.transferOrder.transferDetail[j].item 			  	= new Object();
				transferOrderSchema.transferOrder.transferDetail[j].item.SKUId 			= record.getLineItemValue('item', 'custcol_wtka_upccode', 	i+1);
				transferOrderSchema.transferOrder.transferDetail[j].item.SKUCode   		= record.getLineItemValue('item', 'item_display', 			i+1);
				transferOrderSchema.transferOrder.transferDetail[j].item.SKUName   		= record.getLineItemValue('item', 'description', 			i+1);
				
				transferOrderSchema.transferOrder.transferDetail[j].countItemOrdered 		= parseInt(record.getLineItemValue('item', 'quantity', i+1));

				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice 						= new Object();
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.priceId 				= "";
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.currencyCode 			= readValue(record.getFieldValue('currencyname'));
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.amtCurrentPrice 		= parseFloat(record.getLineItemValue('item', 'rate', i+1));
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.amtPromoPrice 			= parseFloat(record.getLineItemValue('item', 'rate', i+1));
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.priceEffectiveStartDt 	= null;
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.priceEffectiveEndDt 	= null;
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.lastModifiedByUser 	= "";
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.lastModifiedTimestamp 	= "";
				transferOrderSchema.transferOrder.transferDetail[j].amtExtended 						= null; 
				transferOrderSchema.transferOrder.transferDetail[j].amtExtendedCost						= null; 
				transferOrderSchema.transferOrder.transferDetail[j].transferDetailMemo 					= "";
				totalItemsShipped += parseInt(record.getLineItemValue('item', 'quantityfulfilled',  i+1));
				j++;
			}
		}
		
		transferOrderSchema.transferOrder.shipment = new Array();
		transferOrderSchema.transferOrder.shipment[0]			 									= new Object();
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader 								= new Object();
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.shipmentId 					= readValue(recordId);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.shipmentCode 					= carriername;
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.shippingMethod 				= carriercode;
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.trackingNumber 				= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.shipmentStatus 				= "CL";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.countTotalShipped 				= totalItemsShipped;
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.countTotalReceivedInShipment 	= parseInt(record.getLineItemValue('item', 'quantityreceived',  i+1));
		if(!transferOrderSchema.transferOrder.shipment[0].shipmentHeader.countTotalReceivedInShipment)
		{
			transferOrderSchema.transferOrder.shipment[0].shipmentHeader.countTotalReceivedInShipment = 0;
		}
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.shipDt 						= readValue(record.getFieldValue('shipdate'));
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.shipReceivedDt 				= readValue(record.getFieldValue('custbody_ka_to_expected_recv_date')); 
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.expectedShipDate 				= readValue(record.getFieldValue('custbody_ka_to_expected_ship_date'));
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.handlingInstructions 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.carrierName 					= carriername;

		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient 						= new Object();
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.customerId 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.customerEmail 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.honorific 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.firstName 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.lastName 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.customerPhoneNumber  = "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.gender 				= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.birthDt 				= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.locale 				= "";
			
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress 							= new Object();
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.billingAddressId 			= ""; //readValue(fromLocationData.externalid); 
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.billingFirstName	 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.billingLastName 	 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.billingEmail 				= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.billingAltEmail 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.billingAddressLine1 		= ""; //readValue(fromLocationData.address1);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.billingAddressLine2		= ""; //readValue(fromLocationData.address2);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.poBox 					= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.postalCode 		 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.provinceName 				= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.provinceAltName 	 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.countryName 		 		= ""; //readValue(fromLocationData.country);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.countryAltName 	 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.inActiveDate 				= "";

		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress 							= new Object();
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.shippingAddressId 		= ""; //readValue(toLocationData.externalid); 
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.recipientFirstName		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.recipientLastName 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.recipientEmail 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.recipientAltEmail 	 	= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.recipientPhoneNumber 	= ""; //readValue(toLocationData.addrphone);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.shippingAddressLine1 	= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.shippingAddressLine2 	= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.poBox 				 	= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.cityName 		 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.postalCode 		 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.provinceName 	 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.provinceAltName 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.countryName 		 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.countryAltName 	 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.inActiveDate 	 		= "";

		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation					= new Object();
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.locationCode 	= readValue(fromLocationData.externalid); 
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.locationName 	= readValue(fromLocationData.name);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.addressLine1 	= readValue(fromLocationData.address1);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.addressLine2 	= readValue(fromLocationData.address2);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.city 			= readValue(fromLocationData.city);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.province 		= readValue(fromLocationData.state);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.country 		= readValue(fromLocationData.country);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.mailingCode 	= readValue(fromLocationData.zip);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.phoneNumber 	= readValue(fromLocationData.addrphone);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.latitude 		= 0.00000;
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.longitude 		= 0.00000;
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.openingDate 	= readValue(fromLocationData.custrecord_ka_store_opening_date);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.closedDate 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.locationType 	= readValue(fromLocationData.custrecord_ra_loctype);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.storeHours 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.locStatus 		= (fromLocationData.isinactive == 'F') ? 'Open' : 'Closed';
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.DCLocCode 		= readValue(fromLocationData.externalid);
		transferOrderSchema.transferOrder.shipment[0].shipmentDetail 									= new Array();		

		for(var i=0; i < record.getLineItemCount('item'); i++)
		{
			var itemType = record.getLineItemValue('item', 'itemtype', i+1);
			if(itemType == 'InvtPart' || itemType == 'NonInvtPart')
			{
				newDetail 					= new Object();
				newDetail.orderDetailId 	= readValue(record.getLineItemValue('item', 'id', 				 i+1));
				newDetail.quantityOrdered 	= parseInt(record.getLineItemValue('item', 	'quantity',			 i+1));
				newDetail.quantityShipped 	= parseInt(record.getLineItemValue('item', 	'quantityfulfilled', i+1));
				transferOrderSchema.transferOrder.shipment[0].shipmentDetail.push(newDetail);
			}
		}
	}
	catch(to_error)
	{
		nlapiLogExecution('DEBUG','error message',so_error.message);
		ErrorObj.editype 	 				= 940;
		ErrorObj.orderid 					= record.getId();
		ErrorObj.messages[0] 				= new Object();
		ErrorObj.messages[0].messagetype 	= "Transfer Order Error";
		ErrorObj.messages[0].message 	 	= "Error Details: " + to_error.message;
		transferOrderSchema 				= ErrorObj;
		var subject = 'Outbound 940 call processing failure';
		var body 	= 'Hello,<br><br>';
		body += 'Process against <b>' + InboundOrderNumber + '</b> failed in NetSuite.';
		body += '<b>NetSuite Response: </b><br>';
		body += JSON.stringify(ErrorObj);
		body += '<br><br><br><br><br><br><br>Thanks';
		body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
		nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
		if(sendEmail)	nlapiSendEmail(243423, toList, subject, body, ccList);
	}
	return transferOrderSchema;
}

function Validate945InBoundDataTOSO(dataIn) // Validation routines for 945 process checks for SO & TO
{
	//Pre-Validation checks on DataIn
	ErrorObj.editype 	  	= 945;
	ErrorObj.orderid 	  	= 0;
	ErrorObj.ordernumber  	= 0;	
	ErrorObj.shipmentdate 	= '';
	ErrorObj.messages 	 	= new Array();
	ErrorObj.messages[0] 	= new Object();
	FinalShipArray 			= new Array();
	FinalOrderIdArray 		= new Array();
	FinalShipQty 			= new Array();
	FinalUPCCodes 			= new Array();
	var update_flag			= 'F';
	var shipmentStatus 		= '';
	
	try
	{
		//1. Check if dataIn is empty
		if(!emptyInbound(dataIn)) 	return false; 
		
		if(Trantype945 == 'salesorder_b2b')
		{
			//2. Check if dataIn has header level objects values set
			if(!emptyObject(dataIn.order, 									"order"))			return false; 
			if(!emptyObject(dataIn.order.orderHeader, 						"orderHeader"))		return false; 
			if(!emptyObject(dataIn.order.orderDetail, 						"orderDetail"))		return false; 
			if(!emptyObject(dataIn.order.shipment, 							"shipment"))		return false; 
			
			transformID 		  = FetchOrderId('salesorder', dataIn.order.orderHeader.orderId);
			ErrorObj.orderid 	  = transformID;
			ErrorObj.ordernumber  = dataIn.order.orderHeader.orderId;	
			
			if(!emptyObject(dataIn.order.shipment[0].shipmentHeader.shipDt, "shipDt"))			return false;
			
			ErrorObj.shipmentdate = dataIn.order.shipment[0].shipmentHeader.shipDt;
			
			Trantype945			  = (dataIn.order.orderHeader.childSourceSystem == 'Hybris') ? 'salesorder' : 'salesorder_b2b';
			shipmentStatus = dataIn.order.shipment[0].shipmentHeader.shipmentStatus;
			if(Trantype945 == 'salesorder_b2b' && (shipmentStatus != 'CL' && shipmentStatus != 'PR'))	update_flag = 'SO';	 // Check for Updates
			
			//2.1 Check for Location based fulfillment
			if(Trantype945 == 'salesorder_b2b')
			{
				var POId 				= nlapiLookupField('salesorder', 	transformID, 'intercotransaction');
				var locationid 			= nlapiLookupField('purchaseorder', POId, 		 'location'); 
				var LocationsIds 		= nlapiGetContext().getSetting('SCRIPT', 'custscript_945_so_locations');
				var AssignedLocation 	= (LocationsIds != null && LocationsIds.length > 0) ? LocationsIds.split(',') : -1;
				
				if(AssignedLocation != -1 && AssignedLocation.indexOf(locationid) == -1)
				{
					ErrorObj.messages[0].messagetype = "LocationId not configured";
					ErrorObj.messages[0].message 	 = "Order cannot be processed as Fulfillment Location " + locationid + " is not mapped for processing. Please contact Administrator";
					return false;
				}
			}
			
			if(update_flag == 'F')
			{
				//3. Check if dataIn has line level objects
				if(!containLines(dataIn.order.orderDetail, 		"orderDetail items present"))  	return false; 
				if(!containLines(dataIn.order.shipment, 		"shipment items present")) 		return false; 
			
				//4. Check if dataIn has LineNumber,UPCNumber and SKUId available for orderDetail and transferDetail object
				for(var i in dataIn.order.orderDetail)
				{
					if(!emptyObject(dataIn.order.orderDetail[i].orderLineNumber, 	"TransferLineNumber")) 	return false; 
					if(!emptyObject(dataIn.order.orderDetail[i].item.SKUId, 		"SKUId")) 			return false; 
				}
				
				//5. Check if dataIn has orderDetailId, quantityOrdered and quantityShipped in shipment.shipmentDetail object
				var LineDetails = dataIn.order.shipment[0];
				for (var j=0; j < LineDetails.shipmentDetail.length; j++)
				{
					var orderDetailId   = LineDetails.shipmentDetail[j].orderDetailId;
					if(!emptyObject(orderDetailId, 	 "OrderDetailId")) 	 return false; 
					FinalShipArray[FinalShipArray.length] 		= orderDetailId;

					var quantityOrdered = LineDetails.shipmentDetail[j].quantityOrdered;
					if(!emptyObject(quantityOrdered, "QuantityOrdered")) return false; 
					FinalOrderIdArray[FinalOrderIdArray.length] = quantityOrdered;
					
					var quantityShipped = LineDetails.shipmentDetail[j].quantityShipped;
					if(!emptyObject(quantityShipped, "QuantityShipped")) return false; 
					FinalShipQty[FinalShipQty.length] 			= quantityShipped;
					
					//Check if dataIn has Quantity Ordered == Quantity Shipped
					if(Trantype945 == 'salesorder' && quantityOrdered != quantityShipped)
					{
						ErrorObj.messages[0].messagetype = "Quantity Mismatch";
						ErrorObj.messages[0].message 	 = "Order cannot be processed as quantity ordered does not match quantity shipped";
						return false;
					}
				}
				
				//6. Fetch UPCCode from orderDetail object for final processing - in case of partial shipments
				for(var j=0; FinalShipArray != null && j < FinalShipArray.length; j++)
				{
					for(var i in dataIn.order.orderDetail)
					{
						if(dataIn.order.orderDetail[i].orderLineNumber === FinalShipArray[j])
						{
							FinalUPCCodes[FinalUPCCodes.length] = dataIn.order.orderDetail[i].item.SKUId;
						}
					}
				}
			}
		}
		else // Transfer Order Checks
		{
			//2. Check if dataIn has header level objects values set
			if(!emptyObject(dataIn.transferOrder, 					"transfer")) 			return false;  
			if(!emptyObject(dataIn.transferOrder.transferHeader, 	"transferHeader"))		return false; 
			if(!emptyObject(dataIn.transferOrder.transferDetail, 	"transferDetail"))		return false; 
			if(!emptyObject(dataIn.transferOrder.shipment, 			"shipment"))			return false; 
			
			shipmentStatus = dataIn.transferOrder.shipment[0].shipmentHeader.shipmentStatus;
			if(shipmentStatus != 'CL' && shipmentStatus != 'PR')	update_flag = 'TO';		 // Check for Updates
			transformID 		  = FetchOrderId('transferOrder', dataIn.transferOrder.transferHeader.transferId);
			ErrorObj.orderid 	  = transformID;
			ErrorObj.ordernumber  = dataIn.transferOrder.transferHeader.transferId;	
		
			//2.1 Check for Location based fulfillment
			var locationValue 		= FetchIdFromName('location', dataIn.transferOrder.transferHeader.toLocation.locationCode);
			if(locationValue[0] == 'Error')	return false;
			var locationid			= locationValue[0];
			var LocationsIds 		= nlapiGetContext().getSetting('SCRIPT', 'custscript_945_to_locations');
			var AssignedLocation 	= (LocationsIds != null && LocationsIds.length > 0) ? LocationsIds.split(',') : -1;
			
			if(AssignedLocation != -1 && AssignedLocation.indexOf(locationid) == -1)
			{
				ErrorObj.messages[0].messagetype = "LocationId not configured";
				ErrorObj.messages[0].message = "Order cannot be processed as Fulfillment Location " + locationid + " is not mapped for processing. Please contact Administrator";
				return false;
			}
		
			if(update_flag == 'F')
			{
				//3. Check if dataIn has line level objects
				if(!containLines(dataIn.transferOrder.transferDetail, 	"transferOrderdetails items present")) 	return false;  
				if(!containLines(dataIn.transferOrder.shipment, 		"shipment items present"))  			return false;  
				
				ErrorObj.shipmentdate = dataIn.transferOrder.shipment[0].shipmentHeader.shipDt;
				
				//4. Check if dataIn has LineNumber,UPCNumber and SKUId available for orderDetail and transferDetail object
				for(var i in dataIn.transferOrder.transferDetail)
				{
					if(!emptyObject(dataIn.transferOrder.transferDetail[i].transferLineNumber, "TransferLineNumber")) 	return false; 
					if(!emptyObject(dataIn.transferOrder.transferDetail[i].item.SKUId, 			"SKUId")) 				return false; 
				}
				
				//5. Check if dataIn has orderDetailId, quantityOrdered and quantityShipped in shipment.shipmentDetail object
				var LineDetails = dataIn.transferOrder.shipment[0];
				
				for(var j=0; j < LineDetails.shipmentDetail.length; j++)
				{
					var orderDetailId = LineDetails.shipmentDetail[j].orderDetailId;
					if(!emptyObject(orderDetailId, 	 "OrderDetailId")) 	 return false; 
					FinalShipArray[FinalShipArray.length] = orderDetailId;
				
					var quantityOrdered = LineDetails.shipmentDetail[j].quantityOrdered;
					if(!emptyObject(quantityOrdered, "QuantityOrdered")) return false; 
					FinalOrderIdArray[FinalOrderIdArray.length] = quantityOrdered;
				
					var quantityShipped = LineDetails.shipmentDetail[j].quantityShipped;
					if(!emptyObject(quantityShipped, "QuantityShipped")) return false; 
					FinalShipQty[FinalShipQty.length] = quantityShipped;
				}
				
				//6. Fetch UPCCode from transferDetail object for final processing - in case of partial shipments
				for(var j=0; j < FinalShipArray.length; j++)
				{
					for(var i in dataIn.transferOrder.transferDetail)
					{
						if(dataIn.transferOrder.transferDetail[i].transferLineNumber === FinalShipArray[j]) FinalUPCCodes[FinalUPCCodes.length] = dataIn.transferOrder.transferDetail[i].item.SKUId;
					}
				}		
			}
		}
		
		if(update_flag == 'F')
		{
			//7. Check if Line IDs match
			if(FinalUPCCodes.length <= 0)
			{
				ErrorObj.messages[0].messagetype = "Lineids do not match";
				ErrorObj.messages[0].message = "Order cannot be processed as shipment object Lineids could not be found in Detail Object";
				return false;
			}
		}

		//8. Check if salesorder/transferOrder is Valid and in Pending Fulfillment status
		var finalmessage 							= new Object();
		finalmessage.status 						= "Success";
		finalmessage.records						= new Array();
		finalmessage.records[0] 					= new Object();
		finalmessage.records[0].status 				= "Success";
		finalmessage.records[0].transactiontype 	= "";
		finalmessage.records[0].transactionid 		= 0;
		finalmessage.records[0].transactionnumber	= 0;
		if(update_flag != 'F')	finalmessage.records[0].message	= "Order has been updated successfully"
		try
		{
			var orderInvalidFlag = 'F';
			var recType 		 = (Trantype945 == 'salesorder_b2b' || Trantype945 == 'salesorder') ? 'salesorder' : 'transferOrder';
			var record 		 	 = nlapiLoadRecord(recType, transformID);
			var recordStatus 	 = record.getFieldValue('statusRef');
			var recordStat 	 	 = record.getFieldValue('status');
			var salesItemCount 	 = record.getLineItemCount('item');	

			if(Trantype945 == 'salesorder')
			{
				var recterms 		= record.getFieldValue('terms');
				var recpaymeth 		= record.getFieldValue('paymentmethod');
				salesItemCount 		= record.getLineItemCount('item');	
				
				finalTransformType = ((recterms != null && recterms.length > 0) || (recpaymeth != null && recpaymeth.length > 0)) ? 'cashsale' : 'invoice';
			}
			if(update_flag !== 'F')	 // Check for Updates
			{
				finalmessage.records[0].transactiontype 	= recType;
				finalmessage.records[0].transactionid 		= transformID;
				finalmessage.records[0].transactionnumber	= record.getFieldValue('tranid');
			}
			
			if(update_flag == 'TO')	 		// Check for TO Updates
			{
				record.setFieldValue('trandate', dataIn.transferOrder.transferHeader.transactionDt);
				record.setFieldValue('memo', 	 dataIn.transferOrder.transferHeader.transferHeaderMemo);
				if(dataIn.transferOrder.transferHeader.transferStatus == 'Closed' || dataIn.transferOrder.transferHeader.transferStatus == 'Rejected')
				{
					for(var i=0; i<record.getLineItemCount('item'); i++)
					{
						record.selectLineItem('item', i+1);
						record.setCurrentLineItemValue('item', 'isclosed', 'T');
						record.commitLineItem('item');
					}
					finalmessage.records[0].message	= "Transfer order record has been closed";
				}
				nlapiSubmitRecord(record);
				return finalmessage;
			}
			else if(update_flag == 'SO')	// Check for SO Updates
			{
				record.setFieldValue('trandate', dataIn.order.orderHeader.orderTransactionDt);
				record.setFieldValue('memo', 	 dataIn.order.orderHeader.orderHeaderMemo);
				try
				{
					var customerRecord 	= nlapiLoadRecord('customer', dataIn.order.orderHeader.billingCustomer.customerId);
					customerRecord.setFieldValue('email', 		dataIn.order.orderHeader.billingCustomer.customerEmail);
					customerRecord.setFieldValue('salutation', 	dataIn.order.orderHeader.billingCustomer.honorific);
					customerRecord.setFieldValue('firstname', 	dataIn.order.orderHeader.billingCustomer.firstName);
					customerRecord.setFieldValue('lastname', 	dataIn.order.orderHeader.billingCustomer.lastName);
					customerRecord.setFieldValue('phone', 		dataIn.order.orderHeader.billingCustomer.customerPhoneNumber);
					//customerRecord.setFieldValue('gender', dataIn.order.orderHeader.billingCustomer.gender);
					nlapiSubmitRecord(customerRecord);
					finalmessage.records[1] 			= new Object();
					finalmessage.records[1].status		= "Success";
					finalmessage.records[1].recordtype 	= "customer";
					finalmessage.records[1].recordid 	= dataIn.order.orderHeader.billingCustomer.customerId;
				}
				catch(custErr)
				{
					finalmessage.status 			 	= "Exception";
					finalmessage.records[1] 			= new Object();
					finalmessage.records[1].status		= "Exception";
					finalmessage.records[1].recordtype 	= "customer";
					finalmessage.records[1].recordid 	= dataIn.order.orderHeader.billingCustomer.customerId;
					finalmessage.records[1].message 	= "Customer record cannot be updated. Details as below."; 
					finalmessage.records[1].details 	= custErr; //custErr.getCode() + ' : ' + custErr.getDetails();
				}
				if(dataIn.order.orderHeader.orderStatus == 'Closed' || dataIn.order.orderHeader.orderStatus == 'Rejected')
				{
					for(var i=0; i<record.getLineItemCount('item'); i++)
					{
						record.selectLineItem('item', i+1);
						record.setCurrentLineItemValue('item', 'isclosed', 'T');
						record.commitLineItem('item');
					}
					finalmessage.records[0].message	= "Sales order record has been closed";
				}
				nlapiSubmitRecord(record);
				return finalmessage;
			}
			else
			{
				if(recType == 'salesorder')
				{
					switch(recordStatus)
					{
						case 'pendingBilling':
						case 'cancelled':
						case 'fullyBilled':
						case 'closed':
						case 'partiallyFulfilled':
						case 'pendingBillingPartFulfilled':
							orderInvalidFlag = 'T';
							break;
					}
				}
				else
				{
					switch(recordStatus)
					{
						case 'pendingApproval':
						case 'pendingReceipt':
						case 'cancelled':
						case 'rejected':
						case 'closed':
							orderInvalidFlag = 'T';
							break;
					}
				}
				if(orderInvalidFlag == 'T')
				{
					ErrorObj.messages[0].messagetype = "Invalid order";
					ErrorObj.messages[0].message 	 = "Order cannot be fulfilled as it is in " + recordStat + " status.";
					return false;
				}
				
				//9. Check if fulfillment can be processed
				try
				{
					var itemFulfillment 	 = nlapiTransformRecord(recType, transformID, 'itemfulfillment');
					var itemFulfillItemCount = itemFulfillment.getLineItemCount('item');
					var inventoryItemFulfillmentCount = 0;

					for (var i=1; i<=salesItemCount; i++)
					{
						itemType = record.getLineItemValue('item','itemtype',i);
						if(itemType != 'GiftCert')
						{
							inventoryItemFulfillmentCount += 1;
						}
					}
					
					if(itemFulfillItemCount == 0)
					{
						ErrorObj.messages[0].messagetype = "Item Fulfillment Failure";
						ErrorObj.messages[0].message 	 = "Order cannot be processed as items are backordered and there are no items available to fulfill";
						return false;
					}
					else
					{
						if(Trantype945 == 'salesorder' && inventoryItemFulfillmentCount != itemFulfillItemCount)
						{
							ErrorObj.messages[0].messagetype = "Item Fulfillment Failure";
							ErrorObj.messages[0].message = "Order cannot be processed as item count on fulfillment process does not match item count on salesorder";
							return false;
						}
					}
				}
				catch(err1)
				{
					ErrorObj.status 				 = "Exception";
					ErrorObj.messages[0].messagetype = "Item Fulfillment Failure";
					ErrorObj.messages[0].message 	 = "Order cannot be fulfilled as items are backordered and there are no items available to fulfill.";
					nlapiLogExecution('DEBUG', 'Transform Error', err1);
					return false;
				}
			}
		}
		catch(err)
		{
			nlapiLogExecution('debug', 'Order load error', err);
			if(update_flag != 'F')	 // Check for Updates
			{
				finalmessage.status 					= "Exception";
				finalmessage.records[0].status			= "Exception";
				finalmessage.records[0].transactiontype = recType;
				finalmessage.records[0].transactionid 	= transformID;
				if(Trantype945 == 'transferOrder')	finalmessage.records[0].transactionnumber	= dataIn.transferOrder.transferHeader.transferId;
				else								finalmessage.records[0].transactionnumber	= dataIn.order.orderHeader.orderId;
				finalmessage.records[0].message 	 	= "Order cannot be updated. Details as below."; 
				finalmessage.records[0].details 		= err; //err.getCode() + ' : ' + err.getDetails();
				return finalmessage;
			}
			else	return (invalidOrder());
		}
		return true;
	}
	catch(validate_err)
	{
		ErrorObj.status 				 = "Exception";
		ErrorObj.messages[0].messagetype = "Validation Failure";
		ErrorObj.messages[0].message 	 = "Order cannot be processed. Details as below."; 
		ErrorObj.messages[0].details 	 = validate_err; //validate_err.getCode() + ' : ' + validate_err.getDetails();
		nlapiLogExecution('DEBUG', 'Transform Error', validate_err);
		return false;
	}
}

function FinalTransformProcess(dataIn) // Final routines for 945 process checks for SO & TO
{
	var FinalOrderNumber, FinalOrderId, SourceType, ReturnReqRefNumber, TrackingNumber, TranDate;
	var finalmessage 	= new Object();
	var fulfillmentId;
	finalmessage.status = "Success";
	//var dataIn = JSON.parse(dataIn);
	if(Trantype945 == 'salesorder_b2b' || Trantype945 == 'salesorder')
	{
		TranDate 					= new Date(dataIn.order.shipment[0].shipmentHeader.shipDt);
		TranDate 					= nlapiDateToString(TranDate, 'date');
		finalmessage.orderid 		= transformID;
		finalmessage.ordernumber 	= dataIn.order.orderHeader.orderId;
		finalmessage.shipmentdate 	= TranDate;
		FinalOrderId 				= transformID;// dataIn.order.orderHeader.orderId;
		FinalOrderNumber 			= dataIn.order.orderHeader.orderId; //dataIn.order.orderHeader.parentOrderNumber;
		SourceType 					= 'salesorder';
		ReturnReqRefNumber 			= '';
		TrackingNumber 				= dataIn.order.shipment[0].shipmentHeader.trackingNumber;
	}
	else //Transfer Order
	{
		TranDate 					= new Date (dataIn.transferOrder.shipment[0].shipmentHeader.shipDt);
		TranDate 					= nlapiDateToString(TranDate, 'date');
		finalmessage.orderid 		= transformID; 
		finalmessage.ordernumber 	= dataIn.transferOrder.transferHeader.transferId;
		finalmessage.shipmentdate 	= TranDate;
		FinalOrderId 				= transformID; //dataIn.transferOrder.transferHeader.transferId;
		FinalOrderNumber 			= dataIn.transferOrder.transferHeader.transferId; //dataIn.transferOrder.transferHeader.transferNumber;
		SourceType 					= 'transferOrder';
		ReturnReqRefNumber 			= '';
		TrackingNumber 				= dataIn.transferOrder.shipment[0].shipmentHeader.trackingNumber;
	}
	finalmessage.records = new Array();
	
	try
	{
		var itemFulfillment = nlapiTransformRecord(SourceType, FinalOrderId, 'itemfulfillment');
			
		if(Trantype945 == 'salesorder_b2b' || Trantype945 == 'transferOrder')
		{
			for(var i=0; i < itemFulfillment.getLineItemCount('item'); i++)
			{
				itemFulfillment.selectLineItem('item', i+1);
				var ItemCode = itemFulfillment.getLineItemValue('item', 'custcol_wtka_upccode', i+1);
				itemFulfillment.setCurrentLineItemValue('item', 'quantity', null);
				itemFulfillment.setCurrentLineItemValue('item', 'fulfill',  'F');

				for(var j=0; j <FinalUPCCodes.length; j++)
				{
					if(FinalUPCCodes[j] == ItemCode)
					{
						itemFulfillment.setCurrentLineItemValue('item', 'quantity', FinalShipQty[j]);
						itemFulfillment.setCurrentLineItemValue('item', 'fulfill',  'T');
						itemFulfillment.setCurrentLineItemValue('item', 'custcol_returnreqrefnumber', 		ReturnReqRefNumber);
						itemFulfillment.setCurrentLineItemValue('item', 'custcol_shipmenttrackingnumber', 	TrackingNumber);
						break;
					}
				}
				itemFulfillment.commitLineItem('item');
			}
		}
		else
		{
			for(var i=0; i < itemFulfillment.getLineItemCount('item'); i++)
			{
				itemFulfillment.setLineItemValue('item', 'fulfill', 						i+1, 'T');
				itemFulfillment.setLineItemValue('item', 'custcol_returnreqrefnumber', 		i+1, ReturnReqRefNumber);
				itemFulfillment.setLineItemValue('item', 'custcol_shipmenttrackingnumber', 	i+1, TrackingNumber);
			} 
		}
		itemFulfillment.setLineItemValue('package', 'packageweight', 		 1, parseInt('1'));
		itemFulfillment.setLineItemValue('package', 'packagedescr', 		 1, ReturnReqRefNumber);
		itemFulfillment.setLineItemValue('package', 'packagetrackingnumber', 1, TrackingNumber);
		itemFulfillment.setFieldValue('trandate', 	TranDate);
		itemFulfillment.setFieldText('shipstatus', 	'shipped');
	
		fulfillmentId 	= nlapiSubmitRecord(itemFulfillment, true); 
		if(Trantype945 == 'salesorder_b2b' || Trantype945 == 'salesorder')
		{
			//IS - Appended code - START
			if(fulfillmentId) nlapiSubmitField('salesorder', FinalOrderId ,'custbody_erp_inv_fulfilled', 'T');
			//IS - Appended code - END		
		}

		finalmessage.records[0] 					= new Object();
		finalmessage.records[0].transactiontype 	= "Item Fulfillment";
		finalmessage.records[0].transactionid 		= fulfillmentId;
		finalmessage.records[0].transactiondate 	= TranDate;
		itemfulfillmentflag							= true;
		
		//Create entry into customrecord for tracking of all inbound/outbound calls
		var fulFillLogs = LogCreation(1, 2, FinalOrderId, fulfillmentId, dataIn, 1, finalmessage, 'T'); //0 - Outbound Call, 1 - Inbound call
	}
	catch(err)
	{
		var i = (itemfulfillmentflag) ? 1 : 0;
		finalmessage.records[i] 		= new Object();
		finalmessage.records[i].message = 'Item Fulfillment record could not be created. Details as below.'; 
		finalmessage.records[i].details = err; //err.getCode() + ' : ' + err.getDetails();

		var errorLogs = LogCreation(1, 2, FinalOrderId, 0, dataIn, 1, finalmessage, 'F'); //0 - Outbound Call, 1 - Inbound call		
		
		var subject = 'Inbound 945 call processing failure';
		var body 	= 'Hello,<br><br>';
		body += 'Item fulfillment process against order <b>' + FinalOrderNumber + '</b> failed in NetSuite due to below error.<br>';
		body += '<br><b>Error Details: </b><br>' + err.getCode() + ' : ' + err.getDetails() ;
		body += '<br><br><b>Data received in NetSuite: </b><br><br>' + JSON.stringify(dataIn) + '<br><br><br>';
		body += '<br><br>Process needs to be resubmitted or handled manually through user interface after corrections.';
		body += '<br><br><br><br><br><br><br>Thanks';
		body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
		nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
		if(sendEmail)	nlapiSendEmail(243423, toList, subject, body, ccList);
	}
	
	try
	{
		if(Trantype945 == 'salesorder')
		{
			var finalRecord   = nlapiTransformRecord('salesorder', FinalOrderId, finalTransformType);
			//finalRecord.setFieldValue('trandate', TranDate);  //IDS 012916 Removed to default the date in the Cash Sale
			var finalRecordId = nlapiSubmitRecord(finalRecord, true); 

			if(finalRecordId ) nlapiSubmitField('salesorder', FinalOrderId ,'custbody_erp_inv_cs_created', 'T'); 

			finalmessage.records[1] 				= new Object();
			finalmessage.records[1].transactiontype = (Trantype945 == 'salesorder') ? (finalTransformType == 'cashsale')? "Cash Sale" : "Invoice" : "Item Receipt";
			finalmessage.records[1].transactionid 	= finalRecordId;
			finalmessage.records[1].transactiondate = TranDate;
			var recordLogs = LogCreation(1, 2, FinalOrderId, finalRecordId, dataIn, 1, finalmessage, 'T'); //0 - Outbound Call, 1 - Inbound call
		}
	}
	catch(err1)
	{
		var subject = 'Inbound 945 call processing failure';
		var body 	= 'Hello,<br><br>';
		if(itemfulfillmentflag) body += 'Item fulfillment process executed successfully against <b>' + FinalOrderId + '</b>, but ';

		finalmessage.records[1] = new Object();
		if(Trantype945 == 'salesorder')
		{
			finalmessage.records[1].message = 'Cash Sale/Invoice could not be created. Details as below.'; 
			body += 'Invoice creation process against order <b>' + FinalOrderNumber + '</b> failed in NetSuite due to below error.<br>';
		}
		finalmessage.records[1].details = err1.getCode() + ' : ' + err1.getDetails();

		var finalLogs = LogCreation(1, 2, FinalOrderId, 0, dataIn, 1, finalmessage, 'F'); //0 - Outbound Call, 1 - Inbound call		

		body += '<br><b>Error Details: </b><br>' + err1.getCode() + ' : ' + err1.getDetails() ;
		body += '<br><br><b>Data received in NetSuite: </b><br><br>' + JSON.stringify(dataIn) + '<br><br><br>';
		body += '<br><br>Transaction needs to be resubmitted or handled manually through user interface after corrections.';
		body += '<br><br><br><br><br><br><br>Thanks';
		body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
		nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
		if(sendEmail)	nlapiSendEmail(243423, toList, subject, body, ccList);
	}

	nlapiLogExecution('DEBUG', 'Final Message', JSON.stringify(finalmessage));
	return finalmessage;
}

function putRESTlet945TO(dataIn) //945 call from Cloudhub for TO processing
{
	nlapiLogExecution('DEBUG', 'DATAIN', JSON.stringify(dataIn));
	Trantype945 			= 'transferOrder';
	var PreValidationChecks = Validate945InBoundDataTOSO(dataIn);
	switch(PreValidationChecks)
	{
		case true:
			var transformProcess = FinalTransformProcess(dataIn);
			nlapiLogExecution('DEBUG', 'transformProcess', JSON.stringify(transformProcess));
			return transformProcess;

		case false:
			var errorProcess = ErrorProcess(dataIn);
			return ErrorObj;

		default:		
			nlapiLogExecution('DEBUG', 'updateProcess', JSON.stringify(PreValidationChecks));
			return PreValidationChecks;
	}
}

function putRESTlet945SO(dataIn) //945 call from Cloudhub for TO processing
{
	nlapiLogExecution('DEBUG', 'DATAIN', JSON.stringify(dataIn));
	Trantype945 			= 'salesorder_b2b';
	var PreValidationChecks = Validate945InBoundDataTOSO(dataIn);
	switch(PreValidationChecks)
	{
		case true:
			var transformProcess = FinalTransformProcess(dataIn);
			nlapiLogExecution('DEBUG', 'transformProcess', JSON.stringify(transformProcess));
			return transformProcess;
		
		case false:
			var errorProcess = ErrorProcess(dataIn);
			return ErrorObj;
		
		default:		
			nlapiLogExecution('DEBUG', 'updateProcess', JSON.stringify(PreValidationChecks));
			return PreValidationChecks;
	}
}