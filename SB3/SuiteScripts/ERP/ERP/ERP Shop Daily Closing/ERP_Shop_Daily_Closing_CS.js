/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.02       11 Dec 2015     ivan.sioson
 * 1.03       May 5 2016 Tim Frazer - added AUR field and fixed reloading of goals on edit
 */


function erp_sdc_PageSave(){

	var closingNotesForm = nlapiGetFieldValue('customform');	
	if(closingNotesForm == 124){
	

		if(nlapiGetFieldValue('custrecord_erp_complete') == 'T'){
		
				var varianceNSTerminal = nlapiGetFieldValue('custrecord_erp_dclose_variance');
				var varianceNSTerminalReason = nlapiGetFieldValue('custrecord_erp_dclose_variance_reason');
				if (varianceNSTerminal < 0 && isNullOrEmpty(varianceNSTerminalReason)){
					alert('Are you sure you want to submit with variance reason not set');
					
				}
		
				var dailySales = nlapiGetFieldValue('custrecord_erp_dclose_daily_actual');
				if(isNullOrEmpty(dailySales)){
					alert('Are you sure you want to submit even if sales are not retrieved in the system');
					
				}
		
		                return true;
		
		}else{return true;}
		
		
	} else{
		
		return true;
	}

}

function erp_sdc_PageInit(type, name){

	var closingNotesForm = nlapiGetFieldValue('customform');	
	
	
	if(nlapiGetUser() == 298465 && closingNotesForm == 124){		
		//nlapiSetFieldValue('customform', 132);
	}
	
	if(closingNotesForm == 124){
	
			console.log("erp_sdc_PageInit " + type);
			checkIfComplete();
		
		
			var thisLocation = nlapiGetFieldValue('custrecord_erp_dclose_shop_name');
			if(isNullOrEmpty(thisLocation)){
				 thisLocation = nlapiGetLocation();
			}
		
			var recLocation = nlapiLoadRecord('location', thisLocation);
			setMainShopClosing(recLocation);
		
			if(type == 'edit'){
		
		
				resetAllFields();
				recalcClosingNotes();
		
			}
	
	}else if(closingNotesForm == 132){
		
		
	}else{
				
					
	} 
	
}


function setMandatoryFields(mandatory){


	 nlapiSetFieldMandatory('custrecord_erp_dclose_hours_operation', mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_daily_goal', mandatory);


	 nlapiSetFieldMandatory('custrecord_erp_date_created'				, mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_complete'                    , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_closing_lead'         , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_daily_togoal'   , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_district_director'    , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_hours_operation'      , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_market_director'      , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_opening_lead'         , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_period_actual'        , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_qtr_actual'           , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_shop_director'        , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_shop_name'            , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_shop_name_short'      , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_shop_type'            , mandatory);


	 nlapiSetFieldMandatory('custrecord_erp_dclose_daily_actual'            , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_weekly_goal'             , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_period_goal'             , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_qtr_goal'                , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_actual_hours'            , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_saleslabhour'            , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_sptdpt'                  , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_upt_units_pertrans'      , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_netsuite_closing'        , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_peak_times'              , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_payment_terminal'        , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_returns'                 , mandatory);
	 //nlapiSetFieldMandatory('custrecord_erp_dclose_variance_reason'			, mandatory);
}



function checkIfComplete(){

		console.log('checkIfComplete');

		 var complete = nlapiGetFieldValue('custrecord_erp_complete');
		 var setMandatory = null;

		 if(complete == 'T'){

			 setMandatory = true;

		 }
		 if(complete == 'F'){

			 setMandatory = false;
		 }

		 setMandatoryFields(setMandatory);

}





function erp_sdc_FieldChange(type, name, linenum){


	 //console.log("erp_sdc_CS_FieldChanged " + type);
	
	var closingNotesForm = nlapiGetFieldValue('customform');	
	
	
	if(closingNotesForm == 124){
	

	 if(true){

		 if(name == 'custrecord_erp_dclose_returns' || name == 'custrecord_erp_dclose_ecomm_returns'){

		  var regular = nlapiGetFieldValue('custrecord_erp_dclose_ecomm_returns');
			 var total = nlapiGetFieldValue('custrecord_erp_dclose_returns');
			 var exEcomm = total - regular;

			 nlapiSetFieldValue('custrecord_erp_dclose_regreturn', exEcomm);


		 }

		 //Sales Comp Stuff

		 if(name == 'custrecord_erp_dclose_daily_actual'){

			 var sales = nlapiGetFieldValue('custrecord_erp_dclose_daily_actual');

			var comp = Math.round(sales);

			 nlapiSetFieldValue('custrecord_erp_dclose_daily_salescomp', comp);

		 }

		 if(name == 'custrecord_erp_dclose_weekly_actual'){


		   var sales = nlapiGetFieldValue('custrecord_erp_dclose_weekly_actual');

		var comp = Math.round(sales);

			 nlapiSetFieldValue('custrecord_erp_dclose_weekly_salescomp', comp);

		 }

		 if(name == 'custrecord_erp_dclose_period_actual'){

		   var sales = nlapiGetFieldValue('custrecord_erp_dclose_period_actual');

		   var comp = Math.round(sales);

			 nlapiSetFieldValue('custrecord_erp_dclose_period_salescomp', comp);

		 }

		 if(name == 'custrecord_erp_dclose_qtr_actual'){

		   var sales = nlapiGetFieldValue('custrecord_erp_dclose_qtr_actual');

			var	 comp = Math.round(sales);

			 nlapiSetFieldValue('custrecord_erp_dclose_qtr_salescomp', comp);

		 }

		 if(name == 'custrecord_erp_complete'){
			 checkIfComplete();
		 }

		 if(name == 'custrecord_erp_dclose_shop_name'){

			 resetAllFields();
			 recalcClosingNotes();

		 }

		 if(name == 'custrecord_erp_date_created'){

			 resetAllFields();
			 recalcClosingNotes()

		 }

			if(name == 'custrecord_erp_dclose_daily_goal' || name == 'custrecord_erp_dclose_daily_actual'){
				recalcActualVSGoals('daily');
	 		}

			if(name == 'custrecord_erp_dclose_weekly_goal' || name == 'custrecord_erp_dclose_weekly_actual'){

				recalcActualVSGoals('weekly');

	 		}

			if(name == 'custrecord_erp_dclose_period_goal' || name == 'custrecord_erp_dclose_period_actual'){
				recalcActualVSGoals('period');
	 		}

			if(name == 'custrecord_erp_dclose_qtr_goal' || name == 'custrecord_erp_dclose_qtr_actual'){


				recalcActualVSGoals('qtr');

	 		}

			if(name == 'custrecord_erp_dclose_netsuite_closing' || name == 'custrecord_erp_dclose_payment_terminal' ){

				var nsClosing = nlapiGetFieldValue('custrecord_erp_dclose_netsuite_closing');
	 			var terminal = nlapiGetFieldValue('custrecord_erp_dclose_payment_terminal');

	 			var varianceNSTerminal =  terminal - nsClosing;
				nlapiSetFieldValue('custrecord_erp_dclose_variance', varianceNSTerminal );
			}


	 		if(name == 'custrecord_erp_dclose_actual_hours'){

	 				var  dailyActual = nlapiGetFieldValue('custrecord_erp_dclose_daily_actual');
	 				var  dailyActualHours = nlapiGetFieldValue('custrecord_erp_dclose_actual_hours');
	 				var salesLabHour = dailyActual / dailyActualHours;

						if(dailyActualHours ==  0 || dailyActualHours == '' ){
	 						nlapiSetFieldValue('custrecord_erp_dclose_saleslabhour', 0);
						}else{
							nlapiSetFieldValue('custrecord_erp_dclose_saleslabhour', salesLabHour);
						}
	 		}



	 }else{

		 return true;
	 }
	 
	}else{
		return true;
	}

 }