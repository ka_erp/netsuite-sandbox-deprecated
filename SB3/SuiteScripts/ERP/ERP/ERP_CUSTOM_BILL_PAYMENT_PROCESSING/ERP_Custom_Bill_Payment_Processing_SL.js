/**
 *	File Name		:	ERP_Custom_Bill_Payment_Processing_SL.js
 *	Function		:	Custom Bill Payment Processing page
 * 	Remarks			:	
 *	Prepared by		:	A.Villanueva
 *	Release Dates	:	02-Nov-2016
 * 	Current Version	:	1.0
**/

 /**
 * SUITELET
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet_CustomBillPayment(request, response){

    var epPaymentSelectionForm = new EPPaymentSelectionForm();
   
    //Add Vendor Number Column
	epPaymentSelectionForm.AddColumn('text', 'Vendor No.', 'entitynumber', '', '', '', 'vendor');
	
	//Add Vendor Name Column
	epPaymentSelectionForm.AddColumn('text', 'Vendor Name', 'altname', '', '', '', 'vendor');

	epPaymentSelectionForm.BuildUI(request, response);

	var form  = epPaymentSelectionForm.GetForm();
	response.writePage(form);
}