/**
 *	File Name		:	ERP_Custom_Bill_Payment_Processing_SL.js
 *	Function		:	Custom Bill Payment Processing page
 * 	Remarks			:	
 *	Prepared by		:	A.Villanueva
 *	Release Dates	:	02-Nov-2016
 * 	Current Version	:	1.0
**/

 /**
 * SUITELET
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet_CustomBillPayment(request, response){

	//main(request, response);
	
	//nlapiLogExecution('debug', 'response', response);
  //nlapiLogExecution('debug', 'form', form.toString());

    //response = main(request, response);
	
	//response.writePage(form);
  
  nlapiLogExecution('debug', 'moment(11/1/2016 3:00:00 PM)', moment('11/1/2016 3:00:00 PM'));
   nlapiLogExecution('debug', 'moment(now)', moment(new Date()));
   nlapiLogExecution('debug', 'moment(11/1/2016 3:00:00 PM) <= moment(now)', moment('11/1/2016 3:00:00 PM') <= moment(new Date()));
  
  nlapiLogExecution('debug', 'moment(11/1/2016 3:00:00 PM) > moment(now)', moment('11/1/2016 3:00:00 PM') > moment(new Date()));
	
}