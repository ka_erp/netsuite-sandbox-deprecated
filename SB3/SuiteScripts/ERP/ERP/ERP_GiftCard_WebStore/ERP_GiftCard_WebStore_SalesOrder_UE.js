/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Oct 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function isolate_SalesOrder_userEventBS(type){
 
         nlapiLogExecution("DEBUG", 'type for: ' + nlapiGetRecordId() , type);

	if(type != 'xedit' || type != 'delete'){
	
	var count = nlapiGetLineItemCount("item"); var hasGC = false;
	var gcount = 0; 
	
	var soEmail = nlapiGetFieldValue('email');
	var fName =  nlapiGetFieldValue('custbody_ava_customerfirstname');
	var lName = nlapiGetFieldValue('custbody_ava_customerlastname');
	
	for(var i=1; i<= count; i++){
		
		nlapiSelectLineItem('item',i);
				
		var itemtype = nlapiGetCurrentLineItemValue('item', 'itemtype');											
		var invGC = nlapiGetCurrentLineItemValue('item', 'custcol_erp_inventory_gc');
		
		var lineItemAmount = nlapiGetCurrentLineItemValue('item', 'amount');	
		var lineItemRate = nlapiGetCurrentLineItemValue('item', 'rate');
		var lineItemItem = nlapiGetCurrentLineItemValue('item', 'item');
		var lineItemQty = nlapiGetCurrentLineItemValue('item','quantity');
		var lineItemType = nlapiGetCurrentLineItemValue('item','itemtype');
		var linetaxcode = nlapiGetCurrentLineItemValue('item','taxcode');
		
		var objLineObject = new Object();
		objLineObject.amount = lineItemAmount;
		objLineObject.rate  = lineItemRate;
		objLineObject.item  = lineItemItem;
		objLineObject.qty   = lineItemQty;
		objLineObject.email = soEmail;
		objLineObject.fName = fName;
		objLineObject.lName = lName;
		objLineObject.inv = invGC;
			
	

		if(invGC == 'T'){
			

			for(var f=0; f < lineItemQty ; f++){
												
				var randomCode =  createCode(); // Math.random().toString(36).substring(11);  //randomCode = "GC" + randomCode;
				
				var newline = i + f + 1;
				
				nlapiInsertLineItem('item', newline);
				nlapiSelectLineItem('item', newline);
				//nlapiSelectNewLineItem('item');
				nlapiSetCurrentLineItemValue('item','item', 3048);
				nlapiSetCurrentLineItemValue('item','itemtype', 'GiftCert');
				nlapiSetCurrentLineItemValue('item','giftcertfrom', fName + " " + lName);
				nlapiSetCurrentLineItemValue('item','giftcertrecipientemail', 'testemail@asdbcd.com');
				nlapiSetCurrentLineItemValue('item','giftcertrecipientname', fName + " " + lName);
				nlapiSetCurrentLineItemValue('item','amount', lineItemRate);
				nlapiSetCurrentLineItemValue('item','rate', lineItemRate);	
				nlapiSetCurrentLineItemValue('item','taxcode', linetaxcode);	
				nlapiSetCurrentLineItemValue('item','custcol_item_style2', 'Gift Card');

				//nlapiSetCurrentLineItemValue('item','giftcertnumber',  randomCode );					
				//nlapiSetCurrentLineItemValue('item','custcol_erp_memo',  randomCode);
				
				nlapiCommitLineItem("item");				
				count++; 				
					
			}
			
		}else{
			// nlapiLogExecution("DEBUG", nlapiDateToString(new Date(), "datetimetz") + "item", JSON.stringify(objLineObject) );	
			
		}
		
	}
	
	
	
	
	removeGCInventory();
	
	}
	
}



function removeGCInventory(){
	

	var gcount = 0; var hasGC = false; 
	var count = nlapiGetLineItemCount("item"); var outstandingcount = count;
	
	
	nlapiLogExecution("DEBUG", nlapiDateToString(new Date(), "datetimetz") + " count 1", count );	
	nlapiLogExecution("DEBUG", nlapiDateToString(new Date(), "datetimetz") + " outstandingcount 1", outstandingcount );
	
	for(var i=1; i<= count; i++){
	
		nlapiSelectLineItem('item',i);			
		var invGC = nlapiGetCurrentLineItemValue('item', 'custcol_erp_inventory_gc');				
		var itemType = nlapiGetCurrentLineItemValue('item', 'itemtype');
		
		nlapiLogExecution("DEBUG", nlapiDateToString(new Date(), "datetimetz") +"  " + i +  " ================> Type", itemType );	
		if(itemType == 'GiftCert'){			
			gcount++; hasGC = true;	
		}
		
		if(invGC == 'T'){
			//gcount++;
			nlapiRemoveLineItem('item', i);  i--; count--;
			outstandingcount--;
		}
						
	}
	
	
	nlapiLogExecution("DEBUG", nlapiDateToString(new Date(), "datetimetz") + " count 2", count );	
	nlapiLogExecution("DEBUG", nlapiDateToString(new Date(), "datetimetz") + " gcount 2", gcount );
	nlapiLogExecution("DEBUG", nlapiDateToString(new Date(), "datetimetz") + " outstandingcount 2", outstandingcount );
	
	if(gcount == outstandingcount){
		nlapiSetFieldValue('custbody_erp_allgc', 'T'); 				
	}else{
		nlapiSetFieldValue('custbody_erp_allgc', 'F');
	}
	
	
	if(hasGC){		
		nlapiSetFieldValue('custbody_erp_so_hasgc', 'T'); 		
	}else{
		//nlapiSetFieldValue('custbody_erp_so_hasgc', 'F');
	}
	
	
	nlapiSetFieldValue('custbody_giftcard_items',gcount);
}

function createCode()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 7; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    text = "GC" + text;  
    return text;
}
