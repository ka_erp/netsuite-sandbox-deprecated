/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Oct 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */

function cashsales_GiftCardPostProcess_UE_BS(){
	
	
		
}


function cashsales_GiftCardPreProcess_UE_BL(){
		
	var invFulfilled = nlapiGetFieldValue("custbody_erp_inv_fulfilled");
	
	if(type == 'edit' && invFulfilled == 'T'){
				
		var count = nlapiGetLineItemCount("item");
		
		nlapiLogExecution('DEBUG', 'Gift card object line: count', count); 			
		
		
		for(var i=1; i<= count; i++){
			
			
			
			
			var itemtype = nlapiGetLineItemValue('item', 'itemtype', i); 
			//var itemname = nlapiGetLineItemText('item', 'item', i); 
			var itemid = nlapiGetLineItemValue('item', 'item', i); 
			
			
			nlapiLogExecution("DEBUG", "item", "item: " +  itemid + " type:" + itemtype );
			

			var arrLineObj = new Array();
						
			if(itemtype == 'GiftCert'){
				
				var lineItemAmount = nlapiGetLineItemValue('item', 'amount',i);	
				var lineItemRate = nlapiGetLineItemValue('item', 'rate',i);
				var lineItemItem = nlapiGetLineItemValue('item', 'item',i);
				var lineItemQty = nlapiGetLineItemValue('item','quantity',i);
				
				var soEmail = nlapiGetFieldValue('email');
				var fName =  nlapiGetFieldValue('custbody_ava_customerfirstname');
				var lName = nlapiGetFieldValue('custbody_ava_customerlastname');
				
				
				var objLineObject = new Object();
				objLineObject.itemtype = itemtype;
				objLineObject.amount = lineItemAmount;
				objLineObject.rate  = lineItemRate;
				objLineObject.item  = lineItemItem;
				objLineObject.qty   = lineItemQty;
				objLineObject.email = soEmail;
				objLineObject.fName = fName;
				objLineObject.lName = lName;
				
				nlapiLogExecution('AUDIT', 'Gift card object line push', JSON.stringify(objLineObject)); 
				arrLineObj.push(objLineObject);
											
				nlapiLogExecution("DEBUG", "removing the cert", i + " item:" + lineItemItem);
				nlapiRemoveLineItem('item', i); i--; count--;
				
//				var randomCode = Math.random().toString(36).substring(11);  randomCode = "GC" + randomCode; 
				
										
			}
			
		}
		
		nlapiLogExecution("AUDIT", "creating new GCs", JSON.stringify(arrLineObj));
		for(var j = 0; j < 10; j++){
			
			var objLine = arrLineObj[0];
			
			nlapiSelectNewLineItem('item');
			nlapiSetCurrentLineItemValue('item','item', 3048);
			nlapiSetCurrentLineItemValue('item','giftcertfrom', objLine.fName + " " + objLine.lName);
			nlapiSetCurrentLineItemValue('item','giftcertrecipientemail', objLine.email);
			nlapiSetCurrentLineItemValue('item','giftcertrecipientname', objLine.fName + " " + objLine.lName);								
			nlapiSetCurrentLineItemValue('item','amount', objLineObject.rate);
			nlapiSetCurrentLineItemValue('item','rate', objLineObject.rate);								
			nlapiCommitLineItem("item");
			//i++; count++; 
			
			
		}
		
	}
	
	
}




function isolate_CashSales_userEventBS(type){
	
	try{
		
		nlapiLogExecution('AUDIT', 'isolate_CashSales_userEventBS', nlapiGetRecordId());
		
		if(type != 'delete'){
		
		var exec_context = nlapiGetContext().getExecutionContext();
		nlapiLogExecution("AUDIT", "CONTEXT", JSON.stringify(exec_context));
		
			
			if(exec_context == "webservices" || exec_context == "suitelet"){	
				
				
				var count = nlapiGetLineItemCount("item"); nlapiLogExecution("DEBUG", "count", "item count: " + count);							
				
					for(var i=1; i<= count; i++){
							
						nlapiSelectLineItem('item',i);
												
						var itemtype = nlapiGetCurrentLineItemValue('item', 'itemtype');
						var itemname = nlapiGetCurrentLineItemText('item', 'item');
						var itemid = nlapiGetCurrentLineItemValue('item', 'item');
						
						
						nlapiLogExecution("DEBUG", "item", "item: " +  itemid + " type:" + itemtype );
						
						if(itemtype == 'GiftCert'){
							nlapiLogExecution("DEBUG", "removing the cert", itemname);
							nlapiRemoveLineItem('item', i); i--; count--;
						}else{
							nlapiSetCurrentLineItemValue('item', 'custcol_erp_memo', 'test');
							nlapiCommitLineItem('item');
						}					
					}
				
					//nlapiSetFieldValue('customform', 174); 
					
				
			}else if(exec_context == "userinterface"){
				//nlapiLogExecution("AUDIT", "TRYING TO SEND EMAIL", "sending emaiL");
				//sendEmail(type);
				
				
				nlapiLogExecution("AUDIT", "exec_context", exec_context);
				
				
				var count = nlapiGetLineItemCount("item"); var gccount = 0; 
				var initgccount = nlapiGetFieldValue("custbody_giftcard_items"); 
				var idCreatedFrom = nlapiGetFieldValue("createdfrom");
				
				nlapiLogExecution("AUDIT", "count", count);
				nlapiLogExecution("AUDIT", "initgccount", initgccount);
				nlapiLogExecution("AUDIT", "idCreatedFrom", idCreatedFrom);

				for(var i=1; i<= count; i++){
					
					var itemtype = nlapiGetLineItemValue('item', 'itemtype', i);
					if(itemtype == 'GiftCert'){
						gccount++;
					}					
				}
				
				nlapiLogExecution("AUDIT", "gccount", gccount);
				
				if((initgccount == gccount) && (gccount != 0)){		 //Ensure that the GC count is not sent when zero			
					nlapiSubmitField('salesorder', idCreatedFrom, 'custbody_erp_allgc_fulfilled', 'T'); 
				}
				
				
				var giftLine = nlapiFindLineItemValue('item','itemtype','GiftCert');
				nlapiLogExecution("AUDIT", "first gc line", giftLine);
				
				if(giftLine > 0){
				//nlapiSelectLineItem('item',giftLine);
				var cert = nlapiGetLineItemValue('item', 'giftcertnumber', giftLine);
				nlapiSetFieldValue('custbody_first_gift_cert_code',cert);
				}
			}
		
		}
	}catch(ex){}
}

function isolate_CashSales_userEventAS(type){
	
	if(true){
	
	var recordId = nlapiGetRecordId();
	nlapiLogExecution('AUDIT', 'isolate_CashSales_userEventAS', recordId);
	
	if(true){ //type == 'create' ){
		
		//var customForm = nlapiLookupField('cashsale', recordId, 'customform');
		
		var salesOrderId = nlapiLookupField('cashsale', recordId, 'createdfrom');
		
		if(salesOrderId){
		
			var csOject = nlapiLoadRecord('salesorder', salesOrderId);
			var csFulfilled = csOject.getFieldValue('custbody_erp_inv_fulfilled'); 		
			var csHasGC = csOject.getFieldValue('custbody_erp_so_hasgc'); 
			var csAllGCFulfilled = csOject.getFieldValue('custbody_erp_allgc_fulfilled'); 

		}
		
		//if(customForm == 174){
		if(csAllGCFulfilled == 'T'){
			sendEmail(type);
			
//			var createdFrom = nlapiLookupField('cashsale', recordId, 'createdfrom'); //nlapiGetFieldValue('createdfrom');
//			nlapiSubmitField('salesorder',createdFrom, 'custbody_erp_allgc_fulfilled','T');
			
		}
		
	}
	
	}
	
//		else if(type == 'delete'){
//		
//		var createdFrom = nlapiGetFieldValue('createdfrom');
//		nlapiSubmitField('salesorder',createdFrom, 'custbody_erp_allgc_fulfilled','T');
//	}
	
	
}

function isolate_GiftCards_userEventBL(type){
	
	nlapiLogExecution('AUDIT', 'isolate_GiftCards_userEventBL', nlapiGetRecordId());
	
	var exec_context = nlapiGetContext().getExecutionContext();
	nlapiLogExecution("AUDIT", "CONTEXT", JSON.stringify(exec_context));
	
		
	if(exec_context == "userinterface"){

		//if(type == 'create'){
		
		nlapiLogExecution("AUDIT", "ENTER UI", 'trying to set the form id');			
		nlapiSetFieldValue('customform', 174); 
				
		//		var exec_context = nlapiGetContext().getExecutionContext();
		//		nlapiLogExecution("AUDIT", "CONTEXT", JSON.stringify(exec_context));
		//			
		//		
		//		if(exec_context == "userinterface" ){
		//			
		//			var count = nlapiGetLineItemCount("item"); nlapiLogExecution("DEBUG", "count", "item count: " + count);
		//			var cert = "";
		//			
		//			try{
		//			
		//				for(var i=1; i<= count; i++){
		//						
		//					nlapiSelectLineItem('item',i);
		//					cert = cert + " " +  nlapiGetCurrentLineItemValue('item', 'giftcertnumber');
		//					
		//					var itemtype = nlapiGetCurrentLineItemValue('item', 'itemtype');
		//					var itemname = nlapiGetCurrentLineItemText('item', 'item');
		//					var itemid = nlapiGetCurrentLineItemValue('item', 'item');
		//					
		//					
		//					nlapiLogExecution("DEBUG", "item", "item: " +  itemid + " type:" + itemtype );
		//					
		//					if(itemtype != 'GiftCert'){
		//						nlapiLogExecution("DEBUG", "removing the cert", itemname);
		//						nlapiRemoveLineItem('item', i); i--; count--;
		//					}else{
		//						nlapiSetCurrentLineItemValue('item', 'custcol_erp_memo', 'test');
		//						nlapiCommitLineItem('item');
		//					}					
		//				}
		//				
		//				nlapiSetFieldValue('custbody_first_gift_cert_code',cert);
		//			
		//			}catch(ex){}
		//	
		//		}
		
		}
	

		//}
	
}

function sendEmail(type) {
	
	// If this is a new record, proceed
//	if(type == 'create') {
		
		try {
			
			// If this record was not created from the UI context, return
			/*if(nlapiGetContext().getExecutionContext() != 'userinterface') {
				
				return;
			}*/
			
			var soId
			,	emailMerger
			,	mergeResult
			,	templateId
			,	emailBody
			,	emailSender
			,	emailSubject
			,	emailRecipient;
				
			templateId = '17';
			
			emailSender = 1549;				
			emailSubject = 'Your Kit and Ace gift card confirmation';	// TODO: Get desired subject
			
			soId = nlapiGetRecordId();
			
			emailRecipient = nlapiGetFieldValue('entity');	
			emailMerger = nlapiCreateEmailMerger(templateId);		
			emailMerger.setTransaction(soId);
			mergeResult = emailMerger.merge();		
			emailBody = mergeResult.getBody();
			
//			testBody = testBody.splice(200, 500);		
//			nlapiLogExecution('DEBUG', 'Merge result', testBody);
			
			try {
				
				nlapiSendEmail(emailSender, emailRecipient, emailSubject, emailBody, null, null, {transaction: soId});
			} catch(e) {
				
				nlapiLogExecution('ERROR', 'Error sending Sales Order notification email', e);
			}
			
		} catch(e) {
			
			nlapiLogExecution('ERROR', 'Unexpected Error', e);
			nlapiSendEmail(6, 'client-login@exploreconsulting.com', 'Unexpected Error sending notification email', e);
		}
//	}
}

