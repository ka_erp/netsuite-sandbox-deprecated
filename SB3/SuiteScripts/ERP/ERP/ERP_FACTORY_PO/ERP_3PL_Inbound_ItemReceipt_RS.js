/**
 *	File Name		:	ERP_3PL_Inbound_ItemReceipt_RS.js
 *	Function		:	
 * 	Remarks			:	
 *	Prepared by		:	christopher.neal@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{
	var ITEM_RECEIPT_FOLDER_ID = 682322; //@Apple: 682322 is the foler ID in SB3
	var STATUS_READY = SCRIPTCONFIG.getScriptConfigValue('IR Processed Status: Ready');

	var LogDirection = 1; //2 - Outbound Call, 1 - Inbound call
}


function putRESTlet_ItemReceipt(dataIn) //Inbound call from Cloudhub for Item Receipt processing
{
	nlapiLogExecution('DEBUG', 'DATAIN', JSON.stringify(dataIn));

	var responseObj = validateAndCreateInboundItemReceipt(dataIn); //an object to return to the ESB
	
	return responseObj;
}


function validateAndCreateInboundItemReceipt(dataIn){

	var responseObj = {};

	try {	

		//1)Validate the inbound message
		var objResult = FactoryPOLib.validateTranId(dataIn.tranid);		
		//If invalid PO
		if(!objResult.success){
			throw nlapiCreateError('ERROR', JSON.stringify(objResult.error), true);
		}
	
		//2)Create Custom Record instance in Netsuite (Inbound Item Receipt Message - customrecord_erp_3pl_inbound_itemreceipt) 
		var poId = objResult.internalid;
		var ItemReceiptMsg = nlapiCreateRecord('customrecord_erp_3pl_inbound_itemreceipt');

		ItemReceiptMsg.setFieldValue('custrecord_itemreceipt_recordid', dataIn.tranid);
		ItemReceiptMsg.setFieldValue('custrecord_itemreceipt_linkedtransaction', poId);

		var newFile = nlapiCreateFile(JSON.stringify(dataIn.tranid), 'PLAINTEXT', JSON.stringify(dataIn));
		newFile.setFolder(ITEM_RECEIPT_FOLDER_ID);

		var fileId = nlapiSubmitFile(newFile);	
		ItemReceiptMsg.setFieldValue('custrecord_itemreceipt_inboundfile', fileId);

		var msgString = JSON.stringify(dataIn);
		ItemReceiptMsg.setFieldValue('custrecord_itemreceipt_inboundmessage', msgString);

		ItemReceiptMsg.setFieldValue('custrecord_itemreceipt_status', STATUS_READY);

		var ItemReceiptMsgId = nlapiSubmitRecord(ItemReceiptMsg);
		nlapiLogExecution('DEBUG', 'Created Item Receipt Message Object', 'Record ID: ' + ItemReceiptMsgId);

		//3)Create a response message
		responseObj.status = 'SUCCESS';
		responseObj.message = 'NetSuite accepted the inbound message';

	} catch (error){		
		nlapiLogExecution('DEBUG', 'Error', error.toString());
		responseObj = FactoryPOLib.generateErrorObject('NetSuite failed to process the inbound message', error);
	}
	
	//Logging
	FactoryPOLib.log(LogDirection, (poId ? poId : 0), 0, dataIn, FactoryPOLib.getLogStatusID(responseObj.status), responseObj, 'T', ItemReceiptMsgId);

	return responseObj;
}
