/**
 *	File Name		:	ERP_3PL_ICSO_Automation_UE.js
 *	Function		:	ICSO Automation
 * 	Remarks			:	Developed for Factory PO automation
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{
	var PROCESSED_TRAN_VAL; //Processed Transactions field value of Inbound IR Message record
	
	//Logging
	var ORIG_PO_ID;
	var INBOUND_IR_MSG_ID;	
	var LogDirection = 1; //2 - Outbound Call, 1 - Inbound call
	
	//FACTORY PO - filters
	var CHANNELS = SCRIPTCONFIG.getScriptConfigValue('ICSO Automation: Channels');
	var FROM_LOCATIONS = SCRIPTCONFIG.getScriptConfigValue('ICSO Automation: From Locations');
	var TO_LOCATIONS = SCRIPTCONFIG.getScriptConfigValue('ICSO Automation: To Locations');
}

/**
 * AFTER SUBMIT
 */
function afterSubmit_performTransform(type){
	
	var stContext = nlapiGetContext().getExecutionContext();
	var stRecType = nlapiGetRecordType();
		
	nlapiLogExecution('DEBUG', 'context', stContext);
	nlapiLogExecution('DEBUG', 'rectype', stRecType); 
	nlapiLogExecution('DEBUG', 'type', type); 
	
	if(stRecType.toUpperCase() == 'SALESORDER' && stContext  == 'userevent'){
		if(type == 'create' || type == 'edit' || type == 'xedit'){
			
			var recICSO = nlapiLoadRecord('salesorder', nlapiGetRecordId());
			var stICPO = recICSO.getFieldValue('intercotransaction');
			var stStatus = recICSO.getFieldValue('statusRef');
			
			//FACTOR 1: Process only when ICSO is performed
			if(stICPO && stStatus.toUpperCase() == 'PENDINGFULFILLMENT'){
				
				var recICPO = nlapiLoadRecord('purchaseorder', stICPO);
				var stExtOrderNo = recICPO.getFieldValue('custbody_wtka_extsys_order_number');
				var stCreatedFromIR = recICPO.getFieldValue('custbody_ka_item_receipt_number');
				
				//FACTOR 2: Process only if PO is not linked to a Hybris Order
				if(!stExtOrderNo){
					
					ORIG_PO_ID = 0; // For logging
					INBOUND_IR_MSG_ID = null; //For logging
					
					//FACTOR 3: Process only if CREATED FROM ITEM RECEIPT field is not empty
					if(stCreatedFromIR){
						
						var stOrigPOChannelTxt;
						var stOrigPOLocation;
						var stOrigPOShipTo;
						
						//Get Original PO using ICPO's CREATED FROM ITEM RECEIPT field
						var arOrigPO = nlapiSearchRecord('itemreceipt', null,
										[new nlobjSearchFilter('internalid', null, 'is', stCreatedFromIR)],
										[new nlobjSearchColumn('createdfrom'),
										 new nlobjSearchColumn('custbody_ka_erp_channel_new', 'createdfrom'),
										 new nlobjSearchColumn('location', 'createdfrom'),
										 new nlobjSearchColumn('shipto', 'createdfrom')]);
						
						if(arOrigPO && arOrigPO.length > 0){	
							ORIG_PO_ID = arOrigPO[0].getValue('createdfrom');
							stOrigPOChannelTxt = arOrigPO[0].getText('custbody_ka_erp_channel_new', 'createdfrom');
							stOrigPOLocation = arOrigPO[0].getValue('location', 'createdfrom');
							stOrigPOShipTo = arOrigPO[0].getValue('shipto', 'createdfrom');
						}
							
						//Find Inbound IR Message custom record
						if(ORIG_PO_ID){
							var arIRMsg = FactoryPOLib.searchInboundIRMessage(ORIG_PO_ID, stCreatedFromIR);
							if(arIRMsg && arIRMsg.length > 0){
								INBOUND_IR_MSG_ID = arIRMsg[0].getValue('internalid');
								PROCESSED_TRAN_VAL = arIRMsg[0].getValue('custrecord_itemreceipt_processed_tran');
							} 
						}
						
						//FACTOR 4: Process only if Orig PO has the right fields (Channel, Location, ShipTo > Location)
						if(FactoryPOLib.isValidChannel(stOrigPOChannelTxt, CHANNELS)			//Channel = valid (based on Script Config)
						  && FactoryPOLib.isValidLocation(stOrigPOLocation, FROM_LOCATIONS)		//Location = valid (based on Script Config)
						  && FactoryPOLib.isValidShipToLocation(stOrigPOShipTo, TO_LOCATIONS)){	//ShipTo Location = valid (based on Script Config)
						  
							//Automation
							automateICSO(stICPO, nlapiGetRecordId(), recICSO);
							
							//Update Inbound IR Message Transaction Links
							if(INBOUND_IR_MSG_ID){
								FactoryPOLib.linkToInboundIRMessage(INBOUND_IR_MSG_ID, PROCESSED_TRAN_VAL);
							}
						}
					}
				}
			}			
		}
	}
}

/**
 * Creation of records for ICSO Automation
 */
function automateICSO(stICPOId, stICSOId, recICSO){
	
	nlapiLogExecution('DEBUG', 'automateICSO', 'Performing ICSO Automation'); 
			
	var icsoTranDate 	 = recICSO.getFieldValue('trandate');
	var icsoRecterms 	 = recICSO.getFieldValue('terms');
	var icsoRecpaymeth 	 = recICSO.getFieldValue('paymentmethod');
	var icsoFinalTransformType = (icsoRecterms || icsoRecpaymeth) ? 'cashsale' : 'invoice';
			
	var stProcessDesc;
	var stTranId;
	
	//0. Log and link ICSO to Inbound IR Message
	stProcessDesc = 'ICSO) Logging manually created ICSO: ' + stICSOId;
	automationSuccessLogging(stICSOId, stProcessDesc, stProcessDesc, 'salesorder');
	
	//1. Fulfill ICSO
	stProcessDesc = 'ICSO) Fulfilling ICSO: ' + stICSOId;
	nlapiLogExecution('DEBUG', 'stProcessDesc', stProcessDesc);
	try{
		var recICSOIF = nlapiTransformRecord('salesorder', stICSOId, 'itemfulfillment');
		recICSOIF.setFieldValue('trandate', icsoTranDate);
		recICSOIF.setFieldText('shipstatus', 'shipped');
		var stICSOIFId = nlapiSubmitRecord(recICSOIF, true);
				
		//Logging
		automationSuccessLogging(stICSOIFId, stProcessDesc, 'ICSO) Created ICSO Item Fulfillment: ' + stICSOIFId, 'itemfulfillment');

	} catch(ex){
		nlapiLogExecution('DEBUG', 'ERROR - Fulfilling ICSO', ex);
		
		//Logging
		FactoryPOLib.log(LogDirection, ORIG_PO_ID, 0, stProcessDesc, FactoryPOLib.getLogStatusID('ERROR'), 
						ex.getCode() + ' : ' + ex.getDetails(), 'F', INBOUND_IR_MSG_ID);
						
		return; 
	}

	//2. Receive ICPO
	stProcessDesc = 'ICSO) Receiving ICPO: ' + stICPOId;
	nlapiLogExecution('DEBUG', 'stProcessDesc', stProcessDesc);
	try{
		var recICPOIR = nlapiTransformRecord('purchaseorder', stICPOId, 'itemreceipt');
		recICPOIR.setFieldValue('trandate', icsoTranDate);
		var stICPOIRId = nlapiSubmitRecord(recICPOIR, true);

		//Logging
		automationSuccessLogging(stICPOIRId, stProcessDesc, 'ICSO) Created ICPO Item Receipt: ' + stICPOIRId, 'itemreceipt');
		
		//Resubmit ICPO IR
		resubmitIR_suitelet(stICPOIRId);
		
	} catch(ex){
		nlapiLogExecution('DEBUG', 'ERROR - Receiving ICPO', ex);
		
		//Logging
		FactoryPOLib.log(LogDirection, ORIG_PO_ID, 0, stProcessDesc, FactoryPOLib.getLogStatusID('ERROR'), 
						ex.getCode() + ' : ' + ex.getDetails(), 'F', INBOUND_IR_MSG_ID);
						
		return;
	}
	
	//3. Invoice ICSO
	stProcessDesc = 'ICSO) Create Invoice/Cash Sale for ICSO: ' + stICSOId;
	nlapiLogExecution('DEBUG', 'stProcessDesc', stProcessDesc);
	try{
		var recICSOInv = nlapiTransformRecord('salesorder', stICSOId, icsoFinalTransformType);
		recICSOInv.setFieldValue('trandate', icsoTranDate);
		var stICSOInvId = nlapiSubmitRecord(recICSOInv, true, true);
		
		//Tran ID - will be used in ICPO Billing
		stTranId = nlapiLookupField(icsoFinalTransformType, stICSOInvId, 'tranid');

		//Logging
		automationSuccessLogging(stICSOInvId, stProcessDesc, 'ICSO) Created ICSO Invoice/Cash Sale: ' + stICSOInvId, icsoFinalTransformType, stTranId);
		
	} catch(ex){
		nlapiLogExecution('DEBUG', 'ERROR - Invoicing ICSO', ex);
		
		//Logging
		FactoryPOLib.log(LogDirection, ORIG_PO_ID, 0, stProcessDesc, FactoryPOLib.getLogStatusID('ERROR'), 
						ex.getCode() + ' : ' + ex.getDetails(), 'F', INBOUND_IR_MSG_ID);
						
		return;
	}
	
	//3. Bill ICPO
	stProcessDesc = 'ICSO) Billing ICPO: ' + stICPOId;
	nlapiLogExecution('DEBUG', 'stProcessDesc', stProcessDesc);
	try{
				
		var recICPOBill = nlapiTransformRecord('purchaseorder', stICPOId, 'vendorbill');
		recICPOBill.setFieldValue('tranid', stTranId); 					//Set tranid of ICSO Invoice/Cash Sale here
		recICPOBill.setFieldValue('trandate', icsoTranDate);
		var stICPOBillId = nlapiSubmitRecord(recICPOBill, true, true);
		
		//Logging
		automationSuccessLogging(stICPOBillId, stProcessDesc, 'ICSO) Created ICPO Bill: ' + stICPOBillId, 'vendorbill', stTranId);
		
	} catch(ex){
		nlapiLogExecution('DEBUG', 'ERROR - Billing ICPO', ex);
		
		//Logging
		FactoryPOLib.log(LogDirection, ORIG_PO_ID, 0, stProcessDesc, FactoryPOLib.getLogStatusID('ERROR'), 
						ex.getCode() + ' : ' + ex.getDetails(), 'F', INBOUND_IR_MSG_ID);
						
		return;
	}
}

/**
 * Success logging and Update of the Inbound IR Message Transaction Links
 */
function automationSuccessLogging(stCreatedTranId, stProcessDesc, stResultDesc, stTranType, stTranId){
	
	//Logging
	FactoryPOLib.log(LogDirection, ORIG_PO_ID, stCreatedTranId, stProcessDesc, FactoryPOLib.getLogStatusID('SUCCESS'), stResultDesc, 'F', INBOUND_IR_MSG_ID);
	
	var stKey = '';
	switch(stTranType){
		case 'salesorder':
			stKey = 'ICSO';
			break;
		case 'itemfulfillment':
			stKey = 'ICSOIF';
			break;
		case 'itemreceipt':
			stKey = 'ICPOIR';
			break;
		case 'invoice':
			stKey = 'ICSOINV';
			break;
		case 'cashsale':
			stKey = 'ICSOCS';
			break;
		case 'vendorbill':
			stKey = 'ICPOVB';
			break;
		default:
			break;
	}
			
	//Store transaction to Processed Transactions
	if(stKey){
		PROCESSED_TRAN_VAL = FactoryPOLib.updateInboundMessageTranLinks(PROCESSED_TRAN_VAL, stKey, stCreatedTranId, 
								stTranId ? stTranId : nlapiLookupField(stTranType, stCreatedTranId, 'tranid'));
	}	
}

/**
 * Calls suitelet to resubmit ICPO IR
 */
function resubmitIR_suitelet(irId){
	
	var stLogStatus = FactoryPOLib.getLogStatusID('ERROR');
	var stResult = '';

	try{
		var stUrl = nlapiResolveURL('SUITELET', 'customscript_erp_3pl_resubmit_ir_sl', 'customdeploy_erp_3pl_resubmit_ir_sl', true);
		nlapiLogExecution('DEBUG', 'resubmitIR_suitelet', 'URL: ' + stUrl);	
			
		var params = new Array();
		params['irid'] = irId;
		
		nlapiLogExecution('DEBUG', 'irId', irId); 
						
		var objResponse = nlapiRequestURL(stUrl, params);
		nlapiLogExecution('DEBUG', 'resubmitIR_suitelet', 'objResponse.getCode(): ' + objResponse.getCode());	
		if(objResponse.getCode() == '200'){
			stResult = objResponse.getBody();
			nlapiLogExecution('DEBUG', 'resubmitIR_suitelet', 'Response: ' + stResult);			
			stLogStatus = FactoryPOLib.getLogStatusID('SUCCESS');
		}
	} catch(ex){		
		stResult = ex.toString();
		nlapiLogExecution('DEBUG', 'resubmitIR_suitelet', 'error: ' + stResult);	
	}
	
	//Logging
	FactoryPOLib.log(LogDirection, ORIG_PO_ID, 0, 'ICSO) Resubmitting ICPO IR (via suitelet): ' + irId, stLogStatus, 
		stResult, 'F', INBOUND_IR_MSG_ID);
		
}