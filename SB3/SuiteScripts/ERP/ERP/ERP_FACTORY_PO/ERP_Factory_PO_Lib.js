/**
 *	File Name		:	ERP_Factory_PO_Lib.js
 *	Function		:	Factory PO common functions
 * 	Remarks			:	
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

var FactoryPOLib = {
	
	//Checks if request parameter is empty
	isEmptyRequestObject : function(objReqParam, stObjKey, stTranId){
		var objResult = {};
		objResult.isEmpty = false;
		
		if(!objReqParam || JSON.stringify(objReqParam) == '{}' || objReqParam.length < 1){
			var errorObj = {};					
			errorObj.status = 'Exception';
			errorObj.orderid = stTranId ? stTranId : '';	
			errorObj.message = stObjKey + ' data is empty';
			
			objResult.isEmpty = true;
			objResult.error = errorObj;
		}
		
		return objResult;
	},
	
	//Returns internalid of PO based on tranid
	getInternalId : function(tranId){
		
		var poId = null;
		
		if(tranId){
			var cols = new Array();
			var filters = new Array();
			filters.push(new nlobjSearchFilter('tranid', null, 'is', tranId));
			filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
			var results = nlapiSearchRecord('purchaseorder', null, filters, cols);	

			if(results && results.length >= 0){
				poId = results[0].id;			
			}
		}
		
		return poId;
	},
	
	//Validates tranid
	validateTranId : function(tranId){
		
		var poId = this.getInternalId(tranId);
		
		var objResult = {};
		objResult.success = true;
		objResult.internalid = poId;
		
		//Invalid
		if(!poId){
			var errorObj = {};					
			errorObj.status = 'Exception';
			errorObj.orderid = tranId;		
			errorObj.message = 'Invalid Purchase Order ID';
			
			objResult.success = false;
			objResult.error = errorObj;
		}
		
		return objResult;
	},
	
	//Validation of quantityReceived
	validateQuantityReceived : function(dataIn){
		var objResult = {};
		objResult.success = true;
		
		for(var j = 0; j < dataIn.purchaseOrder.shipment.length; j++){
			var objShipment = dataIn.purchaseOrder.shipment[j];
			var stShipStatus = objShipment.shipmentHeader.shipmentStatus;
			if(stShipStatus.toUpperCase() == 'RECEIVED'){
				var stQtyReceived = 0;
				if(objShipment.shipmentDetail && objShipment.shipmentDetail.length > 0){
					for(var i = 0; i < objShipment.shipmentDetail.length; i++){
						if(objShipment.shipmentDetail[i].quantityReceived){	
							stQtyReceived += parseFloat(objShipment.shipmentDetail[i].quantityReceived);
						}			
					}
				}			
				if(stQtyReceived == 0){
					var errorObj = {};			
					errorObj.status = 'Exception';
					errorObj.orderid = dataIn.purchaseOrder.orderHeader.orderId;		
					errorObj.message = 'purchaseOrder.shipment[' + j + ']'
						+ ' : Purchase Order cannot be processed as shipment status says Received but quantity received is 0';
					
					objResult.success = false;
					objResult.error = errorObj;
					
					return objResult;
				}
			}		
		}
		
		return objResult;
	},
		
	//Logging interface for PO
	log : function(ediType, sourceTransaction, recordId, dataIn, ediStatus, ediResponse, callFlag, linkedInboundIRMessage){
	
		var ediNumber = SCRIPTCONFIG.getScriptConfigValue('Factory PO: EDI Number');

		LogCreation(ediType, ediNumber, sourceTransaction, recordId, dataIn, ediStatus, ediResponse, callFlag, linkedInboundIRMessage); //Call to ERP_WTKA_Library.js
	
	},
	
	//Translates status to equivalent ID based on the custom list 'WTKA EDI Status'
	getLogStatusID : function(stStatus){
		switch(stStatus.toUpperCase()){
			case 'SUCCESS':
				return 1;
			case 'FAILURE':
				return 2;
			case 'EXCEPTION':
			case 'ERROR':
				return 3;
			default:
				return '';
		}		
	},
	
	//Sends error mail
	sendIntegrationErrorMail : function(dataIn, subject, orderNumber, errorObj, invalid_orderid){
		
		//Defaults
		var sendEmail = false; //TODO
		var ccList = null;
		var toList = ['3PL_eComm_Integration@kitandace.com'];	
		var emailAuthor = 243423;
		
		//Get email parameters from Script Config
		var stToList = SCRIPTCONFIG.getScriptConfigValue('3PL: Email Notification: To List');
		if(stToList) toList = JSON.parse(stToList);
		
		var stCCList = SCRIPTCONFIG.getScriptConfigValue('3PL: Email Notification: CC List');
		if(stCCList) ccList = JSON.parse(stToList);
		
		var stEmailAuthor = SCRIPTCONFIG.getScriptConfigValue('3PL: Email Notification: Author');
		if(stEmailAuthor) emailAuthor = stEmailAuthor;
		
		//Send Email
		var body = 'Hello,<br><br>';
		if(invalid_orderid == 'T')	body += 'Process failed in NetSuite. <br><b>Data Received from Mulesoft: </b><br>';
		else	body += 'Process against <b>' + orderNumber + '</b> failed in NetSuite. <br><b>Data Received from Mulesoft: </b><br>';
		body += '<br>' + JSON.stringify(dataIn) + '<br><br><br>';
		body += '<b>NetSuite Response: </b><br>';
		body += JSON.stringify(errorObj);
		body += '<br><br><br><br><br><br><br>Thanks';
		body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
		if(sendEmail)	nlapiSendEmail(emailAuthor, toList, subject, body, ccList);
	},
	
	//Returns error object
	generateErrorObject : function(stErrorMessage, objErrorDetails){
		var errorObj = {};
		errorObj.status = 'ERROR';
		errorObj.message = stErrorMessage;
		errorObj.details = objErrorDetails;
		return errorObj;
	},
	
	//Creates journal entry record for PO
	createJournalEntry : function(stPOTranId, stAmount, stSubsidiary, stCurrency, stFxRate, stToReverseJE){
		
		//Get Accounts from Script Config
		var stDebitAccount = SCRIPTCONFIG.getScriptConfigValue('Factory PO: Debit Account');
		var stCreditAccount = SCRIPTCONFIG.getScriptConfigValue('Factory PO: Credit Account');

		var stMemo = 'For Purchase Order #' + stPOTranId;
		if(stToReverseJE){
			stMemo += ' - Reversal for ' + stToReverseJE;
		}
		
		var objResult = {};
		objResult.success = true;
		
		try{
			
			var recJE = nlapiCreateRecord('journalentry');
			recJE.setFieldValue('subsidiary', stSubsidiary);
			recJE.setFieldValue('currency', stCurrency);
			recJE.setFieldValue('exchangerate', stFxRate);
			recJE.setFieldValue('trandate', nlapiDateToString(new Date()));
			recJE.setFieldValue('memo', stMemo);
			
			var stListName = 'line';
			//Debit line (or Credit line if reversal)
			recJE.selectNewLineItem(stListName);
			recJE.setCurrentLineItemValue(stListName, 'account', stDebitAccount);
			recJE.setCurrentLineItemValue(stListName, stToReverseJE ? 'credit' : 'debit', stAmount);
			recJE.commitLineItem(stListName);
			//Credit line (or Debit line if reversal)
			recJE.selectNewLineItem(stListName);
			recJE.setCurrentLineItemValue(stListName, 'account', stCreditAccount);
			recJE.setCurrentLineItemValue(stListName, stToReverseJE ? 'debit' : 'credit', stAmount);
			recJE.commitLineItem(stListName);
					
			objResult.journalid = nlapiSubmitRecord(recJE);
			
		}catch(error){
			var errorObj = {};					
			errorObj.status = 'Exception';
			errorObj.orderid = stPOTranId;		
			errorObj.message = 'Error in creating Journal Entry: ' + error.toString();
			
			objResult.success = false;
			objResult.error = errorObj;
		}
		
		return objResult;
	},
	
	//Update Transaction Links of Inbound Message 
	updateInboundMessageTranLinks : function(stTranHistory, stTranKey, stId, stTranId){	
	
		nlapiLogExecution('debug', 'Old Processed Tran value', JSON.stringify(stTranHistory));
	
		if(!stTranHistory || stTranHistory == 'undefined'){
			stTranHistory = '{}';
		}
		
		var objTranHistory = JSON.parse(stTranHistory);
		objTranHistory[stTranKey] = {id: stId, tranid: stTranId};
		stTranHistory = JSON.stringify(objTranHistory);
		
		nlapiLogExecution('debug', 'New Processed Tran value', JSON.stringify(stTranHistory));
		
		return stTranHistory;
	},
	
	//Returns Inbound IR Message records for the PO Id
	searchInboundIRMessage : function(poId, irId){
		
		var arFilters = [new nlobjSearchFilter('isinactive', null, 'is', 'F'),
						new nlobjSearchFilter('custrecord_itemreceipt_po', null, 'anyof', poId)];
		if(irId){
			arFilters.push(new nlobjSearchFilter('custrecord_itemreceipt_transformed_ir', null, 'anyof', irId));
		}
		
		var arResults = nlapiSearchRecord('customrecord_erp_3pl_inbound_itemreceipt', null, arFilters,
					[new nlobjSearchColumn('internalid').setSort(true),					//Descending
					 new nlobjSearchColumn('custrecord_itemreceipt_po'),
					 new nlobjSearchColumn('custrecord_itemreceipt_processed_tran')]);  //Add columns as needed
					 
		nlapiLogExecution('debug', 'searchInboundIRMessage', 'poId: ' + poId + ' | ' + 'irId: ' + irId);
		nlapiLogExecution('debug', 'searchInboundIRMessage', 'arResults: ' + JSON.stringify(arResults));
					 
		//Search again if Item Receipt ID parameter is present but search cannot find any staging record for it.
		//This may happen if the IR is not yet linked to the staging record by the time this search was performed.
		//e.g. During ICPO creation triggered by UE script on transform of PO to IR (ERP_TransferPrice_IR_CreateIntercoPO.js)
		if(irId && (!arResults || arResults.length == 0)){
			arResults = this.searchInboundIRMessage(poId); //Search with only the PO Id this time
		}
					
		return arResults;
	},
	
	//Returns an array of transaction IDs linked to the Inbound IR Message record
	getTransactionLinkIds : function(stProcessedTrans){
		//Get IDs of processed transactions
		var arIds = [];
		if(stProcessedTrans){
			var objProcTrans = JSON.parse(stProcessedTrans);
			for(var tran in objProcTrans){
				if(objProcTrans[tran].id){					
					arIds.push(objProcTrans[tran].id);	
				}
			}
		}
		
		//Only return valid transactions
		var arFinalIds = [];
		var arResults = nlapiSearchRecord('transaction', null,
						[new nlobjSearchFilter('mainline', null, 'is', 'T'),
						 new nlobjSearchFilter('internalid', null, 'anyof', arIds)],
						[new nlobjSearchColumn('internalid').setSort()]);
		if(arResults){
			for(var i = 0; i < arResults.length; i++){	
				if(arFinalIds.indexOf(arResults[i].getValue('internalid')) < 0){				
					arFinalIds.push(arResults[i].getValue('internalid'));
				}
			}
		}
		
		return arFinalIds;
	},
	
	//Returns Inbound IR Message JSON
	getInboundMessage : function(stFileId, stInboundMsg){
		
		//Get JSON from the file specified in Inbound Message File field 
		if(stFileId){
			var objFile = nlapiLoadFile(stFileId);
			var stFileContents = objFile.getValue();
			if(stFileContents){
				stInboundMsg = objFile.getValue();
			}		
		} 
		
		return stInboundMsg;
	},
	
	//Returns an object containing Qty received per SKUId from Inbound IR Message
	getInboundMessageReceivedItems : function(stInboundMsg){
		var objItems = {};
		var objSkuIdPerLine = {};
		
		if(stInboundMsg){
			var objPO = JSON.parse(stInboundMsg);
			objPO = objPO.purchaseOrder;
			nlapiLogExecution('debug', 'objPO', JSON.stringify(objPO));
			
			if(objPO){
				//Store SKUIds
				if(objPO.orderDetail && objPO.orderDetail.length > 0){
					for(var i = 0; i < objPO.orderDetail.length; i++){				
						var objLineItem = objPO.orderDetail[i];				
						if(objLineItem.orderLineNumber && objLineItem.item){
							objSkuIdPerLine[objLineItem.orderLineNumber] = objLineItem.item.SKUId;
						}				
					}			
				}
				nlapiLogExecution('debug', 'objSkuIdPerLine', JSON.stringify(objSkuIdPerLine));
				
				//Store quantityReceived
				if(objPO.shipment && objPO.shipment.length > 0){			
					var objShipment = objPO.shipment[0];			
					if(objShipment.shipmentDetail && objShipment.shipmentDetail.length > 0){
						for(var i = 0; i < objShipment.shipmentDetail.length; i++){
							var objLineShipmentDetail = objShipment.shipmentDetail[i];
							if(objSkuIdPerLine[objLineShipmentDetail.orderDetailId]){
								objItems[objSkuIdPerLine[objLineShipmentDetail.orderDetailId]] = objLineShipmentDetail.quantityReceived;
							}					
						}
					}
				}
			}
		}
		
		nlapiLogExecution('debug', 'objItems', JSON.stringify(objItems));
		return objItems;
	},
	
	//Returns an object containing Qty received per Line No. from Inbound IR Message
	getInboundMessageReceivedItemsPerLine : function(stInboundMsg, stShipmentId){
		var objQtyPerLine = {};
		
		if(stInboundMsg){
			var objPO = JSON.parse(stInboundMsg);
			objPO = objPO.purchaseOrder;
			nlapiLogExecution('debug', 'objPO', JSON.stringify(objPO));
			
			if(objPO){
				//Store quantityReceived
				if(objPO.shipment && objPO.shipment.length > 0){

					//Get target shipment object based on shipmentId
					var objShipment;
					for(var i = 0; i < objPO.shipment.length; i++){
						if(objPO.shipment[i].shipmentHeader && objPO.shipment[i].shipmentHeader.shipmentId == stShipmentId){
							objShipment = objPO.shipment[i];
						}
					}
					
					//Process shipmentDetail
					if(objShipment.shipmentDetail && objShipment.shipmentDetail.length > 0){
						for(var i = 0; i < objShipment.shipmentDetail.length; i++){
							var objLineShipmentDetail = objShipment.shipmentDetail[i];
							
							//Store quantityReceived per line number
							if(objLineShipmentDetail.orderDetailId){
								//Extract Line No.
								var arSplit = objLineShipmentDetail.orderDetailId.split("_");
								if(arSplit && arSplit.length > 1){
									var stLineNo = arSplit[1];
									
									//Ex. objQtyPerLine["1"] = 10
									objQtyPerLine[stLineNo] = objLineShipmentDetail.quantityReceived;	
								}
							}									
						}
					}
				}
			}
		}
		
		nlapiLogExecution('debug', 'objQtyPerLine', JSON.stringify(objQtyPerLine));
		return objQtyPerLine;
	},
	
	//Updates Inbound IR Message fields, linking transactions to Inbound IR Message record
	linkToInboundIRMessage : function(stIRMsgId, stProcessedTrans){		
		if(stIRMsgId){	

			var poId;
		
			//Get IDs of processed transactions
			var arIds = this.getTransactionLinkIds(stProcessedTrans);
			
			//Surround with try-catch as this is susceptible to timing issues (e.g. record collision)
			try{
				
				//Set Inbound IR Message field values
				var recInboundIRMsg = nlapiLoadRecord('customrecord_erp_3pl_inbound_itemreceipt', stIRMsgId);
				recInboundIRMsg.setFieldValues('custrecord_itemreceipt_linkedtransaction', arIds);			//multi-select
				recInboundIRMsg.setFieldValue('custrecord_itemreceipt_processed_tran', stProcessedTrans); 	//text
				
				poId = recInboundIRMsg.getFieldValue('custrecord_itemreceipt_po');
				
				nlapiSubmitRecord(recInboundIRMsg);
				
			} catch(error){
				
				nlapiLogExecution('debug', 'linkToInboundIRMessage error', error.toString());
				
				var LogDirection = 1; //2 - Outbound Call, 1 - Inbound call
				this.log(LogDirection, poId ? poId : 0, 0, 'Updating staging record', this.getLogStatusID('ERROR'), error.toString(), 'F', stIRMsgId);
			}
		}
	},
	
	//Returns ICPO Transaction No.
	getICPOTranNumber : function(stOrigPOId){
		
		var intCountIR = 0;
		var stOrigPOTranId = '';
		var stICPOTranId = '';
		
		//Prefix
		var stPrefix = 'N'; //Default
		var stSCPrefix = SCRIPTCONFIG.getScriptConfigValue('Factory PO: ICPO: Prefix');
		
		//Search Item Receipts of Orig PO
		if(stOrigPOId){
			var arIR = nlapiSearchRecord('itemreceipt', null,
						[new nlobjSearchFilter('mainline', null, 'is', 'T'),
						 new nlobjSearchFilter('createdfrom', null, 'anyof', stOrigPOId)],
						[new nlobjSearchColumn('internalid'),
						 new nlobjSearchColumn('tranid', 'createdfrom')]);
						 
			if(arIR){
				for(var i = 0; i < arIR.length; i++){
					intCountIR++;
					stOrigPOTranId = arIR[i].getValue('tranid', 'createdfrom');
				}
			}
			
			//Create Transaction No. for ICPO
			//Intercompany PO# will be created with the prefix: N and followed by the original PO number
			stICPOTranId = (stSCPrefix ? stSCPrefix : stPrefix) + stOrigPOTranId; //ex. N000246203
			
			//If a secondary item receipt is created from the original PO, the ICPO will be named with prefix: N + Original PO number + _1
			//Example: Original PO Number: 000246203 ICPO Number: N000246203 Second ICPO number: N000246203_1 Third ICPO Number: N000246203_2
			if(intCountIR > 1){
				stICPOTranId += '_' + (intCountIR - 1); //ex. N000246203_1
			} 
		}
		
		nlapiLogExecution('debug', 'getICPOTranNumber', stICPOTranId);
		return stICPOTranId;
	},
	
	//Checks if Channel is valid for Factory PO processing
	isValidChannel : function(stChannelText, stSCChannels){
		if(stChannelText && stSCChannels){
			
			nlapiLogExecution('debug', 'isValidChannel', 'PO Channel: ' + stChannelText);
			nlapiLogExecution('debug', 'isValidChannel', 'Valid CHANNELS: ' + stSCChannels);
			
			var arChannels = JSON.parse(stSCChannels);
			if(arChannels.indexOf(stChannelText) >= 0){
				return true;
			}
		}
		return false;
	},
	
	//Checks if Location is valid for Factory PO processing
	isValidLocation : function(stLocation, stSCFromLocations){
		if(stLocation && stSCFromLocations){
			
			var stLocCode = nlapiLookupField('location', stLocation, 'externalid');
			nlapiLogExecution('debug', 'isValidLocation', 'stLocation: ' + stLocation);
			nlapiLogExecution('debug', 'isValidLocation', 'stLocCode: ' + stLocCode);
			nlapiLogExecution('debug', 'isValidLocation', 'Valid FROM_LOCATIONS: ' + stSCFromLocations);
			
			var arLocations = JSON.parse(stSCFromLocations);
			if(arLocations.indexOf(stLocCode) >= 0 || arLocations.indexOf(parseInt(stLocCode)) >= 0){
				return true;
			}
		}
		
		return false;
	},
	
	//Checks if Ship To (Customer) > Location is valid for Factory PO processing
	isValidShipToLocation : function(stShipTo, stSCToLocations){
		if(stShipTo){		
			var arResult = nlapiSearchRecord('customer', null,
							[new nlobjSearchFilter('internalid', null, 'is', stShipTo)],
							[new nlobjSearchColumn('custentity_ka_location'),
							 new nlobjSearchColumn('externalid', 'custentity_ka_location')]);
							 
			if(arResult && arResult.length > 0){
				var stShipToLoc = arResult[0].getValue('externalid', 'custentity_ka_location');
				nlapiLogExecution('debug', 'isValidShipToLocation', 'stShipTo: ' + stShipTo);
				nlapiLogExecution('debug', 'isValidShipToLocation', 'stShipToLoc: ' + stShipToLoc);
				nlapiLogExecution('debug', 'isValidShipToLocation', 'Valid TO_LOCATIONS: ' + stSCToLocations);
				
				if(stShipToLoc && stSCToLocations){
					var arToLocs = JSON.parse(stSCToLocations);
					if(arToLocs.indexOf(stShipToLoc) >= 0 || arToLocs.indexOf(parseInt(stShipToLoc)) >= 0){
						return true;
					}
				}
			}
		}
		
		return false;
	}

};