/**
 *@NApiVersion 2.x
 *@NScriptType MapReduceScript
 */

//File: ERP_3PLProcessPurchaseOrders_MR.js 

define(['N/search',
        'N/log',
        'N/record',
        '../ERP_Utility/lib_script_config',
        '../ERP_Utility/lib_ka_functions'
        ],
    po_mass_outbound);


function po_mass_outbound(search, log, record, lib_script_config, lib_ka_functions){
    
	function getInputData()
    {
		log.debug('Getting Input Data...');		
		
        var poSearch = search.load({
            id: 'customsearch_po_3pl_export_search' //Run Saved search: 'POs to Export to 3PL'
        });

		return poSearch;
    }	

    function map(context)
    {
        var searchResult = JSON.parse(context.value);
    	var poId = searchResult.id;
        var poName = searchResult.values.tranid;

    	log.debug('Mapping) poId: ' + poId + ', pdName: ' + poName);
    	
    	context.write(poId, poName); //Key-Value pair
    }

    function reduce(context)
    {
    	log.debug('Reducing...');

        var scobj = lib_script_config.createInstance();
        var STATUS_SEND = scobj.getScriptConfigValue('Export to 3PL Status: Send');
    	
    	var poId = context.key; 
    	
    	var id = record.submitFields({
    		type: record.Type.PURCHASE_ORDER,
    		id: poId,
    		values: {
    			custbody_export_3pl_status: STATUS_SEND
    		},
    		options: {
    			enableSourcing: false,
    	        ignoreMandatoryFields : true
    		}
    	});
    }

    function summarize(summary)
    {
    	log.debug('Summarizing...');
        var type = summary.toString();
        log.debug(type + ' Usage Consumed', summary.usage);
        log.debug(type + ' Number of Queues', summary.concurrency);
        log.debug(type + ' Number of Yields', summary.yields);
        log.debug('...Done!');
    }

    return {
        getInputData: getInputData,
        map: map,
        reduce: reduce,
        summarize: summarize
    };
}

