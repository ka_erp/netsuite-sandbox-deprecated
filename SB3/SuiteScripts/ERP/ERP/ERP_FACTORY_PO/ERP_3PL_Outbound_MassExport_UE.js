/**
 *	File Name		:	ERP_3PL_Outbound_MassExport_UE.js
 *	Function		:	Checks if the field 'Export 3PL Status' (custbody_export_3pl_status) has been set.
 *						If YES, then there is a call to Mulesoft/Cloudhub sending the record ID to integrate the 
 *						record with the 3PL system
 *	Prepared by		:	christopher.neal@kitandace.com, 
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{	
	STATUS_READY= SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Ready');
	STATUS_SEND = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Send');
	STATUS_SENT = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Sent'); 	
    STATUS_ACCEPTED = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Accepted');
    STATUS_REJECTED = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Rejected');
    STATUS_ERROR = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Error');

 	sendEmail = true; //needed in WTKA_Library.js
}

function afterSubmit_performExport(type){

	nlapiLogExecution('DEBUG', 'context', nlapiGetContext().getExecutionContext());
	nlapiLogExecution('DEBUG', 'type', type); 

	if (type == 'xedit'){

		var exportStatus = nlapiGetFieldValue('custbody_export_3pl_status'); //check if the field EXPORT TO 3PL is set
		nlapiLogExecution('DEBUG', 'exportStatus', exportStatus); 

		if (exportStatus == STATUS_SEND){
			nlapiLogExecution('DEBUG', 'send to 3PL', 'send to 3PL'); 

	  		var tranId = nlapiLookupField(nlapiGetRecordType(), nlapiGetRecordId(), 'tranid');	  		
	  		var status =  nlapiLookupField(nlapiGetRecordType(), nlapiGetRecordId(), 'status');

	  		try {
	  			var recordValues = InvokeCloudhub(nlapiGetRecordType(), nlapiGetRecordId(), tranId, status); 	//call to send to Record ID to Mulesoft/Cloudehub to integrate the record.
																												//call to ERP_WTKA_Library.js

				nlapiLogExecution('DEBUG', 'recordValues', recordValues);

				if (recordValues == 'T'){
					nlapiLogExecution('DEBUG', 'SUCCESS', 'Message sent to Cloudhub successfully for transaction ' + tranId); 
					nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'custbody_export_3pl_status', STATUS_SENT);
		        } else {	            
		            nlapiLogExecution('DEBUG', 'FAIL', 'Message to Cloudhub failed. Please check Integration logs. Transaction ' + tranId);
		            nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'custbody_export_3pl_status', STATUS_ERROR);
				}
	  		} catch (error) {
	  			//nlapiLogExecution('DEBUG', 'FAIL', 'Message to Cloudhub failed. Please check Integration logs. Transaction ' + tranId '. ' + error.toString());
		        nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'custbody_export_3pl_status', STATUS_ERROR);
	  		}				
		}
	}
}
