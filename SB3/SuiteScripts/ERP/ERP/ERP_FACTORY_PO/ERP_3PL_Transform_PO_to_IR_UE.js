/**
 *	File Name		:	ERP_3PL_Transform_PO_to_IR_UE.js
 *	Function		:	Triggers the transform of Factory PO to IR, and creation of Reversal Journal Entry
 * 	Remarks			:	
 *	Prepared by		:	christopher.neal@kitandace.com, apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{	
	var STATUS_TOPROCESS = SCRIPTCONFIG.getScriptConfigValue('IR Processed Status: To Process');
	
	var LogDirection = 1; //2 - Outbound Call, 1 - Inbound call
}

/**
 * AFTER SUBMIT
 */
function afterSubmit_performTransform(type){

	nlapiLogExecution('DEBUG', 'context', nlapiGetContext().getExecutionContext());
	nlapiLogExecution('DEBUG', 'type', type); 

	if (type == 'xedit' || type == 'create'){

		var exportStatus = nlapiGetFieldValue('custrecord_itemreceipt_status'); //check if the STATUS field is set to Ready
		nlapiLogExecution('DEBUG', 'exportStatus', exportStatus); 

		if (exportStatus == STATUS_TOPROCESS){
			
			var recId = nlapiGetRecordId();
			var rec = nlapiLoadRecord(nlapiGetRecordType(), recId);
			var poId = rec.getFieldValue('custrecord_itemreceipt_po');
			var irId = rec.getFieldValue('custrecord_itemreceipt_transformed_ir');
			var fileId = rec.getFieldValue('custrecord_itemreceipt_inboundfile');
				
			//Call suitelet to transform the PO and create RJE
			//This can trigger the existing UE script for the creation of Intercompany PO (ERP - TransferPrice CreateInterCoPO UE)
			transformPOtoIR_SL(poId, recId, fileId, irId);
		}
	}
}

/**
 * Calls suitelet to transform PO to IR
 */
function transformPOtoIR_SL(poId, recId, stFileId, irId){
	
	try{
		var stUrl = nlapiResolveURL('SUITELET', 'customscript_erp_3pl_transform_po_sl', 'customdeploy_erp_3pl_transform_po_sl', true);
		nlapiLogExecution('DEBUG', 'transformPOtoIR_SL', 'URL: ' + stUrl);	
			
		var params = new Array();
		params['poid'] = poId;
		params['imid'] = recId;
		params['fileid'] = stFileId;
		params['irid'] = irId;
		
		nlapiLogExecution('DEBUG', 'poid', poId); 
		nlapiLogExecution('DEBUG', 'imid', recId); 
		nlapiLogExecution('DEBUG', 'fileid', stFileId); 
		nlapiLogExecution('DEBUG', 'irid', irId); 
						
		var objResponse = nlapiRequestURL(stUrl, params);
		nlapiLogExecution('DEBUG', 'transformPOtoIR_SL', 'objResponse.getCode(): ' + objResponse.getCode());	
		if(objResponse.getCode() == '200'){
			var stBody = objResponse.getBody();
			nlapiLogExecution('DEBUG', 'transformPOtoIR_SL', 'Response: ' + stBody);
		}
		
	} catch(error){
		
		nlapiLogExecution('DEBUG', 'transformPOtoIR_SL', 'error: ' + error.toString());
		
		//Logging
		FactoryPOLib.log(LogDirection, poId, 0, 'Calling Suitelet ERP_3PL_Transform_PO_to_IR_UE', FactoryPOLib.getLogStatusID('ERROR'), 
			error.toString(), 'F');
	}
}