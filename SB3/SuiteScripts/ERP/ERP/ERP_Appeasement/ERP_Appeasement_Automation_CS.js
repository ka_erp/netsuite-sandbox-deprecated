/**
 * Appeasement form automation 
 *
 * Version    Date            Author           Remarks
 * 1.00       29 Jul 2016     apple.villanueva
 *
 */
 
 {
	var IS_APPEASEMENT_FORM;
	var APPEASEMENT_ACCOUNT;
	var APPEASEMENT_LOCATIONS;
 }
 
/**
 * PAGE INIT
 * @param type Access Mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit_Appeasement(type){
	
	//Get Script Config values via suitelet
	var stUrl = nlapiResolveURL('SUITELET', 'customscript_erp_appeasement_auto_sl', 'customdeploy_erp_appeasement_auto_sl') 
				+ '&act=scriptconfig';
	var reply = nlapiRequestURL(stUrl);
	if(reply.getCode() == '200'){
		var stBody = reply.getBody();
		if(stBody){
			var objScriptConfig = JSON.parse(stBody);
			
			if(isAppeasementForm(objScriptConfig['form'])){
				IS_APPEASEMENT_FORM = true;
				APPEASEMENT_ACCOUNT = objScriptConfig['account'];
				APPEASEMENT_LOCATIONS = objScriptConfig['location'];
			}
		}
	}		
	
	if(IS_APPEASEMENT_FORM){
		
		//Default Account field
		setAccount();
		
		//Default Credit Card fields
		setCreditCard();
	}
}

/**
 * FIELD CHANGED
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged_Appeasement(type, name, linenum){ 

	//Customer
	if(name == 'entity'){
		if(IS_APPEASEMENT_FORM){			
			//Default Account
			setAccount();
		}
	}
	//Location
	else if(name == 'location'){
		if(IS_APPEASEMENT_FORM){			
			var stObjLocs = APPEASEMENT_LOCATIONS;
			var stLocNames = '';
			var arLocIds = [];
			if(stObjLocs){				
				var objLocs = JSON.parse(stObjLocs);
				var bIsFirst = true;
				
				for(var loc in objLocs){
					arLocIds.push(objLocs[loc]);
					
					if(!bIsFirst){
						stLocNames += ', ';
					}
					
					stLocNames += loc;
					
					bIsFirst = false;
				}
			}
			
			//Show error message if wrong location was selected
			if(nlapiGetFieldValue('location') && arLocIds.indexOf(nlapiGetFieldValue('location')) < 0){
				alert('Please select from the following locations: ' + stLocNames);
				nlapiSetFieldValue('location', '', false, true); //Clear location
			}
		}
	}
}

/**
 * SAVE RECORD
 * @returns {Boolean}
 */
function clientSaveRecord_Appeasement(){	
	if(IS_APPEASEMENT_FORM){
		
		//Default Account field
		setAccount();
		
		//Default Credit Card fields
		setCreditCard();
	}
		
	return true;
}

/**
 * Sets Account field
 */
function setAccount(){	
	//Set default to Account field
	if(APPEASEMENT_ACCOUNT && nlapiGetFieldValue('account') != APPEASEMENT_ACCOUNT){
		nlapiSetFieldValue('account', APPEASEMENT_ACCOUNT);
	}
}

/**
 * Sets Account field
 */
function setCreditCard(){	
	//Set defaults to Credit Card fields
	nlapiSetFieldValue('ccapproved', 'T');
	nlapiSetFieldValue('chargeit', 'F');
}

/**
 * Checks if the form is an Appeasement form
 */
function isAppeasementForm(stSCAppeasementForms){

	if(stSCAppeasementForms){				
		var arAppeasementForms = JSON.parse(stSCAppeasementForms);
		
		for(var i = 0; i < arAppeasementForms.length; i++){
			if(arAppeasementForms[i] == nlapiGetFieldValue('customform')){
				return true;
			}
		}
	}
	
	return false;
}