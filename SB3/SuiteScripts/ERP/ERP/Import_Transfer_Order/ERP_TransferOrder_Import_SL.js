/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       31 Aug 2015     Ivan
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function main_TransferOrder_Import_SL(request, response){
	
	//if (request.getMethod() == 'POST'){
	nlapiLogExecution('DEBUG', 'Transfer Order Staging', 'enter this'); 
	var transID = request.getParameter( 'transactionRecord' );
	var stagingID = request.getParameter( 'stagingLink' );
	
	try{
		
		if(transID){

		    Log.a("main_TransferOrder_Import_SL", "before calling " +  new Date().toLocaleString()); 		  
			//nlapiSubmitField('transferorder', transID ,'memo', 'link: ' + stagingID);		
			//nlapiSubmitField('transferorder', transID ,'custbody_ka_process_execution', 'T');	

 var loadTO = nlapiLoadRecord('transferorder', transID );
loadTO.setFieldValue('custbody_ka_process_execution', 'T');
nlapiSubmitRecord(loadTO ); 

			nlapiSetRedirectURL('RECORD', 'transferorder', transID, false);			
		    Log.a("main_TransferOrder_Import_SL", "after calling " +  new Date().toLocaleString()); 
		  			
		}
		
	}catch(ex){
		Log.d("createTransferOrder", "Unexpected error createTransferOrder:  " + ex);
        response.write(e)
	}
	//}
	
}



function isNullOrEmpty(valueStr){
    return(valueStr == null || valueStr == "" || valueStr == undefined);
}


function oldMain(){ // Function was meant to create Transfer Order but the execution has failed so this is did not push through due to Governance
	
		nlapiLogExecution('DEBUG', 'Transfer Order Staging', 'enter this'); 
		var transID = request.getParameter( 'transactionRecord' );	
		
		nlapiLogExecution('DEBUG', 'Transfer Order Staging ID', transID); 		
		response.write('transfer order staging transID '+transID + '\n');
				
		try{
			
			if(transID){
				
				var recTOHeadStaging = nlapiLoadRecord('customrecord_tran_order_staging_header', transID);
				var checkIfPOWasCreated = recTOHeadStaging.getFieldValue('custrecord_transfer_order'); 
				
				nlapiLogExecution('DEBUG', 'Transfer Order Staging - checkIfPOWasCreated', checkIfPOWasCreated); 
				
				if(isNullOrEmpty(checkIfPOWasCreated)){										
					
					createTransferOrder = recTOHeadStaging.getFieldValue('custrecord_erp_create_transfer_order'); 				
					
					if(createTransferOrder == 'T'){
					var id = "";
					
					if(recTOHeadStaging){
					
					
						   var transferOrder = nsdal.createObject("transferorder", TransferOrderProperties)
						   .withSublist("item", TransferOrderLineProperties);
				
						   transferOrder.trandate = recTOHeadStaging.getFieldValue('custrecord_transfer_stage_date');   
						   transferOrder.location = recTOHeadStaging.getFieldValue('custrecord_transfer_from_location');  
						   transferOrder.transferlocation = recTOHeadStaging.getFieldValue('custrecord_transfer_to_location');
						   transferOrder.department = recTOHeadStaging.getFieldValue('custrecord_transfer_department');      
						   transferOrder.employee =  recTOHeadStaging.getFieldValue('custrecord_transfer_employee');      
						   transferOrder.subsidiary =  recTOHeadStaging.getFieldValue('custrecord_transfer_subsidiary');   
						   transferOrder.memo =  recTOHeadStaging.getFieldValue('custrecord_transfer_reference_number');   
						   transferOrder.custbody_ka_order_type =  recTOHeadStaging.getFieldValue('custrecord_transfer_type');
				
						   Log.d("ProcessTransfers - Creating Transfer Order", "Date:  " + recTOHeadStaging.getFieldValue('custrecord_transfer_stage_date'));
							   
							//   Log.d("ProcessTransfers - Creating Transfer Order", "Date:  " + results[i].getValue('custrecord_transfer_stage_date'));   
							//   Log.d("ProcessTransfers - Creating Transfer Order", "From Location:  " + results[i].getValue('custrecord_transfer_from_location'));
							//   Log.d("ProcessTransfers - Creating Transfer Order", "To Location:  " + results[i].getValue('custrecord_transfer_to_location'));
							//   Log.d("ProcessTransfers - Creating Transfer Order", "Dept:  " + results[i].getValue('custrecord_transfer_department'));
							//   Log.d("ProcessTransfers - Creating Transfer Order", "Employee:  " + results[i].getValue('custrecord_transfer_employee'));
							//   Log.d("ProcessTransfers - Creating Transfer Order", "Subsidiary:  " + results[i].getValue('custrecord_transfer_subsidiary'));
							//   Log.d("ProcessTransfers - Creating Transfer Order", "Memo:  " + results[i].getValue('custrecord_transfer_reference_number'));
							//   Log.d("ProcessTransfers - Creating Transfer Order", "Transfer Type:  " + details[0].getValue('custrecord_transfer_type'));
							
							   	
							   var lineCount = recTOHeadStaging.getLineItemCount('recmachcustrecord_transfer_stage_header');	
							   Log.d("ProcessTransfers - Line", "lineCount:  " + lineCount);
							   var approveLines = recTOHeadStaging.getFieldValue('custrecord_erp_approve_lines');  
									
								for (var k = 1; k <= lineCount; k++){
																									 
									  recTOHeadStaging.selectLineItem('recmachcustrecord_transfer_stage_header', k);  				
									  var newLine = transferOrder.item.addLine();					  					   					  
									  				  
								      newLine.item = recTOHeadStaging.getCurrentLineItemValue('recmachcustrecord_transfer_stage_header', 'custrecord_transfer_stage_item');
								      newLine.quantity = recTOHeadStaging.getCurrentLineItemValue('recmachcustrecord_transfer_stage_header', 'custrecord_transfer_quantity');
								      newLine.expectedreceiptdate = recTOHeadStaging.getCurrentLineItemValue('recmachcustrecord_transfer_stage_header', 'custrecord_expected_receipt_date');
								      newLine.expectedshipdate = recTOHeadStaging.getCurrentLineItemValue('recmachcustrecord_transfer_stage_header', 'custrecord_transf_stage_expe_ship_date');
								      							      
								}
					
								 Log.d("ProcessTransfers - Line", "JSON transferOrder:  " + JSON.stringify(transferOrder));
								
								 id = transferOrder.save(true, true);
								 
								 if(id){
									 nlapiSubmitField('customrecord_tran_order_staging_header',transID, 'custrecord_transfer_order', id); 
								 }
								 
								 Log.d("createTransferOrder", "Transfer Order:  " + id);								 
								
						}
					}									
				}else{
 
				}					
			}																
		}catch(e){
	        Log.d("createTransferOrder", "Unexpected error createTransferOrder:  " + e);
	        response.write(e)
		}		

}