/**
 *	File Name		:	ERP_Hide_WTKAExternalOrders_List_UE.js
 *	Function		:	Hides the WTKA_External_Orders sublist
 * 	Remarks			:	
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

/*
*	name: beforeLoad_IntegrationLogs
*	descr: 	Hide WTKA_External_Orders sublist
*
*	author: apple.villanueva@kitandace.com
*	@param: {type} type of operation on the record
*   @param: {form} the form the script is deployed on 
*   @param: {request} the request sent to the script
*
*/
function beforeLoad_HideExternalOrdersList(type, form, request){
	
	if(nlapiGetContext().getExecutionContext() == 'userinterface' && (type == 'edit' || type == 'view')){
		
		var sublistToHide = form.getSubList('recmachcustrecord_external_transactions_links');
		
		if(sublistToHide){
			sublistToHide.setDisplayType('hidden');
		}
	}
	
}

