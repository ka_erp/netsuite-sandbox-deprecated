/**
 *	File Name		:	ERP_TransferOrder_RS.js
 *	Function		:	
 * 	Remarks			:	
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

function getTransferOrderObject(dataIn)  // REST API Inbound call for generating TO Canonical data object model
{
	nlapiLogExecution('DEBUG', 'dataIn', JSON.stringify(dataIn));
	var type 			= 'transferOrder';
	InboundOrderNumber  = dataIn.id;
	InboundOrderId 		= FetchOrderId(type, InboundOrderNumber);
	var status 			= Validate940(type, InboundOrderId); 
	if(status)
	{
		var record 	 	= nlapiLoadRecord(type, InboundOrderId);
		var CanonObject = generateTOCanonObject(record);
		nlapiLogExecution('DEBUG', 'Canonical Object', JSON.stringify(CanonObject)); 
		//var transferOrderLogs 	= LogCreation(2, 1, InboundOrderId, 0, dataIn, 1, CanonObject, 'T'); //2 - Outbound Call, 1 - Inbound call
		
		//For additional logging - START
		if(CanonObject.status == 'Exception'){
			//Exception occurred
			LogCreation(2, 1, InboundOrderId, 0, dataIn, 3, CanonObject, 'T'); //3-Exception
		} else {
			//Success Logging
			LogCreation(2, 1, InboundOrderId, 0, dataIn, 1, CanonObject, 'T'); //1-Success
		}
		//For additional logging - END
		
		return CanonObject;
	}
	else
	{
		var errorStatus = (ErrorObj.status == "Error") ? 2 : 3; // 2- Error, 3-Exception
		
		//For additional logging - START
		//Commented out so that invalid order will be logged also
		//if(invalid_orderid == 'F')
		//{
			var orderid 	= (InboundOrderId != null) ? InboundOrderId : 0;
			var ErrorLogs 	= LogCreation(2, 1, orderid, 0, dataIn, errorStatus, ErrorObj, 'F'); //2 - Outbound Call, 1 - Inbound call
			nlapiLogExecution('DEBUG', 'Error Object', JSON.stringify(ErrorObj));
		//}
		//For additional logging - END
		
		//Send Error Mail
		send3PLIntegrationErrorMail(dataIn, 'Outbound 940 call processing failure', InboundOrderNumber);
		
		return ErrorObj;
	}
}

function generateTOCanonObject(record) //Generation of Canonical data object model for TO
{
	try{
		var recordId 	= record.getId();
		/* Comment Out Reason: Values will be retrieved from Script Config
		var carriercode = (record.getFieldValue('shipcarrier') 	!= null) ? 	record.getFieldValue('shipcarrier') : record.getFieldValue('carrier');
		var carriername = record.getFieldText('shipmethod');
		carriername 	= (carriername.match(/FedEx/gi) != null) ? 'FedEx' : (carriername.match(/UPS/gi) != null) ? 'UPS' : 'FedEx'; //Default to FedEx if Ship method is not set
		*/
		
		//Script Config implementation
		var objShipInfo = getShippingInfo(record);
		
		var fromLocationData  	= getLocationData(record.getFieldValue('location'));
		var toLocationData  	= getLocationData(record.getFieldValue('transferlocation'));
		
		var transferOrderSchema 				  									= new Object();
		transferOrderSchema.transferOrder 		  									= new Object();
		transferOrderSchema.transferOrder.transferHeader  							= new Object();
		transferOrderSchema.transferOrder.transferHeader.transferId 				= readValue(record.getFieldValue('tranid'));
		transferOrderSchema.transferOrder.transferHeader.transferNumber 			= "";
		transferOrderSchema.transferOrder.transferHeader.fromLocation 		  		= new Object();
		transferOrderSchema.transferOrder.transferHeader.fromLocation.locationCode 	= readValue(fromLocationData.externalid); 
		transferOrderSchema.transferOrder.transferHeader.fromLocation.locationName 	= readValue(fromLocationData.name);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.addressLine1 	= readValue(fromLocationData.address1);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.addressLine2 	= readValue(fromLocationData.address2);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.city 			= readValue(fromLocationData.city);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.province 		= readValue(fromLocationData.state);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.country 		= readValue(fromLocationData.country);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.mailingCode 	= readValue(fromLocationData.zip);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.phoneNumber 	= readValue(fromLocationData.addrphone); 
		transferOrderSchema.transferOrder.transferHeader.fromLocation.latitude 		= 0.00000; 
		transferOrderSchema.transferOrder.transferHeader.fromLocation.longitude 	= 0.00000; 
		transferOrderSchema.transferOrder.transferHeader.fromLocation.openingDate 	= readValue(fromLocationData.custrecord_ka_store_opening_date);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.closedDate 	= "";
		transferOrderSchema.transferOrder.transferHeader.fromLocation.locationType 	= readValue(fromLocationData.custrecord_ra_loctype);
		transferOrderSchema.transferOrder.transferHeader.fromLocation.storeHours 	= "";
		transferOrderSchema.transferOrder.transferHeader.fromLocation.locStatus 	= (fromLocationData.isinactive == 'F') ? 'Open' : 'Closed';
		transferOrderSchema.transferOrder.transferHeader.fromLocation.DCLocCode 	= readValue(fromLocationData.externalid); 
		
		transferOrderSchema.transferOrder.transferHeader.toLocation 		  		= new Object();
		transferOrderSchema.transferOrder.transferHeader.toLocation.locationCode 	= readValue(toLocationData.externalid); 
		transferOrderSchema.transferOrder.transferHeader.toLocation.locationName 	= readValue(toLocationData.name);
		transferOrderSchema.transferOrder.transferHeader.toLocation.addressLine1 	= readValue(toLocationData.address1);
		transferOrderSchema.transferOrder.transferHeader.toLocation.addressLine2 	= readValue(toLocationData.address2);
		transferOrderSchema.transferOrder.transferHeader.toLocation.city 			= readValue(toLocationData.city);
		transferOrderSchema.transferOrder.transferHeader.toLocation.province 		= readValue(toLocationData.state);
		transferOrderSchema.transferOrder.transferHeader.toLocation.country 		= readValue(toLocationData.country);
		transferOrderSchema.transferOrder.transferHeader.toLocation.mailingCode 	= readValue(toLocationData.zip);
		transferOrderSchema.transferOrder.transferHeader.toLocation.phoneNumber 	= readValue(toLocationData.addrphone);
		transferOrderSchema.transferOrder.transferHeader.toLocation.latitude 		= 0.00000;
		transferOrderSchema.transferOrder.transferHeader.toLocation.longitude 		= 0.00000;
		transferOrderSchema.transferOrder.transferHeader.toLocation.openingDate 	= readValue(toLocationData.custrecord_ka_store_opening_date);
		transferOrderSchema.transferOrder.transferHeader.toLocation.closedDate 		= "";
		transferOrderSchema.transferOrder.transferHeader.toLocation.locationType 	= readValue(toLocationData.custrecord_ra_loctype);
		transferOrderSchema.transferOrder.transferHeader.toLocation.storeHours 		= "";
		transferOrderSchema.transferOrder.transferHeader.toLocation.locStatus 		= (toLocationData.isinactive == 'F') ? 'Open' : 'Closed';
		transferOrderSchema.transferOrder.transferHeader.toLocation.DCLocCode 		= readValue(toLocationData.externalid); 
		
		transferOrderSchema.transferOrder.transferHeader.transactionDt 		  	= readValue(record.getFieldValue('trandate'));
		transferOrderSchema.transferOrder.transferHeader.transferStatus 	 	= readValue(record.getFieldValue('status'));
		transferOrderSchema.transferOrder.transferHeader.transferClosedDt 	 	= "";
		transferOrderSchema.transferOrder.transferHeader.createTS 			  	= readValue(record.getFieldValue('createddate'));
		transferOrderSchema.transferOrder.transferHeader.lastModifiedTs 	  	= readValue(record.getFieldValue('lastmodifieddate'));
		transferOrderSchema.transferOrder.transferHeader.transferType 		  	= record.getFieldText('custbody_ka_order_type');
		transferOrderSchema.transferOrder.transferHeader.transferHeaderMemo   	= readValue(record.getFieldValue('memo'));
		transferOrderSchema.transferOrder.transferHeader.totalQuantityOrdered 	= parseInt(totalquantity);
		transferOrderSchema.transferOrder.transferHeader.totalAmountOrdered 	= parseFloat(record.getFieldValue('total')); 
		
		transferOrderSchema.transferOrder.transferDetail  = new Array();

		var totalItemsShipped = 0;
		var j=0;
		for(var i=0; i<record.getLineItemCount('item'); i++)
		{
			var itemType = record.getLineItemValue('item', 'itemtype', i+1);
			var itemInternalId = record.getLineItemValue('item', 'item', i+1); //get the Netsuite Internal ID of the item
			if(itemType == 'InvtPart' || itemType == 'NonInvtPart')
			{
				transferOrderSchema.transferOrder.transferDetail[j] 					= new Object();
				transferOrderSchema.transferOrder.transferDetail[j].transferId 			= readValue(recordId);
				transferOrderSchema.transferOrder.transferDetail[j].transferLineNumber	= record.getLineItemValue('item', 'id', 				  i+1);
				
				transferOrderSchema.transferOrder.transferDetail[j].item 			  	= new Object();
				transferOrderSchema.transferOrder.transferDetail[j].item.SKUId 			= record.getLineItemValue('item', 'custcol_wtka_upccode', 	i+1);

				var itemFullSkuCode =    record.getLineItemText('item', 'item', 	i+1);   ///"KM031052S : KM031052-10022-M"; //temp Ivan//nlapiLookupField('inventoryitem', itemInternalId, 'itemid'); //get the full SKU code (e.g. 'KM031052S : KM031052-10022-M')
				nlapiLogExecution('DEBUG', 'TEMP: itemFullSkuCode', itemFullSkuCode);

				var itemSkuCode =    itemFullSkuCode.split(":")[1]; //itemFullSkuCode.split(':'); //[1]; //perform split and get after the colon (e.g. ' KM031052-10022-M')
                itemSkuCode =  itemSkuCode.split(" ")[1]; 

				nlapiLogExecution('DEBUG', 'TEMP: itemSkuCode', itemSkuCode);			

				/*if (itemSkuCode[0] == ' '){
					itemSkuCode = itemSkuCode.substring(1); //remove 1st character from the string, it is a whitespace (e.g. 'KM031052-10022-M')					
				}*/
				
				nlapiLogExecution('DEBUG', 'DONE: itemSkuCode', itemSkuCode);

				transferOrderSchema.transferOrder.transferDetail[j].item.SKUCode = itemSkuCode;
				transferOrderSchema.transferOrder.transferDetail[j].item.SKUName   		= record.getLineItemValue('item', 'description', 			i+1);
				
				transferOrderSchema.transferOrder.transferDetail[j].countItemOrdered 		= parseInt(record.getLineItemValue('item', 'quantity', i+1));

				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice 						= new Object();
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.priceId 				= "";
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.currencyCode 			= readValue(record.getFieldValue('currencyname'));
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.amtCurrentPrice 		= parseFloat(record.getLineItemValue('item', 'rate', i+1));
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.amtPromoPrice 			= parseFloat(record.getLineItemValue('item', 'rate', i+1));
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.priceEffectiveStartDt 	= null;
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.priceEffectiveEndDt 	= null;
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.lastModifiedByUser 	= "";
				transferOrderSchema.transferOrder.transferDetail[j].amtItemPrice.lastModifiedTimestamp 	= "";
				transferOrderSchema.transferOrder.transferDetail[j].amtExtended 						= null; 
				transferOrderSchema.transferOrder.transferDetail[j].amtExtendedCost						= null; 
				transferOrderSchema.transferOrder.transferDetail[j].transferDetailMemo 					= "";
				totalItemsShipped += parseInt(record.getLineItemValue('item', 'quantityfulfilled',  i+1));
				j++;
			}
		}
		
		transferOrderSchema.transferOrder.shipment = new Array();
		transferOrderSchema.transferOrder.shipment[0]			 									= new Object();
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader 								= new Object();
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.shipmentId 					= readValue(recordId);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.shipmentCode 					= /*carriername*/ objShipInfo.shipmentCode;
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.shippingMethod 				= /*carriercode*/ objShipInfo.shippingMethod;
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.trackingNumber 				= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.shipmentStatus 				= "CR";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.countTotalShipped 				= totalItemsShipped;
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.countTotalReceivedInShipment 	= parseInt(record.getLineItemValue('item', 'quantityreceived',  i+1));
		if(!transferOrderSchema.transferOrder.shipment[0].shipmentHeader.countTotalReceivedInShipment)
		{
			transferOrderSchema.transferOrder.shipment[0].shipmentHeader.countTotalReceivedInShipment = 0;
		}
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.shipDt 						= readValue(record.getFieldValue('shipdate'));
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.shipReceivedDt 				= readValue(record.getFieldValue('custbody_ka_to_expected_recv_date')); 
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.expectedShipDate 				= readValue(record.getFieldValue('custbody_ka_to_expected_ship_date'));
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.handlingInstructions 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.carrierName 					= /*carriername*/ objShipInfo.carrierName;
		
		var expectedReceiveDate  = readValue(record.getFieldValue('custbody_ka_to_expected_recv_date'));
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.expectedReceiveDate 			= expectedReceiveDate ? moment.utc(moment(expectedReceiveDate)).format("YYYY-MM-DDTHH:MM:SSZ") : '';

		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient 						= new Object();
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.customerId 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.customerEmail 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.honorific 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.firstName 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.lastName 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.customerPhoneNumber  = "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.gender 				= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.birthDt 				= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.locale 				= "";
			
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress 							= new Object();
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.billingAddressId 			= ""; //readValue(fromLocationData.externalid); 
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.billingFirstName	 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.billingLastName 	 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.billingEmail 				= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.billingAltEmail 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.billingAddressLine1 		= ""; //readValue(fromLocationData.address1);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.billingAddressLine2		= ""; //readValue(fromLocationData.address2);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.poBox 					= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.postalCode 		 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.provinceName 				= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.provinceAltName 	 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.countryName 		 		= ""; //readValue(fromLocationData.country);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.countryAltName 	 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.billingAddress.inActiveDate 				= "";

		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress 							= new Object();
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.shippingAddressId 		= ""; //readValue(toLocationData.externalid); 
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.recipientFirstName		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.recipientLastName 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.recipientEmail 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.recipientAltEmail 	 	= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.recipientPhoneNumber 	= ""; //readValue(toLocationData.addrphone);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.shippingAddressLine1 	= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.shippingAddressLine2 	= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.poBox 				 	= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.cityName 		 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.postalCode 		 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.provinceName 	 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.provinceAltName 			= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.countryName 		 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.countryAltName 	 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.recipient.shippingAddress.inActiveDate 	 		= "";

		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation					= new Object();
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.locationCode 	= readValue(fromLocationData.externalid); 
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.locationName 	= readValue(fromLocationData.name);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.addressLine1 	= readValue(fromLocationData.address1);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.addressLine2 	= readValue(fromLocationData.address2);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.city 			= readValue(fromLocationData.city);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.province 		= readValue(fromLocationData.state);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.country 		= readValue(fromLocationData.country);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.mailingCode 	= readValue(fromLocationData.zip);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.phoneNumber 	= readValue(fromLocationData.addrphone);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.latitude 		= 0.00000;
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.longitude 		= 0.00000;
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.openingDate 	= readValue(fromLocationData.custrecord_ka_store_opening_date);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.closedDate 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.locationType 	= readValue(fromLocationData.custrecord_ra_loctype);
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.storeHours 		= "";
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.locStatus 		= (fromLocationData.isinactive == 'F') ? 'Open' : 'Closed';
		transferOrderSchema.transferOrder.shipment[0].shipmentHeader.fulfillingLocation.DCLocCode 		= readValue(fromLocationData.externalid);
		transferOrderSchema.transferOrder.shipment[0].shipmentDetail 									= new Array();		

		for(var i=0; i < record.getLineItemCount('item'); i++)
		{
			var itemType = record.getLineItemValue('item', 'itemtype', i+1);
			if(itemType == 'InvtPart' || itemType == 'NonInvtPart')
			{
				newDetail 					= new Object();
				newDetail.orderDetailId 	= readValue(record.getLineItemValue('item', 'id', 				 i+1));
				newDetail.quantityOrdered 	= parseInt(record.getLineItemValue('item', 	'quantity',			 i+1));
				newDetail.quantityShipped 	= parseInt(record.getLineItemValue('item', 	'quantityfulfilled', i+1));
				transferOrderSchema.transferOrder.shipment[0].shipmentDetail.push(newDetail);
			}
		}
	
	} catch(to_error){
		
		nlapiLogExecution('DEBUG','error message',to_error.message);
		ErrorObj.editype 	 				= 940;
		ErrorObj.orderid 					= recordId;
		ErrorObj.messages[0] 				= new Object();
		ErrorObj.messages[0].messagetype 	= "Transfer Order Error";
		ErrorObj.messages[0].message 	 	= "Error Details: " + to_error.message;
		
		//For additional logging - START
		ErrorObj.status = 'Exception';
		//For additional logging - END
		
		transferOrderSchema 				= ErrorObj;
		
	} 
	
	return transferOrderSchema;
}
