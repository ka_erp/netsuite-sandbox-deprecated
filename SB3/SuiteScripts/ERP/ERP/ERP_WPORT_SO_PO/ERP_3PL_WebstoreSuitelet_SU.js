/**
 *	File Name		:	ERP_3PL_WebstoreSuitelet_SU.js
 *	Function		:	
 * 	Remarks			:	
 *	Prepared by		:	christopher.neal@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/



/*
*	name: callButtonSuitelet
*	descr:  Function which is triggered when the 'Send to 3PL' button is pushed.
*			Calls a suitelet to send a message to the 3PL
*
*	author: Wipro Ltd.
*	updated: christopher.neal@kitandace.com
*	@param: {obj} request
*	@param: {obj} response
*
*/
function WebStoreLoadSuitelet(request,response){
	var recordValues = '';
	try
	{
		switch(request.getParameter('type')) {
			case 'custom':
				var RecType 	= request.getParameter('rectype');
				var RecId 		= request.getParameter('recid');
				var TranNo      = request.getParameter('tranno');
				var RecStatus 	= request.getParameter('stats');
				recordValues 	= InvokeCloudhub(RecType, RecId, TranNo, RecStatus); //call to WTKA_Library.js
				break;

			default:
				recordValues = '1';
				break;
		}
		response.write(recordValues);
	} catch(err) {
		nlapiLogExecution('Debug','Error', err);
	}
}