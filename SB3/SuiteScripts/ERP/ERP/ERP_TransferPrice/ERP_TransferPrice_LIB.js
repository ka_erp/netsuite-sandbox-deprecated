/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       23 Sep 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */


function calculateRate(baseRate, receiptCurrency, vendorCurrency,  strVendorRetailFormula){
	
	var rateArr = new Array();
	
	var fxRate = nlapiExchangeRate(receiptCurrency, vendorCurrency, nlapiDateToString(new Date()));	
	
	rateArr.push(fxRate); 
	
	if(!isNullOrEmpty(baseRate) && !isNullOrEmpty(strVendorRetailFormula)){
		
		rate = baseRate; 		
		rate = rate * fxRate *  strVendorRetailFormula;
		
		rateArr.push(rate); 
		
	}
	
	
	
	return rateArr; 
	
}


function getRetailMemo(rate, ratecurrency, fxrate, fxcurrency, vendormarkup, newrate){
	
	
	var memo = ""; 
				
	memo = "Calculation:\n FCA: " + rate + " " + ratecurrency 
	+ "\n FXrate: " + fxrate + " " +   fxcurrency 
	+ "\n Markup: " +  vendormarkup
	+ "\n New Rate: " + newrate ; 
	
	return memo; 
	
}


function validateTransferVRetail(retailprice, transferprice, override){
	
	var bValid = true;
	var strMessage = ''; 
	var objValidate = new Object(); 
	
	
	if(false) nlapiLogExecution('DEBUG', retailprice + ' > ' + transferprice + ' @ '  + i , retailprice > transferprice);
	
	
	if(retailprice < transferprice ){
		strMessage = 'ERROR: Issue: Retail < Transfer \n (' + retailprice + ' < ' +  transferprice + ')'; 
		bValid = false;
		
		if(override == 'T'){bValid = true; strMessage = 'WARNING: Override: Retail < Transfer \n (' + retailprice + ' < ' +  transferprice + ')';} 
				
	}else if(transferprice <= 0){
		strMessage = 'ERROR: Issue: Transfer Amount is <= 0'; 
		bValid = false;
		
		if(override == 'T'){bValid = true; strMessage = 'WARNING: Override: Transfer <= 0 \n (' + transferprice + ')';} 
				
	}else if(retailprice <= 0){
		strMessage = 'ERROR: Issue: Retail Amount is <= 0'; 
		bValid = false;
		
		if(override == 'T'){bValid = true; strMessage = 'WARNING: Override: Retail <= 0 \n (' + retailprice + ')';}				
	}
	
	
	else{
		strMessage = 'NO ISSUE' ;
	}
	
	
	//Set Memo
	objValidate.status = bValid;
	objValidate.message = strMessage; 
		
	return objValidate;
	
}





//Get Retail Price

//Get Retail Currency


//Validate the Retail vs. Transfer Price






function getItemResultObj(items, currency, pricelevel){ 			//Based on Price Level

	var strSavedSearchIDItm = null;
	var arSaveSearchFiltersItm = new Array();
	var arSavedSearchColumnsItm = new Array();
	var arSavedSearchResultsItm = null;

	var arrResultObjectArray = new Array();
	var arrIdObjectArray = new Array();

	var objItemResult = new Object();

	if(!isNullOrEmpty(items)){
		arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null , 'anyof', items ));
	}

	arSaveSearchFiltersItm.push(new nlobjSearchFilter( 'currency', 'pricing',  'anyof', currency));
	arSaveSearchFiltersItm.push(new nlobjSearchFilter( 'pricelevel', 'pricing',  'is', pricelevel));

	arSavedSearchColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('name'));
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('currency','pricing'));
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('pricelevel','pricing'));
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('unitprice','pricing'));

	arSavedSearchResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchColumnsItm);

	if(!isNullOrEmpty(arSavedSearchResultsItm)){

		//Build the result set object lookup and fill the Array Maps
		for (var j = 0 ; j < arSavedSearchResultsItm.length; j++){

			var obj = new Object();
			obj.id = arSavedSearchResultsItm[j].getValue('internalid');
			obj.name = arSavedSearchResultsItm[j].getValue('name');
			obj.salesprice = arSavedSearchResultsItm[j].getValue('unitprice','pricing');
			obj.currency = arSavedSearchResultsItm[j].getValue('currency','pricing');
			obj.pricelevel = arSavedSearchResultsItm[j].getValue('pricelevel','pricing');

			arrResultObjectArray.push(obj);
			arrIdObjectArray.push(obj.id);
		}

		nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : arrResultObjectArray", JSON.stringify(arrResultObjectArray));
		nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : arrIdObjectArray", JSON.stringify(arrIdObjectArray));

		objItemResult.ids = arrIdObjectArray;
		objItemResult.objs = arrResultObjectArray;

		return objItemResult;

	}else{
		return null;
	}


}

function convertCurrency( amount , currencyId, currencyObject){
	
	var ids = currencyObject.ids;
	var obj = currencyObject.obj;
	var conversion = amount; 
	
	if(!isNullOrEmpty(ids)){
		
		var index = ids.indexOf(currencyId);
		var curr = obj[index];	
		
		nlapiLogExecution('DEBUG', 'func:convertCurrency', 'currencyId:' +  currencyId + " "+ JSON.stringify(curr)); 
		
		conversion = curr.rate * amount;		 
		
	}
	
	return conversion;
	
}


function getRate(sourceRate,sourceRateCurrency,destinationCurrency,currencyObject, markup){
	
	var getRateObject = new Object();
	var conversion = sourceRate;
	
	var ids = currencyObject.ids;
	var obj = currencyObject.obj;
	
	var currencyId = sourceRateCurrency+">"+destinationCurrency;
	
	var finding = ids.indexOf(currencyId);
	
	nlapiLogExecution('DEBUG', 'Finding ' + currencyId, finding);
	
	if(finding > -1){
		
		var a = obj[finding];
		nlapiLogExecution('DEBUG', 'Finding ' + currencyId, JSON.stringify(a));
		
		
		conversion = a.rate * sourceRate;		
		conversion = conversion * markup;
		
		var memo = ""; 
		
		memo = "Calculation:\n FCA: " + sourceRate + " " + a.sourceTxt 
		+ "\n FXrate: " + a.rate + " " +   a.destinationTxt 
		+ "\n Markup: " +  markup
		+ "\n New Rate: " + conversion ; 
		
	}
	
	
	
	
	
	
	
	getRateObject.rate = conversion; 
	getRateObject.memo = memo; 
	
	
	
	
	return getRateObject;
	
}


function getItemFCAResultObj(items){
	
	var strSavedSearchIDItm = null;
	var arSaveSearchFiltersItm = new Array();
	var arSavedSearchColumnsItm = new Array();
	var arSavedSearchResultsItm = null;

	var arrResultObjectArray = new Array();
	var arrCurrencyArray = new Array();
	var tempCurrencyArray = new Array();
	var arrIdObjectArray = new Array();

	var objItemResult = new Object();

	if(!isNullOrEmpty(items)){
		arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null , 'anyof', items ));
	}
	
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('name'));
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('custitem_erp_vendorprice_last_fca'));
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('custitem_erp_vendorprice_last_currency'));

	arSavedSearchResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchColumnsItm);

	if(!isNullOrEmpty(arSavedSearchResultsItm)){

		//Build the result set object lookup and fill the Array Maps
		for (var j = 0 ; j < arSavedSearchResultsItm.length; j++){

			var obj = new Object();
			obj.id = arSavedSearchResultsItm[j].getValue('internalid');
			obj.name = arSavedSearchResultsItm[j].getValue('name');
			obj.salesprice = arSavedSearchResultsItm[j].getValue('custitem_erp_vendorprice_last_fca');
			obj.currency = arSavedSearchResultsItm[j].getValue('custitem_erp_vendorprice_last_currency');
			obj.currencyTxt = arSavedSearchResultsItm[j].getText('custitem_erp_vendorprice_last_currency');
					
			arrResultObjectArray.push(obj);
			arrIdObjectArray.push(obj.id);
			
			if(tempCurrencyArray.indexOf(obj.currency) < 0){
				var currObject = new Object();
				currObject.id  = obj.currency;
				currObject.name = obj.currencyTxt;
				
				arrCurrencyArray.push(currObject);
				tempCurrencyArray.push(obj.currency); 
			}
			
		}

		nlapiLogExecution("DEBUG", "getItemFCAResultObj : arrResultObjectArray", JSON.stringify(arrResultObjectArray));
		nlapiLogExecution("DEBUG", "getItemFCAResultObj : arrIdObjectArray", JSON.stringify(arrIdObjectArray));

		objItemResult.ids = arrIdObjectArray;
		objItemResult.objs = arrResultObjectArray;
		objItemResult.curr = arrCurrencyArray;

		return objItemResult;

	}else{
		return null;
	}
}

function getPOItemArray(){

	var arrP0Items = new Array();
	var intGetPOlineCount = nlapiGetLineItemCount('item');

	for(var i = 1; i <= intGetPOlineCount; i++){

		var idItemId = nlapiGetLineItemValue('item','item', i);
		var strItemType = nlapiGetLineItemValue('item','itemtype', i);

		if(strItemType == 'InvtPart'){
			arrP0Items.push(idItemId);
		}

		//nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : getPOItemArray: Item", strItemType);
	}


	nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : getPOItemArray", arrP0Items.toString());
	return arrP0Items;
}


function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}

function createCurrencyExchange(objSource, objCurrencyList){

	var fxResult = new Object();
	var currencyIds = new Array();
	var currencyFxObj = new Array();
		
	if(!isNullOrEmpty(objCurrencyList) && !isNullOrEmpty(objSource)){
		
		for(var c = 0 ; c < objCurrencyList.length; c++){
			
			
			
			//Source to List
			
			var fxObject = new Object
			var sourceCurrency = objSource.id;
			var sourceCurrencyText = objSource.name;
			var destinationCurrency = objCurrencyList[c].id;
			var destinationCurrencyText = objCurrencyList[c].name;
						
			var fxRate = nlapiExchangeRate(sourceCurrency, destinationCurrency, nlapiDateToString(new Date()));
			
			fxObject.source = sourceCurrency; 
			fxObject.sourceTxt = sourceCurrencyText; 
			fxObject.destination = destinationCurrency; 
			fxObject.destinationTxt = destinationCurrencyText; 
			fxObject.rate = fxRate; 
			fxObject.date = nlapiDateToString(new Date()); 		 	
			
			var currencyId = sourceCurrency + ">" + destinationCurrency ;
			
			currencyIds.push(currencyId);
			currencyFxObj.push(fxObject);
			
			
			
			
			//List to Source
			
			var fxObject = new Object();
			var sourceCurrency = objCurrencyList[c].id;
			var sourceCurrencyText = objCurrencyList[c].name;
			var destinationCurrency =  objSource.id;
			var destinationCurrencyText =  objSource.name;
			
			var fxRate = nlapiExchangeRate(sourceCurrency, destinationCurrency, nlapiDateToString(new Date()));
			
			fxObject.source = sourceCurrency; 
			fxObject.sourceTxt = sourceCurrencyText; 
			fxObject.destination = destinationCurrency; 
			fxObject.destinationTxt = destinationCurrencyText; 
			fxObject.rate = fxRate; 
			fxObject.date = nlapiDateToString(new Date()); 		 	
			
			var currencyId =   sourceCurrency  + ">" + destinationCurrency;
			
									
			currencyIds.push(currencyId);
			currencyFxObj.push(fxObject);
		}
				
	}
	
	
	fxResult.ids	= currencyIds;
	fxResult.obj 	= currencyFxObj;
	
	return fxResult; 

}
