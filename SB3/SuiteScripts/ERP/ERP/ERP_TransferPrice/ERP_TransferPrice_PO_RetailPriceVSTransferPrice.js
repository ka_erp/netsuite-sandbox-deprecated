/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       23 Sep 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 * 
 */


function mainPOInterCoPrice_UE_BS(type){
	
	nlapiLogExecution("AUDIT","PURCHASE ORDER", nlapiGetRecordId());
    nlapiLogExecution("AUDIT","PURCHASE ORDER FORM", nlapiGetFieldValue('customform'));
	
	var idCustomform = nlapiGetFieldValue('customform'); 
	
	if(idCustomform == 159){	//TODO: Make these parameters
	
			nlapiLogExecution("AUDIT","PURCHASE ORDER", nlapiGetRecordId());
						
			var bPOError = false;
			var idPriceLevel = 1;
			var idCurrency = nlapiGetFieldValue('currency');								
			var priceMethod = nlapiGetFieldValue('custbody_erp_price_method'); //1 - Standard , 2 - Base Price		
																			
			bPOError = validateTransferPrice(); 	
			
			nlapiLogExecution("AUDIT", "PO ERROR " + nlapiGetRecordId(), bPOError);
			var apStat = nlapiGetFieldValue("approvalstatus");	 nlapiLogExecution("AUDIT", "status", apStat);	
			
			if(bPOError){
				nlapiSetFieldValue('approvalstatus', 1); 	nlapiLogExecution("AUDIT", "put status to ", 'pending approval');
						  			
			}else{
				nlapiSetFieldValue('approvalstatus', 2);
			}
			
	
	}
}

function validateTransferPrice(){

	var bError = false; 
	var intGetPOlineCount = nlapiGetLineItemCount('item');

	for(var i = 1; i <= intGetPOlineCount; i++){

		var idItemId = nlapiGetLineItemValue('item','item', i);
		var idItemName = nlapiGetLineItemText('item','item', i);
		var strItemName = nlapiGetLineItemValue('item','description', i);
		var strItemType = nlapiGetLineItemValue('item','itemtype', i);
		
		var fRetailPrice = nlapiGetLineItemValue('item','custcol_ka_po_retail_value', i); fRetailPrice = Number(fRetailPrice); 
		var fTransferPrice = nlapiGetLineItemValue('item','rate', i); fTransferPrice = Number(fTransferPrice);
		var bOverride = nlapiGetLineItemValue('item','custcol_erp_transferprice_validation', i);
		
		nlapiLogExecution('DEBUG','Override',bOverride);
		 			
		var objValidate = validateTransferVRetail(fRetailPrice, fTransferPrice, bOverride); 
		
		var memo  = nlapiGetLineItemValue('item','custcol_erp_to_memo', i);		
		
		if(!isNullOrEmpty(memo)){			
			var checkIfValidated = memo.indexOf('Validation')		
			
			if(checkIfValidated > -1){
				memo = memo.substr(0, checkIfValidated - 2);			
			}
			
			memo = memo + '\n\nValidation: \n' + objValidate.message;
			
		}
		
				
		if(objValidate.status == false){ bError = true;} 
		
		nlapiSetLineItemValue('item','custcol_erp_to_memo', i, memo);
	}
	
	return bError; 
	
}

