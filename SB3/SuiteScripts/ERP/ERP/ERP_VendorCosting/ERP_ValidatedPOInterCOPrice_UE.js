/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       23 Sep 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function validatePOInterCoPrice_UE_BS(type){

	nlapiLogExecution("AUDIT","PURCHASE ORDER", nlapiGetRecordId());
    nlapiLogExecution("AUDIT","PURCHASE ORDER FORM", nlapiGetFieldValue('customform'));
	
	var idCustomform = nlapiGetFieldValue('customform');	
	if(idCustomform == 159){
	
	nlapiLogExecution("AUDIT","PURCHASE ORDER", nlapiGetRecordId());
	
	
	//TODO: Make these parameters
	var idPriceLevel = 1;
	var idCurrency = nlapiGetFieldValue('currency');
	//var mode = 'set';// 'validate';
	
	var priceMethod = nlapiGetFieldValue('custbody_erp_price_method'); //1 - Standard , 2 - Base Price		

	// get all items from the order
	var arrItems = getPOItemArray();

	// search the price
	var objItemResult = getItemResultObj(arrItems, idCurrency, idPriceLevel);

	// go through the validation

	var bPOError = false;

	nlapiLogExecution("DEBUG","JSON 2OBJ", JSON.stringify(objItemResult));

		if(!isNullOrEmpty(objItemResult)){
	
			
			if(priceMethod != 2){ // 2 - Retail
	
				var objItemResultIds = objItemResult.ids;
				var objItemResultObjs = objItemResult.objs;
				nlapiLogExecution("DEBUG","JSON ID", JSON.stringify(objItemResultIds));
				nlapiLogExecution("DEBUG","JSON OBJ",JSON.stringify(objItemResultObjs));
	
				var arrP0Items = new Array();
				var intGetPOlineCount = nlapiGetLineItemCount('item');
	
	
	
				for(var i = 1; i <= intGetPOlineCount; i++){
	
					var idItemId = nlapiGetLineItemValue('item','item', i);
					var idItemName = nlapiGetLineItemText('item','item', i);
					var strItemName = nlapiGetLineItemValue('item','description', i);
					var strItemType = nlapiGetLineItemValue('item','itemtype', i);
					var rate = nlapiGetLineItemValue('item','rate', i); rate = Number(rate);
	
					if(strItemType == 'InvtPart'){
						var arrindex = objItemResultIds.indexOf(idItemId);
	
						if(arrindex >= 0){
							var arrObj  = objItemResultObjs[arrindex];
							var retailPrice = arrObj.salesprice;
							nlapiLogExecution("DEBUG", "PO :  " + nlapiGetRecordId() , idItemName + " porate: " + rate + " srp: " + retailPrice + " json: " + arrObj.salesprice);
	
							if(rate >= retailPrice || rate == 0){
								nlapiLogExecution("DEBUG", "PO :  " + nlapiGetRecordId() , idItemName + " : Purchase Price looks like it has been defaulted to Retail Price. Please Check");
								nlapiSetLineItemValue('item', 'custcol_erp_to_memo', i, "ERROR:  transfer price is >= retail, or zero. retails at: " + retailPrice );
								bPOError = true;
							}else{
								nlapiSetLineItemValue('item', 'custcol_erp_to_memo', i, "");
							}
						}
					}
	
	
					//nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : getPOItemArray: Item", strItemType);
				}
	
				nlapiLogExecution("AUDIT", "PO ERROR " + nlapiGetRecordId(), bPOError);
				if(bPOError){
					nlapiSetFieldValue('approvalstatus', 1);

          var apStat = nlapiGetFieldValue("approvalstatus");
		      nlapiLogExecution("AUDIT", "status", apStat); 
          
				}else{
					nlapiSetFieldValue('approvalstatus', 2);
				}
				
				
			}else if(priceMethod == 2){
										
				var vendorId = nlapiGetFieldValue('entity'); 
				var retailPercentage = ""; 
				
				if(vendorId){
					retailPercentage = nlapiLookupField('vendor', vendorId, 'custentity_intercompany_retail_percent'); 
				}
				
				if(!isNullOrEmpty(retailPercentage)){									
					retailPercentageTxt = retailPercentage;
					retailPercentage = eval(retailPercentage); 
					
					nlapiLogExecution('AUDIT',"Retail %", retailPercentage); 
					
					var objItemResultIds = objItemResult.ids;
					var objItemResultObjs = objItemResult.objs;
					nlapiLogExecution("DEBUG","JSON ID", JSON.stringify(objItemResultIds));
					nlapiLogExecution("DEBUG","JSON OBJ",JSON.stringify(objItemResultObjs));
		
					var arrP0Items = new Array();
					var intGetPOlineCount = nlapiGetLineItemCount('item');
		
					for(var i = 1; i <= intGetPOlineCount; i++){
		
						var idItemId = nlapiGetLineItemValue('item','item', i);
					    var idItemName = nlapiGetLineItemText('item','item', i);
						var strItemName = nlapiGetLineItemValue('item','description', i);
						var strItemType = nlapiGetLineItemValue('item','itemtype', i);
						var rate = nlapiGetLineItemValue('item','rate', i); rate = Number(rate);
		
						if(strItemType == 'InvtPart'){
							var arrindex = objItemResultIds.indexOf(idItemId);
		
							if(arrindex >= 0){
								var arrObj  = objItemResultObjs[arrindex];
								var retailPrice = arrObj.salesprice;
								nlapiLogExecution("AUDIT", "UE : Item " + strItemName , "porate: " + rate + " srp: " + retailPrice + " json: " + arrObj.salesprice);
		
								
								if(retailPrice > 0){
									retailPrice = (retailPrice * retailPercentage);
									nlapiSetLineItemValue('item', 'rate', i, retailPrice);
									nlapiSetLineItemValue('item', 'custcol_erp_to_memo', i, "sales price:  " + arrObj.salesprice + " retail formula: " + retailPercentageTxt + " new rate:" + retailPrice);
									
								}else{
									bPOError = true;
									nlapiSetLineItemValue('item', 'rate', i, retailPrice);
									nlapiSetLineItemValue('item', 'custcol_erp_to_memo', i, "ERROR: sales price: " + arrObj.salesprice + "retail price not set");
								}						
								
	
							}else{
								bPOError = true;
								nlapiSetLineItemValue('item', 'rate', i, 0);
								nlapiSetLineItemValue('item', 'custcol_erp_to_memo', i, "ERROR:  retail price item not found");
							}	
						}
					}
					
					nlapiLogExecution("AUDIT", "PO ERROR " + nlapiGetRecordId(), bPOError);
					if(bPOError){
						nlapiSetFieldValue('approvalstatus', 1);
            
            var apStat = nlapiGetFieldValue("approvalstatus");
  		      nlapiLogExecution("AUDIT", "status", apStat);  
            
					}else{
						nlapiSetFieldValue('approvalstatus', 2);
					}
					
				}else{
					
					var intGetPOlineCount = nlapiGetLineItemCount('item');
					
					for(var i = 1; i <= intGetPOlineCount; i++){
						nlapiSetLineItemValue('item', 'custcol_erp_to_memo', i , 'WARNING: Retail price formula is not set in the vendor. Standard pricing used'); 
					}
				}	
				
				
			}	
		}
	
	}
	//throw nlapiCreateError("NO_FILE_FOUND", "No File found - Could not Process");
}


function getItemResultObj(items, currency, pricelevel){

	var strSavedSearchIDItm = null;
	var arSaveSearchFiltersItm = new Array();
	var arSavedSearchColumnsItm = new Array();
	var arSavedSearchResultsItm = null;

	var arrResultObjectArray = new Array();
	var arrIdObjectArray = new Array();

	var objItemResult = new Object();

	if(!isNullOrEmpty(items)){
		arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null , 'anyof', items ));
	}

	arSaveSearchFiltersItm.push(new nlobjSearchFilter( 'currency', 'pricing',  'anyof', currency));
	arSaveSearchFiltersItm.push(new nlobjSearchFilter( 'pricelevel', 'pricing',  'is', pricelevel));

	arSavedSearchColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('name'));
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('currency','pricing'));
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('pricelevel','pricing'));
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('unitprice','pricing'));

	arSavedSearchResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchColumnsItm);

	if(!isNullOrEmpty(arSavedSearchResultsItm)){

		//Build the result set object lookup and fill the Array Maps
		for (var j = 0 ; j < arSavedSearchResultsItm.length; j++){

			var obj = new Object();
			obj.id = arSavedSearchResultsItm[j].getValue('internalid');
			obj.name = arSavedSearchResultsItm[j].getValue('name');
			obj.salesprice = arSavedSearchResultsItm[j].getValue('unitprice','pricing');
			obj.currency = arSavedSearchResultsItm[j].getValue('currency','pricing');
			obj.pricelevel = arSavedSearchResultsItm[j].getValue('pricelevel','pricing');

			arrResultObjectArray.push(obj);
			arrIdObjectArray.push(obj.id);
		}

		nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : arrResultObjectArray", JSON.stringify(arrResultObjectArray));
		nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : arrIdObjectArray", JSON.stringify(arrIdObjectArray));

		objItemResult.ids = arrIdObjectArray;
		objItemResult.objs = arrResultObjectArray;

		return objItemResult;

	}else{
		return null;
	}


}


function getPOItemArray(){

	var arrP0Items = new Array();
	var intGetPOlineCount = nlapiGetLineItemCount('item');

	for(var i = 1; i <= intGetPOlineCount; i++){

		var idItemId = nlapiGetLineItemValue('item','item', i);
		var strItemType = nlapiGetLineItemValue('item','itemtype', i);

		if(strItemType == 'InvtPart'){
			arrP0Items.push(idItemId);
		}

		//nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : getPOItemArray: Item", strItemType);
	}


	nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : getPOItemArray", arrP0Items.toString());
	return arrP0Items;
}


function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}
