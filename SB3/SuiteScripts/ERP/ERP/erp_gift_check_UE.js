/**
 * Module number
 *
 * Version    Date            Author           Remarks
 * 1.00       16 Nov 2016     timfrazer
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create,edit
 *                      Invoice
 * @returns {Void}
 */
function giftCardAuthCheck_BS(type){

  var count = nlapiGetLineItemCount("item");

	for(var i=1; i<= count; i++) {
		var itemtype = nlapiGetLineItemValue('item', 'itemtype', i);

		//wow found a gift cert?
		if(itemtype == 'GiftCert'){

			var objLineObject = {};
			objLineObject.itemtype = itemtype;
			objLineObject.number = nlapiGetLineItemValue('item','description',i);
			objLineObject.amount = nlapiGetLineItemValue('item', 'amount',i);
			objLineObject.rate  = nlapiGetLineItemValue('item', 'rate',i);
			objLineObject.item  = nlapiGetLineItemValue('item', 'item',i);
			objLineObject.qty   = nlapiGetLineItemValue('item','quantity',i);
			objLineObject.giftCertAuth = nlapiGetLineItemValue('item', 'giftcertnumber',i);
			objLineObject.giftcertkey = nlapiGetLineItemValue('item','giftcertkey',i);
			objLineObject.email = nlapiGetLineItemValue('item','giftcertrecipientemail',i);
			objLineObject.sender = nlapiGetLineItemValue('item','giftcertfrom',i);
			objLineObject.recipient = nlapiGetLineItemValue('item','giftcertrecipientname',i);
			//decide if the Auth code needs to be changed
			nlapiLogExecution("DEBUG", "Object",JSON.stringify(objLineObject));

			var gcFix = validateGc(objLineObject.number,objLineObject.giftCertAuth);

			//false no auth code found
			//true auth code doesn't need to be replaced
			//auth code return if needs to be replaced
				//check gc auth and number

					if(gcFix == false){ // cannot find Gift card in master record
					//	console.log('Error')
						nlapiSetLineItemValue('item','custcol_erp_gc_auth',i,'T');
						var error ={};
						error.invoice =nlapiGetFieldValue('tranid');
						error.number = objLineObject.number;
						error.giftCertAuth =objLineObject.giftCertAuth;
						//sendEmail(error); - this is done via saved search every 30min
						nlapiLogExecution("DEBUG","Gift card that had issue: ",JSON.stringify(error));

					}
					else if (gcFix == true){//change nothing
						nlapiLogExecution("DEBUG", "Keeping cert", i + " Number: " + objLineObject.giftcertnumber );

					}
					else { // uh oh need to remove the item and put in the proper auth code
						nlapiRemoveLineItem('item', i);
						objLineObject.giftCertAuth = gcFix;
						addGc(objLineObject);
					}


			nlapiLogExecution("DEBUG", "removing the cert", i + " item:" + objLineObject.item );
	}
}
}

function addGc(objLineObject){

		nlapiSelectNewLineItem('item');
		nlapiSetCurrentLineItemValue('item','item', 3048,true, true);
		nlapiSetCurrentLineItemValue('item','rate', objLineObject.rate,true, true);
		nlapiSetCurrentLineItemValue('item','giftcertfrom', objLineObject.sender,true, true);
		nlapiSetCurrentLineItemValue('item','giftcertkey',objLineObject.giftcertkey,true, true);
		nlapiSetCurrentLineItemValue('item','giftcertrecipientemail', objLineObject.email),true, true;
		nlapiSetCurrentLineItemValue('item','giftcertrecipientname', objLineObject.recipient,true, true);
		nlapiSetCurrentLineItemValue('item','giftcertnumber', objLineObject.giftCertAuth,true, true);
		nlapiSetCurrentLineItemValue('item','amount', objLineObject.amount,true, true);
		nlapiSetCurrentLineItemValue('item','description', objLineObject.number,true, true);
		nlapiCommitLineItem("item");
}



function validateGc(gcNumber,gcAuth){

		var filter = new Array();
		var columns = new Array();
		nlapiLogExecution("DEBUG","Gift Card number ",JSON.stringify(gcNumber));

		filter.push(new nlobjSearchFilter('name',null, 'is',gcNumber)); // card number
		columns.push(new nlobjSearchColumn('name', null, null)); // card number
		columns.push(new nlobjSearchColumn('custrecord_wtka_giftlist_authcode', null, null)); // card auth code

		var results = nlapiSearchRecord('customrecord_wtka_giftcard_list','customsearch_erp_gc_search',filter,columns);

		nlapiLogExecution("DEBUG","Gift Card Results ",JSON.stringify(results));
		//we found something with the gift card number
		if (results!= null){

			var gcAuthSearch = results[0].getValue('custrecord_wtka_giftlist_authcode');
			nlapiLogExecution("DEBUG","GgcAuthSearch: ",gcAuthSearch);
			if(gcAuth!= gcAuthSearch) {//check if the cert auth matches else return what is in the custom record
				return gcAuthSearch;
			}
			else { // no need to change a thing - n
				return true;
			}
		}

	return false; // outright fail cannot find gift card
}

// function sendEmail(error){
//
// 	var subject = 'Gift Card Fix Failure Invoice '+ error.invoice;
// 	var body 	= 'Hello,<br><br>';
// 	body 		+= JSON.stringify('invoice: '+error.invoice);
// 	body 		+= '<br>'+JSON.stringify('number: '+error.number);
// 	body 		+= '<br>'+JSON.stringify('Auth Code: '+error.giftCertAuth);
// 	body 		+= '<br><br>Unable to find Card number/incorrect card number. Please process manually through user interface.';
// 	body 		+= '<br><br><br><br>Thanks';
// 	body 		+= '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
// 	nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
// console.log('email:',subject+body)
// nlapiSendEmail(298467,298467,subject,body,null,null,null,null,null,null,298467);
// }
