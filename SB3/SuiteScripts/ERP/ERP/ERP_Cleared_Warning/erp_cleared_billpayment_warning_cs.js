/**
 *	File Name		:	erp_cleared_billpayment_warning_cs.js
 *	Function		:	Prompts a warning message when a user attempts to change/void a Cleared bill payment
 * 	Remarks			:	
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

function saveRecord_ClearedWarning(){
	
	var stCleared;
	var stRecType = nlapiGetRecordType();
	
	//Bill Payment is updated
	if(stRecType == 'vendorpayment'){
		stCleared = nlapiGetFieldValue('cleared');
	} 
	
	//Bill Payment is voided
	//A Voiding Journal is loaded into the UI
	else if(stRecType == 'journalentry'){
		//Get Cleared value of the Bill Payment
		var stBillPayment = nlapiGetFieldValue('createdfrom');
		if(stBillPayment){
			stCleared = nlapiLookupField('vendorpayment', stBillPayment, 'cleared');
		}
	}

	if(stCleared == 'T'){
		return confirm('Alert: the Bill Payment record has been reconciled');
	}

	return true;
}