/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       10 Oct 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function transferOrderdefault_BL(type, form, request){
	
//	 var idUser = nlapiGetUser(); 
//	 nlapiSetFieldValue("subsidiary",nlapiGetSubsidiary(), false, false);
//	 nlapiSetFieldValue("employee", idUser, true, false);
//	 var location = nlapiLookupField('employee', idUser, 'location');
//	 nlapiSetFieldValue("location", location, true, false);
		
	if(type == 'view'){
	
	//nlapiGetField("custbodycustbody_erp_transferorder_sts").setDisplayType("hidden"); 
	
// 		9	Damaged	
//		3	Pullback
//		8	Shop to Shop
	
//		nlapiGetField("location").setDisplayType("hidden");
//		nlapiGetField("transferlocation").setDisplayType("hidden");
	//	
		nlapiGetField("custbody_erp_rolelist").setDisplayType("hidden");
		
		var orderType = nlapiGetFieldValue('custbody_ka_order_type');
		
//		if(orderType == 8){	
//			nlapiGetField("custbody_erp_location_destination").setDisplayType("inline");
//			nlapiGetField("custbody_erp_location_source").setDisplayType("inline");
//		}else if(orderType == 9 || orderType == 3){			
//			nlapiGetField("custbody_erp_location_source").setDisplayType("inline");
//			nlapiGetField("custbody_erp_location_destination").setDisplayType("inline");
//		}
		
			
			nlapiGetField("custbody_erp_location_destination").setDisplayType("hidden");
			nlapiGetField("custbody_erp_location_source").setDisplayType("hidden");
		
	}
	else if(type == 'edit' ){
		
//		nlapiGetField("location").setDisplayType("hidden");
//		nlapiGetField("transferlocation").setDisplayType("hidden");
		
	}
	
}
