/**
 *	File Name		:	WTKA_GiftCard_Fix.js
 *  Script Type		: 	Scheduled
 *	Schedule		: 	Every day at <TIME>
 *	Function		:	Script to correct invoices with incorrect Authorization Code for Gift Certificates
 * 	Authors			:	Rini Thomas
 *	Company			:	Wipro Limited
 *	Release Dates	:	DD-MMMM-2016 (v1.0)
 * 	Current Version	:	1.0
**/

{
	var recordList 			= new Array(), 	script_entry, 				retryIndex 			= 0;
	var governanceMinUsage 	= 200,			scriptMaxTimeout 	= 3500; //3600 - is max
	var index	 			= 0, 			lastProcessed 	= 0,		recId	 			= 0;
	var suiteletID = '574', 				suiteletDeployID = '1'; 	// Update with New Parameters after moving to different environment
}

function fixIncorrectGiftCards() //Scheduled script
{
	nlapiLogExecution('debug', 'Trigger Script');
	script_entry  = new Date(); //Script Start Time
	var abortFlag = false;
	try
	{
		var custRecord		= nlapiLoadRecord('customrecordwtka_giftcard_counter', 1);
		var recordedIndex  	= custRecord.getFieldValue('custrecordwtka_giftcard_index');
		retryIndex  		= custRecord.getFieldValue('custrecordwtka_giftcard_retry');
		index 				= parseInt(recordedIndex);
		if(index >= 0) //Records to be processed
		{
			/* Find all Invoice records with Gift Cards in Line*/
			var filters = new Array();
			filters.push(['mainline', 	'is', 	'F']); 				// Line level
			filters.push('AND');
			filters.push(['type', 		'is', 	'CustInvc']);		// Invoice
			filters.push('AND');
			filters.push(['item', 		'is', 	3048]); 			// Gift Card item
			filters.push("AND");
			filters.push(['formulanumeric: ABS(CONCAT({internalid}, {line}))', "greaterthanorequalto", index, null]);  // Lookup unprocessed records

			filters = generateFilter(filters);

			var FinalString = new nlobjSearchColumn('formulanumeric').setFormula('ABS(CONCAT({internalid}, {line}))');
			var columns = new Array();
			columns.push(new nlobjSearchColumn('internalid'));
			columns.push(new nlobjSearchColumn('line'));
			columns.push(new nlobjSearchColumn('item'));		//GiftCert
			columns.push(new nlobjSearchColumn('memo'));		//Card Number
			columns.push(new nlobjSearchColumn('giftcert'));	// Auth Code
			columns.push(FinalString.setSort());

			var recList = nlapiSearchRecord('transaction', null, filters, columns);
			if(recList != null)
			{
				nlapiLogExecution('debug', 'RESULTS', recList.length);
				var i=0;
				for(; !abortFlag && i < recList.length; i++)
				{
					recId 				= recList[i].getId();
					var line 	 		= recList[i].getValue('line');
					var processString 	= String(recId) + String(line);
					lastProcessed 		= parseInt(processString);
					if(getScriptTimeConsumed() < scriptMaxTimeout && nlapiGetContext().getRemainingUsage() > governanceMinUsage)
					{
						if(checkExisting(recId)) continue; // find unique records
						recordList.push(recId);
						try
						{
							fixRecord(recId);
							recordList.push(recId);
						}
						catch(err_fix)
						{
							nlapiLogExecution('DEBUG', 'Error in processing request',  	err_fix);
							nlapiLogExecution('DEBUG', 'Skipping record: ',  			recId);
							createUnprocessedRecord(recId, err_fix);
							continue;
						}
					}
					else
					{
						nlapiLogExecution('DEBUG', 'Script time consumed', 	 getScriptTimeConsumed());
						nlapiLogExecution('DEBUG', 'Script units remaining', nlapiGetContext().getRemainingUsage());
						nlapiLogExecution('DEBUG', 'Script limits reached', 'Rescheduling...');
						var counterRetry = parseInt(retryIndex) + 1;
						updateIndex(lastProcessed, counterRetry);
						abortFlag = true;
					}
				}
				if(!abortFlag && i == recList.length && index > 0)	updateIndex(0, 0); // Reset Counters
			}
			else if(index != 0)
			{
				updateIndex(0, 0); //Reset Counters
			}
		}
	}
	catch(Err_fixIncorrectGiftCards)
	{
		if(Err_fixIncorrectGiftCards.code == 'SSS_SEARCH_TIMEOUT')
		{
			nlapiLogExecution('DEBUG', 'Error in Processing request', recId);
			if(index == lastProcessed) // Retry
			{
				if(retryIndex < 3) //try processing record max of 3 times
				{
					nlapiLogExecution('DEBUG', 'Rescheduling script. Attempt #' + retryIndex, 'Process timed out and rescheduling script');
					var counterRetry = parseInt(retryIndex) + 1;
					if(lastProcessed == 0) lastProcessed = 1; //Trigger for index > 0
					updateIndex(lastProcessed, counterRetry);
				}
				else // Retry
				{
					nlapiLogExecution('DEBUG', 'Error: Retry failed', 'Skipping record: ' + recId);
					createUnprocessedRecord(recId, Err_fixIncorrectGiftCards);
					var nextRec 		= parseInt(recId) + 1; //skip current record in next call
					var processString 	= String(nextRec) + '0';
					lastProcessed 		= parseInt(processString);
					updateIndex(lastProcessed, 0); //Skip process
				}
			}
			else // timeout for different record
			{
				updateIndex(lastProcessed, 1);
			}
		}
		else
		{
			//ERROR HANDLING
			nlapiLogExecution('debug', 'Err_fixIncorrectGiftCards', Err_fixIncorrectGiftCards);
			if(recId>0)	createUnprocessedRecord(recId, Err_fixIncorrectGiftCards);

			updateIndex(0, 0); //Abort process

			var subject = 'Gift Card Fix Failure';
			var body 	= 'Hello,<br><br>';
			body 		+= 'Gift Card Fix Scheduled script failed in NetSuite due to below error.<br>';
			body 		+= JSON.stringify(Err_fixIncorrectGiftCards);
			body 		+= '<br><br>Process needs to be handled manually through user interface after corrections.';
			body 		+= '<br><br><b>Please reset the custom record Index value to 0 after corrections!<b>';
			body 		+= '<br><br><br><br><br><br><br>Thanks';
			body 		+= '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
			nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
			// nlapiSendEmail(243423, toList, subject, body, ccList);


		}
	}
}

function generateFilter(filters)
{
	var subFilter = new Array();
	var lastID 		= 0;
	var loopFlag 	= true;
	while(loopFlag)
	{
		var partialFormula = generateFormula(lastID, subFilter);
		if(partialFormula == null)
		{
			loopFlag = false;
		}
		else
		{
			subFilter 	= partialFormula.subFilter;
			lastID  = partialFormula.lastID;
		}
	}

	if(subFilter.length > 0)
	{
		filters.push("AND");
		filters.push(subFilter);
	}
	return filters;
}

function generateFormula(lastID, subFilter)
{
	var maxLoop = 33;
	var responseObject = new Object();

	var filter = new Array()
	filter.push(new nlobjSearchFilter('internalidnumber', 	null, 'greaterthan', lastID));

	var columns = new Array();
	columns.push(new nlobjSearchColumn('internalid').setSort());
	columns.push(new nlobjSearchColumn('name'));
	columns.push(new nlobjSearchColumn('custrecord_wtka_giftlist_authcode'));
	columns.push(new nlobjSearchColumn('custrecord_wtka_giftlist_cardnumber'));

	var searchResult = nlapiSearchRecord('customrecord_wtka_giftcard_list', null, filter, columns);

	if(searchResult == null)	return null;

	var s=0;
	var subIndex = parseInt(searchResult.length/maxLoop) + 1;
	for(var c=0; c<subIndex; c++)
	{
		var formulaString = 'formulanumeric: CASE';
		for(; searchResult != null && s<searchResult.length && s<(maxLoop*c+maxLoop); s++)
		{
			formulaString += ' WHEN DECODE({memo}, \'' + searchResult[s].getValue('name') + '\', \'1\', \'0\') = 1 AND DECODE ({giftcertificate}, \'' + searchResult[s].getValue('custrecord_wtka_giftlist_authcode') + '\', \'1\', \'0\') = 0 THEN 1';
			lastID = searchResult[s].getValue('internalid');
		}
		formulaString += ' ELSE 0 END';
		if(subFilter != null && subFilter.length > 0)	subFilter.push("OR");
		subFilter.push([String(formulaString), "greaterthan", 0, null]);  // Lookup unprocessed records
	}

	responseObject.lastID  = lastID;
	responseObject.subFilter = subFilter;
	return responseObject;
}

function checkExisting(recId)
{
	for(var r=0; r<recordList.length; r++)
	{
		if(recId == recordList[r])	return true;
	}
	return false;
}

function fixRecord(recId)
{
	nlapiLogExecution('debug', 'Fixing Record', recId);
	var change = false;
	try
	{
		var record = nlapiLoadRecord('invoice', recId); //INVOICE RECORD ONLY
		for(var l=0; l<record.getLineItemCount('item'); l++)
		{
			var itemType = record.getLineItemValue('item', 'itemtype', l+1);
			if(itemType != 'GiftCert') continue; //Skip if not Gift Certificate

			var authCode = record.getLineItemValue('item', 'giftcertnumber', l+1); //Gift Cert Auth Code
			var cardNo	 = record.getLineItemValue('item', 'description', 	 l+1); //Card Number

			var validAuthCode = lookupAuthCode(cardNo, authCode);

			switch(validAuthCode.status)
			{
				case 'ERROR':
					//ERROR HANDLING ?? Break process?
					nlapiLogExecution('debug', 'lookupAuthCode', validAuthCode.value);
					break;
				case 'VALID':
					//DO NOTHING - VALID AUTH CODE
					continue; // Skip line
				default : //SUCCESS

					// Add new line with correct Gift Card Auth Code
					record.selectNewLineItem('item');
					record.setCurrentLineItemValue('item', 'item', 						record.getLineItemValue('item', 'item', 					l+1));
					//set the internak id for the giftcert - this is take from script ERP_GiftCard_WebStore_CashSales_CS.js
					record.setCurrentLineItemValue('item', 'giftcertkey', 						record.getLineItemValue('item','giftcertkey',l+1));

					record.setCurrentLineItemValue('item', 'item', 						record.getLineItemValue('item', 'item', 					l+1));
					record.setCurrentLineItemValue('item', 'description', 				cardNo);
					record.setCurrentLineItemValue('item', 'giftcertfrom', 				record.getLineItemValue('item', 'giftcertfrom', 			l+1));
					record.setCurrentLineItemValue('item', 'giftcertrecipientname', 	record.getLineItemValue('item', 'giftcertrecipientname', 	l+1));
					record.setCurrentLineItemValue('item', 'giftcertrecipientemail', 	record.getLineItemValue('item', 'giftcertrecipientemail', 	l+1));
					record.setCurrentLineItemValue('item', 'giftcertmessage', 			record.getLineItemValue('item', 'giftcertmessage', 			l+1));
					record.setCurrentLineItemValue('item', 'giftcertnumber', 			validAuthCode.value);
					record.setCurrentLineItemValue('item', 'quantity', 					record.getLineItemValue('item', 'quantity', 				l+1));
					record.setCurrentLineItemValue('item', 'price', 					record.getLineItemValue('item', 'price', 					l+1));
					record.setCurrentLineItemValue('item', 'amount', 					record.getLineItemValue('item', 'amount', 					l+1));
					record.commitLineItem('item');

					// Remove line with incorrect Gift Card Auth Code
					record.removeLineItem('item', l+1);
					change = true;
			}
		}
		if(change)
		{
			nlapiSubmitRecord(record);
		}
	}
	catch(Err_fixRecord)
	{
		//ERROR HANDLING
		nlapiLogExecution('debug', 'Err_fixRecord', Err_fixRecord);
		createUnprocessedRecord(recId, Err_fixRecord);
	}
}

function lookupAuthCode(cardNo, authCode)
{
	var resp = new Object();
	try
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('name', 	null, 'is', cardNo));
		// filters.push(new nlobjSearchFilter('custrecord_wtka_giftlist_authcode', 	null, 'is', authCode));

		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_wtka_giftlist_authcode'));

		var recList = nlapiSearchRecord('customrecord_wtka_giftcard_list', null, filters, columns);
		if(recList == null)
		{
			resp.status = 'ERROR';
			resp.value  = 'NOT FOUND';
		}
		else
		{
			if(recList.length > 1)
			{
				resp.status = 'ERROR';
				resp.value  = 'DUPLICATES || ' + JSON.stringify(recList);
			}
			else
			{
				if(authCode == recList[0].getValue('custrecord_wtka_giftlist_authcode'))
				{
					resp.status = 'VALID';
					resp.value  = 'VALID AUTH CODE';
				}
				else
				{
					resp.status = 'SUCCESS';
					resp.value  = recList[0].getValue('custrecord_wtka_giftlist_authcode');
				}
			}
		}
	}
	catch(Err_lookupAuthCode)
	{
		resp.status = 'ERROR';
		resp.value  = Err_lookupAuthCode;
	}
	return resp;
}

function getScriptTimeConsumed()
{
	var currentTime = new Date();
	var timeDiff 	= (currentTime - script_entry)/1000; //in seconds
	return timeDiff;
}

function updateIndex(index, retry)
{
	nlapiLogExecution('debug', 'Updating Counters');
	//UPDATE COUNTER
	var fields = new Array();
	var values = new Array();

	fields.push('custrecordwtka_giftcard_index');
	values.push(index);

	var counter = retry;
	if(counter !== null)
	{
		fields.push('custrecordwtka_giftcard_retry');
		values.push(counter);
	}
	nlapiSubmitField('customrecordwtka_giftcard_counter', 1, fields, values, false);
}

function createUnprocessedRecord(recordId, errorMsg)
{
	nlapiLogExecution('debug', 'Creating Unprocessed record', recordId);
	try
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_wtka_unprocessed_recid',   null, 'is', 	recordId));

		var columns = new Array();
		columns.push(new nlobjSearchColumn('custrecord_wtka_unprocessed_gift_error'));

		var recList = nlapiSearchRecord('customrecord_wtka_unprocessed_gift_rec', null, filters, columns);
		if(recList == null)
		{
			try
			{
				var unprocessedRecord = nlapiCreateRecord('customrecord_wtka_unprocessed_gift_rec');
				unprocessedRecord.setFieldValue('custrecord_wtka_unprocessed_recid', 		recordId);
				unprocessedRecord.setFieldValue('custrecord_wtka_unprocessed_gift_error', 	errorMsg);
				nlapiSubmitRecord(unprocessedRecord);
			}
			catch(err_cr_upr)
			{
				nlapiLogExecution('debug', 'ERROR in creating Unprocessed record', err_cr_upr);
			}
		}
		else
		{
			var error_Existing = recList[0].getValue('custrecord_wtka_unprocessed_gift_error');
			if(error_Existing != errorMsg)
			{
				var newMsg = error_Existing + ' ||...|| ' + errorMsg;
				try
				{
					var unprocessedRecord = nlapiLoadRecord('customrecord_wtka_unprocessed_gift_rec', recList[0].getId());
					unprocessedRecord.setFieldValue('custrecord_wtka_unprocessed_gift_error', newMsg);
					nlapiSubmitRecord(unprocessedRecord);
				}
				catch(err_upd_upr)
				{
					nlapiLogExecution('debug', 'ERROR in updating Unprocessed record', err_upd_upr);
				}
			}
		}
	}
	catch(err_upr)
	{
		nlapiLogExecution('debug', 'ERROR', err_upr);
	}

}

/**
 *	File Name		:	WTKA_GiftCard_Fix.js
 *  Script Type		: 	User Event
 *  Script Name		:	WTKA_GiftCard_Fix_UE
 *  Event Type		:	After Submit
 *	Function		:	Script to reschedule script or send error email notification
 * 	Authors			:	Rini Thomas
 *	Company			:	Wipro Limited
 *	Release Dates	:	DD-MMMM-2016 (v1.0)
 * 	Current Version	:	1.0
**/

function userEvent_WTKA_GiftCard_Fix(type) // user event
{
	if(type == 'edit' || type == 'xedit')
	{
		var custRecord 	 = nlapiLoadRecord('customrecordwtka_giftcard_counter', nlapiGetRecordId());
		var index 		 = custRecord.getFieldValue('custrecordwtka_giftcard_index');
		nlapiLogExecution('debug', 'index', index);
		if(index > 0)
		{
			nlapiLogExecution('DEBUG', 'Rescheduling script from After Submit', 'Process timed out and rescheduling script');
			var status = nlapiScheduleScript('customscript_wtka_giftcard_fix', 'customdeploy_wtka_giftcard_fix'); // RescheduleScript
			nlapiLogExecution('debug', 'Script', status);
		}
	}
}

/**
 *	File Name		:	WTKA_GiftCard_Fix.js
 *  Script Type		: 	Suitelet
 *	Function		:	Script to display unprocessed records and remove them by selecting checkbox
 * 	Authors			:	Rini Thomas
 *	Company			:	Wipro Limited
 *	Release Dates	:	DD-MMMM-2016 (v1.0)
 * 	Current Version	:	1.0
**/

function ViewGiftCardList(request, response)
{
	if (request.getMethod() == 'GET')
	{
		try
		{
			var unprocRecorList = nlapiCreateForm('Unprocessed Records');
			unprocRecorList.setTitle('Unprocessed Gift Card Records');

			var recList			= unprocRecorList.addSubList('recordlist',  'list', 	'Unprocessed Invoices');
			var recId 			= recList.addField('wtka_recid', 			'select', 	'Invoice Record', 	'transaction');
			recId.setDisplayType('disabled');

			//Fetch data based on Request parameters
			var filters = new Array();
			var cols 	= new Array();
			cols.push(new nlobjSearchColumn('custrecord_wtka_unprocessed_recid'));
			cols.push(new nlobjSearchColumn('custrecord_wtka_unprocessed_gift_error'));

			var searchResultChild = nlapiSearchRecord('customrecord_wtka_unprocessed_gift_rec', null, filters, cols);
			if(searchResultChild != null)
			{

				var errorMsg 		= recList.addField('wtka_error', 			'textarea', 	'Error Message');
				recId.setDisplayType('readonly');
				var custRecId 			= recList.addField('wtka_custrecid', 	'select', 		'Custom Record', 	'customrecord_wtka_unprocessed_gift_rec');
				custRecId.setDisplayType('hidden');
				var process 		= recList.addField('wtka_processed', 		'checkbox', 	'Processed?');
				process.setDisplayType('entry');
				for(var j=0; j < searchResultChild.length; j++)
				{
					recList.setLineItemValue('wtka_recid', 		j+1, searchResultChild[j].getValue('custrecord_wtka_unprocessed_recid'));
					recList.setLineItemValue('wtka_error', 		j+1, searchResultChild[j].getValue('custrecord_wtka_unprocessed_gift_error'));
					recList.setLineItemValue('wtka_custrecid', 	j+1, searchResultChild[j].getId());
					recList.setLineItemValue('wtka_processed', 	j+1, 'F');
				}
				recList.addMarkAllButtons();
				recList.addRefreshButton();
				unprocRecorList.addSubmitButton('Delete Selected');
			}
			else
			{
				recList.setLineItemValue('wtka_recid', 1, 'No results found!');
			}
			response.writePage(unprocRecorList);
		}
		catch(err)
		{
			nlapiLogExecution('debug', 'ViewGiftCardList', err);
		}
	}
	else //POST
	{
		try
		{
			// nlapiLogExecution('debug', 'COUNT', request.getLineItemCount('recordlist'));

			for(var i=0; i<request.getLineItemCount('recordlist'); i++)
			{
				var checked = request.getLineItemValue('recordlist', 'wtka_processed', i+1);
				if(checked == 'T')
				{
					try
					{
						nlapiDeleteRecord('customrecord_wtka_unprocessed_gift_rec', request.getLineItemValue('recordlist', 'wtka_custrecid', i+1));
					}
					catch(err_Del)
					{
						nlapiLogExecution('debug', 'Error in deleting record', err_Del);
					}
				}
			}
			try
			{
				nlapiSetRedirectURL('SUITELET', suiteletID, suiteletDeployID);
			}
			catch(err_refresh)
			{
				nlapiLogExecution('debug', 'Error in Loading Suitelet', err_refresh);
			}
		}
		catch(unexp_Err)
		{
			nlapiLogExecution('debug', 'Error in POST', unexp_Err);
		}
	}
}
