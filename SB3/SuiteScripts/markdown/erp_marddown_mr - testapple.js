/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 * @param {record} objRec
 * @param {search} objSearch
 * @param {error} objError
 * @param {email} objEmail
 * @param {runtime} objRuntime
 * 1) Schedule a search to look at custom records (search for item recs that are within the hour and not processing)
 * 2) If the custom record matches [time frame] & active then run map reduce
 * 3)
 */


/*
status list
1 pending
2 progress
3 completed
4 error
*/


define(['N/search',
        'N/record',
        'N/email',
        'N/runtime',
        'N/error',
        'N/file',
        ],
    function(search, record, email, runtime, error, tism) {

      function getInputData() {
  		    //Get all of the item records that need to be changed
  		    // the search contains the filters for the markdown
  		    var itmsToAdj = []; 	


         // var mySearch = search.load({
          //     id: 'customsearch_pa_headers'
          // });
          //
          // var resultSet = mySearch.run();
          //
          //  resultSet.each(function(result) {
          //    var recid = result.getValue({
          //      name: 'id'
          //     });
          //   log.debug('Records ',recid);
          //    return true;
          //  });


           // set the headers for the scheduler discount
                  //  record.submitFields({
                  //      type:'customrecord_price_adjustment',
                  //      id:searchResult.id,
                  //      values: {
                  //      custrecord_pa_status:'2' // in progress
                  //      }
                  //    });

        //  log.debug('count of items Results', JSON.stringify(resultSet));
        
        var myFilter = search.createFilter({
                name: 'isinactive',
                operator: search.Operator.IS,
                values: ['F']
          });

          var searchObj = search.load({
  		        id: 'customsearch_pa_items',
            filters: myFilter
  		    });

              //count id list once 


//Create date filter so that it only shows adjustments equal to or less than current search

  		    var pagedData = searchObj.runPaged({
				pageSize: 1000
  		    });

  		    //log.debug('input count: ', pagedData.count);

  		    pagedData.pageRanges.forEach(function(pageRange) {
  		        var page = pagedData.fetch({
  		            index: pageRange.index
  		        });
  		        itmsToAdj = itmsToAdj.concat(page.data);
  		    });
  		    log.debug('search results', JSON.stringify(itmsToAdj));
  		    // send an array of items to discount to MAP
          return itmsToAdj;
  		}

      // as this is a 1-1 ratio a reduce is not rquired for doing the price adjustments

        function map(context) {

        	log.debug('map parameters',context.value);

        	var searchResult = JSON.parse(context.value);


          //adjustment record
          var itm = {
                    id: 0,
                    priceLevel:0,
                    currency:0,
                    recLine:0,
                    newRecPrice:0,
                    oldRecPrice:0
          };

          
          itm.id =searchResult.values.formulatext;

          var itemObj = record.load({ // 5 GOV
                        type: record.Type.INVENTORY_ITEM,
                        id: itm.id // internal ID of the item
                    });

          itm.newRecPrice = searchResult.values.formulacurrency;
          itm.currency = 'price'+searchResult.values.formulatext_2;
          itm.priceLevel =  searchResult.values.formulatext_1.toString();
          itm.adjId =  searchResult.values.formulanumeric; //price record id
    //      log.debug('Child adj ID ', JSON.stringify(itm.adjId));
      //    var priceAdjustmentRec = record.load({ type:'customrecord_price_adjustment', id: '100'});


                  //find the row that has the internal ID of the price level
                itm.recLine = itemObj.findSublistLineWithValue({
                    sublistId:itm.currency,
                    fieldId: 'pricelevel',
                    value: itm.priceLevel
                  });
               // log.debug('item rec line: ', JSON.stringify(itm.recLine ));



                itm.oldRecPrice= itemObj.getMatrixSublistValue({
                    sublistId: itm.currency,
                    fieldId: 'price',
                    column:0, // always this as we don't do discounts on volume
                    line:itm.recLine
                   });

                     itemObj.setMatrixSublistValue({
                          sublistId: itm.currency,
                          fieldId: 'price',
                          column: 0,
                          line: itm.recLine,
                          value: itm.newRecPrice
                      });

          var itemSaved =   itemObj.save();

          if (itemSaved){
            record.submitFields({
              //backup old price field 3 GOV
                   type:'customrecord_pa_child',
                   id:itm.adjId,
                   values: {
                   custrecord_pa_price_backup:itm.oldRecPrice,
                   custrecord_pa_adj_child_status:3 //completed
                 }
                 });

          }
          else {
            record.submitFields({
                   type:'customrecord_pa_child',
                   id:itm.adjId,
                   values: {
                   custrecord_pa_child_log: JSON.stringify(itemSaved),
                   custrecord_pa_adj_child_status:4 //error
                 }
                 });

          }

        	//context.write(itm.id,1); //Key-Value pair ? - why?
            context.write(searchResult.id, 1);
        }
		
		function reduce(context){
			log.debug('reduce', 'reduce | ' + context.key + ' : ' + context.values);
			context.write(context.key, context.values);
		}

        function summarize(summary)
        {
            /*var mapKeys= [];
            summary.mapSummary.keys.iterator().each(function(key){
                mapKeys.push(key);
                return true;

            });
            log.debug('mapKeys', JSON.stringify(mapKeys));*/
			
			var parentIds = [];
			summary.output.iterator().each(function(key, value){
				parentIds.push(key);
				return true;
			});
			log.debug('parentIds', JSON.stringify(parentIds));

          //Set the progress for the Price adjustment scheduler to done
        	
            log.debug('summarizing...');
            var type = summary.toString();
            log.debug('summary ',JSON.stringify(summary));
            log.debug(type + ' Usage Consumed', summary.usage);
            log.debug(type + ' Number of Queues', summary.concurrency);
            log.debug(type + ' Number of Yields', summary.yields);
        }

        return {
            getInputData: getInputData,
            map: map,
			reduce: reduce,
            summarize: summarize
        };

//iterate
        function sendEmail(recordId) {
            var senderId = -5;
            var recipientEmail = 'timothy.frazer@kitandace.com';
            var timeStamp = new Date().getUTCMilliseconds();
            var recipientId = 305596; // temp

            email.send({
                author: senderId,
                recipients: recipientId,
                subject: 'Price adjustment scheduler', // include name of price adjustment
                body: 'email body'
                // what should be in the email body?
            });
        }

    });
