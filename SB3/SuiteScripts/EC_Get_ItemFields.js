/**
 * Company            Explore Consulting
 * Copyright          2015 Explore Consulting, LLC
 * Type               NetSuite EC_Get_ItemFields
 * Version            1.0.0.0
 **/


// Function to get an item's field values based on the internal id of each field
function getItemFields(request, response)
{
    nlapiLogExecution('debug', 'getItemFields', 'start');

    try
    {
        var params = request.getAllParameters()
        ,   internalids = params['internalid'].split(',')
        ,   fields = params['fields'].split(',')
        ,   columns = new Array();

        nlapiLogExecution('debug', 'internalids', internalids);
        
        for (var i = 0; i < fields.length; i++) {
            columns.push(new nlobjSearchColumn(fields[i]));
        }

        
        
        var filters = [
            new nlobjSearchFilter('internalid', null, 'anyof', internalids)
        ]
        ,   searchresults = nlapiSearchRecord('inventoryitem', null, filters, columns);

nlapiLogExecution('debug', 'searchresults', JSON.stringify(searchresults));

        if (searchresults)
        {
            response.write(JSON.stringify(searchresults));
        }
        
    }
    catch(e)
    {
        nlapiLogExecution('DEBUG', 'getItemFields', 'Unexpected Error occurred while processing:  ' + e);
        
    }
}