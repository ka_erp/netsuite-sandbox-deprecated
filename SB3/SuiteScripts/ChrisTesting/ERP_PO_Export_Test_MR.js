/**
 * @NApiVersion 2.0
 * @NScriptType MapReduceScript
 */
define(['N/search', 'N/record', 'N/email', 'N/runtime', 'N/error'],
    function(search, record, email, runtime, error)
    {
        
		function getInputData()
	    {
			log.debug('getInputData');
			
			var poSearch = search.load({
				id: 'customsearch_po_3pl_export_search'
			});

			return poSearch;
	    }	
	
        function map(context)
        {
        	log.debug('map');
        	
        	var searchResult = JSON.parse(context.value);
        	var poId = searchResult.id;
        	
        	log.debug('poId: ' + poId);
        	
        	context.write(poId, 1); //Key-Value pair
        }

        function reduce(context)
        {
        	log.debug('reduce');
        	
        	var poId = context.key;
        	
        	var id = record.submitFields({
        		type: record.Type.PURCHASE_ORDER,
        		id: poId,
        		values: {
        			custbody_po_export_3pl_status: 4 //4 - Sent to 3PL
        		},
        		options: {
        			enableSourcing: false,
        	        ignoreMandatoryFields : true
        		}
        	});
        }

        function summarize(summary)
        {
        	log.debug('summarizing...');
            var type = summary.toString();
            log.debug(type + ' Usage Consumed', summary.usage);
            log.debug(type + ' Number of Queues', summary.concurrency);
            log.debug(type + ' Number of Yields', summary.yields);
        }

        return {
            getInputData: getInputData,
            map: map,
            reduce: reduce,
            summarize: summarize
        };
    });

