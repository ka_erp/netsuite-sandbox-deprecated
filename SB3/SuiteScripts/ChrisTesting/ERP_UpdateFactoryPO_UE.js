/**
 *	File Name		:	ERP_UpdateFactoryPO_UE.js
 *	Function		:	
 *	Prepared by		:	 
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{
	STATUS_SENT = 4;
	STATUS_SUCCESS = 2;
}


function afterSubmit_performExport(type){
	nlapiLogExecution('DEBUG', 'context', nlapiGetContext().getExecutionContext());
	nlapiLogExecution('DEBUG', 'type', type);
	
	if (type == 'xedit'){
		
		var exportStatus = nlapiGetFieldValue('custbody_po_export_3pl_status');
		nlapiLogExecution('DEBUG', 'exportStatus', 'exportStatus');
		
		if (exportStatus == STATUS_SENT){
			var recordid = nlapiGetRecordId();
			var tranId = nlapiLookupField(nlapiGetRecordType(), recordid , 'tranid');	  		
	  		var status =  nlapiLookupField(nlapiGetRecordType(), recordid, 'status');	  		
	  		
	  		//var recordValues =  InvokeCloudhub(nlapiGetRecordType(), nlapiGetRecordId(), tranId, status); 	//call to send to Record ID to Mulesoft/Cloudehub to integrate the record.
	  																											//call to WTKA_Library.js
	  		
	  		var startProcessUrl = nlapiResolveURL('SUITELET', 'customscript_mapreducepractisesuitelet', 'customdeploy_mapreducepractisesuitelet') + '&recordid=' + recordid + '&tranid=' + tranId;
	  		startProcessUrl = 'https://forms.sandbox.netsuite.com' + startProcessUrl; 
	  		nlapiLogExecution('DEBUG', 'startProcessUrl', startProcessUrl);
	        var status = nlapiRequestURL(startProcessUrl); //trigger the processing of the records
	        nlapiLogExecution('DEBUG', 'status', status);
	        
	        var curRecord = nlapiLoadRecord('purchaseorder', recordid);
	        curRecord.setFieldValue('custbody_po_export_3pl_status', STATUS_SUCCESS);
	        var id = nlapiSubmitRecord(curRecord);
	  		
	        nlapiLogExecution('DEBUG', 'DONE', id);
	        //nlapiSetFieldValue('custbody_po_export_3pl_status', STATUS_SUCCESS);
		}
	}
}









