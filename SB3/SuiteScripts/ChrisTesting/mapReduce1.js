/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 */
 
define(['N/file','N/https'],
    function(file) {
        const PUNCTUATION_REGEXP = /[\u2000-\u206F\u2E00-\u2E7F\\'!"#\$%&\(\)\*\+,\-\.\/:;<=>\?@\[\]\^_`\{\|\}~]/g;
        function getInputData() {
        	log.debug('getting input data...');
            return "the quick brown fox \njumped over the lazy dog.".split('\n');
        }
        function map(context) {
        	log.debug('mapping...');
            for (var i = 0; context.value && i < context.value.length; i++){
            	if (context.value[i] !== ' ' && !PUNCTUATION_REGEXP.test(context.value[i])){
            		context.write(context.value[i], 1);
            		log.debug(i + ': ' + context.value[i]);
            	}                    
            }            	                
//            var response = https.get({
//                url: 'https://www.google.com'
//            });
            //log.debug(JSON.stringify(response));
        }
        function reduce(context) {
        	log.debug('reducing...');
            context.write(context.key, context.values.length);
            log.debug(context.key + ': ' + context.values.length);            
        }
        function summarize(summary) {
        	log.debug('summarizing...');
            var type = summary.toString();
            log.audit(type + ' Usage Consumed', summary.usage);
            log.audit(type + ' Number of Queues', summary.concurrency);
            log.audit(type + ' Number of Yields', summary.yields);
            var contents = '';
            summary.output.iterator().each(function(key, value) {
                contents += (key + ' ' + value + '\n');
                return true;
            });
            var fileObj = file.create({
                name: 'wordCountResult.txt',
                fileType: file.Type.PLAINTEXT,
                contents: contents
            });
            //fileObj.folder = -15; //suitescripts folder
            fileObj.folder = 682318; //ChrisTesting in SB3
            fileObj.save();
            
            
        }
        return {
            getInputData: getInputData,
            map: map,
            reduce: reduce,
            summarize: summarize
        };
    });