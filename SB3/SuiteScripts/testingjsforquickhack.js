function stripOutTaxFromPriceSetForm(type) {

	var KA_CASH_CRASH_RETURN_FORM_INTERNALID = '189';
	var KA_CASH_CRASH_NEW_SALE_FORM_INTERNALID = '188';

	var transactionFormInternalID = nlapiGetFieldValue('customform');

	if (transactionFormInternalID == KA_CASH_CRASH_RETURN_FORM_INTERNALID
			|| transactionFormInternalID == KA_CASH_CRASH_NEW_SALE_FORM_INTERNALID) {

	

		var subId = nlapiGetSubsidiary();
	
		try {
	
			if (subId == 7 || subId == 8) {
	
				nlapiGetLineItemField('item', 'price').setDisplayType('normal');
			} else {
				nlapiGetLineItemField('item', 'price').setDisplayType('hidden');
			}
	
		} catch (ex) {
		}
	
	}

}