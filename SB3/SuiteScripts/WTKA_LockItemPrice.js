/**
 *	File Name		:	WTKA_LockItemPrice.js
 *	Function		:	Disable change of prices based on role
 * 	Authors			:	Rini Thomas
 *	Company			:	Wipro Limited
 *	Release Dates	:	DD-MMM-2016 (v1.0)
 * 	Current Version	:	1.0
**/

//Set role restriction based on Deployment Audience - Run only for roles to be blocked
//Set price level restriction based on script parameter
function disablePriceChange(type) //before submit	
{
	if(type == 'edit' || type == 'xedit')
	{
		var oldRecord = nlapiGetOldRecord();
		var disableSet = ['price1', 'price2', 'price3', 'price4', 'price5', 'price6', 'price7', 'price8'];
		
		var priceLevels 	 	= nlapiGetContext().getSetting('SCRIPT', 'custscript_wtka_pricelevel_blocked');
		var blockedPriceLevels 	= (priceLevels != null && priceLevels.length > 0) ? priceLevels.split(',') : null;
		// nlapiLogExecution('debug', 'blockedPriceLevels', JSON.stringify(blockedPriceLevels));
		var changeFlag = false;
		for(var i=0; i<disableSet.length; i++)
		{
			var field = oldRecord.getAllLineItemFields(disableSet[i]);
			for(var j=1; j<=nlapiGetLineItemCount(disableSet[i]); j++)
			{
				var priceLevel = String(oldRecord.getLineItemValue(disableSet[i], 'pricelevel', j));
				var oldValue   = oldRecord.getLineItemValue(disableSet[i], 'price_1_', j);
				var newValue   = nlapiGetLineItemValue(disableSet[i], 'price_1_', j);
				if((blockedPriceLevels != null && blockedPriceLevels.indexOf(priceLevel) > -1) && (oldValue != newValue))
				{
					changeFlag = true;
					nlapiLogExecution('debug', 'changeFlag: ' + changeFlag, 'oldValue: ' + oldValue + '  || ' + 'newValue: ' + newValue);
					// nlapiSetLineItemValue(disableSet[i], 'price_1_', j, oldValue); // Revert the change
					break;
				}
			}
		}
		if(changeFlag)
		{
			var err = nlapiCreateError("INSUFFICIENT_PERMISSION", "Price level is locked. Price cannot be changed!", true);
			throw err;
		}
	}	
}