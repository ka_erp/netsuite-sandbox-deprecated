/**
 *	File Name		:	ERP_WPORT_HYB_Order_001_RL.js
 *	Function		:	Order Process - Step 1 (Inbound Integration Entry Point, Creates/Updates a WTKA_External_Orders record, Stores Order object in file)
 * 	Remarks			:	Relevant scripts isolated from WTKA_NetSuite_ICTO_Process.js
 *	Prepared by		:	A.Villanueva
 *	Release Dates	:	31-May-2016
 * 	Current Version	:	1.0
**/

function postOrderRequest(dataIn) // Entry point for Order RESTLet
{
	nlapiLogExecution('DEBUG', 'dataIn', JSON.stringify(dataIn));

	//Get the Order ID from the dataIn, if it exists
	var tranId;
	try{
		tranId = dataIn.order.orderHeader.orderId;
	} catch (tranExc) {
		tranId = '';
	}


	try
	{	
		if(PrevalidationCheck(dataIn))
		{
			if(createLog(dataIn))
			{
				var finalResp 	  = new Object();
				finalResp.status  = "Success";
				finalResp.message = "Requested accepted and will be processed shortly.";
				nlapiLogExecution('DEBUG', 'finalResponse', JSON.stringify(finalResp));
				finalMessage.records.push(finalResp);
				createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, null, SYNC_STATUS.CREATED, JSON.stringify(finalMessage), null, 10); //Library call to WTKA_Library.js //Record Type: 12 is Inventory Transfer
			}
			else
			{
				finalMessage.records.push(ErrorObj);
				nlapiLogExecution('DEBUG', 'orderRequest-else', JSON.stringify(finalMessage));
				var message = '<br><b>Error Details: </b><br>' + JSON.stringify(ErrorObj);
				sendOrderMail('Order', message, dataIn);
				createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, null, SYNC_STATUS.ERROR, JSON.stringify(finalMessage), null, 11); //Library call to WTKA_Library.js //Record Type: 12 is Inventory Transfer
			}
			
		}
		else
		{
			ediStatus 	  = (ErrorObj.status == "Error") ? 2 : 3; // 2- Error, 3-Exception
			finalMessage.status = ErrorObj.status;
			finalMessage.records.push(ErrorObj);
			nlapiLogExecution('DEBUG', 'Error', JSON.stringify(finalMessage));
			var message = '<br><b>Error Details: </b><br>' + JSON.stringify(ErrorObj);
			sendOrderMail('Order', message, dataIn);
			createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, null, SYNC_STATUS.ERROR, JSON.stringify(finalMessage), null, 12); //Library call to WTKA_Library.js //Record Type: 12 is Inventory Transfer
		}
	}
	catch(err)
	{
		var finalResp 	  = new Object();
		finalResp.status  = "Exception";
		finalResp.message = "Unable to Process Orders. Error Details: ";
		finalResp.message = err.getCode() + ':' + err.getDetails();
		finalMessage.records.push(finalResp);
		nlapiLogExecution('DEBUG', 'finalMessage', JSON.stringify(finalMessage));
		var message = '<br><b>Error Details: </b><br>Code: ' + err.getCode() + ' Details: ' + err.getDetails();
		sendOrderMail('Order', message, dataIn);
		createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, null, SYNC_STATUS.ERROR, JSON.stringify(finalMessage), null, 13); //Library call to WTKA_Library.js //Record Type: 12 is Inventory Transfer
	}
	return finalMessage;
}

function PrevalidationCheck(dataIn)
{
	ediStatus 		= 2;
	ErrorObj.status = "Error";
	try
	{
		//Pre-Validation checks on DataIn
	
		//1. Check if dataIn is empty
		if(!emptyInbound(dataIn))	return false;

		//2. Check if dataIn has header level objects values set
		if(!emptyObject(dataIn.order, 				"Order"))					return false;
		if(!emptyObject(dataIn.order.orderHeader, 	"orderHeader")) 			return false;
		if(!emptyObject(dataIn.order.orderDetail, 	"orderDetail"))				return false;
		if(!emptyObject(dataIn.order.shipment, 		"Shipment"))				return false;
		
		//3.Check if objects have lines
		if(!containLines(dataIn.order.orderDetail, 	"orderDetail present")) 	return false;
		if(!containLines(dataIn.order.shipment, 	"shipment present")) 		return false;
		
		//4.Check if fields have values
		if(!emptyObject(dataIn.order.orderHeader.orderId, 	  											"OrderId"))					return false;
		if(!emptyObject(dataIn.order.orderHeader.orderStatus, 	  										"OrderStatus"))				return false;
		if(!emptyObject(dataIn.order.orderHeader.orderTransactionDt, 	 								"OrderTransactionDt"))		return false;
		
		if(!emptyObject(dataIn.order.orderHeader.billingCustomer, 										"BillingCustomer")) 		return false;
		if(!emptyObject(dataIn.order.orderHeader.billingCustomer.locale, 	 							"Locale")) 					return false;
		if(!emptyObject(dataIn.order.orderHeader.billingCustomer.customerEmail, 	 					"CustomerEmail")) 			return false;
		
		if(!emptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress,						"BillingAddress"))	 		return false;
		if(!emptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.billingFirstName,		"BillingFirstName"))		return false;
		if(!emptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.billingLastName,		"BillingLastName"))			return false;
		
		
		var locale  		= dataIn.order.orderHeader.billingCustomer.locale.split('/');
		locale = locale[locale.length-1].toUpperCase(); //dataIn.order.orderHeader.sellingLocation.locationCode.toUpperCase();
		
		
		/*if(!emptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.billingAddressLine1,	"BillingAddressLine1"))	 	return false;
		if(!emptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.cityName,				"BillingCity"))				return false;

		if(locale != 'NZ' || locale != 'UK' || locale != 'GB')
		{
			if(!emptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.provinceName,			"BillingProvinceName"))		return false;	
		}
		
		if(!emptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.postalCode,				"BillingPostalCode"))		return false;*/
		if(!emptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.countryName,			"BillingCountry"))			return false;
		
		if(!emptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress,						"ShippingAddress"))	 		return false;
		if(!emptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.recipientFirstName,	"ShippingFirstName"))		return false;
		if(!emptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.recipientLastName,		"ShippingLastName"))		return false;
		/*if(!emptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.shippingAddressLine1,	"ShippingAddressLine1"))	return false;
		if(!emptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.cityName,				"ShippingCity"))			return false;

		if(locale != 'NZ' || locale != 'UK'|| locale != 'GB')
		{
			if(!emptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.provinceName,			"ShippingProvinceName"))	return false;
		}
		
		if(!emptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.postalCode,			"ShippingPostalCode"))		return false;*/
		if(!emptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.countryName,			"ShippingCountry"))			return false;
			
		if(!emptyObject(dataIn.order.orderHeader.paymentInfo, 											"PaymentInfo")) 			return false;
		if(!emptyObject(dataIn.order.orderHeader.paymentInfo.tenderType, 								"TenderType"))	 			return false;
		
		if(!emptyObject(dataIn.order.orderHeader.sellingLocation, 										"SellingLocation")) 		return false;
		if(!emptyObject(dataIn.order.orderHeader.sellingLocation.locationCode, 	 						"SellingLocationCode"))		return false;
		
		//- Check if orderStatus is Created/Released/Shipped/Complete
		if(dataIn.order.orderHeader.orderStatus == 'Created' || dataIn.order.orderHeader.orderStatus == 'Shipped')
		{
			ErrorObj.messages[0] 			 = new Object();
			ErrorObj.messages[0].messagetype = "Invalid Order Status";
			ErrorObj.messages[0].message 	 = "Order cannot be created as the order "  + dataIn.order.orderHeader.orderId + " is in " + dataIn.order.orderHeader.orderStatus + " status.";
			return false;
		}
		
		var FinalUPCCodes = new Array();
		for(var i in dataIn.order.orderDetail)
		{
			if(!emptyObject(dataIn.order.orderDetail[i].orderLineNumber,				"OrderLineNumber"))  	return false;
			if(!emptyObject(dataIn.order.orderDetail[i].countItemOrdered,				"CountItemOrdered"))  	return false;
			if(!emptyObject(dataIn.order.orderDetail[i].item,							"Item"))  				return false;
			if(!emptyObject(dataIn.order.orderDetail[i].item.SKUId,						"SKUId"))  				return false;
			if(!emptyObject(dataIn.order.orderDetail[i].amtItemPrice,					"AmtItemPrice"))  		return false;
			if(!emptyObject(dataIn.order.orderDetail[i].amtItemPrice.amtCurrentPrice,	"AmtCurrentPrice"))		return false;
			//if(!emptyObject(dataIn.order.orderDetail[i].tax,							"Tax"))					return false;
			//if(!containLines(dataIn.order.orderDetail[i].tax, 							"Tax data present"))	return false;
		}
		
		for(var i in dataIn.order.shipment)
		{
			if(!emptyObject(dataIn.order.shipment[i].shipmentHeader,					"ShipmentHeader"))  			return false;
			//if(!emptyObject(dataIn.order.shipment[i].shipmentHeader.shipmentId,			"ShipmentId"))  				return false;
			//if(!emptyObject(dataIn.order.shipment[i].shipmentHeader.shipmentCode, 		"ShipmentCode"))	 			return false;
			//if(!emptyObject(dataIn.order.shipment[i].shipmentHeader.shippingMethod, 	"ShippingMethod"))	 			return false;
			//if(!emptyObject(dataIn.order.shipment[i].shipmentHeader.trackingNumber, 	"TrackingNumber"))	 			return false;
			if(!emptyObject(dataIn.order.shipment[i].shipmentDetail, 					"ShipmentDetail"))	 			return false;
			if(!containLines(dataIn.order.shipment[i].shipmentDetail, 					"shipmentDetails present")) 	return false;
			
			for(var j in dataIn.order.shipment[i].shipmentDetail)
			{
				if(!emptyObject(dataIn.order.shipment[i].shipmentDetail[j].orderDetailId, 		"OrderDetailId"))	 	return false;
				if(!emptyObject(dataIn.order.shipment[i].shipmentDetail[j].quantityOrdered, 	"QuantityOrdered"))	 	return false;
				if(!emptyObject(dataIn.order.shipment[i].shipmentDetail[j].quantityShipped, 	"QuantityShipped"))	 	return false;
			}
		}
		/* Validate dates */
		if(!validateISODate(dataIn.order.orderHeader.orderTransactionDt))	return false;
		
		return true;
	}
	catch(validate_err)
	{
		ErrorObj.status					 = "Exception";
		ErrorObj.messages[0] 			 = new Object();
		ErrorObj.messages[0].messagetype = "Validation Failed";
		ErrorObj.messages[0].message 	 = "Error: " + validate_err;
		var message = '<br><b>Error Details: </b><br>Code: ' + JSON.stringify(ErrorObj);
		sendOrderMail('Order', message, dataIn);
		createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 12, SYNC_STATUS.ERROR, JSON.stringify(finalMessage), null, 14); //Library call to WTKA_Library.js //Record Type: 12 is Inventory Transfer
		return false;
	}
}

function createLog(dataIn)
{
	try
	{
		var orderStatus = 0, fulfillFlag = false, errorFlag = false;
		var ExtOrderId  = dataIn.order.orderHeader.orderId;
		nlapiLogExecution('DEBUG', 'ExtOrderId', ExtOrderId);
		
		var cols = new Array();
		cols.push(new nlobjSearchColumn('custrecord_wtka_orderid'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_inbound_request'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_request_counter'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_order_status'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_completed_request'));
		
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_wtka_orderid', null, 'haskeywords', ExtOrderId));
		
		var extRecord 	 = 0, requestFileId = 0, requestCounter = 0, fileId = 0, completedStatus;
		var searchRecord = nlapiSearchRecord('customrecord_wtka_external_orders', null, filters, cols);
		if(searchRecord != null)
		{
			var found = false;
			for(var s=0; !found && s<searchRecord.length; s++)
			{
				var orderValue = searchRecord[s].getValue('custrecord_wtka_orderid');
				if(orderValue == ExtOrderId)
				{
					requestFileId   = searchRecord[s].getValue('custrecord_wtka_inbound_request');
					requestCounter  = searchRecord[s].getValue('custrecord_wtka_request_counter');
					orderStatus 	= searchRecord[s].getValue('custrecord_wtka_order_status');
					extRecord 		= nlapiLoadRecord('customrecord_wtka_external_orders', searchRecord[s].getId());
					completedStatus	= searchRecord[s].getValue('custrecord_wtka_completed_request');
					found			= true;
				}
			}
			if(!found)
			{
				extRecord   = nlapiCreateRecord('customrecord_wtka_external_orders');
				orderStatus = 1; //Request Accepted
			}
		}
		else
		{
			extRecord   = nlapiCreateRecord('customrecord_wtka_external_orders');
			orderStatus = 1; //Request Accepted
		}
		
		if(requestFileId == 0)
		{
			var filters 	= new Array();
			filters.push(new nlobjSearchFilter('name', 		null, 'is',		folderName));

			var fileCabinet = nlapiSearchRecord('folder', 	null, filters, 	null);
			var folderId   	= (fileCabinet != null) ? fileCabinet[0].getId() : 0;

			var requestArray 	 = new Object();
			requestArray.records = new Array();
			
			requestArray.records.push(dataIn);
			
			var newFile = nlapiCreateFile(ExtOrderId, 'PLAINTEXT', JSON.stringify(requestArray));
			newFile.setFolder(folderId);
			fileId 		= nlapiSubmitFile(newFile);
		}
		else
		{
			var extFile 	= nlapiLoadFile(folderName + '/' + ExtOrderId);
			var fileContent = JSON.parse(extFile.getValue());
			
			if(dataIn.order.orderHeader.orderStatus == 'Completed')
			{
				if(completedStatus == 'F') //Check for duplicate Order Completed requests
				{
					if(orderStatus == 2) //Inventory Transfer Complete
					{
						if(orderPrevalidationCheck(dataIn))
						{
							extRecord.setFieldValue('custrecord_wtka_customerid', customer);
							orderStatus = 3;
							extRecord.setFieldValue('custrecord_wtka_completed_request', 'T');
						}
						else
						{
							ErrorObj.messagetype 	= "Invalid Order Request";
							nlapiLogExecution('debug', 'Error', JSON.stringify(ErrorObj));
							errorFlag = true;
						}
					}
					else
					{
						ErrorObj.messages[0] 			 	= new Object();
						ErrorObj.messages[0].messagetype 	= "Invalid Request Status";
						ErrorObj.messages[0].message 		= "Request cannot be processed as the current status is " + searchRecord[0].getText('custrecord_wtka_order_status');
						errorFlag = true;
					}
				}
				else
				{
					nlapiLogExecution('DEBUG', 'Duplicate Request');
					ErrorObj.messages[0] 			 	= new Object();
					ErrorObj.messages[0].messagetype 	= "Duplicate request";
					ErrorObj.messages[0].message 		= "Completed Order request already present for order " + dataIn.order.orderHeader.orderId;
					errorFlag = true;
				}
			}
			else
			{
				if(completedStatus == 'T' || orderStatus > 2) // Order Completed
				{
					nlapiLogExecution('DEBUG', 'Invalid Order Request');
					ErrorObj.messages[0] 			 	= new Object();
					ErrorObj.messages[0].messagetype 	= "Invalid Order Request";
					ErrorObj.messages[0].message 		= "Order " + dataIn.order.orderHeader.orderId + " has already been processed.";
					errorFlag = true;
				}
				else	orderStatus = 1;
			}
			fileContent.records.push(dataIn);
			
			var updatedFile = nlapiCreateFile(extFile.getName(), 'PLAINTEXT', JSON.stringify(fileContent));
			updatedFile.setFolder(extFile.getFolder());
			fileId 			= nlapiSubmitFile(updatedFile);
			requestCounter++;
		}
		
		if(!errorFlag)
		{
			extRecord.setFieldValue('custrecord_wtka_order_status', 	orderStatus);
			extRecord.setFieldValue('custrecord_wtka_orderid', 			ExtOrderId);
		}
		
		extRecord.setFieldValue('custrecord_wtka_inbound_request', 	fileId);
		extRecord.setFieldValue('custrecord_wtka_request_counter', 	requestCounter);
		extRecord.setFieldValue('externalid',ExtOrderId);
		nlapiLogExecution('DEBUG', 'WTKA External ID set', ExtOrderId);
		var recId = nlapiSubmitRecord(extRecord);
		
		if(errorFlag)	return false;
		return true;
	}
	catch(log_err)
	{
		ErrorObj.status						= "Exception";
		ErrorObj.messages[0]				= new Object();
		ErrorObj.messages[0].messagetype 	= "Log Creation Error"
		ErrorObj.messages[0].message 		= "Error in log creation. " + log_err
		nlapiLogExecution('DEBUG', 'Error in Log Creation', log_err);
		var subject = 'Order processing failure';
		var message = '<br><b>Error Details: </b><br>Code: ' + JSON.stringify(ErrorObj);
		sendOrderMail('Order', message, dataIn);
		return false;
	}
}

function orderPrevalidationCheck(dataIn)
{
	ediStatus 		= 2;
	ErrorObj.status = "Error";
	try
	{
		if(dataIn.order.orderHeader.orderStatus != 'Completed')
		{
			ErrorObj.messages[0]			 = new Object();
			ErrorObj.messages[0].messagetype = "Order not Completed";
			ErrorObj.messages[0].message 	 = "Order cannot be created as the order "  + dataIn.order.orderHeader.orderId + " is not in Completed status but is in " + dataIn.order.orderHeader.orderStatus + " status.";
			return false;
		}
				
		var zeroShipped = 0;
		for(var i in dataIn.order.shipment)
		{
			/* Validate dates */
			if(!emptyObject(dataIn.order.shipment[i].shipmentHeader.shipDt, 	 				"ShipDt"))	 			return false;
			else if(!validateISODate(dataIn.order.shipment[i].shipmentHeader.shipDt, 			'shipDt'))				return false;
			
			if(dataIn.order.shipment[i].shipmentHeader.shipReceivedDt != null && dataIn.order.shipment[i].shipmentHeader.shipReceivedDt != '')
			{
				if(!validateISODate(dataIn.order.shipment[i].shipmentHeader.shipReceivedDt, 'shipReceivedDt'))	return false;
			}
			if(dataIn.order.shipment[i].shipmentHeader.expectedShipDate != null && dataIn.order.shipment[i].shipmentHeader.expectedShipDate != '')
			{
				if(!validateISODate(dataIn.order.shipment[i].shipmentHeader.expectedShipDate, 'expectedShipDate'))	return false;
			}
			
			/* Check for empty Shipments */ //Create Orders and no Item Fulfillments
			var shippedCount = 0;
			for(var j in dataIn.order.shipment[i].shipmentDetail)
			{
				if(dataIn.order.shipment[i].shipmentDetail[j].quantityShipped > 0)	shippedCount++;
			}
			
			if(shippedCount == 0)	zeroShipped++;
						
			/* Check for PO Vendor */
			var locale  		= dataIn.order.orderHeader.billingCustomer.locale.split('/');
			var sellingLocation = locale[locale.length-1].toUpperCase();
			if(sellingLocation != 'CA')	sellingLocation = 'US';
			/* Comment Out Reason: Replaced with dynamic logic using Script Config settings
			var poVendor = 0;
			switch(sellingLocation)
			{
				case 'CA':
					switch(dataIn.order.shipment[i].shipmentHeader.fulfillingLocation.country.toUpperCase())
					{
						case 'US':
							poVendor	 =  CA_US_Vendor; //SW US Inc. - Inventory - Canada (Inventory)
							break;
						case 'CA': //eComm
							poVendor	 =  1;
							break;
					}
					break;
				case 'US':
					switch(dataIn.order.shipment[i].shipmentHeader.fulfillingLocation.country.toUpperCase())
					{
						case 'CA':
							poVendor	 =  US_CA_Vendor; //Kit and Ace Designs Inc. - USA (Inventory)
							break;
						case 'US': //eComm
							poVendor	 =  1;
							break;
					}
					break;
			}*/
			var poVendor = validatePOVendor(sellingLocation, dataIn.order.shipment[i].shipmentHeader.fulfillingLocation.country.toUpperCase()); //Dynamic logic
			if(poVendor == 0)
			{
				ErrorObj.messages[0] 		  		= new Object();
				ErrorObj.messages[0].messagetype 	= "PO Vendor Error";
				ErrorObj.messages[0].message 	 	= "Request cannot be processed as the corresponding PO Vendor could not be found";
				return false;
			}
			
			createLogFlag = true; //Reset flag
			
			orderShipID[i] = dataIn.order.orderHeader.orderId + '_' + dataIn.order.shipment[i].shipmentHeader.shipmentId;
		
			var orderID = FetchOrderId('salesorder', orderShipID[i], 'ext', 'tranid', 'nonIcto');
			if(orderID != 0)
			{
				ErrorObj.messages[0] 		  		= new Object();
				ErrorObj.messages[0].messagetype 	= "Duplicate request";
				ErrorObj.messages[0].message 	 	= "Request cannot be processed as the order " + orderShipID[i] + " already exists " + orderID.tranid + ".";
				return false;
			}
		}
		
		if(zeroShipped == dataIn.order.shipment.length)
		{
			ErrorObj.messages[0] 		  		= new Object();
			ErrorObj.messages[0].messagetype 	= "Empty Shipment";
			ErrorObj.messages[0].message 	 	= "Request cannot be processed as the all shipments have zero items shipped.";
			return false;
		}
			
		/* Lookup Customer */
		var locale  = dataIn.order.orderHeader.billingCustomer.locale.split('/');
		var country = locale[locale.length-1].toUpperCase();
		
		/* Lookup Tax Code */
		var country_val = country;
		if(country_val != 'CA')	country_val = 'US';
		var state  	 = dataIn.order.orderHeader.billingCustomer.shippingAddress.provinceName.split('-');
		var province = state[state.length-1].toUpperCase();
		taxCode  	 = FetchTaxCode(country_val, province);
		if(taxCode < 0)
		{
			ErrorObj.messages[0] 		  		= new Object();
			ErrorObj.messages[0].messagetype 	= "Invalid Tax Code";
			ErrorObj.messages[0].message 	 	= "Request cannot be processed as the tax code / group for  " + province + " is not found.";
			return 0;
		}
		
		customer = lookupCustomer(dataIn);
		if(customer == 0)	return false;
		location = (country == 'CA') ? CA_Virtual : US_Virtual;
		
		/* Lookup Items */
		var FinalUPCCodes = new Array();
		for(var i in dataIn.order.orderDetail)
		{
			FinalUPCCodes[i] = dataIn.order.orderDetail[i].item.SKUId;
		}
		FinalItemIDs = FetchIdFromName('item', FinalUPCCodes);
		if(FinalItemIDs[0] == 'Error')	return false
		
		/* Lookup payment method */
		extSysPayment = LookupPayment(dataIn.order.orderHeader.paymentInfo.tenderType, dataIn.order.orderHeader.paymentInfo.currencyCode);
		if(extSysPayment == Ext_Payment)
		{
			ErrorObj.messages[0] 			 = new Object();
			ErrorObj.messages[0].messagetype = "Payment error";
			ErrorObj.messages[0].message 	 = "Invalid query. Error: Payment Method for " + dataIn.order.orderHeader.paymentInfo.tenderType + " (" + dataIn.order.orderHeader.paymentInfo.currencyCode + ")  cannot be found.";
			return false;
		}
		return true;
	}
	catch(validate_err)
	{
		ErrorObj.status					 = "Exception";
		ErrorObj.messages[0] 			 = new Object();
		ErrorObj.messages[0].messagetype = "Validation Failed";
		ErrorObj.messages[0].message 	 = "Error: " + validate_err;
		var message = '<br><b>Error Details: </b><br>Code: ' + JSON.stringify(ErrorObj);
		sendOrderMail('Order', message, dataIn);
		createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 12, SYNC_STATUS.ERROR, JSON.stringify(ErrorObj), null, 15); //Library call to WTKA_Library.js //Record Type: 12 is Inventory Transfer
		return false;
	}
}