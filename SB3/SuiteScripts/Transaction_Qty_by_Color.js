Transaction = {
    context: nlapiGetContext(),
    beforeSubmit: function (type) {
        'use strict';
        var order_context = nlapiGetContext().getExecutionContext();
		var customForm = nlapiGetFieldValue('customform');
		var userId = nlapiGetUser();
		nlapiLogExecution('DEBUG', 'beforeSubmit', 'userId: ' + userId);
		
		var erpLineCount = nlapiGetLineItemCount('item');
				
		if(erpLineCount <= 100){
		
			//if(userId != 298465 || userId != 1898){

			nlapiLogExecution('DEBUG', 'beforeSubmit', 'IN type: ' + type + ' recordType: ' + nlapiGetRecordType() + ' userId:' + userId );
			nlapiLogExecution('AUDIT', 'beforeSubmit', ' context: ' + order_context + ' type:' + nlapiGetRecordType() + ' id:' + nlapiGetRecordId()); 

			var lineIndex, color, qty;
			if (this.isNotBlank(type) && (type.toString() === 'create' || type.toString() === 'edit' || type.toString() === 'copy')) {
				nlapiLogExecution('DEBUG', 'beforeSubmit', 'Processing Lines');
				lineIndex = parseInt(nlapiGetLineItemCount('item'), 10);
				nlapiLogExecution('DEBUG', 'beforeSubmit', 'lineIndex:' + lineIndex.toString());
				while (lineIndex > 0) {
					//For legacy transactions
					if (!this.isNumber(nlapiGetLineItemValue('item', 'custcol_prewfxcolor', lineIndex))) {
						nlapiLogExecution('DEBUG', 'beforeSubmit', 'legacy trigger');
						nlapiSetLineItemValue('item', 'custcol_prewfxcolor', lineIndex, nlapiLookupField('item', nlapiGetLineItemValue('item', 'item', lineIndex), 'custitem3'));
					}
					color = nlapiGetLineItemText('item', 'custcol_prewfxcolor', lineIndex);
					nlapiLogExecution('DEBUG', 'beforeSubmit', 'color: ' + color);
					qty = this.isNumber(nlapiGetLineItemValue('item', 'quantity', lineIndex)) ? parseInt(nlapiGetLineItemValue('item', 'quantity', lineIndex), 10) : -1;
					if (this.isNotBlank(color) && qty > 0 && (nlapiGetRecordType() !== 'itemfulfillment' || (nlapiGetRecordType() === 'itemfulfillment' && nlapiGetLineItemValue('item', 'itemreceive', lineIndex) === 'T'))) {
						nlapiLogExecution('DEBUG', 'beforeSubmit', 'Working on line: ' + lineIndex.toString() + ' color: ' + color);
						this.summary.addColor(color, qty);
					}
					lineIndex = lineIndex - 1;
				}
				nlapiLogExecution('DEBUG', 'beforeSubmit', 'setting custbody_po_quantity');
				nlapiSetFieldValue('custbody_po_quantity', this.summary.message());
			}
			nlapiLogExecution('DEBUG', 'beforeSubmit', 'OUT');
			
			//}
		}
		
    },
    isBlank: function (s) {
        return s === undefined || s === null || s === '' || s.length < 1;
    },
    isNotBlank: function (s) {
        return !this.isBlank(s);
    },
    isNumber: function (n) {
        return this.isNotBlank(n) && !isNaN(parseInt(n, 10)) && isFinite(n);
    },
    summary: {
        colors: [],
        total: 0,
        addColor: function (colorName, qty) {
            var foundColor = _.findWhere(this.colors, {
                name: colorName
            });
            if (foundColor) {
                //nlapiLogExecution('DEBUG', 'summary', 'Adding ' + qty.toString() + ' to color: ' + foundColor.name);
                foundColor.total += qty;
            } else {
                //nlapiLogExecution('DEBUG', 'summary', 'Initializing ' + qty.toString() + ' to color: ' + colorName);
                this.colors.push({
                    name: colorName,
                    total: qty
                });
            }
            this.total += qty;
        },
        message: function () {
            var output = '';
            _.each(this.colors, function (color) {
                output += 'Unit Count - ' + color.name + ': ' + color.total + '\n';
            });
            if (this.colors.length > 0) {
                output += 'Total Unit Count: ' + this.total;
            }
            //nlapiLogExecution('DEBUG', 'summary', 'message ' + output);
            return output;
        }
    },
    updateLegacy: function (type) {
        var lineIndex, record, records, searchId;
        searchId = this.context.getSetting('SCRIPT', 'custscript_trans_searchid');
        records = jSuite.runSearch({
            searchId: searchId
        }).Results;

        if (records.length > 0) {
            record = nlapiLoadRecord(records[0].recordType.val, records[0].id.val);
            lineIndex = parseInt(record.getLineItemCount('item'), 10);
            while (lineIndex > 0) {
                //We need around 1000 Units to complete the operations
                if (parseInt(this.context.getRemainingUsage(), 10) < 1000) {
                    nlapiSubmitRecord(record);
                    nlapiScheduleScript('customscript_dh_update_legacy_trans', 'customdeploy_dh_update_legacy_trans');
                }
                if (!this.isNumber(record.getLineItemValue('item', 'custcol_prewfxcolor', lineIndex))) {
                    record.setLineItemValue('item', 'custcol_prewfxcolor', lineIndex, nlapiLookupField('item', record.getLineItemValue('item', 'item', lineIndex), 'custitem3'));
                }
                lineIndex = lineIndex - 1;
            }
            nlapiSubmitRecord(record);
            nlapiScheduleScript('customscript_dh_update_legacy_trans', 'customdeploy_dh_update_legacy_trans');
        }
    }
};

//NOTES:
// 1 - Update Sandbox Item field from Color to PreWFXColor
// 2 - Added new transaction Column Field - Sourcing in the PreWFXColor from the Item selected
// 2a -Had to set display to 'Normal', otherwise I cannot read the Color value on the Item Fulfillment record