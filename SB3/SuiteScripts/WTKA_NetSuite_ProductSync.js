/**
 *	File Name		:	WTKA_NetSuite_ProductSync.js
 *	Function		:	GET and POST methods for Product synchronization with External System 
 * 	Authors			:	Rini Thomas, Vinod Kumar
 *	Company			:	Wipro Limited
 *	Release Dates	:	5-May-2016 (v1.0)
 * 	Current Version	:	1.0
**/

{
	var ErrorObj    		= new Object();
	ErrorObj.status    		= "Error";
	ErrorObj.messages    	= new Array();
	ErrorObj.messages[0]  	= new Object();
	var returnObject   		= new Object();
	returnObject.response  	= new Object();
	var retObject 			= new Object();
	
	var counterFlag   		= false,			scriptLimit			= false,	sendEmail 			= true; //false;
	var lastProcessedId  	= 0,				fileCounter   		= 0, 		governanceMinUsage  = 100,			recordLimit			= 950;;
	var crRequest, 			crResourceId, 		crFileId, 			crStatus,	crFileCounter, 		crRequestId,	crLastProcessed,	processFromDate, crRequestObj;
	var folderName			= 'External System Synchronization';
	var toList 				= ['3PL_eComm_Integration@kitandace.com'];
	var	ccList 				= null; //['']; //Enter CC Addresses as array
	var subject 			= 'Product Synchronization failure';
	var body 				= 'Hello,<br><br>';
	body 					+= 'Product Synchronization failed in NetSuite due to below error.<br>';
}

function get_WTKA_NetSuiteProductData(dataIn)
{
	try
	{
		returnObject = getNetSuiteSyncData(dataIn, 2);
	}
	catch(get_err)
	{
		sendSyncMail(sendEmail, subject, body, get_err, dataIn);
		returnObject.response = get_err;
		nlapiLogExecution('debug', 'GET error', get_err);
	}
	return returnObject;
}

function post_WTKA_NetSuite_ProductSync(dataIn)
{
	try
	{
		nlapiLogExecution('DEBUG', 'Inbound Request', JSON.stringify(dataIn));
		var filters 	= new Array();
		filters.push(new nlobjSearchFilter('name', null, 'is', folderName));
		var fileCabinet = nlapiSearchRecord('folder', null, filters, null);
		var folderId	= (fileCabinet != null) ? fileCabinet[0].getId() : 0;
		
		if(ValidateRequest(dataIn)) //Queued
		{
			try
			{
				var newFile = nlapiCreateFile(dataIn.resourceId, 'PLAINTEXT', '');
				newFile.setFolder(folderId);
				var fileId = nlapiSubmitFile(newFile);

				var ProductUpdate = nlapiCreateRecord('customrecord_wtka_extsys_sync');
				ProductUpdate.setFieldValue('custrecord_wtka_extsys_sync_request',  	JSON.stringify(dataIn));
				ProductUpdate.setFieldValue('custrecord_wtka_extsys_sync_resourceid', 	dataIn.resourceId);
				ProductUpdate.setFieldValue('custrecord_wtka_extsys_sync_status',  		0); // 0 = not started / In Queue, // 1 = In Progress, // 2 = Completed, // 3 = Error 
				ProductUpdate.setFieldValue('custrecord_wtka_extsys_sync_type',  		2); // Product Sync Type
				ProductUpdate.setFieldValue('custrecord_wtka_extsys_sync_file',     	fileId); // File ID
				var updateRecordId  = nlapiSubmitRecord(ProductUpdate);

				returnObject.response.status  	= "PENDING";
				returnObject.response.eta   	= "10 mins";
				nlapiLogExecution('DEBUG', 'responseObject', JSON.stringify(returnObject));
			}
			catch(err)
			{
				returnObject.response.message = "ERROR: " + err;
				sendSyncMail(sendEmail, subject, body, err, dataIn);
				nlapiLogExecution('debug', 'POST error', err);
			}
			return returnObject;
		}
		else
		{
			nlapiLogExecution('DEBUG', 'ErrorObj ', JSON.stringify(ErrorObj));
			sendSyncMail(sendEmail, subject, body, ErrorObj, dataIn);
			return ErrorObj;
		}
	}
	catch(post_err)
	{
		ErrorObj.status    		= "Exception";
		ErrorObj.messages[0]  	= post_err;
		sendSyncMail(sendEmail, subject, body, post_err, dataIn);
		nlapiLogExecution('debug', 'POST error', post_err);
		return ErrorObj;
	}
}

function ValidateRequest(dataIn)
{
	try
	{
		if(!emptyInbound(dataIn)) 															return false;
		if(!emptyObject(dataIn.resourceId,            			"ResourceId"))    			return false; 
		if(!emptyObject(dataIn.productRequest,        			"ProductRequest"))   		return false; 

		//Validate Date
		if(dataIn.productRequest.fromDate != null && dataIn.productRequest.fromDate != '')
		{
			if(!validateISODate(dataIn.productRequest.fromDate))	return false;
		}
		
		var resourceId     		= dataIn.resourceId;
		var filters   			= new Array();
		filters.push(new nlobjSearchFilter('custrecord_wtka_extsys_sync_resourceid', 	null, 'is', resourceId));
		filters.push(new nlobjSearchFilter('custrecord_wtka_extsys_sync_type', 		null, 'is', 2));
		
		var updateRecord   = nlapiSearchRecord('customrecord_wtka_extsys_sync', null, filters, null);
		if(updateRecord != null && updateRecord.length > 0)
		{
			ErrorObj.messages[0].messagetype = "Bad Request";
			ErrorObj.messages[0].message   = "ResourceId already exists. Please try again with a new ResourceId."
			return false;
		}
		return true;
	}
	catch(validate_Err)
	{
		sendSyncMail(sendEmail, subject, body, validate_Err, dataIn);
		nlapiLogExecution('debug', 'Validate error', validate_Err);
		return false;
	}
}

function FetchProductRecords()
{
	try
	{
		/*** Batch Statuses:
		0 = not started / In Queue
		1 = In Progress
		2 = Completed
		3 = Error ***/

		//1. Fetch data from custom record based on status 0 and 1
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_wtka_extsys_sync_status', null, 'lessthan', 	2));
		filters.push(new nlobjSearchFilter('custrecord_wtka_extsys_sync_type', 	 null, 'is', 		2)); //Product Sync

		//Fetch records which are in Progress first and then by Internal id
		var cols = new Array();
		cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_request'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_resourceid'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_file'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_filecounter'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_lastprocess'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_timeoutcount'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_status').setSort(true)); //Fetch records which are in Progress first and then by Internal id
		cols.push(new nlobjSearchColumn('internalid').setSort());
		
		var searchRecords = nlapiSearchRecord('customrecord_wtka_extsys_sync', null, filters, cols);
		if(searchRecords != null)
		{
			nlapiLogExecution('debug', 'searchRecords-Fetch', searchRecords.length);
			for(var i = 0; nlapiGetContext().getRemainingUsage() > governanceMinUsage && searchRecords != null && i < searchRecords.length ; i++)
			{
				// Fetch record and split parameters to be processed
				crRequest 		= searchRecords[i].getValue('custrecord_wtka_extsys_sync_request');
				crResourceId 	= searchRecords[i].getValue('custrecord_wtka_extsys_sync_resourceid');
				crFileId 		= searchRecords[i].getValue('custrecord_wtka_extsys_sync_file');
				crFileCounter 	= searchRecords[i].getValue('custrecord_wtka_extsys_sync_filecounter');
				crLastProcessed = searchRecords[i].getValue('custrecord_wtka_extsys_sync_lastprocess');
				crStatus 		= searchRecords[i].getValue('custrecord_wtka_extsys_sync_status');
				crTimeOutCount	= searchRecords[i].getValue('custrecord_wtka_extsys_sync_timeoutcount');
				crRequestId 	= searchRecords[i].getId();

				crTimeOutCount 	= (crTimeOutCount != null && crTimeOutCount.length > 0) ? crTimeOutCount : 0;
				crRequestObj 	= JSON.parse(crRequest);
				processFromDate = crRequestObj.productRequest.fromDate;
				
				fileCounter 	= crFileCounter;
				while(crStatus == 0 || crStatus == 1)
				{
					if(nlapiGetContext().getRemainingUsage() > governanceMinUsage)
					{
						// Update record status to 1 to indicate 'In Progress'
						if (crStatus == 0)
						{
							nlapiSubmitField('customrecord_wtka_extsys_sync', crRequestId, 'custrecord_wtka_extsys_sync_status', 1, false);
							crStatus = 1; //Set the field to In progress
						}

						// Get Total count of records based on the request
						var totalLoops = 0;
						try
						{
							nlapiLogExecution('debug', 'crLastProcessed', crLastProcessed);
							var searchResult = performSearch(processFromDate, crLastProcessed, 'T');
							if(searchResult != null)
							{
								//Extract total count and divide by 1000, which provides 'n' number of loopings to be done.
								nlapiLogExecution('DEBUG', 'total Results', searchResult.length);
								totalLoops = searchResult[0].getValue('internalid', null, 'count');
								nlapiLogExecution('DEBUG', 'totalLoops - ID', totalLoops); //remove
								totalLoops = parseInt(totalLoops/1000) + 2; 
								nlapiLogExecution('DEBUG', 'totalLoops', totalLoops);
								
								//3. PayLoad function to append to array and submit to file.
								BuildProductPayLoad(totalLoops);
								if(scriptLimit)
								{
									SwitchScheduleScript(2);
									break;
								}
							}
							else
							{
								nlapiLogExecution('DEBUG', 'LENGTH', '0');
								crStatus = 2; //Completed state
								updateCustomRecord();
								fileCounter 	= 0;
								lastProcessedId = 0;
							}
						}
						catch(err)
						{
							nlapiLogExecution('debug', 'Error in Processing request', crRequestId);
							nlapiLogExecution('debug', 'Error Details', err);
							crStatus = 3;
							updateCustomRecord();
							fileCounter 	= 0;
							lastProcessedId = 0;
							sendSyncMail(sendEmail, subject, body, err, crRequestObj);
							break;
						}
					}		
				}
				if(scriptLimit)
				{
					scriptLimit = false;
					break;
				}
			}
		}
		else
		{
			nlapiLogExecution('DEBUG', 'PROCESS', 'No records to process. All requests have been processed');
		}
	}
	catch(fetch_err)
	{
		sendSyncMail(sendEmail, subject, body, fetch_err, '');
		nlapiLogExecution('debug', 'Fetch error', fetch_err);
	}
}

function BuildProductPayLoad(loopCounter)
{
	try
	{
		var finalFileContents 	= '';
		var lastProcessed 		= 0;
		var recordCounter 		= 0;
		var productObject		= new Array();

		for(var i=0; scriptLimit != true && i < loopCounter; i++)
		{
			nlapiLogExecution('DEBUG', 'loopCounter', parseInt(i+1));
			
			var finalResult = performSearch(processFromDate, crLastProcessed, 'F');
		
			if(finalResult != null)
			{
				nlapiLogExecution('DEBUG', 'Total Results - subresults', finalResult.length);
				for(var j=0; j < finalResult.length && recordCounter < recordLimit; j++)
				{
					var productSync 					= new Object();
					productSync.SKUId 					= finalResult[j].getValue('upccode');
					productSync.SKUCode 				= finalResult[j].getValue('itemid').split(':')[1];
					productSync.SKUName 				= finalResult[j].getValue('displayname');
					productSync.sizeCode 				= finalResult[j].getText('custitem_prewfxsize');
					productSync.sizeDescription 		= "";
					productSync.styleDescription 		= finalResult[j].getValue('custitem_ka_item_style_description');
					productSync.styleStatus 			= "New"; //ENUM
					productSync.inheritanceDescription 	= "Child"; //ENUM
					productSync.styleColourVersionName 	= finalResult[j].getValue('custitem_wfx_color_version'); //ENUM
					productSync.fitAttributeDescription = "";
					productSync.technologyDescription 	= "";
					// productSync.fabricationDescription 	= "";
					productSync.influencerDescription 	= "";
					productSync.climateDescription		= "";
					productSync.productStory 			= "";
					productSync.productManager 			= ""; 
					productSync.productDesigner 		= "";
					productSync.productDeveloper 		= "";
					productSync.productPatternMaker 	= "";
					productSync.lineName 				= finalResult[j].getText('custitem_wfx_line'); //ENUM
					productSync.typeName 				= finalResult[j].getText('custitem_wfx_type'); //ENUM
					productSync.genderDescription 		=  finalResult[j].getText('custitem_wfx_gender'); 
					productSync.uom 					= finalResult[j].getText('custitem_wfx_uom'); //ENUM
					productSync.avgCost 				= 0; //quantityParse(finalResult[j].getValue('averagecost'));
					
					productSync.masterStyle 			= new Object();
					productSync.masterStyle.code 		= finalResult[j].getValue('custitem_ka_item_master_style'); 
					productSync.masterStyle.name 		= finalResult[j].getValue('custitem_ka_item_master_style');  
					
					productSync.masterStyle.season 		= new Object();
					productSync.masterStyle.season.code = finalResult[j].getValue('custitem_wfx_season');
					productSync.masterStyle.season.name = finalResult[j].getText('custitem_wfx_season');
					
					productSync.masterStyle.productBrand 	  = new Object();
					productSync.masterStyle.productBrand.code = "";
					productSync.masterStyle.productBrand.name = "";
					
					productSync.masterStyle.productDivision 	 = new Object();
					productSync.masterStyle.productDivision.code = finalResult[j].getValue('custitem_wfx_division'); 
					//productSync.masterStyle.productDivision.name = finalResult[j].getTextValue('custitem_wfx_division');  //20160316 IDJS
									 productSync.masterStyle.productDivision.name = finalResult[j].getValue('custitem_wfx_division');           //20160316 IDJS
					
					productSync.masterStyle.productDepartment 	   = new Object();
					productSync.masterStyle.productDepartment.code = ""; 
					productSync.masterStyle.productDepartment.name = ""; 
					
					productSync.masterStyle.productClass 	  = new Object();
					productSync.masterStyle.productClass.code = "";
					productSync.masterStyle.productClass.name = "";
					
					productSync.masterStyle.productSubClass 	 = new Object();
					productSync.masterStyle.productSubClass.code = ""; 
					productSync.masterStyle.productSubClass.name = ""; 
					
					productSync.season 		= new Object();
					productSync.season.code = finalResult[j].getValue('custitem_wfx_season');
					productSync.season.name = finalResult[j].getText('custitem_wfx_season');
					
					productSync.style 	   = new Object();
					productSync.style.code = "";  
					productSync.style.name = "";
					
					productSync.style.season 	  = new Object();
					productSync.style.season.code = "";
					productSync.style.season.name = "" ;
					
					productSync.style.styleColours 			 = new Array();
					productSync.style.styleColours[0] 		 = new Object();
					productSync.style.styleColours[0].code 	 = finalResult[j].getValue('custitem_color_version_code'); 
					productSync.style.styleColours[0].name 	 = finalResult[j].getValue('custitem_wfx_colorname');
					productSync.style.styleColours[0].version = finalResult[j].getValue('custitem_wfx_color_version');

					productSync.vendors = getVendorList(finalResult[j].getId());
					
					productSync.colourWay 				  = new Object(); 
					productSync.colourWay.code 			  = finalResult[j].getValue('custitem_wfx_color_code');
					productSync.colourWay.name 			  = finalResult[j].getValue('custitem_wfx_colorname');
					productSync.colourWay.version 		  = finalResult[j].getValue('custitem_wfx_color_version'); //Number
					productSync.colourWay.type 			  = "";  
					productSync.colourWay.colourReference = finalResult[j].getValue('custitem_wfx_color_reference'); //Number
					
					productSync.colourWay.colourGroup 	   = new Object(); 
					productSync.colourWay.colourGroup.code = finalResult[j].getValue('custitem_wfx_color_group_code');
					productSync.colourWay.colourGroup.name = finalResult[j].getValue('custitem_wfx_color_group');
					
					productSync.colourWay.colourWaySubGroup 	 = new Object();  
					productSync.colourWay.colourWaySubGroup.code = "";  
					productSync.colourWay.colourWaySubGroup.name = "";  

					productSync.price  = getPriceList(finalResult[j].getId());
					
					productObject.push(productSync);
					recordCounter++;
					
					lastProcessedId = finalResult[j].getValue('internalid'); 
					lastProcessed 	= lastProcessedId;
					crLastProcessed = lastProcessedId;
					if(finalResult.length < 1000) counterFlag = true;
					if(nlapiGetContext().getRemainingUsage() < governanceMinUsage)
					{
						counterFlag = true;
						scriptLimit = true;
						nlapiLogExecution('DEBUG', 'Breakpoint - Governance', recordCounter);
						break;
					}
				}
			}		
			else
			{
				nlapiLogExecution('DEBUG', 'No results', 'No contents');
				crStatus 		= 2;
				updateCustomRecord();
				fileCounter 	= 0;
				lastProcessedId = 0;
				//Reset globals before breaking
				crRequest = 0, crResourceId = '', crFileId = 0, crFileCounter = 0, crLastProcessed = 0, crRequestId = '', crTimeOutCount = 0;
				break;
			}
			if(recordCounter >= recordLimit)	nlapiLogExecution('DEBUG', 'Breakpoint - Record Limit', recordCounter);
			nlapiLogExecution('DEBUG', 'Final - recordCounter', recordCounter);
			if(recordCounter == recordLimit || counterFlag)
			{
				if(productObject.length > 0)
				{
					var resp = JSON.stringify(productObject);
					// nlapiLogExecution('DEBUG', 'resp length', resp.length);
					
					nlapiLogExecution('DEBUG', 'Inside file creation loop', 'Inside file creation loop');
					//nlapiLogExecution('DEBUG', 'Governance Limit-Before File Creation', nlapiGetContext().getRemainingUsage());
					var fileUpdate 	= nlapiLoadFile(folderName + '/' + crResourceId);
					var fileContent = fileUpdate.getValue();
					
					var newFileName = fileUpdate.getName();	
					if(fileCounter > 0) newFileName += '_' + parseInt(fileCounter);
					nlapiLogExecution('DEBUG', 'newFileName: ' + newFileName, 'fileCounter: ' + fileCounter);
					
					var updatedFile = nlapiCreateFile(newFileName, 'PLAINTEXT', JSON.stringify(productObject));
					updatedFile.setFolder(fileUpdate.getFolder());
					var idupdate = nlapiSubmitFile(updatedFile);
					nlapiLogExecution('DEBUG', 'Governance Limit-After File Creation', nlapiGetContext().getRemainingUsage());
					fileCounter++;

					//Update CustomRecord
					updateCustomRecord();
					
					// Reset Values
					productObject 	= new Array();
					recordCounter 	= 0;
					scriptLimit		= true;
					counterFlag 	= false;
					break;
				}
			}
			if(scriptLimit)	break;
		}
	}
	catch(build_err)
	{
		sendSyncMail(sendEmail, subject, body, build_err, crRequestObj);
		nlapiLogExecution('debug', 'Build error', build_err);
	}
}

function performSearch(dateValue, lastProcessed, initCall)
{
	try
	{
		//Base filters
		var filters = new Array();
		filters.push(new nlobjSearchFilter('type', 	 null, 'anyof',  'InvtPart')); 
		filters.push(new nlobjSearchFilter('parent', null, 'noneof', '@NONE@')); 
		//filters.push(new nlobjSearchFilter('custitem_wfx_season', null, 'noneof', [3,31,32,24,5,30,6,7,25])); //TEMP								//Blacklist        	
		filters.push(new nlobjSearchFilter('custitem_wfx_season', null, 'anyof', [40,34,38,39,29]));                                                 //Whitelist
		//filters.push(new nlobjSearchFilter('custitem_erp_hybris_filter', null, 'is', 'T')); //Send 120 Select
		
		if(dateValue != null && dateValue != '')
		{
			newDateValue = moment(dateValue).format('l LT');
			filters.push(new nlobjSearchFilter('modified', 			null, 'after', 		newDateValue)); 
		}
		//else Initial Product Sync-no date filter
		
		if(lastProcessed > 0)
		{
			filters.push(new nlobjSearchFilter('internalidnumber', 	null, 'greaterthan', lastProcessed)); 
		}
		
		var cols = new Array();
		if(initCall == 'F')
		{
			cols.push(new nlobjSearchColumn('internalid').setSort());
			cols.push(new nlobjSearchColumn('itemid'));
			cols.push(new nlobjSearchColumn('upccode'));
			cols.push(new nlobjSearchColumn('displayname'));
			cols.push(new nlobjSearchColumn('custitem_prewfxsize')); 
			cols.push(new nlobjSearchColumn('custitem_ka_item_style_description')); 
			cols.push(new nlobjSearchColumn('custitem_wfx_line')); 
			cols.push(new nlobjSearchColumn('custitem_wfx_type'));
			cols.push(new nlobjSearchColumn('custitem_wfx_gender'));  
			cols.push(new nlobjSearchColumn('custitem_wfx_uom'));
			cols.push(new nlobjSearchColumn('custitem_ka_item_master_style'));
			cols.push(new nlobjSearchColumn('custitem_wfx_season'));
			cols.push(new nlobjSearchColumn('custitem_wfx_division'));
			cols.push(new nlobjSearchColumn('custitem_color_version_code'));
			cols.push(new nlobjSearchColumn('custitem_wfx_colorname'));
			cols.push(new nlobjSearchColumn('custitem_wfx_color_version'));
			cols.push(new nlobjSearchColumn('custitem_wfx_color_code'));
			cols.push(new nlobjSearchColumn('custitem_wfx_color_reference'));
			cols.push(new nlobjSearchColumn('custitem_wfx_color_group_code'));
			cols.push(new nlobjSearchColumn('custitem_wfx_color_group'));
		}
		else
		{
			cols.push(new nlobjSearchColumn('internalid', null, 'count'));
		}

		var searchResult = nlapiSearchRecord('item', null, filters, cols);
		return searchResult;
	}
	catch(performSearch_err)
	{
		sendSyncMail(sendEmail, subject, body, performSearch_err, crRequestObj);
		nlapiLogExecution('debug', 'Perform Search error', performSearch_err);
		return null;
	}
}
	
function getVendorList(itemId)
{
	var Vendors = new Array();
	try
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', itemId));

		var cols = new Array();
		cols.push(new nlobjSearchColumn('entityid', 'vendor'));
		var itemRecord = nlapiSearchRecord('item', null, filters, cols);
		if(itemRecord != null)
		{
			for(var v=0; v<itemRecord.length; v++)
			{
				var vendorList 				 = new Object();
				vendorList.code 			 = (itemRecord[v].getValue('entityid', 'vendor') != null) ? itemRecord[v].getValue('entityid', 'vendor') : "";
				vendorList.name 			 = (itemRecord[v].getValue('entityid', 'vendor') != null) ? itemRecord[v].getValue('entityid', 'vendor') : "";
				vendorList.altCode 			 = ""; 
				vendorList.factories 		 = new Array(); 
				vendorList.factories[0] 	 = new Object(); 
				vendorList.factories[0].code = "";
				vendorList.factories[0].name = ""; 
				Vendors.push(vendorList);
			}
		}
		else
		{
			var vendorList 				 = new Object();
			vendorList.code 			 = "";
			vendorList.name 			 = "";
			vendorList.altCode 			 = ""; 
			vendorList.factories 		 = new Array(); 
			vendorList.factories[0] 	 = new Object(); 
			vendorList.factories[0].code = "";
			vendorList.factories[0].name = ""; 
			Vendors.push(vendorList);
		}
	}
	catch(vendor_err)
	{
		sendSyncMail(sendEmail, subject, body, vendor_err, crRequestObj);
		nlapiLogExecution('debug', 'Vendor List error', vendor_err);
	}
	return Vendors;
}

function getPriceList(itemId)
{
	var Prices = new Array();
	try
	{
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', itemId));
		filters.push(new nlobjSearchFilter('pricelevel', 'pricing', 'is', [1, 5, 56]));	// 1- Base Price, 5- Online Price, 56-Sale Price
		
		var cols = new Array();
		cols.push(new nlobjSearchColumn('unitprice',  'pricing'));
		cols.push(new nlobjSearchColumn('currency',   'pricing').setSort());
		cols.push(new nlobjSearchColumn('pricelevel', 'pricing').setSort());
		var itemRecord = nlapiSearchRecord('item', null, filters, cols);
		
		if(itemRecord != null)
		{
			var records 	= false;
			var records1 	= false;
			for(var p=0; p<itemRecord.length; p++)
			{
				var priceArray = new Array();

				if(records)			records  = false;
				else if(records1)	records1 = false;
				else
				{
					for(var i=0; i<3 && (p+i)<itemRecord.length; i++)
					{
						if(i > 0 && itemRecord[p+i].getValue('currency', 'pricing') != priceArray[i-1].currency)	break;
						priceArray[i] = new Object();
						priceArray[i].currency 		=  itemRecord[p+i].getValue('currency', 'pricing');
						priceArray[i].price    		= (itemRecord[p+i].getValue('unitprice', 'pricing') != null) ? parseFloat(itemRecord[p+i].getValue('unitprice', 'pricing')) : 0.00;
						priceArray[i].priceLevel 	=  itemRecord[p+i].getValue('pricelevel', 'pricing');
					}
					
					if(priceArray.length > 1)	records1 = true;
					if(priceArray.length > 2)	records = true;
					
					var priceList = new Object();
					// priceList.priceId 			= "0";
					priceList.currencyCode 		= itemRecord[p].getText('currency', 'pricing');
					
					var prices = getPrices(priceArray, priceArray.length, false);
					
					priceList.amtCurrentPrice 	= prices.currentPrice;
					priceList.amtPromoPrice 	= prices.promoPrice;
					
					priceList.priceEffectiveStartDt = null;
					priceList.priceEffectiveEndDt 	= null;
					priceList.lastModifiedByUser 	= "";
					priceList.lastModifiedTimestamp = "";
					Prices.push(priceList);
				}
			}
		}
		else
		{
			var priceList = new Object();
			// priceList.priceId 				= "0";
		
			priceList.amtCurrentPrice 		= 0.00;
			priceList.currencyCode 			= "";
			priceList.amtPromoPrice 		= 0.00;
			priceList.priceEffectiveStartDt = null;
			priceList.priceEffectiveEndDt 	= null;
			priceList.lastModifiedByUser 	= "";
			priceList.lastModifiedTimestamp = "";
			Prices.push(priceList);
		}
	}
	catch(price_err)
	{
		sendSyncMail(sendEmail, subject, body, price_err, crRequestObj);
		nlapiLogExecution('debug', 'Price List error', price_err);
	}
	return Prices;
}

function getPrices(priceArray, arrayLength, recall)
{
	if(!recall)	retObject = new Object();
	try
	{
		switch(arrayLength)
		{
			case 1:
				if(recall)
				{
					retObject.currentPrice = (priceArray[0].priceLevel == 5)   ? priceArray[0].price : null;
					recall = false;
				}
				else
				{
					retObject.currentPrice = (priceArray[0].priceLevel != 56)  ? priceArray[0].price : 0;
				}
				retObject.promoPrice 	= (priceArray[0].priceLevel == 56)   ? priceArray[0].price 		: null;
				break;
			case 2:
				getPrices(priceArray, 1, true);
				retObject.currentPrice 	= (retObject.currentPrice != null)   ? retObject.currentPrice   : (priceArray[1].priceLevel == 5)  ? priceArray[1].price : priceArray[0].price;
				retObject.promoPrice   	= (retObject.promoPrice != null) 	 ? retObject.promoPrice 	: (priceArray[1].priceLevel == 56) ? priceArray[1].price : null;
				break;
			case 3:
				getPrices(priceArray, 2, true);
				retObject.currentPrice 	= (retObject.currentPrice != null)   ? retObject.currentPrice   : (priceArray[2].priceLevel == 5)  ? priceArray[2].price : 0;
				retObject.promoPrice   	= priceArray[2].price;
				// retObject.promoPrice   	= (retObject.promoPrice != null) 	? retObject.promoPrice 	: (priceArray[2].priceLevel == 56) ? priceArray[2].price : null;
				break;
			default:
				retObject.currentPrice  = 0;
				retObject.promoPrice 	= null;
		}
	}
	catch(prices_err)
	{
		sendSyncMail(sendEmail, subject, body, prices_err, crRequestObj);
		nlapiLogExecution('debug', 'Price error', prices_err);
	}
	return retObject;
}