function ViewGiftCardList(request, response)
{
	var unprocRecorList = nlapiCreateForm('Unprocessed Records');
	unprocRecorList.setTitle('Unprocessed Gift Card Records');

	var recList			= unprocRecorList.addSubList('recordlist', 'inlineeditor', 'Unprocessed Invoices');
	var recId 			= recList.addField('wtka_recid', 			'select', 		'Invoice Record', 	'transaction');
	// recId.setDisplayType('disabled');
	var errorMsg 		= recList.addField('wtka_error', 			'textarea', 	'Error Message');
	// recId.setDisplayType('disabled');
	var custRecId 			= recList.addField('wtka_custrecid', 	'select', 		'Custom Record', 	'customrecord_wtka_unprocessed_gift_rec');
	// custRecId.setDisplayType('disabled');
	var process 		= recList.addField('wtka_processed', 		'checkbox', 	'Processed?');
	
	//Fetch data based on Request parameters
	var filters = new Array();
	var cols 	= new Array();
	cols.push(new nlobjSearchColumn('custrecord_wtka_unprocessed_recid'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_unprocessed_gift_error'));
	// cols.push(new nlobjSearchColumn('custrecord_wtka_unprocessed_processed'));

	var searchResultChild = nlapiSearchRecord('customrecord_wtka_unprocessed_gift_rec', null, filters, cols);
	if(searchResultChild != null)
	{
		for(var j=0; j < searchResultChild.length; j++)
		{
			recList.setLineItemValue('wtka_recid', 		j+1, searchResultChild[j].getValue('custrecord_wtka_unprocessed_recid'));
			recList.setLineItemValue('wtka_error', 		j+1, searchResultChild[j].getValue('custrecord_wtka_unprocessed_gift_error'));
			recList.setLineItemValue('wtka_custrecid', 	j+1, searchResultChild[j].getId());
			// recList.setLineItemValue('wtka_processed', 	j+1, searchResultChild[j].getValue('custrecord_wtka_unprocessed_processed'));
		}
		unprocRecorList.addButton('select_all', 'Mark All', 'selectAllRecords();');
		unprocRecorList.addButton('deselect_all', 'Mark None', 'deselectAllRecords();');
		// unprocRecorList.addSubmitButton('Submit Records');
		
	}
	else
	{
		recList.setLineItemValue('wtka_recid', 1,	'No results found!');
	}
	response.writePage(unprocRecorList);
}

function selectAllRecords(){
  alert('Select');
}

function deselectAllRecords(){
  alert('Unselect');
}