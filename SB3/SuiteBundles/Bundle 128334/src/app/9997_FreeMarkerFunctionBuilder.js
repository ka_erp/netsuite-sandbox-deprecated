/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 May 2016     aalcabasa
 *
 * @NModuleScope TargetAccount
 */
define(['../data/9997_CompanyLOVDAO', '../lib/wrapper/9997_NsWrapperRuntime', '../data/9997_CompanyConfigDAO'],

function(lovDAO, runtime, companyDAO) {
    
    function buildFreeMarkerFunctions(settings) {
        var lovObj = lovDAO.retrieve();
        
        return {
            getCountryCodeFunction: getCountryCodeFunction(lovObj.countries),
            getStateCodeFunction: getStateCodeFunction(lovObj.states),
            getAmountFunction: getAmountFunction(settings),
            computeTotalAmountFunc: getComputeTotalAmountFunc()
        };
    }
    
    /**
     * Gets country code function to be appended in FreeMarker template library    
     *
     * @returns {String}      - country code function with FreeMarker syntax
     */
    function getCountryCodeFunction(countries) {
        var countryCodeFunction = ['<#function getCountryCode (country)>', '      <#assign _2663_countryCodes = {} >'];
        
        for ( var key in countries) {
            countryCodeFunction.push('      <#assign _2663_countryCodes = _2663_countryCodes + {"'
                                     + countries[key].text + '":"' + countries[key].code + '"} >');
        }
        countryCodeFunction.push('      <#return _2663_countryCodes[country]>');
        countryCodeFunction.push('</#function>');
        
        return countryCodeFunction.join('\n');
    }
    
    /**
     * Gets getStateCode function to be appended in FreeMarker template library    
     *
     * @returns {String}      - getStateCode with FreeMarker syntax
     */
    function getStateCodeFunction(states) {
        var stateCodeFunction = ['<#function getStateCode (state)>', '      <#assign _2663_stateCodes = {} >']
        for (var i = 0, ii = states.length; i < ii; i++) {
            var state = states[i];
            stateCodeFunction.push('      <#assign _2663_stateCodes = _2663_stateCodes + {"' + state.text + '":"'
                                   + state.id + '"} >');
        }
        stateCodeFunction.push('      <#return _2663_stateCodes[state]>');
        stateCodeFunction.push('</#function>');
        return stateCodeFunction.join('\n');
    }
    
    /**
     * Retrieves get amount function to be appended in FreeMarker template library    
     *
     * @param    {String}    templateType    - format template type
     * @returns  {String}                    - getAmount function with FreeMarker syntax
     */
    // TODO learn how this is supposed to work and optimize
    function getAmountFunction(settings) {
        
        // check if multicurrency feature is enabled to determine foreign amount
        var foreignAmount = runtime.isMultiCurrency() ? 'payment.fxamount' : '""';
        
        // compare base currency and bank currency to determine which amount to retrieve
        var pfa = settings.pfa;
        var companyBank = settings.companyBank;
        
        var company = companyDAO.retrieve();
        var baseCurrency = runtime.isOW() ? companyBank.currency : company.baseCurrency
        var bankCurrency = companyBank.currency;// custrecord_2663_currency
        var exchangeRates = pfa.exchangeRates;// custrecord_2663_exchange_rates
        var sameBaseAndBankCurrency = exchangeRates && (baseCurrency === bankCurrency) ? 'true' : 'false';
        
        // build FreeMarker getAmount function
        var amountFunc = [
            '<#function getAmount (payment, multiCurrency=false)>',
            '   <#assign _2663_sameBaseAndBankCurrency = ' + sameBaseAndBankCurrency + '>',
            '   <#assign _2663_amountValue = payment.amount>',
            '   <#assign _2663_foreignAmount = ' + foreignAmount + '>',
            '   <#if multiCurrency && _2663_sameBaseAndBankCurrency>',
            '       <#assign _2663_foreignAmount = payment.formulacurrency>',
            '   </#if>',
            '   <#if _2663_foreignAmount?has_content>',
            '       <#assign _2663_amountValue = _2663_foreignAmount>',
            '   </#if>',
            '   <#if (_2663_amountValue < 0) >',
            '       <#assign _2663_amountValue = _2663_amountValue * -1 >',
            '   </#if>',
            '   <#return _2663_amountValue>',
            '</#function>'];
        
        return amountFunc.join('\n');
    }
    
    /**
     * Retrieves get compute total amount function to be appended in FreeMarker template library    
     * 
     * @param    {Array}     payments        - list of payments
     * @param    {Boolean}   multiCurrency   - if template is multi-currency  
     * @returns  {String}                    - get compute total amount function with FreeMarker syntax
     */
    function getComputeTotalAmountFunc() {
        var computeTotalAmountFunc = [
            '<#function computeTotalAmount payments, multiCurrency=false>',
            '    <#assign _2663_total = 0>',
            '    <#list payments as payment>',
            '        <#assign _2663_total = _2663_total + getAmount(payment, multiCurrency)>',
            '    </#list>',
            '    <#return _2663_total>',
            '</#function>'];
        
        return computeTotalAmountFunc.join('\n');
    }
    
    return {
        buildFreeMarkerFunctions: buildFreeMarkerFunctions
    };
    
})
