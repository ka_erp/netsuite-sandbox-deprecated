/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 May 2016     aalcabasa
 *
 * @NModuleScope TargetAccount
 */
define([
    './9997_EFTDataSource',
    '../lib/wrapper/9997_NsWrapperRecord',
    '../lib/wrapper/9997_NsWrapperRuntime',
    '../lib/wrapper/9997_NsWrapperSearch',
    '../lib/wrapper/9997_NsWrapperFile',
    './9997_EPFreeMarkerAdapter',
    './9997_FreemarkerSearchColumnBuilder'],

function(epDataSource, record, runtime, search, file, freeMarkerAdapter, freemarkerSearchColumnBuilder) {
    
    var BASE_CURRENCY_ID = 1;// TODO add to constants
    
    var fileFormat;
    var templateSettings;
    
    function create(settings) {
        var pfaDAO = settings.pfaDAO;
        var companyBankDAO = settings.companyBankDAO;
        var currencyDAO = settings.currencyDAO;

        var pfaRecord;
        var companyBankRecord;
        var currencies;
        var sequences;
        var payments;
        var entities;
        var entityBanks;
        var transactions;
        
        var accountType = 'Non-Bank';
        
        freemarkerSearchColumnBuilder.initializeFreemarkerTemplate(getTemplate(settings));

        return {
            pfaId: settings.pfa.id,
            pfaNumber: settings.pfa.name,
            getPfa: function getPfa() {
                if (!pfaRecord) {
                    pfaRecord = pfaDAO.retrieveNsRecord(settings.pfa.id);
                }
                return pfaRecord;
            },
            
            getBankDetails: function getBankDetails() {
                if (!companyBankRecord) {
                    companyBankRecord = companyBankDAO.retrieveNsRecord(settings.companyBank.id);
                }
                return companyBankRecord;
            },
            
            getTemplateContent: function getTemplateContent() {
                return getTemplate(settings);
            },
            
            getCurrencies: function() {
                if (currencies) {
                    // Do nothing, we have what we need
                } else if (runtime.isMultiCurrency()) {
                    currencies = currencyDAO.getAllCurrencies();
                } else {
                    currencies = currencyDAO.retrieveNsRecord(BASE_CURRENCY_ID);
                }
                
                return currencies;
            },
            
            requiresSequences: function requiresSequences() {
                return getTemplateSettings(getTemplate(settings)).sequenceSettings.requiresSequences;
            },
            
            getSequences: function() {
                if (!sequences) {
                    var templateSettings = getTemplateSettings(getTemplate(settings));
                    settings.sameDaySequenceOnly = templateSettings.sequenceSettings.sameDaySequenceOnly;
                    sequences = pfaDAO.getSequences(settings);
                }
                return sequences;
            },
            
            getFileDetails: function getFileDetails() {
                var fileFormat = getFileFormat();
                var paymentFileDetails = settings.paymentFileDetails;
                var extension = fileFormat.outputFileExtension;
                
                var fileName = [];
                var prefix = paymentFileDetails.fileNamePrefix;
                
                if (prefix) {
                    fileName.push(prefix);
                }
                
                fileName.push(settings.pfa.name);
                fileName.push('.');
                fileName.push(extension);
                
                var isXML = extension === 'xml';
                
                var fileDetails = {
                    name: fileName.join(''),
                    fileType: isXML ? file.Type.XMLDOC : file.Type.PLAINTEXT,
                    folder: paymentFileDetails.folder,
                    encoding: fileFormat.outputFileEncodingText || file.Encoding.UTF8,
                    isXML: isXML
                };
                
                log.debug('EP File Details', JSON.stringify(fileDetails));
                
                return fileDetails;
            },
            
            getPayments: function getPayments() {
                if (!payments) {
                    var parameters = {
                        pfaId: settings.pfa.id,
                        searchColumns: freemarkerSearchColumnBuilder.getPaymentColumns(),
                        accountType: accountType
                    };

                    payments = this.retrievePayments(parameters);
                }

                return payments;
            },
            
            getEntities: function() {
                if (!entities) {
                    if (!payments) {
                        this.getPayments();
                    }

                    var parameters = {
                        entityIds: getEntityIds(payments),
                        searchColumns: freemarkerSearchColumnBuilder.getEntityColumns()
                    };

                    entities = this.retrieveEntities(parameters);
                }

                return entities;
            },
            
            getEntityBankDetails: function() {
                if (!entityBanks) {
                    if (!payments) {
                        this.getPayments();
                    }

                    var parameters = {
                        templateId: settings.paymentFileDetails.templateId,
                        entityIds: getEntityIds(payments),
                        searchColumns: freemarkerSearchColumnBuilder.getEbankColumns(),
                        accountType: accountType
                    };

                    entityBanks = this.retrieveEntityBankDetails(parameters);
                }

                return entityBanks;
            },
            
            getTransactions: function() {
                if (!transactions) {
                    if (!payments) {
                        this.getPayments();
                    }

                    var parameters = {
                        paymentIds: getPaymentIds(payments),
                        searchColumns: freemarkerSearchColumnBuilder.getTransactionColumns()
                    };

                    transactions = this.retrieveTransactions(parameters);
                }

                return transactions;
            },
        };
    }
    
    function getFileFormat(settings) {
        if (!fileFormat) {
            fileFormat = settings.templateDAO.retrieve(settings.paymentFileDetails.templateId);
        }
        return fileFormat;
    }
    
    function getTemplateSettings(templateContent) {
        if (!templateSettings) {
            templateSettings = freeMarkerAdapter.getTemplateSettings(templateContent);
        }
        return templateSettings;
    }
    
    var template;
    function getTemplate(settings) {
        if (!template) {
            var fmFunctions = settings.freeMarkerFunctionBuilder.buildFreeMarkerFunctions({
                pfa: settings.pfa,
                companyBank: settings.companyBank
            });
            
            var template = [
                settings.freeMarkerLibDAO.retrieveContents(),
                fmFunctions.getCountryCodeFunction,
                fmFunctions.getStateCodeFunction,
                fmFunctions.getAmountFunction,
                fmFunctions.computeTotalAmountFunc,
                getFileFormat(settings).freeMarkerBody];
            template = template.join('\n');
        }
        return template;
    }
    
    var entityIds;
    function getEntityIds(payments) {
        if (!entityIds) {
            var ids = getIdsFromPayments(payments);
            entityIds = ids.entityIds;
            paymentIds = ids.paymentIds;
        }
        
        return entityIds;
    }
    
    var paymentIds;
    function getPaymentIds(payments) {
        if (!paymentIds) {
            var ids = getIdsFromPayments(payments);
            entityIds = ids.entityIds;
            paymentIds = ids.paymentIds;
        }
        
        return paymentIds;
    }

    function getIdsFromPayments(payments) {
        var entityIds = [];
        var paymentIds = [];
        for (var i = 0; payments && i < payments.length; i++){
            var payment = payments[i];
            var entityId = payment.getValue('entity');
            if (entityIds.indexOf(entityId) === -1) {
                entityIds.push(entityId);
            }
            paymentIds.push(payment.id);
        }
        return {
            entityIds: entityIds,
            paymentIds: paymentIds
        };
    }
    
    return {
        create: create
    };
});
