/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Jun 2016     aalcabasa
 *
 * @NModuleScope TargetAccount
 */
define(['../data/9997_PaymentFileGenerationDAO', 'N/config'],

function(paymentFileGenerationDAO, config) {
    var CLIENT_SCRIPT_NAME = '9997_payment_file_generation_cs.js';
    
    function getBankAccounts() {
        return paymentFileGenerationDAO.getBankAccountsWithEFTFormats();
    }
    
    // Temp: These should be translated someday
    function create() {
        return {
            title: 'EP EFT - Bill Payment File Generation',
            clientScriptFile: CLIENT_SCRIPT_NAME,
            paymentFieldGroupLabel: 'EFT Payment Information',
            referenceNoteLabel: 'EFT File Reference Note',
            savedSearchFieldId: 'custscript_9997_file_gen_saved_search',
            getBankAccounts: getBankAccounts,
            defaultSavedSearch: 'customsearch_9997_payments_for_ep'
        };
    }
    
    return {
        create: create
    }
})
