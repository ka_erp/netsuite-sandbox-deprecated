/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount
 */

define([],

function () {
    var TRANSACTION_ID_FIELD = 'custrecord_2663_payments_for_process_id';
    var TRANSACTION_AMOUNT_FIELD = 'custrecord_2663_payments_for_process_amt';
    var DISCOUNTS_FIELD = 'custrecord_2663_payments_for_process_dis';
    var ENTITIES_FIELD = 'custrecord_2663_transaction_entities';
    var JOURNALS_FIELD = 'custrecord_2663_journal_keys';
    var PARTIAL_PAID_FIELD = 'custrecord_2663_partially_paid_keys';

    function transformPFA(pfaRecord) {
        var newPfa = createEmptyPFAList();

        newPfa[TRANSACTION_ID_FIELD] = parseArray(pfaRecord[TRANSACTION_ID_FIELD]);
        newPfa[TRANSACTION_AMOUNT_FIELD] = parseArray(pfaRecord[TRANSACTION_AMOUNT_FIELD]);
        newPfa[DISCOUNTS_FIELD] = parseArray(pfaRecord[DISCOUNTS_FIELD]);
        newPfa[ENTITIES_FIELD] = parseArray(pfaRecord[ENTITIES_FIELD]);
        newPfa[JOURNALS_FIELD] = parseArray(pfaRecord[JOURNALS_FIELD]);
        newPfa[PARTIAL_PAID_FIELD] = parseArray(pfaRecord[PARTIAL_PAID_FIELD]);
        return newPfa;
    }

    function addTransactionsToPFA(outputPFA, inputPfa, tranId) {
        var tranIndex = inputPfa[TRANSACTION_ID_FIELD].indexOf(tranId);
        if (tranIndex != -1) {
            outputPFA[TRANSACTION_ID_FIELD].push(inputPfa[TRANSACTION_ID_FIELD][tranIndex]);
            outputPFA[TRANSACTION_AMOUNT_FIELD].push(inputPfa[TRANSACTION_AMOUNT_FIELD][tranIndex]);
            outputPFA[DISCOUNTS_FIELD].push(inputPfa[DISCOUNTS_FIELD][tranIndex]);

            if (tranIndex < inputPfa[ENTITIES_FIELD].length) {
                outputPFA[ENTITIES_FIELD].push(inputPfa[ENTITIES_FIELD][tranIndex]);
            }
        }

        if (inputPfa[JOURNALS_FIELD].indexOf(tranId) != -1) {
            outputPFA[JOURNALS_FIELD].push(tranId);
        }

        if (inputPfa[PARTIAL_PAID_FIELD].indexOf(tranId) != -1) {
            outputPFA[PARTIAL_PAID_FIELD].push(tranId);
        }

        return outputPFA;
    }

    function stringifyPFAValues(pfa) {
      var outputObj = {};

      outputObj[TRANSACTION_ID_FIELD] = stringifyArray(pfa[TRANSACTION_ID_FIELD]);
      outputObj[TRANSACTION_AMOUNT_FIELD] = stringifyArray(pfa[TRANSACTION_AMOUNT_FIELD]);
      outputObj[DISCOUNTS_FIELD] = stringifyArray(pfa[DISCOUNTS_FIELD]);
      outputObj[ENTITIES_FIELD] = stringifyArray(pfa[ENTITIES_FIELD]);
      outputObj[JOURNALS_FIELD] = stringifyArray(pfa[JOURNALS_FIELD]);
      outputObj[PARTIAL_PAID_FIELD] = stringifyArray(pfa[PARTIAL_PAID_FIELD]);

      return outputObj;
    }
    
    function stringifyArray(array) {
        var outputString = '';
        if (array.length > 0) {
            outputString = JSON.stringify(array);
        }
        return outputString;
    }
    
    function parseArray(arrayString) {
        var outputArray = [];
        if (arrayString && arrayString.length > 0) {
            outputArray = JSON.parse(arrayString);
        }
        return outputArray;
    }

    function removePFATransactionsNotInList(pfaRecord, processedTranIds) {
        var inputPfa = transformPFA(pfaRecord);
        var outputPfa = createEmptyPFAList();

        processedTranIds = processedTranIds || [];

        for (var i = 0; i < processedTranIds.length; i++) {
            var tranId = processedTranIds[i];
            outputPfa = addTransactionsToPFA(outputPfa, inputPfa, tranId);
        } 

        return stringifyPFAValues(outputPfa);
    }
    
    function createEmptyPFAList() {
        var pfa = {};
        pfa[TRANSACTION_ID_FIELD] = [];
        pfa[TRANSACTION_AMOUNT_FIELD] = [];
        pfa[DISCOUNTS_FIELD] = [];
        pfa[ENTITIES_FIELD] = [];
        pfa[JOURNALS_FIELD] = [];
        pfa[PARTIAL_PAID_FIELD] = [];

        return pfa;
    }

    return {
        removePFATransactionsNotInList: removePFATransactionsNotInList
    };
});
