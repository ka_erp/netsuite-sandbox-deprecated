/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount 
 */

define(['../lib/9997_DAOFactory'],

function (daoFactory) {
    var RECORD_TYPE = 'customrecord_2663_bank_details';

    var FIELD_MAP = {
        'name': 'name',
        'glBankAccount': 'custrecord_2663_gl_bank_account',
        'subsidiary': 'custrecord_2663_subsidiary',
        'currency': 'custrecord_2663_currency',
        'legalName': 'custrecord_2663_legal_name',
        'printCompanyName': 'custrecord_2663_print_company_name',
        'eftTemplate': 'custrecord_2663_eft_template',
        'ddTemplate': 'custrecord_2663_dd_template',
        'ppTemplate': 'custrecord_2663_pp_template',
        'eftTemplateFlds': 'custrecord_2663_eft_template_flds',
        'eftTemplateDetails': 'custrecord_2663_eft_template_details',
        'ddTemplateDetails': 'custrecord_2663_dd_template_details',
        'ppTemplateDetails': 'custrecord_2663_pp_template_details',
        'eftFileCabinetId': 'custrecord_2663_eft_file_cabinet_id',
        'bankDepartment': 'custrecord_2663_bank_department',
        'bankClass': 'custrecord_2663_bank_class',
        'bankLocation': 'custrecord_2663_bank_location',
        'transMarked': 'custrecord_2663_trans_marked',
        'eftBatch': 'custrecord_2663_eft_batch',
        'ddBatch': 'custrecord_2663_dd_batch',
        'summarized': 'custrecord_2663_summarized',
        'autoAp': 'custrecord_2663_auto_ap',
        'bankBatchNumber': 'custrecord_2663_bank_batch_number',
        'fileNamePrefix': 'custrecord_2663_file_name_prefix',
        'bankApprovalType': 'custrecord_2663_bank_approval_type',
        'bankPaymentLimit': 'custrecord_2663_bank_payment_limit'
    }; 

    var daoParams = {
        recordType: RECORD_TYPE,
        fieldMap: FIELD_MAP,
    };

    var dao = daoFactory.getDAO(daoParams);

    function retrieve(id){
        return dao.retrieve(id);
    }

    function retrieveNsRecord(id){
        return dao.retrieveNsRecord(id);
    }

    return {
        retrieve: retrieve,
        retrieveNsRecord: retrieveNsRecord
    };
});
