/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount 
 */

define(['../lib/9997_DAOFactory'],

function (daoFactory) {
    var RECORD_TYPE = 'customrecord_2663_payment_file_format';

    var FIELD_MAP = {
        'name': 'name',
        'paymentFileType': 'custrecord_2663_payment_file_type',
        'formatCountry': 'custrecord_2663_format_country',
        'includeAllCurrencies': 'custrecord_2663_include_all_currencies',
        'fileFields': 'custrecord_2663_file_fields',
        'refFields': 'custrecord_2663_ref_fields',
        'entityRefFields': 'custrecord_2663_entity_ref_fields',
        'fieldValidator': 'custrecord_2663_field_validator',
        'nativeFormat': 'custrecord_2663_native_format',
        'maxLines': 'custrecord_2663_max_lines',
        'formatCurrency': 'custrecord_2663_format_currency',
        'updateEntityDetails': 'custrecord_2663_update_entity_details',
        'useFreeMarker': 'custrecord_2663_use_free_marker',
        'freeMarkerBody': 'custrecord_2663_free_marker_body',
        'outputFileExtension': 'custrecord_2663_output_file_extension',
        'outputFileEncoding': 'custrecord_2663_output_file_encoding'
    }; 

    var daoParams = {
        recordType: RECORD_TYPE,
        fieldMap: FIELD_MAP,
    };

    var dao = daoFactory.getDAO(daoParams);

    function retrieve(id){
        return dao.retrieve(id);
    }

    function retrieveNsRecord(id){
        return dao.retrieveNsRecord(id);
    }

    return {
        retrieve: retrieve,
        retrieveNsRecord: retrieveNsRecord
    };
});
