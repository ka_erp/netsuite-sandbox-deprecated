/**
 * � 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 *
 * @author jseego
 * @NScriptName Electronic Payments Payment CS
 * @NScriptId _9997_payment_cs
 * @NApiVersion 2.0
 * @NScriptType ClientScript
 * @NModuleScope Public
 */


 
define(['./9997_PaymentFlagProcessor'],

function(proc) {

    // TODO: When core issue 392395 is fixed, remove these variables and all references
    // work around for an issue (ignoreFieldChange not working)
    var isEpFlagChanged = false;
    var isOtherFlagChanged = false;

    function pageInit(context){
        // setup restore point
        var rec = context.currentRecord;
        proc.saveOtherPaymentOptions(rec);
        
        if (context.mode == 'create'){
            // trigger field change requirements
            if (proc.getElectronicPaymentsFlag(rec)){
                proc.clearOtherPaymentOptions(rec);
            }
        }
    }

    function fieldChanged(context){
        var rec = context.currentRecord;
        var fieldId = context.fieldId;
        
        if (proc.isEpPaymentMethod(rec, fieldId)){
            isEpFlagChanged = true;
            var isForEp = proc.getElectronicPaymentsFlag(rec);
            if (isForEp) {
                proc.clearOtherPaymentOptions(rec);
            }
            else {
                if (!isOtherFlagChanged){
                    proc.restoreOtherPaymentOptions(rec);
                }
            }
            isEpFlagChanged = false;
        }
        
        else if (proc.isOtherPaymentMethod(rec, fieldId)){
            if (!isEpFlagChanged){
                isOtherFlagChanged = true;
                proc.setElectronicPaymentsFlag(rec, false);
                isOtherFlagChanged = false;
            }
        }
    }
    
    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged
    };
});            