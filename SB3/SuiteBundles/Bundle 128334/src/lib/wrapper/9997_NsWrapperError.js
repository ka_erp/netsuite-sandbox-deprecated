/**
 * © 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code. 
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Sep 2015         ldimayuga
 *
 * @NModuleScope TargetAccount
 */
define(['N/error'], function(error) {
    
    /**
    * Instantiate an error object (specifying the name, message, and notify off) *
    * @param {Object} config
    * @param {string} config.name the error name
    * @param {string} [config.message=null] the error message
    * @param {boolean} [config.notifyOff=false] indicate if the email notification(configured on the script record page) will be suppressed or not. 
    * @return {SuiteScriptError|UserEventError}
    *
    * @throws {error.SuiteScriptError} if the any mandatory argument (name or message) is missing 
    *
    * @since 2015.1
    */
    function create(params) {
        var errorParams = {};
        
        errorParams.code = params.code;
        
        if (params.error) {
            errorParams.message = extractErrorMessage(params.error);
        } else {
            errorParams.message = errorParams.message;
        }
        
        return error.create(params);
    }
    
    function extractErrorMessage(error) {
        var errorMessage = [];
        if (error.code) {
            errorMessage.push(error.code);
        }
        
        if (error.message) {
            errorMessage.push(error.message);
        }
        
        return errorMessage.join(': ');
    }
    
    function logAndCreateError(code, message) {
        log.error(code, message);
        return error.create({
            name: code,
            message: message
        });
    }
    
    return {
        create: create,
        logAndCreateError: logAndCreateError
    };
});
