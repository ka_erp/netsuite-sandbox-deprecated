/**
 * © 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 Oct 2015     ssantiago
 *
 * @NModuleScope TargetAccount
 */
define(['N/runtime'], function(runtime) {
    
    /**
    * Check if a feature is turned on and in effect
    * @param {Object} options
    * @param { string } options.feature id of the feature
    * @return {boolean}
    */
    function isFeatureInEffect(feature) {
        return runtime.isFeatureInEffect(feature);
    }
    
    /**
     * Check if account is OW by checking SUBSIDIARIES feature.
     * @return {boolean}
     */
    function isOW() {
        return isFeatureInEffect('SUBSIDIARIES');
    }
    
    function isMultiCurrency() {
        return isFeatureInEffect('multicurrency');
    }
    
    function isPartnerCommissionEnabled() {
        return isFeatureInEffect('PARTNERCOMMISSIONS');
    }
    
    function isEmployeeCommissionEnabled() {
        return isFeatureInEffect('COMMISSIONS');
    }
    
    /**
    * Get the current log in user object
    * @return {User}
    */
    function getCurrentUser() {
        return runtime.getCurrentUser();
    }
    
    /**
    * Get the current executing Script object
    * @return {Script}
    */
    function getCurrentScript() {
        return runtime.getCurrentScript();
    }
    
    return {
        ContextType: runtime.ContextType,
        EnvType: runtime.EnvType,
        Permission: runtime.Permission,
        executionContext: runtime.executionContext,
        isFeatureInEffect: isFeatureInEffect,
        isOW: isOW,
        isMultiCurrency: isMultiCurrency,
        isPartnerCommissionEnabled: isPartnerCommissionEnabled,
        isEmployeeCommissionEnabled: isEmployeeCommissionEnabled,
        getCurrentUser: getCurrentUser,
        getCurrentScript: getCurrentScript,
    };
    
});
