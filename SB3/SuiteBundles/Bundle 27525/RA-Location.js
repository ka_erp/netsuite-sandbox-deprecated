/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 VIII 2013     mkaluza
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	// get current location type
	var record = nlapiGetNewRecord();
	var currLocationType = record.getFieldValue('custrecord_ra_loctype');

	// get option for Headquarters in the select list
	var field = record.getField('custrecord_ra_loctype');
	var internalId = record.getId();
	var option = field.getSelectOptions('Headquarters');
	var optionHQ = option[0].getId();
	
	if (currLocationType == optionHQ) {
		var filters = [];
		filters[0] = new nlobjSearchFilter('custrecord_ra_loctype',null,"is", optionHQ);
		
		if (type == 'edit') {
			filters[1] = new nlobjSearchFilter('internalidnumber',null,"notequalto", internalId);
		}
		var result = nlapiSearchRecord('location',null,filters,null);
		
		if (result != null && result.length > 0) {
			nlapiLogExecution('DEBUG', 'Too many HQs', 'Too many HQs! Allowed number is 1.');
			throw ('You may not have more than one Headquarters location defined. Please choose another Location Type, or contact support.');
		}
	}
}
