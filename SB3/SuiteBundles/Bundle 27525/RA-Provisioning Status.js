/**
 * Module Description
 * Provides functionality to get or set workstation provisioning status
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Jan 2014     dotocka
 *
 */

/**
 * Sets the Workstation Provisioning Status value to workstation with specified Id
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function setWorkstationStatus(dataIn) {
	
	nlapiLogExecution('DEBUG', 'setWorkstationStatus: Method to set status called');
	
	var id = parseInt(dataIn.Id, 10);
	if(isNaN(id)){
		throw nlapiCreateError(inputDataErrorCode, "Provided Workstation Id is not set or the value is not number", true);
	}
	
	var filter = new nlobjSearchFilter('internalidnumber', null, 'equalto', id);
	var result = nlapiSearchRecord('customrecord_ra_workstation', null, filter);
	if(!result){
		throw nlapiCreateError(workstationNotFoundErrorCode, "Workstation with provided Id does not exist in the system", true);
	}
	
	var ws_status = parseInt(dataIn.Status, 10);
	if(isNaN(ws_status)){
		throw nlapiCreateError(inputDataErrorCode, "Provided Status is not set or value is not a number", true);
	}	
	
	if(ws_status < NotProvisioned || ws_status > Provisioned){
		throw nlapiCreateError("INVALID_WORKSTATION_STATUS", "Workstation Status is not within range (" + NotProvisioned + "-" + Provisioned + ")", true);
	}
	
	nlapiLogExecution('DEBUG', 'setWorkstationStatus: Input data', 'ws:' + id + ' status:' + ws_status);
	
	var workstation = nlapiLoadRecord('customrecord_ra_workstation', id);
	workstation.setFieldValue('custrecord_ra_ws_provisioning_status', ws_status);

	if(ws_status == Provisioned){
		workstation.setFieldValue('custrecord_ra_workstation_installurl', '');
	}
	
	nlapiSubmitRecord(workstation);
	
	nlapiLogExecution('DEBUG', 'setWorkstationStatus: Workstation status set');
}


/**
 * Gets the integer value representing Workstation Provisioning Status
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function getWorkstationStatus(dataIn) {
	
	var response = new Object();
	response.success = new Object();
	
	nlapiLogExecution('DEBUG', 'getWorkstationStatus: Method to get status called');

	var id = parseInt(dataIn.Id, 10);
	if(isNaN(id)){
		throw nlapiCreateError(inputDataErrorCode, "Provided Workstation Id is not set or the value is not number", true);
	}
	
	var filter = new nlobjSearchFilter('internalidnumber', null, 'equalto', id);
	var result = nlapiSearchRecord('customrecord_ra_workstation', null, filter);
	if(!result){
		throw nlapiCreateError(workstationNotFoundErrorCode, "Workstation with provided Id does not exist in the system", true);
	}
	
	nlapiLogExecution('DEBUG', 'getWorkstationStatus: Retrieving status for workstation', 'ws:' + id);
	
	var workstation = nlapiLoadRecord('customrecord_ra_workstation', id);
	var ws_status = workstation.getFieldValue('custrecord_ra_ws_provisioning_status');
	
	nlapiLogExecution('DEBUG', 'getWorkstationStatus: Workstation status retrieved', 'ws:' + id + ' status:' + ws_status);
	
	response.success.code = "OK";
	response.success.message = ws_status;
	return response;
}