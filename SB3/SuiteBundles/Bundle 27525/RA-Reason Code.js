/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 Mar 2015     pmalik
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	if (type == 'create') {
		var id = nlapiGetRecordId ();
    	var ty = nlapiGetRecordType ();
    	
    	if ( ty == "customrecord_ra_reasoncode" ) {
    		var externalId = nlapiGetFieldValue ( "externalid" );
    		var code= nlapiGetFieldValue ( "custrecord_ra_cd_rsn" );
    		
    		if (externalId == null || externalId == "") {
    			var record = nlapiLoadRecord ( ty, id );
           		externalId = code;
           		record.setFieldValue ( "externalid", externalId );
    			nlapiSubmitRecord ( record, false );
       		}
    	}
    }
}

function userEventBeforeLoad(type, form) {
	if(type == 'view') {
	  form.removeButton('makecopy');
	}
}
