/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 IX 2013     mkaluza
 *
 */

// @import RA-SettingLib.js 

function userEventBeforeLoad(type, form, request)
{
	if (type == 'view')
	{
		InitFields();
		
		var settingType = nlapiGetFieldText(fieldType);
		var settingValueType = nlapiGetFieldText(fieldValueType);
		UpdateFields(settingType, settingValueType, false, true);
	}
}

function userEventAfterSubmit(type) {
	if (type == 'create' || type == 'edit') {
	    var externalId = nlapiGetFieldValue ('externalid');
	    var name = nlapiGetFieldValue ('name');
	    var type = nlapiGetFieldText (fieldType);
	    var prefix = '';
	    nlapiLogExecution('DEBUG', 'After load', 'Type: ' + type + ' Name: ' + name + ' ExternalID: ' + externalId);
	    
	    switch (type) {
	    case 'NS Replication settings':				prefix = 'REP_SET_';	break;
	    case 'RARS Replication XML Settings':		prefix = 'REP_XML_';	break;
	    case 'NetSuite Inbound Trigger':			prefix = 'REP_ITR_'; 	break;
	    case 'NetSuite Outbound Trigger':			prefix = 'REP_OTR_'; 	break;
	    case 'NetSuite Integration Configuration':	prefix = 'REP_CON_'; 	break;
	    case 'NetSuite Credit Card Mapping':		prefix = 'REP_CCM_'; 	break;
	    case 'RA Settings':							prefix = 'POS_SET_';	break;
	    case 'RA Flags':							prefix = 'POS_FLG_';	break;
	    
	    
	    default:									prefix = 'POS_OTH_'; 		break;
	    }
	    
	    var externalIdName = prefix + name;
	    if (externalId == null || externalId == "" || externalIdName != externalId) {
	       var record = nlapiLoadRecord ( nlapiGetRecordType(), nlapiGetRecordId() );
	       record.setFieldValue ( 'externalid', externalIdName );
	       nlapiSubmitRecord ( record, false );
	    }
	}
}