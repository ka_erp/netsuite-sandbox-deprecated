/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 XII 2013     mkaluza
 *
 */

// @import RA-TaxLib.js
// @import RA-Framework.js

function userEventBeforeLoad(type, form, request)
{
	// Skip the function if calling by webservices
	if (nlapiGetContext().getExecutionContext() == 'webservices' || nlapiGetContext().getExecutionContext() == 'bundleinstallation') return;
	var useSubsidiaries = nlapiGetContext().getFeature('SUBSIDIARIES');
	
		
	var taxScheduleId = nlapiGetFieldValue(fieldScheduleLinkId);
	var locations = GetRecords('location', ['internalid', 'name', fieldMapping, (useSubsidiaries ? 'subsidiary' : 'name')]).sort(function(a, b) { return a.name.localeCompare(b.name); });
	var taxIdList = [-8];
	
	// Get tax ids used in the list
	for (var loc = 0; loc < locations.length; loc++) {
		var location = locations[loc];
		var mapping = DeserializeMapping(locations[loc].custrecord_ra_taxgroups_mapping);
		var taxIdValue = GetTaxIdFromMapping(mapping, taxScheduleId);
		
		if (taxIdValue != null && taxIdValue != '') {
			location.taxid = taxIdValue;
			taxIdList.push(taxIdValue);
		}
	}
	var taxGroups = GetRecords('taxGroup', ['internalid', 'itemid', 'rate', 'country'], ['internalid', 'anyOf', taxIdList]);
	var taxCodes = GetRecords('salestaxitem', ['internalid', 'itemid', 'rate', 'country'], ['internalid', 'anyOf', taxIdList]);
	for (var loc = 0; loc < locations.length; loc++) {
		var location = locations[loc];
		var tax = GetTax(taxGroups, taxCodes, location.taxid);
		
		if (tax != null) {
			if (tax != null && tax.country == 'CA') {
				tax.rate = FormatCanadianTaxRate(location.taxid);
			}
			
			location.taxname = tax.itemid;
			location.taxrate = tax.rate.indexOf('%') !== -1 ? tax.rate : tax.rate + '%';
			location.subsidiary = useSubsidiaries ? location.subsidiary : null;
		} else {
			location.taxrate = '0.00%';
		}
	}

	// Add a sublist to Tax configuration tab
	var taxConfigurationSublist = form.addSubList('custpage_taxconfiguration_sublist', 'list', 'Tax configuration', 'custom');
	taxConfigurationSublist.addField('internalid', 'integer', 'Internal ID');
	taxConfigurationSublist.addField('name', 'text', 'Name');
	if (useSubsidiaries) {
		taxConfigurationSublist.addField('subsidiary', 'text', 'Subsidiary');
	}
	taxConfigurationSublist.addField('taxid', 'select', 'Tax Code/Group', 'salestaxitem').setDisplayType('entry');
	taxConfigurationSublist.addField('taxrate', 'text', 'Rate');
	taxConfigurationSublist.setLineItemValues(locations);
	
	addLocationChangedCount();
}

function GetTax(taxGroups, taxCodes, taxId) {
	for (var i = 0; i < taxGroups.length; i++) {
		if (taxGroups[i].internalid == taxId) {
			return taxGroups[i];
		}
	}
	for (var i = 0; i < taxCodes.length; i++) {
		if (taxCodes[i].internalid == taxId) {
			return taxCodes[i];
		}
	}
	return null;
}

function userEventBeforeSubmit(type)
{
	if (type == 'edit' && nlapiGetContext().getExecutionContext() == 'bundleinstallation') return;
	
	if (type == 'edit' || type == 'create') {
		var linkId = nlapiGetFieldValue(fieldScheduleLinkId);
		if (linkId == null || linkId == '') {
			createTaxSchedule(nlapiGetFieldValue('name'), nlapiGetFieldValue(fieldDescription));
		} else {
			nlapiSubmitField('taxschedule', linkId, 'name', nlapiGetFieldValue('name'));
			nlapiSubmitField('taxschedule', linkId, 'description', nlapiGetFieldValue(fieldDescription));
		}
		
		saveLocations();
	}
	else if (type == 'delete') {
		var linkId = nlapiGetFieldValue(fieldScheduleLinkId);
		if (linkId != null)
		{
			try {
				nlapiDeleteRecord('taxSchedule', linkId);
			}
			catch (e) {
				nlapiLogExecution('DEBUG', 'Delete error', e);
			}
		}
	}
}

function userEventAfterSubmit(type)
{
}
	

function saveLocations()
{
	var savedLocations = 0;
	var taxScheduleId = nlapiGetFieldValue(fieldScheduleLinkId);
	var locations = GetRecords('location', ['internalid', 'name', fieldMapping]);	
	for (var loc = 0; loc < locations.length; loc++) {
		var location = locations[loc];
		if (location != null) {
			var mapping = location != null ? DeserializeMapping(location.custrecord_ra_taxgroups_mapping) : null;
			var savedTaxId = GetTaxIdFromMapping(mapping, taxScheduleId);
			var linenum = nlapiFindLineItemValue('custpage_taxconfiguration_sublist', 'internalid', location.internalid);
			var selectedTaxId = nlapiGetLineItemValue('custpage_taxconfiguration_sublist', 'taxid', linenum);
			if (savedTaxId != selectedTaxId) {
				saveLocation(location, taxScheduleId, selectedTaxId);
				savedLocations++;
				if (savedLocations > maxSavedLocations) {
					return;
				}
			}
		}
	}
}

function saveLocation(location, taxScheduleId, taxId) 
{
	if (location != null)
	{
		var mapping = location != null ? DeserializeMapping(location.custrecord_ra_taxgroups_mapping) : null;
		var scheduleIndex = GetTaxScheduleIndex(mapping, taxScheduleId);

		if (scheduleIndex == null) {
			if (mapping == null) {
				mapping = {};
			}
			if (mapping.taxSchedule == null) {
				mapping.taxSchedule = [];
			}
			scheduleIndex = mapping.taxSchedule.length;
			mapping.taxSchedule[scheduleIndex] = {};
		}
				
		mapping.taxSchedule[scheduleIndex].taxGroup = taxId;
		mapping.taxSchedule[scheduleIndex].internalId = taxScheduleId;
		location.mapping = mapping;
		nlapiSubmitField('location', location.internalid, fieldMapping, SerializeMapping(mapping));
	}
}

function addLocationChangedCount() {
	form.addField(fieldLocationChangedCount, 'integer', 'Location changed count').setDisplayType('hidden');
	nlapiSetFieldValue(fieldLocationChangedCount, 0);
}

