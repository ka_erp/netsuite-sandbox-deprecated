/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 May 2013     bsomerville
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function customerInfo(dataIn) {
	
	var action = dataIn.Action;
	var customerID = dataIn.Customer;
	var raposID = dataIn.RaposId;
	var firstName = dataIn.FirstName;
	var lastName = dataIn.LastName;
	var location = dataIn.Location;
	var email = dataIn.Email;
	var exists = false;
	
	if(action == "Count") {
		var countRet = new Object();
		countRet.FirstName = firstName;
		countRet.LastName = lastName;
		countRet.Count = customerCount (firstName,lastName);
		return countRet;
	}
	
	var customerRecord = null;
	
	if(raposID == "_DEFAULT_") {
		nlapiLogExecution("DEBUG", "Default customer request", raposID);
	} else {
		// consider checking location/subsidiary here
		customerRecord = (raposID || customerID) ? customerRecordFromID(customerID,raposID) : null;
	}
	
	if(customerRecord && customerRecord.getFieldValue('isinactive') == 'T') customerRecord = null;
	
	switch(action) {
	// Exists - return true if the customer exists and is active
		case "Exists":
		  exists = (customerRecord != null);
		  break;
    // Ensure Exists - create the customer if needed
		case "Ensure Exists":
		  if (!customerRecord) {			  			  			 
			  nlapiLogExecution("DEBUG", "Did not find customer", raposID);
			  
			  if(!raposID || (raposID == "_DEFAULT_") ) {
				  nlapiLogExecution("DEBUG", "Getting location customer", customerID);
				  customerRecord = getCustomerFromLocation(location);
			  } else {			  
				  customerRecord = createCustomer(raposID, location, firstName, lastName, email);
			  }
		  }
		  exists = true;
		  break;		
		case "Get Employee":
			customerRecord = getEmployeeFromName(firstName, lastName);
			exists = customerRecord != null;
			break;
		case "Touch Employee":
			customerRecord = touchEmployee(customerID);
			exists = customerRecord != null;
			break;
		case "Remove Access":
			customerRecord = setEmployeeAccess(customerID, false);
			exists = customerRecord != null;
			break;
		case "Set Employee RAPOS ID":
			customerRecord = setEmployeeRaposId(customerID, raposID);
			exists = customerRecord != null;
			break;
		default:
		  throw "Invalid action - " + action;
	}
	
	nlapiLogExecution("DEBUG", "Configuring response", customerID);
	var result = new Object();		
	result.Customer = customerRecord != null ? customerRecord.getId() : "NOT FOUND";
	result.Exists = exists;
	result.RaposId = customerRecord != null ? customerRecord.getFieldValue("custentity_ra_externalid") : "NOT FOUND";
	if( result.RaposId )
		result.RaposId = customerRecord != null ? customerRecord.getFieldValue("externalid") : "NOT FOUND";
	result.OperatorId = customerRecord != null ? customerRecord.getFieldValue("custentity_ra_operatorid") : "NOT FOUND";
	result.Name = customerRecord != null ? customerRecord.getFieldValue ("entityid") : '';	
	result.Subsidiary = customerRecord != null ? customerRecord.getFieldValue("subsidiary") : '';
	result.LastModified = customerRecord != null ? customerRecord.getFieldValue("lastmodifieddate") : '';
	result.IsInactive = customerRecord != null ? customerRecord.getFieldValue("isinactive") == 'T' : false; 
	result.Email = customerRecord != null ? customerRecord.getFieldValue("email") : '';
	result.HasAccess = customerRecord != null ? customerRecord.getFieldValue("giveaccess") == 'T' : false;
	result.IsSalesRep = customerRecord != null ? customerRecord.getFieldValue("salesrep") == 'T' : false; 
	return result;
}

function getCustomerFromLocation ( location ) {
	// Determines default customer for a location
	if(!location) return null;
	
	var locationRecord = nlapiLoadRecord("location", location, null);
	
	var locCustomer = locationRecord.getFieldValue("custrecord_ra_loc_defcust");
	
	var customer = null;
	
	try {
		customer = nlapiLoadRecord("customer", locCustomer, null);
	} catch(err){ /* Customer not found */ }
	
	return customer;
}

function touchEmployee (empRecordId) {
	var empRecord = null;
	empRecord = nlapiLoadRecord("employee", empRecordId, null);
	nlapiSubmitRecord(empRecord,true,false);
	empRecord = nlapiLoadRecord("employee", empRecordId, null);
	return empRecord;
}

function setEmployeeAccess (empRecordId, hasAccess) {
	var empRecord = null;
	empRecord = nlapiLoadRecord("employee", empRecordId, null);
	empRecord.setFieldValue('giveaccess', hasAccess);
	nlapiSubmitRecord(empRecord,true,false);
	return empRecord;
}

function setEmployeeRaposId (empRecordId, raposId) {
	var empRecord = null;
	empRecord = nlapiLoadRecord("employee", empRecordId, null);
	empRecord.setFieldValue('custentity_ra_operatorid', raposId);
	nlapiSubmitRecord(empRecord,true,false);
	return empRecord;
}

function getEmployeeFromName(firstName, lastName) {
	var empRecord = null;
	
	var filters = new Array(); 
	var columns = new Array(); 
	
	filters[0] = new nlobjSearchFilter("firstname",null,"is",firstName);
	filters[1] = new nlobjSearchFilter("lastname",null,"is",lastName);
	columns[0] = new nlobjSearchColumn("internalid",null,null); 	
	
	var results = nlapiSearchRecord("employee",null,filters,columns);
	
	if(results) {
		var empRecordId = results[0].getValue(columns[0]);
		
		empRecord = nlapiLoadRecord("employee", empRecordId, null);
	}
	
	return empRecord;
}

function customerRecordFromID ( customerID, raposID ) {
	// Looks up customer using customerID, then RAPOS ID (external)
	if(!customerID && raposID) {
		customerID = getCustomerIDFromRAPOS(raposID);
	}
	
	if(!customerID) {
		customerID = raposID.replace('CU','');
	} else {	
		customerID = customerID.replace('CU','');
	}
	
	var custRec = null;
	
	try {
		custRec = nlapiLoadRecord("customer", customerID, null);	
		if(custRec && custRec.getFieldFalue("isinactive") == 'T') return null;
	} catch (err) { /* Customer not found */ }	
	
	return custRec;
}

function getCustomerIDFromRAPOS ( raposid ) {
	var filters = new Array(); 
	var columns = new Array(); 
	
	filters[0] = new nlobjSearchFilter("custentity_ra_externalid",null,"is",raposid);
			 
	columns[0] = new nlobjSearchColumn("internalid",null,null); 	
	
	var results = nlapiSearchRecord("entity",null,filters,columns);
	var temp = null;
	
	nlapiLogExecution("DEBUG", "In Cust Lookup", raposid);

	if(results) {
		temp = results[0].getValue(columns[0]);
		nlapiLogExecution("DEBUG", "Found internal ID ", temp);
	} 
	return temp;
}

function getSubsidiaryFromLocation (location) {
	// Determine the subsidiary for a given location
	 var subsidiaryRecord = null;
	  
	 try {
	  	subsidiaryRecord = nlapiLoadRecord("location", location, null);
	 } catch(err) { /* Subsidiary not found */ }	
	  
	 if(subsidiaryRecord)
		  return subsidiaryRecord.getFieldValue("subsidiary");
	 else
		 return null;
}

function customerCount(firstName, lastName) {
	var filters = new Array(); 
	var columns = new Array(); 	
	filters[0] = new nlobjSearchFilter("entityid",null,"startswith",firstName + ' ' + lastName );		
	columns[0] = new nlobjSearchColumn("internalid",null,null); 		
	var results = nlapiSearchRecord("customer",null,filters,columns);
	
	var count = (results == null) ? 0 : results.length;
	
	return count;
}

function getLastNameWithCount(firstName, lastName)
{
	var initialcount = customerCount(firstName, lastName);
	var count = initialcount;
	var initialLastName = lastName;
	nlapiLogExecution("DEBUG", "Existing Check", count);
	
	while(count > 0) {
		lastName = initialLastName + ' ' + (++initialcount);
		count = customerCount(firstName, lastName);
	}
	
	return lastName;
}

function createCustomer(raposID, location, firstName, lastName, email) {
	// Creates a simple customer using the Name and RAPOS ID (external)
	// Subsequent updates occur using the existing process
	nlapiLogExecution("DEBUG", "In Create Customer", raposID);
	
	var subsidiary = getSubsidiaryFromLocation(location);
	
	nlapiLogExecution("DEBUG", "Subsidiary", subsidiary);
	
	lastName = getLastNameWithCount(firstName, lastName);
	
	nlapiLogExecution("DEBUG", "LastName", lastName);
	
	var custRec = nlapiCreateRecord("customer", null);
	
	custRec.setFieldValue("custentity_ra_externalid", raposID);
	custRec.setFieldValue("externalid", raposID);
	custRec.setFieldValue("firstname", firstName);
	custRec.setFieldValue("lastname", lastName);
	if(email) {
		custRec.setFieldValue("email", email); 
	} else {
		custRec.setFieldValue("emailtransactions", 'F'); 
	} 

	
	custRec.setFieldValue("custentity_ra_cust_donotdownload", 'T');
	
	custRec.setFieldValue("entityid", firstName + ' ' + lastName);
	if(subsidiary)
		custRec.setFieldValue("subsidiary", subsidiary);	
	
    try {
    	nlapiSubmitRecord(custRec,false,true);
    } 
    catch(e)
    {
       if((e instanceof nlobjError) && (e.getCode() == "DUP_ENTITY"))
       {
    	   custRec = customerRecordFromID(null, raposID);
       }
       else throw e;
    }
	
    return custRec;
}