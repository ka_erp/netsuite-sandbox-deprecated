/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 Sep 2013     dotocka
 * 1.01       25 Mar 2015     dotocka
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	if (request.getMethod() == 'GET') {
		var copy_to_form = nlapiCreateForm('Select workstations ...', true);
		copy_to_form.addSubmitButton('Copy');		
		copy_to_form.addButton('cancel', 'Cancel', 'window.close()');
		copy_to_form.setScript('customscript_ra_cl_workstation_list');
		
		//get parameters
		var locationId = request.getParameter('lc');
		var workStationId = request.getParameter('ws');
		
		var filters = [];
		var columns = [];
		filters[filters.length] = new nlobjSearchFilter('custrecord_ra_ws_loc', null, 'anyof', parseInt(locationId));
		filters[filters.length] = new nlobjSearchFilter('internalidnumber', null, 'notequalto', parseInt(workStationId));
		columns[columns.length] = new nlobjSearchColumn('name');
		columns[columns.length] = new nlobjSearchColumn('internalid');
		
		var workstationResults = nlapiSearchRecord('customrecord_ra_workstation', null, filters, columns);
		
		var text = copy_to_form.addField('text', 'text', '');
		text.setDisplayType('inline');
		text.setDefaultValue('Please select workstations to which you want to copy the provider settings');
		text.setLayoutType('outsideabove');
		
		copy_to_form.addField('copy_to_all','checkbox', 'Copy to all in the same location', null);
		
		var workstation_id_field = copy_to_form.addField('workstation_id_field','text', null, null);
		workstation_id_field.setDisplayType('hidden');
		workstation_id_field.setDefaultValue(workStationId);
		
		var workstation_list = copy_to_form.addSubList('workstation_list', 'list', 'Workstation List', null);		 
		var workstation = workstation_list.addField('workstation','text', 'Workstation', null);
		workstation.setDisplayType('inline');		 
		
		workstation_list.addField('chbx_copyto','checkbox', 'Copy', null);
		
		var ws_id = workstation_list.addField('ws_id','text', 'Workstation ID', null);
		ws_id.setDisplayType('hidden');
		
		for ( var i = 1; workstationResults !== null && i <= workstationResults.length; i++) {
			var result = workstationResults[i - 1];
			workstation_list.setLineItemValue('workstation', i, result.getValue('name'));
			workstation_list.setLineItemValue('ws_id', i, result.getValue('internalid'));
		}
		
		response.writePage(copy_to_form);
	}
	else {//Copy
		submit(request, response);
	}
}

//Copy settings to chosen workstations
function submit(request, response){	
	var workstationId = request.getParameter('workstation_id_field');
	var wsCount = request.getLineItemCount('workstation_list');
	var workstation = nlapiLoadRecord('customrecord_ra_workstation', parseInt(workstationId));
	
	for(var i=1; i <= parseInt(wsCount); i++){
    	
    	var copy = request.getLineItemValue('workstation_list', 'chbx_copyto', i);
    	
    	if(copy == 'T'){
    		var id = request.getLineItemValue('workstation_list', 'ws_id', i);
    		var wsCopyTo = nlapiLoadRecord('customrecord_ra_workstation', parseInt(id));
    		
    		for (var property in providerProperties){
    			wsCopyTo.setFieldValue(property, workstation.getFieldValue(property));
    		}
    		
    		nlapiSubmitRecord(wsCopyTo);
    	}
	}
	response.writeLine(createClientScript());
}

function createClientScript(){
	var clientScript = '<script type="text/javascript">';
    clientScript += 'window.close();' + '\n' + '</script>';
	return clientScript;
}