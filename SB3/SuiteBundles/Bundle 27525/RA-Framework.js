/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 Jan 2015     dotocka
 *
 */
/*
 * Returns URL to the NSPOS server with or without SSO token based on parameter + attaches suffix if needed
 */
function GetNSPOSIntegrationUrl(urlSuffix, sso){
	
	var urlAWS = nlapiGetContext().getSetting('SCRIPT', 'custscript_ra_pa_nspos_server_name');
	var url = 'https://';
	
	if(sso){
		var urlSSO = nlapiOutboundSSO('customsso_ra_nspos_integration');
		var parPosition = urlSSO.indexOf('?', 0);
		url += urlAWS + urlSuffix + urlSSO.substring(parPosition); //add suffix before parameters start
	}
	else{
		url += urlAWS + urlSuffix; //remove the token and add suffix
	}	
	return url;
}

function GetNSPOSIntegrationToken(){
	var sso = nlapiOutboundSSO('customsso_ra_nspos_integration');
	
	var params = ParseParameters(sso);
	//https://url.com?oauth_token=64640a6071081d0b03110c016c112305044c1a5f49533f6f144507486ef2f14b67955cc6&dc=002&env=PRODUCTION
	sso = params['oauth_token'];
	
	return sso;
}

/*
 * Returns JSON object representing NetSuitePassport
 */
function GetNSPassport(password, email){
	var context = nlapiGetContext();
    var userAccountId = context.getCompany();
    var passport = {
    		Account : userAccountId,
    		Email : email || nlapiGetFieldValue("email"),
    		Password : password || ""
    };
	return passport;
}

/*
 * Returns dictionary of key/value pair where key is parameter name and value is parameter value
 */
function ParseParameters(url){
	 var parameters = {};
	 var parPosition = url.indexOf('?', 0) + 1;
	 
	 var params = url.substring(parPosition).split('&');
	 
	 for(var i = 0; i < params.length ; i++){
		 var parts = params[i].split('=');
		 parameters[parts[0]] = parts[1];
	 }
	 return parameters;
}

function GetRecords(recordType, columns, filters) {
	var dict = new Array();
	var searchColumns = [];
	for (var c = 0; c < columns.length; c++) 
	{
		searchColumns[c] = new nlobjSearchColumn(columns[c],null,null);
	}
	searchColumns[columns.length] = new nlobjSearchColumn('isInactive',null,null);
	// Create search; alternatively nlapiLoadSearch() can be used to load a saved search
	var filterExpression = [['isInactive', 'is', 'F']];
	if (filters != null) {
		filterExpression[1] = 'and';
		filterExpression[2] = filters;
	}
	var search = nlapiCreateSearch(recordType, filterExpression, searchColumns);
	var searchResults = search.runSearch();
	var resultIndex = 0; 
	var resultStep = 1000; // Number of records returned in one step (maximum is 1000)
	var resultSet; 
	do 
	{
	    // fetch one result set
	    resultSet = searchResults.getResults(resultIndex, resultIndex + resultStep);
	    resultIndex = resultIndex + resultStep;
	    if (resultSet && resultSet.length > 0) {
			for ( var i = 0; i < resultSet.length; i++) {
				if (resultSet[i].getValue('isInactive') == 'F') {
					var item = new Object();
					for (var c = 0; c < columns.length; c++) 
					{
						item[columns[c]] = resultSet[i].getValue(columns[c]);
					}
					dict.push(item);
				}
			}
		}
	} while (resultSet && resultSet.length > 0)
	return dict;
}

function GetRecordById(recordTypes, columns, id) {
	if (id != null && id != '') {
		var filters = new Array();
		var searchColumns = [];
		for (var c = 0; c < columns.length; c++)  {
			searchColumns[c] = new nlobjSearchColumn(columns[c],null,null);
		}
		filters[0] = new nlobjSearchFilter('internalid', null,'is', id);
		for (var r = 0; r < recordTypes.length; r++) {
			var searchResults = nlapiSearchRecord(recordTypes[r], null, filters, searchColumns);
			if (searchResults && searchResults.length == 1) {
				var item = new Object();
				for (var c = 0; c < columns.length; c++) {
					item[columns[c]] = searchResults[0].getValue(columns[c]);
				}
				return item;
			}
		}
	}
	return null;
}