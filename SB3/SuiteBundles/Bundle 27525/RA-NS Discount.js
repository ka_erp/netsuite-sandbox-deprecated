/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       22 XI 2013     mkaluza
 *
 */

function userEventBeforeLoad(type, form, request){
	nlapiLogExecution('DEBUG', 'Before load', 'Add fields');
	form.addFieldGroup( 'myfieldgroup', 'RA Custom Fields');
	var field = form.addField('custpage_ra_discount_reduction_type', 'select', 'Reduction type', 'customlist_ra_discount_reduction_type','myfieldgroup');
	field.setMandatory(true);
	
	var discountLinkId = GetDiscountLinkedId(nlapiGetRecordId());
	if (discountLinkId != -1) {
		var discountItem = nlapiLoadRecord('customrecord_ra_discount', discountLinkId);
		nlapiSetFieldValue('custpage_ra_discount_reduction_type', discountItem.getFieldValue('custrecord_ra_discount_reduction_type'));
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){	
	nlapiLogExecution('DEBUG', 'after submit', 'type: "' + type + '"');
	
	var name = nlapiGetFieldValue('itemid');
	var rate = nlapiGetFieldValue('rate');
	var displayName = nlapiGetFieldValue('displayname');
	var description = nlapiGetFieldValue('description');
	var id = nlapiGetNewRecord().getId();
	var reductionType = nlapiGetFieldValue('custpage_ra_discount_reduction_type');
	var account = nlapiGetFieldValue('account');
    var nonposting = nlapiGetFieldValue('nonposting');
	var discountLinkId = GetDiscountLinkedId(id);
	
	if (type == 'create' || type == 'edit') {
		nlapiLogExecution('DEBUG', 'CreateEdit', 'Id: ' + id + ' Name: ' + name + ' rate: ' + rate + ' Display Name: ' + displayName + ' Description: ' + description
				+ 'Reduction type' + reductionType);	
			
		var discountItem = (type == 'create' || discountLinkId == -1 ? nlapiCreateRecord('customrecord_ra_discount') : nlapiLoadRecord('customrecord_ra_discount', discountLinkId));
		discountItem.setFieldValue('name', name);
		discountItem.setFieldValue('custrecord_ra_discount_account',account);
        discountItem.setFieldValue('custrecord_ra_discount_nonposting',nonposting);
		discountItem.setFieldValue('custrecord_ra_discount_display_name', displayName);
		discountItem.setFieldValue('custrecord_ra_discount_description', description);
		discountItem.setFieldValue('custrecord_ra_discount_link', id);
		discountItem.setFieldValue('custrecord_ra_discount_reduction_type', reductionType);
		discountItem.setFieldValue('custrecord_ra_discount_rate', rate.charAt(rate.length-1) == '%' ? rate.substring(0,rate.length-2) : rate);
		nlapiSubmitRecord(discountItem);
		
		// Save record to RA-Change Log
		var recChange = nlapiCreateRecord('customrecordchangelog');
		recChange.setFieldValue('custrecordrecordtype', 'customrecord_ra_discount');
		recChange.setFieldValue('custrecordrecordid', discountItem.getId());
		nlapiSubmitRecord(recChange);
		
	} else if (type == 'delete') {
		var discountLinkId = GetDiscountLinkedId(id);
		if (discountLinkId != -1) {
			nlapiLogExecution('DEBUG', 'Delete', 'Discount: ' + discountLinkId + ' Name: ' + name);
			nlapiDeleteRecord('customrecord_ra_discount', discountLinkId);
		}
	}	
}

// Search for linked RA-Discount
function GetDiscountLinkedId(id) {
	if (id) {
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ra_discount_link', null, 'equalTo', id);
		var column = new nlobjSearchColumn('internalid',null,null);
		var searchResults = nlapiSearchRecord('customrecord_ra_discount', null, filters, column);
		
		if (searchResults && searchResults.length == 1) {
			return searchResults[0].getValue('internalid');
		}
	}
	return -1;
}
