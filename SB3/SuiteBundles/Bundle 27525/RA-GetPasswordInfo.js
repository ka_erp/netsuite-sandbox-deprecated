/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       19 Nov 2014     pmalik
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function getRESTlet(dataIn) {
	
	var companyInfo = nlapiLoadConfiguration('companypreferences');
	
	var minPassLength = companyInfo.getFieldValue('minpasswordlength');
	var passExpiration = companyInfo.getFieldValue('passwordexpiredays');
	var passPolicy = companyInfo.getFieldValue('password_policy');
	
	if (isNullOrEmpty(minPassLength))
	{
		minPassLength = "-1";
	}
	
	if (isNullOrEmpty(passExpiration))
	{
		passExpiration = "-1";
	}
	
	if (isNullOrEmpty(passPolicy))
	{
		passPolicy = "Strong";
	}
		
	var	res = new Object();
	res.PasswordLength = minPassLength;
	res.PasswordExpiration = passExpiration;
	res.PasswordPolicy = passPolicy;
	
	return res;
}

function isNullOrEmpty(value){
	if (value == null || value == "")
	{
		return true;
	}
	return false;
}
