/**
 * Copyright 2015 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */
var Tax = Tax || {};
Tax.Adapter = Tax.Adapter || {};

Tax.Adapter.BaseAdapter = function BaseAdapter() {
	this.rawdata = [];
	this.daos = [];
};

Tax.Adapter.BaseAdapter.prototype = Object.create(Tax.Processor.prototype);

Tax.Adapter.BaseAdapter.prototype.transform = function transform(params) {	
};

Tax.Adapter.BaseAdapter.prototype.process = function process(result, params) {
	try {
		for (var idao = 0; this.daos && idao < this.daos.length; idao++) {
			var data = Tax.Cache.MemoryCache.getInstance().load(this.daos[idao]);
			if (data) {
				this.rawdata = this.rawdata.concat(data);
			}
		}
	} catch (ex) {
		logException(ex, 'Tax.Adapter.BaseAdapter.process');
	}
	return {adapter:this.transform(params)};
};