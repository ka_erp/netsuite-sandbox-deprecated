/**
 * © 2016 NetSuite Inc.
 * User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 *
 * @NScriptName FAM Delete Scheduled Forecasts Script
 * @NScriptId _fam_mr_deleteforecast
 * @NScriptType mapreducescript
 * @NApiVersion 2.x
*/

define([
    '../adapter/fam_adapter_search',
    '../adapter/fam_adapter_task',
    '../adapter/fam_adapter_runtime',
    '../adapter/fam_adapter_record'
],

function (search, task, runtime, record) {
    var dhrRec = 'customrecord_ncfar_deprhistory';

    function searchDHR() {
        var histToDelete = [];
        var searchObj = search.load({ id : 'customsearch_fam_forecast_values' });
        var pagedData = searchObj.runPaged({ pageSize : 1000 });
        
        log.audit('input', 'histories to delete: ' + pagedData.count);
        pagedData.pageRanges.forEach(function (pageRange) {
            var page = pagedData.fetch({ index : pageRange.index });
            histToDelete = histToDelete.concat(page.data);
        });
        
        return histToDelete;
    }
    
    return {
        getInputData : function() {
            log.debug('Input Stage');
            var procRec = this.loadProcessInstance(), stateVal = {};
            
            if (procRec) {
                var procName = procRec.getValue('custrecord_far_proins_processname');
                log.audit('Processing Start', procName + ' (' + procRec.id + ')');
            }
            else {
                log.audit('Processing Start', 'no parameter');
            }
            
            try {
                return searchDHR();
            }
            catch(ex) {
                log.error('delete error', ex.toString());
            }
        },
        
        map : function(context) {
            log.debug('map parameters', 'value: ' + context.value);
            var searchResult = JSON.parse(context.value);
            
            try {
                var recId = record.delete({type:dhrRec, id:searchResult.id});
                log.debug('delete DHR', recId);
            }
            catch(ex) {
                log.error('delete error', ex.toString());
            }
        },
        
        summarize : function(summary) {
            log.debug('summarize');
            var srchDHR = searchDHR(), procRec = this.loadProcessInstance(), 
                schedTask = task.create({ taskType : task.TaskType.SCHEDULED_SCRIPT});

            log.audit('Instance Record', JSON.stringify(procRec));
            log.audit('Search Results', JSON.stringify(srchDHR));
            if (procRec && !(srchDHR && srchDHR.length > 0)) {
                var stateVal = JSON.parse(procRec.getValue('custrecord_far_proins_procstate'));
                stateVal.Stage = 2;
                procRec.setValue('custrecord_far_proins_procstate', JSON.stringify(stateVal));
                procRec.save();
            }

            schedTask.scriptId = 'customscript_fam_bgp_ss';
            schedTask.submit();
        },
        
        loadProcessInstance : function() {
            var scriptObj = runtime.getCurrentScript(),
                procId = scriptObj ? scriptObj.getParameter({ name : 'custscript_fam_forecast_bgpid' }) : '';
            if (procId) { return record.load({ type : 'customrecord_bg_procinstance', id : procId }); }
            return null;
        }
    }
});