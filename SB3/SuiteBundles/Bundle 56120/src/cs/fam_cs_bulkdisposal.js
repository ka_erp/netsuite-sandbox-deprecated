/**
 * � 2016 NetSuite Inc.
 * User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 *
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NScriptId _fam_cs_bulktransfer
 * @NModuleScope TargetAccount
 */

define(['N/url'],
    function(url) {
        function pageInit(){
            //void
        }
        function cancelBulkDisposal() {
            var disposalLink = 
                    url.resolveScript({
                        scriptId            : 'customscript_ncfar_disposal_sl',
                        deploymentId        : 'customdeploy_ncfar_disposal_sl',
                        returnExternalUrl   : false,
                        params              : null
                    });
            window.location = disposalLink;
        }
        return {
            pageInit: pageInit,
            cancelBulkDisposal: cancelBulkDisposal
        };
    });