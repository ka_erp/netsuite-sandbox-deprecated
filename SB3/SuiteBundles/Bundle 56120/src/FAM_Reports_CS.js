/**
 * � 2015 NetSuite Inc.
 * User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
**/

var FAM;
if (!FAM) { FAM = {}; }

FAM.Reports_CS = new function () {   
    
    /**
     * The recordType (internal id) corresponds to the "Applied To" record in your script deployment
     * @appliedtorecord recordType
     *
     * @param {String} type Sublist internal id
     * @param {String} name Field internal id
     * @param {Number} linenum Optional line item number, starts from 1
     * @returns {Void}
     */
    this.fieldChanged = function (type, name, linenum) {
        switch (name){
            case 'custpage_reporttype':
                var repType = +nlapiGetFieldValue('custpage_reporttype');
                
                nlapiGetField('custpage_showcomponents').setDisplayType('hidden');
                switch(repType){
                	case FAM.ReportType['Asset Register']:
                		nlapiGetField('custpage_showcomponents').setDisplayType('normal');
                	case FAM.ReportType['Asset Summary']:
                		nlapiGetField('custpage_saveresults').setDisplayType('hidden');
                		break;
            		default:
            			nlapiGetField('custpage_saveresults').setDisplayType('normal');
                }
            break;
        }
    };
};
