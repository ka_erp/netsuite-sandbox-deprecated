 /**
 *	File Name		:	WTKA_CustomerRefund.js
 *  Script Type		: 	User Event	
 *	Deployment		: 	Credit Memo	
 *	Event			:	Create	(After Submit)
 *	Function		:	Script to Create Customer Refunds from Credit Memos 
 * 	Authors			:	Rini Thomas
 *	Company			:	Wipro Limited
 *	Release Dates	:	1-June-2016 (v1.0)
 * 	Current Version	:	1.0
**/
 

function afterSubmitCreditMemo(type)
{
	try
	{
		if(type == 'create' && nlapiGetContext().getExecutionContext() == 'userinterface')
		// if(nlapiGetContext().getExecutionContext() == 'userinterface')
		{
			var recID  		= nlapiGetRecordId();
			var record 		= nlapiLoadRecord('creditmemo', recID);
			var creditMemo	= record.getFieldValue('tranid');
			var formID 		= record.getFieldValue('customform');
			if(formID == 189) //Set Form ID - K&A Cash Crash Return form
			{
				var status 			  = record.getFieldValue('status');
				if(status != 'Fully Applied')
				{
					var custID 	  		  = record.getFieldValue('entity');
					var department 	  	  = record.getFieldValue('department');
					var location 	  	  = record.getFieldValue('location');
					var payMethod 		  = record.getFieldValue('custbody_ka_invoice_payment_method');
					var tranDate 		  = record.getFieldValue('trandate');
					var total	 		  = record.getFieldValue('total');
					if(total > 0)
					{
						nlapiLogExecution('DEBUG', 'Credit Memo #' + creditMemo, 'ID: ' + recID);
						var custRefRecordId   = createCustomerRefunds(recID, custID, payMethod, tranDate, location, department); // Create Customer Refund record for Credit Memo
						if(custRefRecordId > 0)
						{
							var custRefRecord = nlapiLoadRecord('customerrefund', custRefRecordId);
							var custRefTranNo = custRefRecord.getFieldValue('tranid');
							nlapiLogExecution('DEBUG', 'Customer Refund #' + custRefTranNo + ' created successfully', 'ID: ' + custRefRecordId);
						}
					}
				}
				else
				{
					nlapiLogExecution('DEBUG', 'Credit Memo #' + creditMemo + ' already in Fully Applied Status', 'ID: ' + recID);
				}
			}
		}
	}
	catch(err_main)
	{
		nlapiLogExecution('debug', 'Error', err_main);
	}
}


function createCustomerRefunds(creditMemo, custID, payMethod, tranDate, location, department)
{
	var custRefRecordId = -1;
	try
	{
		
		var custRefRecord 	= nlapiCreateRecord('customerrefund', {recordmode:'dynamic'} );
		custRefRecord.setFieldValue('customer',		 custID);
		custRefRecord.setFieldValue('paymentmethod', payMethod);
		custRefRecord.setFieldValue('trandate', 	 tranDate);
		custRefRecord.setFieldValue('location', 	 location);
		custRefRecord.setFieldValue('department', 	 department);
		custRefRecord.setFieldValue('account', 		 115); // 12230	: Undeposited Funds/Cash Clearing
		var apply = custRefRecord.getLineItemCount('apply');
		if(apply <= 0)
		{
			nlapiLogExecution('DEBUG', 'No Credit Memos found');
			return custRefRecordId;
		}
		for(var i=1; i <= apply;i++)
		{
			custRefRecord.selectLineItem('apply', i);
			var docId 		 = custRefRecord.getCurrentLineItemValue('apply', 'doc');
			var refundAmount = custRefRecord.getCurrentLineItemValue('apply', 'due');
			
			if(docId == creditMemo) 
			{
			   custRefRecord.setCurrentLineItemValue('apply', 'apply',  'T');
			   custRefRecord.setCurrentLineItemValue('apply', 'amount', refundAmount);
			   custRefRecord.commitLineItem('apply');
			   break;
			}
		}
		custRefRecordId = nlapiSubmitRecord(custRefRecord, true, false);
	}
	catch(err)
	{
		nlapiLogExecution('DEBUG', 'Error in creating Customer Refund', err);
	}
	return custRefRecordId;
}