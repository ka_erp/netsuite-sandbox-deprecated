/**
* © 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code. 
*/

/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 16 Jun 2014 jvelasquez
 * 
 */
var dadGlobal = dadGlobal || {};
dadGlobal.CREATE_PERMISSION = 2;

function dadShowFolderInIframe(folderId) {
    dadGlobal.folderId = folderId;
    Ext.get('dadFileCabinet').dom.src = "/app/common/media/mediaitemfolders.nl?folder=" + dadGlobal.folderId + "&ifrmcntnr=T&" + (new Date());
}

// continuously get the selected folder
setInterval(function() {
    var folderName = jDnd('#dadFileCabinet').contents().find("#div__medialisthdr").text();
    if (dadHasNoValue(folderName)) {
        folderName = 'none';
    }
    Ext.get('folderinfo').update(folderName);
    var links = jDnd('#dadFileCabinet').contents().find("#div__medialisthdr a");
    if (links.length === 0) {
        return;
    }

    var folderid = links[links.length - 1].href.replace('javascript:showFolderContents(', '').replace(')', '');
    // Ext.get('folderid').update(folderid);
    dadGlobal.folderId = folderid;

}, 1000);

// set the nav_tree
setInterval(function() {

    // Issue: 297853 [DnD] New 14.2 UI: Folder tree does not expand inside
    // DnD e
    // get the variable nav_tree of the iframe and assign it as to variable
    // nav_tree of the parent page, we got nav_tree from the error message
    var iframeNavTree = jDnd('#dadFileCabinet').get(0).contentWindow.nav_tree;
    if (iframeNavTree) {
        nav_tree = iframeNavTree;
    }
}, 1000);

var dadGlobal = dadGlobal || {};
dadGlobal.folderFullName = 'Getting full folder name ...';
dadGlobal.browserSupported = true;

/**
 * Adds a event handler for beforeunload event. The handler prompts a warning
 * when leaving page while files are being uploaded.
 */
function dadAddBeforeUnloadEventHandler() {
    window.addEventListener('beforeunload', function(e) {
        try {
            // get number of pending uploads
            var currentUploadHash = dadGlobal.currentUploadHash;
            var pendingUploadsCount = Object.keys(currentUploadHash).length;
            if (pendingUploadsCount > 0) {
                // display prompt to whether to stay or navigate
                // away from page
                // to user
                // When a non-empty string is assigned to the
                // returnValue Event
                // property, a dialog box appears,
                // asking the users for confirmation to leave
                // the page.
                // When no value is provided, the event is
                // processed silently.

                // from:
                // https://developer.mozilla.org/en-US/docs/Web/Reference/Events/beforeunload
                // For some reasons, Webkit-based browsers don't
                // follow the spec
                // for the dialog box. An almost cross-working
                // example would be
                // close from the below example.
                var confirmationMessage = "You have an upload in progress. Leaving the page will cancel the upload.\n\nThis page is asking you to confirm that you want to leave - data you have entered may not be saved.";
                e = e || window.event;
                e.returnValue = confirmationMessage; // Gecko
                // + IE
                if (Ext.isChrome) {
                    return confirmationMessage; // Webkit,
                    // Safari,
                    // Chrome etc.
                }
            }
        } catch (e) {
            // ignore error since we do not know what happens
            // when an error
            // occurs on this event
        }
    }, false);
}

/**
 * Stops the propagation of an event
 * 
 * @param {Object}
 *        e Event
 */
function dadStopPropagation(e) {
    if (e.preventDefault) {
        e.preventDefault();
    }
    if (e.stopPropagation) {
        e.preventDefault();
    }
    return false;
}

/**
 * Sets the text/html of the status
 * 
 * @param {Object}
 *        status
 */
function dadSetStatus(status) {
    status = status.replace(/ /g, '&nbsp;');
    Ext.get('dadDropZoneBackgroundStatus').update(status);
}

dadGlobal.filesBeingUploaded = 0;
dadGlobal.filesBeingUploadedBatch = 0;
dadGlobal.batchSize = 0;
// one xmlRequest instance is used for each file
dadGlobal.xmlRequests = [];
// this is used as current index for dadGlobal.xmlRequests
dadGlobal.uploadId = 0;
/**
 * Actions to take when a file is dropped into the richtext editor
 * 
 * @param {Object}
 *        evt
 */
var dadGlobal = dadGlobal || {};
function dadFileDrop(evt) {
    dadWait();
    try {

        var logger = new dadobjLogger(arguments);
        dadGlobal.dadIsDragEnter = false;
        evt = evt || window.event;
        Ext.Msg.hide();
        Ext.get('dadInfo').hide();
        var e = evt || window.event;
        var target = e.target || e.srcElement;

        if (target.id == 'dadDropZone') {
            header = target;
            Ext.get('dadDropZoneBackground').setStyle('backgroundColor', 'transparent');
        }

        var folderName = jDnd('#folderinfo').text();
        if (folderName === 'none') {
            Ext.MessageBox.show({
                title : dadGlobal.TITLE,
                msg : 'Please select a folder destination',
                buttons : Ext.MessageBox.OK,
                icon : Ext.MessageBox.WARNING
            });
            return dadStopPropagation(evt);
        }

        var files = evt.dataTransfer.files;
        var count = files.length;
        if (count === 0) {
            logger.log('no files');
            return dadStopPropagation(evt);
        }

        var isFolderInactive = nlapiLookupField('folder', dadGlobal.folderId, 'isinactive');
        if (isFolderInactive === 'T') {
            dadGlobal.folderId = null;
            uiShowWarning('You are uploading to an inactive folder. Please select a different folder.', dadGlobal.TITLE, function() {
            });
            return dadStopPropagation(evt);
        }

        // check if there are existing files
        var fileNames = [];
        for (var i = 0; i < count; i++) {
            var file = files[i];
            logger.log('file=' + JSON.stringify(file));
            logger.log('file.name=' + file.name);
            fileNames.push(file.name);

            // check file size, do not allow more than 5mb
            var MAXIMUM_IMAGE_SIZE = 5 * 1000 * 1000;
            if (file.size > MAXIMUM_IMAGE_SIZE) {
                Ext.MessageBox.show({
                    title : dadGlobal.TITLE,
                    msg : 'The file ' + file.name + ' exceeds the maximum file size of ' + MAXIMUM_IMAGE_SIZE / 1000 / 1000 + ' mb.',
                    buttons : Ext.MessageBox.OK,
                    icon : Ext.MessageBox.WARNING
                });
                return dadStopPropagation(evt);
            }
        }

        var param = {};
        // param.scriptId = nlapiGetRecordType();
        param.fileNames = fileNames;
        param.folderId = dadGlobal.folderId;
        logger.log('fileNames=' + JSON.stringify(fileNames));
        dadSetStatus('Checking existing files...');
        dadSuiteletProcessAsync('getExistingFiles', param, function(existingFileNames) {
            logger.log('existingFileNames=' + JSON.stringify(existingFileNames));
            if (existingFileNames.length === 0) {
                // no existing files
                jDnd('#dadDropZoneBackground').effect('highlight', {}, 3000);
                dadUploadNow();
                return dadStopPropagation(evt);
            }
            // there are existing files
            Ext.get('dadInfo').update('Existing files found.');
            var fileNamesStr = '';
            for (var f = 0; f < existingFileNames.length; f++) {
                fileNamesStr = fileNamesStr + '<br>' + existingFileNames[f];
            }
            var question = existingFileNames.length > 1 ? 'The following existing files will be overwritten: ' : 'This existing file will be overwritten: ';
            question = question + fileNamesStr + '.<br><br>Continue?';
            Ext.MessageBox.confirm(dadGlobal.TITLE, question, function(btn) {
                if (btn == 'no') {
                    Ext.get('dadInfo').hide();
                    dadResetStatus();
                    uiShowInfo('No files were copied.');
                    return dadStopPropagation(evt);
                }
                jDnd('#dadDropZoneBackground').effect('highlight', {}, 3000);
                dadUploadNow();
            });
        });

    } catch (e) {
        // TODO: handle exception
        alert(e.toString());
        return dadStopPropagation(evt);
    }

    return dadStopPropagation(evt);

    function dadUploadNow() {

        var logger = new dadobjLogger(arguments, false);
        logger.log('count=' + count);

        dadGlobal.filesBeingUploaded = parseInt(dadGlobal.filesBeingUploaded, 10) + count;
        var status = 'Uploading ' + dadGlobal.filesBeingUploaded + ' file' + (dadGlobal.filesBeingUploaded == 1 ? '' : 's') + '...';
        Ext.get("dadInfo").update(status);
        dadSetStatus(status);

        var xmlRequests = dadGlobal.xmlRequests;
        if (dadHasNoValue(dadGlobal.url)) {
            dadGlobal.url = nlapiResolveURL('SUITELET', 'customscript_dad_user_service_sl', 'customdeploy_dad_user_service_sl');
        }
        var url = dadGlobal.url;
        for (var i = 0; i < count; i++) {
            var file = files[i];
            logger.log('i=' + i);
            dadGlobal.filesBeingUploadedBatch = dadGlobal.filesBeingUploadedBatch + 1;
            dadGlobal.batchSize = dadGlobal.batchSize + file.size;

            // include other info
            var vFD = new FormData();
            vFD.append('file', file);
            // vFD.append('recordtype', nlapiGetRecordType());
            // vFD.append('recordid', nlapiGetRecordId());
            vFD.append('folderId', dadGlobal.folderId);

            // if (header === null && textbox === null) {
            // vFD.append('subListId', subListId);
            // vFD.append('lineid', lineId);
            // } else {
            // vFD.append('subListId', '');
            // vFD.append('lineid', '');
            // if (dadHasValue(textbox)) {
            // vFD.append('textboxid', textbox.getAttribute('id'));
            // }
            // }

            xmlRequests.push(new XMLHttpRequest());
            // oXHR.upload.addEventListener('progress', uploadProgress, false);
            // oXHR.addEventListener('load', uploadFinish, false);
            // oXHR.addEventListener('error', uploadError, false);
            // oXHR.addEventListener('abort', uploadAbort, false);

            var uploadId = dadGlobal.uploadId;
            logger.log('uploadId=' + uploadId);
            logger.log('xmlRequests.length=' + xmlRequests.length);

            xmlRequests[uploadId].uploadId = uploadId;
            xmlRequests[uploadId].fileName = file.name;
            xmlRequests[uploadId].action = 'uploadInFileCabinet';

            xmlRequests[uploadId].open('POST', url, true /* async */);
            xmlRequests[uploadId].onreadystatechange = function() {

                logger.log('this.readyState=' + this.readyState);
                logger.log('this.status=' + this.status);
                // document.getElementById("dadInfo").innerHTML =
                // 'Uploading...';
                if (this.readyState == 4 && this.status != 200) {
                    logger.log('ERROR: this.status=' + this.status);
                }

                if (this.readyState == 4 && this.status == 200) {

                    jDnd('#dadFileCabinet').contents().find("*").hide();

                    Ext.get('dadFileCabinet').dom.src = "/app/common/media/mediaitemfolders.nl?folder=" + dadGlobal.folderId + "&ifrmcntnr=T&" + (new Date());
                    Ext.Msg.hide();
                    // upload completed
                    dadSetStatus(xmlRequests[uploadId].upload.fileName + ' upload completed.');

                    // var returnValue = dadHandleResponse(this);
                    // logger.log('returnValue=' + returnValue);
                    // logger.log('returnValue=' + JSON.stringify(returnValue));
                    // logger.log('this.uploadId=' + this.uploadId);
                    // // alert('this.uploadId=' + this.uploadId);
                    delete dadGlobal.currentUploadHash[this.uploadId];
                    //
                    // var dadFile = returnValue;
                    // // check error
                    // if (dadHasValue(dadFile.error)) {
                    // dadGlobal.filesBeingUploaded = parseInt(
                    // dadGlobal.filesBeingUploaded, 10) - 1;
                    // dadShowWarning(dadFile.error);
                    // return;
                    // }
                    // if (dadHasValue(dadFile.textboxid)) {
                    // // append url to textbox
                    // target.value = target.value + ' ' + dadFile.fileName
                    // + ' https://' + window.location.hostname
                    // + nlapiResolveURL('mediaitem', dadFile.fileId);
                    // NS.form.setChanged(true);
                    // }

                    // // addOrUpdateOrDeleteDadFile
                    // dadSuiteletProcessAsync(
                    // 'addOrUpdateOrDeleteDadFile',
                    // returnValue,
                    // function(data) {
                    // dadSetStatus(xmlRequests[uploadId].upload.fileName
                    // + ' upload completed.');
                    // })
                    //
                    dadGlobal.filesBeingUploaded = parseInt(dadGlobal.filesBeingUploaded, 10) - 1;
                }
            };

            // progress bar
            xmlRequests[uploadId].upload.fileName = file.name;
            xmlRequests[uploadId].upload.uploadId = uploadId;

            xmlRequests[uploadId].upload.addEventListener("progress", function(e) {
                dadGlobal.currentUploadHash[this.uploadId].loaded = e.loaded;
            }, false);

            dadGlobal.currentUploadHash[uploadId] = {};
            dadGlobal.currentUploadHash[uploadId].fileName = file.name;
            dadGlobal.currentUploadHash[uploadId].fileSize = file.size;
            dadGlobal.currentUploadHash[uploadId].loaded = 0;
            dadGlobal.currentUploadHash[uploadId].startGetTime = null; // (new
            // Date()).getTime();
            dadGlobal.currentUploadHash[uploadId].uploadId = uploadId;
            xmlRequests[uploadId].send(vFD);
            if (dadHasNoValue(dadGlobal.dadShowUploadProgress)) {
                dadGlobal.dadShowUploadProgress = setInterval('dadShowUploadProgress();', 1000);
            }

            dadGlobal.uploadId = uploadId + 1;

        }
        // show detailed progress
        // dadShowFolderMenu();
    }
    return dadStopPropagation(evt);
}

// holds the current uploads where each active upload is one property. The key
// is the upload id.
dadGlobal.currentUploadHash = {};
function dadShowUploadProgress() {
    var logger = new dadobjLogger(arguments, true);
    // if (dadGlobal.dadIsDragEnter) {
    // logger.log('dadGlobal.dadIsDragEnter=' + dadGlobal.dadIsDragEnter);
    // return;
    // }

    var currentUploadHash = dadGlobal.currentUploadHash;
    var pendingUploadsCount = Object.keys(currentUploadHash).length;
    if (pendingUploadsCount > 1) {
        dadGlobal.isMultipleFiles = true;
    }
    if (pendingUploadsCount === 0) {
        dadGlobal.batchSize = 0;
        // no more uploads
        if (Ext.get('dadProgressBars')) {
            Ext.get('dadProgressBars').update('None');
        }
        clearInterval(dadGlobal.dadShowUploadProgress);
        delete dadGlobal.dadShowUploadProgress;
        if (dadGlobal.folderTooltip) {
            dadGlobal.folderTooltip.syncSize();
        }

        if (dadGlobal.isMultipleFiles) {
            dadSetStatus('Your files have been uploaded successfully.');
            dadGlobal.isMultipleFiles = false;
        }

        setTimeout('dadResetStatus();', 10000);
        return;
    }

    // there are still pending uploads
    var totalBytesSize = 0;
    var totalBytesLoaded = 0;
    var totalBytesRemain = 0;
    var totalSecondsElapsed = 0;
    var currentGetTime = (new Date()).getTime();
    var secondsRemainingLongest = 0;
    var largestFileSizeInQueue = 0;
    var fileCountInQueue = 0;
    for ( var p in currentUploadHash) {
        var o = currentUploadHash[p];
        // logger.log('o.fileSize=' + o.fileSize);
        totalBytesSize = totalBytesSize + o.fileSize;
        logger.log('o.loaded=' + o.loaded);
        if (o.loaded === 0) {
            // do not include yet in computation of seconds remaining since it
            // will result in infinity seconds
            if (o.fileSize > largestFileSizeInQueue) {
                largestFileSizeInQueue = o.fileSize;
            }
            fileCountInQueue = fileCountInQueue + 1;
            totalBytesRemain = totalBytesRemain + o.fileSize;
            continue;
        }
        totalBytesRemain = totalBytesRemain + (o.fileSize - o.loaded);
        if (dadHasNoValue(o.startGetTime)) {
            o.startGetTime = (new Date()).getTime();
        }

        var secondsElapsed = (currentGetTime - o.startGetTime) / 1000;
        logger.log('secondsElapsed=' + secondsElapsed);
        var bytesPerSecond = o.loaded / secondsElapsed;
        logger.log('bytesPerSecond=' + bytesPerSecond);
        logger.log('o.fileSize - o.loaded=' + (o.fileSize - o.loaded));
        var secondsRemaining = (o.fileSize - o.loaded) / bytesPerSecond;
        logger.log('secondsRemaining=' + secondsRemaining);
        if (secondsRemaining < 0) {
            secondsRemaining = 0;
        }
        if (secondsRemaining > secondsRemainingLongest) {
            secondsRemainingLongest = secondsRemaining
        }
        totalSecondsElapsed = totalSecondsElapsed + secondsElapsed;
        totalBytesLoaded = totalBytesLoaded + o.loaded;
        totalBytesSize = totalBytesSize + o.fileSize;
    }

    var bytesPerSecond = totalBytesLoaded / totalSecondsElapsed;
    // add the longest seconds remaining and the estimated time for the largest
    // file in queue
    var totalSecondsRemaining = secondsRemainingLongest;
    dadGlobal.MAXIMUM_PARALLEL_UPLOADS = 6;
    if (bytesPerSecond > 0) {
        var batchCountInQueue = fileCountInQueue / dadGlobal.MAXIMUM_PARALLEL_UPLOADS;
        batchCountInQueue = parseInt(batchCountInQueue, 10);
        if (fileCountInQueue % dadGlobal.MAXIMUM_PARALLEL_UPLOADS > 0) {
            batchCountInQueue = batchCountInQueue + 1;
        }
        var secondsInQueue = batchCountInQueue * largestFileSizeInQueue / bytesPerSecond;
        totalSecondsRemaining = totalSecondsRemaining + secondsInQueue;
    }

    var completed = parseInt(100 * (dadGlobal.batchSize - totalBytesRemain) / dadGlobal.batchSize, 10);
    // var completed = parseInt(100 * (dadGlobal.filesBeingUploadedBatch -
    // pendingUploadsCount) / dadGlobal.filesBeingUploadedBatch);
    if (completed > 100) {
        completed = 100;
    }
    var status = 'Uploading ' + pendingUploadsCount + ' file' + (pendingUploadsCount == 1 ? '' : 's');
    status = status + ' &middot; ' + completed + '%';
    var totalSecondsRemainingRounded = Math.round(totalSecondsRemaining);
    if (totalSecondsRemainingRounded > 1) {
        status = status + ' &middot; ' + totalSecondsRemainingRounded + ' seconds remaining'
    } else if (totalSecondsRemainingRounded == 1) {
        status = status + ' &middot; ' + totalSecondsRemainingRounded + ' second remaining'
    } else {
        if (totalSecondsRemaining > 0 && completed > 0) {
            status = status + ' &middot; less than 1 second remaining';
        }
    }

    // var fullWidth = Ext.get('dadDropZone').getWidth() - 30;
    var fullWidth = Ext.get('dadDropZone').getWidth();

    status = status.replace(/ /g, '&nbsp;');
    Ext.get('dadDropZoneBackgroundStatus').update(status);
    var width = fullWidth * completed / 100;
    if (width > fullWidth) {
        width = fullWidth;
    }
    var xBar = Ext.get('dadDropZoneBackgroundBar');
    xBar.show();
    xBar.dom.style.width = width;

    // ===================================================
    // each file
    // ===================================================
    var dadProgressBars = Ext.get('dadProgressBars');
    if (dadProgressBars === null) {
        // tooltip not displayed so exit
        return;
    }
    dadProgressBars.update('');
    var dadProgressBarsWidth = dadProgressBars.getWidth();
    for ( var p in currentUploadHash) {
        var o = currentUploadHash[p];
        logger.log('o=' + JSON.stringify(o));
        logger.log('o.fileSize=' + o.fileSize);
        totalBytesSize = totalBytesSize + o.fileSize;
        logger.log('o.loaded=' + o.loaded);

        totalBytesLoaded = totalBytesLoaded + o.loaded;
        var secondsElapsed = (currentGetTime - o.startGetTime) / 1000;
        logger.log('secondsElapsed=' + secondsElapsed);
        if (secondsElapsed === 0 || o.loaded === 0) {
            // will result in infinity seconds
            html = '<div class="dadProgressBarBox"><div class="dadProgressStatus"><div class="dadFileStatusText">' + o.fileName + ' &middot; ' + ' Waiting in queue...</div>' + '<a href=# class="dadCancelLink" onclick="dadAbortUpload(event, ' + o.uploadId + '); return false;">Cancel</a></div></div>';
            dadProgressBars.insertHtml('beforeEnd', html);
            continue;
        }

        var bytesPerSecond = o.loaded / secondsElapsed;
        logger.log('bytesPerSecond=' + bytesPerSecond);
        logger.log('o.fileSize - o.loaded=' + (o.fileSize - o.loaded));
        var secondsRemaining = (o.fileSize - o.loaded) / bytesPerSecond;
        if (secondsRemaining < 0) {
            secondsRemaining = 0;
        }
        secondsRemaining = Math.round(secondsRemaining);

        var completed = Math.round(100 * o.loaded / o.fileSize);
        if (completed > 100) {
            completed = 100;
        }
        var width = Math.round(o.loaded / o.fileSize * dadProgressBarsWidth) + 'px';

        // logger.log('secondsRemaining=' + secondsRemaining);
        // totalSecondsRemaining = totalSecondsRemaining + secondsRemaining;
        status = o.fileName + ' &middot; ' + completed + '%';
        if (secondsRemaining === 0) {
            status = status + ' &middot; less than a second remaining';
        } else {
            status = status + ' &middot; ' + secondsRemaining + ' second' + (secondsRemaining > 1 ? 's' : '') + ' remaining'
        }

        status = status.replace(/ /g, '&nbsp;');
        var progressBar = '<div class="dadProgressBar" style="width: ' + width + '">&nbsp;</div>'
        // Issue: 259152 [DnD] Allow canceling of files in queue
        // this comment just shows the location of the change.
        // the actual change is in CL 600387
        var progressStatus = '<div class="dadProgressStatus"><div class="dadFileStatusText">' + status + '</div><a href=# class="dadCancelLink" onclick="dadAbortUpload(event, ' + o.uploadId + '); return false;">Cancel</a>' + '</div>'

        html = '<div class="dadProgressBarBox clearfix">' + progressBar + progressStatus + '</div>';
        dadProgressBars.insertHtml('beforeEnd', html);
    }
    dadGlobal.folderTooltip.syncSize();
}

function dadAbortUpload(evt, uploadId) {
    dadSetStatus(dadGlobal.xmlRequests[uploadId].upload.fileName + ' upload cancelled.');
    dadGlobal.xmlRequests[uploadId].abort();
    delete dadGlobal.currentUploadHash[uploadId];

    evt = evt || window.event;
    dadStopPropagation(evt);
}

/**
 * Returns true if the passed element is a NS textbox or textarea
 * 
 * @param {Object}
 *        el
 */
function dadIsInput(el) {
    if (typeof el.getAttribute == 'undefined') {
        // this happens in line items
        return false;
    }

    var cls = el.getAttribute('class');
    return [ 'input textarea', 'inputreq textarea', 'input', 'inputreq' ].indexOf(cls) > -1;
}

/**
 * Event handler for other drag-and-drop events. Used for demo purposes.
 * 
 * @param {Object}
 *        evt
 */
function dadNoOperationHandler(evt) {
    return dadStopPropagation(evt);
}

// flag if drag enter is triggered and drag exit is not yet triggered
dadGlobal.dadIsDragEnter = false;
function dadDragEnter(e) {
    var logger = new dadobjLogger(arguments);
    e = e || window.event;
    var target = e.target || e.srcElement;

    dadResetStatus();
    dadGlobal.dadIsDragEnter = true;

    var xDadInfo = Ext.get('dadInfo');
    if (target.id == 'dadDropZone') {
        // record level
        // highlight drop zone
        Ext.get(target.id + 'Background').setStyle('backgroundColor', 'lightgreen');
    } else if (dadIsInput(target) === true) {
        // text boxes
        Ext.get(target).setStyle('backgroundColor', 'lightgreen');
    } else {
        // line item level
        Ext.get('dadInfo').update('Drop your file here to attach to this line item');
        // find the parent table row
        // for some reasons, calling Ext.get() on a literal returns null, so get
        // parent if needed
        var notNullTarget = Ext.get(target) || Ext.get(target.parentNode);
        var xTr = (notNullTarget).findParent('tr', 50, true);
        xTr.select('td').setStyle('backgroundColor', 'lightgreen');

        logger.log('xTr.dom.id=' + xTr.dom.id);

        // get parent table
        var xTable = xTr.findParent('.listtable', 50, true);
        logger.log('xTable.dom.id=' + xTable.dom.id);
        var listId = xTable.dom.id.replace('_splits', '');
        logger.log('listId=' + listId);
        var lineNum = xTr.dom.id.replace(listId + '_row_', '');
        lineNum = parseInt(lineNum, 10);
        if (nlapiGetLineItemValue(listId, 'id', lineNum) == '') {
            xTr.select('td').setStyle('backgroundColor', 'pink');
            Ext.get('dadInfo').update('The line item needs to be saved before you can attach file.');
        }
        // check if new, if yes, do not allow drop
        xDadInfo.setTop(xTr.getTop() - Ext.get('dadInfo').getHeight());
        xDadInfo.setLeft(e.clientX + 30);
        Ext.get('dadInfo').show();
    }

    return dadStopPropagation(e);
}

function dadDragExit(e) {
    var logger = new dadobjLogger(arguments);
    e = e || window.event;

    dadGlobal.dadIsDragEnter = false;

    var target = e.target || e.srcElement;
    if (target.id == 'dadDropZone') {
        Ext.get(target.id + 'Background').setStyle('backgroundColor', 'transparent');
    } else if (dadIsInput(target) === true) {
        // text boxes
        Ext.get(target).setStyle('backgroundColor', '');
    } else {
        if ((target.outerHTML || '').indexOf('<tr') == -1) {
            if (Ext.get(target) === null) {
                // target might be a text content
                return;
            }
            target = Ext.get(target).findParent('tr');
        }
        Ext.get(target).select('td').setStyle('backgroundColor', 'transparent');
    }
    Ext.get('dadInfo').hide();
    logger.log('end');

    return dadStopPropagation(e);
}

/**
 * Add drag and drop events to an element
 * 
 * @param {Object}
 *        el
 */
function dadAddDnDEvents(el) {
    dadAddEvent(el, "drop", dadFileDrop);
    dadAddEvent(el, "dragenter", dadDragEnter);
    dadAddEvent(el, "dragover", dadNoOperationHandler);
    if (Ext.isGecko) {
        // FF
        dadAddEvent(el, "dragexit", dadDragExit);
    } else {
        // chrome, safari
        dadAddEvent(el, "dragleave", dadDragExit);
    }
}

function dadPositionRecordDropZone() {
    var logger = new dadobjLogger(arguments, true);

    // var xHeader = Ext.get('div__header');
    var x = Ext.get('dadDropZoneBackgroundStatus');
    // var width = xHeader.dom.clientWidth;
    var width = jDnd('#div__header').outerWidth();
    x.setWidth(width);

    var xRecordDropZoneBackground = Ext.get('dadDropZoneBackground');
    var xRecordDropZone = Ext.get('dadDropZone');
    xRecordDropZone.setWidth(width);
    xRecordDropZoneBackground.setWidth(width);
    xRecordDropZone.setTop(134);
    xRecordDropZone.show();

    xRecordDropZoneBackground.setHeight(xRecordDropZone.getHeight());
    xRecordDropZoneBackground.setTop(xRecordDropZone.getTop());
    xRecordDropZoneBackground.setLeft(xRecordDropZone.getLeft());
    xRecordDropZoneBackground.show();
}

function dadShowMenu() {

    if (dadGlobal.browserSupported === false) {
        return;
    }
    if (dadGlobal.folderTooltip) {
        // dadGlobal.folderTooltip.show();
        return;
    }
    if (dadGlobal.userPermission === false) {
        return;
    }

    // ========================================
    // show destination folder
    // ========================================
    var html = '';
    html = html + 'You can upload up to 5 files simultaneously.';
    html = html + '<br><br>';

    // ========================================
    // pending uploads
    // ========================================
    html = html + 'Pending uploads:';
    html = html + '<div id="dadProgressBars" style="border: 0px dotted red">';
    html = html + 'None';
    html = html + '</div>';

    var target = 'dadDropZone'
    var tooltipId = target + 'Tooltip';
    var tooltip = (new Ext.ToolTip({
        title : '',
        id : tooltipId,
        target : target,
        anchor : 'top',
        html : html,
        autoHide : false,
        closable : true,
        autoShow : true,
        hideDelay : 10000,
        dismissDelay : 0,
        anchorOffset : 0,
        width : 400,
        height : 'auto',
        bodyStyle : 'padding: 10px; background-color: white',
        listeners : {

            'hide' : function() {
                this.destroy();
                delete dadGlobal.folderTooltip;
            },
            'renderX' : function() {

                this.header.on('click', function(e) {
                    e.stopEvent();
                    Ext.Msg.alert('Link', 'Link to something interesting.');

                }, this, {
                    delegate : 'a'
                });
            }
        }
    }));
    dadGlobal.folderTooltip = tooltip;
    tooltip.show();

    // show individual file progress if needed
    var currentUploadHash = dadGlobal.currentUploadHash;
    var pendingUploadsCount = Object.keys(currentUploadHash).length;
    if (pendingUploadsCount > 0) {
        dadShowUploadProgress();
    }

    return false;
}

function dadResetStatus() {
    var logger = new dadobjLogger(arguments);
    var status = 'Drop files here.';
    status = status.replace(/ /g, '&nbsp;');
    Ext.get('dadDropZoneBackgroundBar').hide();
    if (Ext.get('dadDropZoneBackgroundStatus').dom.innerHTML != status) {
        Ext.get('dadDropZoneBackgroundStatus').update(status);
    }
    // S3 - Issue 262998 : [DnD] I want to close the drag & drop balloon after
    // all files in queue have been uploaded, together with the confirmation
    // message appearing after upload completion.
    // if (dadHasValue(dadGlobal.folderTooltip)) {
    // dadGlobal.folderTooltip.hide()
    // }
    logger.end();
}

/**
 * Attaches an event to an element
 * 
 * @param {Object}
 *        elem Element
 * @param {Object}
 *        evnt Event
 * @param {Object}
 *        func Function
 */
function dadAddEvent(elem, evnt, func) {
    if (elem.addEventListener) // W3C DOM
        elem.addEventListener(evnt, func, false);
    else if (elem.attachEvent) { // IE DOM
        elem.attachEvent("on" + evnt, func);
    } else { // No much to do
        elem[evnt] = func;
    }
}

dadGlobal.folderFullName = '';
function dadPageInit() {
    var logger = new dadobjLogger(arguments);
    Ext.getBody().setStyle({
        overflow : 'hidden'
    });

    // check browser
    if (Ext.isChrome || Ext.isGecko) {
        var xHeader = Ext.get('div__header');
        var x = Ext.get('dadDropZoneBackgroundStatus');
        x.setWidth(xHeader.dom.clientWidth);

        dadResetStatus();
        dadPositionRecordDropZone();

        var userPermission = nlapiGetContext().getPermission('LIST_FILECABINET');
        // user has either view or no access to documents and files
        if (userPermission < dadGlobal.CREATE_PERMISSION) {
            dadGlobal.userPermission = false;
            dadSetStatus('You do not have access to Documents and Files. Contact your account administrator to request for access.');
        } else {
            var dRecordDropZone = Ext.get('dadDropZone').dom;
            dadAddDnDEvents(dRecordDropZone);
            dadAddBeforeUnloadEventHandler();
        }

    } else {
        dadGlobal.browserSupported = false;
        dadSetStatus('Use the latest version of Chrome or Firefox to enable file drag and drop.');
        dadPositionRecordDropZone();
        Ext.get('dadDropZone').dom.title = '';
    }

    Ext.get('dadFileCabinet').dom.setAttribute('src', '/app/common/media/mediaitemfolders.nl?whence=&ifrmcntnr=T');
    dadResizeElements();
    Ext.get('dadFileCabinet').show();

    jDnd(window).resize(function() {
        dadPositionRecordDropZone();
        dadResizeElements();

    });
    jDnd(Ext.getBody()).resize(function() {
        dadPositionRecordDropZone();
        dadResizeElements();
    });

    jDnd('body').click(function(e) {
        if (dadHasValue(dadGlobal.folderTooltip) && (e.target.id !== 'dadDropZone')) {
            dadGlobal.folderTooltip.hide();
        }
    });

    // Issue: 298529 [DnD] DnD File Cabinet > File Cabinet's Search is not
    /* make the search file work */
    setInterval(function() {
        // add server commands
        var jIframe = jDnd('#dadFileCabinet');
        if (jIframe.contents().find('#server_commands').get(0) === undefined) {
            var serverCommandsMarkup = '<iframe src="javascript:false" id="server_commands" name="server_commands" style="display:none" height="0"></iframe>';
            jIframe.contents().find('#div__body').append(serverCommandsMarkup);
        }

        // change wait cursor code to arrow
        var jSearchButton = jIframe.contents().find('#mediasrch_b');
        if (jSearchButton.length === 0) {
            return;
        }
        var onClickCode = jSearchButton.get(0).getAttribute('onclick');
        onClickCode = onClickCode.replace("wait", 'arrow');
        jSearchButton.attr('onclick', onClickCode);
    }, 1000);

}

/**
 * Adds trim functions if browser has no support
 */
function dadAddTrimFunctions() {
    if (typeof String.trim == 'undefined') {
        String.prototype.trim = function() {
            return this.replace(/^\s+|\s+$/g, "");
        };
    }

    if (typeof String.ltrim == 'undefined') {
        String.prototype.ltrim = function() {
            return this.replace(/^\s+/, "");
        };
    }

    if (typeof String.rtrim == 'undefined') {
        String.prototype.rtrim = function() {
            return this.replace(/\s+$/, "");
        };
    }
}

dadAddTrimFunctions();

Ext.onReady(function() {
    dadPageInit();
    jDnd('#dadFileCabinet').load(function() {
        jDnd('#dadFileCabinet').contents().find("body").on('click', function(e) {
            if (dadHasValue(dadGlobal.folderTooltip) && (e.target.id !== 'dadDropZone')) {
                dadGlobal.folderTooltip.hide();
            }
        });
    });

});

function dadResizeElements() {
    var logger = new dadobjLogger(arguments);
    var xDropZone = Ext.get('dadDropZone');
    var xFrame = Ext.get('dadFileCabinet');
    var xHeader = Ext.get('div__header');

    // set iframe height
    dadGlobal.HEIGHT_ALLOWANCE = 0;
    if (Ext.isChrome) {
        dadGlobal.HEIGHT_ALLOWANCE = 30;
    }
    var iframeHeight = Ext.getBody().dom.clientHeight - xDropZone.getTop() - xDropZone.getHeight() - dadGlobal.HEIGHT_ALLOWANCE;
    xFrame.setHeight(iframeHeight);

    // set width
    var headerWidth = parseInt(xHeader.getWidth());
    logger.log('headerWidth=' + headerWidth);
    xFrame.setWidth(headerWidth + 20);
    xDropZone.setWidth(headerWidth);

    Ext.getBody().setStyle({
        overflow : 'hidden'
    });
}
