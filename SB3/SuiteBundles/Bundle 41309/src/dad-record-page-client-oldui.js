/**
* © 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code. 
*/

/**
 * Module Description
 * 
 * Version Date Author Remarks 1.00 11 Jun 2014 jvelasquez
 * 
 */
var dadGlobal = dadGlobal || {};
dadGlobal.folderFullName = '';
dadGlobal.browserSupported = true;
dadGlobal.CREATE_PERMISSION = 2;

/**
 * Used in getting user feedback
 */
function dadSendComments() {
    // hide balloon
    dadGlobal.folderTooltip.hide();
    // show send message popup window
    var msg = "<textarea id='dadComments' rows='10' style='width: 100%; font-size: 8pt'>";
    msg += '\r\n';
    msg += '\r\n';
    msg += '\r\n';
    msg += 'User: ' + nlapiGetContext().getName() + '\r\n';
    msg += 'Role: ' + nlapiGetContext().getRoleId() + '\r\n';
    msg += 'Email: ' + nlapiGetContext().getEmail() + '\r\n';
    msg += 'Company: ' + nlapiGetContext().getCompany() + '\r\n';
    msg += '</textarea>';
    msg += '<div style="background-color: white; border: 1px solid gray; margin-top: 2px; padding: 2px; vertical-align: middle; font-size: 8pt"><span style="top: -3px; position: relative">Rating:</span> ' + dadGetRatingMarkup() + '</div>';
    Ext.Msg.show({
        title : 'File Drag and Drop &raquo; Let us know what you think',
        msg : msg,
        width : 480,
        buttons : Ext.MessageBox.OKCANCEL,
        multiline : false,
        fn : dadSendCommentsCallback,
        animEl : 'addAddressBtn',
        icon : Ext.MessageBox.INFO,
        buttons : {
            ok : "Send",
            cancel : "Cancel"
        }
    });

    setTimeout("Ext.get('dadComments').dom.focus();", 500);

    return false;
}

/**
 * Submits the private message.
 * 
 * @param {Object}
 *        buttonId
 * @param {Object}
 *        text
 * @param {Object}
 *        opt
 */
function dadSendCommentsCallback(buttonId, text, opt) {
    if (buttonId == 'cancel') {
        return false;
    }
    var msg = Ext.get('dadComments').dom.value.trim();
    if (msg === '') {
        uiShowWarning('Please provide message.', null, function() {
            dadSendComments();
        });
        return;
    }
    nlapiSendEmail(nlapiGetUser(), 'suitelabs-support@netsuite.com', 'File Drag and Drop Feedback', msg + '\r\nRating: ' + dadGlobal.rating + (dadGlobal.rating == 1 ? ' star' : ' stars'));
    if (Ext.isChrome) {
        alert('Your message has been sent.', 'Confirmation');
    } else {
        uiShowInfo('Your message has been sent.', 'Confirmation');
    }
}

/**
 * Adds a event handler for beforeunload event. The handler prompts a warning
 * when leaving page while files are being uploaded.
 */
function dadAddBeforeUnloadEventHandler() {
    window.addEventListener('beforeunload', function(e) {
        try {
            // get number of pending uploads
            var currentUploadHash = dadGlobal.currentUploadHash;
            var pendingUploadsCount = Object.keys(currentUploadHash).length;
            if (pendingUploadsCount > 0) {
                // display prompt to whether to stay or navigate away from page
                // to user
                // When a non-empty string is assigned to the returnValue Event
                // property, a dialog box appears,
                // asking the users for confirmation to leave the page.
                // When no value is provided, the event is processed silently.

                // from:
                // https://developer.mozilla.org/en-US/docs/Web/Reference/Events/beforeunload
                // For some reasons, Webkit-based browsers don't follow the spec
                // for the dialog box. An almost cross-working example would be
                // close from the below example.
                var confirmationMessage = "You have an upload in progress. Leaving the page will cancel the upload.\n\nThis page is asking you to confirm that you want to leave - data you have entered may not be saved.";
                e = e || window.event;
                e.returnValue = confirmationMessage; // Gecko + IE
                if (Ext.isChrome) {
                    return confirmationMessage; // Webkit, Safari, Chrome etc.
                }
            }
        } catch (e) {
            // ignore error since we do not know what happens when an error
            // occurs on this event
        }
    }, false)
}

/**
 * Stops the propagation of an event
 * 
 * @param {Object}
 *        e Event
 */
function dadStopPropagation(e) {
    if (e.preventDefault) {
        e.preventDefault();
    }
    if (e.stopPropagation) {
        e.preventDefault();
    }
    return false;
}

function dadOpenPopup(url) {
    var width = screen.width - 200;
    var height = screen.height - 300;
    var left = (screen.width - width) / 2;
    var top = (screen.height - height) / 2;
    top = top - 50; // position window slightly above so that we can see the
    // status bar
    var params = 'width=' + width + ', height=' + height;
    params += ', top=' + top + ', left=' + left;
    params += ', directories=no';
    params += ', location=no';
    params += ', menubar=no';
    params += ', resizable=no';
    params += ', scrollbars=no';
    params += ', status=no';
    params += ', toolbar=no';
    // dadWait('Opening popup window' + '...');
    // IN SS record popup for example, when a row is deleted, NetSuite redirects
    // to a listing instead of showing the form of the parent SS Form. We track
    // the original
    // url so that when we detected the listing, we change the location to this
    // URL
    window.open(url, "mywindow", params);
}

function dadShowFile(fileId) {
    var logger = new dadobjLogger(arguments);
    var url = nlapiResolveURL('mediaitem', fileId);
    logger.log('url=' + url);
    dadOpenPopup(url);
}

function dadDelete(fileId, link) {
    var logger = new dadobjLogger(arguments);
    Ext.get('dadDeleteStatus').update('<img src=/images/setup/hourglass.gif /> Deleting...');
    dadSuiteletProcessAsyncUser('deleteFile', fileId, function(data) {
        if (data != 'ok') {
            Ext.get('dadDeleteStatus').update('&nbsp;');
            uiShowError('File delete failed.');
            return;
        }

        dadSuiteletProcessAsync('deleteDadFile', fileId, function(data) {
            if (data != 'ok') {
                Ext.get('dadDeleteStatus').update('&nbsp;');
                uiShowError('deleteDadFile failed.');
                return;
            }
            // hide row
            var xLink = Ext.get(link);
            var selector = 'tr';
            var xRow = Ext.get(xLink.findParent(selector));
            xRow.fadeOut({
                callback : function() {
                    xRow.dom.style.display = 'none';
                }
            });
            Ext.get('dadDeleteStatus').update('&nbsp;');
            dadGlobal.dadWin.syncSize();
            logger.log('end');
        })
    });
}

function dadShowLineItemFiles(subListId) {
    var logger = new dadobjLogger(arguments);
    dadWait('Please wait...');

    // get current line
    var lineNum = nlapiGetCurrentLineItemIndex(subListId);
    if (lineNum == -1) {
        uiShowInfo('Select a line item.');
        return;
    }

    if (lineNum > nlapiGetLineItemCount(subListId)) {
        uiShowInfo('Select an existing line item.');
        return;
    }
    Ext.get('dadWindowHeader').update('Files Attached to Line: ' + lineNum);
    var lineId = nlapiGetLineItemValue(subListId, 'id', lineNum);
    var param = {
        recordTypeScriptId : nlapiGetRecordType(),
        recordId : nlapiGetRecordId(),
        subListId : subListId,
        lineId : lineId
    };
    dadSuiteletProcessAsync('getLineItemFiles', param, function(fileIds) {
        logger.log('getLineItemFiles; fileIds=' + fileIds);
        if (fileIds.length === 0) {
            uiShowInfo('No files attached for this line item');
            return;
        }
        var filters = [];
        filters.push([ 'internalid', 'anyof', fileIds ]);
        var columns = [];
        columns.push(new nlobjSearchColumn('created'));
        columns[0].setSort(true);
        columns.push(new nlobjSearchColumn('name'));
        columns.push(new nlobjSearchColumn('owner'));
        var results = nlapiSearchRecord('file', null, filters, columns);
        if (results === null) {
            uiShowInfo('No files attached for this line item');
            return;
        }
        var files = [];
        for (var i = 0; i < results.length; i++) {
            var file = {};
            var id = results[i].getId();
            var fileName = results[i].getValue('name');
            file['Date Created'] = results[i].getValue('created');
            file.File = '<a href=# onclick="dadShowFile(' + id + ', this); return false;">' + fileName + '</a>';
            // file['Uploaded By'] = results[i].getText('owner');
            file.Delete = '<a href=# onclick="dadDelete(' + id + ', this); return false;">Delete</a>';
            files.push(file);
        }

        var html = dadCreateTableHtml(files) + '<span id="dadDeleteStatus">&nbsp;</span>';
        // if (Ext.isGecko) {
        // uiShowInfo(html);
        // return;
        // }
        // else {
        // uiShowInfo('<div style="width: 500px; height: 500px;">' + html +
        // '</div>');
        // return;
        // }

        // uiShowInfo is not displaying properly in chrome so try using
        // window.show
        Ext.Msg.hide();
        Ext.get('dadFilesContent').update(html);
        if (dadHasValue(dadGlobal.dadWin)) {
            dadGlobal.dadWin.show();
            return;
        }
        dadGlobal.dadWin = new Ext.Window({
            applyTo : 'dadFilesBox',
            renderTo : window,
            layout : 'fit',
            width : '700',
            height : 'auto',
            closeAction : 'hide',
            plain : false,
            modal : true,
            buttons : [ {
                text : 'Close',
                handler : function() {
                    dadGlobal.dadWin.hide();
                }
            } ]
        });
        // dadGlobal.dadWin.center();
        var left = (Ext.getBody().getWidth() - dadGlobal.dadWin.getWidth()) / 2;
        var top = (Ext.getBody().getHeight() - dadGlobal.dadWin.getHeight()) / 2;
        dadGlobal.dadWin.show();
        dadGlobal.dadWin.setPosition(left, top);
        logger.log('end');
    });
}

/**
 * Sets the text/html of the status
 * 
 * @param {Object}
 *        status
 */
function dadSetStatus(status) {
    status = status.replace(/ /g, '&nbsp;');
    Ext.get('dadRecordDropZoneBackgroundStatus').update(status);
}

dadGlobal.filesBeingUploaded = 0;
dadGlobal.filesBeingUploadedBatch = 0;
dadGlobal.batchSize = 0;
// one xmlRequest instance is used for each file
dadGlobal.xmlRequests = [];
// this is used as current index for dadGlobal.xmlRequests
dadGlobal.uploadId = 0;
/**
 * Actions to take when a file is dropped into the richtext editor
 * 
 * @param {Object}
 *        evt
 */
function dadFileDrop(evt) {
    var logger = new dadobjLogger(arguments);

    dadGlobal.dadIsDragEnter = false;
    var e = evt || window.event;
    var target = e.target || e.srcElement;
    // [DnD] Highlight on textbox is not removed after fail upload of file
    if (target.getAttribute('type') == 'text' || target.outerHTML.indexOf('<textarea') === 0) {
        Ext.get(target).setStyle('backgroundColor', '');
    }

    // hide first so that alerts will display properly
    if (dadHasValue(dadGlobal.folderTooltip)) {
        dadGlobal.folderTooltip.hide();
    }

    // TODO: move checking in page load
    var userPermission = nlapiGetContext().getPermission('LIST_FILECABINET');
    // user has either view or no access to documents and files
    if (userPermission < dadGlobal.CREATE_PERMISSION) {
        dadGlobal.folderId = null;
        Ext.get('dadRecordDropZoneBackground').setStyle('backgroundColor', 'transparent');
        uiShowWarning('You do not have access to Documents and Files. Contact your account administrator to request for access.', dadGlobal.TITLE, function() {
        });
        return dadStopPropagation(evt);
    }

    if (dadHasNoValue(dadGlobal.folderId) && dadHasNoValue(dadGlobal.folderPathPattern)) {
        // no target folder selected
        uiShowWarning('Please select a target folder.', dadGlobal.TITLE, function() {
            dadChangeTargetFolder(false);
        });
        return dadStopPropagation(evt);
    }

    var currentUploadHash = dadGlobal.currentUploadHash;
    var pendingUploadsCount = Object.keys(currentUploadHash).length;
    if (pendingUploadsCount > 5) {
        // Issue: 258882 [DnD] Prevent further file drops when the number of
        // pending
        Ext.get('dadRecordDropZoneBackground').setStyle('backgroundColor', 'transparent');
        uiShowInfo('You currently have more than 5 simultaneous uploads in progress. Please wait for the current uploads to finish before adding more files.', dadGlobal.TITLE, function() {
            // dadShowFolderMenu();
        });
        return dadStopPropagation(evt);
    }

    if (dadCreateSubFolderIfNeeded() === false) {
        return dadStopPropagation(evt);
    }

    var isFolderInactive = nlapiLookupField('folder', dadGlobal.folderId, 'isinactive');
    if (isFolderInactive === 'T') {
        Ext.get('dadRecordDropZoneBackground').setStyle('backgroundColor', 'transparent');
        uiShowWarning('You are uploading to an inactive folder. Please select a different folder.', dadGlobal.TITLE, function() {
            dadChangeTargetFolder();
        });
        return dadStopPropagation(evt);
    }

    dadGetCurrentFolderNameIfNeeded();
    if (dadGlobal.folderFullName === '') {
        dadGlobal.folderId = null;
        Ext.get('dadRecordDropZoneBackground').setStyle('backgroundColor', 'transparent');
        uiShowWarning('You do not have access to this folder. Please select a different folder.', dadGlobal.TITLE, function() {
            dadChangeTargetFolder();
        });
        return dadStopPropagation(evt);
    }

    dadGlobal.dadIsDragEnter = false;
    Ext.get('dadInfo').hide();
    var lineId = null;
    var header = null;
    var textbox = null;

    if (target.id == 'dadRecordDropZone') {
        header = target;
        Ext.get('dadRecordDropZoneBackground').setStyle('backgroundColor', 'transparent');
    } else if (dadIsInput(target) === true) {
        // text boxes
        Ext.get(target).setStyle('backgroundColor', 'transparent');
        textbox = target;
    } else {
        // line item, now get the row of the line item
        var outerHTML = target.outerHTML || '';
        if (outerHTML.indexOf('<tr') == -1) {
            target = Ext.get(target).findParent('tr');
        }
        // logger.log('target.class='+Ext.get(target).select();
        // get line index from the id
        logger.log('target.id=' + target.id);
        var pos = target.id.indexOf('_row_') + 5;
        var lineNum = parseInt(target.id.substr(pos), 10);
        logger.log('lineNum=' + lineNum);

        // get sublits id
        pos = target.id.indexOf('_row_');
        var subListId = target.id.substr(0, pos);
        logger.log('subListId=' + subListId);

        lineId = nlapiGetLineItemValue(subListId, 'id', lineNum);
        logger.log('lineId=' + lineId);
        if (dadHasNoValue(lineId)) {
            uiShowWarning('Line item should be saved first before any file can be attached to it.');
            return;
        }
        Ext.get(target).select('td').setStyle('backgroundColor', 'transparent');
    }
    // logger.log('evt.target='+evt.target.innerHTML);
    // logger.log("evt.dataTransfer.getData('Text')="+evt.dataTransfer.getData('Text'));
    // logger.log("evt.dataTransfer.getData('html')="+evt.dataTransfer.getData('html'));
    // logger.log("evt.dataTransfer.getData('Text')="+JSON.stringify(evt.dataTransfer));

    var files = evt.dataTransfer.files;
    var count = files.length;
    if (count === 0) {
        logger.log('no files');
        return dadStopPropagation(evt);
    }

    // check if there are existing files
    var fileNames = [];
    for (var i = 0; i < count; i++) {
        var file = files[i];
        logger.log('file=' + JSON.stringify(file));
        logger.log('file.name=' + file.name);
        fileNames.push(file.name);

        // check file size, do not allow more than 5mb
        var MAXIMUM_IMAGE_SIZE = 5242880; // 5 * 1000 * 1000;
        if (file.size > MAXIMUM_IMAGE_SIZE) {
            Ext.MessageBox.show({
                title : dadGlobal.TITLE,
                msg : 'The file ' + file.name + ' exceeds the maximum file size of 5MB.',
                buttons : Ext.MessageBox.OK,
                icon : Ext.MessageBox.WARNING
            });
            return dadStopPropagation(evt);
        }
    }

    var param = {};
    param.scriptId = nlapiGetRecordType();
    param.fileNames = fileNames;
    param.folderId = dadGlobal.folderId;
    logger.log('fileNames=' + JSON.stringify(fileNames));
    dadSetStatus('Checking existing files...');
    dadSuiteletProcessAsync('getExistingFiles', param, function(existingFileNames) {
        logger.log('existingFileNames=' + JSON.stringify(existingFileNames));
        if (existingFileNames.length === 0) {
            // no existing files
            jDnd('#dadRecordDropZoneBackground').effect('highlight', {}, 3000);
            dadUploadNow();
            return dadStopPropagation(evt);
        }
        // there are existing files
        Ext.get('dadInfo').update('Existing files found.');
        var fileNamesStr = '';
        for (var f = 0; f < existingFileNames.length; f++) {
            fileNamesStr = fileNamesStr + '<br>' + existingFileNames[f];
        }
        var question = existingFileNames.length > 1 ? 'The following existing files will be overwritten: ' : 'This existing file will be overwritten: ';
        question = question + fileNamesStr + '.<br><br>Continue?';
        Ext.MessageBox.confirm(dadGlobal.TITLE, question, function(btn) {
            if (btn == 'no') {
                Ext.get('dadInfo').hide();
                dadResetStatus();
                uiShowInfo('No files were copied.');
                return dadStopPropagation(evt);
            }
            jDnd('#dadRecordDropZoneBackground').effect('highlight', {}, 3000);
            dadUploadNow();
        });
    });

    function dadUploadNow() {

        var logger = new dadobjLogger(arguments, false);
        logger.log('count=' + count);

        dadGlobal.filesBeingUploaded = parseInt(dadGlobal.filesBeingUploaded, 10) + count;
        var status = 'Uploading ' + dadGlobal.filesBeingUploaded + ' file' + (dadGlobal.filesBeingUploaded == 1 ? '' : 's') + '...';
        Ext.get("dadInfo").update(status);
        dadSetStatus(status);

        var xmlRequests = dadGlobal.xmlRequests;
        if (dadHasNoValue(dadGlobal.url)) {
            dadGlobal.url = nlapiResolveURL('SUITELET', 'customscript_dad_user_service_sl', 'customdeploy_dad_user_service_sl');
        }
        var url = dadGlobal.url;
        for (var i = 0; i < count; i++) {
            var file = files[i];
            logger.log('i=' + i);
            dadGlobal.filesBeingUploadedBatch = dadGlobal.filesBeingUploadedBatch + 1;
            dadGlobal.batchSize = dadGlobal.batchSize + file.size;

            // include other info
            var vFD = new FormData();
            vFD.append('file', file);
            vFD.append('recordtype', nlapiGetRecordType());
            vFD.append('recordid', nlapiGetRecordId());
            vFD.append('folderId', dadGlobal.folderId);

            if (header === null && textbox === null) {
                vFD.append('subListId', subListId);
                vFD.append('lineid', lineId);
            } else {
                vFD.append('subListId', '');
                vFD.append('lineid', '');
                if (dadHasValue(textbox)) {
                    vFD.append('textboxid', textbox.getAttribute('id'));
                }
            }

            xmlRequests.push(new XMLHttpRequest());
            // oXHR.upload.addEventListener('progress', uploadProgress, false);
            // oXHR.addEventListener('load', uploadFinish, false);
            // oXHR.addEventListener('error', uploadError, false);
            // oXHR.addEventListener('abort', uploadAbort, false);

            var uploadId = dadGlobal.uploadId;
            logger.log('uploadId=' + uploadId);
            logger.log('xmlRequests.length=' + xmlRequests.length);

            xmlRequests[uploadId].uploadId = uploadId;
            xmlRequests[uploadId].fileName = file.name;
            xmlRequests[uploadId].open('POST', url, true /* async */);
            xmlRequests[uploadId].onreadystatechange = function() {

                logger.log('this.readyState=' + this.readyState);
                logger.log('this.status=' + this.status);
                // document.getElementById("dadInfo").innerHTML =
                // 'Uploading...';
                if (this.readyState == 4 && this.status != 200) {
                    // if upload is cancelled or other conditions
                    dadGlobal.filesBeingUploaded = parseInt(dadGlobal.filesBeingUploaded, 10) - 1;
                    logger.log('ERROR: this.status=' + this.status);
                }

                if (this.readyState == 4 && this.status == 200) {
                    // upload completed
                    var returnValue = dadHandleResponse(this);
                    logger.log('returnValue=' + returnValue);
                    logger.log('returnValue=' + JSON.stringify(returnValue));
                    logger.log('this.uploadId=' + this.uploadId);
                    // alert('this.uploadId=' + this.uploadId);
                    delete dadGlobal.currentUploadHash[this.uploadId];

                    var dadFile = returnValue;
                    // check error
                    if (dadHasValue(dadFile.error)) {
                        dadGlobal.filesBeingUploaded = parseInt(dadGlobal.filesBeingUploaded, 10) - 1;
                        uiShowWarning(dadFile.error);
                        return;
                    }

                    var recordId = nlapiGetRecordId();
                    if (dadHasNoValue(recordId)) {
                        var dadFileIds = nlapiGetFieldValue('custpage_dad_file_ids');
                        nlapiSetFieldValue('custpage_dad_file_ids', dadFileIds.concat(',' + dadFile.fileId));
                    }

                    if (dadHasValue(dadFile.textboxid)) {
                        // append url to textbox
                        target.value = target.value + ' ' + dadFile.fileName + ' https://' + window.location.hostname + nlapiResolveURL('mediaitem', dadFile.fileId);
                        NS.form.setChanged(true);
                    }

                    // addOrUpdateOrDeleteDadFile
                    dadSuiteletProcessAsync('addOrUpdateOrDeleteDadFile', returnValue, function(data) {
                        dadSetStatus(xmlRequests[uploadId].upload.fileName + ' upload completed.');
                    })

                    dadGlobal.filesBeingUploaded = parseInt(dadGlobal.filesBeingUploaded, 10) - 1;
                }
            };

            // progress bar
            xmlRequests[uploadId].upload.fileName = file.name;
            xmlRequests[uploadId].upload.uploadId = uploadId;

            xmlRequests[uploadId].upload.addEventListener("progress", function(e) {
                dadGlobal.currentUploadHash[this.uploadId].loaded = e.loaded;
            }, false);

            dadGlobal.currentUploadHash[uploadId] = {};
            dadGlobal.currentUploadHash[uploadId].fileName = file.name;
            dadGlobal.currentUploadHash[uploadId].fileSize = file.size;
            dadGlobal.currentUploadHash[uploadId].loaded = 0;
            dadGlobal.currentUploadHash[uploadId].startGetTime = null; // (new
            // Date()).getTime();
            dadGlobal.currentUploadHash[uploadId].uploadId = uploadId;
            xmlRequests[uploadId].send(vFD);
            if (dadHasNoValue(dadGlobal.dadShowUploadProgress)) {
                dadGlobal.dadShowUploadProgress = setInterval('dadShowUploadProgress();', 1000);
            }

            dadGlobal.uploadId = uploadId + 1;

        }
        // show detailed progress
        // dadShowFolderMenu();
    }
    return dadStopPropagation(evt);
}

// holds the current uploads where each active upload is one property. The key
// is the upload id.
dadGlobal.currentUploadHash = {};
function dadShowUploadProgress() {
    var logger = new dadobjLogger(arguments, true);
    if (dadGlobal.dadIsDragEnter) {
        logger.log('dadGlobal.dadIsDragEnter=' + dadGlobal.dadIsDragEnter);
        return;
    }

    var currentUploadHash = dadGlobal.currentUploadHash;
    var pendingUploadsCount = Object.keys(currentUploadHash).length;
    if (pendingUploadsCount > 1) {
        dadGlobal.isMultipleFiles = true;
    }
    if (pendingUploadsCount === 0) {
        dadGlobal.batchSize = 0;
        // no more uploads
        if (Ext.get('dadProgressBars')) {
            Ext.get('dadProgressBars').update('None');
        }

        // Ext.get('dadRecordDropZoneBackgroundBar').hide();
        clearInterval(dadGlobal.dadShowUploadProgress);
        delete dadGlobal.dadShowUploadProgress;
        if (dadGlobal.folderTooltip) {
            dadGlobal.folderTooltip.syncSize();
        }
        // automatically refresh after all the files have been uploaded, in
        // order to see the updated list of files for this record.
        if (dadHasValue(Ext.get('mediaitemtxt')))
            dadTriggerEvent('mediaitemtxt', 'click');

        if (dadGlobal.isMultipleFiles) {
            setTimeout('dadSetStatus(\'Your files have been uploaded successfully.\');', 100);
            dadGlobal.isMultipleFiles = false;
        }
        setTimeout('dadResetStatus();', 10000);
        return;
    }

    // there are still pending uploads
    var totalBytesSize = 0;
    var totalBytesLoaded = 0;
    var totalBytesRemain = 0;
    var totalSecondsElapsed = 0;
    var currentGetTime = (new Date()).getTime();
    var secondsRemainingLongest = 0;
    var largestFileSizeInQueue = 0;
    var fileCountInQueue = 0;
    for ( var p in currentUploadHash) {
        var o = currentUploadHash[p];
        // logger.log('o.fileSize=' + o.fileSize);
        totalBytesSize = totalBytesSize + o.fileSize;
        logger.log('o.loaded=' + o.loaded);
        if (o.loaded === 0) {
            // do not include yet in computation of seconds remaining since it
            // will result in infinity seconds
            if (o.fileSize > largestFileSizeInQueue) {
                largestFileSizeInQueue = o.fileSize;
            }
            fileCountInQueue = fileCountInQueue + 1;
            totalBytesRemain = totalBytesRemain + o.fileSize;
            continue;
        }
        totalBytesRemain = totalBytesRemain + (o.fileSize - o.loaded);
        if (dadHasNoValue(o.startGetTime)) {
            o.startGetTime = (new Date()).getTime();
        }

        var secondsElapsed = (currentGetTime - o.startGetTime) / 1000;
        logger.log('secondsElapsed=' + secondsElapsed);
        var bytesPerSecond = o.loaded / secondsElapsed;
        logger.log('bytesPerSecond=' + bytesPerSecond);
        logger.log('o.fileSize - o.loaded=' + (o.fileSize - o.loaded));
        var secondsRemaining = (o.fileSize - o.loaded) / bytesPerSecond;
        logger.log('secondsRemaining=' + secondsRemaining);
        if (secondsRemaining < 0) {
            secondsRemaining = 0;
        }
        if (secondsRemaining > secondsRemainingLongest) {
            secondsRemainingLongest = secondsRemaining;
        }
        totalSecondsElapsed = totalSecondsElapsed + secondsElapsed;
        totalBytesLoaded = totalBytesLoaded + o.loaded;
        totalBytesSize = totalBytesSize + o.fileSize;
    }

    var bytesPerSecond = totalBytesLoaded / totalSecondsElapsed;
    // add the longest seconds remaining and the estimated time for the largest
    // file in queue
    var totalSecondsRemaining = secondsRemainingLongest;
    dadGlobal.MAXIMUM_PARALLEL_UPLOADS = 6;
    if (bytesPerSecond > 0) {
        var batchCountInQueue = fileCountInQueue / dadGlobal.MAXIMUM_PARALLEL_UPLOADS;
        batchCountInQueue = parseInt(batchCountInQueue, 10);
        if (fileCountInQueue % dadGlobal.MAXIMUM_PARALLEL_UPLOADS > 0) {
            batchCountInQueue = batchCountInQueue + 1;
        }
        var secondsInQueue = batchCountInQueue * largestFileSizeInQueue / bytesPerSecond;
        totalSecondsRemaining = totalSecondsRemaining + secondsInQueue;
    }

    var completed = parseInt(100 * (dadGlobal.batchSize - totalBytesRemain) / dadGlobal.batchSize, 10);
    // var completed = parseInt(100 * (dadGlobal.filesBeingUploadedBatch -
    // pendingUploadsCount) / dadGlobal.filesBeingUploadedBatch);
    if (completed > 100) {
        completed = 100;
    }
    var status = 'Uploading ' + pendingUploadsCount + ' file' + (pendingUploadsCount == 1 ? '' : 's');
    status = status + ' &middot; ' + completed + '%';
    var totalSecondsRemainingRounded = Math.round(totalSecondsRemaining);
    if (totalSecondsRemainingRounded > 1) {
        status = status + ' &middot; ' + totalSecondsRemainingRounded + ' seconds remaining'
    } else if (totalSecondsRemainingRounded == 1) {
        status = status + ' &middot; ' + totalSecondsRemainingRounded + ' second remaining'
    } else {
        if (totalSecondsRemaining > 0 && completed > 0) {
            status = status + ' &middot; less than 1 second remaining';
        }
    }

    var fullWidth = Ext.get('dadRecordDropZone').getWidth() - 30;

    status = status.replace(/ /g, '&nbsp;');
    Ext.get('dadRecordDropZoneBackgroundStatus').update(status);
    var width = fullWidth * completed / 100;
    if (width > fullWidth) {
        width = fullWidth;
    }
    var xBar = Ext.get('dadRecordDropZoneBackgroundBar');
    xBar.show();
    xBar.dom.style.width = width;

    // ===================================================
    // each file
    // ===================================================
    var dadProgressBars = Ext.get('dadProgressBars');
    if (dadProgressBars === null) {
        // tooltip not displayed so exit
        return;
    }
    dadProgressBars.update('');
    var dadProgressBarsWidth = dadProgressBars.getWidth();
    for ( var p in currentUploadHash) {
        var o = currentUploadHash[p];
        logger.log('o=' + JSON.stringify(o));
        logger.log('o.fileSize=' + o.fileSize);
        totalBytesSize = totalBytesSize + o.fileSize;
        logger.log('o.loaded=' + o.loaded);

        totalBytesLoaded = totalBytesLoaded + o.loaded;
        var secondsElapsed = (currentGetTime - o.startGetTime) / 1000;
        logger.log('secondsElapsed=' + secondsElapsed);
        if (secondsElapsed === 0 || o.loaded === 0) {
            // will result in infinity seconds
            var html = '<div class="dadProgressBarBox"><div class="dadProgressStatus"><div class="dadFileStatusText">' + o.fileName + ' &middot; ' + ' Waiting in queue...</div>' + '<a href=# class="dadCancelLink" onclick="dadAbortUpload(event, ' + o.uploadId + '); return false;">Cancel</a></div></div>';
            dadProgressBars.insertHtml('beforeEnd', html);
            continue;
        }

        var bytesPerSecond = o.loaded / secondsElapsed;
        logger.log('bytesPerSecond=' + bytesPerSecond);
        logger.log('o.fileSize - o.loaded=' + (o.fileSize - o.loaded));
        var secondsRemaining = (o.fileSize - o.loaded) / bytesPerSecond;
        if (secondsRemaining < 0) {
            secondsRemaining = 0;
        }
        secondsRemaining = Math.round(secondsRemaining);

        var completed = Math.round(100 * o.loaded / o.fileSize);
        if (completed > 100) {
            completed = 100;
        }
        var width = Math.round(o.loaded / o.fileSize * dadProgressBarsWidth) + 'px';

        // logger.log('secondsRemaining=' + secondsRemaining);
        // totalSecondsRemaining = totalSecondsRemaining + secondsRemaining;
        status = o.fileName + ' &middot; ' + completed + '%';
        if (secondsRemaining === 0) {
            status = status + ' &middot; less than a second remaining'
        } else {
            status = status + ' &middot; ' + secondsRemaining + ' second' + (secondsRemaining > 1 ? 's' : '') + ' remaining'
        }

        status = status.replace(/ /g, '&nbsp;');
        var progressBar = '<div class="dadProgressBar" style="width: ' + width + '">&nbsp;</div>'
        // Issue: 259152 [DnD] Allow canceling of files in queue
        // this comment just shows the location of the change.
        // the actual change is in CL 600387
        var progressStatus = '<div class="dadProgressStatus"><div class="dadFileStatusText">' + status + '</div><a href=# class="dadCancelLink" onclick="dadAbortUpload(event, ' + o.uploadId + '); return false;">Cancel</a>' + '</div>'

        html = '<div class="dadProgressBarBox clearfix">' + progressBar + progressStatus + '</div>'
        dadProgressBars.insertHtml('beforeEnd', html);
    }
    dadGlobal.folderTooltip.syncSize();
}

function dadAbortUpload(evt, uploadId) {
    dadSetStatus(dadGlobal.xmlRequests[uploadId].upload.fileName + ' upload cancelled.');
    dadGlobal.xmlRequests[uploadId].abort();
    delete dadGlobal.currentUploadHash[uploadId];

    evt = evt || window.event;
    dadStopPropagation(evt);
}

/**
 * Returns true if the passed element is a NS textbox or textarea
 * 
 * @param {Object}
 *        el
 */
function dadIsInput(el) {
    if (typeof el.getAttribute == 'undefined') {
        // this happens in line items
        return false;
    }

    var cls = el.getAttribute('class');
    return [ 'input textarea', 'inputreq textarea', 'input', 'inputreq' ].indexOf(cls) > -1;
}

/**
 * Event handler for other drag-and-drop events. Used for demo purposes.
 * 
 * @param {Object}
 *        evt
 */
function dadNoOperationHandler(evt) {
    return dadStopPropagation(evt);
}

// flag if drag enter is triggered and drag exit is not yet triggered
dadGlobal.dadIsDragEnter = false;
function dadDragEnter(e) {
    var logger = new dadobjLogger(arguments);
    e = e || window.event;
    var target = e.target || e.srcElement;

    dadResetStatus();
    dadGlobal.dadIsDragEnter = true;

    var xDadInfo = Ext.get('dadInfo');

    if (target.id == 'dadRecordDropZone') {
        // record level
        // highlight drop zone
        Ext.get(target.id + 'Background').setStyle('backgroundColor', 'lightgreen');
    } else if (dadIsInput(target) === true) {
        // text boxes
        Ext.get(target).setStyle('backgroundColor', 'lightgreen');
    } else {
        // line item level
        Ext.get('dadInfo').update('Drop your file here to attach it to this line item');
        // find the parent table row
        // for some reasons, calling Ext.get() on a literal returns null, so get
        // parent if needed
        var notNullTarget = Ext.get(target) || Ext.get(target.parentNode);
        var xTr = (notNullTarget).findParent('tr', 50, true);
        xTr.select('td').setStyle('backgroundColor', 'lightgreen');

        logger.log('xTr.dom.id=' + xTr.dom.id);

        // get parent table
        var xTable = xTr.findParent('.listtable', 50, true);
        logger.log('xTable.dom.id=' + xTable.dom.id);
        var listId = xTable.dom.id.replace('_splits', '');
        logger.log('listId=' + listId);
        var lineNum = xTr.dom.id.replace(listId + '_row_', '');
        lineNum = parseInt(lineNum, 10);
        if (nlapiGetLineItemValue(listId, 'id', lineNum) == '') {
            xTr.select('td').setStyle('backgroundColor', 'pink');
            Ext.get('dadInfo').update('The line item needs to be saved before you can attach file.');
        }
        // check if new, if yes, do not allow drop
        xDadInfo.setTop(xTr.getTop() - Ext.get('dadInfo').getHeight());
        xDadInfo.setLeft(e.clientX + 30);
        Ext.get('dadInfo').show();
    }

    return dadStopPropagation(e);
}

function dadDragExit(e) {
    var logger = new dadobjLogger(arguments);
    e = e || window.event;

    dadGlobal.dadIsDragEnter = false;

    var target = e.target || e.srcElement;
    if (target.id == 'dadRecordDropZone') {
        Ext.get(target.id + 'Background').setStyle('backgroundColor', 'transparent');
    } else if (dadIsInput(target) === true) {
        // text boxes
        Ext.get(target).setStyle('backgroundColor', '');
    } else {
        if ((target.outerHTML || '').indexOf('<tr') == -1) {
            if (Ext.get(target) === null) {
                // target might be a text content
                return;
            }
            target = Ext.get(target).findParent('tr');
        }
        Ext.get(target).select('td').setStyle('backgroundColor', 'transparent');
    }
    Ext.get('dadInfo').hide();
    logger.log('end');

    return dadStopPropagation(e);
}

/**
 * Get the list of editable sub list id
 */
function dadGetEditableSublists() {
    var logger = new dadobjLogger(arguments, true);
    var editableSubLists = [];
    if (dadHasNoValue(nlapiGetFieldValue('custpage_dad_sublists'))) {
        return editableSubLists;
    }
    var subListIds = JSON.parse(nlapiGetFieldValue('custpage_dad_sublists'));
    for (var i = 0; i < subListIds.length; i++) {
        var splitId = subListIds[i] + '_splits';
        // look for the listtextnonedit which can only be found on editable
        // sublists
        if (dadHasNoValue(Ext.get(splitId)))
            continue;

        if (Ext.get(splitId).select('.listtextnonedit').elements.length > 0 && (Ext.isChrome || Ext.isGecko)) {
            // editable
            editableSubLists.push(subListIds[i]);
            Ext.get('tbl_custpage_dad_button_' + splitId.replace('_splits', '')).dom.style.display = 'block';
        } else {
            // hide Show Files from Selected Item button for non-editable sub
            // lists
            Ext.get('tbl_custpage_dad_button_' + splitId.replace('_splits', '')).dom.style.display = 'none';
        }
    }
    logger.log('end');
    return editableSubLists;
}

/**
 * attach events to line items
 */
function dadSetDragAndDropEventsForLineItems() {
    var logger = new dadobjLogger(arguments, true);
    for (var s = 0; s < dadGetEditableSublists().length; s++) {
        var elId = dadGetEditableSublists()[s] + '_splits';
        var els = Ext.select('#' + elId + ' > tbody > tr').elements;
        logger.log('els.length=' + els.length);
        for (var i = 0; i < els.length; i++) {
            var id = els[i].id;
            if (id.indexOf('_row_') == -1) {
                continue;
            }
            dadAddDnDEvents(els[i]);
        }
    }
}

/**
 * Add drag and drop events to an element
 * 
 * @param {Object}
 *        el
 */
function dadAddDnDEvents(el) {
    dadAddEvent(el, "drop", dadFileDrop);
    dadAddEvent(el, "dragenter", dadDragEnter);
    dadAddEvent(el, "dragover", dadNoOperationHandler);
    if (Ext.isGecko) {
        // FF
        dadAddEvent(el, "dragexit", dadDragExit);
    } else {
        // chrome, safari
        dadAddEvent(el, "dragleave", dadDragExit);
    }
}

/**
 * Needed for: I want to insert the complete link of the file being uploaded
 * unto an editable text field of a form, that way the link will append at the
 * end of any existing text of that field.
 */
function dadSetDragAndDropEventsForTextboxes() {
    var logger = new dadobjLogger(arguments);
    // select all optional NS input single line text boxes
    var selector = 'input[type=text][class=input]';
    var els = jDnd(selector);
    // logger.log('els.length=' + els.length);
    for (var i = 0; i < els.length; i++) {
        dadAddDnDEvents(els[i]);
    }
    // select all required NS input single line text boxes
    var selector = 'input[type=text][class=inputreq]';
    var els = jDnd(selector);
    // logger.log('els.length=' + els.length);
    for (var i = 0; i < els.length; i++) {
        dadAddDnDEvents(els[i]);
    }

    // select all NS input text area (multi-line)
    var selector = 'textarea';
    var els = jDnd(selector);
    // logger.log('els.length=' + els.length);
    for (var i = 0; i < els.length; i++) {
        dadAddDnDEvents(els[i]);
    }
}

function dadHideNonEditSublist() {
    var logger = new dadobjLogger(arguments, true);
    var subListIds = JSON.parse(nlapiGetFieldValue('custpage_dad_sublists'));
    for (var i = 0; i < subListIds.length; i++) {
        var splitId = subListIds[i] + '_splits';
        // look for the listtextnonedit which can only be found on editable
        // sublists
        if (Ext.get(splitId).select('.listtextnonedit').elements.length === 0) {
            // not editable
            Ext.get('tbl_custpage_dad_button_' + subListIds[i]).dom.style.display = 'none';
        } else {
            Ext.get('tbl_custpage_dad_button_' + subListIds[i]).dom.style.display = 'block';
        }
    }
    logger.log('end');
}

function dadPositionRecordDropZone() {
    var logger = new dadobjLogger(arguments, true);

    var dInsertBefore = Ext.select('.pt_end').elements[0]; // Ext.select('.pt_statusblock').elements[0]
    // ||
    var xInsertBefore = Ext.get(dInsertBefore);
    if (xInsertBefore === null) {
        logger.error('xInsertBefore ===  null');
    }
    var xRecordDropZoneBackground = Ext.get('dadRecordDropZoneBackground');
    var xRecordDropZone = Ext.get('dadRecordDropZone');
    var width = Ext.get('dadRecordDropZoneBackgroundStatus').getWidth() + 30;
    xRecordDropZone.setWidth(width);
    xRecordDropZoneBackground.setWidth(width);
    // xRecordDropZone.setHeight(xInsertBefore.getHeight());
    xRecordDropZone.setTop(xInsertBefore.getTop() + xInsertBefore.getHeight() + 2);
    xRecordDropZone.setLeft(xInsertBefore.getLeft() - xRecordDropZone.getWidth() + Ext.get(dInsertBefore).getWidth());
    xRecordDropZone.show();

    xRecordDropZoneBackground.setHeight(xRecordDropZone.getHeight());
    xRecordDropZoneBackground.setTop(xRecordDropZone.getTop());
    xRecordDropZoneBackground.setLeft(xRecordDropZone.getLeft());
    xRecordDropZoneBackground.show();
}

/**
 * Displays the folder picker and selects the current folder
 */
function dadChangeTargetFolder(isFolderSelected) {

    if (dadGlobal.folderTooltip) {
        dadGlobal.folderTooltip.hide();
    }

    dadShowFileCabinetFolderPicker(isFolderSelected);
}

function dadShowFolderMenu() {
    dadWait();
    setTimeout(dadShowFolderMenuNoDelay, 50);
}

function dadShowFolderMenuNoDelay() {
    Ext.Msg.hide();
    var logger = new dadobjLogger(arguments);
    if (dadGlobal.browserSupported === false) {
        return;
    }

    var userPermission = nlapiGetContext().getPermission('LIST_FILECABINET');
    // user has either view or no access to documents and files
    if (userPermission < dadGlobal.CREATE_PERMISSION) {
        uiShowWarning('You do not have access to Documents and Files. Contact your account administrator to request for access.', dadGlobal.TITLE, function() {
        });
        return false;
    }

    if (dadGlobal.folderTooltip) {
        // dadGlobal.folderTooltip.show();
        return;
    }

    if (dadHasNoValue(dadGlobal.folderId) && dadHasNoValue(dadGlobal.folderPathPattern)) {
        uiShowWarning('Please select a target folder.', dadGlobal.TITLE, function() {
            dadChangeTargetFolder(false);
        });
        return;
    }
    if (dadCreateSubFolderIfNeeded() === false) {
        return;
    }

    var isFolderInactive = nlapiLookupField('folder', dadGlobal.folderId, 'isinactive');
    if (isFolderInactive === 'T') {
        Ext.get('dadRecordDropZoneBackground').setStyle('backgroundColor', 'transparent');
        uiShowWarning('You are uploading to an inactive folder. Please select a different folder.', dadGlobal.TITLE, function() {
            dadChangeTargetFolder(false);
        });
        return false;
    }
    dadGetCurrentFolderNameIfNeeded();
    if (dadGlobal.folderFullName === '') {
        dadGlobal.folderId = null;
        Ext.get('dadRecordDropZoneBackground').setStyle('backgroundColor', 'transparent');
        uiShowWarning('You do not have access to this folder. Please select a different folder.', dadGlobal.TITLE, function() {
            dadChangeTargetFolder(false);
        });
        return false;
    }
    // ========================================
    // show destination folder
    // ========================================
    var html = '';
    html = html + 'You can upload up to 5 files simultaneously. Files can also be dropped into an editable sublist.';
    html = html + '<br><br>';
    // html = html + 'Note: The File Drag & Drop feature is only available in
    // edit mode for both record and line items. The maximum file size is 5mb.';
    // html = html + '<br /><br />';
    html = html + 'Destination folder:<br /><span style="color: black">' + dadGlobal.folderFullName + '</span>';
    html = html + '<br />';
    html = html + '<a href="#" class=dottedlink onclick="dadChangeTargetFolder(); return false;" title="Click to select a different destination folder for this instance. This will be reverted to the default folder when the page is refreshed.">Change</a>';
    html = html + ' &middot; ';
    html = html + '<a target=_blank onclick="dadGlobal.folderTooltip.hide(); return true;" href="/app/common/media/mediaitemfolders.nl?folder=' + dadGlobal.folderId + '" class=dottedlink title="Click to open this folder in a new tab." >Open in new tab...</a>';

    // ========================================
    // pending uploads
    // ========================================
    html = html + '<br /><br />';
    html = html + 'Pending uploads:';
    html = html + '<div id="dadProgressBars" style="border: 0px dotted red">';
    html = html + 'None';
    html = html + '</div>';

    var sendCommentsLinkMarkup = "<a id='dadSendCommentsLink' href='#' onclick='dadSendComments(); return false;' title='Rate File Drag & Drop.' class='dottedlink'>Let us know what you think</a>";
    var tutorialLink = "<a id='dadHelpLink' href='#' onclick='dadHelp(); return false;' title='Click here to launch the tutorial.' class='dottedlink'>Tutorial</a>";
    var helpLink = "<a href='/help/helpcenter/en_US/Output/Help/NetSuiteBasics/AddingFilesRecordsUsingFileDragDropBeta.html' target='_blank' title='Click here to launch the help.' class='dottedlink'>Help</a>";
    html += '<br>';
    html += sendCommentsLinkMarkup;
    html = html + ' &middot; ';
    html += tutorialLink;
    html = html + ' &middot; ';
    html += helpLink;

    var target = 'dadRecordDropZone';
    var tooltipId = target + 'Tooltip';
    var tooltip = (new Ext.ToolTip({
        title : '',
        id : tooltipId,
        target : target,
        anchor : 'top',
        html : html,
        autoHide : false,
        closable : true,
        autoShow : true,
        hideDelay : 10000,
        dismissDelay : 0,
        anchorOffset : 0,
        width : 400,
        height : 'auto',
        bodyStyle : 'padding: 10px; background-color: white',
        listeners : {

            'hide' : function() {
                this.destroy();
                delete dadGlobal.folderTooltip;
            },
            'renderX' : function() {

                this.header.on('click', function(e) {
                    e.stopEvent();
                    Ext.Msg.alert('Link', 'Link to something interesting.');

                }, this, {
                    delegate : 'a'
                });
            }
        }
    }));
    dadGlobal.folderTooltip = tooltip;
    tooltip.show();

    // show individual file progress if needed
    var currentUploadHash = dadGlobal.currentUploadHash;
    var pendingUploadsCount = Object.keys(currentUploadHash).length;
    if (pendingUploadsCount > 0) {
        dadShowUploadProgress();
    }

    return false;
}

function dadGetCurrentFolderNameIfNeeded() {
    // check first if the folder name has been obtained
    if (dadGlobal.folderFullName === '') {
        dadWait('Getting folder details...');
        if (dadCreateSubFolderIfNeeded() === false) {
            return false;
        }
        dadGlobal.folderFullName = dadGetFolderFullName(dadGlobal.folderId);
        Ext.Msg.hide();
    }
    return true;
}

function dadResetStatus() {
    var logger = new dadobjLogger(arguments);
    var status = 'Drop files here <b>(Click for options.)</b>';
    status = status.replace(/ /g, '&nbsp;');
    Ext.get('dadRecordDropZoneBackgroundBar').hide();
    if (Ext.get('dadRecordDropZoneBackgroundStatus').dom.innerHTML != status) {
        Ext.get('dadRecordDropZoneBackgroundStatus').update(status);
    }
    // S3 - Issue 262998 : [DnD] I want to close the drag & drop balloon after
    // all files in queue have been uploaded, together with the confirmation
    // message appearing after upload completion.
    if (dadHasValue(dadGlobal.folderTooltip)) {
        dadGlobal.folderTooltip.hide();
    }
    logger.end();
}

/**
 * Attaches an event to an element
 * 
 * @param {Object}
 *        elem Element
 * @param {Object}
 *        evnt Event
 * @param {Object}
 *        func Function
 */
function dadAddEvent(elem, evnt, func) {
    if (elem.addEventListener) // W3C DOM
        elem.addEventListener(evnt, func, false);
    else if (elem.attachEvent) { // IE DOM
        elem.attachEvent("on" + evnt, func);
    } else { // No much to do
        elem[evnt] = func;
    }
}

function dadCreateSubFolderIfNeeded() {
    var logger = new dadobjLogger(arguments);
    if (dadHasValue(dadGlobal.folderId)) {
        return true;
    }
    // if (dadHasNoValue(dadGlobal.recordTypeSettingFolderId)) {
    // throw 'dadHasNoValue(dadGlobal.recordTypeSettingFolderId))';
    // }

    if (dadHasNoValue(dadGlobal.folderPathPattern)) {
        throw 'dadHasNoValue(dadGlobal.folderPathPattern)';
    }

    dadWait('Getting target folder...');
    // check if subfolder already exists, if not create
    // var parentFolderId = dadGlobal.recordTypeSettingFolderId;
    var actualFolderPath = dadGetActualFolderPath(dadGlobal.folderPathPattern);
    var sw = new tcobjStopWatch();
    logger.log('====================================================================');
    var jsonFolderPath = dadCreateFolderPath(actualFolderPath);
    if (jsonFolderPath === null) {
        return false;
    }
    logger.log('DURATION CREATE FOLDER: ' + sw.measure());
    logger.end();
    dadGlobal.folderId = jsonFolderPath.passedFolderId;
    return true;

}

dadGlobal.folderFullName = '';
function dadPageInit() {
    var logger = new dadobjLogger(arguments);
    var recordTypeScriptId = nlapiGetRecordType();
    if (recordTypeScriptId === null) {
        logger.log('recordTypeScriptId === null');
        return;
    }

    // check browser
    if (Ext.isChrome || Ext.isGecko) {
        // browser is supported
        // get settings for this record type

        var userPermission = nlapiGetContext().getPermission('LIST_FILECABINET');
        dadSuiteletProcessAsync('getRecordTypeSettings', recordTypeScriptId, function(recordTypeSetting) {
            dadSuiteletProcessAsyncUser('getUserPreference', 'value', function(paramUserPref) {
                dadGlobal.paramUserPref = paramUserPref;

                logger.log('recordTypeSetting=' + JSON.stringify(recordTypeSetting));
                if (JSON.stringify(recordTypeSetting) == '{}') {
                    // no record type level setting, get account level folder
                    var folderId = nlapiGetContext().getPreference('custscript_dad_default_dnd_folder_destin');
                    dadGlobal.folderId = folderId;
                } else {

                    if (dadHasValue(recordTypeSetting.folderId)) {
                        dadGlobal.folderId = recordTypeSetting.folderId;
                    } else {
                        dadGlobal.folderPathPattern = recordTypeSetting.folderPathPattern;
                    }
                    // dadGlobal.recordTypeSettingFolderId =
                    // recordTypeSetting.folderId;
                    // dadGlobal.folderId = recordTypeSetting.folderId;
                    // see if we use a unique folder for each corresponding
                    // record
                    // ID
                    // if (recordTypeSetting.isCreateUniqueFolderForEachRecord
                    // ===
                    // false) {
                    // dadGlobal.folderId = recordTypeSetting.folderId;
                    // }
                }

                // get folder full name
                // dadSuiteletProcessAsyncUser('getFolderFullName',
                // dadGlobal.folderId, function(folderFullName) {
                // dadGlobal.folderFullName = folderFullName;
                dadResetStatus();
                dadPositionRecordDropZone();

                Ext.get('dadImgFolderInfo').show();

                // set fixed width
                var x = Ext.get('dadRecordDropZoneBackgroundStatus');
                x.setWidth(x.getWidth());

                dadWhatsNewHelp();

                if (userPermission < dadGlobal.CREATE_PERMISSION) {
                    dadGlobal.userPermission = false;
                    dadSetStatus('You do not have access to Documents and Files.');
                }
                // });
                // END of get folder full name
            });
        });

        // user has either view or no access to documents and files
        if (userPermission >= dadGlobal.CREATE_PERMISSION) {
            var dRecordDropZone = Ext.get('dadRecordDropZone').dom;
            dadAddDnDEvents(dRecordDropZone);

            dadSetDragAndDropEventsForTextboxes();
            // for line items, we need to regularly attach the events since the
            // line
            // items are re-created
            // when selected line item changes or when a new line item is being
            // added
            dadSetDragAndDropEventsForLineItems();
            setInterval('dadSetDragAndDropEventsForLineItems();', 3000);

            // Issue: 259062 [DnD] Prompt a warning when leaving page while
            // files
            // are being uploaded.
            dadAddBeforeUnloadEventHandler();
        }
    } else {
        dadGlobal.browserSupported = false;
        dadSetStatus('Use the latest version of Chrome or Firefox to enable file drag and drop.');
        dadPositionRecordDropZone();
        dadPositionRecordDropZone();
        Ext.get('dadImgFolderInfo').show();
        Ext.get('dadRecordDropZone').dom.title = '';

        // In IE, hide after 3 seconds
        if (Ext.isIE) {
            setTimeout(function() {
                Ext.select('#dadRecordDropZoneBackground, #dadRecordDropZone, #dadImgFolderInfo').fadeOut();
            }, 3000);
        }
    }

    jDnd(window).resize(function() {
        dadPositionRecordDropZone();
    });
    jDnd(Ext.getBody()).resize(function() {
        dadPositionRecordDropZone();
    });

    // ===========================================================================================
    // re-position the drop zone on don changed like when the timeline is
    // collapsed or expanded
    // ===========================================================================================
    // this is to make sure the drop zone is in position after the page loads
    // since it is possible that
    // other scripts like the transaction timeline will insert html at the
    // elements before the drop zone
    dadGlobal.isDOMSubtreeModified = false;
    jDnd(document).bind('DOMSubtreeModified', function() {
        dadGlobal.isDOMSubtreeModified = true;
    });

    jDnd('body').click(function(e) {
        if (dadHasValue(dadGlobal.folderTooltip) && (e.target.id !== 'dadRecordDropZone')) {
            dadGlobal.folderTooltip.hide();
        }
    });

    // then check every 3 seconds if the DOMSubtreeModified was modified
    setInterval(function() {
        // var logger = new dadobjLogger('Check
        // dadGlobal.isDOMSubtreeModified');
        if (dadGlobal.isDOMSubtreeModified) {
            dadPositionRecordDropZone();
            dadGlobal.isDOMSubtreeModified = false;
        }
    }, 3000);
    // end
    // ============================================================================================
}

/**
 * Adds trim functions if browser has no support
 */
function dadAddTrimFunctions() {
    if (typeof String.trim == 'undefined') {
        String.prototype.trim = function() {
            return this.replace(/^\s+|\s+$/g, "");
        };
    }

    if (typeof String.ltrim == 'undefined') {
        String.prototype.ltrim = function() {
            return this.replace(/^\s+/, "");
        };
    }

    if (typeof String.rtrim == 'undefined') {
        String.prototype.rtrim = function() {
            return this.replace(/\s+$/, "");
        };
    }
}

dadAddTrimFunctions();

/**
 * hides the folder picker
 */
function dadCloseFolderPicker() {
    Ext.get('dadTreeContainer').hide();
    dadGlobal.tree.hide();
}

/**
 * Returns the list of folders the current user has access to. Object keys are
 * id, text, parent, leaf and iconCls Note that right now, only around 2,000
 * folders are supported.
 * 
 * @return {Objects[]}
 */
function dadGetFolders() {
    var logger = new dadobjLogger(arguments);
    // get first the root folders
    // TODO: Handle more than 2,000 folders
    var filters = [];
    filters.push([ 'parent', 'is', '@NONE@' ]);
    filters.push('and');
    filters.push([ 'isinactive', 'is', 'F' ]);
    var columns = [];
    columns.push(new nlobjSearchColumn('name'));
    columns.push(new nlobjSearchColumn('parent'));
    columns[0].setSort();
    var results = nlapiSearchRecord('folder', null, filters, columns);
    var folders = {};
    folders.root = [];
    folders.subfolders = [];
    var rootIds = [];
    for (var i = 0; i < results.length; i++) {
        var result = results[i];
        var folder = {
            id : result.getId(),
            text : result.getValue('name'),
            parent : result.getValue('parent'),
            leaf : true,
            iconCls : 'dadNoIcon'
        };

        rootIds.push(result.getId());
        folders.root.push(folder);
    }

    // Get at most 1000 children for performance reasons
    filters = [];
    filters.push([ 'isinactive', 'is', 'F' ]);

    columns = [];
    columns.push(new nlobjSearchColumn('internalid'));
    columns[0].setSort();
    columns.push(new nlobjSearchColumn('name'));
    columns.push(new nlobjSearchColumn('parent'));
    results = nlapiSearchRecord('folder', null, filters, columns);
    for (var i = 0; i < results.length; i++) {
        var result = results[i];
        folder = {
            id : result.getId(),
            text : result.getValue('name'),
            parent : result.getValue('parent'),
            leaf : true
        };

        folders.subfolders.push(folder);
    }
    logger.end();
    return folders;
}

if (typeof Ext != 'undefined') {
    Ext.onReady(function() {
        try {
            if (dadShouldShowDropZone()) {
                dadPageInit();
            }
        } catch (ex) {
            // comment next line in production
            // throw e;
            dadHandleUnexpectedError(ex);
        }

    });
}

/**
 * Returns true if the record has file subtab and among the supported record
 * types.
 */
function dadShouldShowDropZone() {
    var logger = new dadobjLogger(arguments);
    var show = false;
    var recordType = nlapiGetRecordType();
    var supportedRecordTypes = [ 'downloaditem', 'assemblyitem', 'lotnumberedassemblyitem', 'serializedassemblyitem' ];
    if (supportedRecordTypes.indexOf(recordType) > 0) {
        show = true;
    } else if (dadHasValue(Ext.get('medialnk')) || dadHasValue(Ext.get('mediaitemlnk'))) {
        show = true;
    }

    logger.log('show=' + show);
    return show;
}

/**
 * Returns the actual folder path given the pattern
 * 
 * @param {string}
 *        folderPathPattern. Example:
 *        \FinanceFolder\Subsidiary{subsidiaryId}\Record{recordId}\Role{roleId}
 * @returns {string}. Example:
 *          \FinanceFolder\Subsidiary123\Record{recordId-uniquevalue}\Role456
 */
function dadGetActualFolderPath(folderPathPattern) {
    var userId = nlapiGetUser();
    var subsidiaryId = nlapiGetContext().getSubsidiary();
    var departmentId = nlapiGetContext().getDepartment();
    var recordId = nlapiGetRecordId();
    var roleId = nlapiGetContext().getRole();

    if (dadHasNoValue(recordId)) {
        var dt = new Date();
        var month = dt.getMonth() + 1; // getMonth() returns 0 to 11
        var day = dt.getDate();
        var year = dt.getFullYear();
        var hh = dt.getHours();
        var ss = dt.getSeconds();
        var ms = dt.getMilliseconds();

        recordId = '{temp-' + year + '-' + month + '-' + day + '-' + hh + '-' + ss + '-' + ms + '-' + userId + '}';
    }

    var actualFolderPath = folderPathPattern.replace('{subsidiaryId}', subsidiaryId).replace('{departmentId}', departmentId).replace('{recordId}', recordId).replace('{roleId}', roleId);

    return actualFolderPath;
}

/**
 * Creates the passed folder if it does not exists yet. It also creates the
 * parent folders if they don't exists yet.
 * 
 * @param {string}
 *        folderPath. Example:
 *        \FinanceFolder\Subsidiary123\Record{recordId}\Role456
 * @returns {Object} A JSON object with keys: <br>
 *          passedFolderId: This is the id of the passed folder. This is set
 *          even if the folder already exists. <br>
 *          recordFolderId: This is the id of the folder where the recordId
 *          placeholder was found. No need to set this if the "record" folder
 *          (the folder of recordFolderId) already exists. In the example, this
 *          folder is \FinanceFolder\Subsidiary123\Record{recordId}
 */
function dadCreateFolderPath(folderPath) {
    var logger = new dadobjLogger(arguments);
    // Your implementation will need to check existence of parent folders
    // starting from the root folder (FinanceFolder) and so on.

    var jsonFolderPath = {};
    jsonFolderPath.recordFolderId = null;
    var folders = folderPath.split('\\');
    var tempParentFolder = null;

    for (var i = 0; i < folders.length; i++) {

        if (dadHasNoValue(folders[i])) {
            continue;
        }

        var folderName = folders[i];
        var folderId = dadValidateFolderPath(folderName, tempParentFolder);
        if (dadHasNoValue(folderId)) {
            logger.log("b4 var r = nlapiCreateRecord('folder');");
            var r = nlapiCreateRecord('folder');
            r.setFieldValue('name', folderName);
            r.setFieldValue('parent', tempParentFolder);
            try {
                tempParentFolder = nlapiSubmitRecord(r);
            } catch (e) {
                logger.log("error var r = nlapiCreateRecord('folder');" + dadGetErrorDetails(e));
                Ext.get('dadRecordDropZoneBackground').setStyle('backgroundColor', 'transparent');
                uiShowWarning('You have no access to the destination folder. You should change the destination folder before you can drag and drop files.', dadGlobal.TITLE, function() {
                    dadChangeTargetFolder();
                });
                return null;
            }
            logger.log("after var r = nlapiCreateRecord('folder'); folderName=" + folderName + '; tempParentFolder=' + tempParentFolder);
        } else {
            tempParentFolder = folderId;
        }

        jsonFolderPath.passedFolderId = tempParentFolder;
        dadGlobal.folderId = tempParentFolder;
        if (folderName.indexOf('{temp') > -1) {
            jsonFolderPath.recordFolderId = tempParentFolder;
            nlapiSetFieldValue('custpage_dad_record_folder_id', jsonFolderPath.recordFolderId);
        }
    }

    return jsonFolderPath;
}

/**
 * Validates if folder exists
 * 
 * @param {string}
 *        folderName : This is the name of the folder to be searched
 *        parentFolder: Parent folder id of the searched folder
 * @returns {string} folderId : This is the id of the searched folder
 */
function dadValidateFolderPath(folderName, parentFolder) {
    var logger = new dadobjLogger(arguments);
    var filters = [];
    filters.push([ 'name', 'is', folderName ]);
    filters.push('and');
    if (dadHasValue(parentFolder)) {
        filters.push([ 'parent', 'anyof', parentFolder ]);
    } else {
        filters.push([ 'parent', 'anyof', '@NONE@' ]);
    }
    var results = nlapiSearchRecord('folder', null, filters);
    if (dadHasNoValue(results)) {
        return null;
    }
    logger.end();
    return results[0].getId();
}

/**
 * Displays the what new help tooltip
 */
function dadWhatsNewHelp() {
    var logger = new dadobjLogger(arguments);
    try {
        if (dadGlobal.paramUserPref.suppressTutorial == 'T') {
            return;
        }
        var item = 'overview';
        dadSpotlight(null, item);
    } catch (e) {
        dadHandleError(e);
    }
    logger.end();
}

/**
 * Shows the in-app help
 */
function dadHelp() {
    dadGlobal.folderTooltip.hide();
    var helpkey = 'overview';
    dadSpotlight(null, helpkey);
    return;
}

/**
 * The spotlight effect used in the in-app help
 * 
 * @param {Object}
 *        e
 * @param {Object}
 *        helpKey
 */
function dadSpotlight(e, helpKey) {
    var logger = new dadobjLogger(arguments);
    e = e || window.event;
    if (dadHasNoValue(helpKey)) {
        dadHideHelp();
        return false;
    }
    var title = '';
    var helpText = '';
    var anchor = 'top';
    var nextHelpKey = '';
    switch (helpKey) {
    case 'overview':
        title = 'File Drag & Drop Overview';
        helpText = 'The File Drag & Drop SuiteApp enables you to conveniently upload single or multiple files from your desktop to a record page. It can also be used to attach multiple files directly to editable sublists of supported records and custom records.';
        helpText += '<br>';
        helpText += '<br>';
        helpText += 'The File Drag & Drop feature is only available in create and edit mode for both record and text fields. The maximum file size is 5MB.';
        helpText += '<br>';
        helpText += '<br>';
        helpText += "Click <a href='/help/helpcenter/en_US/Output/Help/NetSuiteBasics/AddingFilesRecordsUsingFileDragDropBeta.html' target='_blank'>here</a> to view the help";
        nextHelpKey = 'dadDropzone';
        break;
    case 'dadDropzone':
        title = 'DropZone';
        helpText = 'You can drop more files here if the current simultaneous uploads does not exceed 5.';
        nextHelpKey = 'dadTextFields';
        break;
    case 'dadTextFields':
        title = 'Text Fields';
        helpText = 'You can drop files to text fields.';
        nextHelpKey = 'dadLineItems';
        break;
    case 'dadLineItems':
        title = 'Line Items';
        helpText = 'You can drop files to line items.';
        nextHelpKey = 'end';
        dadShowLineItemBox();
        break;
    case 'end':
        Ext.Msg.hide();
        title = 'End of Tutorial';
        helpText = 'You can now start using File Drag & Drop.';
        break;
    default:
        break;
    }
    var el = dadGetHelpElementId(helpKey);

    helpText += '<br><br>';
    if (helpKey != 'end') {
        helpText += '<a href="#" class="dottedlink smalltext" onclick="return dadSpotlight(event, \'' + nextHelpKey + '\');">Next topic</a>';
        helpText += ' &middot; ';
    }
    helpText += '<a href="#" class="dottedlink smalltext" onclick="Ext.Msg.hide(); return dadSpotlight(event, \'\');">Close</a>';
    if (helpKey == 'overview') {
        helpText += ' &middot; ';
        helpText += "<a href=# onclick='dadSuppressTutorial(); return false;' class='dottedlink'>Don't show this message again</a>";
    }
    helpText += '<br><span style="height: 5px; line-height: 5px">&nbsp;</span>';
    if (dadHasValue(helpText)) {
        dadHideHelp();
        helpText = '<div id="dadHelptext">' + helpText + '</div>';
        dadSetHelp(el, title, helpText, anchor);
    }

    dadGlobal.spot = dadGetSpot();
    if (Ext.get(el) !== null) {
        if (Ext.get(el).getWidth() === 0) {
            // hidden
        } else {
            dadGlobal.lastHelpEl = el;
            dadGlobal.spot.show(dadGlobal.lastHelpEl);
        }
    }
    if (e) {
        dadStopPropagation(e);
    }
    logger.log('end');
    return false;
}

function dadGetHelpElementId(helpKey) {
    var el;
    switch (helpKey) {
    case 'overview':
        el = 'dadRecordDropZone';
        break;
    case 'dadDropzone':
        el = 'dadRecordDropZone';
        break;
    case 'dadLineItems':
        el = 'dadLineItemBox';
        break;
    case 'dadTextFields':
        // HACK: dom
        var labelElId;
        var selector = '#detail_table_lay [type="text"]';
        if (dadHasValue(Ext.select(selector).elements[1].parentNode.id)) {
            labelElId = Ext.select(selector).elements[1].parentNode.id;
        } else if (dadHasValue(Ext.select(selector).elements[2].parentNode.id)) {
            labelElId = Ext.select(selector).elements[2].parentNode.id;
        }
        return labelElId;
    case 'end':
        el = 'dadRecordDropZone';
        break;
    default:
        return helpKey;
    }
    return el;
}

function dadSuppressTutorial() {
    var logger = new dadobjLogger(arguments);
    dadSpotlight(undefined, '');
    uiShowInfo('To display the File Drag and Drop tutorial again, click Tutorial from the Drop zone');
    dadSuiteletProcessAsyncUser('suppressTutorial', 'any', function(isOK) {
        logger.log('isOK=' + isOK);
        if (isOK != 'ok') {
            uiShowError('error in suppressTutorial');
            return;
        }
    });
}

function dadShowLineItemBox() {
    var results = nlapiSearchRecord('file', null, [ 'name', 'is', 'dad-lineitem.png' ]);
    if (results === null) {
        throw 'image not found. imageFileName=dad-lineitem.png';
    }
    var imageURL = nlapiResolveURL('mediaitem', results[0].getId());

    var msg = '<html><div id="dadLineItemBox"><img width="645px" height="206px" src=' + imageURL + '></html>';
    Ext.Msg.show({
        title : 'Line Item',
        msg : msg,
        minWidth : 675
    });

    Ext.select('.x-window-body').setStyle({
        'height' : '198px'
    });

    Ext.select('.x-window-mc').setStyle({
        'background-color' : 'white'
    });

    Ext.select('.x-shadow').setStyle({
        'height' : '246px'
    });
    return false;
}