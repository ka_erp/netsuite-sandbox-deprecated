function NSPOSEmailInvoices(type)
 {
nlapiLogExecution('DEBUG', "Entered Script", "DG");
 //Get variables
 var emailAddress = nlapiGetFieldValue('custbody_nspos_email_address');
 nlapiLogExecution('DEBUG', "Email Address", emailAddress);
 var sendEmail = nlapiGetFieldValue('custbody_nspos_sendemail');
 nlapiLogExecution('DEBUG', "Send Email", sendEmail);
  
//Exit if there is no email address
if (emailAddress)
{
   nlapiLogExecution('DEBUG','Email Address',emailAddress);
}
else
{
nlapiLogExecution('DEBUG','Email Address','NO EMAIL ADDRESS - Exit Script');
return;
}
if (sendEmail == 'T')
{
   nlapiLogExecution('DEBUG','Send Email',sendEmail);
}
else
{
nlapiLogExecution('DEBUG','Email Address','SEND EMAIL FALSE - Exit Script');
return;
}
//Set the Email Field to the correct value from this transaction
try {
nlapiSetFieldValue('email',emailAddress);
}
catch(err) {
nlapiLogExecution('DEBUG','Email','ERROR SETTING FIELD - Exit Script');
nlapiLogExecution('DEBUG','Email',err);
}
//Set toBeEmailed to True so the email actually sends
try{
nlapiSetFieldValue('tobeemailed','T');
}
catch(err){
nlapiLogExecution('DEBUG','tobeemailed','ERROR SETTING FIELD - Exit Script');
nlapiLogExecution('DEBUG','tobeemailed',err);
}
}