/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_Suitelet_Item_GetAverageCost
 * Version            1.0.0.0
 **/


/**
 *
 * @param request
 * @param response
 * @return cost - the average cost for the given location for this item
 */
function getAverageCost(request, response) {

    try
    {
        // Retrieve Parameter Values
        var loc = request.getParameter('location');
        var item = request.getParameter('item');
        var itemtype = request.getParameter('itemtype');
        Log.d('getAverageCost', 'Location Received: ' + loc);
        Log.d('getAverageCost', 'Item Received: ' + item);
        Log.d('getAverageCost', 'Item Type: ' + itemtype);

        // Open the Item Record and Find the Average Cost
        var itemInfo = { cost:"" };
        var itemRecord = nsdal.loadObject(itemtype, item, ["id"])
            .withSublist("locations", ["locationid", "averagecostmli"]);
        if ( itemRecord && loc )
        {
            // Get the Average Cost for this location
            itemInfo.cost = EC.GetItemAverageCost(itemRecord, loc);
            Log.d('getAverageCost', 'Cost found:  ' + itemInfo.cost);

            // If a Cost was found - return that to the client-side script
            if ( itemInfo.cost && itemInfo.cost != null )
                var responseString = JSON.stringify(itemInfo);
            else
                var responseString = JSON.stringify({ "cost":"NOTFOUND" });

            Log.d('getAverageCost', 'responseString: ' + responseString);
            response.write(responseString);
        }
    }
    catch(e)
    {
        Log.d('getAverageCost', 'Unexpected Error thrown from EC_Suitelet_Item_GetAverageCost: ' + e);
        nlapiSendEmail(-5, "rhackey@exploreconsulting.com,rmayo@exploreconsulting.com", "Kit & Ace - EC_Suitelet_GetAverageCost Error", "Error Details:  " + e)
    }
}


EC.GetItemAverageCost = function(itemRecord, location){

    var averageCost = 0.00;
    if ( itemRecord )
    {
        _.each(itemRecord.locations, function(locLine, index)
        {
            //Log.d('Processing Locations Line ' + index+1, 'Location:  ' + locLine.locationid);
            if ( locLine.locationid == location )
            {
                averageCost = locLine.averagecostmli;
                Log.d('Average Cost Found for Location ' + location, 'Avg Cost:  ' + averageCost);
                return;
            }
        });
    }
    return averageCost;
};