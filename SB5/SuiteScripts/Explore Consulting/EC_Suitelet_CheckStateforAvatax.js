/**
 * Company            Explore Consulting
 * Copyright            2014 Explore Consulting, LLC
 * Type                NetSuite EC_Suitelet_CheckStateforAvatax
 * Version            1.0.0.0
 **/


/**Function that will do a search against a custom record to see if this state should have tax on it or not.
 * @param request
 * @param response
 */
function onStart(request, response) {

    try
    {
        var state = request.getParameter('state');
        nlapiLogExecution('DEBUG', 'onStart - Script Parameter', 'State: ' + state);

        var resp = {};
        resp.statefound = false;

        // Do search to see if this state should have Avalero tax applied to it or not.
        if ( state )
        {
            var results = nlapiSearchRecord("customrecord_ec_avatax_state_list", null, [new nlobjSearchFilter("custrecord_state_abbrev", null, "is", state)],
                [new nlobjSearchColumn("name"), new nlobjSearchColumn("custrecord_state_abbrev")]);

            if ( results )
            {
                resp.statefound = true;
            }
        }
    }
    catch(e)
    {
        Log.d("TaxableState", "Unexpected Error Occurred:  " + e);
    }

    var responseString = JSON.stringify(resp);
    nlapiLogExecution('DEBUG', 'onStart - Response to be Returned', 'responseString: ' + responseString);
    response.write(responseString);
}
