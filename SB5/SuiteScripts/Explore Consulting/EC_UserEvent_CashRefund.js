/**
 * Company            Explore Consulting
 * Copyright          2014 Explore Consulting, LLC
 * Type               NetSuite EC_UserEvent_CashRefund.js
 * Version            1.0.0.0
 * Version 						1.0.1 - only run when the externalid Hybris order is empty
 **/

function cr_onAfterSubmit(type) {

	if (!nlapiGetFieldValue('custbody_wtka_extsys_order_number')){

	// If this is a new record, proceed
	if(type == 'create') {

		try {

			var crId
			,	emailMerger
			,	mergeResult
			,	templateId
			,	emailBody
			,	emailSender
			,	emailSubject
			,	emailRecipient
			,	rmaId;

			if(nlapiGetContext().getEnvironment() == 'SANDBOX') {

				templateId = '13';
			} else {

				templateId = '11';	// TODO: Change on migration
			}

			emailSender = 1549;
			emailSubject = 'Return Processed Confirmation';

			crId = nlapiGetRecordId();
			rmaId = nlapiGetFieldValue('createdfrom');

			emailRecipient = nlapiGetFieldValue('entity');
			emailMerger = nlapiCreateEmailMerger(templateId);
			emailMerger.setTransaction(crId);
			emailMerger.setEntity('customer', emailRecipient);
			mergeResult = emailMerger.merge();
			emailBody = mergeResult.getBody();

//			testBody = testBody.splice(200, 500);
//			nlapiLogExecution('DEBUG', 'Merge result', testBody);

			try {

				nlapiSendEmail(emailSender, emailRecipient, emailSubject, emailBody, null, null, {transaction: crId, transaction: rmaId});
			} catch(e) {

				nlapiLogExecution('ERROR', 'Error sending Refund Processed notification email', e);
			}

		} catch(e) {

			nlapiLogExecution('ERROR', 'Unexpected Error', e);
			nlapiSendEmail(6, 'client-login@exploreconsulting.com', 'Unexpected Error generating Refund Processed notification email', e);
		}
	}
}
}
