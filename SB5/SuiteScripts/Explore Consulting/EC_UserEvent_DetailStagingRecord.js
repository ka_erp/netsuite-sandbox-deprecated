/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_UserEvent_DetailStagingRecord
 * Version            1.0.0.0
 **/

function onBeforeLoad(type, form, request) {

}

function onBeforeSubmit(type) {

}

function onAfterSubmit(type) {

    if ( type != 'delete' ) {

        var recordID = nlapiGetRecordId();
        Log.d("onAfterSubmit", "Record ID:  " + recordID);
        var rec = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());

        // If a record is inactivated - make sure the status is set to Error-Cancel
        if ( rec.getFieldValue("isinactive") == "T" ){
            Log.d("onAfterSubmit -- Record ID:  " + recordID, "Record is Inactive - setting status to Error-Cancel");
            rec.setFieldValue("custrecord_inv_adj_status", DetailStagingStatus.ErrorCancel);
        }

        // If the adjustment quantity is set to 0 - then set the status to Completed.  No 0 quantity adjustments will be done.
        if ( rec.getFieldValue("custrecord_inv_adj_adjust_quantity") == 0 ){
            Log.d("onAfterSubmit -- Record ID:  " + recordID, "Record is Inactive - setting status to Error-Cancel");
            rec.setFieldValue("custrecord_inv_adj_status", DetailStagingStatus.Completed);
        }

        var id = nlapiSubmitRecord(rec, true, true);
        Log.d("onAfterSubmit", "Re-saving Record ID:  " + id);
    }
}

Log.AutoLogMethodEntryExit(null, true, true, true);