/**
 * Created by Rachel10 on 3/31/2015.
 * File that will hold some global functions to help execute Inventory Adjustment Portlet functionality.
 */

var Script_GovernanceThreshold = 200;
var File_GovernanceThreshold = 70;
var startTime = moment();
var maxEndTime = moment(startTime).add(50, "minutes");

var HeaderStagingRecordProperties = ["id", "custrecord_header_status", "custrecord_header_adjustment_date", "custrecord_header_adjustment_location",
    "custrecord_header_adjustment_account", "custrecord_header_adjustment_dept", "custrecord_header_importer", "custrecord_header_adjustment_memo"];

var DetailStagingRecordProperties = ["id", "custrecord_inv_adj_status", "custrecord_inv_adj_number", "custrecord_inv_adj_location",
    "custrecord_inv_adj_subsidiary", "custrecord_inv_adj_account", "custrecord_inv_adj_date", "custrecord_inv_adj_department",
    "custrecord_inv_adj_class", "custrecord_inv_adj_quantity_on_hand", "custrecord_inv_adj_transaction", "custrecord_inv_adj_error",
    "custrecord_detail_line_number", "custrecord_header_staging_record", "custrecord_inv_adj_adjust_quantity", "custrecord_inv_adj_upc_code",
    "custrecord_inv_adj_search_qty"];

var InventoryAdjustmentProperties = ["id", "trandate", "account", "subsidiary", "department", "adjlocation", "memo"];
var InvAdjLineProperties = ["item", "adjustqtyby", "location"];

var HeaderStagingStatus = {
    CreateDetailRecords: 1,
    Processing: 2,
    PendingApproval: 3,
    Error: 4,
    Approved: 5,
    Completed: 6
};

var DetailStagingStatus = {
    WaitingApproval: 1,
    ToBeProcessed: 4,
    InProcess: 5,
    ErrorReprocess: 3,
    ErrorReview: 7,
    ErrorCancel: 6,
    Completed: 2
};


function CreateSelectField(field, list){

    if ( field && list ){
        var selectData = nlapiSearchRecord(list, null, new nlobjSearchFilter("isinactive", null, "is", "F"), new nlobjSearchColumn("name"));

        for ( var i=0; i<selectData.length; i++ )
            field.addSelectOption(selectData[i].getId(),selectData[i].getValue("name"));
    }
}

EC.RunTimeScriptCheckers = function(govLimit){

    return nlapiGetContext().getRemainingUsage() < govLimit || moment().isAfter(maxEndTime);
};


var recipient = 'kasupport@exploreconsulting.com';

if ( nlapiGetContext().getEnvironment() == "PRODUCTION" )
    var SendErrorEmailsFrom = 125247;
else
    var SendErrorEmailsFrom = 65646;

EC.SendEmailAlert = function(errorDets, scriptName){
    nlapiSendEmail(SendErrorEmailsFrom, recipient, "Error in " + scriptName,
        "Environment:  " + nlapiGetContext().getEnvironment() + "\nError Details:  " + errorDets);
};

// Queries a record and returns a collection of the columns defined
function runSearch(recordType, columns, filters)
{
    var searchResults = nlapiCreateSearch(recordType, filters, columns);
    var returnSet = [];
    var results = searchResults.runSearch();
    if (results) {
        for(var t = 0; t <= 10000; t+=1000) {
            var resultSet = results.getResults(t, t+1000);
            if(!resultSet){break;}
            resultSet.forEach(function(element) { returnSet.push(element); });
        }
    }
    return returnSet;
}

EC.FindItemIDByUPC = function(upc){

    var itemid = undefined;
    try{
        if ( !upc ) return undefined;
        else upc = parseFloat(upc).toFixed(0);

        var results = nlapiSearchRecord("item", null, new nlobjSearchFilter("upccode", null, "is", upc));
        if ( results ) {
            itemid = results[0].getId();
            Log.d("FindItemIDByUPC", "ITEM ID FOUND:  " + itemid);
        }
        else Log.d("FindItemIDByUPC", "No search results found");
    }
    catch(e){
        Log.d("FindItemIDByUPC", "Unexpected Error while searching for UPC Code:  " + e);
    }
    return itemid;
};
