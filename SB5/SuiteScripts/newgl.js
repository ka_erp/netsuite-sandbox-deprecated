function customizeGlImpact(transactionRecord, standardLines, customLines, book)
{
	
	//For the invoice
	nlapiLogExecution('DEBUG','Ivan', 'yes'); 
	
	
	//Run all I need
	var transactionStatus = transactionRecord.getRecordType().toLowerCase();
	var subsidiaryInternalID=transactionRecord.getFieldValue('subsidiary');
	var transactionId = transactionRecord.getId();
		
	var resultSet = runDiscountReasonSearch(); 
	var glTable = getGLReplacementTable(resultSet);	
	var uniq = _.uniq(glTable, function(person) { return person.originalGLAccount; });
	var accountGL = _.pluck(uniq, 'originalGLAccount'); accountGL =  _.map(accountGL, Number);
	
	
	nlapiLogExecution('AUDIT','glTable: uniq', JSON.stringify(uniq)); 
	nlapiLogExecution('AUDIT','glTable: accountGL', JSON.stringify(accountGL)); 
	
	//Find all accounts to be replaced
	
	var glAccountArray = getGLAccounts(standardLines); var runMe = false; 
	var glAccountArrayObjs = getGLAccountsObjs(standardLines);
	
	
	//var accountsThatMatters = _(glAccountArrayObjs).filter(x.acct, function (x) { return _.contains(values, ['131','132']});	
	var accountsThatMatters = _.filter(glAccountArrayObjs, function (i) {
                return this.keys.indexOf(i.acct) > -1;
	}, {
		"keys": accountGL // values to look for
	});

	

	
	nlapiLogExecution('AUDIT','glAccountArray: glAccountArray', JSON.stringify(glAccountArray)); 
	nlapiLogExecution('AUDIT','glAccountArray: accountsThatMatters', JSON.stringify(accountsThatMatters)); 
	
	
	for (var i = 0; i < uniq.length; i++){
		
		var origAccount = uniq[i].originalGLAccount; nlapiLogExecution('AUDIT','glTable: uniq: origAccount', origAccount); 
		origAccount = parseInt(origAccount);
		
		if(glAccountArray.indexOf(origAccount) > -1){
			runMe = true; 
			break; 
		}
	}
	
	
	nlapiLogExecution('AUDIT', 'Run Me',runMe ); 
	
	
	//Find all the account in this transaction
	
	if(runMe){
	
		//Traverse item / reason codes
		var itemLines = getItemLines(transactionRecord); 
		nlapiLogExecution('AUDIT','itemLines: itemLines', JSON.stringify(itemLines)); 
	
		itemLinesObj = itemLines.objs; 
		
		//traverse
		for (var j = 0 ; j < itemLinesObj.length; j++){
			
			
			nlapiLogExecution('AUDIT','itemLines: itemLines[' + j + '] START',j); 
			
			var discCode = itemLinesObj[j].discountcode;
			var discItem = itemLinesObj[j].discountitem;
			
			//if(false){
				
					var acct = accountsThatMatters[j].acct; 
					var amt = accountsThatMatters[j].debit; amt = parseFloat(amt); 
					var id = accountsThatMatters[j].id; 
					
					nlapiLogExecution('AUDIT','itemLines: itemLines[' + j + ']', JSON.stringify(accountsThatMatters[j])); 								
					
					var findingGLTable = _(glTable).filter(function (x) { 
					
							return x.originalGLAccount == acct 
								&& x.discountCode == discCode
								&& x.transSubsidiary == subsidiaryInternalID
								&& x.discountItem == discItem;
								
								
								
								/**
									transactionLookupData.discountItem 			=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_discount_item' );	
									transactionLookupData.discountCode 			=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_discount_code' );	
									transactionLookupData.originalGLAccount 	= 	resultSet[resultItemIndex].getValue('custrecord_wtka_map_orig_account' );	
									transactionLookupData.overrideGLAccount 	=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_new_account' );	
									transactionLookupData.transSubsidiary 		=	resultSet[resultItemIndex].getValue('custrecord_wtka_discount_reas_subsidiary' );	
									transactionLookupData.overrideLocation	 	=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_new_location' );	
									transactionLookupData.overrideDepartment 	=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_new_department' );												
								**/
								
						
						}									
					);
					
					
					if(amt > 0){
						var replacementAcct = findingGLTable[0]; 
						
						nlapiLogExecution('AUDIT','itemLines: itemLines[' + j + ']: replacementAcct', JSON.stringify(replacementAcct)); 
						
						var newLine = customLines.addNewLine();													
						newLine.setAccountId(parseInt(replacementAcct.originalGLAccount)); 
						newLine.setCreditAmount(amt);
						
						var newLine = customLines.addNewLine();
						newLine.setAccountId(parseInt(replacementAcct.overrideGLAccount)); 
						newLine.setDebitAmount(amt);
					}
					
					nlapiLogExecution('AUDIT','itemLines: itemLines[' + j + ']', JSON.stringify(findingGLTable));
			//}
			
			
		}
	
	
	
	}
	
	
	
	
	//Check if I need to run the script
	
	
}

function getGLAccountsObjs(standardLines){
	
	var linecount 	= standardLines.getCount();
	var accountArray = new Array(); 
	
	for(var i=0; i<linecount; i++) 
	{
	
			var line 	=  standardLines.getLine(i);
			var lineObj = new Object();
			
			var thisObj = new Object(); 
			var acct 	=  line.getAccountId();
			var credit 	=  line.getCreditAmount();
			var debit 	=  line.getDebitAmount();
			var getId 	=  line.getId();
			
			lineObj.acct = acct; 			
			lineObj.debit = debit; 
			lineObj.credit = credit; 
			lineObj.id = getId; 
			
			accountArray.push(lineObj); 
			

	}
	
	return accountArray; 
}

function getGLAccounts(standardLines){
	
	var linecount 	= standardLines.getCount();
	var accountArray = new Array(); 
	
	for(var i=0; i<linecount; i++) 
	{
	
			var line 	=  standardLines.getLine(i);
			
			var thisObj = new Object(); 
			var acct 	=  line.getAccountId();
			var amt 	=  line.getDebitAmount();
			var getId 	=  line.getId();
			
			accountArray.push(acct); 
			

	}
	
	return accountArray; 
}


function deprecated(){
	
	//Transaction stuff
	var transactionStatus = transactionRecord.getRecordType().toLowerCase();
	var subsidiaryInternalID=transactionRecord.getFieldValue('subsidiary');
	
	var transactionId = transactionRecord.getId();	
	
	var rcAmount = summarizeItemByReasonCodes(transactionRecord);		
		
	//var resultSet =  lookupTransactionGlMapping(subsidiaryInternalID,  transactionStatus); setGLPosings(resultSet, rcAmount);
	var resultSet = DiscountReasonMappingLookup(); setGLPosings(resultSet, rcAmount);
	
	var linecount 	= standardLines.getCount();
	nlapiLogExecution('DEBUG', 'standardLines Count', linecount);		
	
	
		
	for(var i=0; i<linecount; i++) 
	{
	
			var line 	=  standardLines.getLine(i);
			
			var thisObj = new Object(); 
			var acct 	=  line.getAccountId();
			var amt 	=  line.getCreditAmount();
			var getId 	=  line.getId();
			
			thisObj.acct =  acct; 
			thisObj.amt = amt; 
			thisObj.id = getId; 
					
			nlapiLogExecution('AUDIT', 'line', JSON.stringify(thisObj)); 
			
			targetAcct = 131; 
					
			if(acct == targetAcct && amt > 0){
			
				var newLine = customLines.addNewLine();
				
				replacementAcct = getReplacementAccount(line);
				
				newLine.setAccountId(replacementAcct); 
				newLine.setCreditAmount(parseFloat(amt));
				
				var newLine = customLines.addNewLine();
				newLine.setAccountId(targetAcct); 
				newLine.setDebitAmount(parseFloat(amt));
			
			}

	}
	
}





function getReplacementAccount(lineindex){
	
	var account = 1213;
	if(lineindex == 1 || lineindex == 5 ){		
		account = 51120; 
	}
		
	return account; 
}

function getGLReplacementTable(resultSet){ //, reasonCodes){
	
	var thisArray = new Array(); 
	
	//nlapiLogExecution('AUDIT','setGLPosings', "setGLPosings" + JSON.stringify(resultSet)); 
	
	if(resultSet != null){
		
			nlapiLogExecution('AUDIT','setGLPosings', "resultSet"); 
			
			for (var resultItemIndex=0; resultItemIndex<resultSet.length; resultItemIndex++){		
				
				
				
				//if(reasonCodes){
					
					
					
					var transactionLookupData = new Object(); 
					
					transactionLookupData.discountItem 			=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_discount_item' );	
					transactionLookupData.discountCode 			=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_discount_code' );	
					transactionLookupData.originalGLAccount 	= 	resultSet[resultItemIndex].getValue('custrecord_wtka_map_orig_account' );	
					transactionLookupData.overrideGLAccount 	=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_new_account' );	
					transactionLookupData.transSubsidiary 		=	resultSet[resultItemIndex].getValue('custrecord_wtka_discount_reas_subsidiary' );	
					transactionLookupData.overrideLocation	 	=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_new_location' );	
					transactionLookupData.overrideDepartment 	=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_new_department' );	
					
					
					/*transactionLookupData.locationGL=resultSet.resultSet( 'custrecordwtka_map_override_location' );										
					transactionLookupData.departmentGL=	resultSet.getValue( 'custrecord_wtka_map_department' );	
					transactionLookupData.transactionStatusType=resultSet.getValue( 'custrecord_wtka_map_transaction_type' );*/
					
					
					var reasonCode =transactionLookupData.overrideGLAccount; 
					//var idsArray = reasonCodes[0];
					//var objsArray = reasonCodes[0];
					
					/*
					if(idsArray.indexOf(reasonCode) >= 0)
						var pointersinarray = idsArray.indexOf(reasonCode);
						transactionLookupData.amount = 	objsArray[pointersinarray].cost;	
					else{						
						transactionLookupData.amount = 0.00;
					}*/
					
					thisArray.push(transactionLookupData); 
					//nlapiLogExecution('DEBUG','discountCollection', JSON.stringify(transactionLookupData)); 		
				//}
				
			}
	}
	nlapiLogExecution('AUDIT','discountCollection: thisArray', JSON.stringify(thisArray)); 		
	
	return thisArray;
	

	
	//traverseLines
		
	
}


function getItemLines(transactionRecord){
	
	var fItemCount = transactionRecord.getLineItemCount('item');
	nlapiLogExecution('AUDIT', 'summarizeItemByReasonCodes fItemCount', fItemCount); 
	
	
	var discountCollection = new Array(); 
	var discountCollectionIds = new Array(); 
	
	try{
	
		for(var a = 1 ; a <= fItemCount; a++){
			
			var itemType = transactionRecord.getLineItemValue('item','itemtype',a);
			if (itemType.toLowerCase()=='invtpart') 
			{
				
					var discountCode = transactionRecord.getLineItemValue('item','custcol_ra_discount_reason',a);
					var costEstimate = transactionRecord.getLineItemValue('item','costestimate',a);
					var discountItemType = transactionRecord.getLineItemValue('item','itemtype',a);		
					var discountItemReason = transactionRecord.getLineItemValue('item','custcol_ra_discount_reason',a);
					var discountItemReasonText = transactionRecord.getLineItemText('item','custcol_ra_discount_reason',a);
					var lineaccount = transactionRecord.getLineItemValue('item','account',a);
					var linesequence = transactionRecord.getLineItemValue('item','line',a);
					var item = transactionRecord.getLineItemValue('item','item',a);
					var discount = transactionRecord.getLineItemValue('item','custcol_ra_discount_reason',a);
					var discountItem = transactionRecord.getLineItemValue('item','custcol_erp_discount_item',a);
					
					costEstimate = parseFloat(costEstimate);
					
					if(lineaccount == 131 && costEstimate > 0){
	
						var newLine = customLines.addNewLine();
						newLine.setAccountId(1198); 
						newLine.setCreditAmount(parseFloat(costEstimate));
						
						var newLine = customLines.addNewLine();
						newLine.setAccountId(131); 
						newLine.setDebitAmount(parseFloat(costEstimate));
					
					}
					
					
					nlapiLogExecution('AUDIT', 'lineaccount', lineaccount + " > " + linesequence + " > " + item + " > " + discount); 
					
					
					var discObj = new Object(); 
					
					discObj.discountcode = discountCode;
					discObj.cost = costEstimate; 
					discObj.discounttype = discountItemType; 
					discObj.discountreason = discountItemReason; 
					discObj.discountitemreason = discountItemReason; 
					discObj.discountitem = discountItem; 
					discObj.item = item; 
					discObj.lineaccount = lineaccount; 
					discObj.linesequence = linesequence; 
					
					
					discountCollectionIds.push(linesequence); 			
					discountCollection.push(discObj); 
					
					
					/*
					if(costEstimate != null){
						costEstimate = parseFloat(costEstimate); 
					}
					if(costEstimate == null){
						costEstimate = 0.0;
					}
					
					
					if(discountCollectionIds.indexOf(discountCode) < 0){

						var discObj = new Object(); 
						
						discObj.discountcode = discountCode;
						discObj.cost = costEstimate; 
						discObj.discounttype = discountItemType; 
						discObj.discountreason = discountItemReason; 
						discObj.discountitemreason = discountItemReason; 
						discObj.item = item; 
						discObj.lineaccount = lineaccount; 
						discObj.linesequence = linesequence; 
						
						
						discountCollectionIds.push(discountCode); 			
						discountCollection.push(discObj); 
						
					}else{
						
						var discountIndex  =  discountCollectionIds.indexOf(discountCode); 
									
						var tempObj = discountCollection[discountIndex]; 			
						tempObj.cost = tempObj.cost + costEstimate; 			
						discountCollection[discountIndex] = tempObj; 
						
						
					} */ 
								
					nlapiLogExecution('DEBUG','discountCollection', JSON.stringify(discountCollection)); 					
			}
		}
		
		var returnObject = new Object(); 
		returnObject.ids = discountCollectionIds;
		returnObject.objs = discountCollection;
		
		return returnObject;
	}
	catch(ex) {}
		
}


function runDiscountReasonSearch()
{
try{
		var columns = new Array();
		columns[0] = new nlobjSearchColumn( 'custrecord_wtka_map_discount_item' ); ///Item
		columns[1] = new nlobjSearchColumn( 'custrecord_wtka_map_discount_code' ); //Discount Reason Code 
		columns[2] = new nlobjSearchColumn( 'custrecord_wtka_map_orig_account' );//original Gl Account
		columns[3] = new nlobjSearchColumn( 'custrecord_wtka_map_new_account' );//Override Gl Account
		columns[4] = new nlobjSearchColumn( 'custrecord_wtka_map_new_location' );//Location
		columns[5] = new nlobjSearchColumn( 'custrecord_wtka_map_new_department' );//department
		columns[6] = new nlobjSearchColumn( 'custrecord_wtka_discount_reas_subsidiary' );//subsidiary

			var searchResultData	 = new Object();		
			var filters = new Array();
				/*filters[0] = new nlobjSearchFilter( 'custrecord_wtka_map_discount_code', null, 'is', discountItemObject.discountItemDiscountReasonInternalID);	
				filters[1] = new nlobjSearchFilter( 'custrecord_wtka_map_discount_item', null, 'is', discountItemObject.discountItemInternalID);
				filters[2] = new nlobjSearchFilter( 'custrecord_wtka_map_orig_account', null, 'anyof', discountItemObject.COGSAccountInternalID);	
				filters[3] = new nlobjSearchFilter( 'custrecord_wtka_discount_reas_subsidiary', null, 'anyof', discountItemObject.transactionSubsidiaryInternalID);
				filters[4] = new nlobjSearchFilter( 'isinactive', null, 'is', 'F');			*/
				
				
				nlapiLogExecution('DEBUG','Before DiscountReasonMappingLookup' );
				var searchresults = nlapiSearchRecord('customrecord_wtka_map_discount_account', null, null,columns);					
				if (searchresults==null ){ return null;}else{return searchresults;}	
				
				/*nlapiLogExecution('DEBUG','Post DiscountReasonMappingLookup', searchresults.length);							
				var searchresult = searchresults[0];	
				searchResultData.originalGLAccount = searchresult.getValue( 'custrecord_wtka_map_orig_account' );	
				searchResultData.overrideGLAccount=	searchresult.getValue( 'custrecord_wtka_map_new_account' );	
				searchResultData.locationGL=	searchresult.getValue( 'custrecord_wtka_map_new_location' );		
				searchResultData.departmentGL=	searchresult.getValue( 'custrecord_wtka_map_new_department' );	
				searchResultData.discountReasonCode=discountItemObject.discountItemDiscountReasonCodeInText; // to add discount code in GL entry
				var savedSearchValues = "originalGLAccount: "+searchResultData.originalGLAccount + 	" overrideGLAccount: "	+searchResultData.overrideGLAccount +" locationGL:"+searchResultData.locationGL +" departmentGL:"+searchResultData.departmentGL	+ "discountItemObject.discountItemDiscountReasonCodeInText="+discountItemObject.discountItemDiscountReasonCodeInText;	
				nlapiLogExecution('DEBUG',  ' Saved Search Result', savedSearchValues);*/
				
				
	}
	catch(err)
	{
		nlapiLogExecution('DEBUG',' DiscountReasonMappingLookup',err);	
		return null;		
	}			
}


function lookupTransactionGlMapping(subsidiaryInternalID,  transactionStatus)
{
	
	nlapiLogExecution('DEBUG','lookupTransactionGlMapping', "lookupTransactionGlMapping"); 
	try{
		
	/* We realized that there is no way to compare transaction type with Transaction type list designed in  WTKA - Transaction GL Mapping si below code is for setting internal ids of transaction type*/
		switch (transactionStatus)
		{
			case 'creditmemo':
				transactionStatus=1;
				break;
			case 'cashrefund':
				transactionStatus=2;
				break;
			default:
				transactionStatus=-1;
			
		}
		
		nlapiLogExecution('DEBUG','lookupTransactionGlMapping', "lookupTransactionGlMapping"); 
		
		if (transactionStatus<=0) {return null;}
		
		//nlapiLogExecution('DEBUG',' lookupTransactionGlMapping:::::transactionStatus', 'Transaction Status='+transactionStatus);				 
		var columns = new Array();
		columns[0] = new nlobjSearchColumn( 'custrecord_wtka_map_original_gl_account' ); ///Original GL Account
		columns[1] = new nlobjSearchColumn( 'custrecord_wtka_map_override_account' ); //Override GL account 
		columns[2] = new nlobjSearchColumn( 'custrecord_wtka_map_trans_subsidiary' );//Subsidiary
		columns[3] = new nlobjSearchColumn( 'custrecordwtka_map_override_location' );//Override Location
		columns[4] = new nlobjSearchColumn( 'custrecord_wtka_map_department' );//Override Department
		columns[5] = new nlobjSearchColumn( 'custrecord_wtka_map_transaction_type' );//Transaction Type
		

			var filters = new Array();
				
				filters[0] = new nlobjSearchFilter( 'custrecord_wtka_map_transaction_type', null, 'is', transactionStatus);
				filters[1] = new nlobjSearchFilter( 'custrecord_wtka_map_trans_subsidiary', null, 'anyof', subsidiaryInternalID);
				filters[2] = new nlobjSearchFilter( 'isinactive', null, 'is', 'F');			
				
				var searchresults = nlapiSearchRecord('customrecord_wtka_map_trans_mapping', null, filters,columns);
				return searchresults;
				/*
				nlapiLogExecution('DEBUG','Post lookupTransactionGlMapping', searchresults.length);							
				var searchresult = searchresults[0];	
				searchResultData.originalGLAccount = searchresult.getValue( 'custrecord_wtka_map_orig_account' );	
				searchResultData.overrideGLAccount=	searchresult.getValue( 'custrecord_wtka_map_new_account' );	
				searchResultData.locationGL=searchresult.getValue( 'custrecord_wtka_map_new_location' );		
				searchResultData.departmentGL=	searchresult.getValue( 'custrecord_wtka_map_new_department' );	
				searchResultData.transactionStatusType=searchresult.getValue( 'custrecord_wtka_map_transaction_type' );	
				return searchResultData;
				*/
	}
	catch(ex)
	{
		nlapiLogExecution('ERROR', 'lookupTransactionGlMapping ' +ex);
		return null;
	}
	
}