function setLocationCurrency(){

var locationId = nlapiGetFieldValue('location'); 
var currencyId = nlapiLookupField('location', locationId, 'custrecord_ra_loc_currency'); 

if(currencyId){

nlapiSetFieldValue('currency', currencyId);

}

}