/**
 *	File Name		:	WTKA_NetSuite_CustomerOptIn.js
 *	Function		:	Create Customers and set Opt-In
 * 	Authors			:	Rini Thomas
 *	Company			:	Wipro Limited
 *	Release Dates	:	29-June-2016 (v1.0)
 * 	Current Version	:	1.0
**/

{
	var ErrorObj 		 = new Object();
	ErrorObj.status 	 = "Error";
	ErrorObj.messages 	 = new Array();
	ErrorObj.messages[0] = new Object();
	
	var sendEmail 		 = false;
	var ccList 			 = null; //['']; //Enter CC Addresses as array
	var toList 			 = ['3PL_eComm_Integration@kitandace.com'];
}

function createCustomer(dataIn)
{
	nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
	try
	{
		var customer = 0, 		custNo, 	record;
		var existing = false;
		if(PrevalidationCheck(dataIn))
		{
			var locale  = dataIn.locale.split('/');
			var country = locale[locale.length-1].toUpperCase(); //Locale - kitandace.com/ca
			if(country == "UK")	country = "GB";
			var subsid = FetchSubsidiaryId('country', country);
			
			if(subsid == -1)
			{
				ErrorObj.messages[0] = new Object();
				ErrorObj.messages[0].status   		= "Error";
				ErrorObj.messages[0].messagetype    = "Subsidiary error";
				ErrorObj.messages[0].message 	    = "Invalid Query. Error: Subsidiary for Country: " + country + " cannot be found.";
				nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
				var errorLogs = LogEntityCreation(1, 4, customer, dataIn, 2, ErrorObj);
				sendErrorEmail(dataIn, ErrorObj);
				return ErrorObj;
			}
			
			//Lookup Existing Leads using Email
			var cols 		 = new Array();
			cols.push(new nlobjSearchColumn('entityid'));
			cols.push(new nlobjSearchColumn('subsidiary'));
			var filters 	 = new Array();
			filters.push(new nlobjSearchFilter('email',  	  null, 'is', 	dataIn.customerEmail));
			filters.push(new nlobjSearchFilter('subsidiary',  null, 'is', 	subsid.code));
			var searchResult = nlapiSearchRecord('customer', null, filters, cols);
			if(searchResult != null)
			{
				customer 	= searchResult[0].getId();
				custNo	 	= searchResult[0].getValue('entityid');
				existing 	= true;
			}
			
			try
			{
				if(existing)
				{
					record = nlapiLoadRecord('customer', customer); //Update
				}
				else
				{
					record = nlapiCreateRecord('customer', {recordmode: 'dynamic'}); //Create
					record.setFieldValue('subsidiary', subsid.code);
					record.setFieldValue('email', 		dataIn.customerEmail);
				}
				
				if(emptyObject(dataIn.firstName))
				{
					var firstName 	 = (dataIn.firstName.match(/unknown/gi) != null) ? '||NONAME||' : dataIn.firstName;
					record.setFieldValue('firstname', 	 firstName);
					
					if(firstName != "||NONAME||" && emptyObject(dataIn.lastName))
					{
						var lastName = (dataIn.lastName.match(/unknown/gi) != null)  ? '||NONAME||' : dataIn.lastName;
						record.setFieldValue('lastname', lastName);
					}
				}
				else
				{
					record.setFieldValue('firstname', 	 '||NONAME||');
				}	
				
				record.setFieldValue('entitystatus', 				13); //Customer - Closed Won
				record.setFieldText('globalsubscriptionstatus', 	'Soft Opt-In');
				record.setFieldValue('custentity_newsletteroptin', 	'T');		
				
				record.selectNewLineItem('addressbook');
				record.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T'); 
				record.setCurrentLineItemValue('addressbook', 'defaultbilling',  'T'); 
				record.setCurrentLineItemValue('addressbook', 'isresidential',   'F'); 
				record.setCurrentLineItemValue('addressbook', 'country', country);
				record.commitLineItem('addressbook');
				
				customer = nlapiSubmitRecord(record, true, true);
				
				if(!existing)
				{
					var customerRecord 	= nlapiLoadRecord('customer', customer);
					custNo	   	   		= customerRecord.getFieldValue('entityid');
				}
				
				var SuccessObj 						= new Object();
				SuccessObj.status 					= "Success";
				SuccessObj.messages 				= new Array();
				SuccessObj.messages[0] 				= new Object();
				SuccessObj.messages[0].recordId 	= customer;
				SuccessObj.messages[0].customerId 	= custNo;
				SuccessObj.messages[0].message 		= "Customer successfully ";
				SuccessObj.messages[0].message 		+= (existing) ? "updated." : "created.";
				nlapiLogExecution('debug', 'SuccessObj', JSON.stringify(SuccessObj));
				var customerLogs = LogEntityCreation(1, 4, customer, dataIn, 1, SuccessObj);
				return SuccessObj;
			}
			catch(err)
			{
				ErrorObj.status 	 = 'Exception'; 
				ErrorObj.messages[0] = new Object();
				ErrorObj.messages[0].messagetype 	= (existing) ? "Customer Update Failure" : "Customer Creation Failure";
				ErrorObj.messages[0].message 		= (existing) ? "Customer " + custNo + " could not be updated" : "Customer could not be created.";
				ErrorObj.messages[0].message 		+= " Details as below."; 
				ErrorObj.messages[0].details 		= err.getCode() + ' : ' + err.getDetails(); //err;
				nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
				sendErrorEmail(dataIn, ErrorObj);
				var errorLogs = LogEntityCreation(1, 4, customer, dataIn, 2, ErrorObj);			
				return ErrorObj;
			}
		}
		else
		{
			nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
			sendErrorEmail(dataIn, ErrorObj);
			var errorLogs = LogEntityCreation(1, 4, customer, dataIn, 2, ErrorObj);	
			return ErrorObj;
		}
	}
	catch(any_err)
	{
		ErrorObj.status 	 = 'Exception'; 
		ErrorObj.messages[0] = new Object();
		ErrorObj.messages[0].messagetype 	= "Customer Update Failure";
		ErrorObj.messages[0].message 		= "Customer could not be created or updated";
		ErrorObj.messages[0].message 		+= " Details as below."; 
		ErrorObj.messages[0].details 		= any_err;
		nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
		sendErrorEmail(dataIn, ErrorObj);
		return ErrorObj;
	}
}

function PrevalidationCheck(dataIn)
{
	//Pre-Validation checks on DataIn
	
	//1. Check if dataIn is empty
	if(!emptyInbound(dataIn))									return false;

	//2. Check if dataIn has header level objects values set
	if(!emptyObject(dataIn.customerEmail, 	"CustomerEmail"))	return false;
	if(dataIn.customerEmail == "unknown")						return false;
	if(!emptyObject(dataIn.locale, 			"Locale"))			return false;
	if(dataIn.locale == "unknown")								return false;
	
	return true;
}

function userEventBeforeLoad(type, form, request)
{
	if((nlapiGetContext().getExecutionContext() == 'userinterface') && (type == 'view'))
	{
		var cols = new Array();
		cols[cols.length] = new nlobjSearchColumn('custrecord_wtka_entity_editype');
		cols[cols.length] = new nlobjSearchColumn('custrecord_wtka_entity_edinumber');
		cols[cols.length] = new nlobjSearchColumn('custrecord_wtka_entity');
		
		var filters = new Array();
		filters[filters.length] = new nlobjSearchFilter('custrecord_wtka_entity', null, 'anyof', nlapiGetRecordId());
		
		var searchResult = nlapiSearchRecord('customrecord_wtka_entitylogs', null, filters, cols);
		if(searchResult !=  null)	displayEntityLogs(form, searchResult);
	}
}

function sendErrorEmail(request, message)
{
	var subject = 'Customer Opt-In failure';
	var body 	= 'Hello,<br><br>';
	body 		+= 'Customer creation and Opt-In request failed in NetSuite due to below error.<br>';
	body 		+= JSON.stringify(message);
	body 		+= '<br><br><b>Data received in NetSuite: </b><br><br>' + JSON.stringify(request) + '<br><br><br>';
	body 		+= '<br><br>Process needs to be resubmitted or handled manually through user interface after corrections.';
	body 		+= '<br><br><br><br><br><br><br>Thanks';
	body 		+= '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
	nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
	if(sendEmail)	nlapiSendEmail(243423, toList, subject, body, ccList);
}

function displayEntityLogs(form, searchResult)
{
	var edinumber = searchResult[0].getText('custrecord_wtka_entity_edinumber');
	
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wtka_entity_parent', null, 'anyof', searchResult[0].getId())); //Parent Id
	
	var cols = new Array();
	cols.push(new nlobjSearchColumn('custrecord_wtka_entity_parent'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_entity_request'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_entity_response'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_entity_status'));
	cols.push(new nlobjSearchColumn('created'));
	
	var searchResultChild = nlapiSearchRecord('customrecord_wtka_entitylogs_child', null, filters, cols);
	if(searchResultChild != null)
	{
		form.addTab('custpage_wtkalogs', 'Integration Logs');
		var LogList = form.addSubList('custpage_wtkaloglist', 'list', searchResult[0].getText('custrecord_wtka_entity_edinumber'), 'custpage_wtkalogs');
		LogList.addField('custpage_wtkacreated', 	'date', 		'Date Created');
		LogList.addField('custpage_wtkatime', 		'timeofday', 	'Time');
		LogList.addField('custpage_wtkaedinumber', 	'text', 		'EDI Number');
		LogList.addField('custpage_wtkaedistatus', 	'text', 		'EDI Status');
		LogList.addField('custpage_wtkarequest', 	'textarea', 	'Request');
		LogList.addField('custpage_wtkaresponse', 	'textarea', 	'Response');
			
		for(var j=0; searchResultChild != null && j<searchResultChild.length; j++)
		{
			var createdDate = new Date (searchResultChild[j].getValue('created'));
		
			LogList.setLineItemValue('custpage_wtkacreated',	j+1, nlapiDateToString(createdDate, 'date')); 
			LogList.setLineItemValue('custpage_wtkatime',	    j+1, nlapiDateToString(createdDate, 'timeofday')); 
			LogList.setLineItemValue('custpage_wtkaedinumber', 	j+1, searchResult[0].getText('custrecord_wtka_entity_edinumber'));
			LogList.setLineItemValue('custpage_wtkaedistatus',  j+1, searchResultChild[j].getText('custrecord_wtka_entity_status'));
			LogList.setLineItemValue('custpage_wtkarequest', 	j+1, searchResultChild[j].getValue('custrecord_wtka_entity_request'));
			LogList.setLineItemValue('custpage_wtkaresponse', 	j+1, searchResultChild[j].getValue('custrecord_wtka_entity_response'));
		}

		if(searchResult.length > 1)
		{
			var filters1 = new Array();
			filters1.push(new nlobjSearchFilter('custrecord_wtka_entity_parent', null, 'anyof', searchResult[1].getId())); //Parent Id
			
			var searchResultChild1 = nlapiSearchRecord('customrecord_wtka_entitylogs_child', null, filters1, cols);
			if(searchResultChild1 != null)
			{
				edinumber 	 = searchResult[1].getText('custrecord_wtka_entity_edinumber');
				var LogList1 = form.addSubList('custpage_wtkaloglist1', 'staticlist', edinumber, 'custpage_wtkalogs');
				LogList1.addField('custpage_wtkacreated1', 		'date', 		'Date Created');
				LogList1.addField('custpage_wtkatime1', 		'timeofday', 	'Time');
				LogList1.addField('custpage_wtkaedinumber1', 	'text', 		'EDI Number');
				LogList1.addField('custpage_wtkaedistatus1', 	'text', 		'EDI Status');
				LogList1.addField('custpage_wtkarequest1', 		'textarea', 	'Request');
				LogList1.addField('custpage_wtkaresponse1', 	'textarea', 	'Response');
				
				for(var j=0; j < searchResultChild1.length; j++)
				{
					var createdDate = new Date (searchResultChild1[j].getValue('created'));
					LogList1.setLineItemValue('custpage_wtkacreated1',		j+1, nlapiDateToString(createdDate, 'date')); 
					LogList1.setLineItemValue('custpage_wtkatime1',	    	j+1, nlapiDateToString(createdDate, 'timeofday')); 
					LogList1.setLineItemValue('custpage_wtkaedinumber1', 	j+1, edinumber);
					LogList1.setLineItemValue('custpage_wtkaedistatus1',  	j+1, searchResultChild1[j].getText('custrecord_wtka_entity_status'));
					LogList1.setLineItemValue('custpage_wtkarequest1', 		j+1, searchResultChild1[j].getValue('custrecord_wtka_entity_request'));
					LogList1.setLineItemValue('custpage_wtkaresponse1', 	j+1, searchResultChild1[j].getValue('custrecord_wtka_entity_response'));
				}
			}
		}
	}
}

function LogEntityCreation(ediType, ediNumber, sourceEntity, dataIn, ediStatus, ediResponse)
{
	var parentId = 0;
	var cols 	 = new Array();
	cols[0]  	 = new nlobjSearchColumn('custrecord_wtka_entity');
	cols[1]  	 = new nlobjSearchColumn('custrecord_wtka_entity_editype');
	cols[2]  	 = new nlobjSearchColumn('custrecord_wtka_entity_edinumber');
	
	var filters  = new Array();
	filters[0]   = new nlobjSearchFilter('custrecord_wtka_entity', 				null, 'anyof', sourceEntity);
	filters[1]   = new nlobjSearchFilter('custrecord_wtka_entity_editype', 		null, 'anyof', ediType);
	filters[2]   = new nlobjSearchFilter('custrecord_wtka_entity_edinumber', 	null, 'anyof', ediNumber);

	var searchRecord = nlapiSearchRecord('customrecord_wtka_entitylogs', null, filters, cols);
	if(searchRecord != null)
	{
		for(var i=0;  i < searchRecord.length ; i++)
		{
			parentId = searchRecord[i].getId()
		}
	}
	else
	{
		var logRecord = nlapiCreateRecord('customrecord_wtka_entitylogs');
		logRecord.setFieldValue('custrecord_wtka_entity_editype', 	ediType);
		logRecord.setFieldValue('custrecord_wtka_entity_edinumber', ediNumber);
		if(sourceEntity > 0) logRecord.setFieldValue('custrecord_wtka_entity',  sourceEntity);
		parentId = nlapiSubmitRecord(logRecord);
	}

	var logRecordChild = nlapiCreateRecord('customrecord_wtka_entitylogs_child');

	logRecordChild.setFieldValue('custrecord_wtka_entity_parent', 	parentId);
	logRecordChild.setFieldValue('custrecord_wtka_entity_request', 	JSON.stringify(dataIn));
	logRecordChild.setFieldValue('custrecord_wtka_entity_response', JSON.stringify(ediResponse));
	logRecordChild.setFieldValue('custrecord_wtka_entity_status', 	ediStatus);
	
	var childId = nlapiSubmitRecord(logRecordChild);
	
	var logArray = new Array();
	logArray[0]  = parentId;
	logArray[1]  = childId;
	return logArray;
}

