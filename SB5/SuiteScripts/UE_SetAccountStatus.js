/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Dec 2014     joel
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	nlapiLogExecution('debug', 'userEventBeforeSubmit', 'type = ' + type);
	
	try {
		if (type == 'changepassword') {
			nlapiLogExecution('debug', 'userEventBeforeSubmit', 'found changepassword');
			
			var cust = nlapiGetNewRecord();
			nlapiLogExecution('debug', 'userEventBeforeSubmit', 'custentity_acct_status(before) = ' + cust.getFieldValue('custentity_acct_status'));
			cust.setFieldValue('custentity_acct_status', 3);
			nlapiLogExecution('debug', 'userEventBeforeSubmit', 'custentity_acct_status(after) = ' + cust.getFieldValue('custentity_acct_status'));
		}
	}
	catch (e) {
		nlapiLogExecution('error', 'userEventBeforeSubmit', 'exception = ' + e);
	}
}

function onBeforeLoad(type){
    nlapiLogExecution('DEBUG', 'onBeforeLoad', 'type: ' + type);
}

function onAfterSubmit(type){
    nlapiLogExecution('DEBUG', 'onAfterSubmit', 'type: ' + type);
}

