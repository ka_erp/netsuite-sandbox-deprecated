function updateCustomer() {

var CustRecId = nlapiGetFieldValue('custentity_erp_hr_customer_link');

    if (!isNullOrEmpty(CustRecId)) {

        var custObj = nlapiLoadRecord('customer', CustRecId);
        nlapiLogExecution('debug', 'Employee Object', 'Should not be here');
        var emplyObj = {};
        var sub = 0;
        emplyObj.workhours = nlapiGetFieldText('custentity_erp_hr_work_hours');
        emplyObj.firstname = nlapiGetFieldValue('firstname');
        emplyObj.firstnameKA = "*KA*" + emplyObj.firstname;
        emplyObj.lastname = nlapiGetFieldValue('lastname');
        emplyObj.email = nlapiGetFieldValue('email');
        emplyObj.title = nlapiGetFieldValue('title');
        emplyObj.inactive = nlapiGetFieldValue('isinactive') == 'T' ? true : false; // disabled account
        emplyObj.released = nlapiGetFieldValue('releasedate') ? true : false; // employee is gone

        //Work hours changed
        if (emplyObj.workhours != custObj.getFieldValue('comments')) {
            custObj.setFieldValue('comments', emplyObj.workhours);
            sub++;
        }
        //check if the first name changed, also if the *KA* star is part of the customer record
        if (emplyObj.firstname != custObj.getFieldValue('firstname').replace('*KA*', '')) {
            custObj.setFieldValue('firstname', '*KA*' + emplyObj.firstname);
            sub++;
        } else if (emplyObj.firstnameKA != custObj.getFieldValue('firstname')) {
            custObj.setFieldValue('firstname', '*KA*' + emplyObj.firstname);
            sub++;
        }
        // if the last name is different for the customer record change to match the employee record
        if (emplyObj.lastname != custObj.getFieldValue('lastname')) {
            custObj.setFieldValue('lastname', emplyObj.lastname);
            sub++;
        }       // make sure that the job title is the same as the employee recod
        if (emplyObj.title != custObj.getFieldValue('title')) {
            custObj.setFieldValue('title', emplyObj.title);
            sub++;
        }

        //employee status changed
        if (emplyObj.inactive || emplyObj.released) {
            //Change firstname, work hours, email ( only change if it is @kitandace.com
            if (custObj.getFieldValue('email') == emplyObj.email) custObj.setFieldValue('email', ''); //if customer email is the same as the employee record then overwrite with Blank
            custObj.setFieldValue('firstname', emplyObj.firstname); // remove the *KA* from the customer record
            custObj.setFieldValue('comments', ''); //remove workhours
            custObj.setFieldValue('title', ''); //remove title

            nlapiLogExecution('debug', 'Changes', 'Employee was released or made inactive');
            sub++;
        }
        //only submit if there are changes
        if (sub > 0) {
            nlapiLogExecution('debug', 'Changes', JSON.stringify(custObj));
            nlapiSubmitRecord(custObj); // this should only be invoke if there are any changes
            nlapiLogExecution('debug', 'Submitted', sub);
        }

        return 1;
    }
        nlapiLogExecution('debug', 'Employee Object', 'No Customer found');
  return 0;
}


function isNullOrEmpty(valueStr) {
    return (valueStr == null || valueStr == "" || valueStr == undefined);
}
