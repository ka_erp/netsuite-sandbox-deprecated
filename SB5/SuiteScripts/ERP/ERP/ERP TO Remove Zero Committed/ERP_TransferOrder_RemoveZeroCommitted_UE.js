/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Oct 2015     casual
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function removeZero_userEventAfterSubmit(type){
  
        
         nlapiLogExecution("DEBUG", "Count", count);
	
	if(type != 'delete'){
		
		var TransferOrder = nlapiLoadRecord("transferorder", nlapiGetRecordId());

var status = TransferOrder.getFieldValue('orderstatus');  
nlapiLogExecution("DEBUG", "orderstatus", status );

if(status  == "B"){

 
 
		var count = TransferOrder.getLineItemCount('item');
		
		nlapiLogExecution("DEBUG", "Count", count);
		
		for(var i=1; i<= count; i++){
			
			
			
			var committed = TransferOrder.getLineItemValue('item', 'quantitycommitted', i);
			nlapiLogExecution("DEBUG", "Count "+ i, committed);
			
			if(committed == 0){
				nlapiLogExecution("DEBUG", "Count "+ i, 'removing this');
				TransferOrder.removeLineItem('item', i); i--; count--;
			}
			
		}
		
		 nlapiSubmitRecord(TransferOrder);
	 
	}
	
}

}
