/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Sep 2016     ivan.sioson
 *
 */

function runSetReasonCodes(){

//	var tranobj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
		
//	if(!isNullOrEmpty(tranobj)){
	
	
		var itemLines =  nlapiGetLineItemCount('item'); // tranobj.getLineItemCount('item');
		nlapiLogExecution('DEBUG','itemLines'   , itemLines);
		
		for(var i = 1  ; i <= itemLines ; i++){
			
			var linetype = nlapiGetLineItemValue('item', 'itemtype', i);
			
			
			
			if(linetype == 'Discount'){
				
				nlapiLogExecution('DEBUG','linetype_' + i  , linetype); 

				var linereturnreasoncode = nlapiGetLineItemValue('item', 'custcol_ra_return_reason', i);
				var linediscountreasoncode = nlapiGetLineItemValue('item', 'custcol_ra_discount_reason', i);
                var linediscountitem = nlapiGetLineItemValue('item', 'item', i);
				
				//tranobj.setLineItemValue(group, name, linenum, value)
				nlapiSetLineItemValue('item', 'custcol_ra_return_reason', i - 1, linereturnreasoncode);
				nlapiSetLineItemValue('item', 'custcol_ra_discount_reason', i - 1, linediscountreasoncode);
                nlapiSetLineItemValue('item', 'custcol_erp_discount_item', i - 1, linediscountitem);
				
				
				nlapiLogExecution('DEBUG','linetype_' + i  , linereturnreasoncode);
                nlapiLogExecution('DEBUG','linetype_' + i  , linediscountreasoncode);
                nlapiLogExecution('DEBUG','lineitem_' + i  , linediscountitem);
				
				
			}
			
		}
		
	}


//}



function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}