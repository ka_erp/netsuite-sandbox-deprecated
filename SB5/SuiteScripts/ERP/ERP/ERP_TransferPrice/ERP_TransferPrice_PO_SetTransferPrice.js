/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       23 Sep 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 * 
 */


function main_TransferPricePOSetTransferPrice_BS(type){
	
		var ordStatus = nlapiGetFieldValue('orderstatus'); 
		
		if(ordStatus == 'A' || ordStatus == 'B'){
		
			nlapiLogExecution("AUDIT","PURCHASE ORDER", nlapiGetRecordId());
		    nlapiLogExecution("AUDIT","PURCHASE ORDER FORM", nlapiGetFieldValue('customform'));
			
			var idCustomform = nlapiGetFieldValue('customform'); 
			
			if(idCustomform == 159){	//TODO: Make these parameters
			
					nlapiLogExecution("AUDIT","PURCHASE ORDER", nlapiGetRecordId());
								
					var bPOError = false;
					var idPriceLevel = 1;
					var idCurrency = nlapiGetFieldValue('currency');								
					var priceMethod = nlapiGetFieldValue('custbody_erp_price_method'); //1 - Standard , 2 - Base Price		
					
					
					if(priceMethod == 1){
						setItemMemo(); 
					}
					else if(priceMethod == 2){					
						
						if(nlapiGetContext().getExecutionContext() == 'userinterface'){
							setItemTransferPrice();						
						}																	
					}
			}
		}
}


function setItemMemo(){
	
	var bError = false; 
	var intGetPOlineCount = nlapiGetLineItemCount('item');
	
	var arrItemArray 			=		getPOItemArray(); 												//Get all the items
	var objItemPriceResults 	= 		getItemFCAResultObj(arrItemArray); 								//store the price details into an object

	
	if(!isNullOrEmpty(objItemPriceResults)){				
		
		var arrIdObjectArray 		= 	objItemPriceResults.ids;
		var arrResultObjectArray 	=   objItemPriceResults.objs;
		
		
		for(var i = 1; i <= intGetPOlineCount; i++){

			var idItemId = nlapiGetLineItemValue('item','item', i);
			
			var itemIndex = arrIdObjectArray.indexOf(idItemId);
			
			if(itemIndex >= 0){		
				
				var sourceRate 				=  	arrResultObjectArray[itemIndex].salesprice;
				var sourceCurrency 			=  	arrResultObjectArray[itemIndex].currencyTxt;
				
			}						
						
			
			var intQty = nlapiGetLineItemValue('item','quantity', i);
			var taxCode = nlapiGetLineItemValue('item','taxcode', i);
			var bOverride = nlapiGetLineItemValue('item','custcol_erp_transferprice_validation', i);
			var fRetailPrice = nlapiGetLineItemValue('item','custcol_erp_retail_price',i); 
			var fRetailValue  = nlapiGetLineItemValue('item','custcol_ka_po_retail_value',i); 
			var idRetailCurrency = nlapiGetLineItemValue('item','custcol_erp_posp_retail_currency',i); 
			
//			Retail Currency	 		2064	List/Record	Currency	 	 	Y	 	 	 	 	 	 	 	 	 	 	 	 	 	 
//			64	Retail Price	 	custcol_erp_retail_price	2602	Currency	 	 	 	 	 	 	 	 	 	 	Y	 	 	 	 	 	 	 
//			10	Retail Value	 	custcol_ka_po_retail_value
			
			
			 
/*
var currentContext = nlapiGetContext();   																			//FIX
if( (currentContext.getExecutionContext() != 'csvimport'))															//FIX
{
			nlapiRemoveLineItem('item', i);		
			nlapiInsertLineItem('item', i);
			
			
			
			nlapiSetLineItemValue('item','item', i, idItemId);
			nlapiSetLineItemValue('item','quantity', i, intQty);
			nlapiSetLineItemValue('item','taxcode', i, taxCode);
			nlapiSetLineItemValue('item','custcol_erp_retail_price', i, fRetailPrice);
			nlapiSetLineItemValue('item','custcol_ka_po_retail_value', i, fRetailValue);
			nlapiSetLineItemValue('item','custcol_erp_posp_retail_currency', i, idRetailCurrency);
			nlapiSetLineItemValue('item','custcol_erp_transferprice_validation', i, bOverride);
	
}																										//FIX							
*/						


			var memo = 'Calculation: \n Standard Pricing is used. \n Last FCA:' + sourceRate + ' ' + sourceCurrency + '\nSee Item - Vendor Price'; 
			
			nlapiSetLineItemValue('item','custcol_erp_to_memo', i, memo);
			
		}			
	}	
	 
}

function setItemTransferPrice(){
	
	//function is using FCA calculation from the item record
	
	try{
	
		var arrItemArray 			=		getPOItemArray(); 												//Get all the items
		var objItemPriceResults 	= 		getItemFCAResultObj(arrItemArray); 								//store the price details into an object
		var intGetPOlineCount 		= 		nlapiGetLineItemCount('item');			
		
		var idVendor 				= 		nlapiGetFieldValue('entity');		
		var strVendorRetailFormula 	= 		nlapiLookupField('vendor', idVendor, 'custentity_intercompany_retail_percent'); 								
		strVendorRetailFormula  	= 		eval(strVendorRetailFormula); 
		
		
		var idCurrencyPO 			= 		nlapiGetFieldValue('currency');									//This is actually PO Currency
		var strCurrencyPOTxt 		= 		nlapiGetFieldText('currency');	
		var objSourceCurrency		= 		new  Object();
		objSourceCurrency.id		=		idCurrencyPO;
		objSourceCurrency.name		= 		strCurrencyPOTxt;
		
		
		if(!isNullOrEmpty(objItemPriceResults)){				
						
			var arrIdObjectArray 		= 	objItemPriceResults.ids;
			var arrResultObjectArray 	=   objItemPriceResults.objs;
			var arrCurrencyObjectArray 	=   objItemPriceResults.curr;
			
			var currencyObject		= 		createCurrencyExchange(objSourceCurrency, arrCurrencyObjectArray);
			
			
			nlapiLogExecution('DEBUG', 'currencyObject', JSON.stringify(currencyObject)); 
			
			var bError = false; 
			var intGetPOlineCount = nlapiGetLineItemCount('item');
	
			for(var i = 1; i <= intGetPOlineCount; i++){
				
				var idItemId = nlapiGetLineItemValue('item','item', i);				
				var itemIndex = arrIdObjectArray.indexOf(idItemId);
				
				if(itemIndex >= 0){		
					
					var sourceRate 				=  	arrResultObjectArray[itemIndex].salesprice;
					var sourceCurrency 			=  	arrResultObjectArray[itemIndex].currency;
					
					
					nlapiLogExecution('AUDIT', 'PO Rate', sourceCurrency + ":" +  idCurrencyPO);
					
					
					
					var rateObj 				=	getRate(sourceRate, sourceCurrency, idCurrencyPO, currencyObject, strVendorRetailFormula);	 																																
					
					var rate = rateObj.rate;
					var memo = rateObj.memo;
					
					nlapiSetLineItemValue('item','rate', i, rate);
					nlapiSetLineItemValue('item','custcol_erp_to_memo', i, memo);
					
//					var rate = convertCurrency(fFCA, idFCACurrency, currencyObject);
//					
//					
//					
//					
//					var newrate = calculateRate(fFCA, idFCACurrency, idVendorCurrency, strVendorRetailFormula); 					
//					var memo = getRetailMemo(fFCA, '', newrate[0], '', strVendorRetailFormula, newrate[1]);
					
					
					
//										
//					
					
												
				}					
			}				
		}	
	
	}catch(ex){
		
		nlapiLogExecution('ERROR', 'func:setItemTransferPrice', ex.toString());
	}
}


