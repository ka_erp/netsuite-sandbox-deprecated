/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 Jan 2016     ivan.sioson
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function erp_sdc_SL(request, response){

	//var transID = request.getParameter( 'transactionRecord' );
	var transDate1 = request.getParameter( 'trandate1' );
	var transDate2 = request.getParameter( 'trandate2' );
	var location = request.getParameter( 'location' );
	//var dailyActual = request.getParameter( 'dailyactual' );
	
			
	try{
		
		var returnObj = new Object(); 
		
		var shopClosingObj = getShopClosing(location, transDate1, transDate2); 		
		var shopUPTsObj = getUPTSPT(location, transDate1, transDate2); 		
		var shopPOSDisc =getSalesPOSDiscount(location, transDate1, transDate2); 
		var shopPeakTimes = getPeakTimes(location, transDate1, transDate2); 
		
		returnObj.closing = shopClosingObj;
		returnObj.upt = shopUPTsObj;
		returnObj.posdiscount = shopPOSDisc;
		returnObj.peaktime = shopPeakTimes; 
		
		response.write(JSON.stringify(returnObj)); 
		
		
		
	}catch(ex){		
		nlapiLogExecution('DEBUG', '>>>> erp_sdc_SL: Error', ex);
	}	
}

function getUPTSPT(locId, date1, date2) {

	var salesArrayIds = new Array();
	var salesArrayObj = new Array();

	var salesObject = new Object();

	var strSavedSearchIDUPTSPT = null;
	var arSaveSearchFiltersUPTSPT = new Array();
	var arSavedSearchColumnsUPTSPT = new Array();
	// var arSavedSearchResultsUPTSPT = null;

	arSaveSearchFiltersUPTSPT.push(new nlobjSearchFilter('location', null, 'anyof', locId));
	arSaveSearchFiltersUPTSPT.push(new nlobjSearchFilter('datecreated', null, 'within', [ date1, date2 ]));

	var arSavedSearchResultsUPTSPT = nlapiLoadSearch('transaction',	'customsearch_erp_closing_upt_v20');

	// arSavedSearchResultsUPTSPT.addColumns(null);
	arSavedSearchResultsUPTSPT.addFilters(arSaveSearchFiltersUPTSPT);

	var resultSet = arSavedSearchResultsUPTSPT.runSearch();

	try {

		var results = resultSet.getResults(0, 1000);

		if (!isNullOrEmpty(results)) {

			var idCurrentItem = results[0].getValue('internalid');
			var columns = results[0].getAllColumns();
			var objResult = results[0];

			var objUPTSPT = new Object();

			objUPTSPT.location = objResult.getValue(columns[0]);
			objUPTSPT.upt = objResult.getValue(columns[1]);
			objUPTSPT.spt = objResult.getValue(columns[2]);
			objUPTSPT.aur = objResult.getValue(columns[3]);

			var upt = objResult.getValue(columns[1]);
			upt = Number(upt).toFixed(1);
			
			var spt = objResult.getValue(columns[2]);
			spt = Number(spt).toFixed(0);

			var aur = objResult.getValue(columns[3]);			
			aur = Number(aur).toFixed(0);

//			nlapiSetFieldValue('custrecord_erp_dclose_upt_units_pertrans', upt);
//			nlapiSetFieldValue('custrecord_erp_dclose_sptdpt', spt);
//			nlapiSetFieldValue('custrecorderp_dclose_aur', aur);
			
			return objUPTSPT;
		}
	} catch (ex) {
		return null;
	}

}


function getShopClosing(locId, date1, date2) {

	var salesArrayIds = new Array();
	var salesArrayObj = new Array();

	var salesObject = new Object();

	var strSavedSearchIDShopClosing = null;
	var arSaveSearchFiltersShopClosing = new Array();
	var arSavedSearchColumnsShopClosing = new Array();

	arSaveSearchFiltersShopClosing.push(new nlobjSearchFilter('location', null,
			'anyof', locId));
	arSaveSearchFiltersShopClosing.push(new nlobjSearchFilter('datecreated',
			null, 'within', [ date1, date2 ]));

	var arSavedSearchResultsShopClosing = nlapiLoadSearch('transaction',
			'customsearch_erp_script_cn_closing');
	arSavedSearchResultsShopClosing.addFilters(arSaveSearchFiltersShopClosing);

	var resultSet = arSavedSearchResultsShopClosing.runSearch();

	try {

		var results = resultSet.getResults(0, 1000);

		if (!isNullOrEmpty(results)) {

			var idCurrentItem = results[0].getValue('internalid');
			var columns = results[0].getAllColumns();
			var objResult = results[0];

			var objClosingSales = new Object();

			objClosingSales.location = objResult.getValue(columns[0]);
			objClosingSales.closingsales = objResult.getValue(columns[2]); // 1 is Amount , 2 is Amount FX

			nlapiSetFieldValue('custrecord_erp_dclose_netsuite_closing',
					objClosingSales.closingsales);

			return objClosingSales;

		}

	} catch (ex) {
		return null;
	}

}


function getSalesPOSDiscount(locId, date1, date2){

	// Order needs to be conforming to :
	// Type + Account + Location + Amount + Currency
	
	var salesArrayIds = new Array();
	var salesArrayObj = new Array();
	var salesObject = new Object();
	
	var strSavedSearchIDSalesRange = null;
	var arSaveSearchFiltersSalesRange = new Array();
	var arSavedSearchColumnsSalesRange = new Array();
	var arSavedSearchResultsSalesRange = null;
	
	arSaveSearchFiltersSalesRange.push(new nlobjSearchFilter('location', null,
			'anyof', locId));
	arSaveSearchFiltersSalesRange.push(new nlobjSearchFilter('datecreated',
			null, 'within', [ date1, date2 ]));
	
	var arSavedSearchResultsSalesRange = nlapiLoadSearch('transaction',
			'customsearch1342');
	
	arSavedSearchResultsSalesRange.addColumns(arSavedSearchColumnsSalesRange);
	
	
	arSavedSearchResultsSalesRange.addFilters(arSaveSearchFiltersSalesRange);
	
	var resultSet = arSavedSearchResultsSalesRange.runSearch();
	
	if (!isNullOrEmpty(resultSet)) {
	
		try {
	
			var results = resultSet.getResults(0, 1000);
			// var objDateRange = new Object();
	
			nlapiLogExecution('Audit', 'Result JSON POS Discount', JSON
					.stringify(results));
	
			if (results) {
	
				var idCurrentItem = results[0].getValue('internalid');
				var columns = results[0].getAllColumns();
				var objResult = results[0];
	
				var objVendor = new Object();
	
				objVendor.type = objResult.getValue(columns[0]);
				objVendor.fd = objResult.getValue(columns[1]); // f&d
				objVendor.ed = objResult.getValue(columns[2]); // employee
																// discount
				objVendor.other = objResult.getValue(columns[3]);
	
				nlapiLogExecution('Audit', 'Result JSON POS Discount OBj', JSON
						.stringify(objVendor));
	
//	
//				nlapiSetFieldValue('custrecord_erp_dclose_fd',
//						Math.abs(objVendor.fd) );
//	
//				nlapiSetFieldValue('custrecord_erp_dclose_employee_discounts',
//						 Math.abs(objVendor.ed) );	
	
				return objVendor;
	
			}
		} catch (ex) {
	
			nlapiLogExecution('Audit', 'getSalesPOSDiscount', ex.toString());
		}
	} else {
		return null;
	}

}



function getPeakTimes(locId, date1, date2) {

	// date1 = '12/01/2015';
	// date2 = '12/01/2015';
	// nlapiLogExecution('DEBUG', 'eff date1', date1);
	// nlapiLogExecution('DEBUG', 'eff date2', date2);
	// nlapiLogExecution('DEBUG', 'eff locId', locId);

	var salesArrayIds = new Array();
	var salesArrayObj = new Array();

	var salesObject = new Object();

	var strSavedSearchIDPeakHours = null;
	var arSaveSearchFiltersPeakHours = new Array();
	var arSavedSearchColumnsPeakHours = new Array();

	arSavedSearchColumnsPeakHours.push(new nlobjSearchColumn('tranid', null,
			'count').setSort(true));

	arSaveSearchFiltersPeakHours.push(new nlobjSearchFilter('location', null,
			'anyof', locId));
	arSaveSearchFiltersPeakHours.push(new nlobjSearchFilter('datecreated',
			null, 'within', [ date1, date2 ]));

	var arSavedSearchResultsPeakHours = nlapiLoadSearch('transaction',
			'customsearch1190_2');
	arSavedSearchResultsPeakHours.addFilters(arSaveSearchFiltersPeakHours);
	arSavedSearchResultsPeakHours.addColumns(arSavedSearchColumnsPeakHours);

	var resultSet = arSavedSearchResultsPeakHours.runSearch();

	try {

		var results = resultSet.getResults(0, 50);

		if (!isNullOrEmpty(results)) {

			nlapiLogExecution('DEBUG', 'eff result', results.length);

			var peakHours = new Array();
			var peakHoursStr = '';
			var peakCount = 1;


			for (var i = 0; i < peakCount; i++) {

				var idCurrentItem = results[i].getValue('internalid');
				var columns = results[i].getAllColumns();
				var objResult = results[i];

				var objClosingSales = new Object();
				objClosingSales.location = objResult.getValue(columns[0]);
				objClosingSales.time = objResult.getValue(columns[1]);
				objClosingSales.count = objResult.getValue(columns[2]);
				objClosingSales.qty = objResult.getValue(columns[3]);
				//
				objClosingSales.time = moment(objClosingSales.time, 'hmm')
						.format('h:mm a');

				peakHours.push(objClosingSales);
				peakHoursStr = peakHoursStr + objClosingSales.time + "\n";

				// //console.log("setPeakTimes" +
				// JSON.stringify(objClosingSales));
			}

			// //console.log("setPeakTimes array" + );
			nlapiLogExecution('DEBUG', 'eff peakHourspeakHours', JSON.stringify(peakHours));
//			nlapiSetFieldValue('custrecord_erp_dclose_peak_times', peakHoursStr);

			return peakHoursStr;

		}

	} catch (ex) {
		return null;
	}

}

function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}