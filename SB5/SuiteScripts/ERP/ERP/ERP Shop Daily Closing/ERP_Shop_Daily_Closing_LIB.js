/**
 * Module Description
 *
 * Version Date Author Remarks 1.00 20 Jan 2016 ivan.sioson
 *
 */

function getAccountPeriodSL(){

	var inputDate = nlapiGetFieldValue('custrecord_erp_date_created');
	var formatDate = moment(inputDate,'MM/DD/YYYY');
	var slResponse = null;

	var todayDate = formatDate.format('L');

	if(!isNullOrEmpty(todayDate)){

		var url = nlapiResolveURL('SUITELET',
			'customscript_erp_shop_daily_closing_sl',
			'customdeploy_erp_shop_daily_closing_sl', true);

		var params = new Array();
		params['transactionDate'] = todayDate;

		 slResponse = nlapiRequestURL(url, params);

	}

	return slResponse;
}



function recalcClosingNotes() {

	// console.log('recalculating custrecord_erp_date_created');

	//ESTABLISH LOCATION AND THE DATE TODAY
	var idLocation = nlapiGetFieldValue('custrecord_erp_dclose_shop_name');
	var inputDate = nlapiGetFieldValue('custrecord_erp_date_created');
	var formatDate = moment(inputDate, 'MM/DD/YYYY');
	var todayDate = formatDate.format('L');


	//REPOPULATE LOCATION SOURCED FIELDS
	var recLocation = nlapiLoadRecord('location', idLocation);
	setMainShopClosing(recLocation);

	//ESTABISH THE DATE RANGE
	var testObject = JSON.parse(getAccountPeriodSL().body);
	var dateRange = testObject;

	var weekStart = formatDate.startOf('isoweek').format('L');
	var weekEnd = formatDate.endOf('isoweek').format('L');

	if(!isNullOrEmpty(dateRange)){

		nlapiSetFieldValue('custrecord_day_daterange', todayDate);
		nlapiSetFieldValue('custrecord_week_daterange', weekStart + ' - ' + weekEnd);
		nlapiSetFieldValue('custrecord_period_daterange', dateRange.period.start + "  - " + dateRange.period.end);
		nlapiSetFieldValue('custrecord_quarter_daterange', dateRange.quarter.start + "  - " + dateRange.quarter.end);

	}
	setSalesInfo(idLocation, "daily", todayDate, todayDate);
	setSalesInfo(idLocation, "weekly", weekStart, todayDate);				//weekEnd);
	setSalesInfo(idLocation, "period", dateRange.period.start, todayDate);	//dateRange.period.end);
	setSalesInfo(idLocation, "qtr", dateRange.quarter.start, todayDate);	//dateRange.quarter.end);




	var salesClosing = getShopClosing(idLocation, todayDate, todayDate);
	var uptspt = setUPTSPT(idLocation, todayDate, todayDate);
	var peakTimes = setPeakTimes(idLocation, todayDate, todayDate);
	setSalesPOSDiscount(idLocation, todayDate, todayDate);
	jQuery( ".progress" ).progressbar({value: 100});
}


function setMainShopClosing(recLocation){

	var locId = recLocation.getId();
	var locShopDirector = recLocation.getFieldValue('custrecord_ka_shop_director');
	var locDistrictDirector = recLocation.getFieldValue('custrecord_erp_dclose_loc_d_director'); //recLocation.getFieldValue('custrecord_ka_district_director');
	var locEmailRecipient = recLocation.getFieldValue('custrecord_erp_dclose_loc_emailuser'); //recLocation.getFieldValue('custrecord_ka_district_director');
	var locMarketDirector = recLocation.getFieldValue('custrecord_erp_dclose_loc_m_director'); 		//recLocation.getFieldValue('custrecord_ka_market_director');
	var locOpening = recLocation.getFieldValue('custrecord_ka_shop_director'); //recLocation.getFieldValue('custrecord_ka_opening_lead');
	var locClosing = recLocation.getFieldValue('custrecord_ka_shop_director'); //recLocation.getFieldValue('custrecord_ka_closing_lead');
	var locCurrencyRA = recLocation.getFieldValue('custrecord_ra_loc_currency');
	var locEmailRecipient = recLocation.getFieldValue('custrecord_erp_dclose_loc_emailuser');


	var locRetailType = recLocation.getFieldValue('custrecord_ka_retail_shop_type'); //'custrecord_ra_loctype'); //

	//nlapiSetFieldValue('custrecord_erp_dclose_shop_name', locId);
	nlapiSetFieldValue('custrecord_erp_dclose_shop_director', locShopDirector);
	//nlapiSetFieldValue('custrecord_erp_dclose_shop_director', locShopDirector);

	if(!isNullOrEmpty(locMarketDirector)){
	nlapiSetFieldValue('custrecord_erp_dclose_market_director', locMarketDirector);
	}

	if(!isNullOrEmpty(locDistrictDirector)){
	nlapiSetFieldValue('custrecord_erp_dclose_district_director', locDistrictDirector);
	}


        if(!isNullOrEmpty(locOpening ) && isNullOrEmpty(nlapiGetFieldValue('custrecord_erp_dclose_opening_lead'))){
	nlapiSetFieldValue('custrecord_erp_dclose_opening_lead', locOpening );
        }

        if(!isNullOrEmpty(locOpening ) && isNullOrEmpty(nlapiGetFieldValue('custrecord_erp_dclose_closing_lead'))){
	nlapiSetFieldValue('custrecord_erp_dclose_closing_lead', locClosing );
        }

	nlapiSetFieldValue('custrecord_erp_dclose_shop_type', locRetailType );
	nlapiSetFieldValue('custrecord_erp_dclose_loc_emailuser', locEmailRecipient );



}


//function recalcClosingNotes2() {
//
//	// //console.log('recalculating recalcClosingNotes');
//
//	// Get and Set Location Field Retrievals
//	var idLocation = nlapiGetFieldValue('custrecord_erp_dclose_shop_name');
//	var recLocation = nlapiLoadRecord('location', idLocation);
//	setMainShopClosing(recLocation);
//
//	// Get and Set Date Field Retrievals
//
//	// Get the date
//	var inputDate = nlapiGetFieldValue('custrecord_erp_date_created');
//	var formatDate = moment(inputDate, 'MM/DD/YYYY');
//	var todayDate = formatDate.format('L'); // //console.log(todayDate);
//
//	// Getting this Week
//	var weekStart = formatDate.startOf('isoweek').format('L');
//	var weekEnd = formatDate.endOf('isoweek').format('L');
//
//	// Getting the Period and the Quarter
//	var testObject = JSON.parse(getAccountPeriodSL().body);
//	var dateRange = testObject; // getPeriodDateRanges(todayDate);
//
//	// Reloading Information
//
//	var salesClosing = getShopClosing(idLocation, todayDate, todayDate);
//	var uptspt = getUPTSPT(idLocation, todayDate, todayDate);
//	var peakTimes = setPeakTimes(idLocation, todayDate, todayDate);
//
//	// //console.log('uptspt ' + JSON.stringify(uptspt));
//	// //console.log('salesClosing' + JSON.stringify(salesClosing));
//
//	//
//
//	nlapiSetFieldValue('custrecord_day_daterange', todayDate);
//	nlapiSetFieldValue('custrecord_week_daterange', weekStart + ' - ' + weekEnd);
//	nlapiSetFieldValue('custrecord_period_daterange', dateRange.period.start
//			+ "  - " + dateRange.period.end);
//	nlapiSetFieldValue('custrecord_quarter_daterange', dateRange.quarter.start
//			+ "  - " + dateRange.quarter.end);
//
//	setSalesInfo(idLocation, "daily", todayDate, todayDate);
//	setSalesInfo(idLocation, "weekly", weekStart, weekEnd);
//	setSalesInfo(idLocation, "period", dateRange.period.start,
//			dateRange.period.end);
//	setSalesInfo(idLocation, "qtr", dateRange.quarter.start,
//			dateRange.quarter.end);
//
//}



function recalcSalesVSLY(mode){


	switch (mode) {

	case 'daily':
		fieldToGoal = 'custrecord_erp_dclose_daily_togoal';
		fieldPercentToGoal = 'custrecord_erp_dclose_daily_salescomp';
		break;

	case 'weekly':
		fieldToGoal = 'custrecord_erp_dclose_weekly_togoal';
		fieldPercentToGoal = 'custrecord_erp_dclose_weekly_salescomp';
		break;

	case 'period':
		fieldToGoal = 'custrecord_erp_dclose_period_togoal';
		fieldPercentToGoal = 'custrecord_erp_dclose_period_salescomp';
		break;

	case 'qtr':
		fieldToGoal = 'custrecord_erp_dclose_qtr_to_goal';
		fieldPercentToGoal = 'custrecord_erp_dclose_qtr_salescomp';
		break;

	default:

	}

}

function recalcActualVSGoals(mode) {

	// Field Names
	var fieldToGoal = '';
	var fieldPercentToGoal = '';

	var modeGoal = nlapiGetFieldValue('custrecord_erp_dclose_' + mode + '_goal');
	var modeActual = nlapiGetFieldValue('custrecord_erp_dclose_' + mode + '_actual');

	var diff = modeActual - modeGoal;
	var perc = 0;

	// console.log(modeGoal);

	if (!isNullOrEmpty(modeGoal) && (modeGoal > 0)) {
		perc = (modeActual / modeGoal) * 100;
		perc = Math.round(perc);

	}

	switch (mode) {

	case 'daily':
		fieldToGoal = 'custrecord_erp_dclose_daily_togoal';
		fieldPercentToGoal = 'custrecord_erp_dclose_daily_ptogoal';
		break;

	case 'weekly':
		fieldToGoal = 'custrecord_erp_dclose_weekly_togoal';
		fieldPercentToGoal = 'custrecord_erp_dclose_weekly_ptogoal';
		break;

	case 'period':
		fieldToGoal = 'custrecord_erp_dclose_period_togoal';
		fieldPercentToGoal = 'custrecord_erp_dclose_period_ptogoal';
		break;

	case 'qtr':
		fieldToGoal = 'custrecord_erp_dclose_qtr_to_goal';
		fieldPercentToGoal = 'custrecord_erp_dclose_qtr_perc_to_goal';
		break;

	default:

	}

	nlapiSetFieldValue(fieldToGoal, diff);
	nlapiSetFieldValue(fieldPercentToGoal, perc);

}

function setSalesPOSDiscount(locId, date1, date2) {

	// Order needs to be conforming to :
	// Type + Account + Location + Amount + Currency

	var salesArrayIds = new Array();
	var salesArrayObj = new Array();
	var salesObject = new Object();

	var strSavedSearchIDSalesRange = null;
	var arSaveSearchFiltersSalesRange = new Array();
	var arSavedSearchColumnsSalesRange = new Array();
	var arSavedSearchResultsSalesRange = null;

	arSaveSearchFiltersSalesRange.push(new nlobjSearchFilter('location', null,
			'anyof', locId));
	arSaveSearchFiltersSalesRange.push(new nlobjSearchFilter('datecreated',
			null, 'within', [ date1, date2 ]));

	var arSavedSearchResultsSalesRange = nlapiLoadSearch('transaction',
			'customsearch1342');

	arSavedSearchResultsSalesRange.addColumns(arSavedSearchColumnsSalesRange);


	arSavedSearchResultsSalesRange.addFilters(arSaveSearchFiltersSalesRange);

	var resultSet = arSavedSearchResultsSalesRange.runSearch();

	if (!isNullOrEmpty(resultSet)) {

		try {

			var results = resultSet.getResults(0, 1000);
			// var objDateRange = new Object();

			nlapiLogExecution('Audit', 'Result JSON POS Discount', JSON
					.stringify(results));

			if (results) {

				var idCurrentItem = results[0].getValue('internalid');
				var columns = results[0].getAllColumns();
				var objResult = results[0];

				var objVendor = new Object();

				objVendor.type = objResult.getValue(columns[0]);
				objVendor.fd = objResult.getValue(columns[1]); // f&d
				objVendor.ed = objResult.getValue(columns[2]); // employee
																// discount
				objVendor.other = objResult.getValue(columns[3]);

				nlapiLogExecution('Audit', 'Result JSON POS Discount OBj', JSON
						.stringify(objVendor));


				nlapiSetFieldValue('custrecord_erp_dclose_fd',
						Math.abs(objVendor.fd) );

				nlapiSetFieldValue('custrecord_erp_dclose_employee_discounts',
						 Math.abs(objVendor.ed) );



				return objVendor;

			}
		} catch (ex) {

			nlapiLogExecution('Audit', 'getSalesPOSDiscount', ex.toString());
		}
	} else {
		return null;
	}

}

function getSalesRangeSL(locId, mode, date1, date2) {

	var url = nlapiResolveURL('SUITELET',
			'customscript_erp_shop_daily_closing_sl',
			'customdeploy_erp_shop_daily_closing_sl', true);


	if(mode == 'daily' ||
	   mode == 'weekly'||
	   mode == 'period'||
	   mode == 'qtr' ){

	   mode = 'salesinfo';

	}else if('dailycredit'){

	   mode = 'creditinfo';

	}else{ mode = ''; }


	var params = new Array();
	params['locationId'] = locId;
	params['transactionDate1'] = date1;
	params['transactionDate2'] = date2;
	params['mode'] = mode; //'salesinfo';

	var slResponse = nlapiRequestURL(url, params);

	return slResponse;
}

function getSalesRange(locId, date1, date2) {

	// Order needs to be conforming to :
	// Type + Account + Location + Amount + Currency

	var salesArrayIds = new Array();
	var salesArrayObj = new Array();
	var salesObject = new Object();

	var strSavedSearchIDSalesRange = null;
	var arSaveSearchFiltersSalesRange = new Array();
	var arSavedSearchColumnsSalesRange = new Array();
	var arSavedSearchResultsSalesRange = null;

	arSaveSearchFiltersSalesRange.push(new nlobjSearchFilter('location', null,
			'anyof', locId));
	arSaveSearchFiltersSalesRange.push(new nlobjSearchFilter('datecreated',
			null, 'within', [ date1, date2 ]));

	var arSavedSearchResultsSalesRange = nlapiLoadSearch('transaction',
			'customsearch990');

	arSavedSearchResultsSalesRange.addColumns(arSavedSearchColumnsSalesRange);
	arSavedSearchResultsSalesRange.addFilters(arSaveSearchFiltersSalesRange);

	var resultSet = arSavedSearchResultsSalesRange.runSearch();

	if (!isNullOrEmpty(resultSet)) {

		try {
			var results = resultSet.getResults(0, 1000);

			if (!isNullOrEmpty(results)) {

				nlapiLogExecution('DEBUG', 'Result JSON', JSON
						.stringify(results));

				for (var i = 0; i < results.length; i++) {

					var idCurrentItem = results[i].getValue('internalid');
					var columns = results[i].getAllColumns();
					var objResult = results[i];

					var objVendor = new Object();

					objVendor.type = objResult.getValue(columns[0]);
					objVendor.accountid = objResult.getValue(columns[1]);
					objVendor.location = objResult.getValue(columns[2]);
					objVendor.netsales_fx = objResult.getValue(columns[6]);
					objVendor.netsales_fx = Number(
							Math.abs(objVendor.netsales_fx)).toFixed(0);
					objVendor.estCost = objResult.getValue(columns[4]);
					objVendor.currency = objResult.getValue(columns[5]);

					nlapiLogExecution('DEBUG', 'objVendor', JSON
							.stringify(objVendor));

					salesArrayIds.push(objVendor.type + objVendor.accountid);
					salesArrayObj.push(objVendor);

				}

				salesObject.ids = salesArrayIds;
				salesObject.obj = salesArrayObj;

			}

			return salesObject;

		} catch (ex) {
			return null;
		}

	} else {
		return null;
	}
}

function resetAllFields() {
// if the goal boxes are already set do not reset the value
if(!nlapiGetFieldValue('custrecord_erp_dclose_daily_goal')){nlapiSetFieldValue('custrecord_erp_dclose_daily_goal', '');}
if(!nlapiGetFieldValue('custrecord_erp_dclose_weekly_goal')){nlapiSetFieldValue('custrecord_erp_dclose_weekly_goal', '');}
if(!nlapiGetFieldValue('custrecord_erp_dclose_period_goal')) {nlapiSetFieldValue('custrecord_erp_dclose_period_goal', '');}
if(!nlapiGetFieldValue('custrecord_erp_dclose_qtr_goal')) {nlapiSetFieldValue('custrecord_erp_dclose_qtr_goal', '');}

if(!nlapiGetFieldValue('custrecord_erp_dclose_actual_hours')) {nlapiSetFieldValue('custrecord_erp_dclose_actual_hours', '');}
if(!nlapiGetFieldValue('custrecord_erp_dclose_saleslabhour')) {nlapiSetFieldValue('custrecord_erp_dclose_saleslabhour', '');}



//	nlapiSetFieldValue('custrecord_erp_dclose_saleslabhour', '');
	nlapiSetFieldValue('custrecord_quarter_daterange', '');


	nlapiSetFieldValue('custrecord_erp_dclose_upt_units_pertrans', '');
	nlapiSetFieldValue('custrecord_erp_dclose_sptdpt', '');
	nlapiSetFieldValue('custrecord_erp_dclose_netsuite_closing', '');
	nlapiSetFieldValue('custrecord_erp_dclose_peak_times', '');
	nlapiSetFieldValue('custrecord_erp_dclose_variance', '');
	nlapiSetFieldValue('custrecord_erp_dclose_employee_discounts', '');
	nlapiSetFieldValue('custrecord_erp_dclose_fd', '');
	nlapiSetFieldValue('custrecord_erp_dclose_returns', '');
	nlapiSetFieldValue('custrecord_erp_dclose_est_margin', '');
	nlapiSetFieldValue('custrecord_erp_dclose_daily_togoal', '');
	nlapiSetFieldValue('custrecord_day_daterange', '');
	nlapiSetFieldValue('custrecord_week_daterange', '');
	nlapiSetFieldValue('custrecord_period_daterange', '');
	nlapiSetFieldValue('custrecord_quarter_daterange', '');
	nlapiSetFieldValue('custrecord_erp_dclose_daily_actual', '');
	nlapiSetFieldValue('custrecord_erp_dclose_weekly_actual', '');
	nlapiSetFieldValue('custrecord_erp_dclose_period_actual', '');
	nlapiSetFieldValue('custrecord_erp_dclose_qtr_actual', '');
	nlapiSetFieldValue('custrecord_erp_dclose_daily_ptogoal', '');
	nlapiSetFieldValue('custrecord_erp_dclose_weekly_ptogoal', '');
	nlapiSetFieldValue('custrecord_erp_dclose_period_ptogoal', '');
	nlapiSetFieldValue('custrecord_erp_dclose_qtr_to_goal', '');
	nlapiSetFieldValue('custrecord_erp_dclose_weekly_togoal', '');
	nlapiSetFieldValue('custrecord_erp_dclose_period_togoal', '');
	nlapiSetFieldValue('custrecord_erp_dclose_qtr_perc_to_goal', '');
	nlapiSetFieldValue('custrecord_erp_dclose_period_togoal', '');





}

function setSalesInfo(locId, modeTxt, date1, date2) {

	try {

		//console.log(date1 + "  " + date2);

		if(!isNullOrEmpty(date1) &&  !isNullOrEmpty(date2)){

			var salesObj = JSON.parse(getSalesRangeSL(locId, modeTxt, date1, date2).body);
			console.log('json : ' + modeTxt + " \n"+ JSON.stringify(salesObj));

			if (!isNullOrEmpty(salesObj)) {

				var salesArrIds = new Array();
				var salesArrObj = new Array();

				salesArrIds = salesObj.ids;
				salesArrObj = salesObj.obj;

				if (!isNullOrEmpty(salesObj)) {

					var salesIndex = salesArrIds.indexOf("213213");
					if (salesIndex > -1) {
						var obj = salesArrObj[salesIndex];
						var netSales = obj.netsales_fx;

						var currency = salesArrObj[salesIndex].currency;
						nlapiSetFieldValue('custrecord_erp_dclose_currency', currency);

						nlapiSetFieldValue('custrecord_erp_dclose_' + modeTxt + '_actual', netSales);
					}

				}



			//Stuff to get the credit memo


			if(modeTxt == 'daily'){

				var creditObject = JSON.parse(getSalesRangeSL(locId, 'dailycredit', date1, date2).body);
				console.log("creditObject json : \n"+ JSON.stringify(creditObject));

				if (!isNullOrEmpty(creditObject)) {

					var salesArrIds = new Array();
					var salesArrObj = new Array();

					salesArrIds = creditObject.ids;
					salesArrObj = creditObject.obj;

					if (!isNullOrEmpty(creditObject)) {

						var salesIndex = salesArrIds.indexOf("213213");
						if (salesIndex > -1) {
							var obj = salesArrObj[salesIndex];
							var netSales = obj.netsales_fx;

							nlapiSetFieldValue('custrecord_erp_dclose_returns', netSales);
						}

					}


				}
			}











//
//				salesArrIds = salesObj.ids;
//				salesArrObj = salesObj.obj;
//
//				if (!isNullOrEmpty(salesArrIds)) {
//
//					// Get the Customer Invoice from the POS
//					var salesIndex = salesArrIds.indexOf("213213");  //CustInvc213");
//					var netSales = 0;
//
//					if (salesIndex > -1) {
//						var obj = salesArrObj[salesIndex];
//						netSales = obj.netsales_fx;
//						// nlapiSetFieldValue('custrecord_erp_dclose_'+ modeTxt
//						// +'_actual', obj.netsales_fx );
//
//						var currency = salesArrObj[salesIndex].currency;
//						nlapiSetFieldValue('custrecord_erp_dclose_currency',
//								currency);
//					}
//
//					// Get the Customer Credit Meo from the POS
//					var creditIndex = salesArrIds.indexOf("CustCred213");
//					//console.log("creditIndex " + JSON.stringify(creditIndex));
//
//					var netcredit = 0;
//
//					if (creditIndex > -1) {
//
//						var obj = salesArrObj[creditIndex];
//						netcredit = obj.netsales_fx;
//						// nlapiSetFieldValue('custrecord_erp_dclose_'+ modeTxt
//						// +'_actual', obj.netcredit_fx );
//
//					}
//
//					//console.log("netSales " + netSales);
//					//console.log("netcredit " + netcredit);
//
//					var netoSales = netSales - netcredit;
//
//					console.log('custrecord_erp_dclose_' + modeTxt + '_actual' + netoSales);
//					nlapiSetFieldValue('custrecord_erp_dclose_' + modeTxt + '_actual', netoSales);
//
//					if (modeTxt == 'daily') {
//
//						try {
//							// Est Margin
//							var estCost = salesArrObj[salesIndex].estCost;
//							var estMargin = ((netSales - estCost) / netSales) * 100;
//
//							estMargin = Math.round(estMargin, 0);
//
//							nlapiSetFieldValue(
//									'custrecord_erp_dclose_est_margin',
//									estMargin);
//							nlapiSetFieldValue('custrecord_erp_dclose_cost',
//									estCost);
//						} catch (ex) {
//						}
//
//						  var creditIndex = salesArrIds.indexOf("CustCred213");
//						  if (creditIndex > -1){
//
//						  var obj = salesArrObj[creditIndex];
//						  nlapiSetFieldValue('custrecord_erp_dclose_returns',
//						  obj.netsales_fx );
//
//						  }
//						/*******************************************************
//						 *  // Credit Memos
//						 *
//						 * var creditIndex = salesArrIds.indexOf("CustCred213");
//						 * if (creditIndex > -1){
//						 *
//						 * var obj = salesArrObj[creditIndex];
//						 * nlapiSetFieldValue('custrecord_erp_dclose_returns',
//						 * obj.netsales_fx );
//						 *
//						 * //console.log('CustCred213 ' + obj.netsales_fx);
//						 *  }
//						 *
//						 *
//						 *  // Employee Discounts var discountIndex =
//						 * salesArrIds.indexOf("CustInvc216"); if (discountIndex >
//						 * -1){
//						 *
//						 * var obj = salesArrObj[discountIndex];
//						 * nlapiSetFieldValue('custrecord_erp_dclose_employee_discounts',
//						 * obj.netsales_fx );
//						 *
//						 * //console.log('CustInvc216 ' + obj.netsales_fx);
//						 *  }
//						 *
//						 *  // F&D var discountIndex =
//						 * salesArrIds.indexOf("CustInvc215"); if (discountIndex >
//						 * -1){
//						 *
//						 * var obj = salesArrObj[discountIndex];
//						 * nlapiSetFieldValue('custrecord_erp_dclose_fd',
//						 * obj.netsales_fx );
//						 *
//						 * //console.log('CustInvc215 ' + obj.netsales_fx);
//						 *  }
//						 *
//						 ******************************************************/
//					}
//
//				} else {
//					nlapiSetFieldValue('custrecord_erp_dclose_' + modeTxt + '_actual', '');
//				}
//
//			   }
			}
		}

	} catch (ex) {

		console.log(ex.toString());
	}
}

function setUPTSPT(locId, date1, date2) {

	var salesArrayIds = new Array();
	var salesArrayObj = new Array();

	var salesObject = new Object();

	var strSavedSearchIDUPTSPT = null;
	var arSaveSearchFiltersUPTSPT = new Array();
	var arSavedSearchColumnsUPTSPT = new Array();
	// var arSavedSearchResultsUPTSPT = null;

	arSaveSearchFiltersUPTSPT.push(new nlobjSearchFilter('location', null,
			'anyof', locId));
	arSaveSearchFiltersUPTSPT.push(new nlobjSearchFilter('datecreated', null,
			'within', [ date1, date2 ]));

	var arSavedSearchResultsUPTSPT = nlapiLoadSearch('transaction',
			'customsearch1188');

	// arSavedSearchResultsUPTSPT.addColumns(null);
	arSavedSearchResultsUPTSPT.addFilters(arSaveSearchFiltersUPTSPT);

	var resultSet = arSavedSearchResultsUPTSPT.runSearch();

	try {

		var results = resultSet.getResults(0, 1000);

		if (!isNullOrEmpty(results)) {

			var idCurrentItem = results[0].getValue('internalid');
			var columns = results[0].getAllColumns();
			var objResult = results[0];

			var objUPTSPT = new Object();

			objUPTSPT.location = objResult.getValue(columns[0]);
			objUPTSPT.upt = objResult.getValue(columns[1]);
			objUPTSPT.spt = objResult.getValue(columns[2]);
			objUPTSPT.aur = objResult.getValue(columns[3]);

			var upt = objResult.getValue(columns[1]);
			upt = Number(upt).toFixed(1);
			var spt = objResult.getValue(columns[2]);
			spt = Number(spt).toFixed(0);

			var aur = objResult.getValue(columns[3]);
			aur = nlapiGetFieldValue('custrecord_erp_dclose_daily_actual')/aur;
			aur = Number(aur).toFixed(0);

			nlapiSetFieldValue('custrecord_erp_dclose_upt_units_pertrans', upt);
			nlapiSetFieldValue('custrecord_erp_dclose_sptdpt', spt);
			nlapiSetFieldValue('custrecorderp_dclose_aur', aur);
			return objUPTSPT;
		}
	} catch (ex) {
		return null;
	}

}

function getShopClosing(locId, date1, date2) {

	var salesArrayIds = new Array();
	var salesArrayObj = new Array();

	var salesObject = new Object();

	var strSavedSearchIDShopClosing = null;
	var arSaveSearchFiltersShopClosing = new Array();
	var arSavedSearchColumnsShopClosing = new Array();

	arSaveSearchFiltersShopClosing.push(new nlobjSearchFilter('location', null,
			'anyof', locId));
	arSaveSearchFiltersShopClosing.push(new nlobjSearchFilter('datecreated',
			null, 'within', [ date1, date2 ]));

	var arSavedSearchResultsShopClosing = nlapiLoadSearch('transaction',
			'customsearch_erp_script_cn_closing');
	arSavedSearchResultsShopClosing.addFilters(arSaveSearchFiltersShopClosing);

	var resultSet = arSavedSearchResultsShopClosing.runSearch();

	try {

		var results = resultSet.getResults(0, 1000);

		if (!isNullOrEmpty(results)) {

			var idCurrentItem = results[0].getValue('internalid');
			var columns = results[0].getAllColumns();
			var objResult = results[0];

			var objClosingSales = new Object();

			objClosingSales.location = objResult.getValue(columns[0]);
			objClosingSales.closingsales = objResult.getValue(columns[1]);

			nlapiSetFieldValue('custrecord_erp_dclose_netsuite_closing',
					objClosingSales.closingsales);

			return objClosingSales;

		}

	} catch (ex) {
		return null;
	}

}

// function getUPTSPT(locId, date1, date2){
//
// var salesArrayIds = new Array();
// var salesArrayObj = new Array();
//
// var salesObject = new Object();
//
// var strSavedSearchIDUPTSPT = null;
// var arSaveSearchFiltersUPTSPT = new Array();
// var arSavedSearchColumnsUPTSPT = new Array();
// //var arSavedSearchResultsUPTSPT = null;
//
//
// arSaveSearchFiltersUPTSPT.push(new nlobjSearchFilter( 'location', null,
// 'anyof', locId));
// arSaveSearchFiltersUPTSPT.push(new nlobjSearchFilter('datecreated', null,
// 'within', [date1,date2]));
//
// var arSavedSearchResultsUPTSPT = nlapiLoadSearch('transaction',
// 'customsearch1188');
//
// // arSavedSearchResultsUPTSPT.addColumns(null);
// arSavedSearchResultsUPTSPT.addFilters(arSaveSearchFiltersUPTSPT);
//
// var resultSet = arSavedSearchResultsUPTSPT.runSearch();
//
//
//
// try{
// ////console.log('upt');
// var results = resultSet.getResults(0,1000);
//
// if(!isNullOrEmpty(results)){
//
// var idCurrentItem = results[0].getValue('internalid');
// var columns = results[0].getAllColumns();
// var objResult = results[0];
//
// var objUPTSPT = new Object();
//
// objUPTSPT.location = objResult.getValue(columns[0]);
// objUPTSPT.upt = objResult.getValue(columns[1]);
// objUPTSPT.spt = objResult.getValue(columns[2]);
//
// nlapiSetFieldValue('custrecord_erp_dclose_upt_units_pertrans', objUPTSPT.upt
// );
// nlapiSetFieldValue('custrecord_erp_dclose_sptdpt', objUPTSPT.spt);
//
// return objUPTSPT;
// }
// }catch(ex){
// return null;
// }
//
// }

function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}

function setPeakTimes(locId, date1, date2) {

	// date1 = '12/01/2015';
	// date2 = '12/01/2015';
	// nlapiLogExecution('DEBUG', 'eff date1', date1);
	// nlapiLogExecution('DEBUG', 'eff date2', date2);
	// nlapiLogExecution('DEBUG', 'eff locId', locId);

	var salesArrayIds = new Array();
	var salesArrayObj = new Array();

	var salesObject = new Object();

	var strSavedSearchIDPeakHours = null;
	var arSaveSearchFiltersPeakHours = new Array();
	var arSavedSearchColumnsPeakHours = new Array();

	arSavedSearchColumnsPeakHours.push(new nlobjSearchColumn('tranid', null,
			'count').setSort(true));

	arSaveSearchFiltersPeakHours.push(new nlobjSearchFilter('location', null,
			'anyof', locId));
	arSaveSearchFiltersPeakHours.push(new nlobjSearchFilter('datecreated',
			null, 'within', [ date1, date2 ]));

	var arSavedSearchResultsPeakHours = nlapiLoadSearch('transaction',
			'customsearch1190_2');
	arSavedSearchResultsPeakHours.addFilters(arSaveSearchFiltersPeakHours);
	arSavedSearchResultsPeakHours.addColumns(arSavedSearchColumnsPeakHours);

	var resultSet = arSavedSearchResultsPeakHours.runSearch();

	try {

		var results = resultSet.getResults(0, 50);

		if (!isNullOrEmpty(results)) {

			nlapiLogExecution('DEBUG', 'eff result', results.length);

			var peakHours = new Array();
			var peakHoursStr = '';
			var peakCount = 1;


			for (var i = 0; i < peakCount; i++) {

				var idCurrentItem = results[i].getValue('internalid');
				var columns = results[i].getAllColumns();
				var objResult = results[i];

				var objClosingSales = new Object();
				objClosingSales.location = objResult.getValue(columns[0]);
				objClosingSales.time = objResult.getValue(columns[1]);
				objClosingSales.count = objResult.getValue(columns[2]);
				objClosingSales.qty = objResult.getValue(columns[3]);
				//
				objClosingSales.time = moment(objClosingSales.time, 'hmm')
						.format('h:mm a');

				peakHours.push(objClosingSales);
				peakHoursStr = peakHoursStr + objClosingSales.time + "\n";

				// //console.log("setPeakTimes" +
				// JSON.stringify(objClosingSales));
			}

			// //console.log("setPeakTimes array" + );
			nlapiLogExecution('DEBUG', 'eff peakHourspeakHours', JSON
					.stringify(peakHours));
			nlapiSetFieldValue('custrecord_erp_dclose_peak_times', peakHoursStr);

			return peakHours;

		}

	} catch (ex) {
		return null;
	}

}
