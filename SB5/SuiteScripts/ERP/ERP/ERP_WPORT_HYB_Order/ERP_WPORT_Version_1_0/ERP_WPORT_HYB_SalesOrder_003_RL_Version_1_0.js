/**
 *	File Name		:	ERP_WPORT_HYB_SalesOrder_003_RL.js
 *	Function		:	Order Process - Step 3 (Sales Orders, Drop Shipped Sales Orders(Creation, Fulfillment and Billing))
 * 	Remarks			:	Relevant scripts isolated from WTKA_NetSuite_ICTO_Process.js
 *	Prepared by		:	A.Villanueva
 *	Release Dates	:	01-Jun-2016
 * 	Current Version	:	1.0
**/


function processOrders(orderRequest)
{
	nlapiLogExecution('debug', 'processOrders', JSON.stringify(orderRequest)); 
	var respObject  = new Object();
	try
	{
		var custRecord  	= nlapiLoadRecord('customrecord_wtka_external_orders', orderRequest.id); //the WTKA_External_Order record
		var resp 			= custRecord.getFieldValue('custrecord_wtka_final_response');
		if(resp != null)	finalResponse = JSON.parse(resp);
		var recordCounter 	= parseInt(custRecord.getFieldValue('custrecord_wtka_request_counter'));
		var recordFileId 	= parseInt(custRecord.getFieldValue('custrecord_wtka_inbound_request'));
		dataIn 		  		= fetchRequestFromFile(recordFileId, recordCounter);
		var lastProcessed 	= custRecord.getFieldValue('custrecord_wtka_order_lastprocessed');
		var tranId = custRecord.getFieldValue('custrecord_wtka_orderid'); //orderid from the JSON call 
		
		var icsoFlag;
		var shipmentArray = new Array();
		var resp 		  = custRecord.getFieldValue('custrecord_wtka_orderid_shipment');
		if(resp != null)	shipmentArray = JSON.parse(resp);
		var i 			  = parseInt(lastProcessed);



		if(i < 0)
		{
			respObject.status = "Success"; //Already completed
			return respObject;
		}
		var retrigger 	  = false;
		customer 		  = custRecord.getFieldValue('custrecord_wtka_customerid');
		
		lookupData(dataIn);
		
		for(; !rollbackFlag && orderShipID != null && i<orderShipID.length; i++)
		{
			//Reset variables
			createFulfillmentFlag = false;
			createInvoiceFlag	  = false;
			
			nlapiLogExecution('debug', 'PROCESSING SHIPMENT', i);
			
			finalMessage 		 		= new Object();
			finalMessage.orderNumber 	= orderShipID[i];
			finalMessage.nsOrderNumber 	= 0;
			finalMessage.records	 	= new Array();
			orderDet					= new Object();
			
			var locale  			= dataIn.order.orderHeader.billingCustomer.locale.split('/');
			var sellingLocation 	= locale[locale.length-1].toUpperCase();
			if(sellingLocation != 'CA')	sellingLocation = 'US';
			var fulfillingLocation 	= dataIn.order.shipment[i].shipmentHeader.fulfillingLocation.country.toUpperCase();
			if(sellingLocation === fulfillingLocation)	icsoFlag = false;
			else										icsoFlag = true;

			var resp = custRecord.getFieldValue('custrecord_wtka_processed_ids');
			if(resp != null)	orderDetails = JSON.parse(resp);
				
			
			var orderFlag = 0;
			var orderID   = FetchOrderId('salesorder', orderShipID[i], 'ext', 'status', 'nonIcto');
			if(orderID != 0) // Existing Order
			{
				if(orderID.col == 'pendingFulfillment') //Check status
				{
					createFulfillmentFlag = true;
				}
				else if(orderID.col == 'pendingBilling') //Check status
				{
					createInvoiceFlag = true;
				}
				else
				{
					continue;
				}
				nsOrderNumber[i] = orderID.col;
				recordId 		 = orderID.id;
				orderFlag		 = recordId;
				record	 		 = nlapiLoadRecord('salesorder', recordId);
			}
			else
			{
				if(!icsoFlag) //ECOMM
				{
					orderFlag = createOrder(dataIn, orderShipID[i], i); // Create Order
					if(!rollbackFlag && orderFlag != "ZERO_SHIPPED")
					{
						orderDet.icso			= false;
						orderDet.orderId 		= orderFlag;
						orderDet.fulfillmentId	= 0;
						orderDet.invoiceId		= 0;
						orderDet.transformtype	= 0;
						orderDet.logId			= 0;
					}
				}
				else //ICTO
				{
					var orderFlag 				= createOrder(dataIn, orderShipID[i], i, orderRequest.id, recordCounter);
					if(!rollbackFlag && orderFlag != "ZERO_SHIPPED")
					{
						orderDet.icso			= true;
						orderDet.orderId 		= orderFlag.so;
						orderDet.poId			= orderFlag.po;
					}
				}
						
				finalMessage.nsOrderNumber = nsOrderNumber[i];
				
				if(createFulfillmentFlag)
				{
					orderDet.fulfillmentId	= createFulfillment(dataIn, orderShipID[i], i); // Fulfill Order
				}
				
				if(createInvoiceFlag)
				{
					orderDet.invoiceId 		= createInvoice(dataIn, orderShipID[i], i);  // Bill Order
					orderDet.transformtype	= finalTransformType;
				}
			}
						
			if(rollbackFlag)	finalResponse.status = "Error";
			if(finalMessage.records.length > 0)	finalResponse.records.push(finalMessage);
			orderDet.logId = LogCreation(1, 5, recordId, recordId, dataIn, ediStatus, finalResponse, 'F'); 
			orderDetails.push(orderDet);

			shipmentArray.push(orderShipID[i]);
			
			if(nlapiGetContext().getRemainingUsage() < governanceMinUsage)
			{
				setRecordFieldValues(orderRequest.id, orderDetails, finalResponse, null, null, shipmentArray, i);
				nlapiLogExecution('debug', 'Usage Exceeded', nlapiGetContext().getRemainingUsage());
				retrigger = true;
				break;
			}
		}
		if(rollbackFlag)
		{
			//nlapiLogExecution('debug', dataIn.order.orderHeader.orderId, 'Initiating Rollback...');
			respObject.status 	 = "Error";
			finalResponse.status = "Error";
			setRecordFieldValues(orderRequest.id, orderDetails, finalResponse, 'T', 6);
		}
		else
		{
			if(!retrigger && i == dataIn.order.shipment.length) //Success - Complete processing complete
			{
				finalResponse.status = "Success";
				respObject.status 	 = "Success";
				setRecordFieldValues(orderRequest.id, orderDetails, finalResponse, null, 4, shipmentArray, -1, icsoFlag);
				nlapiLogExecution('debug', 'Final State of Sales Order Flow', JSON.stringify(finalResponse));
			}
		}
	}
	catch(other_errors)
	{
		rollbackFlag	  = true;
		var finalResp	  = new Object();
		finalResp.status  = "Exception";
		finalResp.message = "Unable to Process Orders. Error Details: ";
		finalResp.message = other_errors;
		try
		{
			var custRecord  	= nlapiLoadRecord('customrecord_wtka_external_orders', orderRequest.id);
			var resp 			= custRecord.getFieldValue('custrecord_wtka_final_response');
			if(resp != null)	finalResponse = JSON.parse(resp);
			finalResponse.status = "Error";
			finalResponse.records.push(finalResp);
			setRecordFieldValues(orderRequest.id, null, finalResponse, 'T', 6);
		}
		catch(logError){ /* Custom record not found */ }
		finalResponse.status = "Error";
		finalResponse.records.push(finalResp);
		nlapiLogExecution('debug', 'Unable to Process Orders', JSON.stringify(finalResponse));
		createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 31, SYNC_STATUS.ERROR, JSON.stringify(finalResponse), null, 41); //Library call to WTKA_Library.js //Record Type: 31 is Sales Order
		respObject.status = "Error";
	}
	return respObject;
}

function lookupData(dataIn, orderCheck)
{
	var FinalUPCCodes = new Array();
	for(var i in dataIn.order.orderDetail)
	{
		FinalUPCCodes[i] = dataIn.order.orderDetail[i].item.SKUId;
	}
	/* Lookup Items */
	FinalItemIDs = FetchIdFromName('item', FinalUPCCodes);
	
	for(var i in dataIn.order.shipment)
	{
		createLogFlag = true; //Reset flag

		orderShipID[i] = dataIn.order.orderHeader.orderId + '_' + dataIn.order.shipment[i].shipmentHeader.shipmentId;

		var orderID = FetchOrderId('salesorder', orderShipID[i], 'ext', 'tranid', 'nonIcto');
		if(orderID != 0)
		{
			nsOrderNumber[i] = orderID.col;
			recordId 		 = orderID.id;
		}
	}
	/* Lookup Customer */
	if(customer == null || customer == '')	customer = lookupCustomer(dataIn);
	var locale  = dataIn.order.orderHeader.billingCustomer.locale.split('/');
	var country = locale[locale.length-1].toUpperCase();
	if(country != 'CA')	country = 'US';
	var subsid  = FetchSubsidiaryId('country', country);
	subsidiary 	= subsid.code;

	/* Lookup Tax Code */
	var state  	 = dataIn.order.orderHeader.billingCustomer.shippingAddress.provinceName.split('-');
	var province = state[state.length-1].toUpperCase();
	taxCode  	 = FetchTaxCode(country, province);
	
	location = (country == 'CA') ? CA_Virtual : US_Virtual;

	/* Lookup payment method */
	extSysPayment = LookupPayment(dataIn.order.orderHeader.paymentInfo.tenderType, dataIn.order.orderHeader.paymentInfo.currencyCode);
}

function createOrder(dataIn, orderId, i, custRecordId, requestIndex)
{
	var tranId;
	try{
		tranId = dataIn.order.orderHeader.orderId;
	} catch (tranExc) {
		tranId = '';
	}


	var ictoFlow = false;
	var recId 	 = 0;
	if(custRecordId != null)
	{
		nlapiLogExecution('debug', 'Creating Intercompany Order: ' + i, orderId);
		ictoFlow = true;
	}
	else
	{
		nlapiLogExecution('debug', 'Creating Order: ' + i, orderId);
	}
	if(!ictoFlow)	recordId = 0;			
	try
	{
		OrderObject = getOrderObject(orderId, dataIn, i, 'orderFlow');
		
		if(ictoFlow)
		{
			OrderObject.poVendor = 0;
			/* Map vendors */
			var locale  		= dataIn.order.orderHeader.billingCustomer.locale.split('/');
			var sellingLocation = locale[locale.length-1].toUpperCase();
			var fulfillingLocation 	= dataIn.order.shipment[i].shipmentHeader.fulfillingLocation.country.toUpperCase();
			if(sellingLocation != 'CA')	sellingLocation = 'US';
			/* Comment Out Reason: Replaced with dynamic logic using Script Config settings
			switch(sellingLocation)
			{
				case 'CA':
					switch(fulfillingLocation)
					{
						case 'US':
							OrderObject.poVendor =  CA_US_Vendor; //SW US Inc. - Inventory - Canada (Inventory)
							break;
					}
					break;
				case 'US':
					switch(fulfillingLocation)
					{
						case 'CA':
							OrderObject.poVendor =  US_CA_Vendor; //Kit and Ace Designs Inc. - USA (Inventory)
							break;
					}
					break;
			}*/
			OrderObject.poVendor = fetchPOVendor(sellingLocation, fulfillingLocation); //Dynamic logic
			nlapiLogExecution('debug', 'PO Vendor', OrderObject.poVendor);
		}
		
		var recordType 	= 'salesorder';
		record 			= nlapiCreateRecord(recordType);
		
		if(ictoFlow)
		{
			var requestObject 		= new Object();
			requestObject.id  		= custRecordId;
			requestObject.index 	= requestIndex;
			requestObject.shipment 	= i;
			record.setFieldValue('custbody_wtka_request_data',			JSON.stringify(requestObject)); //Request Reference
		}
		
		record.setFieldValue('entity', 									OrderObject.customer);
		record.setFieldValue('custbody_wtka_extsys_order_number',		orderId);
		record.setFieldValue('custbody_wtka_extsys_hybris_order',		String(dataIn.order.orderHeader.orderId));
		record.setFieldValue('externalid', 								orderId);
		record.setFieldText('currency',									OrderObject.currency);
		var orderTransactionDt  = moment(dataIn.order.orderHeader.orderTransactionDt).format('l LT');
		if(orderTransactionDt != "Invalid date")
		{
			orderTransactionDt 		= new Date(orderTransactionDt);
			orderTransactionDt 		= nlapiDateToString(orderTransactionDt, 'date');
			record.setFieldValue('trandate', 							orderTransactionDt);
		}
		record.setFieldValue('memo', 									OrderObject.memo);
		record.setFieldValue('location', 								OrderObject.location);
		var shipDt 				= moment(dataIn.order.shipment[i].shipmentHeader.shipDt).format('l LT');
		if(shipDt != "Invalid date")
		{
			shipDt 					= new Date(shipDt);
			shipDt 					= nlapiDateToString(shipDt, 'date');
			record.setFieldValue('shipdate', 							shipDt);
		}
		var shipReceivedDt 		= moment(dataIn.order.shipment[i].shipmentHeader.shipReceivedDt).format('l LT');
		if(shipReceivedDt != "Invalid date")
		{
			shipReceivedDt 			= new Date(shipReceivedDt);
			shipReceivedDt 			= nlapiDateToString(shipReceivedDt, 'date');
			record.setFieldValue('custbody_ka_to_expected_ship_date', 	shipReceivedDt);
		}
		var expectedShipDate 	= moment(dataIn.order.shipment[i].shipmentHeader.expectedShipDate).format('l LT');
		if(expectedShipDate != "Invalid date")
		{
			expectedShipDate 		= new Date(expectedShipDate);
			expectedShipDate 		= nlapiDateToString(expectedShipDate, 'date');
			record.setFieldValue('custbody_ka_to_expected_recv_date', 	expectedShipDate);
		}
		
		/* Set custom Payment */
		record.setFieldValue('paymentmethod', 							OrderObject.paymentmethod);
		record.setFieldValue('custbody_wtka_extsys_payment_method',		dataIn.order.orderHeader.paymentInfo.tenderType);
		record.setFieldValue('ccapproved', 								'T'); //Approved Card
		record.setFieldValue('getauth', 								'F'); //Approved Card
		
		//==========Clear CC details from Sales Order, Cash Sale and return auth=============
		record.setFieldValue('authcode', 								'');
		record.setFieldValue('ccnumber', 								'');
		record.setFieldValue('ccexpiredate', 							'');
		record.setFieldValue('ccname', 									'');
		record.setFieldValue('ccstreet', 								'');
		record.setFieldValue('cczipcode', 								'');	
				
		record.setFieldValue('exchangerate', 							dataIn.order.orderHeader.exchangeRate);
		
		record.setFieldValue('billaddresslist', '');
		record.setFieldValue('billaddressee', 	dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.billingFirstName + ' ' + dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.billingLastName);
		// record.setFieldValue('billphone', 		dataIn.order.orderHeader.billingCustomer.customerPhoneNumber);						//Modified to avoid Invalid Phone Errors
		record.setFieldValue('billaddr1', 		dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.billingAddressLine1);
		record.setFieldValue('billaddr2', 		dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.billingAddressLine2);
		record.setFieldValue('billcity', 		dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.cityName);
		record.setFieldValue('billstate', 		dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.provinceName);
		record.setFieldValue('billzip', 		dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.postalCode);
		record.setFieldValue('billcountry', 	dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.countryName);
		
		/* Set custom Shipping Address */
		record.setFieldValue('shipaddresslist', '');
		record.setFieldValue('shipaddressee', 	dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.recipientFirstName + ' ' + dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.recipientLastName);
		// record.setFieldValue('shipphone', 		dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.recipientPhoneNumber);	//Modified to avoid Invalid Phone Errors
		record.setFieldValue('shipaddr1', 		dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.shippingAddressLine1);
		record.setFieldValue('shipaddr2', 		dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.shippingAddressLine2);
		record.setFieldValue('shipcity', 		dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.cityName);
		record.setFieldValue('shipstate', 		dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.provinceName);
		record.setFieldValue('shipzip', 		dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.postalCode);
		record.setFieldValue('shipcountry', 	dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.countryName);
		
		var zeroShipped = 0;
		for(var j=0; FinalShipArray != null && j < FinalShipArray.length; j++)
		{
			for(var f=0; OrderObject.item != null && f < OrderObject.item.length; f++)
			{
				if(RecordUPCCodes[j] == OrderObject.item[f].UPCCode)
				{
					if(FinalShipQty[j] == 0)	zeroShipped++;
					
					record.selectNewLineItem('item');
					record.setCurrentLineItemValue('item', 'location', 	OrderObject.location);
					record.setCurrentLineItemValue('item', 'item', 		OrderObject.item[f].Id);
					record.setCurrentLineItemValue('item', 'quantity', 	FinalShipOrderQty[j]);
					if(ictoFlow) //Drop Shipment
					{
						record.setCurrentLineItemValue('item', 'povendor', 	OrderObject.poVendor); 
					}
					else //Avoid Drop Shipment
					{
						record.setCurrentLineItemValue('item', 'createpo', 	null);
						record.setCurrentLineItemValue('item', 'povendor', 	null);
					}
					/* Set custom Price */
					record.setCurrentLineItemValue('item', 'price', 	-1);
					record.setCurrentLineItemValue('item', 'rate', 		OrderObject.price[j]);
						
					/* Set Taxes */
					record.setCurrentLineItemValue('item', 'taxcode', OrderObject.taxCode);
					
					//Use Script Config setting
					var stSubUSId = 4; //default
					var stSCSubUS = SCRIPTCONFIG.getScriptConfigValue('Subsidiary: US');
					if(stSCSubUS){
						stSubUSId = JSON.parse(stSCSubUS).id;
					}
					
					if(!isNullOrEmpty(OrderObject.tax) && OrderObject.subsidiary == stSubUSId) //subsidiary - 4 US
					{
						record.setCurrentLineItemValue('item', 'taxrate1', 	OrderObject.tax[j].tax1);
						record.setCurrentLineItemValue('item', 'taxrate2', 	OrderObject.tax[j].tax2);
					}
					
					record.setCurrentLineItemValue('item', 'custcol_wtka_taxrate1', 	OrderObject.tax[j].tax1);
					record.setCurrentLineItemValue('item', 'custcol_wtka_taxrate2', 	OrderObject.tax[j].tax2);
					
					record.commitLineItem('item');
					break;
				}
			}
		}

		if(zeroShipped == RecordUPCCodes.length)
		{
			var  orderObject 				= new Object();
			orderObject.status 				= "Success";
			orderObject.transactiontype 	= "Sales Order";
			orderObject.transactionNumber 	= nsOrderNumber[i];
			orderObject.message 			= "Shipment has zero items shipped"
			finalMessage.records.push(orderObject);
			nlapiLogExecution('debug', 'ORDER RESPONSE ' + orderId, JSON.stringify(finalMessage));
			return "ZERO_SHIPPED";
		}
		
		/* Set total tax amount */
		record.setFieldValue('taxamountoverride', OrderObject.soTax1Amount);
		record.setFieldValue('taxamount2override', OrderObject.soTax2Amount); //PST for Canadian Orders
		
		/* Commenting Setting the Shipcarrier as the system defaults it to Fedex/More */
		//record.setFieldText('shipcarrier', 							OrderObject.shipcarrier);
		if(OrderObject.shipmethod > 0)	record.setFieldValue('shipmethod', OrderObject.shipmethod);
		
		//Set Hybris Interco Dropship flag
		record.setFieldValue('custbody_erp_hybris_interco_dropship', OrderObject.ico ? 'T' : 'F');
		
		recordId = nlapiSubmitRecord(record);
		var soMessage = 'Created Sales Order: ' + recordId;
		createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 31, SYNC_STATUS.CREATED, soMessage, recordId, 42); //Library call to WTKA_Library.js //Record Type: 31 is Sales Order
		
		record 			 = nlapiLoadRecord(recordType, recordId);
		nsOrderNumber[i] = record.getFieldValue('tranid');
		
		var  ordObject 					= new Object();
		ordObject.status 				= "Success";
		ordObject.transactiontype 		= "Sales Order";
		ordObject.transactionid 		= recordId;
		ordObject.transactionNumber 	= nsOrderNumber[i];
		finalMessage.records.push(ordObject);

		if(ictoFlow)
		{
			var poId		 = record.getLineItemValue('item', 'createdpo', 1);
			if(poId == null || poId <= 0)
			{
				ediStatus	 			= 3;
				rollbackFlag 			= true;
				var  orderObject 		= new Object();
				orderObject.status  	= "Exception"; 
				orderObject.messagetype = "Sales Order Creation Failure";
				orderObject.message 	= 'Sales Order record could not be created. Details as below.'; 
				orderObject.details 	= 'Intercompany Purchase Order has not been created. Check if items have been marked as drop ship';
				finalMessage.records.push(orderObject);
				nlapiLogExecution('debug', 'createICSO - Error' , 'PO not created');
				return recId;
			}
			
			var poRecord	 = nlapiLoadRecord('purchaseorder', poId);
			var poNumber	 = poRecord.getFieldValue('tranid');
			
			//Use Script Config setting
			var stPOPrefix = "E"; //Default
			var stSCPOPrefix = SCRIPTCONFIG.getScriptConfigValue('Hybris: PO (ICTO): TranID Prefix'); 			
			poNumber		 = (stSCPOPrefix ? stSCPOPrefix : stPOPrefix) + poNumber; 
			
			poRecord.setFieldValue('tranid', 		poNumber);
			poRecord.setFieldText('approvalstatus', 'Approved');
			poId = nlapiSubmitRecord(poRecord, false, true);
			var poMessage = 'Created Purchase Order: ' + poId;
			createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 15, SYNC_STATUS.CREATED, poMessage, poId, 43); //Library call to WTKA_Library.js //Record Type: 15 is Purchase Order
			
			var  orderObject 				= new Object();
			orderObject.status 				= "Success";
			orderObject.transactiontype 	= "Purchase Order";
			orderObject.transactionid 		= poId;
			orderObject.transactionNumber	= poNumber;
			finalMessage.records.push(orderObject);
			
			recId	 = new Object();
			recId.so = recordId;
			recId.po = poId;
		}
		createFulfillmentFlag = true;
		ediStatus 	 		  = 1;
	}
	catch(err_order)
	{
		ediStatus	 = 3; // Exception
		rollbackFlag = true;
		
		var  orderObject 		= new Object();
		orderObject.status  	= "Exception"; 
		orderObject.messagetype = "Sales Order Creation Failure";
		orderObject.message 	= 'Sales Order record could not be created. Details as below.'; 
		orderObject.details 	= err_order.getCode() + ' : ' + err_order.getDetails();
		finalMessage.records.push(orderObject);
		nlapiLogExecution('debug', orderId + ' createOrder - Error' , err_order);

		createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 31, SYNC_STATUS.ERROR, JSON.stringify(finalMessage), null, 44); //Library call to WTKA_Library.js //Record Type: 31 is Sales Order
	}
	nlapiLogExecution('debug', 'ORDER RESPONSE ' + orderId, JSON.stringify(finalMessage));
	if(ictoFlow)	return recId;
	else			return recordId;
}

function getOrderObject(orderId, dataIn, i, orderFlow)
{
	var itemPrice  = new Array(),   itemTax 	= new Array();
	var taxAmount1 = 0.00, 			taxAmount2  = 0.00;
	var sotaxAmt1  = 0.00, 			sotaxAmt2   = 0.00;
	dualTax 	   = false;

	FinalShipArray 	 	= new Array();
	FinalShipOrderQty 	= new Array();
	FinalShipQty		= new Array();
	RecordUPCCodes		= new Array();

	var OrderObj 			= new Object();
	OrderObj.orderId 		= orderId;
	OrderObj.item 		 	= FinalItemIDs;
	if(orderFlow != null)
	{
		OrderObj.customer 		= customer;
		OrderObj.subsidiary		= subsidiary;
		OrderObj.location 		= location;
			
		//Check Script Config for Multiple DC setting
		if(isMultiDCEnabled()){
			var locale  = dataIn.order.orderHeader.billingCustomer.locale.split('/');
			var country = locale[locale.length-1].toUpperCase();
			
			//Use Script Config mapping for the location
			var objLoc = fetchSOLocMultiDC(country, 
							dataIn.order.shipment[i].shipmentHeader.fulfillingLocation.country,
							dataIn.order.shipment[i].shipmentHeader.fulfillingLocation.locationCode); 
			OrderObj.location = objLoc.loc;
			OrderObj.ico = objLoc.ico;
		}
		
		OrderObj.memo 			= readValue(dataIn.order.orderHeader.orderHeaderMemo);
		OrderObj.paymentmethod 	= extSysPayment;
		OrderObj.creditCard 	= creditCard;
		OrderObj.taxCode 		= taxCode;

		/* Setting Shipping Method based on subsidiary */
		var carriername = dataIn.order.shipment[i].shipmentHeader.shippingMethod;
		/* Comment Out Reason: Replaced with dynamic logic using Script Config settings
		var carriercode = (carriername.match(/FedEx/gi) != null) ? 'FedEx' : (carriername.match(/UPS/gi) != null) ? 'UPS' : null;
		switch(carriercode)
		{
			case 'FedEx':
				OrderObj.shipcarrier = 'nonups';
				switch(OrderObj.subsidiary)
				{
					case '3': //CA
						OrderObj.shipmethod = Fedex_CA;
						break;
					case '4': //US
						OrderObj.shipmethod = Fedex_US;
						break;
					default:
						OrderObj.shipmethod = 0;
				}
				break;

			case 'UPS':
				OrderObj.shipcarrier = 'ups';
				switch(OrderObj.subsidiary)
				{
					case '3': //CA
						OrderObj.shipmethod = UPS_CA;
						break;
					case '4': //US
						OrderObj.shipmethod = UPS_US;
						break;
					default:
						OrderObj.shipmethod = 0;
				}
				break;

			default:
				OrderObj.shipcarrier = 'nonups';
				OrderObj.shipmethod = 0;
		}*/
		var objShipMethod = fetchShippingMethod(carriername, OrderObj.subsidiary); //Dynamic logic
		OrderObj.shipcarrier = objShipMethod.shipcarrier;
		OrderObj.shipmethod = objShipMethod.shipmethod;
	}
	var LineDetails = dataIn.order.shipment[i];
	for(var j=0; LineDetails != null && j < LineDetails.shipmentDetail.length; j++)
	{
		FinalShipArray[j] 	 = LineDetails.shipmentDetail[j].orderDetailId;
		FinalShipOrderQty[j] = parseInt(LineDetails.shipmentDetail[j].quantityOrdered);
		FinalShipQty[j]		 = parseInt(LineDetails.shipmentDetail[j].quantityShipped);
	}

	for(var j=0; FinalShipArray != null && j < FinalShipArray.length; j++)
	{
		for(var k in dataIn.order.orderDetail)
		{
			if(dataIn.order.orderDetail[k].orderLineNumber === FinalShipArray[j])
			{
				RecordUPCCodes[j] 	= dataIn.order.orderDetail[k].item.SKUId; //UPC Codes
				itemPrice[j] 		= parseFloat(dataIn.order.orderDetail[k].amtItemPrice.amtCurrentPrice); //Set Price
				
				var totalOrderQty	= parseInt(dataIn.order.orderDetail[k].countItemOrdered);
				itemTax[j] 			= new Object();
				itemTax[j].tax1 	= 0.00;
				itemTax[j].tax2 	= 0.00;
				
				try
				{
					/* Set tax */
					var taxData 		= FetchTaxData(dataIn.order.orderDetail[k], 'order');
					dualTax 			= taxData.dualTax;
					
					var taxName = dataIn.order.orderDetail[k].tax[0].taxName;
					
					//Use Script Config setting
					var stSubCAId = 3; //Default
					var stSCSubCA = SCRIPTCONFIG.getScriptConfigValue('Subsidiary: CA');
					if(stSCSubCA){
						stSubCAId = JSON.parse(stSCSubCA).id;
					}
					
					if(OrderObj.subsidiary == stSubCAId && taxName.match(/pst/gi) != null)  //switch tax1 and tax2
					{
						itemTax[j].tax1 	= taxData.taxRate2;
						itemTax[j].tax2 	= taxData.taxRate1;
						if(totalOrderQty > 0) //Avoid divide by 0 error
						{
							var taxValue		 = quantityParse(taxData.tax2Amount, 		'float');
							// sotaxAmt1			+= taxValue; 										//Set currently
							var partTaxValue	 = (FinalShipOrderQty[j]/totalOrderQty)*taxValue;		//apportion tax - qtyOrdered
							sotaxAmt1			+= quantityParse(partTaxValue.toFixed(2), 	'float');
							var partTaxValue	 = (FinalShipQty[j]/totalOrderQty)*taxValue;			//apportion tax - qtyShipped
							taxAmount1			+= quantityParse(partTaxValue.toFixed(2), 	'float');
							
							var taxValue 		 = quantityParse(taxData.tax1Amount, 		'float');
							// sotaxAmt2			+= taxValue; 										//Set currently
							var partTaxValue	 = (FinalShipOrderQty[j]/totalOrderQty)*taxValue;		//apportion tax - qtyOrdered
							sotaxAmt2			+= quantityParse(partTaxValue.toFixed(2), 	'float');
							var partTaxValue	 = (FinalShipQty[j]/totalOrderQty)*taxValue;			//apportion tax - qtyShipped
							taxAmount2			+= quantityParse(partTaxValue.toFixed(2), 	'float');
						}
					}
					else
					{
						itemTax[j].tax1 	= taxData.taxRate1;
						itemTax[j].tax2 	= taxData.taxRate2;
						if(totalOrderQty > 0) //Avoid divide by 0 error
						{
							var taxValue		 = quantityParse(taxData.tax1Amount, 		'float');
							// sotaxAmt1			+= taxValue; 										//Set currently
							var partTaxValue	 = (FinalShipOrderQty[j]/totalOrderQty)*taxValue; 		//apportion tax - qtyOrdered
							sotaxAmt1			+= quantityParse(partTaxValue.toFixed(2), 	'float');
							var partTaxValue	 = (FinalShipQty[j]/totalOrderQty)*taxValue;	  		//apportion tax - qtyShipped
							taxAmount1			+= quantityParse(partTaxValue.toFixed(2), 	'float');
							
							var taxValue 		 = quantityParse(taxData.tax2Amount, 		'float');
							// sotaxAmt2			+= taxValue; 										//Set currently
							var partTaxValue	 = (FinalShipOrderQty[j]/totalOrderQty)*taxValue;		//apportion tax - qtyOrdered
							sotaxAmt2			+= quantityParse(partTaxValue.toFixed(2), 	'float');
							var partTaxValue	 = (FinalShipQty[j]/totalOrderQty)*taxValue;			//apportion tax - qtyShipped
							taxAmount2			+= quantityParse(partTaxValue.toFixed(2), 	'float');
						}
					}
				}
				catch(ex){}
			}
		}
	}
	OrderObject.orderItems	= RecordUPCCodes;
	OrderObj.currency 	 	= dataIn.order.orderHeader.paymentInfo.currencyCode;
	OrderObj.tax 		 	= itemTax;
	OrderObj.dualTax 		= dualTax;
	OrderObj.soTax1Amount	= sotaxAmt1;
	OrderObj.soTax2Amount	= sotaxAmt2;
	OrderObj.tax1Amount   	= taxAmount1;
	OrderObj.tax2Amount   	= taxAmount2;
	OrderObj.price 		 	= itemPrice;
	nlapiLogExecution('debug', 'Values', JSON.stringify(OrderObj));
	return OrderObj;
}

function createFulfillment(dataIn, orderId, i, ictoFlow)
{
	var tranId;
	try{
		tranId = dataIn.order.orderHeader.orderId;
	} catch (tranExc) {
		tranId = '';
	}


	nlapiLogExecution('debug', 'Fulfilling Order: ' + i, orderId);
	if(OrderObject.orderId == undefined)
	{
		if(ictoFlow != null)	OrderObject = getOrderObject(orderId, dataIn, i);
		else					OrderObject = getOrderObject(orderId, dataIn, i, true);
	}

	var fulfillmentId 	= 0;
	TranDate 			= '';
	try
	{
		//Transform Order
		TranDate  = moment(dataIn.order.shipment[i].shipmentHeader.shipDt).format('l LT');
		{
			TranDate  	= moment(dataIn.order.shipment[i].shipmentHeader.shipDt).format('l LT');
			TranDate 	= new Date(TranDate);
			TranDate 	= nlapiDateToString(TranDate, 'date');		
		}
		var ReturnReqRefNumber 	 = '';
		var TrackingNumber 		 = dataIn.order.shipment[i].shipmentHeader.trackingNumber;

		var itemFulfillment 	 = nlapiTransformRecord('salesorder', recordId, 'itemfulfillment');
		var itemFulfillItemCount = itemFulfillment.getLineItemCount('item');
		var inventoryItemFulfillmentCount = 0;
		
		if(itemFulfillItemCount == 0)
		{
			var fulfillmentObject 			= new Object();
			fulfillmentObject.status  		= "Error";
			fulfillmentObject.messagetype 	= "Item Fulfillment Failure";
			fulfillmentObject.message 		= 'Order cannot be processed as items are backordered and there are no items available to fulfill';
			ediStatus						= 2;
			rollbackFlag 					= true;
			finalMessage.records.push(fulfillmentObject);
			return -1;
		}
		for(var f=0; f < itemFulfillment.getLineItemCount('item'); f++)
		{
			itemFulfillment.selectLineItem('item', f+1);
			var itemCode = itemFulfillment.getCurrentLineItemValue('item', 'custcol_wtka_upccode');
			for(var j=0; FinalShipArray != null && j < FinalShipArray.length; j++)
			{
				if(RecordUPCCodes[j] == itemCode)
				{
					itemFulfillment.setCurrentLineItemValue('item', 'quantity', 	FinalShipQty[j]);
					itemFulfillment.setCurrentLineItemValue('item', 'fulfill',  	'T');
					break;
				}
			}
			itemFulfillment.commitLineItem('item');
		}
		itemFulfillment.setLineItemValue('package', 'packageweight', 		 1, parseInt('1'));
		itemFulfillment.setLineItemValue('package', 'packagedescr', 		 1, ReturnReqRefNumber);
		itemFulfillment.setLineItemValue('package', 'packagetrackingnumber', 1, TrackingNumber);
		itemFulfillment.setFieldValue('trandate', 	TranDate);
		itemFulfillment.setFieldText('shipstatus', 	'Shipped', 				true);

		fulfillmentId 	= nlapiSubmitRecord(itemFulfillment, true);
		var fulfMessage = 'Created Item Fulfillment: ' + fulfillmentId;
		createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 32, SYNC_STATUS.CREATED, fulfMessage, fulfillmentId, 45); //Library call to WTKA_Library.js //Record Type: 32 is Item Fulfillment

		var fulfillmentObject 				= new Object();
		fulfillmentObject.status 			= "Success";
		fulfillmentObject.transactiontype 	= "Item Fulfillment";
		fulfillmentObject.transactionid 	= fulfillmentId;
		fulfillmentObject.transactionNumber = nlapiLookupField('itemfulfillment', fulfillmentId, 'tranid');
		fulfillmentObject.transactiondate 	= TranDate;
		finalMessage.records.push(fulfillmentObject);

		itemfulfillmentflag	 = true;
		createInvoiceFlag 	 = true;
		ediStatus 			 = 1;
	}
	catch(err_fulfill)
	{
		ediStatus	 = 3; // Exception
		rollbackFlag = true;

		var fulfillmentObject 			= new Object();
		fulfillmentObject.status  		= "Error";
		fulfillmentObject.messagetype 	= "Item Fulfillment Failure";
		fulfillmentObject.message 		= 'Item Fulfillment record could not be created. Details as below.';
		fulfillmentObject.details 		= err_fulfill.getCode() + ' : ' + err_fulfill.getDetails();
		finalMessage.records.push(fulfillmentObject);
		nlapiLogExecution('debug', orderId + ' createFulfillment - Error' , err_fulfill);

		createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 32, SYNC_STATUS.ERROR, JSON.stringify(finalMessage), null, 46); //Library call to WTKA_Library.js //Record Type: 32 is Item Fulfillment
	}
	nlapiLogExecution('debug', 'FULFILLMENT RESPONSE ' + orderId, JSON.stringify(finalMessage));
	return fulfillmentId;
}

function createInvoice(dataIn, orderId, i, ictoFlow)
{
	var tranId;
	try{
		tranId = dataIn.order.orderHeader.orderId;
	} catch (tranExc) {
		tranId = '';
	}	

	nlapiLogExecution('debug', 'Billing Order: ' + i, orderId);
	var finalRecordId  = 0;
	finalTransformType = 0;

	if(OrderObject.orderId == undefined)
	{
		if(ictoFlow != null)	OrderObject = getOrderObject(orderId, dataIn, i);
		else					OrderObject = getOrderObject(orderId, dataIn, i, true);
		nlapiLogExecution('debug', 'Values', JSON.stringify(OrderObject));
	}

	try
	{
		if(TranDate == '') 		
		{
			TranDate  	= moment(dataIn.order.shipment[i].shipmentHeader.shipDt).format('l LT');
			TranDate 	= new Date(TranDate);
			TranDate 	= nlapiDateToString(TranDate, 'date');		
		}
		record = nlapiLoadRecord('salesorder', recordId);
		var recterms 	   = record.getFieldValue('terms');
		var recpaymeth 	   = record.getFieldValue('paymentmethod');
		finalTransformType = ((recterms != null && recterms.length > 0) || (recpaymeth != null && recpaymeth.length > 0)) ? 'cashsale' : 'invoice';
		var finalRecord    = nlapiTransformRecord('salesorder', recordId, finalTransformType);

		for(var r=0; r < finalRecord.getLineItemCount('item'); r++)
		{
			finalRecord.selectLineItem('item', r+1);
			var ItemCode = finalRecord.getLineItemValue('item', 'custcol_wtka_upccode', r+1);

			for(var f=0; RecordUPCCodes != null && f <RecordUPCCodes.length; f++)
			{
				if(RecordUPCCodes[f] == ItemCode)
				{
					finalRecord.setCurrentLineItemValue('item', 'price', 	-1);
					finalRecord.setCurrentLineItemValue('item', 'rate', 	OrderObject.price[f]);
					
					var subsidiary = finalRecord.getFieldText('subsidiary');
					var us = subsidiary.match(/Kit and Ace Operating US/i);
					
					//Use Script Config setting
					var stSCSubUS = SCRIPTCONFIG.getScriptConfigValue('Subsidiary: US');
					if(stSCSubUS){
						var stSCSubUSName = JSON.parse(stSCSubUS).name;
						us = subsidiary.match(new RegExp(stSCSubUSName, 'i'));
					}
										
					if(!isNullOrEmpty(OrderObject.tax) && us != null)
					{
						finalRecord.setCurrentLineItemValue('item', 'taxrate1', 	OrderObject.tax[f].tax1);
						finalRecord.setCurrentLineItemValue('item', 'taxrate2', 	OrderObject.tax[f].tax2);
					}
					
					finalRecord.commitLineItem('item');
					break;
				}
			}
		}

		finalRecord.setFieldValue('trandate', TranDate);

		/* Set total tax amount */
		finalRecord.setFieldValue('taxamountoverride', OrderObject.tax1Amount);
		finalRecord.setFieldValue('taxamount2override', OrderObject.tax2Amount); //PST for Canadian Orders
				
		finalRecord.setFieldValue('ccapproved', 	'T');
		finalRecord.setFieldValue('chargeit', 		'F');
		
		//==========Clear CC details from Cash sale=============
		finalRecord.setFieldValue('ccnumber', 		'');
		finalRecord.setFieldValue('ccexpiredate', 	'');
		finalRecord.setFieldValue('ccname', 		'');
		finalRecord.setFieldValue('ccstreet', 		'');
		finalRecord.setFieldValue('cczipcode', 		'');	
		
		
		finalRecord.setFieldValue('custbody_erp_inv_cs_created', 'T');
		finalRecordId  = nlapiSubmitRecord(finalRecord, true);

		var cashSaleMessage = 'Created Cash Sale: ' + finalRecordId;
		createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 5, SYNC_STATUS.CREATED, cashSaleMessage, finalRecordId, 47); //Library call to WTKA_Library.js  //Record Type: 5 is Cash Sale

		var invoiceObject 				= new Object();
		invoiceObject.status 			= "Success";
		invoiceObject.transactiontype 	= (finalTransformType == 'cashsale') ? "Cash Sale" : "Invoice";
		invoiceObject.transactionid 	= finalRecordId;
		invoiceObject.transactionNumber = nlapiLookupField(finalTransformType, finalRecordId, 'tranid');
		invoiceObject.transactiondate 	= TranDate;
		finalMessage.records.push(invoiceObject);

		ediStatus = 1;
		
		try
		{
			//Close Partially Fulfilled Orders
			record = nlapiLoadRecord('salesorder', recordId);
			var soStatus = record.getFieldValue('status');
			if(soStatus == 'Partially Fulfilled')
			{
				nlapiLogExecution('debug', 'Closing Partially Fulfilled Order: ' + i, orderId);
				for(var r = 1; r<=record.getLineItemCount('item'); r++)
				{
				   /* Only set items those are not completely fulfilled*/	
					// var lineQtyFulfilled = record.getLineItemValue('item', 'quantityfulfilled', r);	
					// var lineQtyOrdered	= record.getLineItemValue('item', 'quantity', 			r);	
					// if(lineQtyFulfilled < lineQtyOrdered)
					{
						record.setLineItemValue('item', 'isclosed', r, 'T');
					}
				}
				var recId = nlapiSubmitRecord(record, false, true);

				var invoiceMessage = 'Created Invoice: ' + recId;
				createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 7, SYNC_STATUS.CREATED, invoiceMessage, recId, 48); //Library call to WTKA_Library.js //Record Type: 7 is Invoice
			}
		}
		catch(close_err)
		{
			ediStatus	 = 3; // Exception
			// rollbackFlag = true; //Rollback if failed?

			var soObject 			= new Object();
			soObject.status 		= 'Exception';
			soObject.messagetype 	= "Partial Sales Order Failure";
			soObject.message 		= 'Sales order with Partially Fulfilled status could not be closed. Details as below.';
			soObject.details 		= close_err.getCode() + ' : ' + close_err.getDetails();
			finalMessage.records.push(soObject);
			nlapiLogExecution('debug', 'closePartialOrder - Error' , close_err);

			createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 7, SYNC_STATUS.ERROR, JSON.stringify(finalMessage), null, 49); //Library call to WTKA_Library.js  //Record Type: 7 is Invoice
		}
	}
	catch(err_transform)
	{
		ediStatus	 = 3; // Exception
		rollbackFlag = true;

		var invoiceObject 			= new Object();
		invoiceObject.status 		= 'Exception';
		invoiceObject.messagetype 	= "Cash Sale/Invoice Failure";
		invoiceObject.message 		= 'Cash Sale/Invoice could not be created. Details as below.';
		invoiceObject.details 		= err_transform.getCode() + ' : ' + err_transform.getDetails();
		finalMessage.records.push(invoiceObject);
		nlapiLogExecution('debug', orderId + ' createInvoice - Error' , err_transform);

		createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 5, SYNC_STATUS.ERROR, JSON.stringify(finalMessage), null, 50); //Library call to WTKA_Library.js  //Record Type: 5 is Cash Sale
	}
	nlapiLogExecution('debug', 'INVOICE RESPONSE ' + orderId, JSON.stringify(finalMessage));
	return finalRecordId;
}