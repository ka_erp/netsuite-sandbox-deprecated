/**
 * Company            Explore Consulting
 * Copyright            2013 Explore Consulting, LLC
 * Type                NetSuite EC_Suitelet_GetTermDays
 * Version            1.0.0.0
 * Description        Boilerplate startup code for a NetSuite RESTlet.
 **/


function getTermDays(request, response)
{
    try
    {
        // Retrieve Parameter Values
        var term = request.getParameter('term');
        nlapiLogExecution('Debug', 'getTermDays', 'term:  ' + term);

        var responseObject = new Object();
        responseObject.NumberOfDays = '';

        // Need to look at the Term record to see what the Net Due Number of Days is set to
        var days = nlapiLookupField('term', term, 'daysuntilnetdue');
        nlapiLogExecution('Debug', 'getTermDays', 'days:  ' + days);

        if ( days != '' && days != null )
        {
            responseObject.NumberOfDays = days;
        }
        else
        {
            responseObject.NumberOfDays = 365;
        }

        var responseString = JSON.stringify(responseObject);
        nlapiLogExecution('DEBUG', 'getTermDays', 'responseString: ' + responseString);

        response.write(responseString);

    }
    catch(e)
    {
        nlapiLogExecution('Debug', 'ERROR in EC_Suitelet_GetTermDays script -- getTermDays function', e);
    }
}