/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var VAT;
if (!VAT) VAT = {};
VAT.NONDEDUCTIBLE_NEXUS = {
		"AT":{"id":"13","nexuscode":"AT"}, "BE":{"id":"20","nexuscode":"BE"}, "BG":{"id":"22","nexuscode":"BG"}, 
		"CH":{"id":"42","nexuscode":"CH"}, "CL":{"id":"45","nexuscode":"CL"}, "CO":{"id":"48","nexuscode":"CO"},
		"CZ":{"id":"56","nexuscode":"CZ"}, "DE":{"id":"57","nexuscode":"DE"}, "DK":{"id":"59","nexuscode":"DK"},
		"ES":{"id":"68","nexuscode":"ES"}, "FI":{"id":"70","nexuscode":"FI"}, "GB":{"id":"77","nexuscode":"GB"},
		"ID":{"id":"101","nexuscode":"ID"}, "FR":{"id":"75","nexuscode":"FR"}, "IE":{"id":"102","nexuscode":"IE"},
		"IT":{"id":"110","nexuscode":"IT"}, "KR":{"id":"122","nexuscode":"KR"}, "LU":{"id":"134","nexuscode":"LU"},
		"MY":{"id":"158","nexuscode":"MY"}, "NL":{"id":"166","nexuscode":"NL"}, "NO":{"id":"167","nexuscode":"NO"},
		"NZ":{"id":"171","nexuscode":"NZ"}, "PE":{"id":"174","nexuscode":"PE"}, "PH":{"id":"177","nexuscode":"PH"},
		"PL":{"id":"179","nexuscode":"PL"}, "PT":{"id":"184","nexuscode":"PT"}, "RO":{"id":"189","nexuscode":"RO"},
		"RS":{"id":"50","nexuscode":"RS"}, "SE":{"id":"196","nexuscode":"SE"}, "SK":{"id":"201","nexuscode":"SK"},
		"SG":{"id":"197","nexuscode":"SG"}, "SI":{"id":"199","nexuscode":"SI"}, "TH":{"id":"215","nexuscode":"TH"},
		"TR":{"id":"222","nexuscode":"TR"}, "TW":{"id":"225","nexuscode":"TW"}, "UY":{"id":"231","nexuscode":"UY"},
		"ZA":{"id":"244","nexuscode":"ZA"}, "CY":{"id":"55","nexuscode":"CY"}, "UA":{"id":"227","nexuscode":"UA"},
		"KE":{"id":"115","nexuscode":"KE"}, "HU":{"id":"100","nexuscode":"HU"}, "VN":{"id":"238","nexuscode":"VN"}
		};

VAT.NonDeductible = function () {
	var featureid = "nondeductible";
	var context = nlapiGetContext();
	var isOneWorld = context.getSetting("FEATURE", "SUBSIDIARIES") == "T";
	var isAdvancedTaxes = context.getFeature('advtaxengine');
	var isMultiCurrency = context.getFeature("multicurrency");
	var isLocation = context.getFeature("LOCATIONS");
	var isDepartment = context.getFeature("DEPARTMENTS");
	var isClass = context.getFeature("CLASSES");
	
	var FIELDS = {
		transaction: "custbody_nondeductible_ref_genjrnl",
		isprocessed: "custbody_nondeductible_processed", 
		journal: "custbody_nondeductible_ref_tran"
	}

	this.run = setNonDeductibleCache;
	function setNonDeductibleCache(form, nexus, trantype, transactionMap, mode) {
		try {
			nlapiLogExecution("Debug", "Enable Feature:" + trantype, transactionMap[trantype].featurelist.indexOf(featureid));
			
			var tranfield = form.getField(FIELDS.transaction);
			var processfield = form.getField(FIELDS.isprocessed);
			if (transactionMap[trantype].featurelist.indexOf(featureid) == -1 && trantype != "GENJRNL") {
				if (tranfield) {
					tranfield.setDisplayType("hidden");
				}
				
				if (processfield) {
					processfield.setDisplayType("hidden");
				}
			
				return true;
			}

			if ((mode == 'copy' || mode == 'create') && trantype != "GENJRNL") {
				nlapiLogExecution("Debug", "Mode:" + trantype, mode);
				form.getField(FIELDS.isprocessed).setDefaultValue("F");
				form.getField(FIELDS.transaction).setDefaultValue(null);
				
				if (tranfield) {
					tranfield.setDisplayType("hidden");
				}
				
				if (processfield) {
					processfield.setDisplayType("hidden");
				}
			}
			
			if (!isITRNDSupported(trantype, nexus)) {
				if (!isSTCNDSupported(trantype, nexus)) {
					hideNonDeductibleFields(trantype);
				}
				return true;
			} else {
				var isnondedfield = form.addField("custpage_isnondeductible", "checkbox", "Is Non-Deductible Supported", null);
				if (isnondedfield) {
					isnondedfield.setDefaultValue("T");
					isnondedfield.setDisplayType("hidden");
				}
			}
			
			//Tax Codes
			var taxCodeMap = getNonDeductibleTaxCodes(nexus, trantype);
			var taxcodecache = taxCodeMap.taxCodes;
			var taxcache = form.addField("custpage_taxcodecache", "longtext", "Tax Code Cache", null);
			taxcache.setDefaultValue(JSON.stringify(taxcodecache));
			if (context.getLogLevel() != "DEBUG" && taxcache) {
				taxcache.setDisplayType("hidden");
			}
			
			//Non-deductible reference codes
			var nondeductiblecodes = taxCodeMap.referenceCodes;
			var nondeductiblefield = form.addField("custpage_nondeductiblecodes", "longtext", "Non-Deductible Tax Codes", null);
			nondeductiblefield.setDefaultValue(JSON.stringify(nondeductiblecodes));
			if (context.getLogLevel() != "DEBUG" && nondeductiblefield) {
				nondeductiblefield.setDisplayType("hidden");
			}
			
			//Buttons
			var expenseSublist = form.getSubList("expense");
			var itemSublist = form.getSubList("item");
			
			if (expenseSublist) {
			    expenseSublist.addButton("custpage_recompute_expense", "Calculate Non-Deductible Tax", "runNonDeductibleExpense()");
			}
			
			if (itemSublist) {
			    itemSublist.addButton("custpage_recompute_item", "Calculate Non-Deductible Tax", "runNonDeductibleItem()");
			}
			
			//Fields
			var logLevel = context.getLogLevel();
			var systemParameterManager = new VAT.SystemParameterManager();
			var columnCacheParameter = systemParameterManager.getByName('COLUMN_CACHE_' + transactionMap[trantype].internalid.toUpperCase());
			var fieldsmapcache = columnCacheParameter ? columnCacheParameter.value : '{}';
			
			var fieldcache = form.addField("custpage_fieldcache", "longtext", "Field Cache", null);
			fieldcache.setDefaultValue(fieldsmapcache);
			fieldcache.setDisplayType(logLevel == 'DEBUG' ? 'normal' : 'hidden');
			
			var checkpref = form.addField("custpage_checkpref", "longtext", "Line Check Preference", null);
			checkpref.setDefaultValue(getCheckPreference());
			checkpref.setDisplayType(logLevel == 'DEBUG' ? 'normal' : 'hidden');
		} catch(ex) {
			var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
			nlapiLogExecution("ERROR", "setNonDeductibleCache", errorMsg);
		}
	}
	
	this.updateJournalEntry = updateJournalEntry;
	function updateJournalEntry(mode, trantype, nexus, transactionMap) {
		nlapiLogExecution('Debug', 'updateJournalEntry', JSON.stringify({mode:mode, nexus:nexus, trantype:trantype}));
		try {
			if (transactionMap[trantype].featurelist.indexOf(featureid) == -1) {return true;}
				
			if (isITRNDSupported(trantype, nexus)) {
				var oldrec = nlapiGetOldRecord();
				var newrec = nlapiGetNewRecord();
				
				var result = checkTransaction(nexus, trantype, oldrec, newrec);
				
				nlapiLogExecution("Debug", "Check Transaction Result", JSON.stringify(result));
				if (!oldrec.getFieldValue(FIELDS.transaction) && result.hasNonDeductible) { //missing journals, create one.
					nlapiLogExecution("Debug", "updateJournalEntry", "Creating New Journal From Missing Journal During Update");
					createJournalEntry(mode, trantype, nexus, transactionMap);
					return;
				}
				
				if (result.isChanged && (!result.hasNonDeductible || result.isReversal)) {//reverse the journal
					var journalrecid = newrec.getFieldValue(FIELDS.transaction);
					var journalrec = nlapiLoadRecord("journalentry", journalrecid);
					journalrec.setFieldValue("reversaldate", nlapiDateToString(new Date()));
					
					nlapiSubmitRecord(unsetMandatoryRecord(journalrec), false, true);
					
					var refrec = nlapiLoadRecord(newrec.getRecordType(), newrec.getId());
					refrec.setFieldValue(FIELDS.transaction, "");
					refrec.setFieldValue(FIELDS.isprocessed, "F");
					nlapiSubmitRecord(refrec, false, true);
				} else if (result.isChanged) {
					var journalrecid = newrec.getFieldValue(FIELDS.transaction);
					var journalrec = nlapiLoadRecord("journalentry", journalrecid);
					
					var tranMap = getNonDeductibleLines(mode, newrec, trantype, nexus);
					nlapiLogExecution("Debug", "Transaction Map", JSON.stringify(tranMap));
					updateJournalEntryPerTran(tranMap, journalrec, oldrec, newrec, trantype);
				}
			}
		} catch(ex) {
			var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
			nlapiLogExecution("ERROR", "updateJournalEntry", errorMsg);
		}
	}
	
	function updateJournalEntryPerTran(tranMap, record, oldrec, newrec, trantype) {
		try {
			nlapiLogExecution('Debug', 'updateJournalEntryPerTran', JSON.stringify(tranMap));
			
			var taxcontrolaccountregister = {};
			//wipe out the lines
			var linecount = record.getLineItemCount("line");
			for(var iline = 1; iline <= linecount; iline++) {
				record.removeLineItem("line", 1);
			}
			
			for(var itran in tranMap) { //iterate over transactions
				var transaction = tranMap[itran];
				//if account posting period changed
				if (oldrec.getFieldValue("postingperiod") != newrec.getFieldValue("postingperiod")) {
					record.setFieldValue("postingperiod", newrec.getFieldValue("postingperiod"));
				}
				
				//if transaction date changed
				if (oldrec.getFieldValue("trandate") != newrec.getFieldValue("trandate")) {
					record.setFieldValue("trandate", newrec.getFieldValue("trandate"));
				}
				
				if (isMultiCurrency) {
					if (transaction.currency) {
						record.setFieldValue("currency", transaction.currency);
					}
					
					if (transaction.exchangerate) {
						record.setFieldValue("exchangerate", transaction.exchangerate);
					}
				}
				
				if (isLocation && transaction.location) {
					record.setFieldValue("location", transaction.location);
				}
				
				if (isDepartment && transaction.department) {
					record.setFieldValue("department", transaction.department);
				}
				
				if (isClass && transaction.class) {
					record.setFieldValue("class", transaction.class);
				}
				
				var transactionLine = transaction.line;
				var type = transaction.type;
				var tranno = transaction.tranno;
				
				nlapiLogExecution("Audit", "createJournalEntryPerTran: No of Lines:" + itran, transactionLine.length);
				nlapiLogExecution("Debug", "createJournalEntryPerTran:" + itran, JSON.stringify(transactionLine));
				
				for(var iline=0; iline < transactionLine.length; iline++) {
					var line = transactionLine[iline];
					var amount = Math.abs(nlapiFormatCurrency(line.taxamount));
					record.selectNewLineItem("line");
					record.setCurrentLineItemValue("line", "account", line.nondeductibleaccount);
					record.setCurrentLineItemValue("line", (trantype=="BILLCRED"?"credit":"debit"), amount);
					record.setCurrentLineItemValue("line", (trantype=="BILLCRED"?"origcredit":"origdebit"), amount);
					record.setCurrentLineItemValue("line", "memo", 
						["Non-deductible adjustment for ", type, tranno, "(" + itran + ")"].join(" "));
					
					if (isLocation && line.location) {
						record.setCurrentLineItemValue("line", "location", line.location);
					}
					
					if (isDepartment && line.department) {
						record.setCurrentLineItemValue("line", "department", line.department);
					}
					
					if (isClass && line.class) {
						record.setCurrentLineItemValue("line", "class", line.class);
					}					
					record.commitLineItem("line");
					//get the control account
					var taxaccount;
					if (taxcontrolaccountregister[line.taxcodeid]) {
						taxaccount = taxcontrolaccountregister[line.taxcodeid];
					} else {
						taxaccount = getTaxControlAccount(line.taxcodeid);
						taxcontrolaccountregister[line.taxcodeid] = taxaccount;
					}

					record.selectNewLineItem("line");
					record.setCurrentLineItemValue("line", "account", taxaccount.purchaseacct);
					record.setCurrentLineItemValue("line", (trantype=="BILLCRED"?"debit":"credit"), amount);
					record.setCurrentLineItemValue("line", (trantype=="BILLCRED"?"origdebit":"origcredit"), amount);
					record.setCurrentLineItemValue("line", "memo", 
						["Non-deductible adjustment for ", type, tranno, "(" + itran + ")"].join(" "));
					
					if (isLocation && line.location) {
						record.setCurrentLineItemValue("line", "location", line.location);
					}
					
					if (isDepartment && line.department) {
						record.setCurrentLineItemValue("line", "department", line.department);
					}
					
					if (isClass && line.class) {
						record.setCurrentLineItemValue("line", "class", line.class);
					}					
					record.commitLineItem("line");
				}
				nlapiSubmitRecord(unsetMandatoryRecord(record), false, true);
			}
		} catch(ex) {
			var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
			nlapiLogExecution("ERROR", "updateJournalEntryPerTran", errorMsg);
		}
	}
	
	this.createJournalEntry = createJournalEntry;
	function createJournalEntry(mode, trantype, nexus, transactionMap) {
		try {
			nlapiLogExecution('Debug', 'createJournalEntry', JSON.stringify({mode:mode, nexus:nexus, trantype:trantype}));
			if (transactionMap[trantype].featurelist.indexOf(featureid) ==-1) {return true;}
				
			if (isITRNDSupported(trantype, nexus)) {
				var newrec = nlapiGetNewRecord();
				
				var tranMap = getNonDeductibleLines(mode, newrec, trantype, nexus);
				createJournalEntryPerTran(tranMap, trantype);
			}
		} catch(ex) {
			var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
			nlapiLogExecution("ERROR", "createJournalEntry", errorMsg);
		}
	}
	
	function createJournalEntryPerTran(tranMap, trantype) {
		nlapiLogExecution('Debug', 'createJournalEntryPerTran: Map', JSON.stringify(tranMap));
		try {
			for(var itran in tranMap) { //iterate over transactions
				var transaction = tranMap[itran];
				var journal;
				if (isOneWorld) {
					journal = nlapiCreateRecord('journalentry', {subsidiary: transaction.subsidiary});
				} else {
					journal = nlapiCreateRecord('journalentry');
				}
				journal.setFieldValue("trandate", transaction.trandate);
				
				if (isMultiCurrency) {
					if (transaction.currency) {
						journal.setFieldValue("currency", transaction.currency);
					}
					if (transaction.exchangerate) {
						journal.setFieldValue("exchangerate", transaction.exchangerate);
					}
				}
				
				if (isLocation && transaction.location) {
					journal.setFieldValue("location", transaction.location);
				}
				
				if (isDepartment && transaction.department) {
					journal.setFieldValue("department", transaction.department);
				}
				
				if (isClass && transaction.class) {
					journal.setFieldValue("class", transaction.class);
				}
				
				var transactionLine = transaction.line;
				var type = transaction.type;
				var tranno = transaction.tranno;
				
				for(var iline =0; iline < transactionLine.length; iline++) {
					var line = transactionLine[iline];
					var amount = Math.abs(nlapiFormatCurrency(line.taxamount));
					journal.selectNewLineItem("line");
					journal.setCurrentLineItemValue("line", "account", line.nondeductibleaccount);
					journal.setCurrentLineItemValue("line", (trantype=="BILLCRED"?"credit":"debit"), amount);
					journal.setCurrentLineItemValue("line", (trantype=="BILLCRED"?"origcredit":"origdebit"), amount);
					journal.setCurrentLineItemValue("line", "memo", 
						["Non-deductible adjustment for ", type, tranno, "(" + itran + ")"].join(" "));
					
					if (line.customer) {
						journal.setCurrentLineItemValue("line", "entity", line.customer);
					}
					
					if (isLocation && line.location) {
						journal.setCurrentLineItemValue("line", "location", line.location);
					}
					
					if (isDepartment && line.department) {
						journal.setCurrentLineItemValue("line", "department", line.department);
					}
					
					if (isClass && line.class) {
						journal.setCurrentLineItemValue("line", "class", line.class);
					}
					
					journal.commitLineItem("line");
					//get the control account
					var taxaccount = getTaxControlAccount(line.taxcodeid);
					
					journal.selectNewLineItem("line");
					journal.setCurrentLineItemValue("line", "account", taxaccount.purchaseacct);
					journal.setCurrentLineItemValue("line", (trantype=="BILLCRED"?"debit":"credit"), amount);
					journal.setCurrentLineItemValue("line", (trantype=="BILLCRED"?"origdebit":"origcredit"), amount);
					journal.setCurrentLineItemValue("line", "memo", 
						["Non-deductible adjustment for ", type, tranno, "(" + itran + ")"].join(" "));
					
					if (line.customer) {
						journal.setCurrentLineItemValue("line", "entity", line.customer);
					}
					
					if (isLocation && line.location) {
						journal.setCurrentLineItemValue("line", "location", line.location);
					}
					
					if (isDepartment && line.department) {
						journal.setCurrentLineItemValue("line", "department", line.department);
					}
					
					if (isClass && line.class) {
						journal.setCurrentLineItemValue("line", "class", line.class);
					}
					
					journal.commitLineItem("line");
				}
				journal.setFieldValue(FIELDS.journal, itran);
				
				var journalid = nlapiSubmitRecord(unsetMandatoryRecord(journal), false, true);
				var refrec = nlapiLoadRecord(type, itran);
				refrec.setFieldValue(FIELDS.transaction, journalid);
				refrec.setFieldValue(FIELDS.isprocessed, "T");
				nlapiSubmitRecord(refrec, false, true);
				
				nlapiLogExecution("Debug", "createJournalEntryPerTran: Result", JSON.stringify({journalid:journalid, type:type, internalid:itran}));
			}
		} catch(ex) {
			var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
			nlapiLogExecution("ERROR", "createJournalEntryPerTran", errorMsg);
		}
	}
	
	this.getTaxControlAccount = getTaxControlAccount; 
	function getTaxControlAccount(taxcodeid) {
		var taxcontrolaccount = {};
		try {
			if (!taxcontrolaccount.purchaseacct || !taxcontrolaccount.saleacct) {
				var rec = nlapiLoadRecord('salestaxitem', taxcodeid);
				taxcontrolaccount.purchaseacct = rec.getFieldValue('purchaseaccount');
				taxcontrolaccount.saleacct = rec.getFieldValue('saleaccount');
			} 
		} catch(ex) {
			var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
			nlapiLogExecution("ERROR", "getTaxControlAccount", errorMsg);
		}
		return taxcontrolaccount;
	}
	
	function getNonDeductibleLines(mode, rec, trantype, nexus) {
		var tranmap = {};
		try {
//			var nondeductiblecodes = getNonDeductibleReferenceCodes(nexus, trantype);
		    var taxCodeMap = getNonDeductibleTaxCodes(nexus, trantype);
            var nondeductiblecodes = taxCodeMap.referenceCodes;
			var itemcount = rec.getLineItemCount("item");
			var expensecount = rec.getLineItemCount("expense"); 	
			
			var internalid = rec.getId();
			var status = rec.getFieldValue("status");
			var type = rec.getRecordType();
			var tranno = rec.getFieldValue("tranid");
			var trandate = rec.getFieldValue("trandate");
			var subsidiaryid = isOneWorld?rec.getFieldValue("subsidiary"):1;
			var currencyid = isMultiCurrency?rec.getFieldValue("currency"):"";
			var exchangerate = isMultiCurrency?rec.getFieldValue("exchangerate"):"";
			var class = isClass?rec.getFieldValue("class"):"";
			var department = isDepartment?rec.getFieldValue("department"):""; 
			var location = isLocation?rec.getFieldValue("location"):"";
			
			var approvalstatus = rec.getFieldValue("approvalstatus");
			var accountingapproval = rec.getFieldValue("accountingapproval") == "T";
			
			///nlapiLogExecution("Debug", "getNonDeductibleLines", JSON.stringify({approvalstatus:approvalstatus, accountingapproval:accountingapproval, itemcount:itemcount, expensecount:expensecount}));
			
			
			if (trantype == "BILL") {
				if (approvalstatus != 2) return tranmap; 
			} else if (trantype == "EXPREPT") {
			    var config = nlapiLoadConfiguration('accountingpreferences')
			    var isExpenseAcctPref = config.getFieldValue("customapprovalexpense") == "T";
			    
				if (isExpenseAcctPref) {
					if (approvalstatus != 2) return tranmap;
				} else if (!accountingapproval && mode != 'approve'){
					return tranmap;
				}
			}
			
			tranmap[internalid] = {internalid: internalid, status: status, type: type, subsidiary: subsidiaryid,
					class: class?class:"", department:department?department:"", location:location?location:"", 
					trandate: trandate, currency: currencyid?currencyid:"", exchangerate: exchangerate?exchangerate:"", tranno:tranno, line:[]};

			var tranlinelist = tranmap[internalid].line;
			//Items
			for(var itemrow = 0; itemrow < itemcount; itemrow++) {
				var itemtaxcodeid = rec.getLineItemValue("item", "taxcode", itemrow+1);
				var itemnondeductibleaccountid = rec.getLineItemValue("item", "custcol_nondeductible_account", itemrow+1);
				var taxamount = rec.getLineItemValue("item", "tax1amt", itemrow+1);
				var itemdesc = rec.getLineItemValue("item", "item_display", itemrow+1);  
				
				var itemclass = rec.getLineItemValue("item", "class", itemrow+1);
				var itemdepartment = rec.getLineItemValue("item", "department", itemrow+1);
				var itemlocation = rec.getLineItemValue("item", "location", itemrow+1);
				var itemcustomer = rec.getLineItemValue("item", "customer", itemrow+1);
				
				nlapiLogExecution("Debug", "itemrow:" + itemrow, JSON.stringify(
						{itemtaxcodeid:itemtaxcodeid, nondeductiblecodes:nondeductiblecodes, itemnondeductibleaccountid:itemnondeductibleaccountid}));
				
				if (!nondeductiblecodes[itemtaxcodeid]) {continue;} //not non-deductible line
				else if (!itemnondeductibleaccountid) {continue;}

				var itemrowobj = {taxcodeid: itemtaxcodeid, taxamount: taxamount, item: itemdesc, customer: itemcustomer?itemcustomer:"",
						class: itemclass?itemclass:"", department: itemdepartment?itemdepartment:"", location: itemlocation?itemlocation:"", 
						nondeductibleaccount: itemnondeductibleaccountid};
				
				tranlinelist.push(itemrowobj);
			}
			
			//Expenses
			for(var expenserow = 0; expenserow < expensecount; expenserow++) {
				var expensetaxcodeid = rec.getLineItemValue("expense", "taxcode", expenserow+1);
				
				var expensenondeductibleaccountid = rec.getLineItemValue("expense", "custcol_nondeductible_account", expenserow+1);
				var taxamount = rec.getLineItemValue("expense", "tax1amt", expenserow+1);
				var accountdesc =  rec.getLineItemValue("expense", "account_display", expenserow+1);
				
				if (trantype == "EXPREPT") {
					accountdesc =  rec.getLineItemValue("expense", "category_display", expenserow+1);
				}
				
				var expensecustomer = rec.getLineItemValue("expense", "customer", expenserow+1);
				var expenseclass = rec.getLineItemValue("expense", "class", expenserow+1);
				var expensedepartment = rec.getLineItemValue("expense", "department", expenserow+1);
				var expenselocation = rec.getLineItemValue("expense", "location", expenserow+1);

				nlapiLogExecution("Debug", "expenserow:" + expenserow, JSON.stringify(
						{expensetaxcodeid:expensetaxcodeid, nondeductiblecodes:nondeductiblecodes, expensenondeductibleaccountid:expensenondeductibleaccountid}));
				
				if (!nondeductiblecodes[expensetaxcodeid]) {continue;} //not non-deductible line
				else if (!expensenondeductibleaccountid) {continue;}
				
				tranlinelist.push({taxcodeid: expensetaxcodeid, 
					taxamount: taxamount, item: accountdesc, customer: expensecustomer?expensecustomer:"",  
					class: expenseclass?expenseclass:"", department: expensedepartment?expensedepartment:"", location: expenselocation?expenselocation:"",
					nondeductibleaccount: expensenondeductibleaccountid});
			}
		} catch(ex) {
			var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
			nlapiLogExecution("ERROR", "getNonDeductibleLines", errorMsg);
		}
		return tranmap;
	}
	
	function checkTransaction(nexus, trantype, oldrec, newrec) {
		var isChanged = false;
		var hasNonDeductible = false;
		var isReversal = false;
		
		try {
//			var nondeductiblecodes = getNonDeductibleReferenceCodes(nexus, trantype);
		    var taxCodeMap = getNonDeductibleTaxCodes(nexus, trantype);
            var nondeductiblecodes = taxCodeMap.referenceCodes;
			
			if (oldrec.getFieldValue("trandate") != newrec.getFieldValue("trandate")) { //check the transaction date
				isChanged = true;
			} else if (isMultiCurrency && oldrec.getFieldValue("exchangerate") != newrec.getFieldValue("exchangerate")) { //check the exchange rate
				isChanged = true;
			} else {
				switch(trantype) {
					case "BILL": case "CHK": case "BILLCRED":
						var olditemcount = oldrec.getLineItemCount("item");
						var newitemcount = newrec.getLineItemCount("item");
						
						var oldexpensecount = oldrec.getLineItemCount("expense");
						var newexpensecount = newrec.getLineItemCount("expense");
						
						nlapiLogExecution("Debug", "Item Count:" + olditemcount, newitemcount);
						nlapiLogExecution("Debug", "Expense Count:" + oldexpensecount, newexpensecount);
						
						if (olditemcount != newitemcount || oldexpensecount != newexpensecount) {
							isChanged = true;
						} else { //check each line item
							for(var iitem = 1; iitem <= olditemcount; iitem++) {
								//check the tax code
								var olditemtaxcode = oldrec.getLineItemValue("item", "taxcode", iitem);
								var newitemtaxcode = newrec.getLineItemValue("item", "taxcode", iitem);
								if (olditemtaxcode != newitemtaxcode && (nondeductiblecodes[olditemtaxcode] || nondeductiblecodes[newitemtaxcode])) {
									isChanged = true;
									break;
								} else if (!nondeductiblecodes[olditemtaxcode]){ //ignore lines that is non-deductible
									continue;
								}
								
								//tax amount changed
								var oldtaxamount = oldrec.getLineItemValue("item", "tax1amt", iitem);
								var newtaxamount = newrec.getLineItemValue("item", "tax1amt", iitem);
								
								if (oldtaxamount != newtaxamount) {
									isChanged = true;
									break;
								}
								
								//account changed
								var oldexpenseaccount = oldrec.getLineItemValue("item", "custcol_nondeductible_account", iitem);
								var newexpenseaccount = newrec.getLineItemValue("item", "custcol_nondeductible_account", iitem);
								
								if (oldexpenseaccount != newexpenseaccount) {
									isChanged = true;
									break;
								}
							}
							
							for(var iexpense = 1; iexpense <= oldexpensecount; iexpense++) {
								//check the tax code
								var olditemtaxcode = oldrec.getLineItemValue("expense", "taxcode", iexpense);
								var newitemtaxcode = newrec.getLineItemValue("expense", "taxcode", iexpense);
								
								if (olditemtaxcode != newitemtaxcode && (nondeductiblecodes[olditemtaxcode] || nondeductiblecodes[newitemtaxcode])) {
									isChanged = true;
									break;
								} else if (!nondeductiblecodes[olditemtaxcode]){ //ignore lines that is non-deductible
									continue;
								}
								
								//tax amount changed
								var oldtaxamount = oldrec.getLineItemValue("expense", "tax1amt", iexpense);
								var newtaxamount = newrec.getLineItemValue("expense", "tax1amt", iexpense);
								
								if (oldtaxamount != newtaxamount) {
									isChanged = true;
									break;
								}
								
								//account changed
								var oldexpenseaccount = oldrec.getLineItemValue("expense", "custcol_nondeductible_account", iexpense);
								var newexpenseaccount = newrec.getLineItemValue("expense", "custcol_nondeductible_account", iexpense);
								
								if (oldexpenseaccount != newexpenseaccount) {
									isChanged = true;
									break;
								}
							}
						}
						break;
					case "EXPREPT":
					    var config = nlapiLoadConfiguration('accountingpreferences')
					    var isExpenseAcctPref = config.getFieldValue("customapprovalexpense") == "T";
					    
						var oldexpensecount = oldrec.getLineItemCount("expense");
						var newexpensecount = newrec.getLineItemCount("expense");

						var oldapprovalstatus = oldrec.getFieldValue("approvalstatus") == "2";
						var newapprovalstatus = newrec.getFieldValue("approvalstatus") == "2";
						
						var oldaccountingapproval = oldrec.getFieldValue("accountingapproval") == "T";
						var newaccountingapproval = newrec.getFieldValue("accountingapproval") == "T";						

						nlapiLogExecution("Debug", "Expense Count:" + oldexpensecount, newexpensecount);
						nlapiLogExecution("Debug", "Accounting Approval", 
								JSON.stringify({oldapprovalstatus:oldapprovalstatus, newapprovalstatus:newapprovalstatus, 
									oldaccountingapproval:oldaccountingapproval, newaccountingapproval:newaccountingapproval}
								));
						
						if (isExpenseAcctPref) {
							if (oldapprovalstatus && !newapprovalstatus) { //revoked
								isReversal = true;
								isChanged = true;
							} else if (!oldapprovalstatus && newapprovalstatus) { //approved
								isChanged = true;
							}
						} else {
							if (oldaccountingapproval && !newaccountingapproval) { //revoked
								isReversal = true;
								isChanged = true;
							} else if (!oldaccountingapproval && newaccountingapproval) { //approved
								isChanged = true;
							} 
						}
						
						if (oldexpensecount != newexpensecount) {
							isChanged = true;
						} else {
							for(var iexpense = 1; iexpense <= oldexpensecount; iexpense++) {
								//check the tax code
								var olditemtaxcode = oldrec.getLineItemValue("expense", "taxcode", iexpense);
								var newitemtaxcode = newrec.getLineItemValue("expense", "taxcode", iexpense);
								
								if (olditemtaxcode != newitemtaxcode && (nondeductiblecodes[olditemtaxcode] || nondeductiblecodes[newitemtaxcode])) {
									isChanged = true;
									break;
								} else if (!nondeductiblecodes[olditemtaxcode]){ //ignore lines that is non-deductible
									continue;
								}
								
								//tax amount changed
								var oldtaxamount = oldrec.getLineItemValue("expense", "tax1amt", iexpense);
								var newtaxamount = newrec.getLineItemValue("expense", "tax1amt", iexpense);
								
								if (oldtaxamount != newtaxamount) {
									isChanged = true;
									break;
								}
								
								//account changed
								var oldexpenseaccount = oldrec.getLineItemValue("expense", "custcol_nondeductible_account", iexpense);
								var newexpenseaccount = newrec.getLineItemValue("expense", "custcol_nondeductible_account", iexpense);
								
								if (oldexpenseaccount != newexpenseaccount) {
									isChanged = true;
									break;
								}
							}
						}
						break;
				}
			}
			
			//check if there are still non-deductible codes
			switch(trantype) {
				case "BILL": case "CHK": case "BILLCRED":
					var newitemcount = newrec.getLineItemCount("item");
					var newexpensecount = newrec.getLineItemCount("expense");
					
					for(var iitem = 1; iitem <= newitemcount; iitem++) {
						var newitemtaxcode = newrec.getLineItemValue("item", "taxcode", iitem);
						if (nondeductiblecodes[newitemtaxcode]){ 
							hasNonDeductible = true;
							break;
						}
					}
						
					for(var iexpense = 1; iexpense <= newexpensecount; iexpense++) {
						var newitemtaxcode = newrec.getLineItemValue("expense", "taxcode", iexpense);
						if (nondeductiblecodes[newitemtaxcode]){ //ignore lines that is non-deductible
							hasNonDeductible = true;
							break;
						}
					}
					break;
				case "EXPREPT":
					var newexpensecount = newrec.getLineItemCount("expense");
					for(var iexpense = 1; iexpense <= newexpensecount; iexpense++) {
						var newitemtaxcode = newrec.getLineItemValue("expense", "taxcode", iexpense);
						if (nondeductiblecodes[newitemtaxcode]){ //ignore lines that is non-deductible
							hasNonDeductible = true;
							break;
						}
					}
					break;
			}
		} catch(ex) {
			var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
			nlapiLogExecution("ERROR", "isTransactionChanged", errorMsg);
		}
		
		return {isChanged: isChanged, hasNonDeductible:hasNonDeductible, isReversal: isReversal};
	}
	
	this.isITRNDSupported = isITRNDSupported;
	function isITRNDSupported(trantype, nexus) {
		nlapiLogExecution('Debug', 'params', JSON.stringify({nexus:nexus, trantype:trantype}));
		try {
			var isitrND = false;
			var journalLink = nlapiGetFieldValue('custbody_nondeductible_ref_genjrnl');
			
			if (journalLink) {
				isitrND = true;
				return isitrND;
			}
			
			if (trantype == 'EXPREPT') {
				isitrND = isNexusNDSupported(trantype, nexus);
			}
			
			return isitrND;
		} catch(ex) {
			var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
			nlapiLogExecution("ERROR", "isITRNDSupported", errorMsg);
		}
	}
	
	this.isSTCNDSupported = isSTCNDSupported;
	function isSTCNDSupported(trantype, nexus) {
		nlapiLogExecution('Debug', 'params', JSON.stringify({nexus:nexus, trantype:trantype}));
		try {
			var isstcND = false;
			isstcND = isNexusNDSupported(trantype, nexus);
			return isstcND;
		} catch(ex) {
			var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
			nlapiLogExecution("ERROR", "isSTCNDSupported", errorMsg);
		}
	}
	
	this.isNexusNDSupported = isNexusNDSupported;
	function isNexusNDSupported(trantype, nexus) {
		
		try {
			var supportednexus = VAT.NONDEDUCTIBLE_NEXUS;
			var isnexusND = false;
				
			if (supportednexus[nexus] && trantype != "GENJRNL") {
				nlapiLogExecution('Debug', 'Non-Journal:' + trantype, JSON.stringify(supportednexus[nexus]));
				isnexusND = true;
					
			} else if (trantype == "GENJRNL") { //check if there is nexus.
				var nexuses = getAllNexuses();
				nlapiLogExecution('Debug', 'All Nexuses', JSON.stringify(nexuses));
				for(var inexus = 0; inexus < nexuses.length; inexus++) { //only need 1 hit to be true.
					if (supportednexus[nexuses[inexus]]) {
						isnexusND = true;
						break;
					}
				}
			}
			
			return isnexusND;
		} catch(ex) {
			var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
			nlapiLogExecution("ERROR", "isNexusNDSupported", errorMsg);
		}
	}
	
	this.getNonDeductibleNexus = getNonDeductibleNexus;
	function getNonDeductibleNexus() {
		var nexuses = {};
		
		try {
			var filter = [new nlobjSearchFilter("custrecord_is_nondeductible", null, "is", 'T')];
			var column = [new nlobjSearchColumn("custrecord_country_name"), new nlobjSearchColumn("name"), new nlobjSearchColumn("custrecord_alt_code")];
			var rs = nlapiSearchRecord('customrecord_tax_report_map', null, filter, column)
				
			for(var irow in rs) {
				var countrycode = rs[irow].getValue('name');
				var countryid = rs[irow].getValue('custrecord_country_name');
				var altcountrycode = rs[irow].getValue('custrecord_alt_code');
				
				nexuses[countrycode] = {id: countryid, nexuscode: countrycode};
				if (altcountrycode) {
					nexuses[altcountrycode] = {id: countryid, nexuscode: altcountrycode};
				}
			}
		} catch(ex) {
			var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
			nlapiLogExecution("ERROR", "getNonDeductibleNexus", errorMsg);
		}
		return nexuses;
	}
	
	function getNonDeductibleTaxCodes(nexus, trantype) {
	    var map = {
	        taxCodes: {},
	        referenceCodes: {}
	    };
	    
	    try {
	        var columns = [
                new nlobjSearchColumn('rate'),
                new nlobjSearchColumn('custrecord_4110_nondeductible_parent'),
                new nlobjSearchColumn('rate', 'custrecord_4110_nondeductible_parent'),
                new nlobjSearchColumn('custrecord_4110_nondeductible_account'),
                new nlobjSearchColumn('custrecord_4110_non_deductible')
            ];
            
            var filters = [
                new nlobjSearchFilter('custrecord_4110_nondeductible_parent', null, 'noneof', '@NONE@', null, 1, 0, true),
                new nlobjSearchFilter('custrecord_4110_non_deductible', null, 'is', 'T', null, 0, 1, false)
            ];
            
            if (isAdvancedTaxes && trantype != 'GENJRNL') {
                filters.push(new nlobjSearchFilter('country', null, 'is', nexus));
            }
            
            var rs = nlapiSearchRecord('salestaxitem', null, filters, columns);
            
            for (var i = 0; i < rs.length; i++) {
                if (rs[i].getValue('custrecord_4110_non_deductible') == 'T') {
                    map.referenceCodes[rs[i].getId()] = {
                        rate: rs[i].getValue('rate'), 
                        account: rs[i].getValue('custrecord_4110_nondeductible_account')
                    };
                } else {
                    var rate = parseFloat(rs[i].getValue('rate'));
                    var nondeductiblerate = parseFloat(rs[i].getValue('rate', 'custrecord_4110_nondeductible_parent'));
                    var deductiblerate = 100 - nondeductiblerate; 
                    
                    map.taxCodes[rs[i].getId()] = {
                        taxcodeid: rs[i].getValue('custrecord_4110_nondeductible_parent'),
                        rate: rate,
                        reclaimablerate: deductiblerate,
                        nonreclaimablerate: nondeductiblerate,
                        account: rs[i].getValue('custrecord_4110_nondeductible_account')
                    };
                }
            }
	    } catch(ex) {
	        var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
	        nlapiLogExecution('ERROR', 'getNonDeductibleTaxCodes', errorMsg);
	    }
	    
	    return map;
	}
	
	this.hideNonDeductibleFields = hideNonDeductibleFields;
	function hideNonDeductibleFields(trantype) {
		try {
			var expensecount = -1;
			var itemcount = -1;
			var linecount = -1;
			
			switch (trantype) {
				case "BILL": case "BILLCRED": case "CHK":
					expensecount = nlapiGetLineItemCount("expense");
					itemcount = nlapiGetLineItemCount("item");
					break;
				case "GENJRNL":
					linecount = nlapiGetLineItemCount("line");
					break;
				case "EXPREPT":
					expensecount = nlapiGetLineItemCount("expense");
					break;
			}
			expensecount = (expensecount == 0)?1:expensecount;
			itemcount = (itemcount == 0)?1:itemcount;
			linecount = (linecount == 0)?1:linecount;
			
			if (trantype == "GENJRNL") {
				nlapiGetField(FIELDS.journal).setDisplayType("hidden");	
			} else {
				nlapiGetField(FIELDS.isprocessed).setDisplayType("hidden");
				nlapiGetField(FIELDS.transaction).setDisplayType("hidden");
			}
			
			for(var iexpense = 1; iexpense <= expensecount; iexpense++) {
				nlapiGetLineItemField("expense", "custcol_nondeductible_account", iexpense).setDisplayType("hidden");
			}
			
			for(var iitem = 1; iitem <= itemcount; iitem++) {
				nlapiGetLineItemField("item", "custcol_nondeductible_account", iitem).setDisplayType("hidden");
			}
			
			for(var iline = 1; iline <= linecount; iline++) {
				nlapiGetLineItemField("line", "custcol_nondeductible_account", iline).setDisplayType("hidden");
			}
		} catch(ex) {
			var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
			nlapiLogExecution("ERROR", "hideNonDeductibleFields", errorMsg);
		}
	}
	
	this.getCheckPreference = getCheckPreference;
	function getCheckPreference() { //subsidiaryid optional
		var ischeckline = "T";
		try {
			var filters = [new nlobjSearchFilter("isinactive", null, "is", 'F'), 
			    new nlobjSearchFilter("custrecord_vat_cfg_name", null, "is", 'custpage_nonded_checkline'),
			    new nlobjSearchFilter("custrecord_vat_cfg_nondeductible", null, "is", 'T')];
			
			var columns = [new nlobjSearchColumn("custrecord_vat_cfg_value")];
			if (isOneWorld) {
				var subid = nlapiGetFieldValue("subsidiary"); 
				if (!isNaN(parseInt(subid)) && !isNaN(Number(subid))) {
					filters.push(new nlobjSearchFilter("custrecord_vat_cfg_subsidiary", null, "is", nlapiGetFieldValue("subsidiary")))
				}
			} 
			
			var rs = nlapiSearchRecord("customrecord_tax_return_setup_item", null, filters, columns);
			
			if (rs) {
				ischeckline = rs[0].getValue("custrecord_vat_cfg_value");
			}
		} catch(ex) {
			var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
			nlapiLogExecution("ERROR", "getCheckPreference", errorMsg);
		}
		
		return ischeckline;
	}
	
	function unsetMandatoryRecord(record) {
		try {
			var count = record.getLineItemCount("line");
			var columnfields = record.getAllLineItemFields("line");
			for(var irow = 0; irow < count; irow++) {
				  for(var ifield = 0; ifield < columnfields.length; ifield++) {
				      var field = record.getLineItemField("line", columnfields[ifield], irow+1);
				      if (field) field.setMandatory(false);
				  }
			}
		} catch(ex) {
			var errorMsg = ex.getCode != null ? ex.getCode() + ': ' + ex.getDetails() : 'Error: ' + (ex.message != null ? ex.message : ex);
			nlapiLogExecution("ERROR", "unsetMandatoryRecord", errorMsg);
		}
		
		return record;
	}
}
