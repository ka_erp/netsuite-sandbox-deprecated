/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var Tax = Tax || {};

Tax.ReportFormatter = function ReportFormatter(params) {
	this.formatter = new VAT.Report.FormatterSingleton.getInstance(params.subsidiary, params.countryCode, params.languageCode);
	this.context = params.context || nlapiGetContext();
};

Tax.ReportFormatter.prototype.format = function format(value, type, format) {

	var formattedValue = value;
	var displayCurrency = false;

	switch(type) {
		case 'date':
			var dateFormat = format ? format : this.getDefaultDateFormat();
			formattedValue = this.formatter.formatDate(value, dateFormat, false);
			break;
		case 'numeric':
			formattedValue = this.formatter.formatCurrency(value, displayCurrency, format); //should displaying of currency be supported?
			break;
		case 'floornumeric':
			var formatResult = (this.formatter.formatCurrency(value, displayCurrency, format) || '').toString();
		    var match = formatResult.match(/(\.|,)([^(.|,)]*?)$/); // look for ',' or '.', whichever occurs last
		    formattedValue = match ? formatResult.substr(0, match.index) : formatResult;
		    break;
		case 'regexp':
			formattedValue = this.formatter.formatRegExpString(value, format);
			break;
		default: // ?
			nlapiLogExecution('DEBUG', 'Tax.ReportFormatter.prototype.format', 'No formatting defined for type "' + type + '"');
			break;
	}

	return formattedValue;
};

Tax.ReportFormatter.prototype.getDefaultDateFormat = function getDefaultDateFormat() {
    return this.context.getPreference('DATEFORMAT').toLowerCase().replace("mm", "MM").replace("month", "MMMM").replace("mon", "MMM");
};