/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Mar 2015     dotocka
 *
 */

//Payment providers section
var MPS = "2";
var Shift4 = "3";

var MPSFields = ["custrecord_ra_provider_info_xservers", "custrecord_ra_provider_info_gservers", "custrecord_ra_provider_info_proxyserver", "custrecord_ra_provider_info_isemv", "custrecord_ra_provider_info_terminalid","custrecord_ra_provider_info_comport"];
var Shift4Fields = ["custrecord_ra_provider_info_url", "custrecord_ra_provider_info_accesscode", "custrecord_ra_provider_info_serialnumber", "custrecord_ra_provider_info_terminalid", "custrecord_ra_provider_info_newutg", "custrecord_ra_provider_info_clientguid", "custrecord_ra_provider_info_authtoken"];
var CommonProviderFields = ["custrecord_ra_provider_info_merchant"];

var providerProperties = { custrecord_ra_cc_provider  : undefined,
		custrecord_ra_provider_info_username : undefined,
		custrecord_ra_provider_info_processor : undefined,
		custrecord_ra_provider_info_merchant : undefined,
		custrecord_ra_provider_info_folder: undefined,
		custrecord_ra_provider_info_xservers : undefined,
		custrecord_ra_provider_info_gservers : undefined,
		custrecord_ra_provider_info_proxyserver : undefined,
		custrecord_ra_provider_info_url : undefined,
		custrecord_ra_provider_info_accesscode : undefined,
		custrecord_ra_provider_info_serialnumber : undefined,
		custrecord_ra_provider_info_isemv : undefined,
		custrecord_ra_provider_info_terminalid : undefined,
		custrecord_ra_provider_info_comport : undefined,
		custrecord_ra_provider_info_newutg : undefined,
		custrecord_ra_provider_info_clientguid : undefined,
		custrecord_ra_provider_info_authtoken : undefined};

//Partner provisioning section
var NotProvisioned = 1;
var ToBeProvisioned = 2;
var InProvisioning = 3;
var Provisioned = 4;

var Critical = 1;
var Error = 2;
var Warning = 3;
var Info = 4;

//Error codes
var inputDataErrorCode = "INPUT_DATA_ERROR";
var workstationNotFoundErrorCode = "WORKSTATION_NOT_FOUND";
var invalidDateFormatErrorCode = "INVALID_DATE_FORMAT";

