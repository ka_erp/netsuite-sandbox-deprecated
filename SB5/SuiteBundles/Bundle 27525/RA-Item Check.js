/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       17 May 2013     bsomerville      Item Functions
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function getRESTlet(dataIn) {
	
	nlapiLogExecution("DEBUG", "Item ID", dataIn.ItemID);
	
	var filters = new Array(); 
	var columns = new Array(); 
	filters[0] = new nlobjSearchFilter("internalid",null,"is",dataIn.ItemID);
	//filters[1] = new nlobjSearchFilter("type",null,"is","inventoryitem");
		 
	columns[0] = new nlobjSearchColumn("internalid",null,null); 
	columns[1] = new nlobjSearchColumn("isinactive",null,null);
	columns[2] = new nlobjSearchColumn("itemid",null,null);
	columns[3] = new nlobjSearchColumn("type",null,null);
	
	var results = nlapiSearchRecord("item",null,filters,columns); 
	 
	if(results) {
	
		var foundID = results[0].getValue(columns[0]);
		var inactive = results[0].getValue(columns[1]);
		var name = results[0].getValue(columns[2]);
		
		var resultObject = new Object();
		resultObject.itemID = dataIn.ItemID;
		resultObject.found = (foundID == dataIn.ItemID);
		resultObject.inactive = inactive;
		resultObject.name = name;	
		resultObject.type = results[0].getValue(columns[3]);
	
		return resultObject;
	} else {
		var resultObject = new Object();
		resultObject.itemID = dataIn.ItemID;
		resultObject.found = false;
		resultObject.inactive = "Y";
		resultObject.name = "NO ITEM";	
			
		return resultObject;
	}
}

function activateItem ( dataIn ) {
	var itemID = dataIn.ItemID;
	
	var result = "";
	
	if(!itemID) {		
		result = "No Item Specified.";
	} else {
		result = setItemActive ( itemID, true );
	}
	
	nlapiLogExecution("DEBUG", "Activate Item", result);
	
	var resultObject = new Object();
	resultObject.itemID = itemID;
	resultObject.result = result;
	
	return resultObject;
}

function deactivateItem ( dataIn ) {
	var itemID = dataIn.ItemID;
	
	var result = "";
	
	if(!itemID) {		
		result = "No Item Specified.";
	} else {
		result = setItemActive ( itemID, false );
	}
	
	nlapiLogExecution("DEBUG", "Deactivate Item", result);
	
	var resultObject = new Object();
	resultObject.itemID = itemID;
	resultObject.result = result;
	
	return resultObject;
}

function setItemActive ( itemID, active) {
	var result = "";	
	var item = nlapiLoadRecord("inventoryitem", itemID, null);	
	
	if(!item) item = nlapiLoadRecord("assemblyitem", itemID, null);
	
	if(item) {
		
		var currentActive = item.getFieldValue("isinactive") != (active ? "F" : "T");
		
		nlapiLogExecution("DEBUG", "Set Active - Current",  item.getFieldValue("isinactive"));
		
		if(currentActive) {		
			item.setFieldValue("isinactive", active ? "F" : "T");
			nlapiSubmitRecord(item, false, true);
			result = "Set to " + (active ? "active" : "inactive") + ".";
						
		} else {
			result = "No change made.";
		}
	} else {
		result = "No item found.";
	}
	
	nlapiLogExecution("DEBUG", "Set Active", result);
	return result;
}

function getOpenItem() {
	var filters = new Array(); 
	var columns = new Array(); 

	filters[0] =  new nlobjSearchFilter("ra_barcode",null,"is","noninventoryitem");
		 
	columns[0] = new nlobjSearchColumn("internalid",null,null); 
	columns[1] = new nlobjSearchColumn("isinactive",null,null);
	columns[2] = new nlobjSearchColumn("itemid",null,null);
	
	var results = nlapiSearchRecord("noninventoryitem",null,filters,columns); 
}
