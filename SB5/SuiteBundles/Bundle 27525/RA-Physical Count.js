function getPhysicalCountDocuments(data) {
    var location = data.location;
    var status = data.status;

    nlapiLogExecution("debug", "getPhysicalCountDocuments", location);

    var filters = new Array();

    filters[0] = new nlobjSearchFilter("custrecord_ra_physcount_loc", null, "is", location, null);

    // Need to add status filter here
    var col = new Array();
    col[0] = new nlobjSearchColumn("name");
    col[1] = new nlobjSearchColumn("internalId");
    var results = nlapiSearchRecord("customlist_ra_physcount_status_list", null, null, col);
    for (var i = 0; results != null && i < results.length; i++) {
        var res = results[i];
        var listValue = (res.getValue("name"));
        var listID = (res.getValue("internalId"));
        if (listValue == status) {
            filters[1] = new nlobjSearchFilter("custrecord_ra_physcount_status", null, "is", listID, null);
        }
    }

    var response = new Object();
    response.Documents = new Array();

    col[0] = new nlobjSearchColumn("internalId");
    col[1] = new nlobjSearchColumn("custrecord_ra_physcount_status");
    col[2] = new nlobjSearchColumn("custrecord_ra_physcount_datereqd");
    col[3] = new nlobjSearchColumn("name");

    var searchResults = nlapiSearchRecord("customrecord_ra_physicalcount", null, filters, col);
    for (var i = 0; searchResults != null && i < searchResults.length; i++) {
        var document = new Object();

        document.DocumentID = searchResults[i].getValue("internalId");
        document.Name = searchResults[i].getValue("name");
        document.Status = searchResults[i].getText("custrecord_ra_physcount_status");
        document.Required = searchResults[i].getValue("custrecord_ra_physcount_datereqd");

        response.Documents.push(document);
    }

    return response;
}

function getPhysicalCountDocument(data) {
    var internalid = data.internalId;
    var document = new Object();

    nlapiLogExecution("debug", "getPhysicalCountDocument", internalid);

    var filters = new Array();
    filters[0] = new nlobjSearchFilter("internalid", null, "anyOf", internalid, null);

    var col = new Array();

    col[0] = new nlobjSearchColumn("internalId");
    col[1] = new nlobjSearchColumn("custrecord_ra_physcount_status");
    col[2] = new nlobjSearchColumn("custrecord_ra_physcount_datereqd");
    col[3] = new nlobjSearchColumn("name");
    col[4] = new nlobjSearchColumn("custrecord_ra_physcount_method");
    col[5] = new nlobjSearchColumn("custrecord_ra_physcount_cycle");

    var searchResults = nlapiSearchRecord("customrecord_ra_physicalcount", null, filters, col);
    for (var i = 0; searchResults != null && i < searchResults.length; i++) {
        var method = searchResults[i].getText("custrecord_ra_physcount_method");

        document.DocumentID = searchResults[i].getValue("internalId");
        document.Name = searchResults[i].getValue("name");
        document.Status = searchResults[i].getText("custrecord_ra_physcount_status");
        document.Requred = searchResults[i].getValue("custrecord_ra_physcount_datereqd");
        document.Method = method;

        filters = new Array();
        if (method == "Cycle") {
            // Get all items in the cycle
            filters[0] = new nlobjSearchFilter("custitem_ra_item_physcountcycle", null, "anyOf", searchResults[i].getValue("custrecord_ra_physcount_cycle"));

            nlapiLogExecution("debug", "Physical Count Method", searchResults[i].getValue("custrecord_ra_physcount_cycle"));

        } else if (method == "List") {
            var itemIds = [];

            var itemCols = new Array();

            itemCols[0] = new nlobjSearchColumn("custrecord_ra_physcountitem_item");

            // Get the list of items for this document
            filters[0] = new nlobjSearchFilter("custrecord_ra_physcountitem_countdoc", null, "anyOf", internalid, null);
            var itemSearchResults = nlapiSearchRecord("customrecord_ra_physicalcount_item", null, filters, itemCols);

            for (var j = 0; itemSearchResults != null && j < itemSearchResults.length; j++) {
                itemIds[j] = itemSearchResults[j].getValue("custrecord_ra_physcountitem_item");
            }

            filters[0] = new nlobjSearchFilter("internalid", null, "anyOf", itemIds, null);
        } else if (method == "Open") {
            // Nothing to do
        }

        if (filters.length > 0) {
            document.Items = new Array();

            var itemCols = new Array();

            itemCols[0] = new nlobjSearchColumn("internalId");
            itemCols[1] = new nlobjSearchColumn("displayName");
            itemCols[2] = new nlobjSearchColumn("upcCode");
            itemCols[3] = new nlobjSearchColumn("name");

            var itemSearchResults = nlapiSearchRecord("item", null, filters, itemCols);
            for (var j = 0; itemSearchResults != null && j < itemSearchResults.length; j++) {
                var item = new Object();
                item.InternalID = itemSearchResults[j].getValue("internalId");
                item.Name = itemSearchResults[j].getValue("name");
                item.DisplayName = itemSearchResults[j].getValue("displayName");
                item.UPC = itemSearchResults[j].getValue("upcCode");
                document.Items.push(item);
            }
        }
    }
    return document;
}

function postPhysicalCountDocument(data) {
    // 2013/03/13 gmo: Fixed bug when count matches on hand

    var internalid = data.DocumentID;
    var items = data.Items;
    var itemIds = [];

    for (var i = 0; items != null && i < items.length; i++) {
        itemIds[i] = items[i].InternalID.toString();
    }

    nlapiLogExecution("debug", "getPhysicalCountDocument", "Document: " + internalid + ", Items: " + itemIds.length);
    nlapiLogExecution("debug", "getPhysicalCountDocument", "First Item: " + itemIds[0]);

    // Get the location for this document
    var filters = new Array();

    filters[0] = new nlobjSearchFilter("internalid", null, "anyOf", internalid, null);

    var col = new Array();

    col[0] = new nlobjSearchColumn("custrecord_ra_physcount_loc");

    var searchResults = nlapiSearchRecord("customrecord_ra_physicalcount", null, filters, col);
    if (searchResults == null && searchResults.length == 0) {
        return "ERROR: Physical count document was not found.";
    }

    var document = nlapiLoadRecord("customrecord_ra_physicalcount", internalid);

    var location = searchResults[0].getValue("custrecord_ra_physcount_loc");

    // Get the inventory for all the items at the location
    filters[0] = new nlobjSearchFilter("internalid", null, "anyOf", itemIds, null);
    filters[1] = new nlobjSearchFilter("internalid", "inventorylocation", "anyOf", location, null);

    col[0] = new nlobjSearchColumn("locationquantityonhand");
    col[1] = new nlobjSearchColumn("internalid");

    searchResults = nlapiSearchRecord("item", null, filters, col);

    nlapiLogExecution("debug", "items found", searchResults != null ? searchResults.length : 0);

    for (var i = 0; searchResults != null && i < searchResults.length; i++) {
        for (var j = 0; items != null && j < items.length; j++) {
            if (items[j].InternalID == searchResults[i].getValue("internalid")) {
                items[j].CurrentOH = searchResults[i].getValue("locationquantityonhand");
                items[j].Adjustment = items[j].Count - (items[j].CurrentOH != null ? items[j].CurrentOH : 0);
            }
        }
    }

    // Clear existing counts
    var filters = new Array();
    var col = new Array();

    filters[0] = new nlobjSearchFilter("custrecord_ra_physicalcountline_doc", null, "anyOf", internalid);
    col[0] = new nlobjSearchColumn("internalid");

    searchResults = nlapiSearchRecord("customrecord_ra_physicalcount_line", null, filters, col);

    for (var i = 0; searchResults != null && i < searchResults.length; i++) {
        nlapiDeleteRecord("customrecord_ra_physicalcount_line", searchResults[i].getValue("internalid"));
    }

    // Record new counts
    for (var i = 0; items != null && i < items.length; i++) {
        var countLine = nlapiCreateRecord("customrecord_ra_physicalcount_line");
        countLine.setFieldValue("custrecord_ra_physicalcountline_doc", internalid);
        countLine.setFieldValue("custrecord_ra_physicalcountline_item", items[i].InternalID);
        countLine.setFieldValue("custrecord_ra_physicalcountline_qty", items[i].Count);
        countLine.setFieldValue("custrecord_ra_physicalcountline_onhand", items[i].CurrentOH ? items[i].CurrentOH : 0.00);
        countLine.setFieldValue("custrecord_ra_physicalcountline_adj", items[i].Adjustment != null ? items[i].Adjustment : items[i].Count);
        countLine.setFieldValue("custrecord_ra_physicalcountline_loc", location);

        nlapiLogExecution("debug", "Adjustment", "Item: " + items[i].InternalID + ", Adjustment: " + (items[i].Adjustment ? items[i].Adjustment : items[i].Count));
        nlapiLogExecution("debug", "Creating line", "Doc: " + internalid + ", Item: " + items[i].InternalID + ", Count: " + items[i].Count);

        nlapiSubmitRecord(countLine, true);
    }

    // Update document status to "Reported"
    document.setFieldText("custrecord_ra_physcount_status", "Reported");
    nlapiSubmitRecord(document, true);

    return items;
}

function commitPhysicalCountDocument() {
    var internalId = nlapiGetRecordId();
    //var document = nlapiLoadRecord("customrecord_ra_physicalcount", internalId);
    var adjustmentDoc = nlapiCreateRecord("inventoryadjustment");

    //alert(document.getFieldValue("lastmodifieddate"));

    adjustmentDoc.setFieldValue("account", nlapiGetFieldValue("custrecord_ra_physcount_adjacct"));
    adjustmentDoc.setFieldValue("adjlocation", nlapiGetFieldValue("custrecord_ra_physcount_loc"));
    adjustmentDoc.setFieldValue("memo", "Committed through Physical Count");
    adjustmentDoc.setFieldValue("trandate", nlapiDateToString(new Date()));
    //adjustmentDoc.setFieldValue("department", 159);

    var filters = new Array();
    var col = new Array();

    filters[0] = new nlobjSearchFilter("custrecord_ra_physicalcountline_doc", null, "anyOf", internalId);
    col[0] = new nlobjSearchColumn("custrecord_ra_physicalcountline_item");
    col[1] = new nlobjSearchColumn("custrecord_ra_physicalcountline_adj");

    var searchResults = nlapiSearchRecord("customrecord_ra_physicalcount_line", null, filters, col);

    for (var i = 0; searchResults != null && i < searchResults.length; i++) {
        adjustmentDoc.selectNewLineItem("inventory");
        adjustmentDoc.setCurrentLineItemValue("inventory", "adjustqtyby", searchResults[i].getValue("custrecord_ra_physicalcountline_adj"));
        adjustmentDoc.setCurrentLineItemValue("inventory", "memo", "From Physical Count Commit");
        adjustmentDoc.setCurrentLineItemValue("inventory", "item", searchResults[i].getValue("custrecord_ra_physicalcountline_item"));
        adjustmentDoc.setCurrentLineItemValue("inventory", "location", nlapiGetFieldValue("custrecord_ra_physcount_loc"));

        adjustmentDoc.commitLineItem("inventory");
    }

    var adjId = nlapiSubmitRecord(adjustmentDoc, true);

    nlapiSetFieldText("custrecord_ra_physcount_status", "Committed");
    nlapiSetFieldValue("custrecord_ra_physcount_adjdoc", adjId);
    nlapiSetFieldValue("custrecord_ra_physcount_postdate", nlapiDateToString(new Date()));
}