/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 X 2014     mkaluza
 *
 */

function pageInit() {
	clientFieldChanged("edit", "custitem_ra_required_comment");
}

function clientFieldChanged(type, name, linenum){
	if (name == "custitem_ra_required_comment")
	{
		var msgField = nlapiGetField("custitem_ra_comment_message");
		var fieldValue = nlapiGetFieldValue("custitem_ra_required_comment");
		if (fieldValue == 'F')
		{
			msgField.setDisplayType('disabled');
			nlapiSetFieldValue("custitem_ra_comment_message", "");
		}
		else
		{
			msgField.setDisplayType('normal');
		}
	}
}
