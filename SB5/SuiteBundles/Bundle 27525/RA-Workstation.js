/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       19 Sep 2013     dotocka
 * 1.01       25 Mar 2015     dotocka
 * 1.1        17 Mar 2016     dotocka
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord customrecord_ra_workstation
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	var defaultLaneNumber = 0;
	if (type == 'view'){
		nlapiSetFieldValue('custrecord_ra_ws_lanenumber', defaultLaneNumber);
		var value = nlapiGetFieldText("custrecord_ra_cc_provider");
		var id = nlapiGetFieldValue("custrecord_ra_cc_provider");
		nlapiLogExecution('DEBUG', 'Before load provider', 'name:' + value + " id:" + id);
		if (!value || value.length == 0){
			showPaymentProviderFields(false);
		} else {
			switch (id) {
			case MPS:
				var array = Shift4Fields;
				for ( var int = 0; int < array.length; int++) {
					hideField(array[int]);
				}
				array = MPSFields;
				for ( var int = 0; int < array.length; int++) {
					showField(array[int], true);
				}
				break;
			case Shift4:
				var array = MPSFields;
				for ( var int = 0; int < array.length; int++) {
					hideField(array[int]);
				}
				array = Shift4Fields;
				for ( var int = 0; int < array.length; int++) {
					showField(array[int], true);
				}
				break;
			default:
				showPaymentProviderFields(false);
				break;
			}
		}
	}
	else {
		if(type == 'create'){
			//remove the "Copy provider info to..." button
			form.removeButton('custformbutton1');
			nlapiSetFieldValue('custrecord_ra_ws_lanenumber', defaultLaneNumber);
		}
		showPaymentProviderFields(true);		
	}
}

function userEventBeforeSubmit(type){
	
	var provisioning_chbx = nlapiGetFieldValue("custrecord_ra_provisioning_set_chbx");
	if(provisioning_chbx == 'T'){
		var url = GetNSPOSIntegrationUrl('/PartnerStaging/NetSuite.Retail.PartnerStaging.ClickOnce.application',true);
		
		var workstationId = nlapiGetRecordId();
		var locationId = nlapiGetFieldValue('custrecord_ra_ws_loc');
	    url += '&Id=' + workstationId + '&location=' + locationId;
		
	    nlapiSetFieldValue('custrecord_ra_workstation_installurl', url);
	    nlapiSetFieldValue('custrecord_ra_provisioning_set_chbx', 'F');
	    nlapiSetFieldValue('custrecord_ra_ws_provisioning_status', ToBeProvisioned);
	}
	
	var newLane  = confirmLane();
	var currLane =  parseInt(nlapiGetFieldValue("custrecord_ra_ws_lanenumber"));
	
	if(newLane != currLane) {
		if(confirm("Lane Number " + currLane + " is already in use. Would you like to use " + newLane + " instead?")) {
			nlapiSetFieldValue('custrecord_ra_ws_lanenumber', newLane);
			return true;
		} else {
			return false;
		}		
	}	
	ClearPaymentProviderFields();
}

function userEventAfterSubmit(type){
	if (type == 'create') {
		var workstationId = nlapiGetRecordId();
		var workstation = nlapiLoadRecord('customrecord_ra_workstation', workstationId);
		var url = workstation.getFieldValue('custrecord_ra_workstation_installurl');
		if(url){
			var url = GetNSPOSIntegrationUrl('/PartnerStaging/NetSuite.Retail.PartnerStaging.ClickOnce.application',true);
						
			var locationId = nlapiGetFieldValue('custrecord_ra_ws_loc');
		    url += '&Id=' + workstationId + '&location=' + locationId;
			
		    workstation.setFieldValue('custrecord_ra_workstation_installurl', url);
		    nlapiSubmitRecord(workstation);
		}
    }
}

/**
 * Shows or hides the field
 * @param {String} fieldName Name of the field to show/hide
 * @param {Boolean} show True if the field should be shown, false if it should be hidden
 */
function showField(fieldName, show){
	var field = nlapiGetField(fieldName);
	if(show){
		field.setDisplayType("normal");
	} else {
		field.setDisplayType("hidden");
	}
}

function confirmLane() {
	nlapiLogExecution('DEBUG', 'Before submit lane', 'lane:' + nlapiGetFieldValue("custrecord_ra_ws_lanenumber"));
	var MAX_LANE_ID = 999;
	
	// Add user approval to non-default
	// Confirm field editable after create (uniqueness scope)
	
	var locId = nlapiGetFieldValue("custrecord_ra_ws_loc"); // Location
	var wsNum = nlapiGetRecordId();		// Internal ID
	
	// Current next available lane
	var nextId = 0;
	
	// Reassign if 0 or lower (default) or if conflict occurs
	var reassign = parseInt(nlapiGetFieldValue("custrecord_ra_ws_lanenumber")) <= 0;
	var lanes = new Array();
	
	nlapiLogExecution('DEBUG', 'Before submit lane', 'ws ' + wsNum + ' ln: ' + parseInt(nlapiGetFieldValue("custrecord_ra_ws_lanenumber")));
		
	// If location is set
	if(locId) {
		
		var filters = new Array(); 
		var columns = new Array(); 
			
		// Filter by location
		filters[0] = new nlobjSearchFilter('custrecord_ra_ws_loc',null,"is", locId);
		columns[0] = new nlobjSearchColumn("internalid",null,null);
		columns[1] = new nlobjSearchColumn("custrecord_ra_ws_lanenumber",null,null);		
		
		var results = nlapiSearchRecord('customrecord_ra_workstation',null,filters,columns);
		
		if(results){
			for(var resultNum = 0; resultNum < results.length; resultNum++) {
			
				nlapiLogExecution('DEBUG', 'Before submit lane', 'check ' + results[resultNum].getValue(columns[0]));
			
				// Ignore current record
				if(parseInt(results[resultNum].getValue(columns[0])) == wsNum) continue;
						
				// Store all used lanes
				lanes.push(parseInt(results[resultNum].getValue(columns[1])));					
			}					
		}
	}
	
	if(reassign || lanes.indexOf( parseInt(nlapiGetFieldValue("custrecord_ra_ws_lanenumber")) ) != -1) {
	
		// Find first available lane
		for(var xx = 1; xx < MAX_LANE_ID; xx++) {
			if(lanes.indexOf(xx) == -1){
				nextId = xx;
				reassign = true;
				break;
			}
		}
	}
	
	nlapiLogExecution('DEBUG', 'Before submit lane', 'next:' + nextId );
	
	
	return reassign ? nextId : parseInt(nlapiGetFieldValue("custrecord_ra_ws_lanenumber"));
	// Set if needed
	//if(reassign)
	//	nlapiSetFieldValue('custrecord_ra_ws_lanenumber', nextId);
}

/**
 * Hides the field
 * @param {String} fieldName Name of the field to show/hide
 */
function hideField(fieldName){
	showField(fieldName, false);
}

/**
 * Show or hides all fields regarding payment provider
 * @param {Boolean} show True if the field should be shown, false if it should be hidden
 */
function showPaymentProviderFields(show){
	var array = CommonProviderFields.concat(MPSFields.concat(Shift4Fields));
	for ( var int = 0; int < array.length; int++) {
		showField(array[int], show);
	}
}

/**
 * Clear unused fields for Payment providers based on selected provider (fields that are not for this provided will be cleared out even when user inserted something into them)
 */
function ClearPaymentProviderFields(){
	var id = nlapiGetFieldValue("custrecord_ra_cc_provider");
	
	switch (id) {
		case MPS:
			var array = Shift4Fields;
			for ( var int = 0; int < array.length; int++) {
				if(MPSFields.indexOf(array[int]) == -1){
					nlapiSetFieldValue(array[int], '');
				}
			}
			break;
		case Shift4:
			var array = MPSFields;
			for ( var int = 0; int < array.length; int++) {
				if(Shift4Fields.indexOf(array[int]) == -1){
					nlapiSetFieldValue(array[int], '');
				}
			}
			break;
		default:
			var array = CommonProviderFields.concat(MPSFields.concat(Shift4Fields));
			for ( var int = 0; int < array.length; int++) {
				nlapiSetFieldValue(array[int], '');
			}
			break;
	}
}