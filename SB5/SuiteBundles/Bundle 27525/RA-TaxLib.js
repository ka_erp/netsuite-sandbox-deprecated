/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 X 2015     mkaluza
 *
 */

var fieldScheduleLinkId = "custrecord_link_taxschedule_id";
var fieldMapping = "custrecord_ra_taxgroups_mapping";
var recordTaxSchedule = "customrecord_ra_tax_schedule";
var fieldgroupZeroGroup = "custpage_zero_tax_group";
var fieldgroupTaxSchedules = "custpage_tax_schedules_group";
var fieldZeroTaxSelect = "custpage_zero_tax_select";
var fieldDescription = "custrecord_ra_taxschedule_description";
var fieldReceiptText = "custrecord_ra_taxschedule_recepit_text";
var fieldLocationChangedCount = "custpage_location_changed_count";
var maxSavedLocations = 100;

function SerializeMapping(mapping) 
{
	var xml = '<Location>';
	if (mapping == null) {
		return null;
	}
	if (mapping.zeroTaxGroup != null)
	{
		xml += '<ZeroTaxGroup>' + mapping.zeroTaxGroup + '</ZeroTaxGroup>';
	}
	if (mapping.taxSchedule != null && mapping.taxSchedule.length > 0)
	{
		for (var i = 0; i < mapping.taxSchedule.length; i++) {
			xml += '<TaxSchedule internalId="' + mapping.taxSchedule[i].internalId + '">' + mapping.taxSchedule[i].taxGroup + '</TaxSchedule>';
		}
	}
	xml += '</Location>';
	return xml;
}

function DeserializeMapping(mapping) 
{
	var unescapedHtml = unescapeHtml(mapping);
	if (unescapedHtml != null && unescapedHtml != "") {
		var xml = nlapiStringToXML(unescapedHtml);
		if (xml != null) {
			var x = {};
			x.zeroTaxGroup = nlapiSelectValue(xml, '/Location/ZeroTaxGroup');
			var schedules = nlapiSelectNodes(xml, '/Location/TaxSchedule');
			
			if (schedules != null) {
				x.taxSchedule = [];
				for (var i = 0; schedules != null && i < schedules.length; i++) {
					x.taxSchedule[i] = {};
					x.taxSchedule[i].internalId = nlapiSelectValue(schedules[i], './@internalId');
					x.taxSchedule[i].taxGroup = nlapiSelectValue(schedules[i], '.');
				}
			}
			
			return x;
		} else {
			return null;
		}
	}
	return null;
}

function unescapeHtml(text) 
{
	if (text != null && text != '')
	{
	    return text.replace(/&amp;/g, '&')
	        .replace(/&lt;/g, '<')
	        .replace(/&gt;/g, '>')
	        .replace(/&quot;/g, '"')
	        .replace(/&#039;/g, "'");
	}
	return null;
}

function GetTaxScheduleIndex(mapping, taxScheduleId) 
{
	if (mapping != null && mapping.taxSchedule != null && mapping.taxSchedule.length > 0)
	{
		for (var i = 0; i < mapping.taxSchedule.length; i++) 
		{
			if (mapping.taxSchedule[i].internalId == taxScheduleId)
			{
				return i;
			}
		}
	}
	return null;
}

function GetTaxIdFromMapping(mapping, taxScheduleId)
{
	if ( mapping != null && mapping.taxSchedule != null &&  mapping.taxSchedule.length > 0 && taxScheduleId != null && taxScheduleId != '') {
		for (var i = 0; i < mapping.taxSchedule.length; i++) {
			if (mapping.taxSchedule[i].internalId == taxScheduleId) {
				var value = parseInt(mapping.taxSchedule[i].taxGroup); 
				return !isNaN(value) ? value : null;
			}
		}
	}
	return null;
}

Array.prototype.diff = function(a) {
    return this.filter(function(i) {return !(a.indexOf(i) > -1);});
};

function createTaxSchedule(name, description)
{
	var idsOld = GetIds('taxSchedule');
	
	// Create new native TaxSchedule if does not exist
	var record = nlapiCreateRecord('taxSchedule');
	record.setFieldValue('name', nlapiGetFieldValue('name'));
	record.setFieldValue('description', nlapiGetFieldValue(fieldDescription));
	
	try {
		nlapiSubmitRecord(record);
	} catch (e) {
		if (e != null && e.code == "DUP_RCRD") {
			return -1;
		}
	}
	
	// Get new ids of native TaxSchedules
	var idsNew = GetIds('taxSchedule');
	
	// Get the difference
	var idsDiff = idsNew.diff(idsOld);
	
	if (idsDiff != null && idsDiff.length == 1) {
		nlapiSetFieldValue(fieldScheduleLinkId, idsDiff[0]);
		return idsDiff[0];
	}
	
	return null;
}

function GetIds(recordType)
{
	var ids = [];
	var column = new nlobjSearchColumn('internalid',null,null);
	var searchResults = nlapiSearchRecord(recordType, null, null, column);
	if (searchResults && searchResults.length > 0) {
		for (var i=0; i < searchResults.length; i++) {
			ids.push(searchResults[i].getValue('internalid'));
		}
	} 
	return ids;
}

function IsEmpty(value) {
	return value == null || value == "" || value == 0; 
}

function GetTaxValue(taxId, value) {
	var columns = new Array();
	var filters = new Array();
	
	if (value == null) {
		value = 'rate';
	}
	
	columns[0] = new nlobjSearchColumn(value,null,null);
	filters[0] = new nlobjSearchFilter('internalid', null,'is', taxId);
	var searchResults = nlapiSearchRecord('taxGroup', null, filters, columns);
	if (searchResults && searchResults.length > 0) {
		return searchResults[0].getValue(value);
	}
	var searchResults = nlapiSearchRecord('salestaxitem', null, filters, columns);
	if (searchResults && searchResults.length > 0) {
		return searchResults[0].getValue(value);
	}
	return 0;
}

function round(s, decPlaces) {
	var zero = 0.0;
	var value = parseFloat(s); 
	return !isNaN(value) ? value.toFixed(decPlaces) : zero.toFixed(decPlaces);	
}

function FormatCanadianTaxRate(taxIdValue) {
	if (taxIdValue != null && taxIdValue != '' && taxIdValue != 0) {
		var taxGroup = GetRecordById(['taxGroup'], ['unitprice1', 'unitprice2'], taxIdValue);
		if (taxGroup != null) {
			return 'GST/HST: ' + round(taxGroup.unitprice1, 2) + '%, PST: ' + round(taxGroup.unitprice2, 2) + '%';
		}
		var taxCode = GetRecordById(['salestaxitem'], ['rate'], taxIdValue);
		if (taxCode != null) {
			return round(taxCode.rate, 2) + '%';
		}
	}
	return '0';
}

