function postItemReceipt(datain) {
    // datain should be JSON from RAPOS
    // { "type":"purchaseorder",
    //   "internalId":"#",
    //   "location":"#",
    //   "item":
    //   [
    //    { "internalId":"#", "type":"inventoryItem", "qty":"#.##", "location":"#" },
    //    { "internalId":"#", "type":"inventoryItem", "qty":"#.##", "location":"#" }
    //   ]
    // }

    // datain.type must be either transferorder or purchaseorder
    if (datain.type != "transferorder" && datain.type != "purchaseorder") {
        var err = new Object();
        err.code = "401";
        err.message = "Invalid type specified in request.  Only transferorder and purchaseorder allowed.";
        return err;
    }

    var defaultV = new Array();

    // I tried setting defaults but it kept complaining
    // Default not received
    //defaultV.quantity = 0;
    // Default location Id
    //defaultV.location = datain.location;

    var orderInternalId = datain.orderInternalId;
    if (orderInternalId == null) {
        // Check if we have an external ID and get the internal ID through a search
        var filters = new Array();
        filters[0] = new nlobjSearchFilter("externalid", null, "is", datain.orderExternalId, null);

        var col = new Array();
        col[0] = new nlobjSearchColumn("internalId");

        var results = nlapiSearchRecord(datain.type, null, filters, col);

        if (results != null && results.length > 0) {
            orderInternalId = results[0].getValue("internalId");
        }
    }

    var itemReceipt = nlapiTransformRecord(datain.type, orderInternalId, "itemreceipt");
    var itemCount = itemReceipt.getLineItemCount("item");

    itemReceipt.setFieldValue("externalid", datain.externalId);

    // Set all quantities to 0 unless we find them below
    for (var j = 1; j <= itemCount; j++) {
        itemReceipt.setLineItemValue("item", "quantity", j, 0);
    }

    var found = false;

    // For each item submitted in request go through and find it in the itemReceipt
    for (var i = 0; i < datain.item.length; i++) {
        found = false;

        var qty = datain.item[i].qty;

        for (var j = 1; j <= itemCount; j++) {
            var itemID = itemReceipt.getLineItemValue("item", "item", j);
            var itemLocation = itemReceipt.getLineItemValue("item", "location", j);
            var itemQty = itemReceipt.getLineItemValue("item", "quantityremainingdisplay", j);

            // Must fully receive transfer lines
            if (datain.type == "transferorder") qty = itemQty;

            //nlapiLogExecution("DEBUG", "itemID", "itemID = " + itemID);
            //nlapiLogExecution("DEBUG", "itemLocation", "itemLocation = " + itemLocation);

            if (itemLocation == null || itemLocation == "" || itemLocation == "null") break;
            itemLocation = itemLocation.toString();

            // We only want to receive an item that matches ID and location
            if (datain.item[i].internalId == itemID
                    && datain.item[i].location == itemLocation) {
                itemReceipt.setLineItemValue("item", "quantity", j, qty);
                found = true;
                break;
            }
        }

        if (found) continue;

        nlapiLogExecution("DEBUG", "item wasn't found", "id = " + datain.item[i].internalId);

        // See if there are any rows without location that match
        for (var j = 1; j <= itemCount; j++) {
            var itemID = itemReceipt.getLineItemValue("item", "item", j);
            var itemLocation = itemReceipt.getLineItemValue("item", "location", j);
            var itemQty = itemReceipt.getLineItemValue("item", "quantityremainingdisplay", j);

            // Must fully receive transfer lines

            if (datain.type == "transferorder") qty = itemQty;

            nlapiLogExecution("DEBUG", "itemID", "itemID = " + itemID);
            nlapiLogExecution("DEBUG", "itemLocation", "itemLocation = " + itemLocation);

            // We only want to receive an item that matches name and location
            if (datain.item[i].internalId == itemID && itemLocation == null) {
                itemReceipt.setLineItemValue("item", "quantity", j, qty);

                // Set the location since location is not already defined for the line
                itemReceipt.setLineItemValue("item", "location", j, datain.item[i].location);
                break;
            }
        }
    }

    // Let's submit the receipt
    var id = nlapiSubmitRecord(itemReceipt);

    return itemReceipt;
}