/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 Jan 2015     dotocka
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord Employee
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	if ((type == 'create'|| type == 'edit'))
	{
		
		nlapiLogExecution('DEBUG', 'Before Submit Employee', 'Entering method');
		var fieldValue = nlapiGetFieldValue("custentity_ra_integration_chbx");
		
		nlapiLogExecution('DEBUG', 'Before Submit Employee', 'Current RA Integration chbx value: ' + fieldValue);
		
		if (fieldValue == 'T')
		{
			var giveAccess = nlapiGetFieldValue("giveaccess");
			if(giveAccess == 'F')
			{
				throw nlapiCreateError('INPUT_DATA_ERROR', '"GIVE ACCESS" checkbox must be enabled if the employee is used for RA integration!', true);
			}
			var previousValue = 'F';
			if(type == 'edit')
			{
				var employee = nlapiLoadRecord('employee', nlapiGetRecordId());//load previous version of the record
				previousValue = employee.getFieldValue('custentity_ra_integration_chbx');
				nlapiLogExecution('DEBUG', 'Before Submit Employee', 'Employee being edited and previous RA Integration chbx value: ' + previousValue);
			}
			
			//service call with password from custpage_ra_integration_password
			var pass = nlapiGetFieldValue('custpage_ra_integration_password');
			
			if((!pass || pass == "" ))
			{
				if(previousValue == 'T')
				{
					nlapiLogExecution('DEBUG', 'Before Submit Employee', 'No integration password specified.');
					return;
				}
				else
				{
					throw nlapiCreateError('INPUT_DATA_ERROR', 'There was no valid password entered for the employee.', true);
				}
			}
			 // Create the request headers
	        var headers = {
	        		"User-Agent-x": "SuiteScript-Call", "Content-Type": "application/json"
	        };
	        
			try{
				var checkUrl = GetNSPOSIntegrationUrl('/RTNSService/Management/CheckServices', false);
				result = nlapiRequestURL(checkUrl, null, headers);
				
				if (result && result.code == 200 && result.body == 'true')
		        {
			        nlapiLogExecution('DEBUG', 'Before Submit Employee', 'Result Check Services: OK!');
				}
				else{
					nlapiLogExecution('DEBUG', 'Before Submit Employee', 'Result Check Services: ERROR!');
					throw nlapiCreateError('CHECK_SERVICES_ERROR', 'Error while trying to contact password change services for integration credentials setup. Contact support please!', true);
				}
			}
			catch(e)
			{
				throw nlapiCreateError('CHECK_SERVICES_ERROR', 'Error while trying to contact password change services for integration credentials setup. Contact support please!', true);
			}
			
			try
			{
				// Token
				var url = GetNSPOSIntegrationUrl('/RTNSService/Management/SetIntegrationCredentials', false);
				var token = GetNSPOSIntegrationToken();
				nlapiLogExecution('DEBUG', 'Before Submit Employee','Acquired Url and token.');
								
		        var data = {
		        		token: token,
		        		passport: GetNSPassport(pass)
		        };
		        
		        result = nlapiRequestURL(url, JSON.stringify(data), headers);
		        nlapiLogExecution('DEBUG', 'Before Submit Employee' , 'Service response: ' + result);
				
		        if (result && result.code == 200 && result.body == 'true')
		        {
			        nlapiLogExecution('DEBUG', 'Before Submit Employee', 'Result Change Password: OK!');
					return;
				}
		        nlapiLogExecution('DEBUG', 'Before Submit Employee', 'Result Change Password: ERROR!');
				throw nlapiCreateError('UNEXPECTED_ERROR', 'Failed to change integration service credentials! Please try again, if problem persists contact support.', true);
			}
			catch(e)
			{
				if(e.code == 'SSS_REQUEST_TIME_EXCEEDED'){
					nlapiLogExecution('DEBUG', 'Before Submit Employee', 'Result Change Password: Timeout exceeded, proceeding!');
					return;
				}
				throw nlapiCreateError('UNEXPECTED_ERROR', e.name + ' ' + e.message, true);
			}
		}
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord Employee
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request)
{
	if ((type == 'create'|| type == 'edit'))
	{
		nlapiLogExecution('DEBUG', 'Before Load Employee', 'Adding NSPOs Integration field.');
		form.addField('custpage_ra_integration_password', 'text', 'NSPOS Integration password').setDisplayType('hidden');
	}	
}