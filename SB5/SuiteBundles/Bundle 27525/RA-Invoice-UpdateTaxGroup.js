/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       11 III 2015     mkaluza
 *
 */

function clientValidateLine(type)
{
	var itemId = nlapiGetCurrentLineItemValue('item', 'item');
	var locationId = nlapiGetFieldValue('location');
	var taxGroupOld = nlapiGetCurrentLineItemValue('item', 'taxcode');
	
	if (locationId && !taxGroupOld)
	{
		var location = nlapiLoadRecord('location', locationId);
		var taxScheduleId = GetItemTaxSchedule(itemId);

		if (location)
		{	
			var mapping = location.getFieldValue('custrecord_ra_taxgroups_mapping');
			var xml = nlapiStringToXML(unescapeHtml(mapping));		
			
			var taxGroup = nlapiSelectValue(xml, "/Location/TaxSchedule[@internalId='" + taxScheduleId + "']");
			
			nlapiSetCurrentLineItemValue('item', 'taxcode', taxGroup, false, true);
		}
	}
	
    return true;
}


function GetItemTaxSchedule(itemId)
{
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('internalidnumber', null, 'equalTo', itemId);
	var column = new nlobjSearchColumn('taxschedule',null,null);
	var searchResults = nlapiSearchRecord('item', null, filters, column);
	if (searchResults && searchResults.length == 1) {
		return searchResults[0].getValue('taxschedule');
	}
	return null;
}

function unescapeHtml(text) 
{
	if (text != null)
	{
	    return text.replace(/&amp;/g, '&')
		    .replace(/&lt;/g, '<')
		    .replace(/&gt;/g, '>')
		    .replace(/&quot;/g, '"')
		    .replace(/&#039;/g, "'");
	}
	return null;
}