function postRefundSalesOrderDeposit(datain) {
    // datain should be JSON from RAPOS
    // { 
	//     type: "salesorder",
    //     orderInternalId: "#",
    //     location: "#",
	//	   paymentMethod: "#",
	//	   account: "#",
	//	   department: "#",
	//	   subsidiary: "#",
	//	   tillId: "#",
	//     customerExternalId: "#",
	//     cancelItem: [{ internalId: "#", 
	//              	  type: "inventoryItem",
	//              	  qty: "#.##",
	//              	  location: "#" },
    //            		{ internalId: "#", 
	//              	  type: "inventoryItem",
	//              	  qty: "#.##", 
	//              	  location: "#" }
	//			 	   ]
    // }
	
	if (datain == null)	{
		return error("There is not input data.");
	}
	
    if (datain.type != "salesorder") {
    	return error("Invalid type specified in request.  Only salesorder allowed.");
    }

    if (datain.cancelItem == null || datain.cancelItem.length == 0){
    	return error("Invalid type specified in request.  Any item is defined.");    	
    }
    
    var context = nlapiGetContext();
    var isMultipleLocationFeatureEnabled = context.getFeature("MULTILOCINVT");
    
    var orderInternalId = datain.orderInternalId;
    if (orderInternalId == null) {
    	if (datain.orderExternalId == null){
    		return error("Internal or External ID of order have to be defined.");
    	}
        // Check if we have an external ID and get the internal ID through a search
        var filter = new nlobjSearchFilter("externalid", null, "is", datain.orderExternalId, null);
        var col = new nlobjSearchColumn("internalId");
        var results = nlapiSearchRecord(datain.type, null, filter, col);

        if (results != null && results.length > 0) {
            orderInternalId = results[0].getValue("internalId");
        }
    }
     
    var cancelItems = {};
    for (var i = 0; i < datain.cancelItem.length; i++){
    	var internalId = datain.cancelItem[i].internalId;
    	
    	if (internalId == null){
    		return error("Item has to have Internal ID.");    		
    	}

    	cancelItems[internalId] = datain.cancelItem[i];		
    	cancelItems[internalId].location = datain.cancelItem[i].location || datain.location;
		
		if (cancelItems[internalId].location == null && isMultipleLocationFeatureEnabled) {
			return error("Item has to have location.");
		}		
    }
    
    var salesOrder = nlapiLoadRecord(datain.type, orderInternalId, null);
    var originItems = salesOrder.getLineItemCount('item');
    for (var j = 1; j <= originItems; j++) {
    	var itemID = salesOrder.getLineItemValue("item", "item", j);
    	var quantity = salesOrder.getLineItemValue("item", "quantity", j);
    	var item = cancelItems[itemID];
    	
    	if (item != null){
    		if (quantity - item.qty < 0){
    			return error("Item quantity cannot be decreased.");
    		}
    		else{
    			salesOrder.setLineItemValue("item", "quantity", j, quantity - item.qty);
    		}
    	}
    }
    
    var id = nlapiSubmitRecord(salesOrder);
    
    if (id == null){
    	return error("Cannot change the sales order.");
    }
    
    // REFUND creation
    var customerInternalId = datain.customerInternalId;
    if (customerInternalId == null) {
    	if (datain.customerExternalId == null){
    		return error("Internal or External ID of customer have to be defined.");
    	}
        // Check if we have an external ID and get the internal ID through a search
        var filter = new nlobjSearchFilter("externalid", null, "is", datain.customerExternalId, null);
        var col = new nlobjSearchColumn("internalId");
        var results = nlapiSearchRecord('customer', null, filter, col);

        if (results != null && results.length > 0) {
        	customerInternalId = results[0].getValue("internalId");
        }
    }
    
    var depositIds = {};
    
    var filter = new nlobjSearchFilter(datain.type, null, "is", orderInternalId, null);
    var col = new nlobjSearchColumn("internalId");
    var results = nlapiSearchRecord('customerdeposit', null, filter, col);
    
    if (results != null && results.length > 0) {
    	for (var k = 0; k < results.length; k++) {
            var internalId = results[k].getValue("internalId");
            depositIds[internalId] = results[k];
    	}
    }
    
    var customerRefund = nlapiCreateRecord('customerrefund',{recordmode:'dynamic'});
    customerRefund.setFieldValue('customer', customerInternalId);
    customerRefund.setFieldValue('paymentmethod', datain.paymentMethod);
    customerRefund.setFieldValue('account', datain.account);
    customerRefund.setFieldValue('location', datain.location);
    customerRefund.setFieldValue('subsidiary', datain.subsidiary);
    customerRefund.setFieldValue('custbody_ra_tillid', datain.tillId);
    
    var refundAmount = 0.00;
    var deposits = customerRefund.getLineItemCount('deposit');
    for (var l = 1; l <= deposits; l++) {
    	var depositInternalId = customerRefund.getLineItemValue('deposit','doc', l);
        var deposit = depositIds[depositInternalId];
    	if (deposit != null) {
    	    customerRefund.setLineItemValue('deposit', 'apply', l, 'T');
            var amount = customerRefund.getLineItemValue('deposit','remaining', l);
    	    customerRefund.setLineItemValue('deposit', 'amount', l, parseFloat(amount));
            refundAmount += parseFloat(amount);
    	}
    	else {
    	    customerRefund.setLineItemValue('deposit', 'apply', l, 'F');
    	}
    }
    
    if (refundAmount > 0.00) {
    	customerRefund.setFieldValue('total', refundAmount);
    	customerRefund.setFieldValue('deposit_total', refundAmount);
    	customerRefund.setFieldValue('haslines', 'T');
    	var id = nlapiSubmitRecord(customerRefund);
    
    	if (id == null){
    		return error("Cannot change the sales order.");
    	}
    }
        
    return salesOrder;
}

function error(message, code) {
	code = code || "401";
	return { code: code, message: message };
}