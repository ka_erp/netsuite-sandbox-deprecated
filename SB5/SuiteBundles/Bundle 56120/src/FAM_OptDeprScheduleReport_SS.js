/**
 * � 2016 NetSuite Inc.
 * User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
**/

var FAM;
if (!FAM) { FAM = {}; }

/**
 * Starter for Background Processing function for Depreciation Schedule Report
 *
 * Parameters:
 *     BGP {FAM.BGProcess} - Process Instance Record for this background process
 * Returns:
 *     true {boolean} - processing should be requeued
 *     false {boolean} - processing should not be requeued; essentially setting the deployment to
 *                       standby
**/
function famOptDeprSchedReport(BGP) {
    var assetSchedReport = new FAM.ScheduleReport_SS(BGP);

    try {
        return assetSchedReport.run();
    }
    catch (e) {
        assetSchedReport.logObj.printLog();
        throw e;
    }
}

/**
 * Class for generating schedule report; Inherits FAM.DepreciationCommon
 *
 * Constructor Parameters:
 *     procInsRec {FAM.BGProcess} - Process Instance Record for this background process
**/
FAM.ScheduleReport_SS = function (procInsRec) {
    // Call Parent Constructor
    FAM.DepreciationCommon.apply(this, arguments);

    // member variables
    this.grpMasterInfo = null;
    this.screenName = 'depreciationschedule2';
    this.cacheFileDescription = 'Depreciation Schedule Report Cache File';
    this.reportFileName = 'FAM_DeprSchedule_BGP' + this.procInsRec.getRecordId();
    this.csvReportFileName = this.reportFileName + 'CSV';
    this.reportFileDescription = FAM.resourceManager.GetString('DSR_desc_DSR', 'DSR_template');
    
    this.instructionLimit = +FAM.SystemSetup.getSetting('deprSchedInsLimit');
    
    // user inputs
    this.arrAssetTypes = [];
    this.arrSubsidiaries = [];
    this.arrAcctBooks = [];
    this.arrClasses = [];
    this.arrDepartments = [];
    this.arrLocations = [];
    this.leaseOption = 'all_assets';
    this.startDate = null;
    this.endDate = null;
    
    // JSON for Depreciation Values
    this.deprInfo = {};
};

// Prototypal Inheritance
FAM.ScheduleReport_SS.prototype = Object.create(FAM.DepreciationCommon.prototype);

FAM.ScheduleReport_SS.prototype.queueId = 0;
FAM.ScheduleReport_SS.prototype.recsProcessed = 0;
FAM.ScheduleReport_SS.prototype.recsFailed = 0;
FAM.ScheduleReport_SS.prototype.jsonFileId = 0;
FAM.ScheduleReport_SS.prototype.stage = 1;
FAM.ScheduleReport_SS.prototype.instructions = 0;
FAM.ScheduleReport_SS.prototype.lastPeriod = 0;
FAM.ScheduleReport_SS.prototype.csv = 0;
FAM.ScheduleReport_SS.prototype.xml = 0;

// user inputs
FAM.ScheduleReport_SS.prototype.isNBV = false;
FAM.ScheduleReport_SS.prototype.deprMethod = 0;
FAM.ScheduleReport_SS.prototype.saveResults = false;

// script parameters
FAM.ScheduleReport_SS.prototype.lowerLimit = 0;
FAM.ScheduleReport_SS.prototype.upperLimit = 0;
FAM.ScheduleReport_SS.prototype.hasSpawned = false;

/**
 * Main function for this class
 *
 * Parameters:
 *     none
 * Returns:
 *     true {boolean} - processing should be requeued
 *     false {boolean} - processing should not be requeued; essentially setting the deployment to
 *                       standby
**/
FAM.ScheduleReport_SS.prototype.run = function () {
    this.logObj.startMethod('FAM.ScheduleReport_SS.run');

    var queueDetails, procComplete = false, queueStringId = null, limitExceeded = false,
        timer = new FAM.Timer();
    
    this.getParameters();
    
    if (this.stage === 1) {
        if (this.saveResults) {
            limitExceeded = this.deleteOldForecasts();
        }
        else {
            this.stage++;
            this.updateBGPAndQueues();
        }
    }
    if (!limitExceeded && (this.stage === 2 || this.stage === 3)) {
        this.logObj.pushMsg('retrieving queue details...');
        queueStringId = FAM.Context.getDeploymentId();
        queueDetails = this.procInsRec.getQueueDetails();
        this.queueId       = queueDetails.queueId;
        
        if (queueDetails.stateValues && queueDetails.stateValues.JSONFileId) {
            this.jsonFileId = queueDetails.stateValues.JSONFileId;
        }
    }
    
    if (this.jsonFileId) {
        timer.start();
        this.deprInfo = FAM_Util.loadJSONFileContents(queueStringId);
        this.logObj.logExecution('Elapsed time for loading JSON file: ' +
            timer.getReadableElapsedTime());
    }
    
    if (!limitExceeded && this.stage === 2) {
        limitExceeded = this.loadActualDepreciations();
    }
    if (!limitExceeded && this.stage === 3) {
        limitExceeded = this.forecastDepreciations();
    }
    if (!limitExceeded && this.stage === 4) {
        queueStringId = null;
        limitExceeded = this.mergeTempFiles();
        if (!limitExceeded) { this.stage++; }
    }
    if (!limitExceeded && this.stage === 5) {
        this.generateReport();
        this.stage++;
    }
    if (!limitExceeded && this.stage === 6) {
        limitExceeded = this.generateCSV();
        if (!limitExceeded) {
            procComplete = true;
            limitExceeded = true;
        }
    }
    
    if (this.stage> 1) {
        this.updateBGPAndQueues(queueStringId, procComplete);
    }
    
    this.logObj.endMethod();
    return limitExceeded;
};

/**
 * Triggers MR Script to effeciently delete old forecast data
 *
 * Parameters:
 *     none
 * Returns:
 *     void
**/
FAM.ScheduleReport_SS.prototype.deleteOldForecasts = function () {
    this.logObj.startMethod('FAM.ScheduleReport_SS.deleteOldForecasts');
    
    var funcName = 'customscript_fam_mr_deleteforecast',
        processId = this.procInsRec.recordId,
        bgpParamId = 'custscript_fam_forecast_bgpid',
        mrTriggerURL = nlapiResolveURL('suitelet', 'customscript_fam_triggermr_su',
            'customdeploy_fam_triggermr_su', true);
    
    var otherScript = nlapiRequestURL(mrTriggerURL, {
        custscript_fam_mapreducescriptid : funcName,
        custscript_fam_bgpid : processId,
        custscript_fam_paramid : bgpParamId
    });
        
    if (otherScript.getBody() == 'T') {
        this.logObj.logExecution('Delete MR Script Triggered | funcname: ' + funcName +
            ' | bgpId : ' + processId + ' | bgpParamId: ' + bgpParamId);
    }
    else {
        this.procInsRec.setFieldValue('status', FAM.BGProcessStatus.Failed);
        this.procInsRec.setFieldValue('message', otherScript.getBody().substring(0, 300));
        this.procInsRec.submitRecord();
    }
    
    this.logObj.endMethod();
    return false;
};

/**
 * Retrieves parameters passed for this operation
 *
 * Parameters:
 *     none
 * Returns:
 *     void
**/
FAM.ScheduleReport_SS.prototype.getParameters = function () {
    this.logObj.startMethod('FAM.ScheduleReport_SS.getParameters');

    this.recsProcessed = +this.procInsRec.getFieldValue('rec_count') || 0;
    this.recsFailed = +this.procInsRec.getFieldValue('rec_failed') || 0;

    this.stage = +this.procInsRec.stateValues.Stage || 1;
    this.lastPeriod = +this.procInsRec.stateValues.LastPeriod || 0;
    this.jsonFileId = this.procInsRec.stateValues.JSONFileId || 0;
    this.csv = this.procInsRec.stateValues.csv || 0;
    this.xml = this.procInsRec.stateValues.xml || 0;
    
    // user inputs
    this.arrAssetTypes = this.procInsRec.stateValues.type || [];
    this.arrSubsidiaries = this.procInsRec.stateValues.subs || [];
    this.arrAcctBooks = this.procInsRec.stateValues.book || [];
    this.arrClasses = +this.procInsRec.stateValues['class'] || [];
    this.arrDepartments = +this.procInsRec.stateValues.dept || [];
    this.arrLocations = +this.procInsRec.stateValues.loc || [];
    this.isNBV = this.procInsRec.stateValues.nbv === 'T';
    this.deprMethod = +this.procInsRec.stateValues.depr || 0;
    this.saveResults = this.procInsRec.stateValues.save === 'T';
    this.leaseOption = this.procInsRec.stateValues.sel || 'all_assets';
    this.startDate = new Date(+this.procInsRec.stateValues.start);
    this.endDate = new Date(+this.procInsRec.stateValues.end);
    this.startDate = new Date(this.startDate.getFullYear(), this.startDate.getMonth(), 1);
    this.endDate = new Date(this.endDate.getFullYear(), this.endDate.getMonth() + 1, 0);
    
    // script parameters
    this.hasSpawned = this.procInsRec.getScriptParam('hasSpawned') === 'T';
    this.lowerLimit = +this.procInsRec.getScriptParam('lowerLimit') || 0;
    this.upperLimit = +this.procInsRec.getScriptParam('upperLimit') || 0;

    this.logObj.endMethod();
};

/**
 * Acquires actual data values to be used in reports
 *
 * Parameters:
 *     none
 * Returns:
 *     {boolean} - flag that determines if the execution and time limit has been reached
**/
FAM.ScheduleReport_SS.prototype.loadActualDepreciations = function () {
    this.logObj.startMethod('FAM.ScheduleReport_SS.loadActualDepreciations');

    var i, histSearch, toJSON, date, currencyData, limitExceeded = false, timer = new FAM.Timer();
    
    while (!limitExceeded) {
        this.logObj.logExecution('low: ' + this.lowerLimit + ' | high: ' + this.upperLimit);
        timer.start();
        histSearch = this.searchActualDepreciations(this.lowerLimit, this.upperLimit);
        this.logObj.logExecution('Elapsed time for Actual Values Search: ' +
            timer.getReadableElapsedTime());
        
        if (histSearch.results) {
            if (this.upperLimit === 0) {
                this.upperLimit = +histSearch.getId(histSearch.results.length - 1);
                this.upperLimit += this.instructionLimit - histSearch.results.length;
            }
            if (!this.hasSpawned) {
                this.hasSpawned = this.procInsRec.scheduleNextQueue(this.upperLimit);
            }
            
            this.logObj.logExecution('start: ' + histSearch.getId(0) + ' | end: ' +
                histSearch.getId(histSearch.results.length - 1));
            
            timer.start();
            for (i = 0; i < histSearch.results.length; i++) {
                limitExceeded = this.hasExceededLimit();
                if (limitExceeded) { break; }
                
                toJSON = {};
                
                date = nlapiStringToDate(histSearch.getValue(i, 'date'));
                toJSON.year = date.getFullYear();
                toJSON.month = date.getMonth() + 1;
                
                currencyData = this.currCache.subCurrData(histSearch.getValue(i, 'subsidiary'),
                    histSearch.getValue(i, 'bookId'));
                toJSON.currency = currencyData.currSym || '';
                
                toJSON.asset = histSearch.getText(i, 'asset') || 0;
                toJSON.assetName = histSearch.getValue(i, 'altname', 'asset');
                toJSON.type = histSearch.getText(i, 'asset_type') || 0;
                
                toJSON.sub = FAM.Context.blnOneWorld ? histSearch.getText(i, 'subsidiary') : 0;
                toJSON.amount = this.isNBV ? +histSearch.getValue(i, 'net_book_value') :
                    +histSearch.getValue(i, 'transaction_amount');
                
                if (!FAM.Context.blnMultiBook) { toJSON.book = 0; }
                else { toJSON.book = histSearch.getText(i, 'bookId') || 'No Accounting Book'; }
                
                if (this.deprMethod) {
                    toJSON.met = histSearch.getText(i, 'depr_method', 'alternate_depreciation') || 0;
                    toJSON.life = histSearch.getValue(i, 'asset_life', 'alternate_depreciation') || 0;
                }
                else {
                    toJSON.met = histSearch.getText(i, 'depr_method', 'asset') || 0;
                    toJSON.life = histSearch.getValue(i, 'lifetime', 'asset') || 0;
                }
                
                this.saveToJSON(toJSON);
                
                this.lowerLimit = +histSearch.getId(i);
                this.instructions++;
            }
            this.logObj.logExecution('Elapsed time saving ' + i + ' Actual Values to JSON: ' +
                timer.getReadableElapsedTime());
        }
        else if (this.upperLimit === 0) {
            if (this.waitForQueues()) {
                limitExceeded = true;
            }
            else {
                this.stage++;
                this.initScriptParams();
                this.updateBGPAndQueues();
            }
            break;
        }
        else if (!this.hasSpawned) {
            this.lowerLimit = this.upperLimit;
            this.upperLimit = 0;
        }
        else {
            break;
        }
    }
    
    this.saveJSONData(FAM.Context.getDeploymentId());
    if (limitExceeded) { this.setScriptParams(); }
    
    this.logObj.endMethod();
    return limitExceeded;
};

/**
 * Saves JSON data to file
 *
 * Parameters:
 *     queue {string} - queue id for the currently running multi-queued process
 * Returns:
 *     void
 */
FAM.ScheduleReport_SS.prototype.saveJSONData = function (queue) {
    this.logObj.startMethod('FAM.ScheduleReport_SS.saveJSONData');
    
    var timer = new FAM.Timer();
    
    if (JSON.stringify(this.deprInfo) !== '{}') {
        timer.start();
        this.jsonFileId = FAM_Util.saveJSONReportContents(JSON.stringify(this.deprInfo),
            this.cacheFileDescription, queue);
        this.logObj.logExecution('Elapsed time for saving JSON file: ' +
            timer.getReadableElapsedTime());
    }
    
    this.logObj.endMethod();
};
 
/**
 * Searches Depreciation History Records for actual data to be used in reports
 *
 * Parameters:
 *     lowLimit {number} - internal id which the search should start
 *     upLimit {number} - internal id which the search should end
 * Returns:
 *     object {FAM.Search} which contains the results
**/
FAM.ScheduleReport_SS.prototype.searchActualDepreciations = function (lowLimit, upLimit) {
    this.logObj.startMethod('FAM.ScheduleReport_SS.searchActualDepreciations');

    var fSearch = new FAM.Search(new FAM.DepreciationHistory_Record(),
        'customsearch_fam_history_search');

    fSearch.setJoinRecord('asset', new FAM.Asset_Record());

    fSearch.addFilter('schedule', null, 'is', 'F');
    fSearch.addFilter('isinactive', null, 'is', 'F');
    fSearch.addFilter('transaction_type', null, 'is', FAM.TransactionType.Depreciation);
    fSearch.addFilter('date', null, 'within', this.startDate, this.endDate);
    fSearch.addFilter('include_report', 'asset', 'is', 'T');
    fSearch.addFilter('isinactive', 'asset', 'is', 'F');
    fSearch.addFilter('depr_active', 'asset', 'is', FAM.DeprActive.True);

    if (upLimit > 0) {
        fSearch.addFilter('internalidnumber', null, 'between', lowLimit + 1, upLimit);
    }
    else {
        fSearch.addFilter('internalidnumber', null, 'greaterthan', lowLimit);
    }
    if (this.arrAssetTypes.length > 0) {
        fSearch.addFilter('asset_type', null, 'anyof', this.arrAssetTypes);
    }
    if (FAM.Context.blnOneWorld) {
        fSearch.addColumn('subsidiary');
        if (this.arrSubsidiaries.length > 0) {
            fSearch.addFilter('subsidiary', null, 'anyof', this.arrSubsidiaries);
        }
    }
    if (FAM.Context.blnMultiBook) {
        fSearch.addColumn('bookId');
        if (this.arrAcctBooks.length > 0) {
            fSearch.addFilter('bookId', null, 'anyof', this.arrAcctBooks);
        }
    }

    if (FAM.Context.blnLocation && this.arrLocations.length > 0) {
        fSearch.addFilter('location', 'asset', 'anyof', this.arrLocations);
    }
    if (FAM.Context.blnDepartment && this.arrDepartments.length > 0) {
        fSearch.addFilter('department', 'asset', 'anyof', this.arrDepartments);
    }
    if (FAM.Context.blnClass && this.arrClasses.length > 0) {
        fSearch.addFilter('classfld', 'asset', 'anyof', this.arrClasses);
    }
    if (this.leaseOption === 'except_leased') {
        fSearch.addFilter('isleased', 'asset', 'is', 'F');
    }
    else if (this.leaseOption === 'leased_only') {
        fSearch.addFilter('isleased', 'asset', 'is', 'T');
    }
    if (this.deprMethod) {
        fSearch.setJoinRecord('alternate_depreciation', new FAM.AltDeprMethod_Record());

        fSearch.addFilter('alternate_method', null, 'is', this.deprMethod);
        fSearch.addFilter('isinactive', 'alternate_depreciation', 'is', 'F');
        fSearch.addFilter('status', 'alternate_depreciation', 'anyof', [FAM.TaxMethodStatus.New,
            FAM.TaxMethodStatus.Depreciating, FAM.TaxMethodStatus['Part Disposed']]);
        
        fSearch.addColumn('asset_life', 'alternate_depreciation');
        fSearch.addColumn('depr_method', 'alternate_depreciation');
    }
    else {
        fSearch.addFilter('alternate_method', null, 'is', '@NONE@');
        fSearch.addFilter('status', 'asset', 'anyof', [FAM.AssetStatus.New,
            FAM.AssetStatus.Depreciating, FAM.AssetStatus['Part Disposed']]);
        
        fSearch.addColumn('lifetime', 'asset');
        fSearch.addColumn('depr_method', 'asset');
    }
    
    fSearch.addColumn('internalid', null, null, 'SORT_ASC');
    fSearch.addColumn('date');
    fSearch.addColumn('asset_type');
    fSearch.addColumn('asset');
    fSearch.addColumn('altname', 'asset');
    fSearch.addColumn('alternate_depreciation');
    fSearch.addColumn('net_book_value');
    fSearch.addColumn('transaction_amount');
    
    fSearch.run();
    this.logObj.endMethod();
    return fSearch;
};

/**
 * [OVERRIDE] Determines if the Execution Limit or Time Limit has exceeded
 *
 * Returns:
 *     {boolean} - determines whether Execution, Time, or Instruction Limit has been exceeded
**/
FAM.ScheduleReport_SS.prototype.hasExceededLimit = function () {
    this.logObj.startMethod('FAM.ScheduleReport_SS.hasExceededLimit');

    var ret = FAM.DepreciationCommon.prototype.hasExceededLimit.apply(this, arguments) ||
        this.instructions > this.instructionLimit;

    this.logObj.endMethod();
    return ret;
};

/**
 * Saves data acquired to JSON for requeueing purposes
 *
 * Parameters:
 *     d {Object} - data to save
 * Returns:
 *     void
**/
FAM.ScheduleReport_SS.prototype.saveToJSON = function (d) {
    this.logObj.startMethod('FAM.ScheduleReport_SS.saveToJSON');
    
    if (this.deprInfo[d.sub] === undefined) {
        this.deprInfo[d.sub] = {};
    }
    if (this.deprInfo[d.sub][d.book] === undefined) {
        this.deprInfo[d.sub][d.book] = { currency : d.currency };
    }
    if (this.deprInfo[d.sub][d.book][d.year] === undefined) {
        this.deprInfo[d.sub][d.book][d.year] = {};
    }
    if (this.deprInfo[d.sub][d.book][d.year][d.type] === undefined) {
        this.deprInfo[d.sub][d.book][d.year][d.type] = {};
    }
    if (this.deprInfo[d.sub][d.book][d.year][d.type][d.asset] === undefined) {
        this.deprInfo[d.sub][d.book][d.year][d.type][d.asset] = {
            name : d.assetName };
    }
    if (this.deprInfo[d.sub][d.book][d.year][d.type][d.asset][d.met] === undefined) {
        this.deprInfo[d.sub][d.book][d.year][d.type][d.asset][d.met] = {
            life : d.life };
    }
    if (this.deprInfo[d.sub][d.book][d.year][d.type][d.asset][d.met][d.month] === undefined) {
        this.deprInfo[d.sub][d.book][d.year][d.type][d.asset][d.met][d.month] = d.amount;
    }
    else {
        if (this.isNBV) {
            this.deprInfo[d.sub][d.book][d.year][d.type][d.asset][d.met][d.month] = d.amount;
        }
        else {
            this.deprInfo[d.sub][d.book][d.year][d.type][d.asset][d.met][d.month] += d.amount;
        }
    }
    
    this.logObj.endMethod();
};

/**
 * Waits for other queues to finish
 *
 * Returns:
 *     {boolean} - determines whether Execution, Time, or Instruction Limit has been exceeded
**/
FAM.ScheduleReport_SS.prototype.waitForQueues = function () {
    this.logObj.startMethod('FAM.ScheduleReport_SS.waitForQueues');

    var limitExceeded = false;

    while (this.procInsRec.hasOnGoingQueue()) {
        if (this.hasExceededLimit()) {
            limitExceeded = true;
            this.procInsRec.setScriptParams({ lowerLimit : this.lowerLimit });
            break;
        }
    }
        
    this.logObj.endMethod();
    return limitExceeded;
};

/**
 * Initializes Script Parameter Values
 *
 * Parameters:
 *     none
 * Returns:
 *     void
**/
FAM.ScheduleReport_SS.prototype.initScriptParams = function () {
    this.logObj.startMethod('FAM.ScheduleReport_SS.initScriptParams');
    
    this.hasSpawned = false;
    this.lowerLimit = 0;
    this.upperLimit = 0;
    
    this.logObj.endMethod();
};

/**
 * Sets Script Parameters before requeueing script
 *
 * Parameters:
 *     none
 * Returns:
 *     void
**/
FAM.ScheduleReport_SS.prototype.setScriptParams = function () {
    this.logObj.startMethod('FAM.ScheduleReport_SS.setScriptParams');
    
    this.logObj.logExecution('Execution Limit | Remaining Usage: ' +
        FAM.Context.getRemainingUsage() + ' | Time Elapsed: ' +
        this.perfTimer.getReadableElapsedTime() + ' | Instructions: ' + this.instructions);
    this.procInsRec.setScriptParams({
        lowerLimit : this.lowerLimit,
        upperLimit : this.upperLimit,
        hasSpawned : this.hasSpawned ? 'T' : 'F'
    });
    
    this.logObj.endMethod();
};
    
/**
 * Retrieves and forecasts depreciation amounts for records based on user selection
 *
 * Parameters:
 *     none
 * Returns:
 *     {boolean} - flag that determines if the processing should be requeued
**/
FAM.ScheduleReport_SS.prototype.forecastDepreciations = function () {
    this.logObj.startMethod('FAM.ScheduleReport_SS.forecastDepreciations');

    var srchRecDepr, limitExceeded = false, timer = new FAM.Timer(),
        recordName = this.deprMethod ? 'tax method' : 'asset';

    this.loadDeprMethodsFunctions();

    if (this.deprMethod) {
        this.logObj.pushMsg('Get group master info');
        timer.start();
        this.grpMasterInfo = FAM.GroupMaster.getInfo('alternate_method', this.arrSubsidiaries,
            true);
        this.logObj.logExecution('Elapsed time loading Group Info: ' +
            timer.getReadableElapsedTime());
    }
    
    while (!limitExceeded) {
        timer.start();
        srchRecDepr = this.searchRecordsToDepreciate(this.lowerLimit, this.upperLimit);
        this.logObj.logExecution('Elapsed time for ' + recordName + ' search: ' +
            timer.getReadableElapsedTime());
        
        if (srchRecDepr.results) {
            this.upperLimit = +srchRecDepr.getId(srchRecDepr.results.length - 1);
            if (!this.hasSpawned) {
                this.hasSpawned = this.procInsRec.scheduleNextQueue(this.upperLimit);
            }
            limitExceeded = this.depreciateRecords(srchRecDepr);
        }
        else if (this.upperLimit === 0) {
            if (this.waitForQueues()) {
                limitExceeded = true;
            }
            else {
                this.stage++;
                this.initScriptParams();
            }
            break;
        }
        else if (!this.hasSpawned) {
            this.lowerLimit = this.upperLimit;
            this.upperLimit = 0;
        }
        else {
            break;
        }
    }
    
    this.saveJSONData(FAM.Context.getDeploymentId());
    if (limitExceeded) { this.setScriptParams(); }
    
    this.logObj.endMethod();
    return limitExceeded;
};

/**
 * Retrieves or counts all tax methods that should be depreciated based on the criteria
 *
 * Parameters:
 *     lowLimit {number} - internal id which the search should start
 *     upLimit {number} - internal id which the search should end
 * Returns:
 *     object {FAM.Search} which contains the results
**/
FAM.ScheduleReport_SS.prototype.searchRecordsToDepreciate = function (lowLimit, upLimit) {
    this.logObj.startMethod('FAM.ScheduleReport_SS.searchRecordsToDepreciate');

    var fSearch, joinId = null;

    if (this.deprMethod) {
        fSearch = new FAM.Search(new FAM.AltDeprMethod_Record());
        joinId = 'parent_asset';
        fSearch.setJoinRecord(joinId, new FAM.Asset_Record());

        fSearch.addFilter('alternate_method', null, 'is', this.deprMethod);
        fSearch.addFilter('isinactive', null, 'is', 'F');
        fSearch.addFilter('status', null, 'anyof', [FAM.TaxMethodStatus.New,
            FAM.TaxMethodStatus.Depreciating, FAM.TaxMethodStatus['Part Disposed']]);
        
        if (this.arrAcctBooks.length > 0) {
            fSearch.addFilter('booking_id', null, 'anyof', this.arrAcctBooks);
        }

        fSearch.addColumn('altname', 'parent_asset');
        fSearch.addColumn('alternate_method');
        fSearch.addColumn('original_cost');
        fSearch.addColumn('residual_value');
        fSearch.addColumn('asset_life');
        fSearch.addColumn('financial_year_start');
        fSearch.addColumn('last_depr_amount');
        fSearch.addColumn('prior_year_nbv');
        fSearch.addColumn('parent_asset');
        fSearch.addColumn('cumulative_depr');
        fSearch.addColumn('convention');
        fSearch.addColumn('period_convention');
        fSearch.addColumn('is_group_master');
        fSearch.addColumn('is_group_depr');
        fSearch.addColumn('booking_id');
    }
    else {
        fSearch = new FAM.Search(new FAM.Asset_Record());

        fSearch.addFilter('status', null, 'anyof', [FAM.AssetStatus.New,
            FAM.AssetStatus.Depreciating, FAM.AssetStatus['Part Disposed']]);

        fSearch.addColumn('name');
        fSearch.addColumn('altname');
        fSearch.addColumn('initial_cost');
        fSearch.addColumn('rv');
        fSearch.addColumn('lifetime');
        fSearch.addColumn('fyscal_year_start');
        fSearch.addColumn('lastDeprAmount');
        fSearch.addColumn('prior_nbv');
        fSearch.addColumn('cummulative_depr');
    }

    if (upLimit > 0) {
        fSearch.addFilter('internalidnumber', null, 'between', lowLimit + 1, upLimit);
    }
    else {
        fSearch.addFilter('internalidnumber', null, 'greaterthan', lowLimit);
    }

    fSearch.addFilter('include_report', joinId, 'is', 'T');
    fSearch.addFilter('isinactive', joinId, 'is', 'F');
    fSearch.addFilter('depr_active', joinId, 'is', FAM.DeprActive.True);
    fSearch.addFilter('depr_start_date', null, 'onorbefore', this.endDate);
    fSearch.addFilter('last_depr_date', null, 'before', this.endDate);

    if (this.leaseOption === 'except_leased') {
        fSearch.addFilter('isleased', joinId, 'is', 'F');
    }
    else if (this.leaseOption === 'leased_only') {
        fSearch.addFilter('isleased', joinId, 'is', 'T');
    }
    if (this.arrAssetTypes.length > 0) {
        fSearch.addFilter('asset_type', joinId, 'anyof', this.arrAssetTypes);
    }
    if (this.arrSubsidiaries.length > 0) {
        fSearch.addFilter('subsidiary', null, 'anyof', this.arrSubsidiaries);
    }
    
    if (FAM.Context.blnLocation && this.arrLocations.length > 0) {
        fSearch.addFilter('location', joinId, 'anyof', this.arrLocations);
    }
    if (FAM.Context.blnDepartment && this.arrDepartments.length > 0) {
        fSearch.addFilter('department', joinId, 'anyof', this.arrDepartments);
    }
    if (FAM.Context.blnClass && this.arrClasses.length > 0) {
        fSearch.addFilter('classfld', joinId, 'anyof', this.arrClasses);
    }

    fSearch.addColumn('internalid', null, null, 'SORT_ASC');
    fSearch.addColumn('current_cost');
    fSearch.addColumn('depr_start_date');
    fSearch.addColumn('annual_entry');
    fSearch.addColumn('last_depr_period');
    fSearch.addColumn('book_value');
    fSearch.addColumn('subsidiary');
    fSearch.addColumn('last_depr_date');
    fSearch.addColumn('depr_method');
    fSearch.addColumn('status');
    fSearch.addColumn('depr_period');

    fSearch.addColumn('asset_type', joinId);
    fSearch.addColumn('quantity', joinId);
    fSearch.addColumn('depr_rules', joinId);
    
    fSearch.run();
    this.logObj.endMethod();
    return fSearch;
};

/**
 * Forecasts all depreciation values for the specified records
 *
 * Parameters:
 *     srchRecDepr {FAM.Search} - records to depreciate
 * Returns:
 *     {boolean} - flag that determines if the processing should be requeued
**/
FAM.ScheduleReport_SS.prototype.depreciateRecords = function (srchRecDepr) {
    this.logObj.startMethod('FAM.ScheduleReport_SS.depreciateRecords');
    
    var i, recObj, updatedValues, limitExceeded = false,
        recordName = this.deprMethod ? 'tax method' : 'asset',
        timer = new FAM.Timer();
    
    for (i = 0; i < srchRecDepr.results.length; i++) {
        if (srchRecDepr.getValue(i, 'is_group_master') !== 'T' &&
            srchRecDepr.getValue(i, 'is_group_depr') === 'T') {

            this.logObj.logExecution('Skip depreciation for tax method: ' + srchRecDepr.getId(i) +
                ', Part of Group Depreciation');
            continue;
        }
        
        limitExceeded = this.hasExceededLimit();
        if (limitExceeded) { break; }

        // Re-initialze logObj for each asset to prevent flooding of log data
        this.logObj = new printLogObj('debug');
        this.logObj.startMethod('FAM.ScheduleReport_SS.forecastDepreciations');
        this.logObj.logExecution('depreciate ' + recordName + ': ' + srchRecDepr.getId(i));
        
        recObj = {
            currCost : srchRecDepr.getValue(i, 'current_cost'),
            deprStart : srchRecDepr.getValue(i, 'depr_start_date'),
            annualMet : srchRecDepr.getValue(i, 'annual_entry'),
            lastPeriod : srchRecDepr.getValue(i, 'last_depr_period'),
            currNBV : srchRecDepr.getValue(i, 'book_value'),
            subsidiary : srchRecDepr.getValue(i, 'subsidiary'),
            subsidiaryName : srchRecDepr.getText(i, 'subsidiary'),
            isAcquired : 'T',
            lastDate : srchRecDepr.getValue(i, 'last_depr_date'),
            deprMethod : srchRecDepr.getValue(i, 'depr_method'),
            methodName : srchRecDepr.getText(i, 'depr_method'),
            status : srchRecDepr.getValue(i, 'status'),
            deprPeriod : srchRecDepr.getValue(i, 'depr_period')
        };

        if (this.deprMethod) {
            recObj.assetId = srchRecDepr.getValue(i, 'parent_asset');
            recObj.assetTextId = srchRecDepr.getText(i, 'parent_asset');
            recObj.assetName = srchRecDepr.getValue(i, 'altname', 'parent_asset');
            recObj.taxMetId = srchRecDepr.getId(i);
            recObj.altMethod = srchRecDepr.getValue(i, 'alternate_method');
            recObj.origCost = srchRecDepr.getValue(i, 'original_cost');
            recObj.resValue = srchRecDepr.getValue(i, 'residual_value');
            recObj.lifetime = srchRecDepr.getValue(i, 'asset_life');
            recObj.fiscalYr = srchRecDepr.getValue(i, 'financial_year_start');
            recObj.lastDepAmt = srchRecDepr.getValue(i, 'last_depr_amount');
            recObj.priorNBV = srchRecDepr.getValue(i, 'prior_year_nbv');
            recObj.cumDepr = srchRecDepr.getValue(i, 'cumulative_depr');
            recObj.assetType = srchRecDepr.getValue(i, 'asset_type', 'parent_asset');
            recObj.assetTypeName = srchRecDepr.getText(i, 'asset_type', 'parent_asset');
            recObj.quantity = srchRecDepr.getValue(i, 'quantity', 'parent_asset');
            recObj.deprRule = srchRecDepr.getValue(i, 'depr_rules', 'parent_asset');
            recObj.convention = srchRecDepr.getValue(i, 'convention');
            recObj.periodCon = srchRecDepr.getValue(i, 'period_convention');
            recObj.isGrpMaster = srchRecDepr.getValue(i, 'is_group_master');
            recObj.isGrpDepr = srchRecDepr.getValue(i, 'is_group_depr');
            recObj.book = srchRecDepr.getValue(i, 'booking_id');
            recObj.bookName = srchRecDepr.getText(i, 'booking_id');
        }
        else {
            recObj.assetId = srchRecDepr.getId(i);
            recObj.assetTextId = srchRecDepr.getValue(i, 'name');
            recObj.assetName = srchRecDepr.getValue(i, 'altname');
            recObj.origCost = srchRecDepr.getValue(i, 'initial_cost');
            recObj.resValue = srchRecDepr.getValue(i, 'rv');
            recObj.lifetime = srchRecDepr.getValue(i, 'lifetime');
            recObj.fiscalYr = srchRecDepr.getValue(i, 'fyscal_year_start');
            recObj.lastDepAmt = srchRecDepr.getValue(i, 'lastDeprAmount');
            recObj.priorNBV = srchRecDepr.getValue(i, 'prior_nbv');
            recObj.cumDepr = srchRecDepr.getValue(i, 'cummulative_depr');
            recObj.assetType = srchRecDepr.getValue(i, 'asset_type');
            recObj.assetTypeName = srchRecDepr.getText(i, 'asset_type');
            recObj.quantity = srchRecDepr.getValue(i, 'quantity');
            recObj.deprRule = srchRecDepr.getValue(i, 'depr_rules');
            
            recObj.book = this.currCache.funcValCache('FAM.Util_Shared.getPrimaryBookId');
            
            recObj.bookName = recObj.book && this.currCache.funcValCache('nlapiLookupField',
                'accountingbook', recObj.book, 'name');
        }
        
        timer.start();
        updatedValues = this.depreciateRecord(recObj, this.endDate);
        limitExceeded = this.checkResults(updatedValues, srchRecDepr.getId(i),
            timer.getReadableElapsedTime());
        
        if (limitExceeded) { break; }
        else {
            this.lowerLimit = +srchRecDepr.getId(i);
            this.lastPeriod = 0;
        }
    }
    
    this.logObj.endMethod();
    return limitExceeded;
};

/**
 * Checks result object of depreciateRecord, updates success/fail count, and writes to Process Log
 *
 * Parameters:
 *     resultObj {object} - return object of depreciateRecord
 *     recId {number} - internal id of the asset/tax method that was depreciated
 *     elapsedTime {string} - elapsed time depreciating the asset, for logging purposes
 * Returns:
 *     true {boolean} - governance limit reached, requeue script first
 *     false {boolean} - proceed depreciating next asset
**/
FAM.ScheduleReport_SS.prototype.checkResults = function (resultObj, recId, elapsedTime) {
        
    this.logObj.startMethod('FAM.ScheduleReport_SS.checkResults');

    var ret = false, recordName = this.deprMethod ? 'Tax Method' : 'Asset';

    if (resultObj && resultObj.error) {
        this.procInsRec.writeToProcessLog('Processing Failed: ' + resultObj.error,
            'Error', recordName + ' Id: ' + recId);
        this.logObj.pushMsg('Failed depreciating ' + recordName + ': ' + recId + ', Elapsed Time: '
            + elapsedTime, 'error');
        this.logObj.printLog();
    }
    else if (resultObj && resultObj.isAborted) {
        this.logObj.logExecution(recordName + ' (Id: ' + recId +
            ') depreciation requeued, Elapsed Time: ' + elapsedTime);
        ret = true;
    }
    else {
        this.logObj.logExecution('Successful depreciation for ' + recordName + ': ' + recId +
            ', Elapsed Time: ' + elapsedTime);
    }

    this.logObj.endMethod();
    return ret;
};

/**
 * [OVERRIDE] Substitutes blank fields with default values
 *
 * Parameters:
 *     recObj {object} - record object to be depreciated
 *     deprDate {Date} - date up to which the record will be depreciated
 * Returns:
 *     void
**/
FAM.ScheduleReport_SS.prototype.setDefaults = function (recordObj, deprDate) {
    this.logObj.startMethod('FAM.ScheduleReport_SS.setDefaults');

    recordObj.isGrpDepr = recordObj.isGrpDepr === 'T';
    recordObj.isGrpMaster = recordObj.isGrpMaster === 'T';

    if (recordObj.isGrpDepr && recordObj.isGrpMaster) {
        recordObj.origCost = 0; // for PB defaulting; should not default to OC!
    }

    FAM.DepreciationCommon.prototype.setDefaults.apply(this, arguments);

    recordObj.status = +recordObj.status;
    if (!recordObj.status) {
        if (this.deprMethod) {
            recordObj.status = FAM.TaxMethodStatus.New;
        }
        else {
            recordObj.status = FAM.AssetStatus.New;
        }
    }

    recordObj.classfld = recordObj.classfld || 0;
    recordObj.department = recordObj.department || 0;
    recordObj.location = recordObj.location || 0;

    if (recordObj.isGrpDepr && recordObj.isGrpMaster) {
        if (this.grpMasterInfo[recordObj.subsidiary] &&
            this.grpMasterInfo[recordObj.subsidiary][recordObj.altMethod]) {

            recordObj.groupInfo = this.grpMasterInfo[recordObj.subsidiary][recordObj.altMethod];
        }
        else {
            throw 'No Data for Group Depreciation';
        }

        if (recordObj.currNBV === 0) {
            recordObj.currNBV = +recordObj.groupInfo.NB || 0;
        }
        if (recordObj.priorNBV === 0) {
            recordObj.priorNBV = +recordObj.groupInfo.PB || +recordObj.groupInfo.OC || 0;
        }

        recordObj.origCost = +recordObj.groupInfo.OC || 0;
        recordObj.currCost = +recordObj.groupInfo.CC || 0;
        recordObj.lifetime = +recordObj.groupInfo.AL;
    }

    this.logObj.endMethod();
};

/**
 * [OVERRIDE] Writes depreciation history record for the next depreciation period
 *
 * Parameters:
 *     recordObj {object} - record object to be depreciated
 *     nextDeprAmt {number} - depreciation amount for the next period
 *     nextDeprDate {Date} - date of the next depreciation
 * Returns:
 *     void
**/
FAM.ScheduleReport_SS.prototype.writeHistory = function (recordObj, nextDeprAmt,
    nextDeprDate) {

    this.logObj.startMethod('FAM.ScheduleReport_SS.writeHistory');

    var fields, currNBV, recordName, recordId, toJSON, year, month, sub, type, amount, currData, asset, met,
        histRec = new FAM.DepreciationHistory_Record(),
        noDPCurr = FAM.SystemSetup.getSetting('nonDecimalCurrencySymbols');

    currNBV = ncRoundCurr(recordObj.currNBV - nextDeprAmt, recordObj.currSym, noDPCurr);

    if (this.startDate.getTime() <= nextDeprDate.getTime() &&
        recordObj.lastPeriod + 1 > this.lastPeriod) {

        if (this.deprMethod) {
            recordName = 'Tax Method';
            recordId = recordObj.taxMetId;
        }
        else {
            recordName = 'Asset';
            recordId = recordObj.assetId;
        }

        if (this.saveResults) {
            fields = {
                asset_type : recordObj.assetType,
                transaction_type : FAM.TransactionType.Depreciation,
                asset : recordObj.assetId,
                date : nlapiDateToString(nextDeprDate),
                transaction_amount : 0,
                net_book_value : 0,
                schedule : 'T',
                scheduled_amount : nextDeprAmt,
                scheduled_nbv : currNBV,
                quantity : recordObj.quantity
            };

            if (this.deprMethod) {
                fields.alternate_depreciation = recordObj.taxMetId;
                fields.alternate_method = recordObj.altMethod;
                fields.actual_depreciation_method = recordObj.deprMethod;
            }

            if (recordObj.subsidiary) { fields.subsidiary = recordObj.subsidiary; }

            histRec.createRecord(fields);
            if (isNaN(histRec.submitRecord(false))) {
                this.logObj.logExecution('Depreciation History creation failed for ' + recordName +
                    ' id: ' + recordId, 'ERROR');
            }
        }

        toJSON = {};
        
        toJSON.year = nextDeprDate.getFullYear();
        toJSON.month = nextDeprDate.getMonth() + 1;
        
        currencyData = this.currCache.subCurrData(recordObj.subsidiary, recordObj.book);
        toJSON.currency = currencyData.currSym || '';
        
        toJSON.asset = recordObj.assetTextId || 0;
        toJSON.assetName = recordObj.assetName;
        toJSON.type = recordObj.assetTypeName || 0;
        
        toJSON.sub = FAM.Context.blnOneWorld ? recordObj.subsidiaryName : 0;
        toJSON.amount = this.isNBV ? currNBV : nextDeprAmt;
        
        toJSON.met = recordObj.methodName || 0;
        toJSON.life = recordObj.lifetime || 0;
            
        if (!FAM.Context.blnMultiBook) { toJSON.book = 0; }
        else { toJSON.book = recordObj.bookName || 'No Accounting Book'; }
        
        this.saveToJSON(toJSON);
        this.lastPeriod = recordObj.lastPeriod + 1;
    }
    
    this.instructions++;
    this.logObj.endMethod();
};

/**
 * Updates Process and Queue Instance Records
 *
 * Parameters:
 *     queueStringId {string} - deployment id if multi-queueing
 *     isDone {boolean} - determines if the process is already complete
 * Returns:
 *     void
**/ 
FAM.ScheduleReport_SS.prototype.updateBGPAndQueues = function (queueStringId, isDone) {
    this.logObj.startMethod('FAM.ScheduleReport_SS.updateBGPAndQueues');
    
    var queueRec, fields = {};
    
    if (queueStringId && this.jsonFileId) {
        fields.state = JSON.stringify({ JSONFileId : this.jsonFileId });
        
        queueRec = new FAM.Queue();
        queueRec.recordId = this.queueId;
        queueRec.submitField(fields);
    }
    else {
        this.procInsRec.stateValues.Stage = this.stage;
        this.procInsRec.stateValues.LastPeriod = this.lastPeriod;
        
        if (!queueStringId && this.jsonFileId) {
            this.procInsRec.stateValues.JSONFileId = this.jsonFileId;
        }
        if (isDone) {
            fields.status = FAM.BGProcessStatus.Completed;
        }
        if (this.csv) {
            this.procInsRec.stateValues.csv = this.csv;
        }
        if (this.xml) {
            this.procInsRec.stateValues.xml = this.xml;
        }
        fields.state = JSON.stringify(this.procInsRec.stateValues);
        this.procInsRec.submitField(fields);
    }
    
    this.logObj.endMethod();
};

/**
 * Merges temporary files created by each queue
 * 
 * Returns:
 *     true {boolean} - limit for instruction/usage/time exceeded
 *     false {boolean} - limit for instruction/usage/time not exceeded
 */
FAM.ScheduleReport_SS.prototype.mergeTempFiles = function() {
    var queueDeployments = this.searchQueueInstances(), timer = new FAM.Timer();
    
    // clear out 
    FAM_Util.saveJSONReportContents(JSON.stringify({}), this.cacheFileDescription,
        FAM.Context.getDeploymentId());
    
    // TODO: if there can be a way to sort files descending by size - more optimized
    //     : not sure if storing in memory is an option
    var limitExceeded = false;
    timer.start();
    for (var queueId = 0; queueId < queueDeployments.length; queueId++) {
        nlapiLogExecution('debug', 'mergeTempFiles', 'deprInfo: ' + JSON.stringify(this.deprInfo));
        limitExceeded = this.mergeFileContents(queueDeployments[queueId]); 
        if (limitExceeded) {
            this.saveJSONData();
            break;
        }
    }
    this.logObj.logExecution('Elapsed time for combining JSON files: ' +
        timer.getReadableElapsedTime());

    return limitExceeded;
};

/**
 * Incrementally appends properties to the merged JSON. The temporary files
 * are also updated in order to remove copied properties.
 * 
 * Parameters:
 *     queueId {String} - Deployment ID of queue that generated a file
 * Returns:
 *     true {boolean} - limit for instruction/usage/time exceeded
 *     false {boolean} - limit for instruction/usage/time not exceeded
 */
FAM.ScheduleReport_SS.prototype.mergeFileContents = function(queueId) {
    var funcname = 'mergeFileContents';
    var subs = FAM_Util.loadJSONFileContents(queueId);
    var limitExceeded = false;

    nlapiLogExecution('debug', funcname, 'subs (' + queueId + ') before: ' + JSON.stringify(subs));
    
    if (JSON.stringify(this.deprInfo) === '{}') {
        nlapiLogExecution('debug', funcname, 'fresh object');
        this.deprInfo = subs;
        subs = {};
    }
    else {
        limitExceeded = this.parseHierarchy(subs, this.deprInfo);
    }

    nlapiLogExecution('debug', funcname, 'subs (' + queueId + ') after: ' + JSON.stringify(subs));
    
    // always save file for queue id
    FAM_Util.saveJSONReportContents(JSON.stringify(subs), this.cacheFileDescription, queueId);
    
    return limitExceeded;
};

/**
 * Recursive function that builds the JSON structure
 * 
 * Parameters:
 *     children {Object} - object to copy for next call of function
 *     parentObj {Object} - stores the merged result
 *
 * Returns:
 *     true {boolean} - limit for instruction/usage/time exceeded
 *     false {boolean} - limit for instruction/usage/time not exceeded
 */
FAM.ScheduleReport_SS.prototype.parseHierarchy = function(children, parentObj) {
    var limitExceeded = false;
    for (var child in children) {
        if (children[child] !== null) {
            if (typeof children[child] === 'object') {
                parentObj[child] = parentObj[child] || {};
                limitExceeded = this.parseHierarchy(children[child], parentObj[child]);
                if (limitExceeded) {
                    nlapiLogExecution('debug', 'parseHierarchy', 'limit exceeded - object');
                    break;
                }
                if (JSON.stringify(children[child]) === '{}') {
                    delete children[child];
                }
            }
            else {
                // TODO: should we just check for all property assignments? 
                // hardcoded constants
                var constProps = [
                    'currency', // book
                    'name',     // asset
                    'life'      // month
                ];
                // only check for month
                if (constProps.indexOf(child) == -1) {
                    this.instructions++;
                    limitExceeded = this.hasExceededLimit(); 
                    if (limitExceeded) {
                        nlapiLogExecution('debug', 'parseHierarchy', 'limit exceeded - property');
                        break;
                    }
                }
                if (constProps.indexOf(child) == -1 && !this.isNBV && parentObj[child]) {
                    parentObj[child] = (+parentObj[child]) + (+children[child]);
                }
                else {
                    parentObj[child] = children[child];
                }
                delete children[child];
            }
        }
    }
    
    return limitExceeded;
};

/**
 * Searches for queue instance record that ran for the bgp record
 * 
 * Returns:
 *     queueDeployments {Array} - array of deployment id strings
 */
FAM.ScheduleReport_SS.prototype.searchQueueInstances = function() {
    var stateValues;
    var queueDeployments = [];
    var fSearch = new FAM.Search(new FAM.Queue());
    fSearch.addFilter('procInstance', null, 'is', this.procInsRec.getRecordId());
    fSearch.addColumn('deployment');
    fSearch.addColumn('state');
    fSearch.run();
    if (fSearch.results) {
        for (var i = 0; i < fSearch.results.length; i++) {
            if (fSearch.getValue(i, 'state')) {
                stateValues = JSON.parse(fSearch.getValue(i, 'state'));
                if (stateValues && stateValues.JSONFileId) {
                    queueDeployments.push(fSearch.getValue(i, 'deployment'));
                }
            }
        }
    }
    return queueDeployments;
};

/**
 * Generates the report by rendering the template
 * 
 * Parameters:
 *     none
 * Returns:
 *     void
 */
 FAM.ScheduleReport_SS.prototype.generateReport = function() {
    this.logObj.startMethod('FAM.ScheduleReport_SS.generateReport');
    
    if (JSON.stringify(this.deprInfo) === '{}') {
        this.logObj.endMethod();
        return;
    }

    var timer = new FAM.Timer();
    timer.start();
    
    var xmlReport, methodName, assetsInclude, reportType,
        dateOptions = { month : 'long', year : 'numeric' },
        templateRenderer = nlapiCreateTemplateRenderer(),
        templateId = FAM_Util.getFileId('FAM_OptScheduleReport_Template.xml'),
        template = nlapiLoadFile(templateId).getValue(),
        jsonDataString = JSON.stringify(this.deprInfo);

    methodName = this.deprMethod ?
        nlapiLookupField('customrecord_ncfar_altmethods', this.deprMethod, 'name') :
        FAM.resourceManager.GetString('custpage_dropdown_accountingmethod', 'famreports');
    assetsInclude = FAM_Util.getAssetSelection(this.leaseOption);
    reportType = this.isNBV ?
        FAM.resourceManager.GetString('custpage_netbookvalue', 'depreciationschedule2') :
        FAM.resourceManager.GetString('custpage_perioddepr', 'depreciationschedule2');
    
    template = template.replace('DSR_varMethod_DSR', methodName);
    template = template.replace('DSR_varAssetInclude_DSR', assetsInclude);
    template = template.replace('DSR_varReportType_DSR', reportType);
    template = template.replace('DSR_varStartDate_DSR', this.startDate.toLocaleDateString(undefined, dateOptions));
    template = template.replace('DSR_varEndDate_DSR', this.endDate.toLocaleDateString(undefined, dateOptions));
    
    template = FAM.Util_Shared.String.translateStrings(template, /DSR_.+?_DSR/g, 'DSR_template');
    
    jsonDataString = jsonDataString.replace(/&/g, '&amp;')
    template = template.replace('{reportData}', jsonDataString);
    
    templateRenderer.setTemplate(template);
    
    xmlReport = templateRenderer.renderToString();
    
    this.xml = FAM_Util.saveFileContents(xmlReport, this.reportFileName, '.xml', 'XMLDOC',
        this.reportFileDescription);
    
    this.logObj.logExecution('Elapsed time for redering the report: ' +
        timer.getReadableElapsedTime());

    this.logObj.endMethod();
};

/**
 * Generates a CSV version of the report
 * 
 * Parameters:
 *     none
 * Returns:
 *     {boolean} - flag that determines if the execution and time limit has been reached
 */
 FAM.ScheduleReport_SS.prototype.generateCSV = function() {
    this.logObj.startMethod('FAM.ScheduleReport_SS.generateCSV');
    
    var i, subCtr, subObj, bookCtr, books, bookObj, yearCtr, years, yearObj, typeCtr, types,
        typeObj, assetCtr, assets, assetObj, methodCtr, methods, methodObj, hasValue,
        limitExceeded = false,
        csvString = '',
        toCSV = [],
        subs = Object.keys(this.deprInfo),
        timer = new FAM.Timer();
    
    timer.start();
    
    if (this.csv) {
        csvString = this.loadCSVFiles();
    }
    
    this.logObj.logExecution('json before: ' + JSON.stringify(this.deprInfo));
    
    subs.sort();
    this.logObj.logExecution('subs: ' + JSON.stringify(subs));
    outer_loop:
    for (subCtr = 0; subCtr < subs.length; subCtr++) {
        subObj = this.deprInfo[subs[subCtr]];
        books = Object.keys(subObj);
        books.sort();
        for (bookCtr = 0; bookCtr < books.length; bookCtr++) {
            bookObj = subObj[books[bookCtr]];
            years = Object.keys(bookObj);
            years.shift(); // remove currency
            years.sort();
            for (yearCtr = 0; yearCtr < years.length; yearCtr++) {
                yearObj = bookObj[years[yearCtr]];
                types = Object.keys(yearObj);
                types.sort();
                for (typeCtr = 0; typeCtr < types.length; typeCtr++) {
                    typeObj = yearObj[types[typeCtr]];
                    assets = Object.keys(typeObj);
                    assets.sort();
                    for (assetCtr = 0; assetCtr < assets.length; assetCtr++) {
                        assetObj = typeObj[assets[assetCtr]];
                        methods = Object.keys(assetObj);
                        methods.shift(); // remove name
                        methods.sort();
                        for (methodCtr = 0; methodCtr < methods.length; methodCtr++) {
                            methodObj = assetObj[methods[methodCtr]];
                            
                            toCSV = [];
                            toCSV.push(subs[subCtr]);
                            toCSV.push(books[bookCtr]);
                            toCSV.push(bookObj.currency);
                            toCSV.push(years[yearCtr]);
                            toCSV.push(types[typeCtr]);
                            toCSV.push(assets[assetCtr]);
                            toCSV.push(assetObj.name);
                            toCSV.push(methods[methodCtr]);
                            toCSV.push(methodObj.life);
                            
                            hasValue = false;
                            for (i = 1; i <= 12; i++) {
                                toCSV.push(methodObj[i] || 0);
                                if (methodObj[i]) { hasValue = true; }
                                this.instructions++;
                            }
                            
                            if (hasValue) {
                                toCSV.map(function (val) {
                                    if (typeof val.indexOf === 'function' && val.indexOf(',') !== -1)
                                        val = '"' + val + '"';
                                    return val;
                                });
                                csvString += toCSV.join(',') + '\n';
                            }
                            delete assetObj[methods[methodCtr]];
                            limitExceeded = this.hasExceededLimit();
                            if (limitExceeded) { break outer_loop; }
                        }
                        delete typeObj[assets[assetCtr]];
                    }
                    delete yearObj[types[typeCtr]];
                }
                delete bookObj[years[yearCtr]];
            }
            delete subObj[books[bookCtr]];
        }
        delete this.deprInfo[subs[subCtr]];
    }
    
    if (csvString) {
        if (!this.csv) {
            csvString = [
                'Subsidiary',
                'Accounting Book',
                'Currency',
                'Year',
                'Asset Type',
                'Asset ID',
                'Asset Name',
                'Depreciation Method',
                'Asset Lifetime',
                'January',
                'February',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            ].join(',') + '\n' + csvString;
        }
        this.logObj.logExecution('json after: ' + JSON.stringify(this.deprInfo));
        this.jsonFileId = FAM_Util.saveJSONReportContents(JSON.stringify(this.deprInfo),
            this.cacheFileDescription);
        FAM_Util.deleteFiles(this.csvReportFileName, FAM.SystemSetup.getSetting('reportFolder'));
        this.csv = FAM_Util.saveFileContents(csvString, this.csvReportFileName, '.csv', 'CSV',
            this.reportFileDescription);
    }
    
    this.logObj.logExecution('Elapsed time for generating CSV: ' +
        timer.getReadableElapsedTime());

    this.logObj.endMethod();
    return limitExceeded;
};

/**
 * Loads CSV Files
 * 
 * Parameters:
 *     none
 * Returns:
 *     {string} - csv contents
 */
 FAM.ScheduleReport_SS.prototype.loadCSVFiles = function() {
    this.logObj.startMethod('FAM.ScheduleReport_SS.loadCSVFiles');
    
    var i, files, fileObj, contents = '', folder = FAM.SystemSetup.getSetting('reportFolder');
    
    files = FAM_Util.searchFiles(this.csvReportFileName, folder, 'CSV');

    for (i = 0; i < files.length; i++) {
        fileObj = nlapiLoadFile(files[i].getId());
        contents += fileObj.getValue();
    }
    
    this.logObj.endMethod();
    return contents;
 };
 