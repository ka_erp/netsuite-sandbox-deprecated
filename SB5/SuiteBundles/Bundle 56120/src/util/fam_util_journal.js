/**
 * © 2015 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 *
 * @NScriptName FAM Journal Utility
 * @NScriptId _fam_util_journal
 * @NApiVersion 2.x
*/

define([
        "../adapter/fam_adapter_record",
        "../adapter/fam_adapter_format",
        "../adapter/fam_adapter_config",
        "../adapter/fam_adapter_runtime",
        "../adapter/fam_adapter_error",
        "./fam_util_systemsetup",
        "./fam_util_translator",
        "../const/fam_const_customlist"
], 

function (record, format, config, runtime, error, 
          fSetup, fTranslator, fList){
    return {
        createJournalEntry : function(obj) {
            var allowCustomTrans = fSetup.getSetting("allowCustomTransaction");
            var journalId;
            
            //NOTE: obj.type is the transaction type id
            journalRec = obj.type? obj.type : record.Type.JOURNAL_ENTRY;
            var JE = record.create({
                type:journalRec,
                isDynamic : true
            });
            var journalId, 
                tempCId = '', 
                tempDId = '', 
                tempLId = '';
            
            if (obj.postingperiod) {
                JE.setValue({
                    fieldId :'postingperiod',
                    value   : obj.postingperiod
                });
            }
            
            if(runtime.isFeatureInEffect({feature :'SUBSIDIARIES'}) && obj.subId){
                JE.setValue({
                    fieldId :'subsidiary',
                    value   : obj.subId
                });
            }
            
            if(obj.trnDate) {
                if (obj.trnDate instanceof Date) {
                    JE.setValue({
                        fieldId :'trandate',
                        value   : obj.trnDate
                    });
                }
                else {
                    var tranDate = format.parse({
                        value: obj.trnDate,
                        type: format.Type.DATE
                    });
                    JE.setValue({
                        fieldId :'trandate',
                        value   : tranDate
                    });
                }
            }

            if( (obj.currId != '') && (obj.currId) && runtime.isFeatureInEffect({feature :'MULTICURRENCY'})){
                JE.setValue({
                    fieldId :'currency',
                    value   : obj.currId.toString()
                });
                JE.setValue({
                    fieldId :'exchangerate',
                    value   : obj.exchangeRate || '1'
                });
            }

            if (!Array.isArray(obj.classId)) {
                JE.setValue({
                    fieldId :'class',
                    value   : obj.classId
                });
            }
            if (!Array.isArray(obj.deptId)) {
                JE.setValue({
                    fieldId :'department',
                    value   : obj.deptId
                });
            }
            if (!Array.isArray(obj.locId)) {
                JE.setValue({
                    fieldId :'location',
                    value   : obj.locId
                });
            }
            
            if(runtime.isFeatureInEffect({feature :'MULTIBOOK'}) && obj.bookId){
                JE.setValue({
                    fieldId :'bookje',
                    value   : 'T'
                });
                JE.setValue({
                    fieldId :'accountingbook',
                    value   : obj.bookId
                });
            }
            
            if (obj.isReversal) {
                JE.setValue({
                    fieldId :'custbody_fam_jrn_is_reversal',
                    value   : obj.isReversal
                });
            }
            
            if (obj.reversalNo) {
                JE.setValue({
                    fieldId :'custbody_fam_jrn_reversal_no',
                    value   : obj.reversalNo
                });
            }
                    
            if (obj.reversalDate) {
                JE.setValue({
                    fieldId :'custbody_fam_jrn_reversal_date',
                    value   : obj.reversalDate
                });
            }
            
            // Check Journal Permission
            var permit = obj.permit || 0;
            var requireApproval = config.load({
                    type: config.Type.ACCOUNTING_PREFERENCES
                }).getValue({
                    fieldId : 'JOURNALAPPROVALS'
                });                
            var fieldName = allowCustomTrans ? 'transtatus' : 'approved';
            var approvedValue = allowCustomTrans ? fList.CustomTransactionStatus['Approved'] : true;
            var pendingValue = allowCustomTrans ? fList.CustomTransactionStatus['Pending Approval'] : false;

            if (obj.alwaysApproved || !requireApproval || +permit > fList.Permissions.Create) {
                JE.setValue({
                    fieldId : fieldName,
                    value   : approvedValue
                });
            }
            else {
                JE.setValue({
                    fieldId : fieldName,
                    value   : pendingValue
                });
            }
            
            // validate accounts & amounts
            if (obj.accts.length === 0) {
                throw error.create({                       
                    name: 'NCFAR_JOURNALERROR',
                    message: fTranslator.getString('custpage_accounts_missing'),
                    notifyOff: true
                });
            }
            
            if (obj.debitAmts.length === 0 && obj.creditAmts.length === 0){
                throw error.create({                       
                    name: 'NCFAR_JOURNALERROR',
                    message: fTranslator.getString('custpage_amounts_missing'),
                    notifyOff: true
                });
            }
            
            for(var n=0; n<obj.accts.length; ++n){
                if( obj.accts[n] == null ){
                    throw error.create({                       
                        name: 'NCFAR_JOURNALERROR',
                        message: fTranslator.getString('custpage_accounts_missing'),
                        notifyOff: true
                    });
                }
                
                if ( obj.debitAmts[n] == null && obj.creditAmts[n] == null){
                    throw error.create({                       
                        name: 'NCFAR_JOURNALERROR',
                        message: fTranslator.getString('custpage_amounts_missing'),
                        notifyOff: true
                    });
                }
            }
            
            // line counter
            for(var i=0, j=1; i<obj.accts.length; ++i){
                JE.selectNewLine({
                    sublistId : 'line'
                });             
                
                try{
                    JE.setCurrentSublistValue({
                        sublistId   : 'line', 
                        fieldId     : 'account', 
                        value       : +obj.accts[i]
                    });
                }
                catch(e){
                    return e;
                }
                
                JE.setCurrentSublistValue({
                    sublistId   : 'line', 
                    fieldId     : 'debit', 
                    value       : obj.debitAmts[i].toString() || "0"
                });
                JE.setCurrentSublistValue({
                    sublistId   : 'line', 
                    fieldId     : 'origdebit', 
                    value       : obj.debitAmts[i].toString() || "0"
                });
                JE.setCurrentSublistValue({
                    sublistId   : 'line', 
                    fieldId     : 'credit', 
                    value       : obj.creditAmts[i].toString() || "0"
                });
                JE.setCurrentSublistValue({
                    sublistId   : 'line', 
                    fieldId     : 'origcredit', 
                    value       : obj.creditAmts[i].toString() || "0"
                });
                
                if( obj.currId != null ){
                    JE.setCurrentSublistValue({
                        sublistId   : 'line', 
                        fieldId     : 'account_cur', 
                        value       : obj.currId.toString()
                    });
                    JE.setCurrentSublistValue({
                        sublistId   : 'line', 
                        fieldId     : 'account_cur_isbase', 
                        value       : 'T'
                    });
                    JE.setCurrentSublistValue({
                        sublistId   : 'line', 
                        fieldId     : 'account_cur_fx', 
                        value       : 'F'
                    });
                }
                
                if (obj.entities[i] != null){
                    JE.setCurrentSublistValue({
                        sublistId   : 'line', 
                        fieldId     : 'entity', 
                        value       : +obj.entities[i]
                    });
                }
                JE.setCurrentSublistValue({
                    sublistId   : 'line', 
                    fieldId     : 'memo', 
                    value       : obj.ref[i].toString()
                });

                // need to allow Class, Department and Location to be single value (same for all rows) or per row, i.e. string or array
                if (obj.classId) {
                    if (Array.isArray(obj.classId)) {
                        tempCId = obj.classId[i];
                    }
                    else {
                        tempCId = obj.classId;
                    }
                    JE.setCurrentSublistValue({
                        sublistId   : 'line', 
                        fieldId     : 'class', 
                        value       : tempCId
                    });
                }

                if (obj.deptId) {
                    if (Array.isArray(obj.deptId)) {
                        tempDId = obj.deptId[i];
                    }
                    else {
                        tempDId = obj.deptId;
                    }
                    JE.setCurrentSublistValue({
                        sublistId   : 'line', 
                        fieldId     : 'department', 
                        value       : tempDId
                    });
                }

                if (obj.locId) {
                    if (Array.isArray(obj.locId)) {
                        tempLId = obj.locId[i];
                    }
                    else {
                        tempLId = obj.locId;
                    }
                    JE.setCurrentSublistValue({
                        sublistId   : 'line', 
                        fieldId     : 'location', 
                        value       : tempLId
                    });
                }
                
                JE.commitLine({
                    sublistId:'line'
                });
                
                ++j;
            }
            
            try{
                journalId = JE.save({
                    enableSourcing: true,
                    ignoreMandatoryFields: true
                });
            }
            catch(e){
                throw e;
            }
            
            return journalId;
        }
    }
});