/**
 * © 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 * 
 * @NScriptName FAM Record Utility
 * @NScriptId _fam_util_record
 * @NApiVersion 2.0
*/

define(["N/record",
        "N/search",
        "../util/fam_util_envcfg"],
        
function (record, search, envcfg){
    return {
        /**
         * Validates if the Subsidiary and record is a valid combination
         * Parameters:
         *     subId {number} - internal ID of the subsidiary record
         *     recordType {record.Type} - record type of the record to check the subsidiary of
         *     recordId {number} - internal ID of the record
         *     
         * Returns:
         *     boolean - TRUE if valid, FALSE if not
         */
        isValidSubsidiaryRecCombination : function (subId, recordType, recordId){
            if(envcfg.isOneWorld()) {
                // Check if record is existing before search to prevent fake false
                if(this.isRecordExisting(recordType, recordId)) {
                    var searchObj = search.create({
                        type : recordType,
                        filters : [["internalid", "is", recordId], "and",
                                   ["isinactive", "is", "F"], "and",
                                   ["subsidiary", "anyof", [subId]]],
                        columns : ["subsidiary"]
                    });
                    var searchResultSet = searchObj.run();
                    var res = searchResultSet.getRange(0,1);
                    
                    return (res && res.length > 0) ? true : false;
                }
                
            }
            return true;
        },
        
        /**
         * Validates if the record is existing
         * Parameters:
         *     recType {string} - record type of the record to check the subsidiary of
         *     recId {number} - internal ID of the record
         *     
         * Returns:
         *     boolean - TRUE if valid, FALSE if not
         */
        isRecordExisting : function (recType, recId) {
            try {
                return Boolean(record.load({ type : recType, id : recId }))
            } catch (e) {
                log.debug("fam_util_record.isRecordExisting",
                    "type: " + recType + " recId: " + recId
                    + " is not existing.");
                return false;
            }
        },
    };
});