/**
 * © 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 * 
 * @NScriptName FAM Base Record
 * @NScriptId _fam_record
 * @NApiVersion 2.0
*/

define([],
        
function () {
    return function(type, fields) {
        this.getRecordTypeId = function() {
            return type || null;
        };
        
        this.getAllFields = function() {
            return fields || null;
        };
    };
    
});