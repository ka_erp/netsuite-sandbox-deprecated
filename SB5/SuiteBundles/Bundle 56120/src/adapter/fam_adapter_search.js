/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

define(["N/search",
        "N/error",
        ],
    famAdapterSearch);

function famAdapterSearch(search, error) {
    
    return {
        Sort : search.Sort,
        Operator : search.Operator,
        Type : search.Type,
        
        lookupFields : function(options) {
            return search.lookupFields(options);
        },
        createColumn : function(options) {
                     return search.createColumn(options);
        },
        
        generateColumn : function(name, join, summary, sort, formula, func, label) {
            var out = {
                name : name,
                join : join,
                summary : summary,
                sort : sort,
                formula : formula,
                label : label
            };
            out['function'] = func ? func : null;
            return this.createColumn(out);
        },
        
        createFilter : function(options) {
            return search.createFilter(options);
        },
        
        generateFilter: function(name, operator, values, join, formula, summary) {
            return this.createFilter({
                name : name,
                join : join,
                operator : operator,
                values : values,
                formula : formula,
                summary : summary
            });
        },
        
        create : function(options) {
            return search.create(options);
        },
        
        load : function(options) {
            return search.load(options);
        }
    }
}
