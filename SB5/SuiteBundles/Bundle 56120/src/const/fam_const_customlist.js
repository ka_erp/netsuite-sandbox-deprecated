/** 
 * © 2015 NetSuite Inc. 
 * User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 *
 * @NScriptName constants Constants - Custom Lists
 * @NScriptId _constants_const_customlist 
 * @NAPIVersion 2.0
 * 
 */ 
 
define([],

function (){ 
    var constants = {};
    
    constants.PeriodConvention = {
        "12m of 30d" : 1, // 12 months of 30 days each
        "Exact 365d" : 2 // Exact number of days in month, year has 365 days
    };

    constants.AnnualMethodEntry = {
        "Anniversary" : 1,
        "Fiscal Year" : 2
    };        

    constants.TransactionType = {
        "Acquisition" : 1,
        "Depreciation" : 2,
        "Sale or Disposal" : 3,
        "Write-Down" : 4,
        "Revaluation" : 5,
        "Sale" : 6,
        "Write-Off" : 7};

    constants.AssetStatus = {
        "Depreciating" : 2,
        "Fully Depreciated" : 3,
        "Disposed" : 4,
        "Disposed (WO)" : 5,
        "New" : 6,
        "Splitting" : 7,
        "Part Disposed" : 8
    };

    constants.DepreciationRules = {
        "Acquisition" : 1,
        "Disposal" : 2,
        "Pro-rata" : 3,
        "Mid-month" : 4
    };

    constants.FinalPeriodCon = {
        "Fully Depreciate" : 1,
        "Retain Balance" : 2
    };

    constants.BGProcessStatus = {
        InProgress : 1,
        Completed : 2,
        CompletedError : 3,
        Failed : 4,
        Queued : 5,
        Reverting : 6
    };
    
    constants.BGLogMessageType = {
        "Error" : 1,
        "Warning" : 2,
        "Message" : 3
    };

    constants.DeprActive = {
        True : 1,
        False : 2,
        OnProjComp : 3 // On Project Completion
    };

    constants.TaxMethodStatus = {
        "New" : 1,
        "Depreciating" : 2,
        "Fully Depreciated" : 3,
        "Disposed" : 4,
        "Part Disposed" : 5
    };

    constants.Conventions = {
        "None" : 1,
        "Half-Year" : 2,
        "Mid-Quarter" : 3,
        "Mid-Month" : 4
    };

    constants.DeprPeriod = {
        "Monthly" : 1,
        "Annually" : 2,
        "Fiscal Period" : 3,
        "Fiscal Year" : 4
    };

    constants.SummarizeBy = {
        "Asset Type" : 1,
        "Parent" : 2,
        "Sub-Category" : 3
    };

    constants.ProposalStatus = {
        New : 1,
        Pending : 2,
        Created : 3,
        Rejected : 4,
        Combined : 5
    };

    constants.BGPActivityType = {
        Direct : 1,
        Custom : 2,
        Planned : 3
    };

    constants.RevisionRules = {
        RemainingLife : 1,
        CurrentPeriod : 2
    };

    constants.DisposalType = {
        "Sale"      : 1,
        "Write Off" : 2
    };

    constants.ReportType = {
        "Asset Register" : 1,
        "Asset Summary" : 2,
        "DepSchedule NBV" : 3,
        "DepSchedule PD" : 4
    };

    constants.CustomTransactionStatus = {
        "Pending Approval" : "A",
        "Approved" : "B"
    };
    
    // TODO Move to fam_const_list
    constants.Permissions = {
        None   : 0,
        View   : 1,
        Create : 2,
        Edit   : 3,
        Full   : 4
    };
    
    return constants; 
});
