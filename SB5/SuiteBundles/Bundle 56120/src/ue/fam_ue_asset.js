/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NScriptName FAM - Asset UE
 * @NScriptId _fam_ue_asset
 * @NScriptType usereventscript
 * @NAPIVersion 2.0
 */

define(["../../src/adapter/fam_adapter_search",
        "../../src/adapter/fam_adapter_error",
        "../../src/util/fam_util_translator",
        "../../src/util/fam_util_envcfg",
        "../../src/const/fam_const_customlist",
        "../../src/record/fam_record_asset",
        ],
    assetUE);

function assetUE(search, error, translator, envcfg, customList, famAsset) {
    var module = {};
    var editableCompoundFields = ["name",
                                  "altname",
                                  "description",
                                  "serialNo",
                                  "alternateNo",
                                  "manufacturer",
                                  "manufactureDate",
                                  "supplier"];
    
    module.beforeLoad = function(context) {
        var fAssetRec = famAsset.createInstance();
        var fAssetRecFields = fAssetRec.getAllFields();
        var fAssetRecFieldKeys = Object.keys(fAssetRecFields);
        
        var ctxAssetRec = context.newRecord;
        var fieldsToSetDisplayType = {
            "disabled" : [],
            "inline" : [],
            "hidden" : []
        };
        
        // Create list of fields to set to inline
        var isCompound = ctxAssetRec.getValue("custrecord_is_compound");
        if (isCompound && context.type == context.UserEventType.EDIT) {
            var uneditableCompoundFields = fAssetRecFieldKeys.filter(function(key) {
                return ((editableCompoundFields.indexOf(key) === -1) && (key !== "internalid"));
            });
            
            fieldsToSetDisplayType.inline = 
                fieldsToSetDisplayType.inline.concat(uneditableCompoundFields);
        }
        
        // Set display type of fields 
        for (f in fieldsToSetDisplayType) {
            var fieldList = fieldsToSetDisplayType[f];
            for (var i = 0; i<fieldList.length; i++) {
                var fieldAlias = fieldList[i];
                var fieldToSetDisplayType = context.form.getField({
                    id: fAssetRecFields[fieldAlias]
                });
                this.updateFieldDisplayTypes(fieldToSetDisplayType, f);
            }
        }
    };
    
    module.beforeSubmit = function(context){
        var newRec = context.newRecord;
        var oldRec = context.oldRecord;
        
        // New asset and Component Of has value OR asset edit and Component Of has changed
        if ((context.type == context.UserEventType.CREATE &&
            newRec.getValue("custrecord_componentof")) ||
            (context.type == context.UserEventType.EDIT &&
            oldRec.getValue("custrecord_componentof") != 
                newRec.getValue("custrecord_componentof"))) {
            
            var oldCmptOf = oldRec ? oldRec.getValue("custrecord_componentof") : null;
            
            // If Component Of is blank, then this is not a component at all. No need to validate.
            if (newRec.getValue("custrecord_componentof")) {
                this.validateComponent(newRec, oldCmptOf);
            };
        };
    };
    
    module.updateFieldDisplayTypes = function(field, displayType) {
        try {
            field.updateDisplayType({displayType: displayType});
        }
        catch (e) {
            log.error("FIELD_DISPLAYTYPE_UPDATE_ERROR", e);
            return false;
        }
        
        return true;
    };
    
    //--- non script entry methods ---//
    module.validateComponent = function(cmptRec, oldCmptOf) {
        var cmptSub = cmptRec.getValue("custrecord_assetsubsidiary"),
            cmptType = cmptRec.getValue("custrecord_assettype"),
            cmptAccMethod = cmptRec.getValue("custrecord_assetaccmethod"),
            cmptStatus = cmptRec.getValue("custrecord_assetstatus"),
            cmptParent = cmptRec.getValue("custrecord_componentof");
        
        // Component cannot add itself as component. Because componentception.
        if (cmptRec.id === Number(cmptParent)) {
            var err = error.create({
                name: "INVALID_COMPONENT_SELF",
                message: translator.getString("custmsg_invalid_component_self", "compoundasset")
            });
            
            throw err;
        }
        
        else if (oldCmptOf) {
            var err = error.create({
                name: "ALREADY_PART_OF_COMPOUND",
                message: translator.getString("client_component_of", "compoundasset")
            });
            
            throw err;
        }
        
        if (!oldCmptOf) {
            // Load parent asset values e.g. sub, asset type, alt method, preferably thru lookupFields
            var prntFields = search.lookupFields({
                type : "customrecord_ncfar_asset",
                id : cmptParent,
                columns: ["custrecord_assetsubsidiary",
                          "custrecord_assettype",
                          "custrecord_assetaccmethod",
                          "custrecord_is_compound"]
            });
            
            var prntSub = envcfg.isOneWorld() ?
                    prntFields["custrecord_assetsubsidiary"][0]["value"] : null,
                prntType = prntFields["custrecord_assettype"][0]["value"],
                prntAccMethod = prntFields["custrecord_assetaccmethod"][0]["value"],
                prntIsCompound = prntFields["custrecord_is_compound"];
            
            if (!prntIsCompound) {
                var err = error.create({
                    name: "PARENT_NOT_COMPOUND",
                    message: translator.getString("custmsg_parent_not_compound", "compoundasset")
                });
                
                throw err;
            }
        
            // Compare values with parent then throw error. That's what this is for anyway.
            var componentFields = { sub : cmptSub, type : cmptType, accMethod : cmptAccMethod };
            var parentFields = { sub : prntSub, type : prntType, accMethod : prntAccMethod };
            var invalidMatches = this.findMismatches(componentFields, parentFields);
            if (invalidMatches.length > 0){
                var err = error.create({
                    name: "FIELD_MISMATCH",
                    message: errMsg = translator.getString(
                        "client_comp_field_mismatch", "compoundasset", [invalidMatches.join(', ')])
                });
                
                throw err;
            };
            
            // Check status of this asset then throw error
            if (!this.validateStatus(Number(cmptStatus))){
                var err = error.create({
                    name: "INVALID_STATUS",
                    message: translator.getString("client_comp_status", "compoundasset")
                });
                
                throw err;
            };
            
            // Check asset if it is not already in the same compound asset hierarchy e.g.
            // if A is parent of B, and B is parent of C, C should not be able to add A as its component
            if (!this.validateIfWithinHierarchy(Number(cmptRec.id), Number(cmptParent))) {
                var err = error.create({
                    name: "INVALID_COMPONENT_ROOT",
                    message: translator.getString("custmsg_invalid_component_root", "compoundasset")
                });
                
                throw err;
            }
        }
        
        return true;
    },
    
    module.findMismatches = function(componentFields, parentFields) {
        var invalidMatches = [];
        if (envcfg.isOneWorld() && (componentFields.sub !== parentFields.sub)) {
            invalidMatches.push(translator.getString("custpage_assetsub", "compoundasset"));
        };
        if (componentFields.type !== parentFields.type) {
            invalidMatches.push(translator.getString("custpage_assettype", "compoundasset"));
        };
        if (componentFields.accMethod !== parentFields.accMethod) {
            invalidMatches.push(translator.getString("custpage_acctg_method", "compoundasset"));
        };
        
        return invalidMatches;
    },
    
    module.validateStatus = function(status) {
        return (status === customList.AssetStatus['New'] ||
                status === customList.AssetStatus['Depreciating'] ||
                status === customList.AssetStatus['Part Disposed']);
    },
    
    module.validateIfWithinHierarchy = function(componentId, parentId) {
        // Will only search 4 times upwards and start from the parent compound.
        var SEARCH_LIMIT = 5;
        
        var isWithinHierarchy = false;
        var i = 0;
        while (!isWithinHierarchy && i < SEARCH_LIMIT-1) {
            var prntFields = search.lookupFields({
                type : "customrecord_ncfar_asset",
                id : Number(parentId),
                columns: ["custrecord_componentof"]
            });
            
            // Prepare data for next iteration
            parentId = prntFields["custrecord_componentof"][0] ? 
                Number(prntFields["custrecord_componentof"][0]["value"]) : null;
            
            if (parentId) {
                i++;
            }
            else {
                // Stop if there is no parent compound 
                break;
            }
            
            // But also check if this component is within the same compound asset hierarchy 
            if (parentId === componentId) {
                isWithinHierarchy = true;
                break;
            }
        }
        
        if ((i == SEARCH_LIMIT-1) && !isWithinHierarchy) {
            log.audit(
                "Compound asset hierarchy limit exceeded",
                ["Exceeded the hierarchy limit of ", SEARCH_LIMIT, ". ",
                 "Returning value as not a circular reference."].join(""))
        };
        
        return !isWithinHierarchy;
    }
    
    return module;
};