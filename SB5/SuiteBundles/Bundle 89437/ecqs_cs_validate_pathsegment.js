// Client script to customize ECQS Category form


// Validate path segment used by Quick Start.
// Allows using Free-Form Text field for path segment 

function onValidateField(type, name, linenum)
{
	console.log('ecqs_cs_validate_pathsegment.js:onValidateField');
	console.log('type = ' + type + ', name = ' + name + ', linenum = ' + linenum);
	
	var isValid = true;
	
	try {
		var rePathSegment = /^[A-Za-z0-9_\-]+$/;
		if (name == 'custrecord_ecqs_category_url' )
		{
			console.log('found custrecord_ecqs_category_url');
			
			var url = String(nlapiGetFieldValue(name));
			console.log('url = ' + url);
			
			isValid = url.length == 0 || rePathSegment.test(url);
			console.log('custrecord_ecqs_category_url isValid = ' + isValid);
			
			if (!isValid) {
				alert('Must be a valid path segment (/^[A–Za–z0–9_\-]+$/)');
			}
		}
	}
	catch (e) {
		console.log('ecqs_cs_validate_pathsegment.js:onValidateField exception: ' + e);
	}
	
	return isValid;
}


// Hide ATTACH, sublist record selector and CUSTOMIZE VIEW buttons on items and subcategories sublists.

function onPageInit(type)
{
	console.log('ecqs_cs_validate_pathsegment.js:onPageInit');
	console.log('type = ' + type);
	
	var selector = jQuery('#recmachcustrecord_ecqs_subcat_parent_existingrecmachcustrecord_ecqs_subcat_parent_fs_lbl_uir_label').parent('div.uir-field-wrapper');
	console.log('selector.length = ' + selector.length);
}
