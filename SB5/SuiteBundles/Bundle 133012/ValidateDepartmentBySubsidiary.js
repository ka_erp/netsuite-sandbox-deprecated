/**
 *	File Name		:	ValidateDepartmentBySubsidiary.js
 *	Function		:	Validate department based on subsidiary 
 * 	Authors			:	Manish Prajapati
 *	Company			:	Wipro Limited
 *	Release Dates	:	<To be determined>
 * 	Current Version	:	1.0
**/

function validateDepartmentBySubsiadiary(type, name)
{
		
		var subsidiaryInternalID=nlapiGetFieldValue('custrecord_wtka_discount_reas_subsidiary');
		var departMentInternalID=nlapiGetFieldValue('custrecord_wtka_map_new_department');
		
		
		if (!subsidiaryInternalID)
		{
			subsidiaryInternalID=nlapiGetFieldValue('custrecord_wtka_map_trans_subsidiary');
			departMentInternalID=nlapiGetFieldValue('custrecord_wtka_map_department');
		}
				
		
		if (!departMentInternalID) return true;
		var columns = new Array();
		columns[0] = new nlobjSearchColumn( 'name' ); 

			var searchResultData	 = new Object();		
			var filters = new Array();
				filters[0] = new nlobjSearchFilter( 'subsidiary', null, 'anyof', subsidiaryInternalID);
				filters[1] = new nlobjSearchFilter( 'internalid', null, 'is', departMentInternalID);	
				nlapiLogExecution('DEBUG','Before search department' );
				var searchresults = nlapiSearchRecord('department', null, filters,columns);					
				if (searchresults==null ||  searchresults.length<=0){
					alert("Selected department doesn't come under selected subsidiary, you must select valid department to proceed further");
				return false;
				}		
	return true;
}


