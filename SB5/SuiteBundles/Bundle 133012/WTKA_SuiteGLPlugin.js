/**
 *	File Name		:	WTKA_SuiteGLPlugin.js
 *	Function		:	Override GL posting for discount reason codes on Invoice and Credit Memos
 * 	Authors			:	Manish Prajapati
 *	Company			:	Wipro Limited
 *	Release Dates	:	<To be determined>
 * 	Current Version	:	1.0
**/

function customizeGlImpact(transactionRecord, standardLines, customLines, book)
{
	try
	{
			// Load record and Check if Discount item is applied and Get Reason code.
		var rectype = transactionRecord.getRecordType().toLowerCase();
		nlapiLogExecution('DEBUG', 'Record Type = ' +rectype);


		// Returns

		if (rectype=='creditmemo' || rectype=='cashrefund')
		{

			nlapiLogExecution('DEBUG', 'Returns', 'Entry');

			//Make entry for any transaction for Gross Sales account.
			makeEntryForEachTransaction(transactionRecord, standardLines, customLines, book);
		}


		// Sales

		if (rectype=='invoice' || rectype=='creditmemo')
		{
			mainInvoiceExecution(transactionRecord, standardLines, customLines, book);
		}

		/*if (rectype=='invoice' || rectype=='creditmemo')
		{

			nlapiLogExecution('DEBUG', 'Sales', 'Entry');

			var discountItemCollection=prepLineItemCollection(transactionRecord);



			nlapiLogExecution('DEBUG', 'Sales', JSON.stringify(discountItemCollection));

			if 	(discountItemCollection==null || discountItemCollection.length<=0)
			{
				nlapiLogExecution('DEBUG', 'No  discount item available');
				return;
			}
			nlapiLogExecution('DEBUG', 'Discount Item Collection Prepared', 'length='+discountItemCollection.length);

			for (discountItemIndex=0; discountItemIndex<discountItemCollection.length;discountItemCollection++)
			{
				var discountItemObject = discountItemCollection[discountItemIndex];
				 nlapiLogExecution('DEBUG', 'discountItemObject- Item Index  ' +discountItemIndex ,JSON.stringify(discountItemObject));

				//1. Lookup Mapping record via reason Code
				var DiscountReasonMappingLookupData= DiscountReasonMappingLookup(discountItemObject);
				 if (DiscountReasonMappingLookupData==null)
				 {
					 nlapiLogExecution('DEBUG', 'Reason Mapping lookup return NO values');
					 continue;
				}

				switch (rectype.toLowerCase())
				{
					case 'creditmemo':
						makeGenralLedgerEntryForCreditMemo(standardLines, customLines, book,DiscountReasonMappingLookupData);
						break;
					case 'invoice':
						makeGenralLedgerEntryForInvoice(standardLines, customLines, book,DiscountReasonMappingLookupData);
					break;
				}

			}
		}*/


	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'customizeGlImpact ' +e);

	}
}


/** IVAN's METHODS START**/

function mainInvoiceExecution(transactionRecord, standardLines, customLines, book){
	//For the invoice
	nlapiLogExecution('DEBUG','Ivan', 'yes');


	//Run all I need
	var transactionStatus = transactionRecord.getRecordType().toLowerCase();
	var subsidiaryInternalID=transactionRecord.getFieldValue('subsidiary');
	var transactionId = transactionRecord.getId();

	var resultSet = runDiscountReasonSearch();
	var glTable = getGLReplacementTable(resultSet);
	var uniq = _.uniq(glTable, function(person) { return person.originalGLAccount; });
	var accountGL = _.pluck(uniq, 'originalGLAccount'); accountGL =  _.map(accountGL, Number);


	nlapiLogExecution('DEBUG','glTable: uniq', JSON.stringify(uniq));
	nlapiLogExecution('DEBUG','glTable: accountGL', JSON.stringify(accountGL));

	//Find all accounts to be replaced

	var glAccountArray = getGLAccounts(standardLines); var runMe = false;
	var glAccountArrayObjs = getGLAccountsObjs(standardLines);


	//var accountsThatMatters = _(glAccountArrayObjs).filter(x.acct, function (x) { return _.contains(values, ['131','132']});
	var accountsThatMatters = _.filter(glAccountArrayObjs, function (i) {
                return this.keys.indexOf(i.acct) > -1;
	}, {
		"keys": accountGL // values to look for
	});




	nlapiLogExecution('DEBUG','glAccountArray: glAccountArray', JSON.stringify(glAccountArray));
	nlapiLogExecution('DEBUG','glAccountArray: accountsThatMatters', JSON.stringify(accountsThatMatters));


	for (var i = 0; i < uniq.length; i++){

		var origAccount = uniq[i].originalGLAccount; nlapiLogExecution('DEBUG','glTable: uniq: origAccount', origAccount);
		origAccount = parseInt(origAccount);

		if(glAccountArray.indexOf(origAccount) > -1){
			runMe = true;
			break;
		}
	}


	nlapiLogExecution('DEBUG', 'Run Me',runMe );


	//Find all the account in this transaction

	if(runMe){

		//Traverse item / reason codes
		var itemLines = getItemLines(transactionRecord);
		nlapiLogExecution('DEBUG','itemLines: itemLines', JSON.stringify(itemLines));

		itemLinesObj = itemLines.objs;

		//traverse
		for (var j = 0 ; j < itemLinesObj.length; j++){


			nlapiLogExecution('DEBUG','itemLines: itemLines[' + j + '] START',j);

			var discCode = itemLinesObj[j].discountcode;
			var discItem = itemLinesObj[j].discountitem;

			//if(false){

					var acct = accountsThatMatters[j].acct;
					var amt = accountsThatMatters[j].debit; amt = parseFloat(amt);
					var id = accountsThatMatters[j].id;

					nlapiLogExecution('DEBUG','itemLines: itemLines[' + j + ']', JSON.stringify(accountsThatMatters[j]));

					var findingGLTable = _(glTable).filter(function (x) {

							return x.originalGLAccount == acct
								&& x.discountCode == discCode
								&& x.transSubsidiary == subsidiaryInternalID
								&& x.discountItem == discItem;



								/**
									transactionLookupData.discountItem 			=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_discount_item' );
									transactionLookupData.discountCode 			=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_discount_code' );
									transactionLookupData.originalGLAccount 	= 	resultSet[resultItemIndex].getValue('custrecord_wtka_map_orig_account' );
									transactionLookupData.overrideGLAccount 	=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_new_account' );
									transactionLookupData.transSubsidiary 		=	resultSet[resultItemIndex].getValue('custrecord_wtka_discount_reas_subsidiary' );
									transactionLookupData.overrideLocation	 	=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_new_location' );
									transactionLookupData.overrideDepartment 	=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_new_department' );
								**/


						}
					);


					if(amt > 0){
						var replacementAcct = findingGLTable[0];

						nlapiLogExecution('DEBUG','itemLines: itemLines[' + j + ']: replacementAcct', JSON.stringify(replacementAcct));


						if(replacementAcct != null){
						var newLine = customLines.addNewLine();
						newLine.setAccountId(parseInt(replacementAcct.originalGLAccount));
						newLine.setCreditAmount(amt);

						var newLine = customLines.addNewLine();
						newLine.setAccountId(parseInt(replacementAcct.overrideGLAccount));
						newLine.setDebitAmount(amt);

						nlapiLogExecution('AUDIT','L/D', 'L:'+ replacementAcct.overrideLocation  + ' D:' + replacementAcct.overrideDepartment);

						if(replacementAcct.overrideLocation) newLine.setLocationId(parseInt(replacementAcct.overrideLocation));	///else newLine.setLocationId(0); // Change Loc
						if(replacementAcct.overrideDepartment) newLine.setDepartmentId(parseInt(replacementAcct.overrideDepartment));	//else newLine.setDepartmentId(0);// Change Dept

						}

						/**




						**/

					}

					nlapiLogExecution('DEBUG','itemLines: itemLines[' + j + ']', JSON.stringify(findingGLTable));
			//}


		}



	}
}

function getGLAccountsObjs(standardLines){

	var linecount 	= standardLines.getCount();
	var accountArray = new Array();

	for(var i=0; i<linecount; i++)
	{

			var line 	=  standardLines.getLine(i);
			var lineObj = new Object();

			var thisObj = new Object();
			var acct 	=  line.getAccountId();
			var credit 	=  line.getCreditAmount();
			var debit 	=  line.getDebitAmount();
			var getId 	=  line.getId();

			lineObj.acct = acct;
			lineObj.debit = debit;
			lineObj.credit = credit;
			lineObj.id = getId;

			accountArray.push(lineObj);


	}

	return accountArray;
}

function getGLAccounts(standardLines){

	var linecount 	= standardLines.getCount();
	var accountArray = new Array();

	for(var i=0; i<linecount; i++)
	{

			var line 	=  standardLines.getLine(i);

			var thisObj = new Object();
			var acct 	=  line.getAccountId();
			var amt 	=  line.getDebitAmount();
			var getId 	=  line.getId();

			accountArray.push(acct);


	}

	return accountArray;
}








function getReplacementAccount(lineindex){

	var account = 1213;
	if(lineindex == 1 || lineindex == 5 ){
		account = 51120;
	}

	return account;
}

function getGLReplacementTable(resultSet){ //, reasonCodes){

	var thisArray = new Array();

	//nlapiLogExecution('DEBUG','setGLPosings', "setGLPosings" + JSON.stringify(resultSet));

	if(resultSet != null){

			nlapiLogExecution('DEBUG','setGLPosings', "resultSet");

			for (var resultItemIndex=0; resultItemIndex<resultSet.length; resultItemIndex++){



				//if(reasonCodes){



					var transactionLookupData = new Object();

					transactionLookupData.discountItem 			=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_discount_item' );
					transactionLookupData.discountCode 			=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_discount_code' );
					transactionLookupData.originalGLAccount 	= 	resultSet[resultItemIndex].getValue('custrecord_wtka_map_orig_account' );
					transactionLookupData.overrideGLAccount 	=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_new_account' );
					transactionLookupData.transSubsidiary 		=	resultSet[resultItemIndex].getValue('custrecord_wtka_discount_reas_subsidiary' );
					transactionLookupData.overrideLocation	 	=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_new_location' );
					transactionLookupData.overrideDepartment 	=	resultSet[resultItemIndex].getValue('custrecord_wtka_map_new_department' );


					/*transactionLookupData.locationGL=resultSet.resultSet( 'custrecordwtka_map_override_location' );
					transactionLookupData.departmentGL=	resultSet.getValue( 'custrecord_wtka_map_department' );
					transactionLookupData.transactionStatusType=resultSet.getValue( 'custrecord_wtka_map_transaction_type' );*/


					var reasonCode =transactionLookupData.overrideGLAccount;
					//var idsArray = reasonCodes[0];
					//var objsArray = reasonCodes[0];

					/*
					if(idsArray.indexOf(reasonCode) >= 0)
						var pointersinarray = idsArray.indexOf(reasonCode);
						transactionLookupData.amount = 	objsArray[pointersinarray].cost;
					else{
						transactionLookupData.amount = 0.00;
					}*/

					thisArray.push(transactionLookupData);
					//nlapiLogExecution('DEBUG','discountCollection', JSON.stringify(transactionLookupData));
				//}

			}
	}
	nlapiLogExecution('DEBUG','discountCollection: thisArray', JSON.stringify(thisArray));

	return thisArray;



	//traverseLines


}


function getItemLines(transactionRecord){

	var fItemCount = transactionRecord.getLineItemCount('item');
	nlapiLogExecution('DEBUG', 'summarizeItemByReasonCodes fItemCount', fItemCount);


	var discountCollection = new Array();
	var discountCollectionIds = new Array();

	try{

		for(var a = 1 ; a <= fItemCount; a++){

			var itemType = transactionRecord.getLineItemValue('item','itemtype',a);
			if (itemType.toLowerCase()=='invtpart')
			{

					var discountCode = transactionRecord.getLineItemValue('item','custcol_ra_discount_reason',a);
					var costEstimate = transactionRecord.getLineItemValue('item','costestimate',a);
					var discountItemType = transactionRecord.getLineItemValue('item','itemtype',a);
					var discountItemReason = transactionRecord.getLineItemValue('item','custcol_ra_discount_reason',a);
					var discountItemReasonText = transactionRecord.getLineItemText('item','custcol_ra_discount_reason',a);
					var lineaccount = transactionRecord.getLineItemValue('item','account',a);
					var linesequence = transactionRecord.getLineItemValue('item','line',a);
					var item = transactionRecord.getLineItemValue('item','item',a);
					var discount = transactionRecord.getLineItemValue('item','custcol_ra_discount_reason',a);
					var discountItem = transactionRecord.getLineItemValue('item','custcol_erp_discount_item',a);

					costEstimate = parseFloat(costEstimate);

					if(lineaccount == 131 && costEstimate > 0){

						var newLine = customLines.addNewLine();
						newLine.setAccountId(1198);
						newLine.setCreditAmount(parseFloat(costEstimate));

						var newLine = customLines.addNewLine();
						newLine.setAccountId(131);
						newLine.setDebitAmount(parseFloat(costEstimate));

					}


					nlapiLogExecution('DEBUG', 'lineaccount', lineaccount + " > " + linesequence + " > " + item + " > " + discount);


					var discObj = new Object();

					discObj.discountcode = discountCode;
					discObj.cost = costEstimate;
					discObj.discounttype = discountItemType;
					discObj.discountreason = discountItemReason;
					discObj.discountitemreason = discountItemReason;
					discObj.discountitem = discountItem;
					discObj.item = item;
					discObj.lineaccount = lineaccount;
					discObj.linesequence = linesequence;


					discountCollectionIds.push(linesequence);
					discountCollection.push(discObj);


					/*
					if(costEstimate != null){
						costEstimate = parseFloat(costEstimate);
					}
					if(costEstimate == null){
						costEstimate = 0.0;
					}


					if(discountCollectionIds.indexOf(discountCode) < 0){

						var discObj = new Object();

						discObj.discountcode = discountCode;
						discObj.cost = costEstimate;
						discObj.discounttype = discountItemType;
						discObj.discountreason = discountItemReason;
						discObj.discountitemreason = discountItemReason;
						discObj.item = item;
						discObj.lineaccount = lineaccount;
						discObj.linesequence = linesequence;


						discountCollectionIds.push(discountCode);
						discountCollection.push(discObj);

					}else{

						var discountIndex  =  discountCollectionIds.indexOf(discountCode);

						var tempObj = discountCollection[discountIndex];
						tempObj.cost = tempObj.cost + costEstimate;
						discountCollection[discountIndex] = tempObj;


					} */

					nlapiLogExecution('DEBUG','discountCollection', JSON.stringify(discountCollection));
			}
		}

		var returnObject = new Object();
		returnObject.ids = discountCollectionIds;
		returnObject.objs = discountCollection;

		return returnObject;
	}
	catch(ex) {}

}


function runDiscountReasonSearch()
{
try{
		var columns = new Array();
		columns[0] = new nlobjSearchColumn( 'custrecord_wtka_map_discount_item' ); ///Item
		columns[1] = new nlobjSearchColumn( 'custrecord_wtka_map_discount_code' ); //Discount Reason Code
		columns[2] = new nlobjSearchColumn( 'custrecord_wtka_map_orig_account' );//original Gl Account
		columns[3] = new nlobjSearchColumn( 'custrecord_wtka_map_new_account' );//Override Gl Account
		columns[4] = new nlobjSearchColumn( 'custrecord_wtka_map_new_location' );//Location
		columns[5] = new nlobjSearchColumn( 'custrecord_wtka_map_new_department' );//department
		columns[6] = new nlobjSearchColumn( 'custrecord_wtka_discount_reas_subsidiary' );//subsidiary


		var filters = new Array();
		filters[0] = new nlobjSearchFilter( 'isinactive', null, 'is', 'F');

			var searchResultData	 = new Object();
			//var filters = new Array();
				/*filters[0] = new nlobjSearchFilter( 'custrecord_wtka_map_discount_code', null, 'is', discountItemObject.discountItemDiscountReasonInternalID);
				filters[1] = new nlobjSearchFilter( 'custrecord_wtka_map_discount_item', null, 'is', discountItemObject.discountItemInternalID);
				filters[2] = new nlobjSearchFilter( 'custrecord_wtka_map_orig_account', null, 'anyof', discountItemObject.COGSAccountInternalID);
				filters[3] = new nlobjSearchFilter( 'custrecord_wtka_discount_reas_subsidiary', null, 'anyof', discountItemObject.transactionSubsidiaryInternalID);
				filters[4] = new nlobjSearchFilter( 'isinactive', null, 'is', 'F');			*/


				nlapiLogExecution('DEBUG','Before DiscountReasonMappingLookup' );
				var searchresults = nlapiSearchRecord('customrecord_wtka_map_discount_account', null, filters,columns);
				if (searchresults==null ){ return null;}else{return searchresults;}

				/*nlapiLogExecution('DEBUG','Post DiscountReasonMappingLookup', searchresults.length);
				var searchresult = searchresults[0];
				searchResultData.originalGLAccount = searchresult.getValue( 'custrecord_wtka_map_orig_account' );
				searchResultData.overrideGLAccount=	searchresult.getValue( 'custrecord_wtka_map_new_account' );
				searchResultData.locationGL=	searchresult.getValue( 'custrecord_wtka_map_new_location' );
				searchResultData.departmentGL=	searchresult.getValue( 'custrecord_wtka_map_new_department' );
				searchResultData.discountReasonCode=discountItemObject.discountItemDiscountReasonCodeInText; // to add discount code in GL entry
				var savedSearchValues = "originalGLAccount: "+searchResultData.originalGLAccount + 	" overrideGLAccount: "	+searchResultData.overrideGLAccount +" locationGL:"+searchResultData.locationGL +" departmentGL:"+searchResultData.departmentGL	+ "discountItemObject.discountItemDiscountReasonCodeInText="+discountItemObject.discountItemDiscountReasonCodeInText;
				nlapiLogExecution('DEBUG',  ' Saved Search Result', savedSearchValues);*/


	}
	catch(err)
	{
		nlapiLogExecution('DEBUG',' DiscountReasonMappingLookup',err);
		return null;
	}
}

/** IVAN's METHODS END**/




function makeEntryForEachTransaction(transactionRecord, standardLines, customLines, book)
{
	try
	{
		// Load record and Check if Discount item is applied and Get Reason code.
			var transactionType = transactionRecord.getRecordType().toLowerCase();
			var subsidiaryInternalID=transactionRecord.getFieldValue('subsidiary');
			//Lookup based on subsidiary and account




			var lookupTransactionResult=lookupTransactionGlMapping(subsidiaryInternalID,transactionType);

			if(lookupTransactionResult==null || lookupTransactionResult.length<=0)
			{
				nlapiLogExecution('DEBUG', "makeEntryForEachTransaction:: Lookup doesn't have any  values", "Subsidiary id = "+subsidiaryInternalID + " transactionType ="+transactionType  );
				return;
			}

			nlapiLogExecution('DEBUG', 'makeEntryForEachTransaction: Transaction Type= ', transactionType);

			var linecount 	= standardLines.getCount();

			nlapiLogExecution('DEBUG', 'makeEntryForEachTransaction: linecount ', linecount);

			for(var lineIndex=0; lineIndex<linecount; lineIndex++)
			{



				var line 	=  standardLines.getLine(lineIndex);
				nlapiLogExecution('DEBUG', 'makeEntryForEachTransaction: for: line ' + lineIndex, line);

				if (!line.isPosting()) continue;
				var acc 	= line.getAccountId();
				var cls 	= line.getClassId();
				var loc 	= line.getLocationId();
				var dep 	=line.getDepartmentId();
				var debit 	= line.getDebitAmount();
				var credit 	=line.getCreditAmount();



				if(acc == null)		continue;

				var debitVal =  parseFloat(debit);
				var creditVal =  parseFloat(credit);

				//nlapiLogExecution('DEBUG', lineIndex + ' Credit and Dabit Values', 'debitVal='+debitVal +' creditVal='+creditVal );

					for (var resultItemIndex=0; resultItemIndex<lookupTransactionResult.length; resultItemIndex++)
					{

						var searchresult = lookupTransactionResult[resultItemIndex];
						var transactionLookupData= new Object();

						transactionLookupData.originalGLAccount = searchresult.getValue('custrecord_wtka_map_original_gl_account' );
						transactionLookupData.overrideGLAccount=	searchresult.getValue('custrecord_wtka_map_override_account' );
						transactionLookupData.locationGL=searchresult.getValue( 'custrecordwtka_map_override_location' );
						transactionLookupData.departmentGL=	searchresult.getValue( 'custrecord_wtka_map_department' );
						transactionLookupData.transactionStatusType=searchresult.getValue( 'custrecord_wtka_map_transaction_type' );

						var amt=0;
						if (acc== transactionLookupData.originalGLAccount)
						{
							nlapiLogExecution('DEBUG',' makeEntryForEachTransaction-Search Result Values', 'acc='+acc +' transactionLookupData.originalGLAccount='+transactionLookupData.originalGLAccount   );
						nlapiLogExecution('DEBUG',' transactionLookupData:::::', JSON.stringify(transactionLookupData));
							if (debitVal>0)
							{
								nlapiLogExecution('DEBUG', 'Start Credit Entry ->makeCreditGLEntry::  ', 'debitVal=' + debitVal+'amount='+amt);
								amt= debitVal + (parseFloat(credit) * parseFloat(-1));
								makeCreditGLEntry(standardLines,customLines,book, transactionLookupData, amt, acc,loc,dep,cls);
								nlapiLogExecution('DEBUG', 'Completed Credit Entry ->makeCreditGLEntry::  ', 'debitVal=' + debitVal+'amount='+amt);
							}
							if(creditVal>0)
							{
								nlapiLogExecution('DEBUG',' transactionLookupData:::::', JSON.stringify(transactionLookupData));
								nlapiLogExecution('DEBUG', 'Start Debit Entry ->makeCreditGLEntry::  ', 'creditVal=' + creditVal + 'amount='+amt);
								amt= creditVal + (parseFloat(debit) * parseFloat(-1));
								makeDebitGLEntry(standardLines,customLines,book, transactionLookupData, amt, acc,loc,dep,cls);
								nlapiLogExecution('DEBUG', 'Completed Debit Entry ->makeCreditGLEntry::  ', 'creditVal=' + creditVal + 'amount='+amt);
							}
							break;
							}
					}
			}

	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'makeEntryForEachTransaction ' +e);

	}
}

function lookupTransactionGlMapping(subsidiaryInternalID,  transactionStatus)
{
	try{

	/* We realized that there is no way to compare transaction type with Transaction type list designed in  WTKA - Transaction GL Mapping si below code is for setting internal ids of transaction type*/
		switch (transactionStatus)
		{
			case 'creditmemo':
				transactionStatus=1;
				break;
			case 'cashrefund':
			transactionStatus=2;
			break;
			default:
			transactionStatus=-1;

		}

		if (transactionStatus<=0) {return null;}

		nlapiLogExecution('DEBUG',' lookupTransactionGlMapping:::::transactionStatus', 'Transaction Status='+transactionStatus);
		var columns = new Array();
		columns[0] = new nlobjSearchColumn( 'custrecord_wtka_map_original_gl_account' ); ///Original GL Account
		columns[1] = new nlobjSearchColumn( 'custrecord_wtka_map_override_account' ); //Override GL account
		columns[2] = new nlobjSearchColumn( 'custrecord_wtka_map_trans_subsidiary' );//Subsidiary
		columns[3] = new nlobjSearchColumn( 'custrecordwtka_map_override_location' );//Override Location
		columns[4] = new nlobjSearchColumn( 'custrecord_wtka_map_department' );//Override Department
		columns[5] = new nlobjSearchColumn( 'custrecord_wtka_map_transaction_type' );//Transaction Type


			var filters = new Array();

				filters[0] = new nlobjSearchFilter( 'custrecord_wtka_map_transaction_type', null, 'is', transactionStatus);
				filters[1] = new nlobjSearchFilter( 'custrecord_wtka_map_trans_subsidiary', null, 'anyof', subsidiaryInternalID);
				filters[2] = new nlobjSearchFilter( 'isinactive', null, 'is', 'F');

				var searchresults = nlapiSearchRecord('customrecord_wtka_map_trans_mapping', null, filters,columns);
				return searchresults;
				/*
				nlapiLogExecution('DEBUG','Post lookupTransactionGlMapping', searchresults.length);
				var searchresult = searchresults[0];
				searchResultData.originalGLAccount = searchresult.getValue( 'custrecord_wtka_map_orig_account' );
				searchResultData.overrideGLAccount=	searchresult.getValue( 'custrecord_wtka_map_new_account' );
				searchResultData.locationGL=searchresult.getValue( 'custrecord_wtka_map_new_location' );
				searchResultData.departmentGL=	searchresult.getValue( 'custrecord_wtka_map_new_department' );
				searchResultData.transactionStatusType=searchresult.getValue( 'custrecord_wtka_map_transaction_type' );
				return searchResultData;
				*/
	}
	catch(ex)
	{
		nlapiLogExecution('ERROR', 'lookupTransactionGlMapping ' +ex);
		return null;
	}

}


/*customer account Debit entry*/
function makeDebitGLEntry(standardLines,customLines,book, transactionLookupData, amt, acc,loc,dep,cls)
{
	try
	{
				nlapiLogExecution('DEBUG', 'makeCreditGLEntry', 'Location=' +transactionLookupData.locationGL + 'Depart=' +transactionLookupData.departmentGL);
				// remove the original amount
					var newLine = customLines.addNewLine();
					newLine.setAccountId(parseInt(transactionLookupData.originalGLAccount )); //TODO : Need to change
					if (isSafeValue(cls))	newLine.setClassId(parseInt(cls));
						nlapiLogExecution('DEBUG', 'makeDebitGLEntry:: Start to Reverse account ', 'Amount=' + amt);
					/*Reverse Account */
					var to_loc= transactionLookupData.locationGL;
					var to_dep=transactionLookupData.departmentGL;
					if(loc.length > 0)		newLine.setLocationId(parseInt(loc));
					if(dep.length > 0)		newLine.setDepartmentId(parseInt(dep));
					if(parseFloat(amt) > 0)	newLine.setDebitAmount(amt);

					newLine.setMemo("Reverse Entry");

					var newLine = customLines.addNewLine();

					nlapiLogExecution('DEBUG', 'makeDebitGLEntry:: Start to Override account ', 'Amount=' + amt +' to_loc='+to_loc +' to_dep='+to_dep);
					/*Override Account */
					newLine.setAccountId(parseInt(transactionLookupData.overrideGLAccount));//TODO : Need to change
					if (isSafeValue(cls)) newLine.setClassId(parseInt(cls));
					if(to_loc.length > 0)		newLine.setLocationId(parseInt(to_loc));	// Change Loc
					if(to_dep.length > 0)		newLine.setDepartmentId(parseInt(to_dep));	// Change Dept
					//if(parseFloat(amt) < 0)
						newLine.setCreditAmount(amt );
					//else					newLine.setCreditAmount(parseFloat(amt));
					newLine.setMemo("Reclass " );


	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'makeGenralLedgerEntry'+ e.toString());

	}

}


/*customer account credit entry*/
function makeCreditGLEntry(standardLines,customLines,book, transactionLookupData, amt, acc,loc,dep,cls)
{
	try
	{
				nlapiLogExecution('DEBUG', 'makeCreditGLEntry', 'Location=' +transactionLookupData.locationGL + 'Depart=' +transactionLookupData.departmentGL);
				// remove the original amount
					var newLine = customLines.addNewLine();
					newLine.setAccountId(parseInt(transactionLookupData.originalGLAccount )); //TODO : Need to change
					if (isSafeValue(cls))	newLine.setClassId(parseInt(cls));

					nlapiLogExecution('DEBUG', 'makeCreditGLEntry:: Start to Reverse account ', 'Amount=' + amt);
					/*Reverse Account */
					var to_loc= transactionLookupData.locationGL;
					var to_dep=transactionLookupData.departmentGL;
					if(loc.length > 0)		newLine.setLocationId(parseInt(loc));
					if(dep.length > 0)		newLine.setDepartmentId(parseInt(dep));
					if(parseFloat(amt) >= 0)	newLine.setCreditAmount(amt);

					newLine.setMemo("Reverse Entry");

					var newLine = customLines.addNewLine();
					nlapiLogExecution('DEBUG', 'makeCreditGLEntry:: Start to Override account ', 'Amount=' + amt +' to_loc='+to_loc +' to_dep='+to_dep);
					/*Override Account */
					newLine.setAccountId(parseInt(transactionLookupData.overrideGLAccount));//TODO : Need to change
					if (isSafeValue(cls)) newLine.setClassId(parseInt(cls));
					if(to_loc.length > 0)		newLine.setLocationId(parseInt(to_loc));	// Change Loc
					if(to_dep.length > 0)		newLine.setDepartmentId(parseInt(to_dep));	// Change Dept
					if(parseFloat(amt) < 0)	newLine.setDebitAmount(amt );
					else						newLine.setDebitAmount(parseFloat(amt));
					newLine.setMemo("Reclass " );


	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'makeGenralLedgerEntry'+ e.toString());

	}

}






function makeGenralLedgerEntryForInvoice(standardLines,customLines,book, DiscountReasonMappingLookupData)
{
	try
	{
		nlapiLogExecution('DEBUG', 'inside makeGenralLedgerEntry for Invoice');
			//2. re-class summary
			var linecount 	= standardLines.getCount();
			nlapiLogExecution('DEBUG', 'standardLines Count', linecount);

			for(var i=0; i<linecount; i++)
			{
				var line 	=  standardLines.getLine(i);

				if (!line.isPosting()) continue;
				var acc 	= line.getAccountId();
				var cls 	= line.getClassId();
				var loc 	= line.getLocationId();
				var dep 	=line.getDepartmentId();
				var debit 	= line.getDebitAmount();
				var credit 	=line.getCreditAmount();
				if(acc == null)		continue;
				var debitVal =  parseFloat(debit);
				if(debitVal == 0)	continue; //Debit only
				var amt =  debitVal + (parseFloat(credit) * parseFloat(-1));
				//// var amt =  parseFloat(debit) + (parseFloat(credit) * parseFloat(-1));
				if (amt==0) continue;
				var key = acc + '|' + cls + '|' + loc + '|' + dep;
				var key 	= 'lineposting?='+line.isPosting()+' acc: ' + acc + ' | cls: ' + cls + ' | loc: ' + loc + ' | dep: ' + dep + ' | debit: ' + debit + ' | credit: ' + credit;
				nlapiLogExecution('DEBUG', i + ' | VALUES', key);

				if(acc==DiscountReasonMappingLookupData.originalGLAccount)
				{
						// remove the original amount
					var newLine = customLines.addNewLine();
					newLine.setAccountId(parseInt(DiscountReasonMappingLookupData.originalGLAccount )); //TODO : Need to change
					if (isSafeValue(cls))	newLine.setClassId(parseInt(cls));
					/*Reverse Account */
					var to_loc= DiscountReasonMappingLookupData.locationGL;
					var to_dep=DiscountReasonMappingLookupData.departmentGL;
					if(loc.length > 0)		newLine.setLocationId(parseInt(loc));
					if(dep.length > 0)		newLine.setDepartmentId(parseInt(dep));
					if(parseFloat(amt) >= 0)	newLine.setCreditAmount(amt);
					//else						newLine.setCreditAmount(parseFloat(amt) * parseFloat(-1));
					newLine.setMemo("Reverse based on discount " +DiscountReasonMappingLookupData.discountReasonCode);
					nlapiLogExecution('DEBUG', 'Ready for Line change', "Amount="+amt );
					var newLine = customLines.addNewLine();

					/*Override Account */
					newLine.setAccountId(parseInt(DiscountReasonMappingLookupData.overrideGLAccount));//TODO : Need to change
					if (isSafeValue(cls)) newLine.setClassId(parseInt(cls));
					if(to_loc.length > 0)		newLine.setLocationId(parseInt(to_loc));	// Change Loc
					if(to_dep.length > 0)		newLine.setDepartmentId(parseInt(to_dep));	// Change Dept
					if(parseFloat(amt) < 0)	newLine.setDebitAmount(amt );
					else						newLine.setDebitAmount(parseFloat(amt));
					newLine.setMemo("Reclass based on discount " +DiscountReasonMappingLookupData.discountReasonCode );
				}
			}
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'makeGenralLedgerEntry'+ e.toString());
		/*
		try {
			var err_title = 'Unexpected error';
			var err_description = '';
			if (e){
				if ( e instanceof nlobjError ){
					err_description = err_description + ' ' + e.getCode() + '|' + e.getDetails();
				} else {
					err_description = err_description + ' ' + e.toString();
				};
			};
			nlapiLogExecution('ERROR', 'Log Error makeGenralLedgerEntry ' + err_title, err_description);
		} catch(ex) {

		};*/
	}

}

function makeGenralLedgerEntryForCreditMemo(standardLines,customLines,book, DiscountReasonMappingLookupData)
{
	try
	{
		nlapiLogExecution('DEBUG', 'inside makeGenralLedgerEntry credit memo');
			//2. re-class summary
			var linecount 	= standardLines.getCount();
			nlapiLogExecution('DEBUG', 'standardLines Count', linecount);

			for(var i=0; i<linecount; i++)
			{
				var line 	=  standardLines.getLine(i);
                                nlapiLogExecution('DEBUG', 'line inside for  = ' +line);
                                nlapiLogExecution('DEBUG', 'line.isPosting() = ' +line.isPosting());

				if (!line.isPosting()) continue;
				var acc 	= line.getAccountId();
				var cls 	= line.getClassId();
				var loc 	= line.getLocationId();
				var dep 	=line.getDepartmentId();
				var debit 	= line.getDebitAmount();
				var credit 	=line.getCreditAmount();

                                 nlapiLogExecution('DEBUG', 'line acc   = ' +acc );
                                 nlapiLogExecution('DEBUG', 'line creditVal   = ' +creditVal );

				if(acc == null)		continue;
				//var debitVal =  parseFloat(debit);
				var creditVal =  parseFloat(credit);
				if(creditVal == 0)	continue; //Credit Only
				var amt =  creditVal + (parseFloat(debit) * parseFloat(-1));
                                nlapiLogExecution('DEBUG', 'line amt  = ' +amt);
				//// var amt =  parseFloat(debit) + (parseFloat(credit) * parseFloat(-1));
				if (amt==0) continue;
				var key = acc + '|' + cls + '|' + loc + '|' + dep;
				var key 	= 'lineposting?='+line.isPosting()+' acc: ' + acc + ' | cls: ' + cls + ' | loc: ' + loc + ' | dep: ' + dep + ' | debit: ' + debit + ' | credit: ' + credit;
				nlapiLogExecution('DEBUG', i + ' | VALUES', key);

				if(acc==DiscountReasonMappingLookupData.originalGLAccount)
				{
						// remove the original amount
					var newLine = customLines.addNewLine();
					newLine.setAccountId(parseInt(DiscountReasonMappingLookupData.originalGLAccount )); //TODO : Need to change
					if (isSafeValue(cls))	newLine.setClassId(parseInt(cls));
					/*Reverse Account */
					var to_loc= DiscountReasonMappingLookupData.locationGL;
					var to_dep=DiscountReasonMappingLookupData.departmentGL;
					if(loc.length > 0)		newLine.setLocationId(parseInt(loc));
					if(dep.length > 0)		newLine.setDepartmentId(parseInt(dep));
					if(parseFloat(amt) >= 0)	newLine.setDebitAmount(amt);
					//else						newLine.setCreditAmount(parseFloat(amt) * parseFloat(-1));
					newLine.setMemo("Reverse based on discount " +DiscountReasonMappingLookupData.discountReasonCode);
					nlapiLogExecution('DEBUG', 'Ready for Line change', "Amount="+amt );
					var newLine = customLines.addNewLine();

					/*Override Account */
					newLine.setAccountId(parseInt(DiscountReasonMappingLookupData.overrideGLAccount));//TODO : Need to change
					if (isSafeValue(cls)) newLine.setClassId(parseInt(cls));
					if(to_loc.length > 0)		newLine.setLocationId(parseInt(to_loc));	// Change Loc
					if(to_dep.length > 0)		newLine.setDepartmentId(parseInt(to_dep));	// Change Dept
					if(parseFloat(amt) < 0)	newLine.setCreditAmount(amt );
					else						newLine.setCreditAmount(parseFloat(amt));
					newLine.setMemo("Reclass based on discount " +DiscountReasonMappingLookupData.discountReasonCode );

				}

			}
	}
	catch(e)
	{
		nlapiLogExecution('ERROR', 'makeGenralLedgerEntry'+ e.toString());
		/*
		try {
			var err_title = 'Unexpected error';
			var err_description = '';
			if (e){
				if ( e instanceof nlobjError ){
					err_description = err_description + ' ' + e.getCode() + '|' + e.getDetails();
				} else {
					err_description = err_description + ' ' + e.toString();
				};
			};
			nlapiLogExecution('ERROR', 'Log Error makeGenralLedgerEntry ' + err_title, err_description);
		} catch(ex) {

		};*/
	}

}


/* List of discount Items and their COGS accounts*/
function prepLineItemCollection(transactionRecord)
{
	try
	{
		var discountItemCollection =  new Array();
		var numlineItems = transactionRecord.getLineItemCount('item');
		 nlapiLogExecution('DEBUG', 'Number of Transaction Line ', "numlineItems="+numlineItems );
		for (var itemIndex=1; itemIndex<= numlineItems; itemIndex++)
		{

			itemInternalID = transactionRecord.getLineItemValue('item','item',itemIndex);
			itemDiscountReason = transactionRecord.getLineItemValue('item','custcol_ra_discount_reason',itemIndex);

			var itemType = transactionRecord.getLineItemValue('item','itemtype',itemIndex);
			 nlapiLogExecution('DEBUG', 'Inside .....prepLineItemCollection', "Item Type="+itemType.toLowerCase()  + ' itemDiscountReason= '+itemDiscountReason);
			if (itemType.toLowerCase()=='invtpart')
			{
				nlapiLogExecution('DEBUG', 'Inventory  Item Found', "Item Index="+itemIndex );
				var discountItemIndex=  itemIndex+1 //Look of discount item which will be always next item.
				//We are checking discount item in next line item. Index should be less then total item length.
				if (discountItemIndex > numlineItems) {continue;}


				//Check if Next item is Discount Item - Otherwise Ignore
				var discountItemType = transactionRecord.getLineItemValue('item','itemtype',discountItemIndex);
				var discountItemReason = transactionRecord.getLineItemValue('item','custcol_ra_discount_reason',discountItemIndex);
				var discountItemReasonText = transactionRecord.getLineItemText('item','custcol_ra_discount_reason',discountItemIndex);
				nlapiLogExecution('DEBUG', 'Inventory  Item Found', "discountItemType="+discountItemType +' discountItemReason='+discountItemReason +' discountItemIndex='+discountItemIndex + 'discountItemReasonText ='+discountItemReasonText);
				/*Checking if Discount of above inventory item available*/
				if (discountItemType.toLowerCase()!='discount'){continue;}
				var discountItemDetail = new Object();
				var discountItemInternalID = transactionRecord.getLineItemValue('item','item',discountItemIndex);
				discountItemDetail.discountItemInternalID=discountItemInternalID;
				discountItemDetail.discountItemDiscountReasonInternalID=	discountItemReason;
				discountItemDetail.discountItemDiscountReasonCodeInText=	discountItemReasonText;


				//Get COGS Account of INVENTORY Item - IMP - NOT from discount item.

				// Get Item COGS Account
				var itemFields = ['expenseaccount', 'type'];
				var inventoryItemInfo = nlapiLookupField('item',itemInternalID , itemFields);
				discountItemDetail.COGSAccountInternalID= inventoryItemInfo.expenseaccount;
				discountItemDetail.inventoryItemIDForDiscountAdded=itemInternalID;
				discountItemDetail.hasDiscountItem=true;
				discountItemDetail.transactionSubsidiaryInternalID=transactionRecord.getFieldValue('subsidiary');
				nlapiLogExecution('DEBUG', 'prepLineItemCollection', "discountItemDetail.COGSAccountInternalID="+discountItemDetail.COGSAccountInternalID +' discountItemDetail.transactionSubsidiaryInternalID='+discountItemDetail.transactionSubsidiaryInternalID +' discountItemDetail.discountItemDiscountReasonCodeInText='+discountItemDetail.discountItemDiscountReasonCodeInText );
				discountItemCollection=pushDataIntoCollection(discountItemCollection,discountItemDetail);
			}


		}
		return discountItemCollection;

		} catch(ex) {
			nlapiLogExecution('ERROR', 'ERROR in prepLineItemCollection' +ex);
			return null;
		};
}

function isSafeValue(dataIn)
{
	if(dataIn!= null && dataIn.length >0)
	{return true;}
	else false;
}

//Lookup Original GL account and Override GL account based on Discount reason.
function DiscountReasonMappingLookup(discountItemObject)
{
try{
		var columns = new Array();
		columns[0] = new nlobjSearchColumn( 'custrecord_wtka_map_discount_item' ); ///Item
		columns[1] = new nlobjSearchColumn( 'custrecord_wtka_map_discount_code' ); //Discount Reason Code
		columns[2] = new nlobjSearchColumn( 'custrecord_wtka_map_orig_account' );//original Gl Account
		columns[3] = new nlobjSearchColumn( 'custrecord_wtka_map_new_account' );//Override Gl Account
		columns[4] = new nlobjSearchColumn( 'custrecord_wtka_map_new_location' );//Location
		columns[5] = new nlobjSearchColumn( 'custrecord_wtka_map_new_department' );//department
		columns[6] = new nlobjSearchColumn( 'custrecord_wtka_discount_reas_subsidiary' );//subsidiary

			var searchResultData	 = new Object();
			var filters = new Array();
				filters[0] = new nlobjSearchFilter( 'custrecord_wtka_map_discount_code', null, 'is', discountItemObject.discountItemDiscountReasonInternalID);
				filters[1] = new nlobjSearchFilter( 'custrecord_wtka_map_discount_item', null, 'is', discountItemObject.discountItemInternalID);
				filters[2] = new nlobjSearchFilter( 'custrecord_wtka_map_orig_account', null, 'anyof', discountItemObject.COGSAccountInternalID);
				filters[3] = new nlobjSearchFilter( 'custrecord_wtka_discount_reas_subsidiary', null, 'anyof', discountItemObject.transactionSubsidiaryInternalID);
				filters[4] = new nlobjSearchFilter( 'isinactive', null, 'is', 'F');


				nlapiLogExecution('DEBUG','Before DiscountReasonMappingLookup' );
				var searchresults = nlapiSearchRecord('customrecord_wtka_map_discount_account', null, filters,columns);
				if (searchresults==null ){ return null;}

				nlapiLogExecution('DEBUG','Post DiscountReasonMappingLookup', searchresults.length);
				var searchresult = searchresults[0];
				searchResultData.originalGLAccount = searchresult.getValue( 'custrecord_wtka_map_orig_account' );
				searchResultData.overrideGLAccount=	searchresult.getValue( 'custrecord_wtka_map_new_account' );
				searchResultData.locationGL=	searchresult.getValue( 'custrecord_wtka_map_new_location' );
				searchResultData.departmentGL=	searchresult.getValue( 'custrecord_wtka_map_new_department' );
				searchResultData.discountReasonCode=discountItemObject.discountItemDiscountReasonCodeInText; // to add discount code in GL entry
				var savedSearchValues = "originalGLAccount: "+searchResultData.originalGLAccount + 	" overrideGLAccount: "	+searchResultData.overrideGLAccount +" locationGL:"+searchResultData.locationGL +" departmentGL:"+searchResultData.departmentGL	+ "discountItemObject.discountItemDiscountReasonCodeInText="+discountItemObject.discountItemDiscountReasonCodeInText;
				nlapiLogExecution('DEBUG',  ' Saved Search Result', savedSearchValues);
				return searchResultData;
	}
	catch(err)
	{
		nlapiLogExecution('DEBUG',' DiscountReasonMappingLookup',err);
		return null;
	}
}

function pushDataIntoCollection(discountItemCollection, discountItemDetail)
{
var hasMatched = false;
		for (i=0; i < discountItemCollection.length; i++) {
			var discountItem =discountItemCollection[i];

			if (discountItem.COGSAccountInternalID==discountItemDetail.COGSAccountInternalID && discountItem.discountItemDiscountReasonInternalID==discountItemDetail.discountItemDiscountReasonInternalID  &&discountItem.discountItemInternalID==discountItemDetail.discountItemInternalID)
			{
				hasMatched= true;
			}
		}
	if (!hasMatched)
	{
		discountItemCollection.push(discountItemDetail)
	}
	return discountItemCollection;
}
