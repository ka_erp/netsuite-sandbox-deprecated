/**
 * © 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 Oct 2015     jmarimla         Roles access
 * 2.00       06 Nov 2015     jmarimla         Employees access
 * 3.00       11 Nov 2015     jmarimla         Changed help text; fix increment bug
 *
 */

var onServerLog = true;
var MSG_TITLE = 'APM SETUP SL MAIN';
var apmServLib = psgp_apm.serverlibrary;
var logger = new psgp_apm.serverlibrary.logger(MSG_TITLE, false);

var SCRIPTID = {
        DB : '_apm_db_sl_main'
      , SIA : '_apm_sia_sl_main'
      , SPM : '_apm_spm_sl_main'
      , SSA : '_apm_ssa_sl_main'
};

function entryPoint(request, response){

    switch (request.getMethod()) {
    case 'GET':
        getData(request, response);
        MSG_TITLE = 'getData Return';
        break;
    case 'POST':
        postData(request, response);
        MSG_TITLE = 'postData Return';
        break;
    }
    
}

function getData(request, response) {
    var form = nlapiCreateForm('APM Setup');
    
    //define components
    var fHelp = form.addField('custpage_f_help', 'help', 'Set up permission to the Application Performance Management SuiteApp');
    
    var permissionsTab = form.addTab('custpage_tab_permissions', 'Permissions');
    
    var slRolesAccess = form.addSubList('custpage_sl_access_roles', 'inlineeditor', 'Roles', 'custpage_tab_permissions');
    var slRolesAccess_fRole = slRolesAccess.addField('custpage_sl_access_role', 'select', 'Role', '-118');
    slRolesAccess_fRole.setMandatory(true);
    var slRolesAccess_ftop10 = slRolesAccess.addField('custpage_sl_access_role_top10', 'checkbox', 'Top 10 Most Utilized');
    
    var slEmployeesAccess = form.addSubList('custpage_sl_access_employees', 'inlineeditor', 'Employees', 'custpage_tab_permissions');
    var slEmployeesAccess_fEmployee = slEmployeesAccess.addField('custpage_sl_access_employee', 'select', 'Employee', 'employee');
    slEmployeesAccess_fEmployee.setMandatory(true);
    var slEmployeesAccess_ftop10 = slEmployeesAccess.addField('custpage_sl_access_employee_top10', 'checkbox', 'Top 10 Most Utilized');
    
    var bSubmit = form.addSubmitButton('Save');
    var apmDbLink = nlapiResolveURL('SUITELET', 'customscript'+SCRIPTID.DB, 'customdeploy'+SCRIPTID.DB);
    var cancelScript = "window.location = '"+apmDbLink+"'";
    var bCancel = form.addButton('custpage_b_cancel', 'Cancel', cancelScript );
    
    //enter current settings
    var sc = new Array();
    sc.push(new nlobjSearchColumn('custrecord_apm_setup_ra_role'));
    sc.push(new nlobjSearchColumn('custrecord_apm_setup_ra_top10'));
    var searchResults = nlapiSearchRecord('customrecord_apm_setup_roles_access', null, null, sc);
    var slRow = 1;
    for (var i in searchResults) {
        var role = searchResults[i].getValue('custrecord_apm_setup_ra_role');
        var top10 = searchResults[i].getValue('custrecord_apm_setup_ra_top10');
        if (!role) continue;
        slRolesAccess.setLineItemValue('custpage_sl_access_role', slRow, role);
        slRolesAccess.setLineItemValue('custpage_sl_access_role_top10', slRow, top10);
        slRow++;
    }
    
    sc = new Array();
    sc.push(new nlobjSearchColumn('custrecord_apm_setup_ea_employee'));
    sc.push(new nlobjSearchColumn('custrecord_apm_setup_ea_top10'));
    searchResults = nlapiSearchRecord('customrecord_apm_setup_employees_access', null, null, sc);
    slRow = 1;
    for (var i in searchResults) {
        var employee = searchResults[i].getValue('custrecord_apm_setup_ea_employee');
        var top10 = searchResults[i].getValue('custrecord_apm_setup_ea_top10');
        if (!employee) continue;
        slEmployeesAccess.setLineItemValue('custpage_sl_access_employee', slRow, employee);
        slEmployeesAccess.setLineItemValue('custpage_sl_access_employee_top10', slRow, top10);
        slRow++;
    }
    
    response.writePage(form);
    
}

function postData(request, response) {
    
    //delete existing 
    var rolesSearchResults = nlapiSearchRecord('customrecord_apm_setup_roles_access', null, null, null);
    for (var i in rolesSearchResults) {
        var id = rolesSearchResults[i].getId();
        nlapiDeleteRecord('customrecord_apm_setup_roles_access', id);
    }
    
    var employeesSearchResults = nlapiSearchRecord('customrecord_apm_setup_employees_access', null, null, null);
    for (var i in employeesSearchResults) {
        var id = employeesSearchResults[i].getId();
        nlapiDeleteRecord('customrecord_apm_setup_employees_access', id);
    }
    
    //save new settings
    var rolesAccessCount = request.getLineItemCount('custpage_sl_access_roles');
    var rolesRecIds = new Array();
    var roles = new Array();
    for (var i = 1; i < rolesAccessCount + 1; i++) {
        var role = request.getLineItemValue('custpage_sl_access_roles', 'custpage_sl_access_role', i);
        var top10 = request.getLineItemValue('custpage_sl_access_roles', 'custpage_sl_access_role_top10', i);
        var record = nlapiCreateRecord('customrecord_apm_setup_roles_access');
        record.setFieldValue('custrecord_apm_setup_ra_role', role);
        record.setFieldValue('custrecord_apm_setup_ra_top10', top10);
        rolesRecIds.push(nlapiSubmitRecord(record));
        roles.push(role);
    }
    logger.debug('setup roles ids', rolesRecIds);
    
    var employeesAccessCount = request.getLineItemCount('custpage_sl_access_employees');
    var employeesRecIds = new Array();
    var employees = new Array();
    for (var i = 1; i < employeesAccessCount + 1; i++) {
        var employee = request.getLineItemValue('custpage_sl_access_employees', 'custpage_sl_access_employee', i);
        var top10 = request.getLineItemValue('custpage_sl_access_employees', 'custpage_sl_access_employee_top10', i);
        var record = nlapiCreateRecord('customrecord_apm_setup_employees_access');
        record.setFieldValue('custrecord_apm_setup_ea_employee', employee);
        record.setFieldValue('custrecord_apm_setup_ea_top10', top10);
        employeesRecIds.push(nlapiSubmitRecord(record));
        employees.push(employee);
    }
    logger.debug('setup employees ids', employeesRecIds);
    
    //set deployment settings
    var deployIds = getDeploymentIds();
    logger.debug('deploy ids', deployIds);

    employees = (employees && employees.length > 0) ? filterEmployeesWithAccess(employees) : new Array();
    logger.debug('filtered employees ids', employees);
    
    if (deployIds && (deployIds.length <= 4)) {
        var submitDeploy = new Array();
        for (var i in deployIds) {
            var sdRec = nlapiLoadRecord('scriptdeployment', deployIds[i]);
            
            sdRec.setFieldValue('allemployees', 'F');
            sdRec.setFieldValue('allpartners', 'F');
            sdRec.setFieldValue('allroles', 'F');
            
            sdRec.setFieldValues('audslctrole', roles); 
            sdRec.setFieldValues('audemployee', employees);
            submitDeploy.push(nlapiSubmitRecord(sdRec));
        }
        logger.debug('submit deploy', submitDeploy);
    }
    
    nlapiSetRedirectURL('SUITELET', 'customscript'+SCRIPTID.DB, 'customdeploy'+SCRIPTID.DB, null, null);
}

function getDeploymentIds() {
    var sf = new Array();
    var sc = new Array();

    sf = [
        ['scriptid', 'is', 'customdeploy'+SCRIPTID.DB]
      , 'or'
      , ['scriptid', 'is', 'customdeploy'+SCRIPTID.SIA]
      , 'or'
      , ['scriptid', 'is', 'customdeploy'+SCRIPTID.SPM]
      , 'or'
      , ['scriptid', 'is', 'customdeploy'+SCRIPTID.SSA]
    ];

    var searchResults = nlapiSearchRecord('scriptdeployment', null, sf, sc);

    var ids = new Array();
    for (var i in searchResults) {
        ids.push(searchResults[i].getId());
    }
    
    return ids;

}

function filterEmployeesWithAccess( employees ) {
    var sf = new Array();
    var sc = new Array();

    sf.push(new nlobjSearchFilter('giveaccess', null, 'is', 'T'));
    sf.push(new nlobjSearchFilter('internalid', null, 'anyof', employees));

    var searchResults = nlapiSearchRecord('employee', null, sf, sc);
    
    var ids =  new Array();
    for (var i in searchResults) {
        ids.push(searchResults[i].getId());
    }
    
    return ids;
};