/**
 * � 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 *
 * @author jseego
 * @NApiVersion 2.0 
 * @NModuleScope public 
 */

define([],

function() {

    var MAP_REC_EP_FLAG = {
        'vendorpayment': 'custbody_9997_is_for_ep_eft',
        'customerpayment': 'custbody_9997_is_for_ep_dd' // TODO
    };
    
    var MAP_REC_PAYMENT_OPTIONS = {
        'vendorpayment': [
        { fieldId: 'toach', type: 'value', defaultValue: false, restoreValue: '' },
        { fieldId: 'billpay', type: 'value', defaultValue: false, restoreValue: '' },
        { fieldId: 'tobeprinted', type: 'value', defaultValue: false, restoreValue: '' },
        { fieldId: 'tranid', type: 'value', defaultValue: '', restoreValue: '' }
        ],
        'customerpayment': [
        // TODO: Out of scope for now. Dependent on fix of Issue 392395
        // { fieldId: 'paymentmethod', type: 'text', defaultValue: '', restoreValue: '' },
        // { fieldId: 'creditcard', type: 'text', defaultValue: '', restoreValue: '' },
        // { fieldId: 'ccnumber', type: 'value', defaultValue: '', restoreValue: '' },
        // { fieldId: 'ccexpiredate', type: 'value', defaultValue: '', restoreValue: '' },
        // { fieldId: 'ccname', type: 'value', defaultValue: '', restoreValue: '' },
        // { fieldId: 'ccstreet', type: 'value', defaultValue: '', restoreValue: '' },
        // { fieldId: 'ccapproved', type: 'value', defaultValue: '', restoreValue: '' },
        // { fieldId: 'pnrefnum', type: 'value', defaultValue: '', restoreValue: '' }
        ]
    };
    
    function isEpPaymentMethod(rec, fieldId){
        return (fieldId == MAP_REC_EP_FLAG[rec.type]);
    }

    function isOtherPaymentMethod(rec, fieldId){
        var otherPaymentOptions = MAP_REC_PAYMENT_OPTIONS[rec.type] || [];
        for (var i = 0, ii = otherPaymentOptions.length; i < ii; i++){
            if (fieldId == otherPaymentOptions[i].fieldId) 
                return true;
        }
        return false;
    }

    function setElectronicPaymentsFlag(rec, value){
        rec.setValue({ fieldId: MAP_REC_EP_FLAG[rec.type], value: value, ignoreFieldChange: true });
    }

    function getElectronicPaymentsFlag(rec){
        return rec.getValue({ fieldId: MAP_REC_EP_FLAG[rec.type] });
    }

    function clearOtherPaymentOptions(rec){
        var otherPaymentOptions = MAP_REC_PAYMENT_OPTIONS[rec.type] || [];
        for (var i = 0, ii = otherPaymentOptions.length; i < ii; i++){
            var objOption = otherPaymentOptions[i];
            if (objOption.type == 'value')
                rec.setValue({ fieldId: objOption.fieldId, value: objOption.defaultValue, ignoreFieldChange: true });
            else if (objOption.type == 'text')
                rec.setText({ fieldId: objOption.fieldId, text: objOption.defaultValue, ignoreFieldChange: true });
        }
    }
    
    function saveOtherPaymentOptions(rec){
        var otherPaymentOptions = MAP_REC_PAYMENT_OPTIONS[rec.type] || [];
        for (var i = 0, ii = otherPaymentOptions.length; i < ii; i++){
            var objOption = otherPaymentOptions[i];
            if (objOption.type == 'value')
                objOption.restoreValue = rec.getValue({ fieldId: objOption.fieldId });
            else if (objOption.type == 'text')
                objOption.restoreValue = rec.getText({ fieldId: objOption.fieldId });
        }
    }

    function restoreOtherPaymentOptions(rec){
        var otherPaymentOptions = MAP_REC_PAYMENT_OPTIONS[rec.type] || [];
        for (var i = 0, ii = otherPaymentOptions.length; i < ii; i++){
            var objOption = otherPaymentOptions[i];
            if (objOption.restoreValue){
            if (objOption.type == 'value')
                rec.setValue({ fieldId: objOption.fieldId, value: objOption.restoreValue, ignoreFieldChange: true });
            else if (objOption.type == 'text')
                rec.setText({ fieldId: objOption.fieldId, text: objOption.restoreValue, ignoreFieldChange: true });
            }
        }
    }    
    
    function isUserOverriden(rec){
        if (rec.type == 'vendorpayment'){
        // case when in bulk payments and user ticked TO BE PRINTED checkbox
        return rec.getValue({ fieldId: 'tobeprinted' });
    }
        else if (rec.type == 'customerpayment'){
            // no possible user override
            return false;
        }
    }
    
    // checks whether a payment record is created via Electronic Payments
    function isEpPayProcessing(rec){
        var isForEP = getElectronicPaymentsFlag(rec);
        if (rec.type == 'vendorpayment'){
            // Check # (tranid) has value containing '/'
            return (isForEP && ((rec.getValue({ fieldId: 'tranid' })).indexOf('/') > -1));
        }
        else if (rec.type == 'customerpayment'){
            return isForEP;
        }
    }

    return {
        isEpPaymentMethod: isEpPaymentMethod,
        isOtherPaymentMethod: isOtherPaymentMethod,
        setElectronicPaymentsFlag: setElectronicPaymentsFlag,
        getElectronicPaymentsFlag: getElectronicPaymentsFlag,
        clearOtherPaymentOptions: clearOtherPaymentOptions,
        saveOtherPaymentOptions: saveOtherPaymentOptions,
        restoreOtherPaymentOptions: restoreOtherPaymentOptions,
        isUserOverriden: isUserOverriden,
        isEpPayProcessing: isEpPayProcessing
    };
});            