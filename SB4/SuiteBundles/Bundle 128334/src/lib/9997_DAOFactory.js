/**
 * © 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 Sep 2015     aalcabasa
 * @NModuleScope TargetAccount
 */
define(["./wrapper/9997_NsWrapperRecord"], function(record) {
    
    /**
     * Returns a basic DAO with fields and the record type setup
     * 
     * @param {Object} params
     * @param {String} params.recordType
     * @param {Object} params.fieldMap - a mapping of object attribute names and their corresponding NS Record field names
     * 
     * */
    function getDAO(params) {
        return new DAO(params);
    }
    

    function DAO(params) {
        var recordType = params.recordType;
        var fieldMap = params.fieldMap;
        var subListMaps = params.subListMaps;
        
        this.create = function create(object) {
            var rec = record.create({
                type: recordType
            });
            for ( var objFieldId in fieldMap) {
                var nsFieldId = fieldMap[objFieldId];
                rec.setValue(nsFieldId, object[objFieldId]);
            }
            return rec.save();
        };
        
        this.retrieve = function retrieve(id) {
            var object = {
                subLists: {}
            };
            var rec = record.load({
                type: recordType,
                id: id
            });
            for ( var i in fieldMap) {
            	var fieldName = fieldMap[i];
                object[i] = rec.getValue(fieldName);
                
                if (rec.getField(fieldName).type === 'select'){
                	object[i + 'Text'] = rec.getText(fieldName); 
                }
            }
            
            var subListMap;
            var params;
            var sublists = object.sublists;
            for ( var j in subListMaps) {
                subListMap = subListMaps[j];
                params = {
                    record: rec,
                    subList: j,
                    subListMap: subListMap
                };
                sublists[j] = extractSubListValues(params);
            }
            
            return object;
        };

        this.retrieveNsRecord = function retrieveNsRecord(id) {
            var wrapperRecord = record.load({
                type: recordType,
                id: id
            });

            return wrapperRecord.getRecord();
        };

        function extractSubListValues(params) {
            var rec = params.record;
            var subListName = params.subList;
            var subListMap = params.subListMap;
            
            var count = rec.getLineCount({
                sublistId: subListName
            });
            var lines = [];
            for ( var i = 0; i < count; i++) {
                var line = {};
                for ( var j in subListMap) {
                    line[j] = rec.getSublistValue({
                        sublistId: subListName,
                        fieldId: subListMap[j],
                        line: i
                    });
                }
                lines.push(line);
            }
            return lines;
        }
        
    }
    
    return {
        getDAO: getDAO
    };
});
