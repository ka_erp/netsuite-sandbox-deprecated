/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Jun 2016     aalcabasa
 *
 * @NModuleScope Public
 */
define([], function() {
    
    function processRenderedContent(rawContent, isNotXML) {
        var processedContent = {};
        
        processedContent.sequenceId = extractReturnContent(rawContent);
        processedContent.fileContent = extractFileContent(rawContent, isNotXML);
        

        return processedContent;
    }
    
    function extractReturnContent(rawContent) {
        var returnContent = rawContent.substring(rawContent.indexOf("#RETURN START#") + "#RETURN START#".length);
        returnContent = returnContent.trim();
        returnContent = returnContent.substring(0, rawContent.indexOf("#RETURN END#"));
        
        return returnContent;
    }
    
    function extractFileContent(rawContent, isNotXML) {
        var formattedFileContent = rawContent.substring(rawContent.indexOf("#OUTPUT START#") + "#OUTPUT START#".length);
        formattedFileContent = formattedFileContent.replace(/(\r\n|\n|\r)/, "");
        formattedFileContent = formattedFileContent.substring(0, formattedFileContent.indexOf("#OUTPUT END#"));
        
        if (rawContent.indexOf("#REMOVE EOL#") > -1) {
            formattedFileContent = formattedFileContent.substring(0, formattedFileContent.indexOf("#REMOVE EOL#") - 2);
        }
        
        if (isNotXML) {
            formattedFileContent = formattedFileContent.replace(/&amp;/g, "&");
            formattedFileContent = formattedFileContent.replace(/&AMP;/g, "&");
        }
        formattedFileContent = formattedFileContent.replace(/\&apos;/g, '\'');
        
        return formattedFileContent;
    }
    
    return {
        processRenderedContent: processRenderedContent
    };
})
