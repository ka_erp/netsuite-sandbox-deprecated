/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 May 2016     aalcabasa
 *
 * @NModuleScope TargetAccount
 */
define(['../lib/wrapper/9997_NsWrapperRuntime'],

function(runtime) {
    var PFA = 'pfa';
    var CBANK = 'cbank';
    var SEQUENCES = 'sequences';
    var CURRENCIES = 'currencies';
    var PAYMENTS = 'payments';
    var ENTITIES = 'entities';
    var ENTITY_BANK_DETAILS = 'ebanks';
    var TRANSACTIONS = 'transactions';
    
    function processDataSource(parameters) {
        var dataSource = parameters.dataSource;
        var templateRenderer = parameters.templateRenderer;
        
        templateRenderer.setTemplateContents(dataSource.getTemplateContent());
        templateRenderer.addRecord(PFA, dataSource.getPfa());
        templateRenderer.addRecord(CBANK, dataSource.getBankDetails());
        
        if (dataSource.requiresSequences()) {
            templateRenderer.addSearchResults(SEQUENCES, dataSource.getSequences());
        }
        
        var currencies = dataSource.getCurrencies();
        if (runtime.isMultiCurrency()) {
            templateRenderer.addSearchResults(CURRENCIES, currencies);
        } else {
            templateRenderer.addRecord(CURRENCIES, currencies);
        }
        
        templateRenderer.addSearchResults(PAYMENTS, dataSource.getPayments());
        templateRenderer.addSearchResults(ENTITIES, dataSource.getEntities());
        templateRenderer.addSearchResults(ENTITY_BANK_DETAILS, dataSource.getEntityBankDetails());
        templateRenderer.addSearchResults(TRANSACTIONS, dataSource.getTransactions());
        
        return templateRenderer;
    }
    
    return {
        processDataSource: processDataSource
    };
});
