/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 May 2016     aalcabasa
 *
 * @NModuleScope TargetAccount
 */
define([
    '../lib/wrapper/9997_NsWrapperError',
    './9997_EPDataSourceProcessor',
    '../lib/wrapper/9997_NsWrapperFile',
    './9997_PaymentFileContentPostProcessor',
    '../data/9997_PFADAO'],

function(error, epDataSourceProcessor, file, postProcessor,pdaDAO) {
    function generateFile(parameters) {
        var templateRenderer = parameters.templateRenderer;
        var dataSource = parameters.dataSource;
        
        epDataSourceProcessor.processDataSource({
            dataSource: dataSource,
            templateRenderer: templateRenderer
        });
        
        var renderedContent = '';
        try {
            renderedContent = templateRenderer.renderAsString();
        } catch (e) {
            log.debug('EP_SOME_ERROR_CODE', e);
            throw error.create({
                code: 'EP_SOME_ERROR_CODE',
                error: e
            });
        }
        
        
        
        var fileData = dataSource.getFileDetails();
        
        var processedResults = postProcessor.processRenderedContent(renderedContent, fileData.isXML);
        
        fileData.contents = processedResults.fileContent;
        
        if (fileData.sequenceId){
            pdaDAO.updateFields(dataSource.pfaId, {
                'custrecord_2663_sequence_id':fileData.sequenceId
            });
        }
        
        
        var paymentFile = file.create(fileData);
        var fileId = paymentFile.save();
        
        log.debug('Generated File ID', fileId);
        
        return fileId;
    }
    
    return {
        generateFile: generateFile
    };
});
