/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount
 */

define(['../lib/wrapper/9997_NsWrapperSearch',
        '../lib/wrapper/9997_NsWrapperRuntime'],

function (search, runtime) {

    var templateExpressions = [];

    function initializeFreemarkerTemplate(template) {
        templateExpressions = parseTemplateExpressions(template);
    }

    function getPaymentColumns() {
        var mapColumns = {};

        mapColumns['internalid'] = createColumn('internalid');
        mapColumns['entity'] = createColumn('entity');
        mapColumns['memo'] = createColumn('memo');

        if (isMultiCurrency()) {
            mapColumns['fxamount'] = createColumn('fxamount');                
            mapColumns['currency'] = createColumn('currency');                
            mapColumns['formulacurrency'] = createFormulaColumn('formulacurrency', 'ABS(ROUND({fxamount}*{exchangerate},2))');
        }

        mapColumns = mergeColumnsWithExpressions(mapColumns, templateExpressions, 'payment');

        return getObjectValues(mapColumns);
    }
    
    function getEntityColumns() {
        var mapColumns = {};

        mapColumns['firstname'] = createColumn('firstname');
        mapColumns['lastname'] = createColumn('lastname');
        mapColumns['isinactive'] = createColumn('isinactive');

        mapColumns = mergeColumnsWithExpressions(mapColumns, templateExpressions, 'entity');

        return getObjectValues(mapColumns);
    }
    
    function getEbankColumns() {
        var mapColumns = {};

        mapColumns['internalid'] = createColumn('internalid');
        mapColumns['custrecord_2663_parent_vendor'] = createColumn('custrecord_2663_parent_vendor');
        mapColumns['custrecord_2663_parent_employee'] = createColumn('custrecord_2663_parent_employee');
        mapColumns['custrecord_2663_parent_customer'] = createColumn('custrecord_2663_parent_customer');
        mapColumns['custrecord_2663_parent_cust_ref'] = createColumn('custrecord_2663_parent_cust_ref');

        mapColumns = mergeColumnsWithExpressions(mapColumns, templateExpressions, 'ebank');

        return getObjectValues(mapColumns);
    }
    
    function getTransactionColumns() {
        var mapColumns = {};

        mapColumns['applyingtransaction'] = createColumn('applyingtransaction');
        mapColumns = mergeColumnsWithExpressions(mapColumns, templateExpressions, 'transaction');

        return getObjectValues(mapColumns);
    }
    
    function mergeColumnsWithExpressions(mapColumns, templateExpressions, type) {
        for (var i = 0; i < templateExpressions.length; i++){    
            // get values under type
            var expression = templateExpressions[i];
            while (expression.indexOf(type + '.') > -1){
                var start = expression.indexOf(type + '.') + (type + '.').length;                    

                // to find end point, loop until character other than alphanumeric and underscore is found
                var end = start;
                var length = expression.length;
                while (end <= length && /[a-zA-Z_0-9]/.exec(expression.charAt(end))){
                    end++;
                }
                var column = expression.substring(start,end);

                // remove entity columns that are specific to employee/vendor/customer
                if (type == 'entity'){
                    var arrInvalidColumns = [];
                    arrInvalidColumns.push('companyname');
                    arrInvalidColumns.push('isperson');
                    arrInvalidColumns.push('printoncheckas');
                    arrInvalidColumns.push('socialsecuritynumber');
                    arrInvalidColumns.push('vatregnumber');
                    if (arrInvalidColumns.indexOf(column) > -1){
                        column = null;
                    }
                }

                if (!isMultiCurrency() && ((type == 'payment' && ['fxamount', 'currency'].indexOf(column) > -1) || 
                        (type == 'transaction' && ['appliedtoforeignamount', 'applyingforeignamount'].indexOf(column) > -1))) {
                    column = null;
                }

                // store only if not yet added before, and not removed due to FreeMarker library exception
                if (column && !mapColumns[column]){
                    mapColumns[column] = createColumn(column);
                }

                expression = expression.substring(end);
            }
        }

        return mapColumns;
    }

    function createColumn(columnName) {
        return search.createColumn({
            name: columnName
        });
    }

    function createFormulaColumn(columnName, formula) {
        return search.createColumn({
            name: columnName,
            formula: formula
        });
    }

    function isMultiCurrency() {
        return runtime.isMultiCurrency();
    }

    function getObjectValues(obj) {
        var values = [];

        for (var key in obj){
            values.push(obj[key]);
        }
        return values;
    }

    function parseTemplateExpressions(template) {
        var templateExpressions = [];    

        // parse free marker expressions 

        // those inside ${ }
        var regEx = /\${([^}]+)}/g;
        var currentMatch;
        while (currentMatch = regEx.exec(template)){
            templateExpressions.push(currentMatch[1]);
        }

        // those inside <# >
        regEx = /<#([^}]+)>/g;
        while (currentMatch = regEx.exec(template)){
            templateExpressions.push(currentMatch[1]);
        }        

        return templateExpressions;
    }

    return {
        initializeFreemarkerTemplate: initializeFreemarkerTemplate,
        getPaymentColumns: getPaymentColumns,
        getEntityColumns: getEntityColumns,
        getEbankColumns: getEbankColumns,
        getTransactionColumns: getTransactionColumns
    };
});
