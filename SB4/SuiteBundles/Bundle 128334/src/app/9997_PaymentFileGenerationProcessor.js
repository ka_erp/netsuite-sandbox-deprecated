/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope Public
 */

define(['../lib/wrapper/9997_NsWrapperRecord',
        '../lib/wrapper/9997_NsWrapperFormat',
        '../lib/wrapper/9997_NsWrapperTask',
        '../lib/wrapper/9997_NsWrapperRuntime',
        '../lib/wrapper/9997_NsWrapperRedirect',
        '../lib/wrapper/9997_NsWrapperConfig'],

function (record, format, task, runtime, redirect, config) {

    // TODO: link to constants.js
    var PAYQUEUED = 1;
    var PAYMARKPAYMENTS = 5;
    var EP_PROCESSPAYMENTS = '1';
    var EP_EFT = '1';
    
    var TEMPLATE_TYPE_MAP = {
        'eft': 1,
        'dd': 2
    };

    function storeSavedSearch(params){
        var objConfig = config.load({ type: config.Type.USER_PREFERENCES });
        var savedSearchField = params.templatetype === 'eft' ? 'custscript_9997_file_gen_saved_search' : 'custscript_9997_dd_file_gen_saved_search';

        objConfig.setValue({
            fieldId: savedSearchField,
            value: params.transavedsearch
        });

        objConfig.save();
    };
    
    function createAdminRecord(params){
        var bankAccount = JSON.parse(params.bankaccount);
        
        var formData = {
            bankaccount: bankAccount.id,
            bankAccountInfo: bankAccount,
            datetobeprocessed: params.datetobeprocessed,
            referencenote: params.referencenote,
            totalamount: format.parse({
                value: params.totalamount,
                type: format.Type.CURRENCY
            }),
            trancount: params.trancount,
            templatetype: TEMPLATE_TYPE_MAP[params.templatetype]
        };

        var rec = record.create({ type: 'customrecord_2663_file_admin', isDynamic: true });
        rec.setValue({ fieldId: 'custrecord_2663_file_processed', value: PAYQUEUED });
        rec.setValue({ fieldId: 'custrecord_2663_last_process', value: EP_PROCESSPAYMENTS });
        rec.setValue({ fieldId: 'custrecord_2663_bank_account', value: formData.bankaccount });
        rec.setValue({ fieldId: 'custrecord_ep_priority', value: 3 });
        rec.setValue({ fieldId: 'custrecord_2663_process_date', value: format.parse({ value: formData.datetobeprocessed, type: format.Type.DATE }) });
        rec.setValue({ fieldId: 'custrecord_2663_file_creation_timestamp', value: new Date() });
        rec.setValue({ fieldId: 'custrecord_9997_file_gen_suitelet_data', value: JSON.stringify(formData) });
        rec.setValue({ fieldId: 'custrecord_2663_payment_type', value: formData.templatetype });
        rec.setValue({ fieldId: 'custrecord_2663_ref_note', value: formData.referencenote });
        var recId = rec.save({ ignoreMandatoryFields: true });

        return recId;
    }
    
    function startPaymentLinking(pfaId, params){
        // update PFA status
        record.submitFields({
            type: 'customrecord_2663_file_admin', 
            id: pfaId,
            values: { 'custrecord_2663_file_processed': PAYMARKPAYMENTS }
        });
    
        // trigger map reduce script
        var mrTask = task.create({ taskType: task.TaskType.MAP_REDUCE });
        mrTask.scriptId = 'customscript_9997_payment_to_admin_mr';
        mrTask.deploymentId = 'customdeploy_9997_payment_to_admin_mr';
        mrTask.params = {
            'custscript_9997_file_gen_saved_search_id': params.transavedsearch,
            'custscript_9997_file_gen_pfa_id': pfaId,
            'custscript_9997_file_gen_user': runtime.getCurrentUser().id,
            'custscript_9997_file_gen_template_type': params.templatetype
        };
        mrTask.submit();
    }
    
    function redirectToAdminRecord(recId){
        redirect.toRecord({ id: recId, type: 'customrecord_2663_file_admin' });    
    }

    return {
        storeSavedSearch: storeSavedSearch,
        createAdminRecord: createAdminRecord,
        startPaymentLinking: startPaymentLinking,
        redirectToAdminRecord: redirectToAdminRecord
    };
});
