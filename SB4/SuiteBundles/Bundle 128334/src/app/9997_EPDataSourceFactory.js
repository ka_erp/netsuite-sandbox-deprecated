/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or 
 * otherwise make available this code.
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 May 2016     aalcabasa
 *
 * @NModuleScope TargetAccount
 */
define([
    'require',
    '../lib/wrapper/9997_NsWrapperSearch',
    '../data/9997_PFADAO',
    '../data/9997_CompanyBankDAO',
    '../data/9997_CurrencyDAO',
    '../data/9997_FreemarkerTemplateDAO',
    '../data/9997_FreeMarkerLibDAO',
    './9997_FreeMarkerFunctionBuilder',
    '../lib/wrapper/9997_NsWrapperFormat',
    '../lib/wrapper/9997_NsWrapperError'],

function(require, search, pfaDAO, companyBankDAO, currencyDAO, 
    templateDAO, freeMarkerLibDAO, freeMarkerFunctionBuilder, formatter, error) {
    
    // TODO move to constants and/or as enum
    var PATH = './';
    var EFT_DATASOURCE = PATH + '9997_EFTDataSource';
    var DD_DATASOURCE = PATH + '9997_DDDataSource';
    var PFA_RECORD = 'customrecord_2663_file_admin';
    var PFA_BANK_ACCOUNT = 'custrecord_2663_bank_account';
    var PFA_PAYMENT_TYPE = 'custrecord_2663_payment_type';
    var PFA_CREATION_TIMESTAMP = 'custrecord_2663_file_creation_timestamp';
    var PFA_EXCHANGE_RATES = 'custrecord_2663_exchange_rates';
    var NAME = 'name';
    
    var COMPANY_BANK_RECORD = 'customrecord_2663_bank_details';
    var BANK_TARGET_FOLDER = 'custrecord_2663_eft_file_cabinet_id';
    var BANK_FILE_PREFIX = 'custrecord_2663_file_name_prefix';
    var BANK_CURRENCY = 'custrecord_2663_currency';
    

    var PAYMENT_TYPE_CODES = {
        '1': 'eft',
        '2': 'dd',
        '3': 'pp'
    };
    
    var DATASOURCE_CLASSES = {
        'dd': DD_DATASOURCE,
        'eft': EFT_DATASOURCE
    };
    
    function getDataSource(pfaId) {
        
        var dsSettings = getDataSourceSettings(pfaId);
        log.debug("dsSettings", JSON.stringify(dsSettings))

        var dataSource;
        var dataSourceClass = DATASOURCE_CLASSES[dsSettings.paymentFileDetails.type];
        
        if (!dataSourceClass) {
            throw error.create({
                name: 'EP_UNSUPPORTED_PAYMENT_FILE_TYPE',
                message: 'File type ' + dsSettings.paymentFileDetails.type + ' is currently not supported by Electronic Payments.'
            });
        }
        
        require([dataSourceClass], function(dataSourceModule) {
            dataSource = dataSourceModule;
        });
        
        return dataSource.create(dsSettings);
    }
    
    function getDataSourceSettings(pfaId) {
        
        var pfa = search.lookupFields({
            type: PFA_RECORD,
            id: pfaId,
            columns: [NAME, PFA_BANK_ACCOUNT, PFA_PAYMENT_TYPE, PFA_CREATION_TIMESTAMP, PFA_EXCHANGE_RATES]
        });
        
        var paymentType = pfa[PFA_PAYMENT_TYPE][0].value;
        var companyBankId = pfa[PFA_BANK_ACCOUNT][0].value;
        var createDate = pfa[PFA_CREATION_TIMESTAMP];
        
        // Make sure that date is formatted correctly instead of tokenizing the value
        createDate = formatter.parse({
            value: createDate,
            type: formatter.Type.DATE
        });
        createDate = formatter.format({
            value: createDate,
            type: formatter.Type.DATE
        });
        

        var templateType = PAYMENT_TYPE_CODES[paymentType];
        
        var templateIdField = 'custrecord_2663_' + templateType + '_template';
        var companyBankDetails = search.lookupFields({
            type: COMPANY_BANK_RECORD,
            id: companyBankId,
            columns: [BANK_TARGET_FOLDER, BANK_FILE_PREFIX, BANK_CURRENCY, templateIdField]
        });
        
        log.debug("companyBankDetails", JSON.stringify(companyBankDetails))
        var dsSettings = {
            pfa: {
                id: pfaId,
                name: pfa[NAME],
                createDate: createDate,
                exchangeRates: pfa[PFA_EXCHANGE_RATES]
            },
            companyBank: {
                id: companyBankId,
                currency: companyBankDetails[BANK_CURRENCY][0].value
            },
            paymentFileDetails: {
                templateId: companyBankDetails[templateIdField][0].value,
                folder: companyBankDetails[BANK_TARGET_FOLDER],
                fileNamePrefix: companyBankDetails[BANK_FILE_PREFIX],
                type: templateType
            },
            pfaDAO: pfaDAO,
            companyBankDAO: companyBankDAO,
            currencyDAO: currencyDAO,
            templateDAO: templateDAO,
            freeMarkerLibDAO: freeMarkerLibDAO,
            freeMarkerFunctionBuilder: freeMarkerFunctionBuilder
        };
        
        return dsSettings;
    }
    
    return {
        getDataSource: getDataSource
    };
});
