/**
 * Copyright 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 *
 * @author aaclan
 * @NApiVersion 2.0
 * @NScriptId 9997_PaymentFileGeneration_cs
 * @NScriptType clientscript
 * @NModuleScope Public
 */

define(['../../data/9997_PaymentFileGenerationDAO', 
        '../../app/9997_PaymentSearchValidator', 
        '../../lib/wrapper/9997_NsWrapperFormat',
        '../../lib/wrapper/9997_NsWrapperError',
        '../../lib/wrapper/9997_NsWrapperMessage',
        '../../lib/wrapper/9997_NsWrapperTemplateURL',
        '../../lib/wrapper/9997_NsWrapperHTTPS'
        ],

function(fileGenerationDAO, searchValidator, format, error, message, url, https) {
    var ERROR_MSG_NO_TRANSACTIONS = 'There are no transactions retrieved using this saved search. Click the Edit Search link if you want to review the filtering criteria. You may also select another saved search or verify that transactions have been tagged for electronic payment.';
    var WARNING_MSG_TRAN_COUNT_LIMIT_EXCEEDED = 'The Electronic Payments SuiteApp currently processes up to 5000 open bills at a time. If you are processing more than 5000 open bills, only the first 5000 bills will be selected for processing.';
    var ERROR_MSG_SAME_PROCESS_RUNNING = 'Another process is queued for the same GL bank account and payment file format. Try again after the first process has been completed.';
    var INFO_MSG_PROCESSING = 'Validating payment transactions...<br/>Do not refresh this page and wait until you are redirected to the Payment File Administration record.';
    
    var TRANSACTION_COUNT_LIMIT = 5000;
    
    var SEARCH_SUMMARY_SERVICE_SCRIPT = 'customscript_9997_search_summary_service';
    var SEARCH_SUMMARY_SERVICE_DEPLOYMENT = 'customdeploy_9997_search_summary_service';
    
    function pageInit(context){
        var record = context.currentRecord;
        setDefaultSearchLinks(record);
        setDefaultProcessDate(record);
        disableSubmitButton(record, true); // preferred to do this in cs instead on load of suitelet in case there is problem enabling/disabling submit button
    }
    
    function setDefaultSearchLinks(record){
        var cachedSavedSearch = record.getValue({ fieldId: 'transavedsearch' });
        if (cachedSavedSearch) {
            updateSearchLinks(record);
        }
    }
    
    function setDefaultProcessDate(record){
        var dateToday = new Date();
        var userTimeZoneDate = format.format({ value: dateToday, type: format.Type.DATE });
        var userTimeZoneDateObj = format.parse({ value: userTimeZoneDate, type: format.Type.DATE });
        record.setValue({ fieldId: 'datetobeprocessed', value: userTimeZoneDateObj });
    }
    
    function fieldChanged(context){
        var record = context.currentRecord;
        var fieldName = context.fieldId;
        
        try{
            switch(fieldName) {
                case 'bankaccount':
                    message.hide();
                    updateBankAccount(record);
                    refreshSavedSearchSummary(record);
                    break;
                case 'transavedsearch':
                    message.hide();
                    updateSearchLinks(record);
                    refreshSavedSearchSummary(record);
                    break;
                case 'trancount':
                    checkCount(record);
                    break;
                default:
            }            
        }
        catch(e){
             var errorText = [e.name, e.message].join(' : '); 
             message.error('Error', errorText);
        }

    }
    
    function getBankAccountInfo(record){
        return JSON.parse(record.getValue({ fieldId: 'bankaccount'}));
    }
    
    function updateBankAccount(record){
        var bankAccountId = getBankAccountId(record);
        var bankAccount = getBankAccountInfo(record, bankAccountId);
        
        var subsidiary = bankAccount && bankAccount.subsidiary ? bankAccount.subsidiary : '';
        var bankCurrency = bankAccount && bankAccount.currency ? bankAccount.currency : '';
        var glBankAccount = bankAccount && bankAccount.glBankAccountTxt ? bankAccount.glBankAccountTxt : '';
        
        var subsidiaryName = subsidiary && subsidiary.name ? subsidiary.name : '' ;
        var baseCurrency = subsidiary && subsidiary.baseCurrency ? subsidiary.baseCurrency : '' ;
        
        var template = bankAccount && bankAccount.template ? bankAccount.template : '' ;
        var templateName = template && template.name ? template.name : '';
        
        record.setValue({ fieldId: 'bankcurrency', value: bankCurrency, ignoreFieldChange: true } );
        record.setValue({ fieldId: 'glbankaccount', value: glBankAccount, ignoreFieldChange: true } );
        record.setValue({ fieldId: 'subsidiary', value: subsidiaryName, ignoreFieldChange: true } );
        record.setValue({ fieldId: 'basecurrency', value: baseCurrency, ignoreFieldChange: true} );
        record.setValue({ fieldId: 'fileformat', value: templateName, ignoreFieldChange: true } );
        record.setValue({ fieldId: 'bankaccountinfo', value: JSON.stringify(bankAccount), ignoreFieldChange: true } );
        
    }
    
    function updateSearchLinks(record){
        var savedSearchId = record.getValue({ fieldId: 'transavedsearch'});
        record.setValue({  fieldId: 'editsearch', value: fileGenerationDAO.getSearchLink(savedSearchId, true), ignoreFieldChange: true } );
        record.setValue({  fieldId: 'previewsearch', value: fileGenerationDAO.getSearchLink(savedSearchId), ignoreFieldChange: true } );
    }
    
    function resetSearchSummary(record){
        record.setValue({ fieldId: 'trancount', value: ''} );
        record.setValue({ fieldId: 'totalamount', value: '', ignoreFieldChange: true });
    }
    
    function getBankAccountId(record){
        var bankAccountInfo = getBankAccountInfo(record);
        var bankAccountId = bankAccountInfo.id || '';
        return bankAccountId;
    }
    
    function refreshSavedSearchSummary(record){
        resetSearchSummary(record);
        var savedSearchId = record.getValue({ fieldId: 'transavedsearch'});
        var bankAccountId = getBankAccountId(record);
        
        if(!bankAccountId || !savedSearchId){
            return;
        }
        validateSavedSearch(record);
        updateSavedSearchSummary(record);
    }
    
    function validateSavedSearch(record){
        var savedSearchId = record.getValue({ fieldId: 'transavedsearch'});
        var tranSearch = fileGenerationDAO.getSearch(savedSearchId);

        var parameters = {};
        parameters['bankAccountInfo'] = JSON.parse(record.getValue({ fieldId: 'bankaccountinfo'}));
        parameters['templateType'] = record.getValue({ fieldId: 'templatetype'});

        searchValidator.validatePaymentSearch(tranSearch, parameters);
    }
    
    function updateSavedSearchSummary(record){
        var savedSearchId = record.getValue({ fieldId: 'transavedsearch'});
        var summary = getSearchSummary(savedSearchId);
        var count = summary.count || '';
        var totalAmount = summary.totalAmount || '';
        
        record.setValue({ fieldId: 'trancount', value: count} );
        record.setValue({ fieldId: 'totalamount', value: totalAmount, ignoreFieldChange: true });
    }
    
    function getSearchSummary(savedSearchId){
        var summary = requestSearchSummary(savedSearchId);
        return summary;
    }

    function requestSearchSummary(savedSearchId){
        var serviceUrl = url.resolveScript({
            scriptId: SEARCH_SUMMARY_SERVICE_SCRIPT,
            deploymentId: SEARCH_SUMMARY_SERVICE_DEPLOYMENT,
            params: {action: 'searchSummary', searchId: savedSearchId}
        });
        
        var response = https.get({
            url: serviceUrl
        });
        
        var result = JSON.parse(response.body);
        var summary = {};
        
        if(result.success && result.data){
            summary = result.data;
        }
        
        return summary;
        
    }

    function checkCount(record){
        var count = record.getValue({ fieldId: 'trancount'});
        
        if (count == ''){
            disableSubmitButton(record, true);
        } else if(hasNoTransactions(record)){
            disableSubmitButton(record, true);
            var err = {
                name: 'EP_NO_TRANSACTIONS',
                message: ERROR_MSG_NO_TRANSACTIONS
            };
            throw error.create(err);
            
        } else if (count > TRANSACTION_COUNT_LIMIT){
            disableSubmitButton(record, false);
            message.warn('Warning', WARNING_MSG_TRAN_COUNT_LIMIT_EXCEEDED);
        } else {
            disableSubmitButton(record, false);
        }
        
    }
    
    function disableSubmitButton(record, disable){
        try{
            var submitBtn = record.getField({ fieldId: 'submitter' }); // TODO: submit button id is not documented
            submitBtn.isDisabled = disable;
        }catch(e){
            // submit button field id may have changed
        }
    }
    
    function hasNoTransactions(record){
        var count = record.getValue({ fieldId: 'trancount'});
        
        if(count <= 0){
            return true;
        }
        return false;
    }
    
    function saveRecord(context){
        var record = context.currentRecord;
        
        var isValid = true;
        
        try{
            validateSavedSearch(record);
            updateSavedSearchSummary(record);
            
            if(hasSameProcessRunning(record)){
                var err = {
                    name: 'EP_SAME_PROCESS_RUNNING',
                    message: ERROR_MSG_SAME_PROCESS_RUNNING
                };
                throw error.create(err);
            }
        }
        catch(e){
            var errorText = [e.name, e.message].join(' : '); 
            message.error('Error', errorText);
            isValid = false;
        }
        
        if(hasNoTransactions(record)){
            isValid = false;
        }
        
        if(isValid){
            message.info('Information', INFO_MSG_PROCESSING);
        }
        
        return isValid;
    }
    
    function hasSameProcessRunning(record){
        var bankAccountInfo = JSON.parse(record.getValue({ fieldId: 'bankaccountinfo'}));
        var glBankAccount = bankAccountInfo.glBankAccount;
        var fileFormat = bankAccountInfo.template.id;

        var sameProcessCount = fileGenerationDAO.getProcessingPFACount({glBankAccount: glBankAccount , fileFormat: fileFormat  });

        if(sameProcessCount > 0){
            return true;
        }
        
        return false;
    }

    return {
        pageInit: pageInit,
        fieldChanged: fieldChanged,
        saveRecord: saveRecord,
    };
});