/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount 
 */

define(['../lib/9997_DAOFactory',
        '../lib/wrapper/9997_NsWrapperSearch',
        '../lib/wrapper/9997_NsWrapperError',
        '../app/9997_SearchListIterator'],

function (daoFactory, search, error, searchListIterator) {
    var RECORD_TYPE = 'customrecord_2663_entity_bank_details';

    var FIELD_MAP = {
        'name': 'name',
        'entityBankType': 'custrecord_2663_entity_bank_type',
        'entityFileFormat': 'custrecord_2663_entity_file_format',
        'entityAcctNo': 'custrecord_2663_entity_acct_no',
        'entityAcctName': 'custrecord_2663_entity_acct_name',
        'entityBankNo': 'custrecord_2663_entity_bank_no',
        'entityBankName': 'custrecord_2663_entity_bank_name',
        'entityBranchNo': 'custrecord_2663_entity_branch_no',
        'entityBranchName': 'custrecord_2663_entity_branch_name',
        'entityAcctSuffix': 'custrecord_2663_entity_acct_suffix',
        'acctType': 'custrecord_2663_acct_type',
        'parentVendor': 'custrecord_2663_parent_vendor',
        'parentEmployee': 'custrecord_2663_parent_employee',
        'parentCustomer': 'custrecord_2663_parent_customer',
        'entityPaymentDesc': 'custrecord_2663_entity_payment_desc',
        'edi': 'custrecord_2663_edi',
        'customerCode': 'custrecord_2663_customer_code',
        'ediValue': 'custrecord_2663_edi_value',
        'childId': 'custrecord_2663_child_id',
        'reference': 'custrecord_2663_reference',
        'babyBonus': 'custrecord_2663_baby_bonus',
        'entityIban': 'custrecord_2663_entity_iban',
        'entityCountryCode': 'custrecord_2663_entity_country_code',
        'entityIbanCheck': 'custrecord_2663_entity_iban_check',
        'entityCountryCheck': 'custrecord_2663_entity_country_check',
        'entityBankCode': 'custrecord_2663_entity_bank_code',
        'entityProcessorCode': 'custrecord_2663_entity_processor_code',
        'entitySwift': 'custrecord_2663_entity_swift',
        'entityAddress1': 'custrecord_2663_entity_address1',
        'entityAddress2': 'custrecord_2663_entity_address2',
        'entityAddress3': 'custrecord_2663_entity_address3',
        'entityCity': 'custrecord_2663_entity_city',
        'entityState': 'custrecord_2663_entity_state',
        'entityZip': 'custrecord_2663_entity_zip',
        'entityCountry': 'custrecord_2663_entity_country',
        'entityBban': 'custrecord_2663_entity_bban',
        'parentCustRef': 'custrecord_2663_parent_cust_ref',
        'entityBic': 'custrecord_2663_entity_bic',
        'entityBankAcctType': 'custrecord_2663_entity_bank_acct_type',
        'entityIssuerNum': 'custrecord_2663_entity_issuer_num',
        'parentPartner': 'custrecord_2663_parent_partner',
        'entityBillingSeqType': 'custrecord_2663_entity_billing_seq_type',
        'dateRefMandate': 'custrecord_2663_date_ref_mandate',
        'entityRefAmended': 'custrecord_2663_entity_ref_amended',
        'finalPayDate': 'custrecord_2663_final_pay_date',
        'numPayments': 'custrecord_2663_num_payments',
        'firstPayDate': 'custrecord_2663_first_pay_date',
        'entityCompanyId': 'custrecord_2663_entity_company_id'
    }; 

    var daoParams = {
        recordType: RECORD_TYPE,
        fieldMap: FIELD_MAP,
    };

    var dao = daoFactory.getDAO(daoParams);

    function retrieve(id){
        return dao.retrieve(id);
    }

    function retrieveNsRecord(id){
        return dao.retrieveNsRecord(id);
    }
    
    function retrieveEntityBanks(formatId, entityIds, entityBankColumns) {
        var entityBankFilters = [
            ['custrecord_2663_parent_customer', 'anyof', '@NONE@'],
            'and',
            ['custrecord_2663_parent_cust_ref', 'anyof', '@NONE@'],
            'and',
            ['custrecord_2663_entity_bank_type', 'anyof', '1'],
            'and',
            [
                [
                    ['custrecord_2663_parent_vendor.custentity_2663_payment_method', 'is', 'T'],
                    'and',
                    ['custrecord_2663_parent_vendor.isinactive', 'is', 'F']
                ],
                'or',
                [
                    ['custrecord_2663_parent_employee.custentity_2663_payment_method', 'is', 'T'],
                    'and',
                    ['custrecord_2663_parent_employee.isinactive', 'is', 'F']
                ]
            ]
        ];

        // check formatId if indicated
        if (formatId) {
            entityBankFilters.push('and');
            entityBankFilters.push(['custrecord_2663_entity_file_format', 'anyof', formatId]);
        }

        var entityBankData = [];
        var listIterator = searchListIterator.create(entityIds);
        while (listIterator.hasNext()) {
            var entityIdsSubset = listIterator.next();

            var entityBankDynamicFilter = entityBankFilters.concat([
                'and',
                [
                    [['custrecord_2663_parent_vendor.internalid', 'anyof', entityIdsSubset]],
                    'or',
                    [['custrecord_2663_parent_employee.internalid', 'anyof', entityIdsSubset]]
                ]
            ]);

            var entityBanksSubset = getAllSearchResults(entityBankDynamicFilter, entityBankColumns);
            entityBankData = entityBankData.concat(entityBanksSubset);
        }

        return orderByList(entityBankData, entityIds);
    }

    function retrieveCustomerRefundBanks(formatId, entityIds, entityBankColumns) {
        var customerRefundFilters = [
            ['custrecord_2663_parent_vendor', 'anyof', '@NONE@'],
            'and',
            ['custrecord_2663_parent_employee', 'anyof', '@NONE@'],
            'and',
            ['custrecord_2663_parent_customer', 'anyof', '@NONE@'],
            'and',
            ['custrecord_2663_entity_bank_type', 'anyof', '1'],
            'and',
            ['custrecord_2663_parent_cust_ref.custentity_2663_customer_refund', 'is', 'T'],
            'and',
            ['custrecord_2663_parent_cust_ref.isinactive', 'is', 'F']
        ];

        // check formatId if indicated
        if (formatId) {
            customerRefundFilters.push('and');
            customerRefundFilters.push(['custrecord_2663_entity_file_format', 'anyof', formatId]);
        }

        // if entity id list is given
        if (entityIds && entityIds.length > 0) {
            log.debug('EntityBankDAO.retrieveCustomerRefundBanks', 'Entity Ids: ' + entityIds.length);
            customerRefundFilters.push('and');
            customerRefundFilters.push(['custrecord_2663_parent_cust_ref.internalid', 'anyof', entityIds]);
        }

        var entityBankData = getAllSearchResults(customerRefundFilters, entityBankColumns);
        return orderByList(entityBankData, entityIds);
    }

    function retrieveCustomerBanks(formatId, entityIds, entityBankColumns) {
        var customerBankFilters = [
            ['custrecord_2663_parent_vendor', 'anyof', '@NONE@'],
            'and',
            ['custrecord_2663_parent_employee', 'anyof', '@NONE@'],
            'and',
            ['custrecord_2663_parent_cust_ref', 'anyof', '@NONE@'],
            'and',
            ['custrecord_2663_entity_bank_type', 'anyof', '1'],
            'and',
            ['custrecord_2663_parent_customer.custentity_2663_direct_debit', 'is', 'T'],
            'and',
            ['custrecord_2663_parent_customer.isinactive', 'is', 'F'],
        ];

        // check formatId if indicated
        if (formatId) {
            customerBankFilters.push('and');
            customerBankFilters.push(['custrecord_2663_entity_file_format', 'anyof', formatId]);
        }

        // if entity id list is given
        if (entityIds && entityIds.length > 0) {
            log.debug('EntityBankDAO.retrieveCustomerBanks', 'Entity Ids: ' + entityIds.length);
            customerBankFilters.push('and');
            customerBankFilters.push(['custrecord_2663_parent_customer.internalid', 'anyof', entityIds]);
        }

        var entityBankData = getAllSearchResults(customerBankFilters, entityBankColumns);
        return orderByList(entityBankData, entityIds);
    }

    function orderByList(entityData, entityIds) {
        var entityBankDataMap = {};
        var orderedEntityBankData = [];
        var missingEntityIds = [];

        for (var i = 0; entityData && i < entityData.length; i++) {
            var entityBank = entityData[i];
            var entityId = getEntityId(entityBank);
            entityBankDataMap[entityId] = entityBank;
        }

        for (var i = 0; entityIds && i < entityIds.length; i++) {
            var entityId = entityIds[i];
            
            var entityBank = entityBankDataMap[entityId];

            if (entityBank) {
                orderedEntityBankData.push(entityBank);
            } else {
                missingEntityIds.push(entityId);
            }
        }
        
        if (missingEntityIds.length > 0) {
            var code = 'EP_ENTITY_BANK_NOT_FOUND';
            var message = 'Error in retrieving bank details. Verify that bank accounts have been assigned to the following entities ' + missingEntityIds.join(',') + '.';
            throw error.logAndCreateError(code, message);
        }
        
        return orderedEntityBankData;
    }
    
    function getEntityId(entityBank) {
        var entityId = entityBank.getValue(FIELD_MAP.parentVendor) ||
                       entityBank.getValue(FIELD_MAP.parentEmployee) ||
                       entityBank.getValue(FIELD_MAP.parentCustomer) ||
                       entityBank.getValue(FIELD_MAP.parentCustRef);

        return entityId;
    }

    function getAllSearchResults(filters, columns) {
        var genericSearch = search.create({
            type: RECORD_TYPE,
            columns: columns,
            filters: filters
        });

        var data = [];
        var iterator = genericSearch.getIterator();
        while (iterator.hasNext()) {
            var result = iterator.next();
            data.push(result);
        }

        return data;
    }

    return {
        retrieve: retrieve,
        retrieveNsRecord: retrieveNsRecord,
        retrieveEntityBanks: retrieveEntityBanks,
        retrieveCustomerRefundBanks: retrieveCustomerRefundBanks,
        retrieveCustomerBanks: retrieveCustomerBanks,
    };
});
