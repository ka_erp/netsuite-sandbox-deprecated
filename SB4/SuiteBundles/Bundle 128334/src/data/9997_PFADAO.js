/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * 
 * @NApiVersion 2.x
 * @NModuleScope TargetAccount 
 */

define(['../lib/wrapper/9997_NsWrapperSearch', '../lib/wrapper/9997_NsWrapperRecord', '../lib/9997_DAOFactory'],

function(search, record, daoFactory) {
    var RECORD_TYPE = 'customrecord_2663_file_admin';
    var PAYPROCESSED = 4;// TODO move to constants and in an enum
    
    var FIELD_MAP = {
        'name': 'name',
        'paymentType': 'custrecord_2663_payment_type',
        'lastProcess': 'custrecord_2663_last_process',
        'fileProcessed': 'custrecord_2663_file_processed',
        'refNote': 'custrecord_2663_ref_note',
        'bankAccount': 'custrecord_2663_bank_account',
        'paymentsForProcessId': 'custrecord_2663_payments_for_process_id',
        'paymentsForProcessAmt': 'custrecord_2663_payments_for_process_amt',
        'processDate': 'custrecord_2663_process_date',
        'postingPeriod': 'custrecord_2663_posting_period',
        'aggMethod': 'custrecord_2663_agg_method',
        'fileRef': 'custrecord_2663_file_ref',
        'account': 'custrecord_2663_account',
        'paymentsForNotify': 'custrecord_2663_payments_for_notify',
        'paymentsForReversal': 'custrecord_2663_payments_for_reversal',
        'department': 'custrecord_2663_department',
        'class': 'custrecord_2663_class',
        'location': 'custrecord_2663_location',
        'fileCreationDate': 'custrecord_2663_file_creation_date',
        'reprocessFlag': 'custrecord_2663_reprocess_flag',
        'processingErrors': 'custrecord_2663_processing_errors',
        'paymentSubsidiary': 'custrecord_2663_payment_subsidiary',
        'fileCreationTimestamp': 'custrecord_2663_file_creation_timestamp',
        'sequenceId': 'custrecord_2663_sequence_id',
        'lastCheckNo': 'custrecord_2663_last_check_no',
        'priority': 'custrecord_ep_priority',
        'paymentCreationQueue': 'custrecord_ep_payment_creation_queue',
        'batchid': 'custrecord_2663_batchid',
        'exchangeRates': 'custrecord_2663_exchange_rates',
        'reversalDate': 'custrecord_2663_reversal_date',
        'reversalPeriod': 'custrecord_2663_reversal_period',
        'reversalReasons': 'custrecord_2663_reversal_reasons',
        'notificationMails': 'custrecord_2663_notification_mails',
        'notificationSender': 'custrecord_2663_notification_sender',
        'parentDeployment': 'custrecord_2663_parent_deployment',
        'directDebitType': 'custrecord_2663_direct_debit_type',
        'updatedEntities': 'custrecord_2663_updated_entities',
        'batchDetail': 'custrecord_2663_batch_detail',
        'journalKeys': 'custrecord_2663_journal_keys',
        'totalAmount': 'custrecord_2663_total_amount',
        'totalTransactions': 'custrecord_2663_total_transactions',
        'status': 'custrecord_2663_status',
        'number': 'custrecord_2663_number',
        'timeStamp': 'custrecord_2663_time_stamp',
        'removedKeys': 'custrecord_2663_removed_keys',
        'approvalLevel': 'custrecord_2663_approval_level',
        'prevApprovalLevel': 'custrecord_2663_prev_approval_level',
        'aggregate': 'custrecord_2663_aggregate',
        'fromBatch': 'custrecord_2663_from_batch',
        'notificationSubject': 'custrecord_2663_notification_subject',
        'emailReceivers': 'custrecord_2663_email_receivers',
        'ccReceivers': 'custrecord_2663_cc_receivers',
        'bccReceivers': 'custrecord_2663_bcc_receivers',
        'partiallyPaidKeys': 'custrecord_2663_partially_paid_keys',
        'transactionEntities': 'custrecord_2663_transaction_entities',
        'dueBatchDetails': 'custrecord_2663_due_batch_details',
        'updatedCount': 'custrecord_2663_updated_count',
        'paymentsForProcessDis': 'custrecord_2663_payments_for_process_dis',
        'appliedCredits': 'custrecord_2663_applied_credits',
        'appliedCreditsAmt': 'custrecord_2663_applied_credits_amt',
        'batchWithCredit': 'custrecord_2663_batch_with_credit',
        'batchMapEntityAmt': 'custrecord_2663_batch_map_entity_amt'
    };
    
    var daoParams = {
        recordType: RECORD_TYPE,
        fieldMap: FIELD_MAP,
    };
    
    var dao = daoFactory.getDAO(daoParams);
    
    function retrieve(id) {
        return dao.retrieve(id);
    }
    
    function retrieveNsRecord(id) {
        return dao.retrieveNsRecord(id);
    }
    
    function getFields(pfaId, pfaFieldList) {
        return search.lookupFields({
            type: RECORD_TYPE,
            id: pfaId,
            columns: pfaFieldList
        });
    }
    
    function updateFields(pfaId, keyValueMapping) {
        record.submitFields({
            type: RECORD_TYPE,
            id: pfaId,
            values: keyValueMapping,
        });
    }
    
    function getSequences(options) {
        var companyBankId = options.companyBank.id;
        var templateId = options.paymentFileDetails.templateId;
        
        var filters = [
            ['custrecord_2663_file_processed', 'is', PAYPROCESSED],
            'AND',
            ['custrecord_2663_bank_account', 'is', companyBankId],
            'AND',
            ['custrecord_2663_payment_type', 'is', templateId]];
        
        if (options.sameDaySequenceOnly) {
            filters.push('AND');
            filters.push(['custrecord_2663_file_creation_timestamp', 'on', options.pfa.createDate]);
        }
        
        var columns = ['custrecord_2663_sequence_id', search.createColumn({
            name: 'custrecord_2663_file_creation_timestamp',
            sort: search.Sort.ASC
        })];
        
        var sequenceSearch = search.create({
            type: 'customrecord_2663_file_admin',
            columns: columns,
            filters: filters
        });
        
        var sequences = [];
        var rs = sequenceSearch.getIterator();
        while (rs.hasNext()) {
            sequences.push(rs.next());
        }
        return sequences;
    }
    
    return {
        retrieve: retrieve,
        retrieveNsRecord: retrieveNsRecord,
        getFields: getFields,
        updateFields: updateFields,
        getSequences: getSequences
    };
});
