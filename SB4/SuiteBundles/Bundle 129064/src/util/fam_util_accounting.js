/**
 * © 2015 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 *  
 * @NScriptName FAM Accounting Utility
 * @NScriptId _fam_util_accounting
 * @NApiVersion 2.x
*/

define(["../adapter/fam_adapter_search",
        "../adapter/fam_adapter_record",
        "../adapter/fam_adapter_format",
        "../adapter/fam_adapter_config",
        "../adapter/fam_adapter_runtime",
        "./fam_util_systemsetup"
], 

function (search, record, formatter, config, runtime, fSystemSetup){
    var periodCache = {};
    var periodError = {};
    
    return {
        getPeriodCache : function (){
            return periodCache;
        },        
        setPeriodCache : function(key, value, isRefresh){
            if (isRefresh){
                periodCache = {};
                return;
            }
            periodCache[key] = value;
        },        
        getPeriodError : function (){
            return periodError;
        },        
        setPeriodError : function(key, value, isRefresh){
            if (isRefresh){
                periodError = {};
                return;
            }
            periodError[key] = value;
        },
        
        
        getPrimaryBookId : function () {
            var searchRes;

            if (!runtime.isFeatureInEffect({feature :"MULTIBOOK"})) {
                return null;
            }
            
            var searchObj = search.create({
                type : record.Type.ACCOUNTING_BOOK,
                filters : [['isprimary', 'is', 'T']]
            });
            var searchResultSet = searchObj.run();
            var res = searchResultSet.getRange(0,1);
            if (res && res.length > 0) {
                return res[0].id;
            }
            return null;
        },
        
        getAccountingPeriodInfo : function () {
            var i, recList = null;
            var filters = [['isQuarter', 'is', 'F'],
                           'and',
                           ['isYear', 'is', 'F']];
            var columns = [search.createColumn({ name: 'startDate', sort: search.Sort.ASC }),
                           search.createColumn({ name: 'endDate' }),
                           search.createColumn({ name: 'periodname'}),
                           search.createColumn({ name: 'closed'}),
                           search.createColumn({ name: 'aplocked'}),
                           search.createColumn({ name: 'arlocked'}),
                           search.createColumn({ name: 'alllocked'}),
                           search.createColumn({ name: 'isAdjust'})];
            var searchObj = search.create({
                type : 'accountingperiod',
                filters : filters,
                columns : columns
            });

            var searchResultSet = searchObj.run();
            var searchLimit = 1000;
            var lowerLimit = 0;
            var upperLimit = searchLimit;
            var searchRes = [];
            
            while(true){
                var res = searchResultSet.getRange(lowerLimit,upperLimit);
                if (0 === res.length){
                    break;
                }
                lowerLimit = upperLimit;
                upperLimit += searchLimit;
                searchRes = searchRes.concat(res);
            }

            if (searchRes && searchRes.length > 0){
                recList = [];
                for (i = 0; i < searchRes.length; i++) {
                    var rec = searchRes[i];
                    recList.push({
                        internalId : rec.id,
                        startDate  : rec.getValue('startDate'),
                        endDate    : rec.getValue('endDate'),
                        periodName : rec.getValue('periodname'),
                        allClosed  : rec.getValue('closed'),
                        apLocked   : rec.getValue('aplocked' ),
                        arLocked   : rec.getValue('arlocked' ),
                        allLocked  : rec.getValue('alllocked'),
                        isAdjust   : rec.getValue('isAdjust')
                    });
                }
            }
            return recList;
        },
        
        getOpenPeriod : function (date) {
            var i = 0
            var ret;
            var msg;
            var checkAdjust = true;
            
            var periodInfo = this.getAccountingPeriodInfo();
            
            var i = 0, ret, msg, checkAdjust = true;
            
            if (periodCache[date.getTime()]) {
                ret = periodCache[date.getTime()];
            }
            else if (periodError[date.getTime()]) {
                throw periodError[date.getTime()];
            }
            else {
                ret = new Date(date.getTime());
                
                do {
                    var periodInfoItem = periodInfo[i];
                    var startDate = formatter.parse(
                            {value: periodInfoItem.startDate, type: formatter.Type.DATE});
                    var endDate = formatter.parse(
                            {value: periodInfoItem.endDate, type: formatter.Type.DATE});
                    var allClosed = periodInfoItem.allClosed;
                    var allLocked = periodInfoItem.allLocked;
                    var apLocked = periodInfoItem.apLocked;
                    var arLocked = periodInfoItem.arLocked;
                    var isAdjust = periodInfoItem.isAdjust;
                    
                    if (ret.getTime() < startDate.getTime()) {
                        var dateStr = formatter.format({value: ret, type: formatter.Type.DATE});
                        msg = "No accounting period found for " + dateStr;
                        periodError[date.getTime()] = msg;
                        throw msg;
                    }
                    
                    if (ret.getTime() > endDate.getTime()) {
                        i++;
                    }
                    else if (allClosed || allLocked ||
                        (fSystemSetup.getSetting("isCheckApLock") && apLocked) ||
                        (fSystemSetup.getSetting("isCheckArLock") && arLocked) ||
                        (!checkAdjust && isAdjust)) {

                        ret = new Date(endDate.getFullYear(),
                            endDate.getMonth(),
                            endDate.getDate() + 1);
                    }
                    else if (checkAdjust && isAdjust) {
                        checkAdjust = false;
                        ret = new Date(startDate.getFullYear(),
                            startDate.getMonth(),
                            startDate.getDate() - 1);
                        i = i > 0 ? i - 1 : 0;
                    }
                    else {
                        if (ret.getTime() !== date.getTime()) {
                            ret = endDate;
                        }
                        periodCache[date.getTime()] = ret;

                    }
                } while (i < periodInfo.length && !periodCache[date.getTime()]);
                
                if (!periodCache[date.getTime()]) {
                    var dateStr = formatter.format({value: date, type: formatter.Type.DATE});
                    msg = "No open future accounting period for " + dateStr;
                    periodError[date.getTime()] = msg;
                    throw msg;
                }
            }
            
            return ret;
        },
        
        getApplicableCurrency : function (subId, bookId) {
            var subRec, subLineNum;
            bookId = bookId && bookId + '';
            
            if (!runtime.isFeatureInEffect({feature :"MULTICURRENCY"})) {
                return null;
            }
            
            if (runtime.isFeatureInEffect({feature :"MULTIBOOK"}) && bookId && 
                bookId != this.getPrimaryBookId() && runtime.isFeatureInEffect({feature :"SUBSIDIARIES"})) {
                    if (subId) {
                        subRec = record.load({
                            type:'subsidiary', 
                            id:subId
                        });
                        subLineNum = subRec.findSublistLineWithValue(
                                'accountingbookdetail', 'accountingbook', bookId);
                        return -1 != subLineNum ? +subRec.getSublistValue('accountingbookdetail',
                            'currency', subLineNum) : null;
                    }
                    else {
                        return null;
                    }
            }
            else if (runtime.isFeatureInEffect({feature :"SUBSIDIARIES"})) {
                var currObj = subId ? search.lookupFields({type: 'subsidiary', id: subId, columns: 'currency'}) : null;
                return currObj ? +currObj.currency[0].value : null;
            }
            else {
              subRec = config.load('companyinformation');
              return +subRec.getValue('basecurrency');
            }
        },
        
        getCurrencySymbol : function (subId, bookId){
            var currId = this.getApplicableCurrency(subId, bookId),
                currSym = {};
        
            if (runtime.isFeatureInEffect({feature :"MULTICURRENCY"}) && currId) {
                currSym = search.lookupFields(
                    { type: 'currency', id: currId, columns: 'symbol' }) || '';
            }
            
            return currSym.symbol || '';
        }
    }
});