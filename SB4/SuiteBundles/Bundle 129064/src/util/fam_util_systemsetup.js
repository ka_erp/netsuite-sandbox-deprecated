/**  
 * © 2016 NetSuite Inc.  
 * User may not copy, modify, distribute, or re-bundle or otherwise make available module code.  
 *    
 * @NScriptName FAM System Setup  
 * @NScriptId _fam_util_systemsetup  
 * @NAPIVersion 2.x
 */  
  
define([
    '../adapter/fam_adapter_search',
    '../adapter/fam_adapter_runtime'
], 
 
function (search, runtime){ 
    var recordId = null; 
    var crtName = 'customrecord_ncfar_systemsetup'; 
    var values = {}; 
    var multiSelectFields = ['selectedUserRoles']; 
    var fields = { 
        disposalForm              : 'custrecord_far_disposalform', 
        reportFolder              : 'custrecord_far_reportfolder', 
        isPostLocation            : 'custrecord_postlocation', 
        isPostDepartment          : 'custrecord_postdepartment', 
        isPostClass               : 'custrecord_postclass', 
        isAutoPropose             : 'custrecord_autopropose', 
        isAutoCreate              : 'custrecord_autocreate', 
        isRestrictRejectProposal  : 'custrecord_restrictrejectassetprop', 
        isConstrainAssetLifetime  : 'custrecord_constrainal', 
        nonDecimalCurrencySymbols : 'custrecord_intcurrsymbols', 
        isRunCsvImport            : 'custrecord_run_script_on_csv_import', 
        isSummarizeJe             : 'custrecord_fam_summarisejournalsby', 
        isAllowFutureDepreciate   : 'custrecord_allowfuturedepr', 
        isAllowValueEdit          : 'custrecord_allowvalueedit', 
        isAllowNegativeCost       : 'custrecord_allownegativecost', 
        selectedUserRoles         : 'custrecord_userrole', 
        isWriteDown               : 'custrecord_wdusedepraccount', 
        isCheckApLock             : 'custrecord_farsetupcheckaplock', 
        isCheckArLock             : 'custrecord_farsetupcheckarlock', 
        isPropApprovedOnly        : 'custrecord_propapprovedonly', 
        queueLimit                : 'custrecord_bgqueuelimit', 
        queuePollingInterval      : 'custrecord_queuepollinginterval', 
        allowBypassUE             : 'custrecord_allowbypass_ue', 
        isFollowAcctngPeriod      : 'custrecord_followacctperiod', 
        reportsStorageDays        : 'custrecord_daysstorereports', 
        allowAdminAllReports      : 'custrecord_adminallreports', 
        allowCustomTransaction    : 'custrecord_allowcustomtransaction', 
        reportFileSizeLimit       : 'custrecord_reportfilesizelimit' 
    }; 
 
    var searchValues = function () { 
        var columns = []; 
        for (j in fields) { 
            if (fields[j]) { 
                columns.push(fields[j]); 
            } 
        } 
 
        var settingsSearch = search.create({ 
            type : crtName, 
            columns : columns 
        }); 
         
        results = settingsSearch.run().getRange({start : 0, end : 1});
        settingsRec = results[0]; 
         
        if (settingsRec) { 
            recordId = settingsRec.id; 
 
            for (i in fields) { 
                values[i] = settingsRec.getValue(fields[i]); 
 
                if (multiSelectFields.indexOf(i) !== -1) { 
                    values[i] = values[i].split(','); 
                } 
            } 
             
            setDefaults(); 
        } 
        else {
            recordId = 0; 
        } 
    }; 
     
    var setDefaults = function () { 
        var defaults = { 
            reportsStorageDays  : 30, 
            reportFileSizeLimit : 5 
        }; 
         
        for (var i in defaults) { 
            if (!values[i]) { 
                values[i] = defaults[i];  
            } 
        } 
        // multiply value to 1,000,000 for MB 
        values['reportFileSizeLimit'] *= 1000000; 
 
        //override FAM Settings if Custom Transactions is disabled 
        if (runtime.isFeatureInEffect({feature :'MULTIBOOK'}) || 
            !runtime.isFeatureInEffect({feature :'CUSTOMTRANSACTIONS'})) { 
            values['allowCustomTransaction'] = false ; 
        } 
    };
      
    return {
        // Forces the next call of getSetting to call searchValues again, thus, 'clears' the 'cache'
        clearCache: function () {
            recordId = null;
            return recordId;
        },
        
        getSetting: function (fld) { 
            if (recordId === null) { 
                searchValues(); 
            } 
            if (recordId === 0) { 
                return null; 
            } 
 
            return values[fld]; 
        }  
    } 
}); 
