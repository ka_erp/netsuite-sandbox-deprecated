/**
 * © 2016 NetSuite Inc.
 * User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
*/

define(['../const/fam_const_customlist'], function (fList) {
    function countMonths(start, end) {
        var additional = 0;            
        
        // To prevent negative value
        if (end.getTime() < start.getTime()){
            return 0;
        }
        if (end.getDate() > start.getDate() - 1) {
            additional = 1; 
        }
        return 12 * (end.getFullYear() - start.getFullYear()) + 
                end.getMonth() - start.getMonth() + additional;
    }
    
    /**
     * Computes end date based on start date and lifetime
     * @param {Date} start - start date
     * @param {number} life - lifetime of the record
     * @param {number} period - depreciation period to use
     * @returns {Date}
    */
    function computeEndDate (start, life, period) {
        var ret;
        
        if (life <= 0) { return start; }
        
        if (period == fList.DeprPeriod.Annually) {
            ret = new Date(start.getFullYear() + life, start.getMonth(), start.getDate() - 1);
        }
        else {
            ret = new Date(start.getFullYear(), start.getMonth() + life, start.getDate() - 1);
        }
        
        return ret;
    }
    
    return {
        computeEndDate : computeEndDate,
        countMonths : countMonths
    };
});
