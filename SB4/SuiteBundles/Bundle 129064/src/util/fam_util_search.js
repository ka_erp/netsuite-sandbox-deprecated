/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 * @NModuleScope Public
 */

define(['../adapter/fam_adapter_search',
        ],
    searchUtil);

function searchUtil(search) {
    return {
        /**
         * Search record using runPaged + fetch
         * @param {String} recordType - record type id
         * @param {String} savedSearchId - saved search id
         * @param {Array} filters - array of filter objects
         * @param {Array} columns - array of columns objects
         * 
         * @returns search result
         * 
         */
        searchRecord : function (recordType, savedSearchId, filters, columns){
            var searchRes    = [],
                searchParams = { type : recordType };
            
            if (savedSearchId){
                searchParams.id = savedSearchId;
            }
            if (filters && filters.length > 0){
                searchParams.filters = filters;
            }
            if (columns && columns.length > 0){
                searchParams.columns = columns;
            }
            
            var searchObj = search.create(searchParams);
            var pagedData = searchObj.runPaged({pageSize : 1000});
            
            pagedData.pageRanges.forEach(function (pageRange) {
                var page = pagedData.fetch({ index : pageRange.index });
                searchRes = searchRes.concat(page.data);
            });
            
            return searchRes;
        }
    }
    
}
