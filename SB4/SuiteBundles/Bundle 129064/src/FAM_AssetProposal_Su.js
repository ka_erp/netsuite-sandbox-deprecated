/**
 * � 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var FAM;
if (!FAM) { FAM = {}; }

FAM.AssetProposal_Su = new function () {
    this.reqParams = {};
    this.defPropPerPage = 50;

    this.run = function (request, response) {
        var bgpMsg = '';
        this.getParameters(request);

        if (request.getMethod() === 'POST') {
            if (this.reqParams.buttonPressed === 'Propose' ||
                this.reqParams.buttonPressed === 'Generate') {
                
                if (this.reqParams.buttonPressed === 'Propose') {
                    this.proposeAssets();
                }
                else if (this.reqParams.buttonPressed === 'Generate') {
                    bgpMsg = this.generateAssets(request);
                }
                if(!bgpMsg){
                    nlapiSetRedirectURL('suitelet', 
                                        'customscript_fam_processstatus_su',
                                        'customdeploy_fam_processstatus_su');
                }
                
            }
            else {
                if (this.reqParams.buttonPressed === 'Reject') {
                    this.rejectProposals(request);
                }
                
                // redirect to same page with same parameters
                nlapiSetRedirectURL(
                    'SUITELET',
                    FAM.Context.getScriptId(),
                    FAM.Context.getDeploymentId(),
                    null,
                    {
                        // need to repass screen parameters
                        custpage_filtermsassettype : [].concat(request.getParameterValues(
                            'custpage_filtermsassettype') || []),
                        custpage_filtermssubsid : [].concat(request.getParameterValues(
                            'custpage_filtermssubsid') || [])
                    }
                );
            }
        }

        response.writePage(this.createForm(bgpMsg));
    };

    /**
     * Retrieves parameters from the request object
     *
     * Parameters:
     *     request {nlobjRequest} - request object from suitelet function
     * Returns:
     *     void
    **/
    this.getParameters = function (request) {
        this.reqParams.buttonPressed = request.getParameter('custpage_ncactionid');
        this.reqParams.assetTypes = [].concat(request.getParameterValues(
            'custpage_filtermsassettype') || []);

        this.reqParams.subsidiaries = [];
        if (FAM.Context.blnOneWorld) {
            this.reqParams.subsidiaries = [].concat(request.getParameterValues(
                'custpage_filtermssubsid') || []);
            this.reqParams.includeChild = request.getParameter('custpage_filtercbincchild');

            if (this.reqParams.subsidiaries.length && this.reqParams.includeChild === 'T') {
                this.reqParams.subsidiaries = FAM.includeSubsidiaryChildren(
                    this.reqParams.subsidiaries);
            }
        }

        this.reqParams.pageNum = +request.getParameter('custpage_proppage') || 1;
        this.reqParams.propPerPage = +request.getParameter('custpage_rowperpage') *
            this.defPropPerPage || this.defPropPerPage;
    };

    /**
     * Searches and proposes new assets and triggering a schedule script if limit is reached
     *
     * Parameters:
     *     none
     * Returns:
     *     void
    **/
    this.proposeAssets = function () {
        var procInsRec = new FAM.BGProcess();

        procInsRec.createRecord({
            status : FAM.BGProcessStatus.InProgress,
            activity_type : FAM.BGPActivityType.Direct,
            func_name : 'ncFAR_NewPropBG',
            proc_name : 'Asset Proposal',
            user : FAM.Context.userId,
            rec_count : 0,
            message : '',
            state_defn : 'ATarray,ATypes,Subsids,AutoGen',
            state : this.reqParams.assetTypes.join(':') + ',' +
                this.reqParams.assetTypes.join(':') + ',' + this.reqParams.subsidiaries.join(':') +
                ',F'
        });

        procInsRec.submitRecord();
        this.scheduleScript('ncFAR_NewPropBG');
    };

    /**
     * Searches for new Asset Proposals
     *
     * Parameters:
     *     blnCount {boolean} - flag to determine if results should be counted or not
     *     start {number} - The index number of the first result to return, inclusive
     *     end {number} - The index number of the last result to return, exclusive
     * Returns:
     *     {FAM.Search} - search results
    **/
    this.searchNewProposals = function (blnCount, start, end) {
        var fSearch = new FAM.Search(new FAM.AssetProposal_Record());

        fSearch.addFilter('status', null, 'is', FAM.ProposalStatus.New);

        if (this.reqParams.assetTypes.length > 0) {
            fSearch.addFilter('asset_type', null, 'anyof', this.reqParams.assetTypes);
        }
        if (this.reqParams.subsidiaries.length > 0) {
            fSearch.addFilter('subsidiary', null, 'anyof', this.reqParams.subsidiaries);
        }

        if (blnCount) {
            fSearch.addColumn('internalid', null, 'count');
        }
        else {
            fSearch.addColumn('subsidiary', null, null, 'SORT_ASC');
            fSearch.addColumn('asset_type', null, null, 'SORT_ASC');
            fSearch.addColumn('transaction');
            fSearch.addColumn('description');
            fSearch.addColumn('quantity');
            fSearch.addColumn('depr_method');
            fSearch.addColumn('initial_cost');
            fSearch.addColumn('rv');
            fSearch.addColumn('lifetime');
            fSearch.addColumn('status');
        }

        fSearch.run(start, end);
        return fSearch;
    };

    /**
     * Triggers Old BGP Scheduled Script
     *
     * Parameters:
     *     funcName {string} - function to process
     * Returns:
     *     void
    **/
    this.scheduleScript = function (funcName) {
        var bgpURL, bgpResponse;

        bgpURL = nlapiResolveURL('SUITELET', 'customscript_ncfar_bgp_starter_sl',
            'customdeploy_ncfar_bgp_starter_deploy', true);
        bgpResponse = nlapiRequestURL(bgpURL, null, { custscript_bgp_processfunction : funcName });

        if (bgpResponse.getBody() == null) {
            nlapiLogExecution('error', 'FAM.AssetProposal_Su.scheduleScript',
                'Unexpected error while scheduling script');
        }
        else {
            nlapiLogExecution('debug', 'FAM.AssetProposal_Su.scheduleScript',
                'Scheduled Sript triggered.');
        }
    };

    /**
     * Generates assets for each marked proposal
     *
     * Parameters:
     *     request {nlobjRequest} - request object from suitelet function
     * Returns:
     *     void
    **/
    this.generateAssets = function (request) {
        var i, propId, proposals = [], propRec = new FAM.AssetProposal_Record(),
            procInsRec = new FAM.BGProcess(), bgpMsg = '';

        for (i = 1; i <= request.getLineItemCount('proposals'); i++) {
            if (request.getLineItemValue('proposals', 'marked', i) === 'T') {
                propId = request.getLineItemValue('proposals', 'id', i);
                if(request.getLineItemValue('proposals', 'custrecord_propstatus', i)==
                    FAM.ProposalStatus.New){
                    proposals.push(propId);
                }
                else {
                    nlapiLogExecution('debug', 'FAM.AssetProposal_Su.generateAssets',
                        'AssetProposal: ' + propId + ' is not processed. Only proposals with ' +
                        'status:NEW is valid for generation.');
                }
            }
        }

        if (proposals.length === 0) {
            throw FAM.resourceManager.GetString('custpage_nopropselected', 'assetproposal');
        }
        
        try{
            procInsRec.createRecord({
                status : FAM.BGProcessStatus.Queued,
                activity_type : FAM.BGPActivityType.Direct,
                func_name : 'ncFAR_GenerateAssetsFromPropList',
                proc_name : 'Asset Generation',
                user : FAM.Context.userId,
                rec_count : 0,
                message : '',
                state_defn : 'PropList',
                state : proposals.join(':')
            });
            procInsRec.submitRecord();
        }
        catch(ex){
            var errObj = JSON.parse(ex.message);
            nlapiLogExecution('ERROR',errObj.name,errObj.message);
            bgpMsg = errObj.message;
        }
        
        if(bgpMsg){
            return bgpMsg;
        }
        
        for (i = 0; i <proposals.length; i++) {
            nlapiSubmitField('customrecord_ncfar_assetproposal',
                             proposals[i],
                             'custrecord_propstatus',
                             FAM.ProposalStatus.Pending);
        }
            
        this.scheduleScript('ncFAR_GenerateAssetsFromPropList');
        
    };

    /**
     * Rejects each marked asset proposal
     *
     * Parameters:
     *     request {nlobjRequest} - request object from suitelet function
     * Returns:
     *     void
    **/
    this.rejectProposals = function (request) {
        var i, propRec = new FAM.AssetProposal_Record();

        for(i = 1; i <= request.getLineItemCount('proposals'); i++) {
            if (request.getLineItemValue('proposals', 'marked', i) === 'T') {
                propRec.recordId = request.getLineItemValue('proposals', 'id', i);
                propRec.submitField({ status : FAM.ProposalStatus.Rejected });
            }
        }
    };

    /**
     * Creates the form to be displayed on the UI
     *
     * Parameters:
     *     none
     * Returns:
     *     {nlobjForm} - form object
    **/
    this.createForm = function (bgpmsg) {
        var i, j, searchProps, currentPageStart = 1, propCount, pageId, pageLabel, fldAction,
            fldAssetType, fldSubsidiary, fldIncludeChild, fldBGPMsg,
            fldPage, fldPropPerPage, subListProp,
            tabProposal = 'custpage_resulttab',
            screenName = 'assetproposal',
            form = nlapiCreateForm(FAM.resourceManager.GetString('custrecord_assetproposal_title',
                screenName));

        form.setScript('customscript_fam_proposal_su_cs');

        fldAction = form.addField('custpage_ncactionid', 'text', 'Action Id');
        fldAction.setDisplayType('hidden');
        fldAction.setDefaultValue('');

        fldAssetType = form.addField('custpage_filtermsassettype', 'multiselect',
            FAM.resourceManager.GetString('custpage_filtermsassettype', screenName),
            (new FAM.AssetType_Record()).getRecordTypeId());
        fldAssetType.setHelpText(FAM.resourceManager.GetString('assetrecord_assettypehelp'));
        if (this.reqParams.assetTypes.length > 0) {
            fldAssetType.setDefaultValue(this.reqParams.assetTypes);
        }

        if (FAM.Context.blnOneWorld) {
            fldSubsidiary = form.addField('custpage_filtermssubsid', 'multiselect',
                FAM.resourceManager.GetString('custpage_filtermssubsid', screenName), 'subsidiary');
            fldSubsidiary.setMandatory(true);
            fldSubsidiary.setHelpText(FAM.resourceManager.GetString('assetrecord_subshelp'));
            if (this.reqParams.subsidiaries.length > 0) {
                fldSubsidiary.setDefaultValue(this.reqParams.subsidiaries);
            }

            fldIncludeChild = form.addField('custpage_filtercbincchild', 'checkbox',
                FAM.resourceManager.GetString('custpage_filtercbincchild', screenName));
            fldIncludeChild.setDefaultValue(this.reqParams.includeChild);
            fldIncludeChild.setHelpText(FAM.resourceManager.GetString(
                'assetrecord_includechildhelp'));
        }
        
        fldBGPMsg = form.addField('custpage_bgpmsg', 'text', 'bgpmsg').setDisplayType('hidden').setDefaultValue(bgpmsg);
        

        form.addSubTab(tabProposal, FAM.resourceManager.GetString('proposals', screenName));

        searchProps = this.searchNewProposals(true);
        propCount = +searchProps.getValue(0, 'internalid', null, 'count');
        fldPage = form.addField('custpage_proppage', 'select',
            FAM.resourceManager.GetString('custpage_selectpage', screenName), null, tabProposal);
        if (propCount === 0) {
            fldPage.addSelectOption(1, '1 to 1 of 1', true);
        }
        else {
            while (currentPageStart <= propCount) {
                pageId = Math.ceil(currentPageStart / this.reqParams.propPerPage);
                pageLabel = currentPageStart + ' to ';
                if (currentPageStart + this.reqParams.propPerPage - 1 < propCount) {
                    pageLabel += (currentPageStart + this.reqParams.propPerPage - 1);
                }
                else {
                    pageLabel += propCount;
                }
                pageLabel += ' of ' + propCount;
                fldPage.addSelectOption(pageId, pageLabel, (this.reqParams.pageNum == pageId));
                currentPageStart += this.reqParams.propPerPage;
            }
        }

        fldPropPerPage = form.addField('custpage_rowperpage', 'select',
            FAM.resourceManager.GetString('custpage_rowperpage', screenName), null, tabProposal);
        for (i = 1; i < 5; i++) {
            fldPropPerPage.addSelectOption(i, i * this.defPropPerPage,
                (this.reqParams.propPerPage / this.defPropPerPage == i));
        }

        subListProp = form.addSubList('proposals', 'list',
            FAM.resourceManager.GetString('proposals', screenName), tabProposal);
        subListProp.addField('marked', 'checkbox', FAM.resourceManager.GetString('marked',
            screenName));
        subListProp.addField('id', 'text', FAM.resourceManager.GetString('propid', screenName)).setDisplayType('inline');
        subListProp.addField('editurl', 'url', FAM.resourceManager.GetString('editurl', screenName),
            true).setLinkText(FAM.resourceManager.GetString('editurl', screenName));
        subListProp.addField('custrecord_propsourceid', 'text', FAM.resourceManager.GetString(
            'custrecord_propsourceid', screenName), 'transaction').setDisplayType('inline');
        subListProp.addField('custrecord_propassettype', 'text', FAM.resourceManager.GetString(
            'custrecord_propassettype', screenName),
            (new FAM.AssetType_Record()).getRecordTypeId()).setDisplayType('inline');
        subListProp.addField('custrecord_propassetdescr', 'textarea', FAM.resourceManager.GetString(
            'custrecord_propassetdescr', screenName)).setDisplayType('inline');
        subListProp.addField('custrecord_propquantity', 'integer', FAM.resourceManager.GetString(
            'custrecord_propquantity', screenName)).setDisplayType('inline');
        subListProp.addField('custrecord_propproc', 'text', 
            FAM.resourceManager.GetString('custrecord_propproc', screenName))
        subListProp.addField('custrecord_propaccmethod', 'text', FAM.resourceManager.GetString(
            'custrecord_propaccmethod', screenName),
            (new FAM.DeprMethod_Record()).getRecordTypeId()).setDisplayType('inline');
        subListProp.addField('custrecord_propassetcost', 'currency', FAM.resourceManager.GetString(
            'custrecord_propassetcost', screenName)).setDisplayType('inline');
        subListProp.addField('custrecord_propresidvalue', 'currency', FAM.resourceManager.GetString(
            'custrecord_propresidvalue', screenName)).setDisplayType('inline');
        subListProp.addField('custrecord_propassetlifetime', 'integer',
            FAM.resourceManager.GetString('custrecord_propassetlifetime',
            screenName)).setDisplayType('inline');
        if (FAM.Context.blnOneWorld) {
            subListProp.addField('custrecord_propsubsid', 'text', FAM.resourceManager.GetString(
                'custrecord_propsubsid', screenName), 'subsidiary').setDisplayType('inline');
        }
        subListProp.addField('custrecord_propstatus', 'text', 'status').setDisplayType('hidden');
        
        
        var splitUrl = nlapiResolveURL('SUITELET',
                                    'customscript_fam_su_proposalsplit',
                                    'customdeploy_fam_su_proposalsplit') + '&propid=$PROPID$',
            splitText = FAM.resourceManager.GetString('custrecord_propsplit', screenName),
            anchorText = '<a class="dottedlink" href='+splitUrl+'>'+splitText+'</a>';

        searchProps = this.searchNewProposals(false, (this.reqParams.pageNum - 1) *
            this.reqParams.propPerPage, this.reqParams.pageNum * this.reqParams.propPerPage);
        if (searchProps.results) {
            for (j = 0; j < searchProps.results.length; j++) {
                subListProp.setLineItemValue('marked', j + 1, 'F');
                subListProp.setLineItemValue('id', j + 1, searchProps.getId(j));
                subListProp.setLineItemValue('editurl', j + 1, nlapiResolveURL('record',
                    (new FAM.AssetProposal_Record()).getRecordTypeId(), searchProps.getId(j),
                    'edit'));
                subListProp.setLineItemValue('custrecord_propsourceid', j + 1,
                    searchProps.getText(j, 'transaction'));
                subListProp.setLineItemValue('custrecord_propassettype', j + 1,
                    searchProps.getText(j, 'asset_type'));
                subListProp.setLineItemValue('custrecord_propassetdescr', j + 1,
                    searchProps.getValue(j, 'description'));
                subListProp.setLineItemValue('custrecord_propquantity', j + 1,
                    searchProps.getValue(j, 'quantity'));
                subListProp.setLineItemValue('custrecord_propproc', j + 1,
                    searchProps.getValue(j, 'quantity')>1?
                            anchorText.replace('$PROPID$',searchProps.getId(j)):null);
                subListProp.setLineItemValue('custrecord_propaccmethod', j + 1,
                    searchProps.getText(j, 'depr_method'));
                subListProp.setLineItemValue('custrecord_propassetcost', j + 1,
                    searchProps.getValue(j, 'initial_cost'));
                subListProp.setLineItemValue('custrecord_propresidvalue', j + 1,
                    searchProps.getValue(j, 'rv'));
                subListProp.setLineItemValue('custrecord_propassetlifetime', j + 1,
                    searchProps.getValue(j, 'lifetime'));

                if (FAM.Context.blnOneWorld) {
                    subListProp.setLineItemValue('custrecord_propsubsid', j + 1,
                        searchProps.getText(j, 'subsidiary'));
                }
                subListProp.setLineItemValue('custrecord_propstatus', j + 1,
                        searchProps.getValue(j, 'status'));
            }
        }
        subListProp.addMarkAllButtons();

        form.addSubmitButton(FAM.resourceManager.GetString('custrecord_assetproposal_refreshbutton',
            screenName));
        form.addButton('Propose', FAM.resourceManager.GetString(
            'custrecord_assetproposal_proposebutton', screenName),
            "main_form.custpage_ncactionid.value='Propose';if (main_form.onsubmit()){main_form.submit();}");
        form.addButton('Generate', FAM.resourceManager.GetString(
            'custrecord_assetproposal_generatebutton', screenName),
            "main_form.custpage_ncactionid.value='Generate';if (main_form.onsubmit()){main_form.submit();}");

        if (FAM.SystemSetup.getSetting('isRestrictRejectProposal') === 'F' ||
            FAM.Context.blnAdmin) {

            form.addButton('Reject', FAM.resourceManager.GetString(
                'custrecord_assetproposal_rejectbutton', screenName),
                "if (main_form.onsubmit()){main_form.custpage_ncactionid.value='Reject';main_form.submit();}");
        }

        return form;
    };
};