/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 *
 */

define(['N/ui/serverWidget'],

function famAdapterUIServerWidget(ui) {
    return {
        FieldType           : ui.FieldType,
        FieldLayoutType     : ui.FieldLayoutType,
        FieldBreakType      : ui.FieldBreakType,
        FieldDisplayType    : ui.FieldDisplayType,
        SublistType         : ui.SublistType,
        createForm          : function(options){
            return ui.createForm(options);
        }
    };
});