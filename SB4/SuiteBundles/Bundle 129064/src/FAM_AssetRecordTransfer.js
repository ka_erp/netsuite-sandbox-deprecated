/**
 * © 2016 NetSuite Inc.
 * User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
**/

var FAM;
if (!FAM) { FAM = {}; }

/**
 * Starter for Background Processing function for Asset Transfer
 *
 * Parameters:
 *     BGP {FAM.BGProcess} - Process Instance Record for this background process
 * Returns:
 *     true {boolean} - processing should be requeued
 *     false {boolean} - processing should not be requeued; essentially setting the deployment to
 *                       standby
**/

function famTransferAsset(BGP) {
    var assetTransfer = new FAM.AssetTransfer(BGP);

    try {
        return assetTransfer.run();
    }
    catch (e) {
        assetTransfer.logObj.printLog();
        throw e;
    }
}

FAM.AssetTransfer = function (procInsRec) {
    this.procInsRec = procInsRec;
    this.recCoreValue = [];
    this.recRBData = {};
    this.bgpRBData = JSON.parse(this.procInsRec.getFieldValue('rollback_data')||"{}");
    this.oldVal = {};
    this.newVal = {};
    this.primaryBook = FAM.Util_Shared.getPrimaryBookId();
    this.intCurrSym = FAM.SystemSetup.getSetting('nonDecimalCurrencySymbols');
    this.trnDate = nlapiDateToString(new Date());
    this.totalRecords = +this.procInsRec.getFieldValue('rec_total') || 0;
    this.recsProcessed = +this.procInsRec.getFieldValue('rec_count') || 0;
    this.recsFailed = +this.procInsRec.getFieldValue('rec_failed') || 0;
    this.userId = this.procInsRec.getFieldValue('user');
    this.logObj = new printLogObj('debug');
    this.subCache = new FAM.FieldCache('subsidiary');
    this.atCache = new FAM.FieldCache('customrecord_ncfar_assettype');
    this.compoundCache  = new FAM.FieldCache();
    this.perfTimer = new FAM.Timer();
    this.objAssetTransfer = {};
    this.wError = this.procInsRec.getScriptParam('upperLimit')||false;
    this.perfTimer.start();    
    
    this.assetsObj = {};    
    this.compoundId;
    this.compoundFields = {
        a : {
            asset_type            : 'typ', 
            asset_account         : 'ast', 
            depr_account          : 'dpr', 
            depr_charge_account   : 'chr', 
            writeoff_account      : 'wof', 
            writedown_account     : 'wdn', 
            disposal_account      : 'dsp', 
            subsidiary            : 'sub', 
            classfld              : 'cls', 
            department            : 'dpt', 
            location              : 'loc', 
            fixed_rate            : 'fr'
        },
        initial_cost      : 'ic', 
        current_cost      : 'cc', 
        rv                : 'rv', 
        book_value        : 'bv', 
        last_depr_date    : 'ldd',
        lastDeprAmount    : 'lda',
        cummulative_depr  : 'cd'
    };
    this.taxFields = ['custrecord_altdepr_assettype',
                      'custrecord_altdepr_subsidiary',
                      'custrecord_altdepraltmethod',
                      'custrecord_altdepr_originalcost',
                      'custrecord_altdepr_currentcost',
                      'custrecord_altdeprrv',
                      'custrecord_altdeprcd',
                      'custrecord_altdeprnbv',
                      'custrecord_altdeprpriornbv',
                      'custrecord_altdepr_assetaccount',
                      'custrecord_altdepr_depraccount',
                      'custrecord_altdepr_chargeaccount',
                      'custrecord_altdepr_writeoffaccount',
                      'custrecord_altdepr_writedownaccount',
                      'custrecord_altdepr_disposalaccount',
                      'custrecord_altdepr_accountingbook',
                      'custrecord_altdeprld',
                        'custrecord_altdepr_fixedrate',
                        'custrecord_altdepr_currency'];
    this.assetFields = ['custrecord_assetsubsidiary',
                        'custrecord_assetsubsidiary.isinactive',
                        'custrecord_assettype',
                        'custrecord_assettype.isinactive',
                        'custrecord_assetclass',
                        'custrecord_assetclass.isinactive',
                        'custrecord_assetdepartment',
                        'custrecord_assetdepartment.isinactive',
                        'custrecord_assetlocation',
                        'custrecord_assetlocation.isinactive',
                        'custrecord_assetcost',
                        'custrecord_assetcurrentcost',
                        'custrecord_assetdeprtodate',
                        'custrecord_assetmainacc',
                        'custrecord_assetdepracc',
                        'custrecord_assetdeprchargeacc',
                        'custrecord_assetwriteoffacc',
                        'custrecord_assetwritedownacc',
                        'custrecord_assetdisposalacc',
                        'custrecord_ncfar_quantity',
                        'custrecord_assetresidualvalue',
                        'custrecord_assetbookvalue',
                        'custrecord_assetlastdepramt',
                        'custrecord_assetpriornbv',
                        'custrecord_componentof',
                        'custrecord_assetlastdeprdate',
                        'custrecord_is_compound',
                        'custrecord_assetfixedexrate'];
};

FAM.AssetTransfer.prototype.execLimit = 500;
FAM.AssetTransfer.prototype.timeLimit = 30 * 60 * 1000; // 30 minutes

FAM.AssetTransfer.prototype.run = function () {
    this.logObj.startMethod('FAM.AssetTransfer.run');
    var blnRequeue, ret, lastProcessed = +this.procInsRec.getScriptParam('lowerLimit') || 0;
    
    if(this.procInsRec.getFieldValue('recordid')==-1){
        blnRequeue = this.bulkTransferAsset(lastProcessed);
    }
    else{
        this.initObjTransfer();
        this.totalRecords = 1;
        
        if (FAM.Util_Shared.isAssetCompound(this.objAssetTransfer.AssetId)){
            this.compoundId = +this.objAssetTransfer.AssetId;
            this.assetsObj = JSON.parse(this.procInsRec.getScriptParam('componentTree')) || {};
            blnRequeue = this.searchAndTransferDescendants(lastProcessed);
        }
        else{
            var validateRet = {};
            var assetVal = this.getAssetValues(this.objAssetTransfer.AssetId, lastProcessed);
            
            if (assetVal){
                var assetValidateValues = {};
                assetValidateValues[this.objAssetTransfer.AssetId] = assetVal;
                
                validateRet = this.validateTransferValues(assetValidateValues);
            }
            else{
                validateRet.isValid = false;
            }
            
            if (validateRet.blnToRequeue){
                blnRequeue = true;
            }
            else if (validateRet.isValid){
                try{
                    blnRequeue = this.transferAsset(assetVal);
                }
                catch(ex){
                    var msg = (ex.getCode != null) ? ex.getCode() + '\n' + ex.getDetails() : ex.toString(),
                        stackTraceLog = (ex.getCode != null) ? '\n' + ex.getStackTrace().join('\n') : '';
                    nlapiLogExecution('ERROR','Asset Transfer', msg + stackTraceLog);
                    this.bgpRBData = FAM.Util_Shared.mergeObject(this.bgpRBData, this.recRBData);
                    this.recsFailed = 1;
                    this.procInsRec.writeToProcessLog(msg, 'Error', this.objAssetTransfer.AssetId);
                }
                
                if(!blnRequeue && !this.wError){
                    this.recsProcessed = 1;
                }
            }
            else{
                this.recsFailed = 1;
            }
        }
    }
    ret = this.updProcIns(blnRequeue);
    this.logObj.endMethod();
    return ret;
};

FAM.AssetTransfer.prototype.bulkTransferAsset = function (lastProcessed) {
    var lastAcctMethId = this.recsProcessed,
        stateVal = this.procInsRec.stateValues,
        arrAssetTransfer = JSON.parse(stateVal.transferVals),
        blnRequeue = false;

    this.totalRecords = arrAssetTransfer.length;
    for(var x = lastAcctMethId; x < arrAssetTransfer.length; x++){
        if (FAM.Util_Shared.isAssetCompound(arrAssetTransfer[x].id)){
            nlapiLogExecution('DEBUG','Skipping Compound Asset', 'ID: ' + arrAssetTransfer[x].id);
            this.procInsRec.writeToProcessLog('Skipped Compound Asset', 'Message', arrAssetTransfer[x].id);
            continue; 
        }        
        //re-initialize global variables for each asset in array
        this.recCoreValue = [];
        this.oldVal = {};
        this.newVal = {};
        this.trnDate = nlapiDateToString(new Date());
        this.initObjTransfer(arrAssetTransfer[x]);
        try{
            var validateRet = {};
            var assetVal = this.getAssetValues(this.objAssetTransfer.AssetId,lastProcessed);
            
            if (assetVal){
                var assetValidateValues = {};
                assetValidateValues[this.objAssetTransfer.AssetId] = assetVal;
                
                validateRet = this.validateTransferValues(assetValidateValues);
            }
            else{
                validateRet.isValid = false;
            }
            
            if (validateRet.blnToRequeue){
                blnRequeue = true;
            }
            else if (validateRet.isValid){
                this.recRBData = {};
                blnRequeue = this.transferAsset(assetVal);
            }
            else{
                this.recsFailed++;
                continue;
            }
            
            if(blnRequeue){
                break;
            }
            this.recsProcessed++;
        }
        catch(ex){
            var msg = (ex.getCode != null) ? ex.getCode() + '\n' + ex.getDetails() : ex.toString(),
                    stackTraceLog = (ex.getCode != null) ? '\n' + ex.getStackTrace().join('\n') : '';
            nlapiLogExecution('ERROR','Bulk Transfer', msg + stackTraceLog);
            this.bgpRBData = FAM.Util_Shared.mergeObject(this.bgpRBData, this.recRBData);
            this.recsFailed++;
            this.procInsRec.writeToProcessLog(msg, 'Error', this.objAssetTransfer.AssetId);
        }        
    }
    return blnRequeue;    
};

FAM.AssetTransfer.prototype.getAssetValues = function (id, lastTaxMethod) {
    var values = nlapiLookupField('customrecord_ncfar_asset', 
            id, this.assetFields);
    if (!values){
        var message = FAM.resourceManager.GetString(
                'custpage_transfer_asset_undefined', null, null, [id]);
        nlapiLogExecution('ERROR','Invalid Asset', message);
        this.procInsRec.writeToProcessLog(message, 'Error', id);
        return null;
    }
    
    var taxFilter = [];
    if (lastTaxMethod){        
        taxFilter.push(new nlobjSearchFilter('internalidnumber', null, 
                       'greaterthan', lastTaxMethod));
    }
    var altMethods = FAM.Util_Shared.getAssetsTaxMethods(id, this.taxFields, taxFilter);
    values['altMethods'] = altMethods[id];
    return values;
};

FAM.AssetTransfer.prototype.initObjTransfer = function (objAsset) {
    this.objAssetTransfer = {};
    if(objAsset){
        this.objAssetTransfer.AssetId = objAsset.id;
        this.objAssetTransfer.AssetType = (objAsset.a||'')+'';
        this.objAssetTransfer.Subsidiary = (objAsset.s||'')+'';
        this.objAssetTransfer.Class = (objAsset.c||'')+'';
        this.objAssetTransfer.Department = (objAsset.d||'')+'';
        this.objAssetTransfer.Location = (objAsset.l||'')+'';
    }else{
        this.objAssetTransfer.AssetId = this.procInsRec.getFieldValue('recordid');
        this.objAssetTransfer.AssetType = this.procInsRec.stateValues.AssetType||'';
        this.objAssetTransfer.Subsidiary = this.procInsRec.stateValues.Subsidiary||'';
        this.objAssetTransfer.Class = this.procInsRec.stateValues.Class||'';
        this.objAssetTransfer.Department = this.procInsRec.stateValues.Department||'';
        this.objAssetTransfer.Location = this.procInsRec.stateValues.Location||'';
    }
    
    //cleanup this.bgpRBData if necessary
    if (this.bgpRBData.hasOwnProperty('pending')){
        this.recRBData = JSON.parse(this.bgpRBData['pending']);
        delete this.bgpRBData['pending'];
    }
};

FAM.AssetTransfer.prototype.transferAsset = function (oldAssetRec) {
    this.logObj.startMethod('FAM.AssetTransfer.transferAsset');
    
    var updateRec, taxSearched = 0,
        lastTaxMethId,
        postingNeeded = false, 
        interCompanyPosting = false,
        assetRec = new FAM.Asset_Record(),
        srcAcct = '',
        desAcct = '',
        msg = '',
        currData,
        blnToRequeue = false;
    
    var parentCompoundId = oldAssetRec['custrecord_componentof'];
    if (this.compoundId){
        lastTaxMethId = this.assetsObj[parentCompoundId] ? this.assetsObj[parentCompoundId]['m'] || 0 : 0; 
    }
    else{
        lastTaxMethId = +this.procInsRec.getScriptParam('lowerLimit') || 0;
    }

    assetRec.recordId = this.objAssetTransfer.AssetId;
    
    this.getOldValues(oldAssetRec);
    this.newVal = this.collateChanges(lastTaxMethId,this.oldVal);
    
    if (this.newVal.subsidiary) {
        postingNeeded = true;
        interCompanyPosting = true;
        
        var transferAcctSearch = new FAM.Search(new FAM.TransferAccount());        
        transferAcctSearch.addFilter('origin_subsidiary', null, 'is',  this.oldVal.subsidiary);
        transferAcctSearch.addFilter('destination_subsidiary', null, 'is', this.newVal.subsidiary);
        transferAcctSearch.addColumn('origin_account');
        transferAcctSearch.addColumn('destination_account');
        
        transferAcctSearch.run();
        if(transferAcctSearch.results && transferAcctSearch.results.length == 1){
            srcAcct = transferAcctSearch.getValue(0, 'origin_account'); 
            desAcct = transferAcctSearch.getValue(0, 'destination_account');
        }
        else {
            throw nlapiCreateError('NCFAR_TRANSFERSETUP', FAM.resourceManager.GetString(
                'custpage_transfer_account_missing', null, null,
                [this.oldVal.subsidiary, this.newVal.subsidiary]));
        }
    }
    // For CDL transfer, do not write DHR/JE if either old or new value is blank
    else if ((this.oldVal.classfld && this.newVal.classfld && this.newVal.classfld != 'unset' &&
            FAM.SystemSetup.getSetting('isPostClass') === 'T') ||
        (this.oldVal.department && this.newVal.department && this.newVal.department != 'unset' &&
            FAM.SystemSetup.getSetting('isPostDepartment') === 'T') || 
        (this.oldVal.location && this.newVal.location && this.newVal.location != 'unset' &&
            FAM.SystemSetup.getSetting('isPostLocation') === 'T') ||
        this.objAssetTransfer.AssetType) {        
        postingNeeded = true;
    }
    
    if (postingNeeded) {
        do {
            if (lastTaxMethId === 0) {
                //fetch Asset Record Data to recCoreValue array
                this.fetchAssetRecordData(oldAssetRec);
            }
            if (this.totalRecords > 0 && oldAssetRec.altMethods) {
                //add tax method data to recCoreValue array
                taxSearched = this.fetchTaxMethodData(oldAssetRec.custrecord_ncfar_quantity,
                                                      oldAssetRec.altMethods, lastTaxMethId);
            }
            
            this.logObj.pushMsg('Process all records for transfer');
            for (var i = 0; i < this.recCoreValue.length; i++) {
                if (this.hasExceededLimit()) {
                    blnToRequeue = true;
                    break;
                }
                
                var newBookVal, recValue = {};
                this.logObj.pushMsg('Processing Asset Id: ' + this.objAssetTransfer.AssetId);
                if (this.recCoreValue[i].altDep) {
                    this.logObj.pushMsg('Tax Method Id: ' + this.recCoreValue[i].altDep);
                }
                try {
                    if (!interCompanyPosting) { //CDL Transfer
                        // For CDL transfer, write DHR/JE only when book id is present.
                        // Asset type for now, maybe others also in the future, 
                        // will use this part, we'll check if it's a CDL transfer(this part is definite).
                        if ((this.newVal.department || this.newVal.classfld || this.newVal.location) 
                                && !this.recCoreValue[i].bookId) {
                           this.logObj.logExecution('Not writing DHR/JE for CDL transfer w/out book id.');
                           continue; 
                        }
                        
                        if (((FAM.Context.getPreference('deptsperline') === 'F' && this.newVal.department) ||
                                (FAM.Context.getPreference('classesperline') === 'F' && this.newVal.classfld) ||
                                (FAM.Context.getPreference('locsperline') === 'F' && this.newVal.location))) { //single entry CDL
                            // out
                            recValue.costType               = 1; //costing schema 1
                            recValue.assetId                = this.objAssetTransfer.AssetId;
                            recValue.bookId                 = this.recCoreValue[i].bookId;
                            recValue.accts                  = [this.recCoreValue[i].oMainAcct,
                                                               this.recCoreValue[i].oDeprAcct,
                                                               this.recCoreValue[i].oMainAcct];
                            recValue.subsidiary             = this.recCoreValue[i].subsidiary;
                            recValue.asset_type             = this.oldVal.asset_type;
                            recValue.classfld               = this.oldVal.classfld;
                            recValue.department             = this.oldVal.department;
                            recValue.location               = this.oldVal.location;
                            recValue.currCost               = this.recCoreValue[i].currCost;
                            recValue.deprToDate             = this.recCoreValue[i].deprCost;
                            recValue.quantity               = this.recCoreValue[i].qty;
                            recValue.acqTransaction_amount  = this.recCoreValue[i].currCost * -1;
                            recValue.acqNet_book_value      = 0;
                            recValue.depTransaction_amount  = -this.recCoreValue[i].deprCost; 
                            recValue.depNet_book_value      = 0;
                            if(this.recCoreValue[i].altDep){
                                recValue.altDep = this.recCoreValue[i].altDep;
                            }
                            recValue.refs                   = ['Asset Transfer Out (FAM)', 'Asset Transfer Out (FAM)', 'Asset Transfer Out (FAM)']; 
                            recValue.failurePoint           = 'CDL/Asset Type Transfer Out';
                            this.writeJournalandHistory(recValue);

                            //in
                            recValue = JSON.parse(JSON.stringify(recValue));  //re-initialize and remove object reference for testability
                            if(this.recCoreValue[i].nMainAcct && this.recCoreValue[i].nDeprAcct){
                                recValue.costType               = 2; //costing schema 1
                                recValue.accts                  = [this.recCoreValue[i].nMainAcct,
                                                                   this.recCoreValue[i].nDeprAcct,
                                                                   this.recCoreValue[i].nMainAcct];
                                recValue.asset_type             = this.oldVal.asset_type;
                                recValue.classfld               = this.newVal.classfld;
                                recValue.department             = this.newVal.department;
                                recValue.location               = this.newVal.location;
                                recValue.currCost               = this.recCoreValue[i].currCost;
                                recValue.acqTransaction_amount  = this.recCoreValue[i].currCost;
                                recValue.acqNet_book_value      = this.recCoreValue[i].currCost;
                                recValue.depTransaction_amount  = this.recCoreValue[i].deprCost; 
                                recValue.depNet_book_value      = this.recCoreValue[i].currCost - this.recCoreValue[i].deprCost;
                                recValue.refs                   = ['Asset Transfer In (FAM)', 'Asset Transfer In (FAM)', 'Asset Transfer In (FAM)']; 
                                recValue.failurePoint           = 'CDL/Asset Type Transfer In';
                                
                                this.writeJournalandHistory(recValue);
                            }
                        }
                        else {//per line CDL transfer/Asset Type transfer
                              //create DHR/JE only if there are valid accounts and asset type is valid
                            if(this.recCoreValue[i].nMainAcct && this.recCoreValue[i].nDeprAcct) {
                                var isOldTypeActive = false;
                                if (this.newVal.asset_type) {
                                    isOldTypeActive = oldAssetRec['custrecord_assettype.isinactive'] == 'T';
                                }
                                //out
                                recValue.costType               = 3; //costing schema 3
                                recValue.assetId                = this.objAssetTransfer.AssetId;
                                recValue.accts                  = [this.recCoreValue[i].oMainAcct,
                                                                   this.recCoreValue[i].oDeprAcct,
                                                                   this.recCoreValue[i].nMainAcct,
                                                                   this.recCoreValue[i].nDeprAcct];
                                recValue.subsidiary             = this.recCoreValue[i].subsidiary;
                                recValue.asset_type             = this.oldVal.asset_type;
                                recValue.classfld = !this.newVal.classfld ? 
                                        this.oldVal.classfld + '' : [this.oldVal.classfld + '', this.oldVal.classfld + '', this.newVal.classfld + '', this.newVal.classfld + ''];
                                recValue.department = !this.newVal.department ? 
                                        this.oldVal.department + '' : [this.oldVal.department + '', this.oldVal.department + '', this.newVal.department + '', this.newVal.department + ''];
                                recValue.location = !this.newVal.location ? 
                                        this.oldVal.location + '' : [this.oldVal.location + '', this.oldVal.location + '', this.newVal.location + '', this.newVal.location + ''];
                                recValue.currCost               = this.recCoreValue[i].currCost;
                                recValue.deprToDate             = this.recCoreValue[i].deprCost;
                                recValue.quantity               = this.recCoreValue[i].qty;
                                recValue.acqTransaction_amount  = this.recCoreValue[i].currCost * -1; 
                                recValue.acqNet_book_value      = 0;
                                recValue.depTransaction_amount  = -this.recCoreValue[i].deprCost; 
                                recValue.depNet_book_value      = 0;
                                if(this.recCoreValue[i].altDep){
                                    recValue.altDep = this.recCoreValue[i].altDep;
                                }
                                recValue.bookId                 = this.recCoreValue[i].bookId;
                                recValue.refs                   = ['Asset Transfer Out (FAM)', 'Asset Transfer Out (FAM)', 'Asset Transfer In (FAM)', 'Asset Transfer In (FAM)']; 
                                recValue.failurePoint           = 'CDL Transfer Out';
                                if(!isOldTypeActive) {
                                    recValue.journalId = this.writeJournalandHistory(recValue); //reuse journal id from cdl transfer out
                                    
                                    //in (should not create journal)
                                    recValue = JSON.parse(JSON.stringify(recValue));  //re-initialize and remove object reference for testability
                                    recValue.asset_type = this.newVal.asset_type || this.oldVal.asset_type;
                                    recValue.acqTransaction_amount = this.recCoreValue[i].currCost; 
                                    recValue.acqNet_book_value     = this.recCoreValue[i].currCost;
                                    recValue.depTransaction_amount = this.recCoreValue[i].deprCost; 
                                    recValue.depNet_book_value     = this.recCoreValue[i].currCost - this.recCoreValue[i].deprCost;
                                    recValue.failurePoint          = 'CDL Transfer In';
                                    
                                    this.writeJournalandHistory(recValue);
                                }else{
                                    msg = 'Inactive Old Asset Type. Skipping creation of DHR/Journal Entries.';
                                    if(recValue.altDep){
                                        msg += ' (Tax Method ID: ' + recValue.altDep + ' )';
                                    }
                                    this.procInsRec.writeToProcessLog(msg, 'Error', 'Asset Id: ' + this.objAssetTransfer.AssetId);
                                }
                            }
                        }
                    } 
                    else { //subsidiary transfer
                        recValue.costType              = 1; //costing schema 1
                        recValue.assetId               = this.objAssetTransfer.AssetId;
                        recValue.bookId                = this.recCoreValue[i].bookId;
                        recValue.inactiveoldsub        = oldAssetRec['custrecord_assetsubsidiary.isinactive']=='T';
                        //out
                        recValue.accts                 = [this.recCoreValue[i].oMainAcct,
                                                          this.recCoreValue[i].oDeprAcct,
                                                          srcAcct];
                        recValue.subsidiary            = this.recCoreValue[i].subsidiary;
                        recValue.asset_type            = this.oldVal.asset_type;
                        recValue.classfld              = this.oldVal.classfld;
                        recValue.department            = this.oldVal.department;
                        recValue.location              = this.oldVal.location;
                        recValue.currCost              = this.recCoreValue[i].currCost;
                        recValue.deprToDate            = this.recCoreValue[i].deprCost;
                        recValue.quantity              = this.recCoreValue[i].qty;
                        recValue.acqTransaction_amount = this.recCoreValue[i].currCost * -1; 
                        recValue.acqNet_book_value     = 0;
                        recValue.depTransaction_amount = -this.recCoreValue[i].deprCost; 
                        recValue.depNet_book_value     = 0;
                        if(this.recCoreValue[i].altDep){
                            recValue.altDep = this.recCoreValue[i].altDep;
                        }
                        recValue.refs = ['Asset Transfer Out (FAM)', 'Asset Transfer Out (FAM)', 'Asset Transfer Out (FAM)']; 
                        recValue.failurePoint = 'Subsidiary Transfer Out';
                         
                        this.writeJournalandHistory(recValue);
                         
                         //in
                        recValue = JSON.parse(JSON.stringify(recValue));  //re-initialize and remove object reference for testability
                        recValue.costType              = 4; //costing schema 4 (reverse debit/credit
                        recValue.accts                 = [this.recCoreValue[i].nMainAcct,
                                                          this.recCoreValue[i].nDeprAcct,
                                                          desAcct];
                        recValue.subsidiary = this.newVal.subsidiary;
                        recValue.asset_type = this.newVal.asset_type || this.oldVal.asset_type;
                        recValue.classfld = this.newVal.classfld || this.oldVal.classfld;
                        recValue.department = this.newVal.department || this.oldVal.department;
                        recValue.location = this.newVal.location || this.oldVal.location;

                        newBookVal = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].bookVal * this.recCoreValue[i].exRate, this.recCoreValue[i].currency, this.intCurrSym);
                        
                        recValue.currCost              = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].currCost * this.recCoreValue[i].exRate, this.recCoreValue[i].currency, this.intCurrSym);
                        recValue.deprToDate            = recValue.currCost - newBookVal;
                        recValue.acqTransaction_amount = recValue.currCost; 
                        recValue.acqNet_book_value     = recValue.currCost;
                        recValue.depTransaction_amount = recValue.deprToDate; 
                        recValue.depNet_book_value     = recValue.currCost - recValue.deprToDate;
                        recValue.refs                  = ['Asset Transfer In (FAM)', 'Asset Transfer In (FAM)', 'Asset Transfer In (FAM)']; 
                        recValue.failurePoint          = 'Subsidiary Transfer In';
                        
                        this.writeJournalandHistory(recValue);
                    }
                    
                    //Update Records here
                    if (!this.recCoreValue[i].altDep) { //update assetrecord
                        if (this.compoundId){
                            this.assetsObj[parentCompoundId] = this.assetsObj[parentCompoundId] || {};
                        }
                        if(this.newVal.asset_type){//emulate UI sourcing
                            this.newVal.asset_account      = this.recCoreValue[i].nMainAcct;
                            this.newVal.depr_account       = this.recCoreValue[i].nDeprAcct;
                            this.newVal.depr_charge_account= this.recCoreValue[i].nChargeAcc;
                            this.newVal.writeoff_account   = this.recCoreValue[i].nWriteOffAcc;
                            this.newVal.writedown_account  = this.recCoreValue[i].nWriteDownAcc;
                            this.newVal.disposal_account   = this.recCoreValue[i].nDispAcc;
                            
                            var objRB = {};
                            assetRec.loadRecord(assetRec.recordId, {recordmode: 'dynamic'});
                            
                            for(var e in this.newVal){
                                objRB[e] = assetRec.getFieldValue(e);
                                assetRec.setFieldValue(e, this.newVal[e]);
                            }
                            
                            this.createRollBackData('a', assetRec.recordId, objRB, oldAssetRec);
                            assetRec.submitRecord();
                            
                            if (this.compoundId && !this.assetsObj[parentCompoundId]['a']){
                                this.rollUpValues(parentCompoundId, this.newVal);
                            }
                        }
                        else{
                            updateRec = Object.assign({}, this.newVal);
                            if(this.newVal.classfld && this.newVal.classfld == 'unset')
                                updateRec.classfld = '';
                            if(this.newVal.department && this.newVal.department == 'unset')
                                updateRec.department = '';
                            if(this.newVal.location && this.newVal.location == 'unset')
                                updateRec.location = '';
                            
                            if (this.newVal.subsidiary){
                                currData = this.subCache.subCurrData(recValue.subsidiary, recValue.bookId);
                                
                                updateRec.initial_cost     = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].origCost * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);                            
                                updateRec.current_cost     = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].currCost * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                                updateRec.rv               = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].rv * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                                updateRec.book_value       = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].bookVal * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                                updateRec.lastDeprAmount   = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].ldAmnt * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                                updateRec.prior_nbv        = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].priorNBV * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                                updateRec.cummulative_depr = FAM.Util_Shared.Math.roundByCurrency(updateRec.current_cost - updateRec.book_value, currData.currSym, this.intCurrSym);
                                updateRec.fixed_rate       = this.recCoreValue[i].exRate;
                            }
                            
                            assetRec.submitField(updateRec);
                            //create asset rollback data only if it failed to updated
                            this.createRollBackData('a', assetRec.recordId, updateRec, oldAssetRec);
                            
                            if (this.compoundId){
                                if (oldAssetRec.custrecord_assetlastdeprdate){
                                    updateRec.last_depr_date = oldAssetRec.custrecord_assetlastdeprdate;
                                }
                                this.rollUpValues(parentCompoundId, updateRec);
                            }
                        }
                    } // tax methods
                    else if (this.newVal.subsidiary || this.newVal.asset_type) { //update tax methods only if subsidiary or asset type was changed
                        this.logObj.logExecution('Updating Tax Method Id: ' + this.recCoreValue[i].altDep);
                        var altDepr = new FAM.AltDeprMethod_Record();
                        altDepr.recordId = this.recCoreValue[i].altDep;
                        
                        updateRec = {};
                        if(this.newVal.asset_type){
                            updateRec.asset_type          = this.newVal.asset_type;
                            updateRec.asset_account      = this.recCoreValue[i].nMainAcct;
                            updateRec.depr_account       = this.recCoreValue[i].nDeprAcct;
                            updateRec.charge_account     = this.recCoreValue[i].nChargeAcc;
                            updateRec.write_off_account  = this.recCoreValue[i].nWriteOffAcc;
                            updateRec.write_down_account = this.recCoreValue[i].nWriteDownAcc;
                            updateRec.disposal_account   = this.recCoreValue[i].nDispAcc;
                        }
                        else{
                            updateRec.subsidiary          = this.newVal.subsidiary;
                            currData = this.subCache.subCurrData(recValue.subsidiary, recValue.bookId);
                            updateRec.original_cost      = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].origCost * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.current_cost       = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].currCost * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.residual_value     = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].rv * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.book_value         = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].bookVal * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.last_depr_amount   = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].ldAmnt * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.prior_year_nbv     = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].priorNBV * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.cumulative_depr    = FAM.Util_Shared.Math.roundByCurrency(updateRec.current_cost - updateRec.book_value, currData.currSym, this.intCurrSym);
                            updateRec.fixed_rate         = this.recCoreValue[i].exRate;
                            updateRec.currency           = this.recCoreValue[i].currency; //update currency
                        }
                        
                        altDepr.submitField(updateRec);
                        //create tax rollback data only if it failed to updated
                        this.createRollBackData('t', altDepr.recordId, updateRec, oldAssetRec['altMethods'][altDepr.recordId]);
                    }
                    else{ 
                        break;
                    }
                }
                catch(e) {
                    msg = '', procmsg = e.toString();
                    
                    if(this.recCoreValue[i].altDep){
                        msg += '\nError Processing Tax Method Id: ' + this.recCoreValue[i].altDep;
                    }else{
                        msg += '\nError Processing Asset Id: ' + this.objAssetTransfer.AssetId;
                    }
                    
                    msg += ' | Error Msg: ' + procmsg;
                    this.logObj.logExecution(msg, 'ERROR');
                    this.procInsRec.writeToProcessLog(msg, 'Error', 'Asset Id: ' + this.objAssetTransfer.AssetId);
                    this.wError = true;
                    throw e;
                    
                }
                
                lastTaxMethId = +this.recCoreValue[i].altDep || 0;
            }
            this.recCoreValue = [];
        } while (!blnToRequeue && taxSearched === 1000);
    } 
    else {//update asset CDL if blank
        updateRec = Object.assign({}, this.newVal);
        if(this.newVal.classfld == 'unset') { updateRec.classfld = ''; }
        if(this.newVal.location == 'unset') { updateRec.location = ''; }
        if(this.newVal.department == 'unset') { updateRec.department = ''; }
        if (Object.keys(this.newVal).length > 0) {
            this.createRollBackData('a', assetRec.recordId, updateRec, oldAssetRec);
            assetRec.submitField(updateRec);
        }
        if (this.compoundId){
            this.rollUpValues(parentCompoundId, updateRec);
        }
    }

    if (blnToRequeue) {
        this.logObj.logExecution('Execution Limit | Remaining Usage: ' +
            FAM.Context.getRemainingUsage() + ' | Time Elapsed: ' +
            this.perfTimer.getReadableElapsedTime());
        if (this.compoundId){
            this.assetsObj[parentCompoundId]['m'] = lastTaxMethId;
            var params = { componentTree : JSON.stringify(this.assetsObj) };
            if (this.assetsObj[parentCompoundId]['l']){
                params['lowerLimit'] = this.assetsObj[parentCompoundId]['l'];
            }
            this.procInsRec.setScriptParams(params);
        }
        else{
            this.procInsRec.setScriptParams({ lowerLimit : lastTaxMethId });
        }
    }
    this.logObj.endMethod();
    return blnToRequeue;
};

FAM.AssetTransfer.prototype.writeJournalandHistory = function(recValue){
    this.logObj.startMethod('FAM.AssetTransfer.writeJournalandHistory');
    var msg,
    zeroVal = (recValue.currCost == 0 && recValue.deprToDate == 0),
    debits = [],
    credits = [],
    deprHistVal = {},
    journalId = null,
    currData = this.subCache.subCurrData(recValue.subsidiary, recValue.bookId);
    
    
    if (!zeroVal){ //don't create history/journal if both current cost and depreciation to date is zero
        if((FAM.Context.blnOneWorld && 
            recValue.subsidiary && 
            this.subCache.fieldValue(recValue.subsidiary, 'isinactive') == 'T') ||
            this.atCache.fieldValue(recValue.asset_type, 'isinactive') == 'T'){ //do not create DHR or Journals for inactive subsidiaries/at
            msg = 'Inactive Old Subsidiary/Asset Type. Skipping creation of DHR/Journal Entries. ';
            if(recValue.altDep){
                msg += ' (Tax Method ID: ' + recValue.altDep + ' )';
            }
            this.procInsRec.writeToProcessLog(msg, 'Error', 'Asset Id: ' + this.objAssetTransfer.AssetId);
            this.wError = true;
            return;
        }
        
        if(recValue.bookId && !recValue.inactiveoldsub){
            if(recValue.costType == 1){
                credits[0] = FAM.Util_Shared.Math.roundByCurrency(recValue.currCost, currData.currSym, this.intCurrSym);
                credits[1] = 0;
                credits[2] = 0;
                debits[0]  = 0;
                debits[1]  = FAM.Util_Shared.Math.roundByCurrency(recValue.deprToDate, currData.currSym, this.intCurrSym);
                debits[2]  = FAM.Util_Shared.Math.roundByCurrency(credits[0] - debits[1], currData.currSym, this.intCurrSym);
            }
            else if(recValue.costType == 2){
                credits[0] = 0;
                credits[1] = FAM.Util_Shared.Math.roundByCurrency(recValue.deprToDate, currData.currSym, this.intCurrSym);
                debits[0]  = FAM.Util_Shared.Math.roundByCurrency(recValue.currCost, currData.currSym, this.intCurrSym);
                credits[2] = FAM.Util_Shared.Math.roundByCurrency(debits[0] - credits[1], currData.currSym, this.intCurrSym);;
                debits[1]  = 0;
                debits[2]  = 0;
            }
            else if(recValue.costType == 3){
                credits[0] = FAM.Util_Shared.Math.roundByCurrency(recValue.currCost, currData.currSym, this.intCurrSym);
                credits[1] = 0;
                credits[2] = 0;
                credits[3] = FAM.Util_Shared.Math.roundByCurrency(recValue.deprToDate, currData.currSym, this.intCurrSym);
                debits[0]  = 0;
                debits[1]  = FAM.Util_Shared.Math.roundByCurrency(recValue.deprToDate, currData.currSym, this.intCurrSym);
                debits[2]  = FAM.Util_Shared.Math.roundByCurrency(recValue.currCost, currData.currSym, this.intCurrSym);
                debits[3]  = 0;
                
            }
            else if(recValue.costType == 4){
                debits[0] = FAM.Util_Shared.Math.roundByCurrency(recValue.currCost, currData.currSym, this.intCurrSym);
                debits[1] = 0;
                debits[2] = 0;
                credits[0]  = 0;
                credits[1]  = FAM.Util_Shared.Math.roundByCurrency(recValue.deprToDate, currData.currSym, this.intCurrSym);
                credits[2]  = FAM.Util_Shared.Math.roundByCurrency(debits[0] - credits[1], currData.currSym, this.intCurrSym);
            }
            
            if (recValue.journalId) {
                journalId = recValue.journalId;
            }
            else if (recValue.accts.indexOf('') == -1 || (recValue.accts[1] == '' &&
                recValue.deprToDate == 0)) {

                this.logObj.pushMsg(recValue.failurePoint + ' - Create Journal');
                
                var transType = 'journalentry', rbKey = 'j';
                
                if(FAM.SystemSetup.getSetting('allowCustomTransaction') === 'T'){
                    transType = 'customtransaction_fam_transfer_jrn';
                    rbKey = 'tranj';
                }
                
                journalId = FAM_Util.createJournalEntry({
                    type : transType,
                    trnDate : this.trnDate,
                    currId : currData.currId, 
                    accts : recValue.accts,
                    debitAmts : debits,
                    creditAmts : credits,
                    ref : recValue.refs,
                    subId : recValue.subsidiary,
                    classId : recValue.classfld,
                    deptId : recValue.department,
                    locId : recValue.location,
                    bookId : recValue.bookId,
                    entities : [],
                    permit : +this.procInsRec.stateValues.JrnPermit
                });
                
                if(!this.recRBData[rbKey]) {this.recRBData[rbKey] = [];}
                this.recRBData[rbKey].push(journalId);
            }
            else {
                msg = 'Skipping creation of Journals because of missing account(s)';
                this.wError = true;
                if (recValue.altDep) {
                    msg += ' (Tax Method ID: ' + recValue.altDep + ' )';
                }
                
                this.logObj.logExecution(msg, 'ERROR');
                this.procInsRec.writeToProcessLog(msg, 'Error', 'Asset Id: ' +
                    this.objAssetTransfer.AssetId);
            }
        }
    }
  //do not create history for zero value and cdl transfer
    if (!zeroVal || this.newVal.subsidiary || this.newVal.asset_type) {
        this.logObj.pushMsg(recValue.failurePoint + ' - Create Depreciation History (Acquisition/Disposal)');
        
        if(!this.recRBData['h']) {this.recRBData['h'] = [];}
        deprHistVal.subsidiary = recValue.subsidiary;
        deprHistVal.asset_type = recValue.asset_type;
        deprHistVal.transaction_type = FAM.TransactionType.Acquisition;
        deprHistVal.asset = recValue.assetId;
        deprHistVal.date = this.trnDate;
        deprHistVal.transaction_amount = FAM.Util_Shared.Math.roundByCurrency(recValue.acqTransaction_amount, currData.currSym, this.intCurrSym);
        deprHistVal.net_book_value = FAM.Util_Shared.Math.roundByCurrency(recValue.acqNet_book_value, currData.currSym, this.intCurrSym);
        deprHistVal.quantity = recValue.quantity;
        
        if(journalId)
            deprHistVal.posting_reference = journalId;
        if(recValue.altDep)
            deprHistVal.alternate_depreciation = recValue.altDep;
        if(FAM.Context.blnMultiBook && recValue.bookId)
            deprHistVal.bookId = recValue.bookId;
        
        var deprHistId = this.createDepreciationHistory(deprHistVal);
        this.recRBData['h'].push(deprHistId);
        
        this.logObj.pushMsg(recValue.failurePoint + ' - Create Depreciation History (Depreciation)');
        deprHistVal.transaction_type = FAM.TransactionType.Depreciation;
        deprHistVal.transaction_amount = FAM.Util_Shared.Math.roundByCurrency(recValue.depTransaction_amount, currData.currSym, this.intCurrSym);
        deprHistVal.net_book_value = FAM.Util_Shared.Math.roundByCurrency(recValue.depNet_book_value, currData.currSym, this.intCurrSym);
        
        deprHistId = this.createDepreciationHistory(deprHistVal);
        this.recRBData['h'].push(deprHistId);
    }
    
    this.logObj.endMethod();
    return journalId;
};

FAM.AssetTransfer.prototype.createDepreciationHistory = function (fldVal){
    this.logObj.startMethod('FAM.AssetTransfer.createDepreciationHistory');
    var deprHistRec = new FAM.DepreciationHistory_Record();
    deprHistRec.createRecord(fldVal);
    var dhrId = deprHistRec.submitRecord();
    
    this.logObj.endMethod();
    return dhrId;
};

FAM.AssetTransfer.prototype.updProcIns = function (bRequeue){
    this.logObj.startMethod('FAM.AssetTransfer.updProcIns');
    
    var ret = true, totalPercent, procFields = {};
    
    if (this.totalRecords === 0) {
        procFields.message = 'No records found.';
    }
    else {
        totalPercent = FAM_Util.round((this.recsProcessed/this.totalRecords) *
            100);
        FAM.Context.setPercentComplete(totalPercent);
        if(this.totalRecords > 1){
            procFields.message = 'Bulk Transfer Status: '; 
        }
        else {
            var msgId = this.compoundId ? this.compoundId : this.objAssetTransfer.AssetId;
            procFields.message = 'Transfer Asset Id: ' + msgId;
            for (var e in this.oldVal) {
                if (this.newVal[e]) {
                    procFields.message += ' | ' +  e + ': \'' + this.oldVal[e] + '\' to \'' +
                        this.newVal[e] + '\'';
                }
            }
            procFields.message += ' | ';
        }
        procFields.message += this.recsProcessed + ' of ' + this.totalRecords + ' Done, ' + totalPercent + '% Complete';
        
    }
    
    procFields.rec_total = this.totalRecords;
    procFields.rec_count = this.recsProcessed;
    procFields.rec_failed = this.recsFailed;
    procFields.rollback_data = JSON.stringify(this.bgpRBData);
    
    if(!bRequeue){
        if(this.recsFailed > 0 || this.wError){
            if(JSON.stringify(this.bgpRBData)!="{}"){
                procFields.status = FAM.BGProcessStatus.Reverting;
                this.procInsRec.submitField(procFields);
                ret = this.triggerRollback();
                this.logObj.endMethod();
                return ret;
            }
            
            procFields.status = FAM.BGProcessStatus.CompletedError;
        }
        else{
            procFields.status = FAM.BGProcessStatus.Completed;
        }
    }
    else{
        //temporarily append recRBData to BGPData
        this.bgpRBData['pending'] = JSON.stringify(this.recRBData);
        procFields.rollback_data = JSON.stringify(this.bgpRBData);
    }

    if(this.wError && !this.compoundId){
        this.procInsRec.setScriptParams({ upperLimit : 1 });
    }
    
    if (procFields.message && procFields.message.length > 300) {
        nlapiLogExecution('DEBUG', 'catchError', 'Error Detail contained more than maximum ' +
            'number (300) characters, truncating message..');
        procFields.message = procFields.message.substring(0, 300);
    }

    this.procInsRec.submitField(procFields);
    
    this.logObj.endMethod();
    return ret;
};

FAM.AssetTransfer.prototype.fetchTaxMethodAccounts = function (assetType){
    this.logObj.startMethod('FAM.AssetTransfer.fetchTaxMethodAccounts');
    var arrDefAltVal = [];
    var defAltDepSearch = new FAM.Search(new FAM.DefaultValuesBook_Record());
    defAltDepSearch.addFilter('assetType', null, 'is',  assetType);
    defAltDepSearch.addFilter('isinactive', null, 'is', 'F');
    defAltDepSearch.addFilter('assetAcc', null, 'noneof','@NONE@');
    defAltDepSearch.addFilter('deprAcc', null, 'noneof','@NONE@');
    defAltDepSearch.addColumn('bookId');
    defAltDepSearch.addColumn('altMethod');
    defAltDepSearch.addColumn('assetAcc');
    defAltDepSearch.addColumn('deprAcc');
    defAltDepSearch.addColumn('chargeAcc');
    defAltDepSearch.addColumn('writeOffAcc');
    defAltDepSearch.addColumn('writeDownAcc');
    defAltDepSearch.addColumn('dispAcc');
    defAltDepSearch.run();
    
    if(defAltDepSearch.results){
        for(var i = 0; i < defAltDepSearch.results.length; i++){
            arrDefAltVal[i] = {bookId: defAltDepSearch.getValue(i, 'bookId'),
                               altMethod: defAltDepSearch.getValue(i, 'altMethod'),
                               assetAcc: defAltDepSearch.getValue(i, 'assetAcc'),
                               deprAcc: defAltDepSearch.getValue(i, 'deprAcc'),
                               chargeAcc: defAltDepSearch.getValue(i, 'chargeAcc'),
                               writeOffAcc: defAltDepSearch.getValue(i, 'writeOffAcc'),
                               writeDownAcc: defAltDepSearch.getValue(i, 'writeDownAcc'),
                               dispAcc: defAltDepSearch.getValue(i, 'dispAcc')
                              };
        }
    }
    this.logObj.endMethod();
    return arrDefAltVal;
};

FAM.AssetTransfer.prototype.fetchTaxMethodData = function (qty, taxMethods, lastProcessed){
    this.logObj.startMethod('FAM.AssetTransfer.fetchTaxMethodData');
    taxMethods = FAM.Util_Shared.sortObject(taxMethods, lastProcessed);
    var taxMethodsCount = Object.keys(taxMethods).length,
        arrDefAltVal = [], 
        j = 0;
    
    if(taxMethodsCount){
        this.logObj.logExecution('Found ' + taxMethodsCount + ' Tax Methods for processing');
    }
    else {
        return 0;
    }

    if(this.newVal.asset_type){
        arrDefAltVal = this.fetchTaxMethodAccounts(this.newVal.asset_type);
    }
    
    for (var i in taxMethods){
        this.recCoreValue.push({});
        j = this.recCoreValue.length - 1;
        
        if(FAM.Context.blnMultiBook){
            this.recCoreValue[j].bookId   = taxMethods[i]['custrecord_altdepr_accountingbook'];
        }
        this.recCoreValue[j].subsidiary   = taxMethods[i]['custrecord_altdepr_subsidiary'];
        var oldCurrData = this.subCache.subCurrData(this.recCoreValue[j].subsidiary, this.recCoreValue[j].bookId);
        var newCurrData = oldCurrData;
        
        if(this.newVal.subsidiary){
            newCurrData = this.subCache.subCurrData(this.newVal.subsidiary, this.recCoreValue[j].bookId);
        } 
        
        this.recCoreValue[j].exRate   = FAM.Util_Shared.getExchangeRate(oldCurrData.currId, newCurrData.currId, this.trnDate);
        this.recCoreValue[j].oMainAcct = taxMethods[i]['custrecord_altdepr_assetaccount'];
        this.recCoreValue[j].oDeprAcct = taxMethods[i]['custrecord_altdepr_depraccount'];
        
        if(this.newVal.asset_type){
            //fetch new accts
            this.recCoreValue[j].nMainAcct = null;
            this.recCoreValue[j].nDeprAcct = null;
            this.recCoreValue[j].nChargeAcc = null;
            this.recCoreValue[j].nWriteOffAcc = null;
            this.recCoreValue[j].nWriteDownAcc = null;
            this.recCoreValue[j].nDispAcc = null;
            for(var ii = 0; ii < arrDefAltVal.length; ii++){
                if(arrDefAltVal[ii].bookId == taxMethods[i]['custrecord_altdepr_accountingbook'] && 
                   arrDefAltVal[ii].altMethod == taxMethods[i]['custrecord_altdepraltmethod']){
                    this.recCoreValue[j].nMainAcct     = arrDefAltVal[ii].assetAcc;
                    this.recCoreValue[j].nDeprAcct     = arrDefAltVal[ii].deprAcc;
                    this.recCoreValue[j].nChargeAcc    = arrDefAltVal[ii].chargeAcc;
                    this.recCoreValue[j].nWriteOffAcc  = arrDefAltVal[ii].writeOffAcc;
                    this.recCoreValue[j].nWriteDownAcc = arrDefAltVal[ii].writeDownAcc;
                    this.recCoreValue[j].nDispAcc      = arrDefAltVal[ii].dispAcc;
                    break;
                }
            }
        }
        else {
            this.recCoreValue[j].nMainAcct = this.recCoreValue[j].oMainAcct;
            this.recCoreValue[j].nDeprAcct = this.recCoreValue[j].oDeprAcct;
        }
        
        this.recCoreValue[j].currCost = taxMethods[i]['custrecord_altdepr_currentcost'];
        this.recCoreValue[j].deprCost = taxMethods[i]['custrecord_altdeprcd'];
        this.recCoreValue[j].qty      = qty;
        this.recCoreValue[j].altMeth  = taxMethods[i]['custrecord_altdepraltmethod'];
        this.recCoreValue[j].altDep   = i;
        // values for conversion
        this.recCoreValue[j].origCost = taxMethods[i]['custrecord_altdepr_originalcost'];
        this.recCoreValue[j].rv       = taxMethods[i]['custrecord_altdeprrv'];
        this.recCoreValue[j].bookVal  = taxMethods[i]['custrecord_altdeprnbv'];
        this.recCoreValue[j].ldAmnt   = taxMethods[i]['custrecord_altdeprld']||0;
        this.recCoreValue[j].priorNBV = taxMethods[i]['custrecord_altdeprpriornbv'];
        this.recCoreValue[j].currency = newCurrData.currId;
    }
    this.logObj.endMethod();
    return taxMethodsCount;
};

FAM.AssetTransfer.prototype.fetchAssetRecordData = function (oldAssetRec){
    this.logObj.startMethod('FAM.AssetTransfer.fetchAssetRecordData');
    this.recCoreValue[0] = {};
    this.recCoreValue[0].bookId   = this.primaryBook||1;
    this.recCoreValue[0].subsidiary   = this.oldVal.subsidiary;
    var oldCurrData = this.subCache.subCurrData(this.recCoreValue[0].subsidiary, this.recCoreValue[0].bookId), newCurrData = oldCurrData;
    if(this.newVal.subsidiary){
        newCurrData = this.subCache.subCurrData(this.newVal.subsidiary, this.recCoreValue[0].bookId);
    } 
    this.recCoreValue[0].exRate   = FAM.Util_Shared.getExchangeRate(oldCurrData.currId, newCurrData.currId, this.trnDate);
    this.recCoreValue[0].oMainAcct = oldAssetRec.custrecord_assetmainacc;
    this.recCoreValue[0].oDeprAcct = oldAssetRec.custrecord_assetdepracc;
    
    if(this.newVal.asset_type){
        //fetch new asset accts
        this.recCoreValue[0].assetType = this.newVal.asset_type;
        var atAccounts = nlapiLookupField('customrecord_ncfar_assettype',this.newVal.asset_type,
                            ['custrecord_assettypeassetacc','custrecord_assettypedepracc',
                             'custrecord_assettypedeprchargeacc','custrecord_assettypewriteoffacc',
                             'custrecord_assettypewritedownacc','custrecord_assettypedisposalacc']);
        this.recCoreValue[0].nMainAcct    = atAccounts.custrecord_assettypeassetacc;
        this.recCoreValue[0].nDeprAcct    = atAccounts.custrecord_assettypedepracc;
        this.recCoreValue[0].nChargeAcc   = atAccounts.custrecord_assettypedeprchargeacc;
        this.recCoreValue[0].nWriteOffAcc = atAccounts.custrecord_assettypewriteoffacc;
        this.recCoreValue[0].nWriteDownAcc= atAccounts.custrecord_assettypewritedownacc;
        this.recCoreValue[0].nDispAcc     = atAccounts.custrecord_assettypedisposalacc;
    }
    else {
        this.recCoreValue[0].nMainAcct = this.recCoreValue[0].oMainAcct;
        this.recCoreValue[0].nDeprAcct = this.recCoreValue[0].oDeprAcct;
    }
    this.recCoreValue[0].currCost = oldAssetRec.custrecord_assetcurrentcost;
    this.recCoreValue[0].deprCost = oldAssetRec.custrecord_assetdeprtodate;
    this.recCoreValue[0].qty      = oldAssetRec.custrecord_ncfar_quantity;
    //values for conversion
    this.recCoreValue[0].origCost = oldAssetRec.custrecord_assetcost;
    this.recCoreValue[0].rv       = oldAssetRec.custrecord_assetresidualvalue;
    this.recCoreValue[0].bookVal  = oldAssetRec.custrecord_assetbookvalue;
    this.recCoreValue[0].ldAmnt   = oldAssetRec.custrecord_assetlastdepramt||0;
    this.recCoreValue[0].priorNBV = oldAssetRec.custrecord_assetpriornbv;
    this.recCoreValue[0].currency = newCurrData.currId;

    this.logObj.endMethod();
};

/**
 * Determines if the Execution Limit or Time Limit has exceeded
 *
 * Returns:
 *     true {boolean} - Execution Limit or Time Limit has exceeded
 *     false {boolean} - Execution Limit or Time Limit has not exceeded
**/
FAM.AssetTransfer.prototype.hasExceededLimit = function () {
    this.logObj.startMethod('FAM.AssetTransfer.hasExceededLimit');

    var ret = FAM.Context.getRemainingUsage() < this.execLimit ||
        this.perfTimer.getElapsedTime() > this.timeLimit;

    this.logObj.endMethod();
    return ret;
};

/**
 * TODO: Move to a util
 */
FAM.AssetTransfer.prototype.validateTransferValues = function (assets){
    this.logObj.startMethod('FAM.AssetTransfer.validateTransferValues');

    var message, 
        relatedRecs,
        blnToRequeue = false,
        isValid = this.assetsObj.lVal === false ? false : true,
        arrDefAltVal = [],
        lastProcessed = this.assetsObj.lValId || 0;
    // Common errors
    if (this.objAssetTransfer.AssetType){
        if (!lastProcessed){
            var assetTypeAccounts = nlapiLookupField('customrecord_ncfar_assettype', this.objAssetTransfer.AssetType,
                    ['custrecord_assettypeassetacc', 'custrecord_assettypedepracc',
                     'custrecord_assettypeassetacc.isinactive', 'custrecord_assettypedepracc.isinactive']);
            //Check AT Asset and Depreciation Accounts
            if (!assetTypeAccounts['custrecord_assettypeassetacc']) {
                message = FAM.resourceManager.GetString('custpage_transfer_atassetacct');
                this.writeErrorLog(message);
                isValid = false;
            }            
            if (assetTypeAccounts['custrecord_assettypeassetacc.isinactive'] === 'T') {
                message = FAM.resourceManager.GetString('custpage_transfer_atassetacct_inactive');
                this.writeErrorLog(message);
                isValid = false;
            }            
            if (!assetTypeAccounts['custrecord_assettypedepracc']){
                message = FAM.resourceManager.GetString('custpage_transfer_atdepracct');
                this.writeErrorLog(message);
                isValid = false;
            }            
            if (assetTypeAccounts['custrecord_assettypedepracc.isinactive'] === 'T') {
                message = FAM.resourceManager.GetString('custpage_transfer_atdepracct_inactive');
                this.writeErrorLog(message);
                isValid = false;
            }
        }
        if (lastProcessed || isValid){
            arrDefAltVal = this.fetchTaxMethodAccounts(this.objAssetTransfer.AssetType);
        }
    }
    // CDL should not be unset if CDL are mandatory
    else if(!lastProcessed && 
            (('unset' === this.objAssetTransfer.Class && FAM.Context.getPreference('classmandatory') === 'T') ||
            ('unset' === this.objAssetTransfer.Department && FAM.Context.getPreference('deptmandatory') === 'T') ||
            ('unset' === this.objAssetTransfer.Location && FAM.Context.getPreference('locmandatory') === 'T'))){            
        message = FAM.resourceManager.GetString('custpage_transfer_cdl_mandatory');
        this.writeErrorLog(message);
        isValid = false;
    }
    
    // Validate per asset
    var assetKeys = Object.keys(assets);
    var start = assetKeys.indexOf(lastProcessed) + 1;
    for (var j = start; j < assetKeys.length; j++){
        if (this.hasExceededLimit()) {
            blnToRequeue = true;
            this.assetsObj['lVal'] = isValid;
            break;
        }
        var assetId = assetKeys[j];
        var assetVal = assets[assetId];
        var newVal = this.collateChanges(0, assetVal);
        var altDepr = FAM.Util_Shared.sortObject(assetVal.altMethods, 0);
        relatedRecs = {
            'Asset ID' : assetId
        }
        
        // Invalid S or AT transfer of component asset
        if ((newVal.asset_type || newVal.subsidiary) &&
            (!this.compoundId || this.compoundId == assetId) &&
            !FAM.Util_Shared.isNullUndefinedOrEmpty(assetVal.custrecord_componentof)){
            message = FAM.resourceManager.GetString('custpage_transfer_component_restriction', 
                    null, null, [assetId]);
            this.writeErrorLog(message, relatedRecs);
            isValid = false;
        }
        
        if (newVal.asset_type){ // AT
            // Missing accounts in asset transfers with book id
            var lastTx = this.assetsObj.lValTx || 0;
            var taxKeys = Object.keys(altDepr);
            var taxStart = taxKeys.indexOf(lastTx) + 1;
            
            for (var i = taxStart; i < taxKeys.length; i++){
                var taxId = taxKeys[i];
                if (this.hasExceededLimit()) {
                    blnToRequeue = true;
                    this.assetsObj['lVal'] = isValid;
                    break;
                }
                var altDeprObj = altDepr[taxId];
                if (!altDeprObj.custrecord_altdepr_accountingbook){continue;}
                var t_isValid = false;
                for(var ii = 0; ii < arrDefAltVal.length; ii++){
                    if(arrDefAltVal[ii].bookId == altDeprObj.custrecord_altdepr_accountingbook && 
                       arrDefAltVal[ii].altMethod == altDeprObj.custrecord_altdepraltmethod){
                        t_isValid = true;
                        break;
                    }
                }
                if(!t_isValid){
                    message = FAM.resourceManager.GetString('client_assettransfer_no_tax_meth_acct');
                    relatedRecs['Tax Method ID'] = taxId;
                    this.writeErrorLog(message, relatedRecs);
                    isValid = false;
                    delete relatedRecs['Tax Method ID'];
                }
                this.assetsObj['lValTx'] = taxId;
            }
            if (blnToRequeue){
                break;
            }
        }
        else{ // SCDL
            if (FAM.Context.blnOneWorld){
                var subsidiary = assetVal.custrecord_assetsubsidiary;
                if (newVal.subsidiary){
                    subsidiary = newVal.subsidiary;
                    
                    // Subsidiary and alternate method combination
                    var altBookIds = [];
                    var lastTx = this.assetsObj.lValTx || 0;
                    for (var i in altDepr){
                        var bookId = altDepr[i]['custrecord_altdepr_accountingbook'];
                        if (bookId && -1 === altBookIds.indexOf(bookId)){
                            altBookIds.push(bookId);
                        }
                        if (+i <= +lastTx){
                            continue;
                        }
                        if (this.hasExceededLimit()) {
                            blnToRequeue = true;
                            this.assetsObj['lVal'] = isValid;
                            break;
                        }
                        var altMethod = altDepr[i]['custrecord_altdepraltmethod'];
                        if (!FAM.Util_Shared.isValidSubsidiaryAltMethod(subsidiary, altMethod)) {
                            message = FAM.resourceManager.GetString('client_assettransfer_invalid_sub_altmeth');
                            relatedRecs['Tax Method ID'] = i;
                            this.writeErrorLog(message, relatedRecs);
                            isValid = false;
                            delete relatedRecs['Tax Method ID'];
                        }                    
                        this.assetsObj['lValTx'] = i;
                    }
                    if (blnToRequeue){
                        break;
                    }
                    
                    // Accounting Book and Subsidiary combination
                    if (altBookIds.length && !FAM.Util_Shared.isValidSubsidiaryBooks(subsidiary, altBookIds)){
                        message = FAM.resourceManager.GetString('client_assettransfer_invalid_sub_bookid');
                        this.writeErrorLog(message, relatedRecs);
                        isValid = false;
                    }

                    // Check transfer accounts
                    if (!FAM.Util_Shared.getTransferAccounts(assetVal.custrecord_assetsubsidiary, newVal.subsidiary)) {
                        message = FAM.resourceManager.GetString('custpage_transfer_notransferacct');
                        this.writeErrorLog(message, relatedRecs);
                        isValid = false;
                    }
                }
                // CDL and Subsidiary combination
                if ('unset' !== newVal.classfld) {
                   var cls = assetVal.custrecord_assetclass;
                   if (newVal.classfld){
                       cls = newVal.classfld
                   }
                   if (cls && !FAM.compareCDLSubsidiary('classification', cls, subsidiary)){
                       message = FAM.resourceManager.GetString('custpage_transfer_subclass_notmatch');
                       this.writeErrorLog(message, relatedRecs);
                       isValid = false;
                   }
                }
                if ('unset' !== newVal.department) {
                    var dept = assetVal.custrecord_assetdepartment;
                    if (newVal.department){
                        dept = newVal.department
                    }
                    if (dept && !FAM.compareCDLSubsidiary('department', dept, subsidiary)){
                        message = FAM.resourceManager.GetString('custpage_transfer_subdept_notmatch');
                        this.writeErrorLog(message, relatedRecs);
                        isValid = false;
                    }
                }
                if ('unset' !== newVal.location) {
                    var loc = assetVal.custrecord_assetlocation;
                    if (newVal.location){
                        loc = newVal.location
                    }
                    if (loc && !FAM.compareCDLSubsidiary('location', loc, subsidiary)){
                        message = FAM.resourceManager.GetString('custpage_transfer_subloc_notmatch');
                        this.writeErrorLog(message, relatedRecs);
                        isValid = false;
                    }
                }
            }
        }
        this.assetsObj['lValId'] = assetId;
        delete this.assetsObj['lValTx'];
    }
    
    if (!blnToRequeue){
        this.assetsObj = {};
    }
    else {
        nlapiLogExecution('DEBUG', 'Execution Limit','Remaining Usage: ' +
                FAM.Context.getRemainingUsage() + ' | Time Elapsed: ' +
                this.perfTimer.getReadableElapsedTime());
        var params = {
            componentTree : JSON.stringify(this.assetsObj) 
        };
        if (this.wError){
            params['upperLimit'] = 1;
        }                        
        this.procInsRec.setScriptParams(params);
    }

    this.logObj.endMethod();
    return {
        blnToRequeue : blnToRequeue,
        isValid      : isValid
    };
};

FAM.AssetTransfer.prototype.writeErrorLog = function(message, relatedRecs){
    var relRec;
    
    for (var i in relatedRecs){
        relRec = relRec ? relRec + ' | ' : '';
        relRec += i + ': ' + relatedRecs[i];
    }
    
    nlapiLogExecution('ERROR','Invalid Transfer Value.', message + (relRec ? ' ' + relRec : ''));
    this.procInsRec.writeToProcessLog(message, 'Error', relRec);
};

FAM.AssetTransfer.prototype.collateChanges = function (lastTaxMethId, oldVal){
    var newVal = {};
    if (this.objAssetTransfer.AssetType && (lastTaxMethId > 0 ||
        this.objAssetTransfer.AssetType != oldVal.asset_type)) {        
        newVal.asset_type = this.objAssetTransfer.AssetType;
    }
    if (this.objAssetTransfer.Subsidiary && (lastTaxMethId > 0 ||
        this.objAssetTransfer.Subsidiary != oldVal.subsidiary)) {        
        newVal.subsidiary = this.objAssetTransfer.Subsidiary;
    }
    if (this.objAssetTransfer.Class &&  (lastTaxMethId > 0 ||
        this.objAssetTransfer.Class != oldVal.classfld)) {      
        newVal.classfld = this.objAssetTransfer.Class;
    }
    if (this.objAssetTransfer.Department && (lastTaxMethId > 0 ||
        this.objAssetTransfer.Department != oldVal.department)) {        
        newVal.department = this.objAssetTransfer.Department;
    }
    if (this.objAssetTransfer.Location && (lastTaxMethId > 0 ||
        this.objAssetTransfer.Location != oldVal.location)) {        
        newVal.location = this.objAssetTransfer.Location;
    }
    return newVal;
};

FAM.AssetTransfer.prototype.getOldValues = function(oldAssetRec){
    this.oldVal.asset_type = oldAssetRec['custrecord_assettype'];
    this.oldVal.subsidiary = oldAssetRec['custrecord_assetsubsidiary'];
    this.oldVal.classfld = oldAssetRec['custrecord_assetclass.isinactive'] === 'T' ? null :
        oldAssetRec['custrecord_assetclass'];
    this.oldVal.department = oldAssetRec['custrecord_assetdepartment.isinactive'] === 'T' ? null :
        oldAssetRec['custrecord_assetdepartment'];
    this.oldVal.location = oldAssetRec['custrecord_assetlocation.isinactive'] === 'T' ? null :
        oldAssetRec['custrecord_assetlocation'];
};

/**
 * Searches and transfers Compound Asset and its descendants
 *
 * @param lastProcessed - id of the last processed asset
 *
 * Returns:
 *     true {boolean} - Execution Limit or Time Limit has exceeded
 *     false {boolean} - Execution Limit or Time Limit has not exceeded
**/
FAM.AssetTransfer.prototype.searchAndTransferDescendants = function (lastProcessed){
    this.logObj.startMethod('FAM.AssetTransfer.searchAndTransferDescendants');
    var blnToRequeue = false;
    var validateRet = {};
    
    lastProcessed = lastProcessed || 0;
    
    var searchResult = FAM.Util_Shared.getDescendants(this.compoundId, this.assetFields, 
                                                      null, false, {
                                                        taxFields: this.taxFields});
    searchResult[this.compoundId] = this.getAssetValues(this.compoundId, 0);
    
    
    if (searchResult && Object.keys(searchResult).length > 0) {
        var searchResult = FAM.Util_Shared.sortObject(searchResult, lastProcessed);
        validateRet = lastProcessed ? {isValid:true} : this.validateTransferValues(searchResult);
        
        if (validateRet.blnToRequeue){
            blnToRequeue = true;
        }
        else if (validateRet.isValid){
            delete searchResult[this.compoundId];
            for (var i in searchResult){
                var assetRec = searchResult[i];
                if ('T' === assetRec['custrecord_is_compound']){
                    continue;
                }
                this.recCoreValue = [];
                this.oldVal = {};
                this.newVal = {};
                this.objAssetTransfer.AssetId = i;
                
                try{
                    blnToRequeue = this.transferAsset(assetRec);
                    if(blnToRequeue){
                        break;
                    }
                    lastProcessed = i;
                    // reset last tax method
                    var parentCompound = assetRec['custrecord_componentof'];
                    if (this.assetsObj[parentCompound]){
                        this.assetsObj[parentCompound]['m'] = 0;
                    }
                    
                }
                catch(ex){
                    var msg = (ex.getCode != null) ? ex.getCode() + '\n' + ex.getDetails() : ex.toString(),
                        stackTraceLog = (ex.getCode != null) ? '\n' + ex.getStackTrace().join('\n') : '';
                    nlapiLogExecution('ERROR','Compound Asset Transfer', msg + stackTraceLog);
                    this.bgpRBData = FAM.Util_Shared.mergeObject(this.bgpRBData, this.recRBData);
                    this.recsFailed = 1;
                    this.procInsRec.writeToProcessLog(msg, 'Error', this.objAssetTransfer.AssetId);
                    validateRet.isValid = false;
                    break;
                }
            }
        }
        else{
            this.recsFailed = 1;
        }
    }
    /**
     * TODO: transfer compound even if no components
     */
    if (!blnToRequeue && validateRet.isValid && 
        Object.keys(this.assetsObj).length > 0) {
        // Update all compound assets here
        for (var i in this.assetsObj){
            if (this.hasExceededLimit()) {
                blnToRequeue = true;
                break;
            }
            
            var compoundAssetRec = new FAM.Asset_Record();
            compoundAssetRec.recordId = i;            
            compoundAssetRec.loadRecord(compoundAssetRec.recordId, {recordmode: 'dynamic'});
            
            this.updateCompoundValues(this.compoundFields,compoundAssetRec,i);
            compoundAssetRec.setFieldValue('prior_nbv','');
            compoundAssetRec.submitRecord();
            
            // Delete from assetsObj
            delete this.assetsObj[i];
        }
    }
    
    if (blnToRequeue && !validateRet.blnToRequeue) {
        nlapiLogExecution('DEBUG', 'Execution Limit','Remaining Usage: ' +
                FAM.Context.getRemainingUsage() + ' | Time Elapsed: ' +
                this.perfTimer.getReadableElapsedTime());
        var params = {
            lowerLimit : lastProcessed,
            componentTree : JSON.stringify(this.assetsObj) 
        };
        if (this.wError){
            params['upperLimit'] = 1;
        }                        
        this.procInsRec.setScriptParams(params);
    }
    
    if (!blnToRequeue && validateRet.isValid){
        this.recsProcessed = 1;
    }
    
    this.logObj.endMethod();
    return blnToRequeue;
};

/**
 * Set field values for the compound asset
 * Iterates through the parameter obj
 *
 * @param obj - fields to iterate
 * @param rec - asset record object (compound)
 * @param id - id of the compound asset
**/
FAM.AssetTransfer.prototype.updateCompoundValues = function (obj, rec, id){
    var fldMap = new FAM.Asset_Record().getAllFields();
    for(var e in obj){
        if (typeof obj[e] === 'object'){ 
            this.updateCompoundValues(obj[e], rec, id);
        }
        else{
            if(!this.recRBData['a'][id]){
                this.recRBData['a'][id] = {};
            }
            
            var key = obj[e];
            if (null!=this.assetsObj[id]['v'][key]){
                this.recRBData['a'][id][fldMap[e]] = rec.getFieldValue(e);
                rec.setFieldValue(e, this.assetsObj[id]['v'][key]);
            }
        }
    }
};

/**
 * Rollup updated values to parent compound asset (all levels)
 * 
 * @param id - id of the parent compound asset to be updated
 * @param obj - contains values to rollup
**/
FAM.AssetTransfer.prototype.rollUpValues = function (id, obj){
    var ldaKey = 'lastDeprAmount';
    for (var k in obj){
        var key = this.compoundFields[k] || this.compoundFields['a'][k];
        if (!key){
            continue;
        }
        if (!this.assetsObj[id]) {
            this.assetsObj[id] = {};
        }
        if (!this.assetsObj[id]['v']) { // v for values
            this.assetsObj[id]['v'] = {};
        }
        
        if (this.compoundFields['a'].hasOwnProperty(k)){
            if (!this.assetsObj[id]['v'][key] && null != obj[k]){
                this.assetsObj[id]['v'][key] = obj[k];
            }
        }
        else if (null != obj[k] && (key !== 'ldd' && key !== 'lda')){
            if(null == this.assetsObj[id]['v'][key]){
                this.assetsObj[id]['v'][key] = 0;
            }
            this.assetsObj[id]['v'][key] += Number(obj[k]);
        }
        else if (key === 'ldd' && obj[k] && obj[ldaKey] >= 0){
            var compoundDate = this.assetsObj[id]['v'][key] ? 
                    nlapiStringToDate(this.assetsObj[id]['v'][key]).getTime() : 0;
            var componentDate = nlapiStringToDate(obj[k]).getTime();
            
            if (compoundDate < componentDate){
                this.assetsObj[id]['v'][key] = obj[k];
                this.assetsObj[id]['v']['lda'] = obj[ldaKey];
            }
            else if (compoundDate === componentDate){
                if(null == this.assetsObj[id]['v']['lda']){
                    this.assetsObj[id]['v']['lda'] = 0;
                }
                this.assetsObj[id]['v']['lda'] += Number(obj[ldaKey]);
            }
        }
    }
    
    var parentCompundId = this.compoundCache.funcValCache('nlapiLookupField', 
            'customrecord_ncfar_asset', id, 'custrecord_componentof');
    if (parentCompundId){
        this.rollUpValues(parentCompundId, obj); 
    }
};

FAM.AssetTransfer.prototype.triggerRollback = function() {
    /**
     * Do you wanna know what makes a good rollback data? See below:
     * { j : [list of standard journal entries],
     *   tranj : [list of custom transfer journal entries]  
     *   h : [list of histories],
     *   a : { <asset_id>: { <fields>: <values>}}, 
     *   t : { <tax_id>: { <fields>: <values>}} };
     */
    var ret, mrTriggerURL = nlapiResolveURL("suitelet", "customscript_fam_triggermr_su",
            "customdeploy_fam_triggermr_su", true);
    
    var funcName = "customscript_fam_mr_rollbackrecords";
    var processId = this.procInsRec.recordId;
    
    var otherScript = nlapiRequestURL(mrTriggerURL,
        { custscript_fam_mapreducescriptid : funcName,
          custscript_fam_bgpid : processId });
        
    if (otherScript.getBody() == "T") {
        nlapiLogExecution("DEBUG", "FAM.BGProcess_SS.run", "Map/Reduce Script triggered | " +
            "funcName: " + funcName);
        ret = false;
    }
    else {
        this.procInsRec.setFieldValue("status", FAM.BGProcessStatus.Failed);
        this.procInsRec.setFieldValue("message", otherScript.getBody().substring(0, 300));
        this.procInsRec.submitRecord();
        this.procInsRec.scheduleScript(null, FAM.Context.getDeploymentId());
        ret = true;
    }
    
    return ret;
};

/**
 * Create record-level rollback data
 * 
 * @param type - 'a' or 't'
 * @param id - record id
 * @param objUpd - field-value pairing 
 */
FAM.AssetTransfer.prototype.createRollBackData = function(type, id, objUpd, oldAssetRec){
    var rec = type=='a'? new FAM.Asset_Record() : new FAM.AltDeprMethod_Record(),
        fldMap = rec.getAllFields();
    
    if(!this.recRBData[type]){
        this.recRBData[type]={};
    }
    this.recRBData[type][id] = {};
    
    for(var e in objUpd){
        this.recRBData[type][id][fldMap[e]] = oldAssetRec[fldMap[e]]||'';
    }
};
