/**
 * Company           Explore Consulting
 * Copyright         2015 Explore Consulting, LLC
 * Description       Used to trigger category JSON cache file creation when categories are added or updated
 **/


EC.onAfterSubmit = function (type) {
    Log.d('triggering scheduled script', 'customscript_ec_create_categories_file');
    var result = nlapiScheduleScript('customscript_ec_create_categories_file', 'customdeploy_ec_sched_createitemcache');
    if (!result) {
        Log.e("failed to schedule script", result);
    }
};

Log.AutoLogMethodEntryExit();