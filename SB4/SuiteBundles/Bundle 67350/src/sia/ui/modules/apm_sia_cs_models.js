/**
 * © 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       23 Feb 2015     jyeh             Initial
 *
 */

Ext4.define('PSGP.APM.SIA.Model.suitescriptDetailData', {
    extend : 'Ext4.data.Model',
    fields : [
        {
            name : 'date',
            type : 'string'
        }, {
            name : 'scripttype',
            type : 'string'
        }, {
            name : 'script',
            type : 'string'
        }, {
            name : 'triggertype',
            type : 'string'
        }, {
            name : 'scriptid',
            type : 'string'
        }, {
            name : 'deployment',
            type : 'int',
            useNull : true
        }, {
            name : 'totaltime',
            type : 'float'
        },  {
            name : 'usagecount',
            type : 'int'
        },  {
            name : 'records',
            type : 'int'
        }, {
            name : 'urlrequests',
            type : 'int'
        }, {
            name : 'searches',
            type : 'int'
        }, {
            name : 'threadid',
            type : 'int'
        }, {
            name : 'id',
            type : 'string'
        }, {
            name : 'ssaend',
            type : 'string'
        }, {
            name : 'ssastart',
            type : 'string'
        }, {
            name : 'scriptwfurl',
            type : 'string'
        }, {
            name : 'deploymenturl',
            type : 'string'
        }
    ]
});