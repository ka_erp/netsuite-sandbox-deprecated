/**
 * Copyright 2014 NetSuite Inc. User may not copy, modify, distribute, or
 * re-bundle or otherwise make available this code.
 */

var VAT = VAT || {};
VAT.EU = VAT.EU || {};
VAT.EU.ESL = VAT.EU.ESL || {};
VAT.EU.ESL.BE = VAT.EU.ESL.BE || {};

/**
 * Report Class
 */
VAT.EU.ESL.BE.Report = function _BEReport(baseDetails, countryDetails) {
    if (!baseDetails) {
        throw nlapiCreateError('MISSING_ARGUMENT', 'baseDetails is required');
    }
    
    if (!countryDetails) {
        throw nlapiCreateError('MISSING_ARGUMENT', 'countryDetails is required');
    }
    
    VAT.EU.ESL.BaseReport.call(this);
    
    try {
        this.initializeBaseDetails(baseDetails);
        this.supplementCountryDetails(countryDetails);
    } catch (e) {
        logException(e, 'VAT.EU.ESL.BE.Report');
        throw e;
    }
};

VAT.EU.ESL.BE.Report.prototype = Object.create(VAT.EU.ESL.BaseReport.prototype);


/**
 * Data Formatter Class
 */
VAT.EU.ESL.BE.DataFormatter = function _BEDataFormatter() {
    VAT.EU.BaseDataFormatter.call(this);
};

VAT.EU.ESL.BE.DataFormatter.prototype = Object.create(VAT.EU.BaseDataFormatter.prototype);

VAT.EU.ESL.BE.DataFormatter.prototype.formatData = function _formatData() {
    try {
        this.setColumnProperty('align');
        this.setDecimalPlaces(2, 'amount');
    } catch (e) {
        logException(e, 'VAT.EU.ESL.BE.DataFormatter.formatData');
        throw e;
    }
};