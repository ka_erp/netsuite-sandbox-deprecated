/**
 * Copyright 2016 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var Tax = Tax || {};
Tax.EU = Tax.EU || {};
Tax.EU.Intrastat = Tax.EU.Intrastat || {};
Tax.EU.Intrastat.GB = Tax.EU.Intrastat.GB || {};
Tax.EU.Intrastat.GB.Csv = Tax.EU.Intrastat.GB.Csv || {};

Tax.EU.Intrastat.GB.Csv.CompanyInfoAdapter = function _CompanyInfoAdapter() {
	Tax.EU.Intrastat.Adapter.CompanyInfoAdapter.call(this);
    this.Name = 'CompanyInfoAdapter';
    this.daos = ['CompanyInformationDAO'];
};
Tax.EU.Intrastat.GB.Csv.CompanyInfoAdapter.prototype = Object.create(Tax.EU.Intrastat.Adapter.CompanyInfoAdapter.prototype);

Tax.EU.Intrastat.GB.Csv.CompanyInfoAdapter.prototype.transform = function _transform(params) {
    if (!params) {
        throw nlapiCreateError('MISSING_PARAMETER', 'params argument is required');
    }

    try {
        var companyInfoDAO = this.rawdata[0];
        
        this.companyInfo = companyInfoDAO.company[0];
        this.onlineDAO = Tax.Cache.MemoryCache.getInstance().load('OnlineDAO');
        this.configList = companyInfoDAO.vatConfig;

        var headerData = this.getHeaderData(params);

        var result = {};
        result[this.Name] = [headerData];

        return result;
    } catch (ex) {
        logException(ex, 'Tax.EU.Intrastat.GB.Csv.CompanyInfoAdapter.transform');
        throw ex;
    }
};

Tax.EU.Intrastat.GB.Csv.CompanyInfoAdapter.prototype.getHeaderData = function _getHeaderData(params) {
    if (!params) {
        throw nlapiCreateError('MISSING_PARAMETER', 'params argument is required');
    }

    try {
        var headerData = this.getTaxPeriod(params);
        headerData.company = this.companyInfo.nameNoHierarchy;
        headerData.vatNo = this.getCompanyVrn();
        headerData.intrastatBranchId = this.getVATConfigData('IntrastatBranchNo') || '';
        headerData.timeStamp = new Date().toString('MM/dd/yyyy');
        headerData.lineIndicator = this.getLineIndicator();

        return headerData;
    } catch (ex) {
        logException(ex, 'Tax.EU.Intrastat.GB.Csv.CompanyInfoAdapter.getHeaderData');
        throw ex;
    }
};

Tax.EU.Intrastat.GB.Csv.CompanyInfoAdapter.prototype.getLineIndicator = function _getLineIndicator() {
	try {
		var cachedResult = Tax.Cache.MemoryCache.getInstance().load('Collector');

		for (var i = 0; i < cachedResult.length; i++) {
			if (cachedResult[i]['ExportAdapter']) {
				return cachedResult[i]['ExportAdapter'].length > 0 ? 'X' : 'N';
			}
		}

		return 'N';
	} catch (ex) {
        logException(ex, 'Tax.EU.Intrastat.GB.Csv.CompanyInfoAdapter.getLineIndicator');
        throw ex;
	}
};
