/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Jun 2013     bsomerville
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {
	// MD5 store description
	// Update RA Functions end
	var fileName = dataIn.FileName;
	var database = dataIn.Database;
	var filePath = dataIn.Path;
	var fileType = dataIn.Type;
	var folderName = dataIn.FolderName;
	var fileID = dataIn.ID;	
	var noData = dataIn.NoData == 'true';
	var md5info = dataIn.MD5;
	var lastModified = dataIn.NewSince;
	var comment = dataIn.Comment;
	var data = dataIn.Data;
	var isBinary = dataIn.IsBinary == 'true';
	
	if(md5info) md5info = md5info.toLowerCase();
	
	var action = dataIn.Action;
	
	var res = null;
	
	switch(action) {
		case "File":
			res = getFile (fileName, fileID, noData, md5info,filePath);
			if(!res) {
				res = new Object();
				res.Status = "Path Filter Mismatch";
			}
			break;
		case "Put":
			res = putFile (database,filePath,fileName, fileType,comment, data, isBinary);
			break;
		case "Folder":
			if(!fileID && folderName)
				fileID = getFolderID (folderName);
			res = getFolderInfo ( fileID );
			break;
		case "GetNew":
			//folderId = getFolderID (folderName);
			res = getNewSince(lastModified, folderName);			
			break;
	}
	
	if(!res) {
		res = new Object();
		res.Status = "Invalid action";
	}
	
	return res;
}


function putFile(database,filepath,filename,type,comment,fileData,isBinary) {	
	var NSPOSRoot = getCreateFolderID("NSPOS", null);
	var RAFSRoot =  getCreateFolderID("RAFS Content", NSPOSRoot);
	var FileRoot =  getCreateFolderID(database, RAFSRoot);
	
	var pathParts = filepath.split('/');
	
	// Ensure directory structure exists
	for(var p in pathParts) {
		FileRoot = getCreateFolderID(pathParts[p], FileRoot);
	}
	
	nlapiLogExecution("DEBUG", "File Put", filename);
	
	// Create full path (will be appended to RAFS Root)
	var fullPath = database + '/' + filepath + '/' + filename;
	
	nlapiLogExecution("DEBUG", "File Put Path", fullPath);
	
	if(!fullPath) return "Failed";
	
	var data = [];
	
	if(!isBinary) {
		nlapiLogExecution("DEBUG", "File Encoding", "ascii");
		data = nlapiDecrypt(fileData,"base64");
		type="PLAINTEXT";
	} else {
		nlapiLogExecution("DEBUG", "File Encoding", "binary");
		nlapiLogExecution("DEBUG", "Content Length", fileData.length);	
		
		// No need to decode binary, but type must be a binary type
		// JPGIMAGE appears to work for all binary types
		type = 'JPGIMAGE';
		data = fileData;
	}
	
	// Create file as either plaintext or JPGIMAGE based on binary status
	var fileobj = nlapiCreateFile(filename, type, data);
	fileobj.setDescription(comment);	// Set comment
	fileobj.setFolder(FileRoot); 		// Set parent
	
	var id = nlapiSubmitFile(fileobj);  // Save to file cabinet
	
	nlapiLogExecution("DEBUG", "Calling GetFile", filename);	
	// Retrieve new file to ensure consistent output with getfile
	return getFile(filename,id,true,null,null);
}

function getFile(fileName, fileID, noData, md5info, rootPath) {
	var file = null;
	
	nlapiLogExecution("DEBUG", "File Lookup", fileName);
	
	try {
		if(fileName)
			file = nlapiLoadFile(fileID ? fileID : fileName);
	} catch (err) {}
	
	
	nlapiLogExecution("DEBUG", "Got file", file ? "True" : "False");
	
	var res = new Object();
	
	if(file) {
		
		var testMD5 = file.getDescription();
		
		if(testMD5) {
			testMD5 = testMD5.toLowerCase();
		}
		
		if(md5info && testMD5 == md5info) {
			noData = true;
			res.Status = "File Match";
		}
		
		var folderId = file.getFolder();
		
		var path = getFolderPath(folderId);
					
		if(rootPath && rootPath.length > 0) {			
			if(path.toLowerCase().indexOf( rootPath.toLowerCase(), 0) < 0)
				return null;
			//if(path.slice(0,rootPath.length).toLowerCase() != rootPath.toLowerCase())
//				return null;
		}
		
		res.Name = file.getName();
		res.ID = file.getId();
		res.Path = path;	
		res.Url = file.getURL();	
		res.Description = file.getDescription();
		res.Type = file.getType();
		res.Size = file.getSize();
		res.Inactive = file.isInactive();
		res.Online = file.isOnline(); 
		if(!noData && file.getSize() < 5242000 )
			res.Contents = file.getValue();						
	} else {
		res.Name = '';
		res.Path = fileName;
		res.Type = 'NOT FOUND';
	}
	
	return res;
}

function getFolderPath ( folderId) {
	var folder= nlapiLoadRecord("folder", folderId, null);
	
	var parent = folder.getFieldValue("parent");
	var name = folder.getFieldValue("name");
	
	if(!parent) return name;
	
	return getFolderPath(parent) + "/" + name; 
}

function getFolderID(filePath) {
	var filters = new Array(); 
	var columns = new Array(); 	
		
	//filters[0] = new nlobjSearchFilter("istoplevel",null,"is",'T' );
	filters[0] = new nlobjSearchFilter("name",null,"is",filePath );
	
	columns[0] = new nlobjSearchColumn("internalid",null,null);
	columns[1] = new nlobjSearchColumn("numfiles",null,null); 		
	var results = nlapiSearchRecord("folder",null,filters,columns);
	
	return results ? results[0].getValue(columns[0]) : null;
}

function getFolderInfo ( folderID ) {
	if(!folderID) return null;
	
	nlapiLogExecution("DEBUG", "Folder Lookup", folderID);
	
	var folder = null;
	try {
		folder = nlapiLoadRecord("folder", folderID, null);
	} catch (err) { nlapiLogExecution("DEBUG", "Folder Lookup Failed", folderID + ' ' + err); }
	
	var res = new Object();	
			
	if(folder) {
		
		res.Name = folder.getFieldValue("name");
		nlapiLogExecution("DEBUG", "Folder Name", res.Name);
		res.ID = folderID;
		
		var files = new Array();
		
		var f_filters = new Array(); 
		var f_columns = new Array(); 	
		f_filters[0] = new nlobjSearchFilter("folder",null,"is",folderID );		
		f_columns[0] = new nlobjSearchColumn("internalid",null,null);
		var f_results = nlapiSearchRecord("file",null,f_filters,f_columns);
		
		if(f_results)
			for(var f_res in f_results) {
				var f_id = f_results[f_res].getValue(f_columns[0]);
				var f_file = null;
				try {
					f_file = nlapiLoadFile(f_id);
				} catch (err) {}
														
				files[f_res] = new Object();
				files[f_res].Name = f_file ? f_file.getName() : "NO ACCESS";
				files[f_res].ID = f_id;			
				files[f_res].Folder = f_file ? f_file.getFolder() : '';				
			}
		
		res.Files = files;
		
		f_filters = new Array(); 
		f_columns = new Array(); 	
		f_filters[0] = new nlobjSearchFilter("parent",null,"is",folderID );		
		f_columns[0] = new nlobjSearchColumn("internalid",null,null);
		f_results = nlapiSearchRecord("folder",null,f_filters,f_columns);
		
		var folders = new Array();
		
		if(f_results) {
			nlapiLogExecution("DEBUG", "Folder Children Found", folderID);
			for(var f_res in f_results) {				
				var f_id = f_results[f_res].getValue(f_columns[0]);
				
				nlapiLogExecution("DEBUG", "Folder Child", f_id);
				
				folders[f_res] = f_id;
			}
		}
		
		res.Folders = folders;
	}
	
	return res;
}

function getFolderData(folder) {
	
	var filters = new Array(); 
	var columns = new Array(); 	
	
	if(folder)
		filters[0] = new nlobjSearchFilter("istoplevel",null,"is",'T' );
	else
		filters[0] = new nlobjSearchFilter("name",null,"is",folder );
	
	columns[0] = new nlobjSearchColumn("internalid",null,null);
	columns[1] = new nlobjSearchColumn("numfiles",null,null); 		
	var results = nlapiSearchRecord("folder",null,filters,columns);
	
	var arr = new Array();
	
	if(results)
		for(var res in results) {
			var id = results[res].getValue(columns[0]);
			
			var file = nlapiLoadRecord("folder", id, null);
			arr[res] = new Object();
			arr[res].Name = file.getFieldValue("name");
			arr[res].ID = id;
			arr[res].NumFiles = results[res].getValue(columns[1]);
			
			var f_filters = new Array(); 
			var f_columns = new Array(); 	
			f_filters[0] = new nlobjSearchFilter("folder",null,"is",id );		
			f_columns[0] = new nlobjSearchColumn("internalid",null,null);
			var f_results = nlapiSearchRecord("file",null,f_filters,f_columns);
			
			var f_arr = new Array();
			
			if(f_results)
				for(var f_res in f_results) {
					var f_id = f_results[f_res].getValue(f_columns[0]);
					var f_file = null;
					try {
						f_file = nlapiLoadFile(f_id);
					} catch (err) {}
										
					f_arr[f_res] = new Object();
					f_arr[f_res].Name = f_file ? f_file.getName() : "NO ACCESS";
					f_arr[f_res].ID = f_id;					
				}
			
			arr[res].Files = f_arr;
		}	

	var returnval = new Object();
	returnval.Count = arr.length;
	returnval.Folders = arr;
	
	return returnval;
}

function getNewSince(newSince, rootPath) {
	var f_filters = new Array(); 
	var f_columns = new Array(); 	
	
	// Note - filter is "modified" not "lastmodified"
	f_filters[0] = new nlobjSearchFilter("modified",null,"after",newSince );
	//f_filters[1] = new nlobjSearchFilter("folder",null,"is",folderId );
	f_columns[0] = new nlobjSearchColumn("internalid",null,null);
	f_columns[1] = new nlobjSearchColumn("url",null,null);
	f_columns[2] = new nlobjSearchColumn("name",null,null);
	var f_results = nlapiSearchRecord("file", null,f_filters,f_columns);
	
	var ret = new Object();
	ret.NewSince = newSince;
	ret.FolderName = rootPath;
	
	var count = 0;
	
	if(f_results) {
		var items = new Array();
		for(var res in f_results) {
			//var item = new Object();
			var id = f_results[res].getValue(f_columns[0]);			
			var name = f_results[res].getValue(f_columns[2]);	
			
			nlapiLogExecution("DEBUG", "File Found", "ID: " + id + "  Name: " + name );
			
			var res = new Object();
			res.ID = id;
			res.Name = name;					
			
			//if(fileInfo) {
				items[count] = res; //fileInfo;
				count += 1;
			//}
		}
		ret.Items = items;
	}
	
	ret.RemainingUnits = nlapiGetContext().getRemainingUsage();
	
	return ret;
}



function getCreateFolderID(filePath, parent) {
	var filters = new Array(); 
	var columns = new Array(); 	
	
	var id = null;
		
	//filters[0] = new nlobjSearchFilter("istoplevel",null,"is",'T' );
	filters[0] = new nlobjSearchFilter("name",null,"is",filePath );
	
	if(parent)
		filters[1] = new nlobjSearchFilter("parent",null,"is",parent );
	
	columns[0] = new nlobjSearchColumn("internalid",null,null);
	columns[1] = new nlobjSearchColumn("numfiles",null,null); 		
	var results = nlapiSearchRecord("folder",null,filters,columns);
	
	if(results) {
		id = results[0].getValue(columns[0]) ;
	} else {
		var folder = nlapiCreateRecord("folder", null);
		folder.setFieldValue("name", filePath);
		if(parent)
			folder.setFieldValue("parent", parent);
		
		id = nlapiSubmitRecord(folder, false, true);
	}
	
	return id;
}
