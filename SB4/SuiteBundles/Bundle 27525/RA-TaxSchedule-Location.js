/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 XII 2013     mkaluza
 *
 */

// @import RA-TaxLib.js
// @import RA-Framework.js

function userEventBeforeLoad(type, form, request)
{
	if (nlapiGetContext().getExecutionContext() == 'webservices' || nlapiGetContext().getExecutionContext() == 'bundleinstallation') return;
	nlapiLogExecution('DEBUG', 'Before load', '');
	form.addTab('custpage_tax_groups_tab', 'Tax Configuration');
	
	AddTaxSchedulesConfigurationTab();
	AddZeroTaxItemTab();
}

function AddTaxSchedulesConfigurationTab()
{
	var taxSchedules = GetRecords(recordTaxSchedule, [fieldScheduleLinkId, 'name']);
	var mapping = DeserializeMapping(nlapiGetFieldValue(fieldMapping));
	
	for (var i = 0; i < taxSchedules.length; i++) {
		var taxScheduleId = taxSchedules[i].custrecord_link_taxschedule_id;
		var taxIdValue = GetTaxIdFromMapping(mapping, taxScheduleId);
		var tax = GetRecordById(['taxGroup', 'salestaxitem'], ['internalid', 'itemid', 'rate', 'country'], taxIdValue);
	
		if (tax != null && tax.country == 'CA') {
			tax.rate = FormatCanadianTaxRate(taxIdValue);
		}
		
		taxSchedules[i].taxid = taxIdValue;
		taxSchedules[i].internalid = taxSchedules[i].custrecord_link_taxschedule_id;
		taxSchedules[i].taxrate = tax != null && tax != '' ? (tax.rate.indexOf('%') !== -1 ? tax.rate : tax.rate + '%') : "0.00%";
	}
	// Add a sublist to Tax configuration tab
	var taxConfigurationSublist = form.addSubList('custpage_taxconfiguration_sublist', 'list', 'Tax Schedules Configuration', 'custpage_tax_groups_tab');
 
	taxConfigurationSublist.addField('internalid', 'integer', 'Internal ID');
	taxConfigurationSublist.addField('name', 'text', 'Name');
	taxConfigurationSublist.addField('taxid', 'select', 'Tax Code/Group', 'salestaxitem').setDisplayType('entry');
	if (type == 'view') {
		taxConfigurationSublist.addField('taxrate', 'text', 'Rate');
	}
	taxConfigurationSublist.setLineItemValues(taxSchedules);


	// Hide custrecord_ra_taxgroups_mapping
	// Initial Configuration Tool need to have the field visible to set value in via SuiteTalk. If the field would have visibility set to hidden, SuiteTalk would not be able to set it.
	// Therefore the field is set to visible and hidden in this suitescript.
	var taxField = nlapiGetField(fieldMapping);
	if (nlapiGetContext().getExecutionContext() != 'webservices' && taxField != null) {
		taxField.setVisible(false);
	}
}
	
function AddZeroTaxItemTab()
{
	var locationId = nlapiGetRecordId();
	var mapping = DeserializeMapping(nlapiGetFieldValue(fieldMapping));
	var zeroTaxItem = mapping != null ? mapping.zeroTaxGroup : null;
	var zeroTaxGroups = GetRecords('taxgroup', ['internalid', 'itemid', 'rate', 'country'], [['rate', 'startswith', '0'], 'or', ['unitprice1', 'startswith', '0']]);
	var zeroTaxCodes = GetRecords('salestaxitem', ['internalid', 'itemid', 'rate', 'country'], ['rate', 'startswith', '0']);
	
	var location = GetRecordById(['location'], ['internalid', 'name', 'country'], locationId);
	var taxItems = [];
	taxItems[0] = {};
	taxItems[0].name = 'Default zero tax group/code';
	var taxConfigurationZeroTaxSublist = form.addSubList('custpage_taxconfiguration_zerotax', 'list', 'Default Zero Tax Item', 'custpage_tax_groups_tab');
	taxConfigurationZeroTaxSublist.addField('name', 'text', 'Name');
	var zeroField = taxConfigurationZeroTaxSublist.addField('taxid', 'select', 'Tax Code/Group', null).setDisplayType('entry');
	
	for (var group = 0; group < zeroTaxGroups.length; group++) {
		if (location == null || location.country == zeroTaxGroups[group].country) {
			zeroField.addSelectOption(zeroTaxGroups[group].internalid, zeroTaxGroups[group].itemid, zeroTaxGroups[group].internalid == zeroTaxItem);
		}
	}
	
	for (var code = 0; code < zeroTaxCodes.length; code++) {
		if (location == null || location.country == zeroTaxCodes[code].country) {
			zeroField.addSelectOption(zeroTaxCodes[code].internalid, zeroTaxCodes[code].itemid, zeroTaxCodes[code].internalid == zeroTaxItem);
		}
	}
	taxConfigurationZeroTaxSublist.setLineItemValues(taxItems);
}

function userEventBeforeSubmit(type)
{
	var context = nlapiGetContext().getExecutionContext();
	if (context == 'webservices' || context == 'bundleinstallation' || context == 'userevent') return;
	nlapiLogExecution('DEBUG', 'userEventBeforeSubmit', nlapiGetContext().getExecutionContext());
	
	var locationId = nlapiGetRecordId();
	var location = GetRecordById(['location'], ['internalid', 'name', 'country'], locationId);
	var taxScheduleRecords = GetRecords(recordTaxSchedule, [fieldScheduleLinkId, 'name', 'internalid']);
	var zeroTaxGroup = nlapiGetLineItemValue('custpage_taxconfiguration_zerotax', 'taxid', 1);
	
	var mapping = {};
	mapping.zeroTaxGroup = zeroTaxGroup;
	mapping.taxSchedule = [];
		
	for (var i = 0; i < taxScheduleRecords.length; i++) 
	{
		mapping.taxSchedule[i] = {};
		var linkId = taxScheduleRecords[i].custrecord_link_taxschedule_id;
		var linenum = nlapiFindLineItemValue('custpage_taxconfiguration_sublist', 'internalid', linkId);
		if (linenum > 0) {
			var selectedTaxId = parseInt(nlapiGetLineItemValue('custpage_taxconfiguration_sublist', 'taxid', linenum))
			if (isNaN(selectedTaxId)) {
				selectedTaxId = null;
			}
			var tax = GetRecordById(['taxGroup', 'salestaxitem'], ['internalid', 'itemid', 'rate', 'country'], selectedTaxId);		
			if (tax != null && location != null && tax.country != location.country) {
				throw "Location " + location.name + " is in nexus " + location.country + " but tax " + tax.itemid + " is in nexus " + tax.country + '\n' +
					"Please use correct tax.";
			}
			mapping.taxSchedule[i] = {};
			mapping.taxSchedule[i].taxGroup = selectedTaxId;
			mapping.taxSchedule[i].internalId = linkId;			
		}
 	}
	
	var serializedMapping = SerializeMapping(mapping);
	nlapiSetFieldValue(fieldMapping, serializedMapping);
}

function userEventAfterSubmit(type) 
{
}

	

	 
	    
		


	
		
		



