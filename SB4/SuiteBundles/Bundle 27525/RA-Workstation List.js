/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 Sep 2013     dotocka
 *
 */

/**
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function copytoAllCheckChanged(type, name, linenum){
	var wsCount = nlapiGetLineItemCount('workstation_list');
	if (name == 'copy_to_all') {
		var value = nlapiGetFieldValue(name);		
		for(var i=1; i <= parseInt(wsCount); i++){
			nlapiSetLineItemValue('workstation_list', 'chbx_copyto', i, value);
		}	
	}
	if(name == 'chbx_copyto'){
		var setCopyToAll = 0;
		for(var i=1; i <= parseInt(wsCount); i++){
			var itemValue = nlapiGetLineItemValue('workstation_list', 'chbx_copyto', i);
			
			if(itemValue == 'T'){
				setCopyToAll++;
			}
		}
		if(setCopyToAll == wsCount){
			nlapiSetFieldValue('copy_to_all', 'T', false);
		}
		else{
			nlapiSetFieldValue('copy_to_all', 'F', false);
		}
	}
}
