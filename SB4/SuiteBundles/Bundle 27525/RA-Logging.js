/**
 * Module Description
 * Provides functionality for logging messages into NS
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Jan 2014     dotocka
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function logMessage(dataIn) {
	
	nlapiLogExecution('DEBUG', 'logMessage: Method to log message called');	
	
	var id = parseInt(dataIn.Id, 10);
	if(isNaN(id)){		
		throw nlapiCreateError(inputDataErrorCode, "Provided Workstation Id is not set or the value is not number", true);
	}
	
	var filter = new nlobjSearchFilter('internalidnumber', null, 'equalto', id);
	var result = nlapiSearchRecord('customrecord_ra_workstation', null, filter);
	if(!result){
		throw nlapiCreateError(workstationNotFoundErrorCode, "Workstation with provided Id does not exist in the system", true);
	}
	
	if(!dataIn.Date){
		throw nlapiCreateError(inputDataErrorCode, "Date value is not set", true);
	}
	var dateObject = getValidDateObject(dataIn.Date);
	if(!dateObject){
		throw nlapiCreateError(invalidDateFormatErrorCode, "Date value is not a valid date format - d/m/yyyy hh:mm:ss TimeZone(optional, Olson Value format)", true);
	}
	
	var severity = parseInt(dataIn.Severity, 10);
	if(isNaN(severity)){
		throw nlapiCreateError(inputDataErrorCode, "Severity is not set or value is not a number", true);
	}
	
	if(severity < Critical || severity > Info){
		throw nlapiCreateError("INVALID_SEVERITY", "Severity is not within range (" + Critical + "-" + Info + ")", true);
	}
	
	if(!dataIn.Message){
		throw nlapiCreateError(inputDataErrorCode, "Message value is not set", true);
	}
		
	var nsDate = nlapiDateToString(dateObject.Date, 'datetimetz');
	
	var logRecord = nlapiCreateRecord('customrecord_ra_ws_provision_log_record');
	logRecord.setFieldValue('custrecord_ra_provision_log_ws', id);
	logRecord.setDateTimeValue('custrecord_ra_provision_log_ws_date', nsDate, dateObject.TimeZone);
	logRecord.setFieldValue('custrecord_ra_provision_log_ws_severity', severity);
	logRecord.setFieldValue('custrecord_ra_provision_log_ws_message', dataIn.Message);
	
	nlapiSubmitRecord(logRecord);
	
	nlapiLogExecution('DEBUG', 'logMessage: Method to log message done');
}

/**
 * Checks for expected date format
 * Expected date format is: d/m/yyyy hh:mm:ss TimeZone(optional, Olson Value format)
 * @param {String} dateParam Parameter string - The string representing date to validate
 * @returns {Object} Output object -> JSON object with Date and TimeZone info, if TimeZone info is not present then GMT zone will be used
 */
function getValidDateObject(dateParam){
	var dateJSON = new Object();
	var bits = dateParam.split(' ');
	if(bits.length < 2 || bits.length > 3){
		return null;
	}
	var dateBits = bits[0].split('/');
	var timeBits = bits[1].split(':');
	var d = new Date(dateBits[2], dateBits[1] - 1, dateBits[0], timeBits[0], timeBits[1], timeBits[2]);
	if(d.getFullYear() == dateBits[2] && (d.getMonth() + 1) == dateBits[1] && d.getDate() == Number(dateBits[0]) && d.getHours() == Number(timeBits[0]) && d.getMinutes() == Number(timeBits[1]) && d.getSeconds() == Number(timeBits[2])){
		dateJSON.Date = d;
		dateJSON.TimeZone = getTimeZone(bits);
		return dateJSON;
	}
	return null;
}

function getTimeZone(bits){
	if(bits.length == 3){
		return bits[2];
	}	
	return 'GMT';
}