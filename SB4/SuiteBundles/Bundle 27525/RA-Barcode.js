/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       15 I 2014     mkaluza
 *
 */

/**
 * Disallow user to edit a barcode text in edit mode
 */
function userEventBeforeLoad(type, form, request)
{
	nlapiLogExecution('DEBUG', 'Type', 'type: ' + type);
	
	// Allow custrecord_ra_barcode_field to be editable just in create mode
	if (type == 'create') {
		nlapiGetField('custrecord_ra_barcode_field').setDisplayType('normal');
	} else {
		nlapiGetField('custrecord_ra_barcode_field').setDisplayType('inline');
	}
}

/**
 * Copy the barcode text to the name field, which is required under assign barcode item functionality.
 */
function userEventBeforeSubmit(type){
	nlapiLogExecution('DEBUG', 'Save', 'type: ' + type);
	nlapiSetFieldValue('name', nlapiGetFieldValue('custrecord_ra_barcode_field')); 
}
