function userEventBeforeSubmit(type) {

	var promoId = nlapiGetFieldValue('custrecord_ra_promoitem_promo');
	var itemId = nlapiGetFieldValue('custrecord_ra_promoitem_item');
	var currentId = nlapiGetRecordId();

	if (!itemId)
		return true;
	
	var duplicatedPromoItems = GetRecords('customrecord_ra_promoitem', ['internalId'], [['custrecord_ra_promoitem_item', 'is', itemId], 'and', ['custrecord_ra_promoitem_promo', 'is', promoId]]);
	
	if (duplicatedPromoItems.length > 0) {
	
		for (var i = 0; i < duplicatedPromoItems.length; i++) {

			if (duplicatedPromoItems[i].internalId != currentId) {
				var promo = GetRecords('customrecord_ra_promo', [['internalId', 'is', promoId]], ['name']);
				var promoName = promo.length > 0 ? promo[0].name : "";

				var item = GetRecords('item', [['internalidnumber', 'equalTo', itemId]], ['name']);
				var itemName = item.length > 0 ? item[0].name : "";
			
				throw ('Promotion ' + promoName + ' already has an item ' + itemName + '!');
			}
		}
	}
	return true;
}