function postPerformSearch(datain) {
    // datain should be JSON:
    // {"type":"SSSSSS","searchId":"##### or SSSSSS","filters":[{"name":"SSSSSS","join":"SSSSSS optional","operator":"SSSSSS","value":"SSSSSS"}]}

    

    var filters = new Array();
    for (var i = 0; datain.filters != null && i < datain.filters.length; i++) {
        nlapiLogExecution("DEBUG", "name", datain.filters[i].name);
        nlapiLogExecution("DEBUG", "operator", datain.filters[i].operator);
        nlapiLogExecution("DEBUG", "value", datain.filters[i].value);
        nlapiLogExecution("DEBUG", "join", datain.filters[i].join);
        filters[i] = new nlobjSearchFilter(datain.filters[i].name, datain.filters[i].join ? datain.filters[i].join : null, datain.filters[i].operator, datain.filters[i].value, null);
    }

    var searchResults = nlapiSearchRecord(null, datain.searchId, filters, null);
    //return searchResults;

    var results = "<Results>";

    for (var i = 0; searchResults != null && i < searchResults.length; i++) {
        var columns = searchResults[i].getAllColumns();
        results += "<Result>";
        for (var j = 0; j < columns.length; j++) {
            var join = columns[j].getJoin() ? columns[j].getJoin() : null;
            var nodeName = (join ? join + "_" : "") + columns[j].getName();

            results += "<" + nodeName + ">";
            results += searchResults[i].getValue(columns[j].getName(), join);
            results += "</" + nodeName + ">";
        }
        results += "</Result>";
    }

    results += "</Results>";

    return results;
}