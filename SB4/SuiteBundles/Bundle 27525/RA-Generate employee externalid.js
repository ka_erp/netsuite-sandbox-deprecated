function generateExternalId () {
    var id = nlapiGetRecordId ();
    var ty = nlapiGetRecordType ();
    var externalId = nlapiGetFieldValue ( "externalid" );
    if (externalId == null || externalId == "") {
       var record = nlapiLoadRecord ( ty, id );

       if ( ty == "employee" ) {
           externalId = "EM" + id;
       } else if ( ty == "customer" ) {
           externalId = "CU" + id;
       }

       record.setFieldValue ( "externalid", externalId );
       nlapiSubmitRecord ( record, false );
       nlapiLogExecution ( "debug", "External ID", externalId );
       //nlapiSetFieldValue ( "externalid", "EM" + id, false );
    }
}