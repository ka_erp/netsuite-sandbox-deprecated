/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       19.08.2015      aharutyunyan
 *
 */

function userEventBeforeSubmit(type)
{
    if ((type != 'create' && type != 'edit') || nlapiGetContext().getExecutionContext() != 'webservices') {
        return;
	}
    nlapiLogExecution('DEBUG', 'Sales Order', 'Is invoked.');

    /* 345648: Removing payment information from Sales Orders that are created in NSPOS */
    var externalId = nlapiGetFieldValue('externalid');

	if(externalId && externalId.length == 32 && /^\d+$/.test(externalId )) {
	
		nlapiSetFieldValue('paymentmethod', null);
		nlapiSetFieldValue('paymethtype', null);
		nlapiSetFieldValue('ispaymethcc', null);
		nlapiSetFieldValue('cccardid', null);
		nlapiSetFieldValue('ccexpiredate', null);
		nlapiSetFieldValue('cchold', null);
		nlapiSetFieldValue('ccnumber', null);
	}
}