/**
 * Company            Explore Consulting
 * Copyright            2014 Explore Consulting, LLC
 * Type                NetSuite EC_UserEvent_Account
 * Version            1.0.0.0
 **/

function onBeforeLoad(type, form, request) {

}

function onBeforeSubmit(type) {

}

function onAfterSubmit(type) {
    if ( type == 'create' || type == 'edit' )
        updateAccountExternalID();
}

// Function to set the native NS external ID field with the GL Account Number
function updateAccountExternalID()
{
    var recID = nlapiGetRecordId();
    try
    {
        nlapiLogExecution('DEBUG', 'updateAccountExternalID', 'STARTING:  ' + nlapiGetRecordId());
        var acctRecord = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
        var acctNumber = acctRecord.getFieldValue('acctnumber');
        nlapiLogExecution('DEBUG', 'updateAccountExternalID', 'acctNumber:  ' + acctNumber);
        var currentExtID = acctRecord.getFieldValue('externalid');
        nlapiLogExecution('DEBUG', 'updateAccountExternalID', 'currentExtID:  ' + currentExtID);
        if ( acctNumber && currentExtID != acctNumber )
        {
            acctRecord.setFieldValue('externalid', acctNumber);
            nlapiLogExecution('DEBUG', 'updateAccountExternalID', 'externalid field set');
            var id = nlapiSubmitRecord(acctRecord, true, true);
            nlapiLogExecution('DEBUG', 'updateAccountExternalID', 'Customer Record Re-submitted:  ' + id);
        }
    }
    catch(e)
    {
        nlapiLogExecution('DEBUG', 'updateCustomerExternalID', 'Unexpected Error occurred while processing:  ' + e);
        var emailBody = "Script:  EC_UserEvent_Account\nFunction:  updateCustomerExternalID\nRecord ID:  " + recID + "\nDetails:\n" + e;
        nlapiSendEmail(-5, "jasperwireless@exploreconsulting.com", "Jasper Wireless - EC_UserEvent_Account", emailBody);
    }
}
