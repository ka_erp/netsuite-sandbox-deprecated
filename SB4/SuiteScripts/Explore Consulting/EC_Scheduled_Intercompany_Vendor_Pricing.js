/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_Scheduled_Intercompany_Vendor_Pricing
 * Version            1.0.0.0
 **/


if ( nlapiGetContext().getExecutionContext() == "PRODUCTION" )
    var SendErrorEmailsFrom = 125247;
else
    var SendErrorEmailsFrom = -5;


var recipient = 'kasupport@exploreconsulting.com';
var startTime = moment();
var maxEndTime = moment(startTime).add(50, "minutes");
var GovernanceThreshold = 100;

var InterCompanyVendorSearchID = "97";      // Search:  InterCompany Vendor General View search (97)
var InterCompanyVendors = [];


function onStart() {
    EC.CheckForIntercompanyVendorPricing();
}

EC.GetInterCompanyVendorsCollection = function(){

    InterCompanyVendors = nlapiSearchRecord("vendor", InterCompanyVendorSearchID);
    Log.d("InterCompany Vendors Collection", InterCompanyVendors);
};

/**
 * Function that will process through Item Receipt line items and see if any items were received for the Canada Retail Warehouse (7) location.
 * If so, the Intercompany Vendor Pricing will be updated for those items.
 * @constructor
 */
EC.CheckForIntercompanyVendorPricing = function(){

    // Pre-load InterCompany Vendors data
    EC.GetInterCompanyVendorsCollection();

    // Pull the script parameters and validate them for processing
    var context = nlapiGetContext();
    var recordID = context.getSetting("SCRIPT", "custscript_record_id");
    var startingLine = context.getSetting("SCRIPT", "custscript_starting_line_number");
    Log.d('UpdateInterCompanyVendorPricing', 'Script Parameter Check - Record ID:  ' + recordID);
    Log.d('UpdateInterCompanyVendorPricing', 'Script Parameter Check - Starting Line To Process:  ' + startingLine);

    if ( !recordID )
        throw nlapiCreateError("INVALID_SCRIPT_PARAMETER", "No Record ID found in script parameter", false);

    if ( !startingLine || startingLine == 0 )
        startingLine = 1;
    var timesUp = false;

    // Open this record and go through the line items.
    var recordProperties = ["id", "subsidiary", "entity"];
    var recordListProperties = ["item", "itemtype", "location", "quantity"];
    var itemReceipt = nsdal.loadObject("itemreceipt", recordID, recordProperties)
        .withSublist("item", recordListProperties);

    if (itemReceipt)
    {
        for ( var i=startingLine; i<=itemReceipt.item.length && !timesUp; i++ ) {
            var lineNumber = i;
            var itemID = itemReceipt.getLineItemValue("item", "item", i);
            var itemLocation = itemReceipt.getLineItemValue("item", "location", i);
            var type = itemReceipt.getLineItemValue("item", "itemtype", i);
            Log.d('Processing ITEM RECEIPT Line ' + lineNumber, 'Item:  ' + itemID + '  Location:  ' + itemLocation);

            // Only the Canada Retail Warehouse location (7) needs further processing.
            if (itemLocation == 7 && itemID) {
                var itemType = getItemType(type);
                EC.LoadAndProcessItem(itemID, itemType);
            }

            timesUp = EC.RunTimeScriptCheckers();
        }

        if ( timesUp )
        {
            Log.d('Scheduling Script to continue updating');
            var params = {
                custscript_record_id: recordID,
                custscript_starting_line_number: i
            };
            var result = nlapiScheduleScript('customscript_ec_sch_intercmpy_vndr_prcng', null, params);                  // 20 units
            Log.d('Scheduling Script RESULT:  ' + result);
            if (result != 'QUEUED') {
                Log.d('scheduled script not queued',
                    'expected QUEUED but scheduled script api returned ' + result);
                var emailBody5 = "Script: EC_Scheduled_Intercompany_Vendor_Pricing.js\nFunction: UpdateInterCompanyVendorPricing\nError: There was an issue re-scheduling the scheduled script in this script";
                nlapiSendEmail(SendErrorEmailsFrom, recipient, "Error in EC_Scheduled_Intercompany_Vendor_Pricing.js", emailBody5);
            }
            else {
                Log.d('UpdateInterCompanyVendorPricing -- Re-Scheduling Script Status', "Result:  " + result);
            }
        }
    }
    else
    {
        Log.d("CheckForIntercompanyVendorPricing", "NO Item Receipt record could be loaded");
    }
};

EC.RunTimeScriptCheckers = function(){

    return nlapiGetContext().getRemainingUsage() < GovernanceThreshold || moment().isAfter(maxEndTime);
};

EC.LoadAndProcessItem = function(itemid, itemtype){

    var itemRecord = nsdal.loadObject(itemtype, itemid, ["id", "type", "currency"])
        .withSublist("itemvendor", ["vendor", "purchaseprice"])
        .withSublist("locations", ["locationid", "averagecostmli"]);
    if ( itemRecord )
    {
        // If the Primary Currency of the Vendor is USD, then we will need to convert the Average Cost accordingly.
        var cost = EC.GetItemAverageCost(itemRecord);
        Log.d('Average Cost pulled from Item Record', cost);

        if ( cost ) {

            var lc = itemRecord.getLineItemCount("itemvendor");
            Log.d('Line Count of Current Item Vendors', lc);
            // Go through the Vendor Listing on this Item and remove any Intercompany Vendors already found there
            for ( var i=lc; i>0; i-- )
            {
                var lineNumber = parseInt(i);
                Log.d('Processing Line ' + lineNumber, 'Item Vendor:  ' + itemRecord.itemvendor[i-1].vendor);
                if (EC.IsVendorAnInterCompanyVendor(itemRecord.itemvendor[i-1].vendor)) itemRecord.removeLineItem("itemvendor", lineNumber);
            }
            Log.d('Line Count after clearing out InterCompany Vendors', itemRecord.getLineItemCount("itemvendor"));

            // Now add Vendor Pricing for all found InterCompany Vendors
            _.each(InterCompanyVendors, function (vendorDetails, index) {

                var line = parseInt(index) + 1;
                Log.d('Adding InterCompany Vendor ' + line, 'Vendor ID:  ' + vendorDetails.getId());
                var convertedCost = EC.ConvertToVendorCurrency(cost, vendorDetails.getText("currency"));
                Log.d('Converting average cost to Vendor Currency', 'convertedCost:  ' + convertedCost);

                // Get Vendor's Markup Percentage and add it to the currently calculated cost
                var markUpPercentage = vendorDetails.getValue("custentity_intercompany_markup_percent");
                Log.d('Vendor Markup Percentage', 'markUpPercentage:  ' + markUpPercentage);
                if (markUpPercentage) {
                    markUpPercentage = markUpPercentage.replace("%", "");
                    markUpPercentage = markUpPercentage/100;
                    var markedUpCost = (parseFloat(convertedCost) * parseFloat(markUpPercentage)) + parseFloat(convertedCost);
                    Log.d('Marked up Vendor Cost', 'markedUpCost:  ' + markedUpCost);

                    itemRecord.selectNewLineItem("itemvendor");
                    itemRecord.setCurrentLineItemValue("itemvendor", "vendor", vendorDetails.getId());
                    itemRecord.setCurrentLineItemValue("itemvendor", "purchaseprice", markedUpCost);
                    itemRecord.commitLineItem("itemvendor");
                }
                else
                {
                    Log.d('No Markup Percentage found on Vendor Record', 'Setting Vendor Price to ConvertedCost:  ' + convertedCost);
                    itemRecord.selectNewLineItem("itemvendor");
                    itemRecord.setCurrentLineItemValue("itemvendor", "vendor", vendorDetails.getId());
                    itemRecord.setCurrentLineItemValue("itemvendor", "purchaseprice", convertedCost);
                    itemRecord.commitLineItem("itemvendor");
                }
            });
            Log.d('Line Count after Adding Vendor Pricing', itemRecord.getLineItemCount("itemvendor"));

            itemRecord.setFieldValue('custitem_ec_intercompany_price_updated', nlapiDateToString(new Date(), "datetimetz"));
            var id = itemRecord.save(true, true);
            Log.d("ProcessItemVendors", "Item Record Re-saved:  " + id);
        }
    }
    else
    {
        Log.d("ProcessItemVendors", "Could not load Item Record");
    }
};

EC.ConvertToVendorCurrency = function(CAD_cost, vendorCurrency){

    // Pull the USD exchange rate as of today's date
    if (vendorCurrency != 'CAD') {
        var rate = nlapiExchangeRate('CAD', vendorCurrency, nlapiDateToString(new Date()));
        return parseFloat(CAD_cost) * rate;
    }
    else
        return parseFloat(CAD_cost);
};

EC.IsVendorAnInterCompanyVendor = function(vendor){

    if (!vendor) return false;
    var vendorFound = _.find(InterCompanyVendors, function(result){ return result.getId() == vendor; });
    if ( vendorFound ) {
        var representsSub = vendorFound.getValue("representingsubsidiary");
        if ( representsSub )
            return true;
        else
            return false;
    }
    else
        return false;
};

EC.GetItemAverageCost = function(itemRecord){

    var averageCost = 0.00;
    if ( itemRecord )
    {
        _.each(itemRecord.locations, function(locLine, index)
        {
            //Log.d('Processing Locations Line ' + index+1, 'Location:  ' + locLine.locationid);
            if ( locLine.locationid == 7 ){
                averageCost = locLine.averagecostmli;
                return;
            }
        });
    }
    return averageCost;
};


Log.AutoLogMethodEntryExit(null, true, true, true);

