/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_UE_Slt_TOHeaderStagingRecord_ClearDetailErrorsButton
 * Version            1.0.0.0
 * Description          This script will add a custom button to the TO Header Staging Record form and then execute
 *                          functionality to go through all of it's TO Line Staging Records and clear errors on them.
 **/

var ScriptName = "EC_UE_Slt_TOHeaderStagingRecord_ClearDetailErrorsButton";

// Function to add the button to the record form
// Deployed as a User Event, on Before Load
function addButton(type, form, request)
{
    try
    {
        // Only show if the Record is in View Mode			// Change as needed
        if ( type != 'view' ) return;

        // Add button to form
        var id = nlapiGetRecordId();
        var rectype = nlapiGetRecordType();

        // The button script will be controlled via a suitelet.  See Suitelet code below.
        var url = nlapiResolveURL('SUITELET','customscript_ec_slt_clearerror_func','customdeploy_ec_slt_clearerror_func') + '&transactionRecord=' + id + '&transactionType=' + rectype;

        form.addButton('custpage_clear_detail_errors','Clear Detail Errors', "document.location='"+url+"'");

    }
    catch(e)
    {
        Log.d("addButton", "Unexpected Error:  " + e);
    }
}



/////////////////////////////   SUITELET FUNCTIONALITY    /////////////////////////////////////
/////////////////////////////    TRIGGERED BY BUTTON      /////////////////////////////////////


/*
 *  Function triggered by custom Button on record form
 */
function buttonFunctionality(request, response)
{
    var transID = request.getParameter( 'transactionRecord' );
    var transType = request.getParameter( 'transactionType' );

    try
    {
        if ( !transID || !transType )
            throw "Parameters missing.";

        // Since this functionality could use alot of governance - we will just send this directly off to a scheduled script
        Log.d('Scheduling Script to continue updating');
        var param = { custscript_to_rec_id: transID };
        var result = nlapiScheduleScript('customscript_ec_sch_clear_all_errors', null, param);                  // 20 units
        Log.d('Scheduling Script RESULT:  ' + result);
        if (result != 'QUEUED') {
            Log.d('scheduled script not queued',
                'expected QUEUED but scheduled script api returned ' + result);
            var emailBody5 = "Script: EC_UE_Slt_TOHeaderStagingRecord_ClearDetailErrorsButton.js\nFunction: buttonFunctionality\nError: There was an issue re-scheduling the scheduled script in this script";
            EC.SendEmailAlert("Error: There was an issue re-scheduling the scheduled script in this script.\nHeader Staging Record:  " + transID, ScriptName);
        }
        else {
            Log.d('buttonFunctionality -- Re-Scheduling Script Status', "Result:  " + result);
        }
    }
    catch(e)
    {
        Log.d("buttonFunctionality ", "Unexpected Error:  " + e);
    }

    // After the button's functionality is complete, we will want to route the user back to the original record's form
    nlapiSetRedirectURL("RECORD", transType, transID, null, null);
}

/////////////////////////////   SCHEDULED SCRIPT FUNCTIONALITY            /////////////////////////////////////
/////////////////////////////    USED TO ENSURE GOVERNANCE WILL BE OK      /////////////////////////////////////

function updateTOLineRecords(){

    // Pull the script parameters and validate them for processing
    var context = nlapiGetContext();
    var recordID = context.getSetting("SCRIPT", "custscript_to_rec_id");
    Log.d('updateTOLineRecords', 'Script Parameter Check - TO Header Staging Record ID:  ' + recordID);

    try{

        if ( !recordID ) throw nlapiCreateError("INVALID_PARAMETER", "NO Record ID parameter found.\nRecord ID:  " + recordID);

        // Get TO Line Staging Records Connected to this TO Header Staging Record
        // This search will find any TO Line Staging Records, connected to the given TO Header Staging Record,
        //      that have a status of Error-Reprocess.  This script will update their status to To Be Processed.
        var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"),
            new nlobjSearchFilter("custrecord_transfer_stage_header", null, "is", recordID),
            new nlobjSearchFilter("custrecord_transfer_stage_line_status", null, "anyof", [TO_LineStagingStatus.ErrorReview, TO_LineStagingStatus.ErrorReprocess])];
        var columns = [new nlobjSearchColumn("internalid")];
        var results = nlapiSearchRecord("customrecord_transfer_order_staging_line", null, filters, columns);
        var currentID;
        var rerun = false;
        var timesUp = false;

        if ( results ) {
            Log.d("updateTOLineRecords", "Number of TO Line Search Results:  " + results.length);

            if ( results.length == 1000 )
                rerun = true;

            for (var i=0; i < results.length && !timesUp; i++) {

                currentID = results[i].getId();
                Log.d("Processing TO Line Staging records - result #" + i, "Current Record ID:  " + currentID);

                // Set the status of the TO Line Staging Record to To Be Processed - meaning it is ready for transfer in NetSuite
                nlapiSubmitField("customrecord_transfer_order_staging_line", currentID, "custrecord_transfer_stage_line_status", TO_LineStagingStatus.ToBeProcessed);

                timesUp = EC.RunTimeScriptCheckers(Script_GovernanceThreshold);
            }

            if ( timesUp || rerun)
            {
                Log.d('Scheduling Script to continue updating');
                var param = { custscript_to_rec_id: recordID };
                var result = nlapiScheduleScript('customscript_ec_sch_clear_all_errors', null, param);                  // 20 units
                Log.d('Scheduling Script RESULT:  ' + result);
                if (result != 'QUEUED') {
                    Log.d('scheduled script not queued',
                        'expected QUEUED but scheduled script api returned ' + result);
                    var emailBody5 = "Script: EC_UE_Slt_TOHeaderStagingRecord_ClearDetailErrorsButton.js\nFunction: updateTOLineRecords\nError: There was an issue re-scheduling the scheduled script in this script";
                    EC.SendEmailAlert("Error: There was an issue re-scheduling the scheduled script in this script.", ScriptName);
                }
                else {
                    Log.d('updateTOLineRecords -- Re-Scheduling Script Status', "Result:  " + result);
                }
            }
        }
    }
    catch(e)
    {
        Log.d("updateTOLineRecords - " + recordID, "Error was throwing during processing:  " + e);
        var fields = ["custrecord_tranfer_stage_status", "custrecord_transfer_error_message"];
        var values = [TO_HeaderStagingStatus.Error_ToBeReviewed, "Error thrown while clearing errors on TO Line Recs for TO Header Record ID " + recordID +  "\n" + e];
        nlapiSubmitField("customrecord_tran_order_staging_header", recordID, fields, values);
        EC.SendEmailAlert(e, ScriptName);
    }
}

Log.AutoLogMethodEntryExit(null, true, true, true);
