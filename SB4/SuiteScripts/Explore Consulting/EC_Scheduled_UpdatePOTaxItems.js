/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_Scheduled_UpdatePOTaxItems
 * Version            1.0.0.0
 **/

var startTime = moment();
var maxEndTime = moment(startTime).add(50, "minutes");
var GovernanceThreshold = 100;
var PO_SEARCH = 504;


function onStart() {
    EC.UpdatePOs();
}

EC.GetPOsToBeUpdated = function(){
    return nlapiSearchRecord("transaction", PO_SEARCH);
};


EC.UpdatePOs = function(){

    try{

        var purchaseOrders = EC.GetPOsToBeUpdated();

        var timesUp = false;
        var currentID;

        if ( purchaseOrders ) {

            Log.d("UpdatePOs", "Number of POs to Update:  " + purchaseOrders.length);

            for (var i=0; i < purchaseOrders.length && !timesUp; i++) {
            //for (var i=0; i < 5 && !timesUp; i++) {

                currentID = purchaseOrders[i].getValue("internalid", null, "GROUP");
                Log.d("Processing PO records - result #" + i, "Current PO ID:  " + currentID);

                try {
                    // Open the PO for processing
                    var PO = nlapiLoadRecord("purchaseorder", currentID);

                    if (PO) {
                        // Go through the line items to update them
                        var lc = PO.getLineItemCount("item");
                        for (var x = 1; x <= lc; x++) {
                            var currentTaxCode = PO.getLineItemValue("item", "taxcode", x);
                            if (currentTaxCode == 11)
                                PO.setLineItemValue("item", "taxcode", x, 3169);
                        }

                        PO.setFieldValue("custbody_temp_updated", "T");
                        var id = nlapiSubmitRecord(PO, true, true);
                        Log.d("Processing PO records  " + i, "PO Re-saved:  " + id);
                    }
                    else {
                        Log.d("UpdatePOs", "Issue loading PO record:  " + currentID);
                    }

                    timesUp = EC.RunTimeScriptCheckers();
                }
                catch(e){
                    Log.d("UpdatePOs - Error thrown during processing record " + i, "PO Record that errorred:  " + currentID + "\n\nError thrown:  " + e);
                }
            }

            if (timesUp) {
                Log.d('Scheduling Script to continue updating');
                var result = nlapiScheduleScript('customscript_ec_sch_update_po_taxitems', null, null);                  // 20 units
                Log.d('Scheduling Script RESULT:  ' + result);
                if (result != 'QUEUED') {
                    Log.d('scheduled script not queued',
                        'expected QUEUED but scheduled script api returned ' + result);
                    var emailBody5 = "Script: EC_Scheduled_UpdatePOTaxItems.js\nFunction: UpdatePOs\nError: There was an issue re-scheduling the scheduled script in this script";
                    nlapiSendEmail(125247, "rhackey@exploreconsulting.com", "Error in EC_Scheduled_UpdatePOTaxItems.js", emailBody5);
                }
                else {
                    Log.d('UpdatePOs -- Re-Scheduling Script Status', "Result:  " + result);
                }
            }
        }
        else
        {
            Log.d("UpdatePOs", "No search results were found");
        }
    }
    catch(e)
    {
        Log.d("UpdatePOs", "Unexpected Error Occurred:  " + e);
    }
};

EC.RunTimeScriptCheckers = function(){
    return nlapiGetContext().getRemainingUsage() < GovernanceThreshold || moment().isAfter(maxEndTime);
};


Log.AutoLogMethodEntryExit(null, true, true, true);