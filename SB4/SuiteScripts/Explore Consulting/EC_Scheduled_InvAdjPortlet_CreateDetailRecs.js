/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_Scheduled_InvAdjPortlet_CreateDetailRecs
 * Version            1.0.0.0
 * Description          Script that will go through and create the Detail Staging Record for Header Staging Records
 *                          that are awaiting Processing.
 *
 **/


// Inventory Count Search
var INV_COUNT_SEARCH = "customsearch_ec_inventory_count";

var ScriptName = "EC_Scheduled_InvAdjPortlet_CreateDetailRecs";

function onStart() {
    EC.ProcessHeaderStagingRecords();
}


EC.ProcessHeaderStagingRecords = function(){

    try{

        // Get Parent Staging Records for Processing
        var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"), new nlobjSearchFilter("custrecord_header_status", null, "anyof", HeaderStagingStatus.CreateDetailRecords)];
        var columns = [new nlobjSearchColumn("custrecord_header_status"), new nlobjSearchColumn("custrecord_header_adjustment_date"),
            new nlobjSearchColumn("custrecord_header_adjustment_location"), new nlobjSearchColumn("custrecord_header_adjustment_account"),
            new nlobjSearchColumn("custrecord_header_adjustment_dept"), new nlobjSearchColumn("custrecord_header_importer"),
            new nlobjSearchColumn("internalid", "file")];
        var results = nlapiSearchRecord("customrecord_inv_adj_staging_header", null, filters, columns);
        var timesUp = false;
        var currentID;
        var reprocess = false;

        if ( results ) {
            Log.d("ProcessHeaderStagingRecords", "Number of Search Results:  " + results.length);

            for (var i=0; i < results.length && !timesUp && !reprocess; i++) {

                currentID = results[i].getId();
                Log.d("Processing Header Staging records - result #" + i, "Current Record ID:  " + currentID);
                reprocess = false;

                try{
                    // Process this Header Detail Record
                    if ( !results[i].getValue("internalid", "file") ) throw nlapiCreateError("NO_FILE_FOUND", "No File found - Could not Process");
                    var headerRec = nsdal.loadObject("customrecord_inv_adj_staging_header", results[i].getId(), HeaderStagingRecordProperties);
                    headerRec.custrecord_header_status = HeaderStagingStatus.Processing;
                    reprocess = EC.ProcessFile(headerRec, results[i].getValue("internalid", "file"));

                    // If governance limits didn't allow us to complete the processing of the file, we need to rescheduled and start at where we left off
                    if ( reprocess && reprocess.reprocess )
                    {
                        Log.d('Scheduling Script to continue updating for a specific file:  Record ID ' + currentID + '  Starting Line:  ' + reprocess.startLine);
                        headerRec.custrecord_header_error_message = "Rescheduling script to start at line " + reprocess.startLine;
                        var id = headerRec.save(true, true);
                        Log.d("ProcessHeaderStagingRecords", "Header Record Re-saved:  " + id);

                        var params = {
                            custscript_header_record_id: currentID,
                            custscript_stating_line_number: reprocess.startLine
                        };

                        var result = nlapiScheduleScript('customscript_sch_det_recs_partial_file', null, params);                  // 20 units
                        Log.d('Scheduling Script RESULT:  ' + result);
                        if (result != 'QUEUED') {
                            Log.d('scheduled script not queued',
                                'expected QUEUED but scheduled script api returned ' + result);
                            EC.SendEmailAlert("Issue Rescheduling Script for a Partially processed file.\nRecord ID:  " + currentID + "\nStarting Line:  " + reprocess.startLine, ScriptName);
                        }
                        else {
                            Log.d('ProcessHeaderStagingRecords -- Re-Scheduling Script Status', "Result:  " + result);
                        }
                    }
                    else
                    {
                        headerRec.custrecord_header_status = HeaderStagingStatus.PendingApproval;
                        headerRec.custrecord_header_error_message = "";
                        var id = headerRec.save(true, true);
                        Log.d("ProcessHeaderStagingRecords", "Header Record Re-saved:  " + id);
                    }
                }
                catch(e)
                {
                    Log.d("ProcessHeaderStagingRecords - " + currentID, "Error was throwing during processing:  " + e);
                    var fields = ["custrecord_header_status", "custrecord_header_error_message"];
                    var values = [HeaderStagingStatus.Error, e];
                    nlapiSubmitField("customrecord_inv_adj_staging_header", currentID, fields, values);
                    EC.SendEmailAlert(e, ScriptName);
                }

                timesUp = EC.RunTimeScriptCheckers(Script_GovernanceThreshold);
            }
        }
        else
        {
            Log.d("ProcessHeaderStagingRecords", "No Header Staging Records are ready for Processing");
        }

        // TODO:  Update function with deployment info
        if ( timesUp && !reprocess )
        {
            Log.d('Scheduling Script to continue updating');
            var result = nlapiScheduleScript('customscript_ec_sch_create_detail_recs', null, null);                  // 20 units
            Log.d('Scheduling Script RESULT:  ' + result);
            if (result != 'QUEUED') {
                Log.d('scheduled script not queued',
                    'expected QUEUED but scheduled script api returned ' + result);
                var emailBody5 = "Script: EC_Scheduled_InvAdjPortlet_CreateDetailRecs.js\nFunction: ProcessHeaderStagingRecords\nError: There was an issue re-scheduling the scheduled script in this script";
                EC.SendEmailAlert("Error: There was an issue re-scheduling the scheduled script in this script.", ScriptName);
            }
            else {
                Log.d('ProcessHeaderStagingRecords -- Re-Scheduling Script Status', "Result:  " + result);
            }
        }
    }
    catch(e)
    {
        Log.d("ProcessHeaderStagingRecords", "Unexpected Error:  " + e);
        EC.SendEmailAlert(e, ScriptName);
    }
};

/**
 *
 * @param headerRec
 * @param fileID
 * @param startingLine {integer} - (optional) this indicates the file in the file that we should start processing with.
 * @returns {*}
 * @constructor
 */
EC.ProcessFile = function(headerRec, fileID, startingLine){

    if ( startingLine )
        var start = parseInt(startingLine);
    else
        var start = 1;

    var result = {reprocess:false, startLine:start};
    try{

        // Open File and process it's contents
        var fileObj = nlapiLoadFile(fileID);
        var fileContents = fileObj.getValue();
        var stopProcessing = false;

        // Parse the file's data
        var rows = CSV.parse(fileContents);                  // CSV.js library file required
        Log.d("ProcessFile - Parsed Data Object", rows);

        // For each row of data - create a connected Staging Record custom record
        // Start with Line 1 of the file to skip the Header Row
        for ( var p=parseInt(start); p<rows.length && !stopProcessing; p++ ){
            // Need to ensure governance will allow us to continue with this file
            stopProcessing = EC.RunTimeScriptCheckers(File_GovernanceThreshold);
            if (!stopProcessing)
                EC.MapPortletInputToStagingRecord(headerRec, rows[p], p+1);
            else {
                //Reschedule and start with this line number
                return { reprocess:true, startLine:p };
            }
        }
    }
    catch(e)
    {
        Log.d("ProcessFile", "Unexpected Error:  " + e);
        throw e;
    }
    return result;
};


EC.MapPortletInputToStagingRecord = function(headerRec, rowData, rowLine){

    try{

        Log.d("MapPortletInputToStagingRecord - rowData CHECK", rowData);
        Log.d("MapPortletInputToStagingRecord - rowData CHECK", "UPC:  " + rowData[0]);         // column 1
        Log.d("MapPortletInputToStagingRecord - rowData CHECK", "Item Name:  " + rowData[1]);   // column 2
        //Log.d("MapPortletInputToStagingRecord - rowData CHECK", "Item ID:  " + rowData[2]);   //
        Log.d("MapPortletInputToStagingRecord - rowData CHECK", "Descr:  " + rowData[2]);       // column 3
        Log.d("MapPortletInputToStagingRecord - rowData CHECK", "Qty:  " + rowData[3]);         // column 4

        var stagingRecord = nsdal.createObject("customrecord_inv_adj_stage", DetailStagingRecordProperties);
        stagingRecord.custrecord_inv_adj_status = DetailStagingStatus.WaitingApproval;
        stagingRecord.custrecord_header_staging_record = headerRec.id;

        // Need to do a lookup to find the Item ID using the UPC Code
        if ( rowData[0] ){
            stagingRecord.custrecord_inv_adj_upc_code = rowData[0];
            var itemIntID = EC.FindItemIDByUPC(rowData[0]);
            if ( itemIntID ){
                stagingRecord.custrecord_inv_adj_number = itemIntID;
            } else {
                stagingRecord.custrecord_inv_adj_status = DetailStagingStatus.ErrorReview;
                stagingRecord.custrecord_inv_adj_error = "Missing NS Item - UPC Code could not be found in system.  Calculated Adjustment Quantity will also need to be reviewed.";
            }
        }

        stagingRecord.custrecord_inv_adj_location = headerRec.custrecord_header_adjustment_location;
        headerRec.subsidiary = EC.FindSubsidiary(headerRec.custrecord_header_adjustment_location);
        stagingRecord.custrecord_inv_adj_subsidiary = headerRec.subsidiary;
        stagingRecord.custrecord_inv_adj_account = headerRec.custrecord_header_adjustment_account;
        stagingRecord.custrecord_inv_adj_date = headerRec.getFieldValue('custrecord_header_adjustment_date');
        stagingRecord.custrecord_inv_adj_department = headerRec.custrecord_header_adjustment_dept;
        stagingRecord.custrecord_inv_adj_class = "";        // For now, we will not be doing anything with Class - client doesn't want to populate
        stagingRecord.custrecord_inv_adj_quantity_on_hand = rowData[3];
        if ( stagingRecord.custrecord_inv_adj_number )
            stagingRecord.custrecord_inv_adj_adjust_quantity = EC.AdjustmentCalculation(stagingRecord.custrecord_inv_adj_number, rowData[3], headerRec, stagingRecord);
        stagingRecord.custrecord_detail_line_number = rowLine;

        var newRecordID = stagingRecord.save(true, true);
        Log.d("MapPortletInputToStagingRecord", "New Staging Record Created:  " + newRecordID);
    }
    catch(e)
    {
        Log.d("MapPortletInputToStagingRecord", "Unexpected Error while creating staging record:  " + e);

        // If the creation of this record fails - let's still create a placeholder record to alert to the error that occurred
        var stagingRecord = nsdal.createObject("customrecord_inv_adj_stage", DetailStagingRecordProperties);
        stagingRecord.custrecord_inv_adj_status = DetailStagingStatus.ErrorReview;
        stagingRecord.custrecord_header_staging_record = headerRec.id;
        stagingRecord.custrecord_detail_line_number = rowLine;
        stagingRecord.custrecord_inv_adj_error = e;
        var newRecordID = stagingRecord.save(true, true);
        Log.d("MapPortletInputToStagingRecord - ERROR CREATION", "New Staging Record Created:  " + newRecordID);

        EC.SendEmailAlert(e);
    }
};

EC.FindSubsidiary = function(location){

    var locRecord = nlapiLoadRecord("location", location);
    var sub = locRecord.getFieldValue("subsidiary");
    if ( sub ) return sub;
    else return "";
};


EC.AdjustmentCalculation = function(itemid, countedQty, headerRec, detailRec){

    // Need to find the difference between the Counted Quantity (taken from imported file) and the system quantity on the given spreadsheet date.
    Log.d("AdjustmentCalculation", "itemid:  " + itemid);
    Log.d("AdjustmentCalculation", "countedQty:  " + countedQty);
    Log.d("AdjustmentCalculation", "Sub:  " + headerRec.subsidiary + "\nLocation:  " + headerRec.custrecord_header_adjustment_location + "\nTran Date:  " + headerRec.getFieldValue('custrecord_header_adjustment_date'));
    var filters = [new nlobjSearchFilter("trandate", null, "onorbefore", headerRec.getFieldValue('custrecord_header_adjustment_date')),
        new nlobjSearchFilter("item", null, "anyof", parseInt(itemid)),
        new nlobjSearchFilter("location", null, "anyof", headerRec.custrecord_header_adjustment_location),
        new nlobjSearchFilter("subsidiary", null, "anyof", headerRec.subsidiary)];

    //if ( headerRec.custrecord_header_adjustment_dept ) filters.push(new nlobjSearchFilter("department", null, "anyof", headerRec.custrecord_header_adjustment_dept));

    var count = nlapiSearchRecord("transaction", INV_COUNT_SEARCH, filters);

    if ( !count ){
        // If we don't find this item in the system, thus not in the search results, then we will use the
        //      Spreadsheet count for the full Adjustment Quantity
        Log.d("AdjustmentCalculation", "NO SEARCH RESULTS FOUND - use the spreadsheet qty:  " + countedQty);
        detailRec.custrecord_inv_adj_search_qty = countedQty;
        return countedQty;
    }
    else
    {
        // Calculate the adjustment quantity
        //var columns = count[0].getAllColumns();
        //var systemCount = count[0].getValue(columns[8]);
        var systemCount = count[0].getValue('quantity', null, 'SUM');
        detailRec.custrecord_inv_adj_search_qty = systemCount;
        Log.d("AdjustmentCalculation", "systemCount:  " + systemCount);
        return parseFloat(countedQty) - parseFloat(systemCount);
    }
};

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

function onStart2() {
    EC.ProcessHeaderStagingRecords_PartialFile();
}

EC.ProcessHeaderStagingRecords_PartialFile = function(){

    try{

        // Pull the script parameters and validate them for processing
        var context = nlapiGetContext();
        var recordID = context.getSetting("SCRIPT", "custscript_header_record_id");
        Log.d('ProcessHeaderStagingRecords_PartialFile', 'Script Parameter Check - Record ID:  ' + recordID);
        var startingLine = context.getSetting("SCRIPT", "custscript_stating_line_number");
        Log.d('ProcessHeaderStagingRecords_PartialFile', 'Script Parameter Check - Starting Line #:  ' + startingLine);

        if ( !recordID || !startingLine ) throw nlapiCreateError("INVALID_PARAMETER", "Either Record ID or Starting Line parameter is missing.\nRecord ID:  " + recordID + "\nStarting Line:  " + startingLine);

        // Get Parent Staging Records for Processing
        var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"), new nlobjSearchFilter("internalid", null, "is", recordID)];
        var columns = [new nlobjSearchColumn("custrecord_header_status"), new nlobjSearchColumn("custrecord_header_adjustment_date"),
            new nlobjSearchColumn("custrecord_header_adjustment_location"), new nlobjSearchColumn("custrecord_header_adjustment_account"),
            new nlobjSearchColumn("custrecord_header_adjustment_dept"), new nlobjSearchColumn("custrecord_header_importer"),
            new nlobjSearchColumn("internalid", "file")];
        var results = nlapiSearchRecord("customrecord_inv_adj_staging_header", null, filters, columns);
        var currentID;
        var reprocess = false;

        if ( results ) {
            Log.d("ProcessHeaderStagingRecords_PartialFile", "Number of Search Results (should be only one):  " + results.length);

            currentID = results[0].getId();
            Log.d("Processing Header Staging records - result #" + 0, "Current Record ID:  " + currentID);
            reprocess = false;

            try{
                // Process this Header Detail Record
                if ( !results[0].getValue("internalid", "file") ) throw nlapiCreateError("NO_FILE_FOUND", "No File found - Could not Process");
                var headerRec = nsdal.loadObject("customrecord_inv_adj_staging_header", results[0].getId(), HeaderStagingRecordProperties);
                headerRec.custrecord_header_status = HeaderStagingStatus.Processing;
                reprocess = EC.ProcessFile(headerRec, results[0].getValue("internalid", "file"), startingLine);

                // If governance limits didn't allow us to complete the processing of the file, we need to rescheduled and start at where we left off
                if ( reprocess && reprocess.reprocess )
                {
                    Log.d('Scheduling Script to continue updating for a specific file:  Record ID ' + currentID + '  Starting Line:  ' + reprocess.startLine);
                    headerRec.custrecord_header_error_message = "Rescheduling script to start at line " + reprocess.startLine;
                    var id = headerRec.save(true, true);
                    Log.d("ProcessHeaderStagingRecords_PartialFile", "Header Record Re-saved:  " + id);

                    var params = {
                        custscript_header_record_id: currentID,
                        custscript_stating_line_number: reprocess.startLine
                    };

                    var result = nlapiScheduleScript('customscript_sch_det_recs_partial_file', null, params);                  // 20 units
                    Log.d('Scheduling Script RESULT:  ' + result);
                    if (result != 'QUEUED') {
                        Log.d('scheduled script not queued',
                            'expected QUEUED but scheduled script api returned ' + result);
                        EC.SendEmailAlert("Issue Rescheduling Script for a Partially processed file.\nRecord ID:  " + currentID + "\nStarting Line:  " + reprocess.startLine, ScriptName);
                    }
                    else {
                        Log.d('ProcessHeaderStagingRecords -- Re-Scheduling Script Status', "Result:  " + result);
                    }
                }
                else
                {
                    headerRec.custrecord_header_status = HeaderStagingStatus.PendingApproval;
                    headerRec.custrecord_header_error_message = "";
                    var id = headerRec.save(true, true);
                    Log.d("ProcessHeaderStagingRecords_PartialFile", "Header Record Re-saved:  " + id);
                }
            }
            catch(e)
            {
                Log.d("ProcessHeaderStagingRecords_PartialFile - " + currentID, "Error was throwing during processing:  " + e);
                var fields = ["custrecord_header_status", "custrecord_header_error_message"];
                var values = [HeaderStagingStatus.Error, e];
                nlapiSubmitField("customrecord_inv_adj_staging_header", currentID, fields, values);
                EC.SendEmailAlert(e, ScriptName);
            }
        }
        else
        {
            Log.d("ProcessHeaderStagingRecords_PartialFile", "Header Staging Record not found");
            throw nlapiCreateError("RECORD_NOT_FOUND", "Header Staging Record not found via search.\nRecord ID:  " + recordID + "\nStarting Line:  " + startingLine);
        }
    }
    catch(e)
    {
        Log.d("ProcessHeaderStagingRecords_PartialFile", "Unexpected Error:  " + e);
        EC.SendEmailAlert(e, ScriptName);
    }
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////




Log.AutoLogMethodEntryExit(null, true, true, true);
