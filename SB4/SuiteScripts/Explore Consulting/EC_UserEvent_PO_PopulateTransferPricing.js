/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_UserEvent_PO_PopulateTransferPricing
 * Version            1.0.0.0
 **/

var recipient = "kasupport@exploreconsulting.com,rhackey@exploreconsulting.com";

if ( nlapiGetContext().getExecutionContext() == "PRODUCTION" )
    var SendErrorEmailsFrom = 125247;
else
    var SendErrorEmailsFrom = -5;

var GovernanceThreshold = 100;

// Custom List called Set Transfer Price Status
var PO_STATUS = {
    Pending: 1,
    Completed: 2,
    IssuesFound: 3
};

function onBeforeLoad(type, form, request) {

}

function onBeforeSubmit(type) {

}

function onAfterSubmit(type) {
    if ( type != 'delete' && nlapiGetFieldValue('customform') == 159 && nlapiGetFieldValue('custbody_update_po_transfer_pricing') == 'T' ) EC.PopulateIntraCompanyTransferPricing_OnSave();
}


EC.GetVendorDetails = function(vendor){

    var data = { markup:0, currency:"CAD" };

    if ( vendor )
    {
        var columns = [new nlobjSearchColumn("currency"), new nlobjSearchColumn("custentity_intercompany_markup_percent")];
        var filters = [new nlobjSearchFilter("internalid", null, "is", vendor)];
        var vd = nlapiSearchRecord("vendor", null, filters, columns);

        if ( vd ) {
            data.markup = vd[0].getValue("custentity_intercompany_markup_percent") || 0;
            data.currency = vd[0].getText("currency") || "";
        }
    }

    Log.d("GetVendorDetails", data);
    return data;
};


EC.AddVendorMarkUp = function(markup, cost){

    if (markup) {
        markup = markup.replace("%", "");
        markup = markup / 100;
        var markedUpCost = (parseFloat(cost) * parseFloat(markup)) + parseFloat(cost);
        Log.d('Marked up Vendor Cost', 'markedUpCost:  ' + markedUpCost);
        return markedUpCost;
    }
    else
        return cost;
};


/**
 * Function to validate From Location field on the Transfer Order form.  This field must always be populated before any
 * lines are added to the order.  If populated, then a Suitelet is called to get the Transfer Pricing for the line.
 * Logic copied over from UE_TransferOrder script but added logic to consider exchange rate and markup calculations.
 * @constructor
 */
EC.PopulateIntraCompanyTransferPricing_OnSave = function(){

    try
    {
        var purchaseOrder = nsdal.loadObject(nlapiGetRecordType(), nlapiGetRecordId(), ["id", "entity", "location", "custbody_set_transfer_price_status", "orderstatus", "approvalstatus", "custbody_update_po_transfer_pricing"])
            .withSublist("item", ["item", "itemtype", "rate"]);

        var errorMsg = false;          // Error Message variable that will help us

        // Get the vendor's markup percentage and currency for later calculations
        var vendorData = EC.GetVendorDetails(purchaseOrder.entity);

        _.each(purchaseOrder.item, function(line, index){

            // Only update items that have a zero line Rate
            // Also check governance here to ensure we have enough to save the pricing already updated on the record
            var lineNumber = index+1;
            Log.d("PopulateIntraCompanyTransferPricing_OnSave = Line: " + lineNumber, "Remaining Governance:  " + nlapiGetContext().getRemainingUsage());
            Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Line Rate:  " + line.rate);
            if ( nlapiGetContext().getRemainingUsage() < GovernanceThreshold )
            {
                // If we are out of governance but we have not fully processed through the items yet, then set the error.
                if ( lineNumber <= parseInt(purchaseOrder.getLineItemCount("item")) ) errorMsg = true;

                Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Line:  " + lineNumber + " --- script is out of governance");
                return;
            }
            else if ( line.rate == 0 )
            {
                Log.d("PopulateIntraCompanyTransferPricing_OnSave", "itemid:  " + line.item);
                Log.d("PopulateIntraCompanyTransferPricing_OnSave", "itemType:  " + line.itemtype);
                var itemType = getItemType(line.itemtype);
                Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Converted itemType:  " + itemType);

                if ( itemType == "inventoryitem" ) {

                    var avgCost = EC.GetAverageCost(purchaseOrder.location, line.item, itemType);
                    Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Average Cost:  " + avgCost);
                    if ( avgCost && avgCost != "NOTFOUND" )
                    {
                        // Need to take this average cost and add in the Vendor Markup
                        var markedupCost = EC.AddVendorMarkUp(vendorData.markup, avgCost);

                        // Now convert it to the Vendor's currency
                        var convertedCost = EC.ConvertToVendorCurrency(markedupCost, vendorData.currency);
                        Log.d('Converting average cost to Vendor Currency', 'convertedCost:  ' + convertedCost);

                        line.rate = convertedCost;
                        Log.d("PopulateIntraCompanyTransferPricing_OnSave - LINE " + lineNumber + " SET", "Average Cost:  " + convertedCost);
                    }
                    else
                    {
                        errorMsg = true;
                        Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Error Found on Line:  " + lineNumber);
                    }
                }
                else {
                    Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Line:  " + lineNumber + " is not an inventory item");
                    return;
                }
            }
            else
                Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Line:  " + lineNumber + " -- rate wasn't zero -- rate already set");
        });

        // If an error was encountered for any line, set the custom status field to reflect this
        Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Show Error?  " + errorMsg);
        if (errorMsg) {
            purchaseOrder.custbody_set_transfer_price_status = PO_STATUS.IssuesFound;
            purchaseOrder.orderstatus = "A";        // A = Pending Approval
            purchaseOrder.approvalstatus = 1;       // 1 = Pending Approval
        } else {
            purchaseOrder.custbody_set_transfer_price_status = PO_STATUS.Completed;
            purchaseOrder.orderstatus = "B";        // B = Pending Receipt
            purchaseOrder.approvalstatus = 2;       // 2 = Approved
        }

        // Reset trigger flag
        purchaseOrder.custbody_update_po_transfer_pricing = false;
        
        var id = purchaseOrder.save(true, true);
        Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Purchase Order Re-saved:  " + id);
    }
    catch(e)
    {
        Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Unexpected Error Occurred:  " + e);
        nlapiSendEmail(SendErrorEmailsFrom, recipient, "Kit & Ace:  Error in EC_UserEvent_PO_PopulateTransferPricing script", "Error Details:  " + e)
    }
};

EC.ConvertToVendorCurrency = function(CAD_cost, vendorCurrency){

    // Pull the USD exchange rate as of today's date
    if (vendorCurrency != 'CAD') {
        var rate = nlapiExchangeRate('CAD', vendorCurrency, nlapiDateToString(new Date()));
        return parseFloat(CAD_cost) * rate;
    }
    else
        return parseFloat(CAD_cost);
};

/**
 * Function to do lookup to Item Record to find the average cost for a specific location
 * @param loc
 * @param item
 * @param itemtype
 * @return itemcost - the average cost for the given location for this item
 */
EC.GetAverageCost = function(loc, item, itemtype) {

    var itemcost = 0;
    try
    {
        var INV_LOCATION = 7;               // TODO:  for now we will hard code this to only look at the Canadian Retail Warehouse (id 7)
        Log.d('getAverageCost', 'Location Received: ' + loc);
        Log.d('getAverageCost', 'Item Received: ' + item);
        Log.d('getAverageCost', 'Item Type: ' + itemtype);

        // Open the Item Record and Find the Average Cost
        var itemRecord = nsdal.loadObject(itemtype, item, ["id", "type"])
            .withSublist("locations", ["locationid", "averagecostmli"]);

        if ( itemRecord && loc )
        {
            // Get the Average Cost for this location
            //itemcost = EC.GetItemAverageCost(itemRecord, loc);
            itemcost = EC.GetItemAverageCost(itemRecord, INV_LOCATION);  // Only worry about the Canadian Warehouse right now -
                                                                         // other location on PO will not have inventory since
                                                                         // they don't carry the item that is being transferred
            Log.d('getAverageCost', 'Cost found:  ' + itemcost);

            // If a Cost was found - return that to the client-side script
            if ( itemcost && itemcost != null )
                return itemcost;
            else
                itemcost = "NOTFOUND";
        }
    }
    catch(e)
    {
        Log.d('getAverageCost', 'Unexpected Error thrown from EC_UserEvent_PO_PopulateTransferPricing: ' + e);
        nlapiSendEmail(SendErrorEmailsFrom, recipient, "Kit & Ace - EC_UserEvent_PO_PopulateTransferPricing Error", "Error Details:  " + e)
    }
    return itemcost;
};

/**
 * Function that goes through location sublist on Item Record and finds the relevant location and then pulls the average cost from that line.
 * @param itemRecord
 * @param location
 * @returns averageCost {number} - average cost found in Locations sublist
 */
EC.GetItemAverageCost = function(itemRecord, location){

    var averageCost = 0.00;
    if ( itemRecord )
    {
        _.each(itemRecord.locations, function(locLine, index)
        {
            //Log.d('Processing Locations Line ' + index+1, 'Location:  ' + locLine.locationid);
            if ( locLine.locationid == location )
            {
                averageCost = locLine.averagecostmli;
                Log.d('Average Cost Found for Location ' + location, 'Avg Cost:  ' + averageCost);
                return;
            }
        });
    }
    return averageCost;
};

Log.AutoLogMethodEntryExit(null, true, true, true);

