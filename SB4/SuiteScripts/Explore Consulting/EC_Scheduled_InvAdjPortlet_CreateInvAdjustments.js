/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_Scheduled_InvAdjPortlet_CreateInvAdjustments
 * Version            1.0.0.0
 **/

var Governance_Overhead_Needs = 100;
var ScriptName = "EC_Scheduled_InvAdjPortlet_CreateInvAdjustments";

function onStart() {
    EC.ProcessInventoryAdjustments();
}

EC.ProcessInventoryAdjustments = function(){

    try{

        // Get Parent Staging Records for Processing
        var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"), new nlobjSearchFilter("custrecord_header_status", null, "anyof", [HeaderStagingStatus.Approved, HeaderStagingStatus.Processing])];
        var columns = [new nlobjSearchColumn("custrecord_header_status"), new nlobjSearchColumn("custrecord_header_adjustment_date"),
            new nlobjSearchColumn("custrecord_header_adjustment_location"), new nlobjSearchColumn("custrecord_header_adjustment_account"),
            new nlobjSearchColumn("custrecord_header_adjustment_dept"), new nlobjSearchColumn("custrecord_header_importer"),
            new nlobjSearchColumn("custrecord_header_adjustment_memo")];
        var results = nlapiSearchRecord("customrecord_inv_adj_staging_header", null, filters, columns);
        var timesUp = false;
        var currentID;
        var reprocess = false;
        var detailsToUpdate = [];

        if ( results ) {
            Log.d("ProcessInventoryAdjustments", "Number of Search Results:  " + results.length);

            for (var i=0; i < results.length && !timesUp; i++) {

                currentID = results[i].getId();
                Log.d("Processing Header Staging records - result #" + i, "Current Record ID:  " + currentID);
                reprocess = false;

                try{
                    // Process this Header Detail Record
                    nlapiSubmitField("customrecord_inv_adj_staging_header", results[i].getId(), "custrecord_header_status", HeaderStagingStatus.Processing);

                    // Find all Detail records for this Header record
                    var filters2 = [new nlobjSearchFilter("isinactive", null, "is", "F"),
                        new nlobjSearchFilter("custrecord_header_staging_record", null, "anyof", currentID),
                        new nlobjSearchFilter("custrecord_inv_adj_status", null, "anyof", [DetailStagingStatus.ToBeProcessed, DetailStagingStatus.ErrorReprocess])];
                    var columns2 = [new nlobjSearchColumn("custrecord_inv_adj_number"), new nlobjSearchColumn("custrecord_inv_adj_quantity_on_hand"),
                        new nlobjSearchColumn("custrecord_inv_adj_subsidiary"), new nlobjSearchColumn("custrecord_inv_adj_adjust_quantity")];

                    var details = runSearch("customrecord_inv_adj_stage", columns2, filters2);

                    if ( details ) {
                        Log.d("ProcessInventoryAdjustments", "Number of Detail Search Results:  " + details.length);

                        detailsToUpdate = [];

                        // Create an inventory adjustment record for these details
                        var invAdjRecord = nsdal.createObject("inventoryadjustment", InventoryAdjustmentProperties)
                            .withSublist("inventory", InvAdjLineProperties);

                        invAdjRecord.trandate = results[i].getValue('custrecord_header_adjustment_date');
                        Log.d("ProcessInventoryAdjustments - Creating Inventory Adjustment", "Date:  " + results[i].getValue('custrecord_header_adjustment_date'));
                        invAdjRecord.account = results[i].getValue('custrecord_header_adjustment_account');
                        Log.d("ProcessInventoryAdjustments - Creating Inventory Adjustment", "Account:  " + results[i].getValue('custrecord_header_adjustment_account'));
                        invAdjRecord.subsidiary = details[0].getValue('custrecord_inv_adj_subsidiary');
                        Log.d("ProcessInventoryAdjustments - Creating Inventory Adjustment", "Sub:  " + details[0].getValue('custrecord_inv_adj_subsidiary'));
                        invAdjRecord.department = results[i].getValue('custrecord_header_adjustment_dept');
                        Log.d("ProcessInventoryAdjustments - Creating Inventory Adjustment", "Dept:  " + results[i].getValue('custrecord_header_adjustment_dept'));
                        invAdjRecord.adjlocation = results[i].getValue('custrecord_header_adjustment_location');
                        Log.d("ProcessInventoryAdjustments - Creating Inventory Adjustment", "Loc:  " + results[i].getValue('custrecord_header_adjustment_location'));
                        invAdjRecord.memo = results[i].getValue('custrecord_header_adjustment_memo');
                        Log.d("ProcessInventoryAdjustments - Creating Inventory Adjustment", "Memo:  " + results[i].getValue('custrecord_header_adjustment_memo'));

                        var notEnoughGovToProceed = false;
                        for (var d=0; d<details.length && !notEnoughGovToProceed; d++) {

                            // Add this detail line to the Inventory Adjustment
                            var newLine = invAdjRecord.inventory.addLine();
                            newLine.item = details[d].getValue('custrecord_inv_adj_number');
                            newLine.adjustqtyby = details[d].getValue('custrecord_inv_adj_adjust_quantity');
                            newLine.location = results[i].getValue('custrecord_header_adjustment_location');        // Required at the line level

                            detailsToUpdate.push(details[d].getId());

                            // Make sure we have governance to keep going with more detail records
                            var numberLeftToProcess = details.length - d;
                            Log.d("ProcessInventoryAdjustments", "numberLeftToProcess:  " + numberLeftToProcess);
                            notEnoughGovToProceed = EC.CalculateGovernanceNeeds(numberLeftToProcess);
                            if ( notEnoughGovToProceed ) timesUp = true;          // Stop the processing of more header records for now
                        }

                        var id = invAdjRecord.save(true, true);
                        Log.d("ProcessInventoryAdjustments", "New Inventory Adjustment Record created:  " + id);

                        if ( id ) {
                            // Now update all detail records to connect them to the newly created inventory adjustment
                            for (var r = 0; r < detailsToUpdate.length; r++) {
                                var fields3 = ["custrecord_inv_adj_transaction", "custrecord_inv_adj_status", "custrecord_inv_adj_error"];
                                var values3 = [id, DetailStagingStatus.Completed, ""];
                                nlapiSubmitField("customrecord_inv_adj_stage", detailsToUpdate[r], fields3, values3);
                            }
                        }
                    }
                }
                catch(e)
                {
                    Log.d("ProcessInventoryAdjustments - " + currentID, "Error was throwing during processing:  " + e);
                    var fields = ["custrecord_header_status", "custrecord_header_error_message"];
                    var values = [HeaderStagingStatus.Error, e];
                    nlapiSubmitField("customrecord_inv_adj_staging_header", currentID, fields, values);

                    // Now update all detail records to show an error occurred
                    for (var x=0; x < detailsToUpdate.length; x++) {
                        var fields6 = ["custrecord_inv_adj_status", "custrecord_inv_adj_error"];
                        var values6 = [DetailStagingStatus.ErrorReview, ""];
                        nlapiSubmitField("customrecord_inv_adj_stage", detailsToUpdate[x], fields6, values6);
                    }

                    EC.SendEmailAlert(e, ScriptName);
                }

                if ( !timesUp )
                    timesUp = EC.RunTimeScriptCheckers(Script_GovernanceThreshold);

                // Just got done processing this Header Staging Record.  Check to see if all Detail Staging Records
                //      got updated.  If they did, then set the status accordingly.
                var finished = EC.AllRecordsComplete(currentID);
                if ( finished ) {
                    var fields4 = ["custrecord_header_status", "custrecord_header_error_message"];
                    var values4 = [HeaderStagingStatus.Completed, ""];
                    nlapiSubmitField("customrecord_inv_adj_staging_header", currentID, fields4, values4);
                }
                else if ( !finished && notEnoughGovToProceed ){
                    // We ran out of governance and there are still records to process
                    nlapiSubmitField("customrecord_inv_adj_staging_header", currentID, "custrecord_header_status", HeaderStagingStatus.Processing);
                }
                /*else{
                    // Since there are still records to be processed,
                    nlapiSubmitField("customrecord_inv_adj_staging_header", currentID, "custrecord_header_status", HeaderStagingStatus.Approved);
                } */
            }
        }
        else
        {
            Log.d("ProcessInventoryAdjustments", "No Header Staging Records are ready for Processing");
        }


        if ( timesUp )
        {
            Log.d('Scheduling Script to continue updating');
            var result = nlapiScheduleScript('customscript_ec_sch_create_inv_adjusts', null, null);                  // 20 units
            Log.d('Scheduling Script RESULT:  ' + result);
            if (result != 'QUEUED') {
                Log.d('scheduled script not queued',
                    'expected QUEUED but scheduled script api returned ' + result);
                var emailBody5 = "Script: EC_Scheduled_InvAdjPortlet_CreateInvAdjustments.js\nFunction: ProcessInventoryAdjustments\nError: There was an issue re-scheduling the scheduled script in this script";
                EC.SendEmailAlert("Error: There was an issue re-scheduling the scheduled script in this script.", ScriptName);
            }
            else {
                Log.d('ProcessInventoryAdjustments -- Re-Scheduling Script Status', "Result:  " + result);
            }
        }
    }
    catch(e)
    {
        Log.d("ProcessInventoryAdjustments", "Unexpected Error:  " + e);
        EC.SendEmailAlert(e, ScriptName);
    }
};

EC.CalculateGovernanceNeeds = function(numberLeftToProcess){
    var govNeed = (parseInt(numberLeftToProcess) * 2) + parseInt(Governance_Overhead_Needs);        // 2 = nlapiSubmitField to a custom record
    Log.d("CalculateGovernanceNeeds", "govNeed:  " + govNeed + "   Remaining Gov:  " + nlapiGetContext().getRemainingUsage());
    return nlapiGetContext().getRemainingUsage() < govNeed || moment().isAfter(maxEndTime);
};

EC.AllRecordsComplete = function(currentID){

    var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"),
        new nlobjSearchFilter("custrecord_header_staging_record", null, "anyof", currentID),
        new nlobjSearchFilter("custrecord_inv_adj_status", null, "noneof", [DetailStagingStatus.Completed, DetailStagingStatus.ErrorCancel])];
    var columns = [new nlobjSearchColumn("internalid")];
    var detailLeft = nlapiSearchRecord("customrecord_inv_adj_stage", null, filters, columns);

    if ( detailLeft ) {
        Log.d("AllRecordsComplete", "There are still " + detailLeft.length + " details records to process for Header Record ID " + currentID);
        return false;
    }
    else return true;
};

Log.AutoLogMethodEntryExit(null, true, true, true);