/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       30 Sep 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only)
 *                      paybills (vendor payments)
 * @returns {Void}
 */



function setItemRecordVendorPrice(idItem, arrVendors, currencyObject, objValues){

			nlapiLogExecution("AUDIT", "func: setItemRecordVendorPrice",  JSON.stringify(objValues));


			var vendorItemLog = true;
			var ItemRecord = nlapiLoadRecord("inventoryitem", idItem);
			var itemVendorCount = ItemRecord.getLineItemCount("itemvendor");

			var arrVendorsId = arrVendors.ids;
			var arrVendorsObj = arrVendors.obj;

			nlapiLogExecution("AUDIT", "func: setItemRecordVendorPrice: arrVendorsId.length",  arrVendorsId.length);

			var itemVendorCount = ItemRecord.getLineItemCount("itemvendor");
			
			
			//TODO: Insert here the new logic to set drop ship items. 					
			var dropshipExpense = nlapiGetContext().getSetting('SCRIPT', 'custscript_erp_dropship_expense');								
			var prefVendorCAN = nlapiGetContext().getSetting('SCRIPT', 'custscript_erp_preferredvendor_can');
			var prefVendorUSA = nlapiGetContext().getSetting('SCRIPT', 'custscript_erp_preferredvendor_usa');
			
			ItemRecord.setFieldValue('isdropshipitem', "T");						 //Dropship = on
			ItemRecord.setFieldValue('dropshipexpenseaccount', dropshipExpense); 	 //Dropship Expense
			

			for(var q=0; q< arrVendorsId.length; q++){

								var thisVendor = arrVendorsId[q];

								var found = false;
								var objVendorMatch = new Object();

								var vendorline = ItemRecord.findLineItemValue('itemvendor', 'vendor', thisVendor);


								objVendorMatch.exist = found;
								objVendorMatch.item = ItemRecord.getId();
								objVendorMatch.itemname = ItemRecord.getFieldValue("itemid");

								objVendorMatch.vendorid = thisVendor;
								objVendorMatch.vendorname = arrVendorsObj[q].vendorname;
								objVendorMatch.vendormarkup = arrVendorsObj[q].markup;
								objVendorMatch.vendorline = vendorline;
								objVendorMatch.vendorcurrency = arrVendorsObj[q].currency;


								ItemRecord.setFieldValue('custitem_erp_vendorprice_last_ir', objValues.ir);
								ItemRecord.setFieldValue('custitem_erp_vendorprice_last_po', objValues.po);
								ItemRecord.setFieldValue('custitem_erp_vendorprice_last_fca', objValues.fca);
								ItemRecord.setFieldValue('custitem_erp_vendorprice_last_currency', objValues.currency);
								ItemRecord.setFieldValue('custitem_erp_vendorprice_last_currfx', JSON.stringify(currencyObject));
								ItemRecord.setFieldValue('custitem_ec_intercompany_price_updated', nlapiDateToString(new Date(), "datetimetz"));
								
								
																											
								var fcaFx = convertCurrency( objValues.fca , objVendorMatch.vendorcurrency, currencyObject);	//get the FCA in the vendor currency
								var rate  = addMarkup(fcaFx, objVendorMatch.vendormarkup); 										//markup based on Vendor rules
								
								if(vendorline > -1){															
										ItemRecord.removeLineItem('itemvendor', objVendorMatch.vendorline, true);
								}											

								ItemRecord.selectNewLineItem('itemvendor');
								
								if(objVendorMatch.vendorid == prefVendorCAN || objVendorMatch.vendorid == prefVendorUSA){
								ItemRecord.setCurrentLineItemValue("itemvendor", 'preferredvendor', 'T');
								}
								
								ItemRecord.setCurrentLineItemValue("itemvendor", "purchaseprice", rate);
								ItemRecord.setCurrentLineItemValue("itemvendor", "vendor", objVendorMatch.vendorid );
								ItemRecord.commitLineItem("itemvendor");
	}

	try{

		var submitid = nlapiSubmitRecord(ItemRecord);
		nlapiLogExecution("AUDIT", "func: setItemRecordVendorPrice: submitid",  submitid);

	}catch(ex){}


	}




function tranverseItemRecords(objItemReceipt, arrVendors, currencyObject){

	nlapiLogExecution("AUDIT", "func: tranverseItemRecords",  objItemReceipt);

	if(!isNullOrEmpty(objItemReceipt)){


		var intIRItemCount = objItemReceipt.getLineItemCount('item');

		for(var o = 1 ; o <= intIRItemCount; o++){

			var intItemId  = objItemReceipt.getLineItemValue('item','item',o);

			var objValues = new Object();

			objValues.itemunit = objItemReceipt.getLineItemValue('item','itemunitprice',o);
			objValues.fca = objItemReceipt.getLineItemValue('item','rate',o);
			objValues.po = objItemReceipt.getFieldValue('createdfrom');
			objValues.ir = objItemReceipt.getId();
			objValues.item = objItemReceipt.getLineItemValue('item','item',o);
			objValues.currency = objItemReceipt.getFieldValue('currency');

			nlapiLogExecution("AUDIT", "func: tranverseItemRecords: intItemId",  intItemId);
			setItemRecordVendorPrice(intItemId, arrVendors, currencyObject, objValues);

		}

	}

}


function createUpdate_VendorInterCOPriceAS(type){

	
	nlapiLogExecution("AUDIT", "func: createUpdate_VendorInterCOPriceAS: type", type);
																													
	if(type == "create"){																															// Ensure that the calculation will not run again when the IR is re-saved																								
		
		try{
		
		 	vendorItemLog = true; submitItemLog = false; checkGovernanceLog = false;
		 	
		 	recId = nlapiGetRecordId();	nlapiLogExecution("AUDIT", "func: createUpdate_VendorInterCOPriceAS", recId);
	
			var ItemReceiptRecord = nlapiLoadRecord('itemreceipt', recId);																			// Check if IR is from a  PO
			var ordType = ItemReceiptRecord.getFieldValue('ordertype');																				// Check if IR is from a  PO
			
			if(ordType == 'PurchOrd'){
			
				var idPO = ItemReceiptRecord.getFieldValue('createdfrom');																			// Check if IR is from a PO with form 100
				var idPOForm = nlapiLookupField('purchaseorder', idPO, 'customform'); 
	
				if(idPOForm == 100){
												

					var idCurrency = ItemReceiptRecord.getFieldValue('currency');																	// Get the IR Currency
					var idCurrencyText = ItemReceiptRecord.getFieldText('currency');
	
					var arrItemArray = getItemSublistAS(ItemReceiptRecord);																			// Get the IR Items, returns Array
	
					if(!isNullOrEmpty(arrItemArray)){
	
						if(true){
	
							var objVendorList = getVendorLocCustomerMap(); 																			// Get all the Vendors
							var currencyConversion = retrieveCurrencyExchange(objVendorList.obj, idCurrency, idCurrencyText);						// Get Currency for Today
							tranverseItemRecords(ItemReceiptRecord, objVendorList,currencyConversion); 												// Traverse Items for Processing
																																					//Updates items inside traversal
						}
					}																
				}				
			}
		
		}catch(ex){nlapiLogExecution('ERROR', 'func:createUpdate_VendorInterCOPriceAS', ex.toString());}
	}
	
}


function retrieveCurrencyExchange(vendorList, currencyId, currencyName){

	var currencyList = new Array();
	var currencyListObject = new Array();
	var currencyObj = new Object()
    var currencyFound = new Array();

	if(!isNullOrEmpty(vendorList)){

		for(var s=0; s < vendorList.length; s++){

			var currencyObject = new Object();

			var vendorObj = vendorList[s];

			var subsidiaryCurrency = vendorObj.subcurrency;
      		var subsidiaryCurrencyName = vendorObj.subcurrencyTxt;
			var vendorCurrency = vendorObj.currency;
			var vendorCurrencyName = vendorObj.currencyTxt;
			var transactionCurrency = currencyId;
			var transactionCurrencyName = currencyName;


      		if(currencyFound.indexOf(vendorCurrency) == -1){

	       		currencyFound.push(vendorCurrency);

    	    	var converteCurrency = converCurrency(currencyId, 1, vendorCurrency);

    	    	currencyObject.transactionCurrencyId = transactionCurrency;
    	    	currencyObject.transactionCurrencyName = transactionCurrencyName;
        		//currencyObject.baseSubCurrencyId = subsidiaryCurrency;
	        	//currencyObject.baseSubCurrencyName = subsidiaryCurrencyName;
  				currencyObject.currencyId = vendorCurrency;
  				currencyObject.currencyName = vendorCurrencyName;
  				currencyObject.rate	= converteCurrency;

  				currencyList.push(vendorCurrency);
  				currencyListObject.push(currencyObject);

     		 }



		}

		// To do Canadian

		var currencyObject = new Object();
		var vendorCurrency = 1;
		var vendorCurrencyName = 'CAD';

		var converteCurrency = converCurrency(currencyId, 1, vendorCurrency);

		currencyObject.transactionCurrencyId = transactionCurrency;
    	currencyObject.transactionCurrencyName = transactionCurrencyName;
		//currencyObject.baseSubCurrencyId = subsidiaryCurrency;
    	//currencyObject.baseSubCurrencyName = subsidiaryCurrencyName;
		currencyObject.currencyId =vendorCurrency;
		currencyObject.currencyName = vendorCurrencyName;
		currencyObject.rate	= converteCurrency;

		currencyList.push(vendorCurrency);
		currencyListObject.push(currencyObject);


		currencyObj.ids = currencyList;
		currencyObj.obj = currencyListObject;

		nlapiLogExecution("DEBUG","func: retrieveCurrencyExchange:  Currency Object: ", JSON.stringify(currencyObj));

		return currencyObj;

	}else{
		return null;
	}




}

function convertCurrency( amount , currencyId, currencyObject){

	var ids = currencyObject.ids;
	var obj = currencyObject.obj;
	var conversion = amount;

	if(!isNullOrEmpty(ids)){

		var index = ids.indexOf(currencyId);
		var curr = obj[index];
		conversion = curr.rate * amount;

	}

	return conversion;

}


function checkGovernance(method, log){

	var objContext = nlapiGetContext();
	var remUnits = objContext.getRemainingUsage();

	if(log) nlapiLogExecution("AUDIT", "func: checkGovernance: " + method, remUnits);

	return remUnits;

}


function addMarkup(currAveCost, vendorMarkup){

	if(!isNullOrEmpty(vendorMarkup)){

		var markuppercentage = vendorMarkup.replace("%","");
		markuppercentage = Number(markuppercentage);

		var costMarkup = (1 + (markuppercentage/100) ) * currAveCost;
		return costMarkup;

	}else{
		return currAveCost;
	}

}






function converCurrency(strCurrency, decCost, strVendorCurrency){

    // Pull the USD exchange rate as of today's date

	try{
    //if (strVendorCurrency != 'CAD') {
        var rate = nlapiExchangeRate(strCurrency, strVendorCurrency, nlapiDateToString(new Date()));
        return parseFloat(decCost) * rate;

        nlapiLogExecution("AUDIT", "func: converCurrency", "subcurrency: "+ strCurrency + " vendorcurrency: " + strVendorCurrency + " rate:" + rate);


    //}
    //else{
    //    return parseFloat(decCost);
    //}

	} catch (ex){
		return parseFloat(decCost);
	}
}

function setItemRecord(objSearchResultItem, arrVendors){

	nlapiLogExecution("DEBUG", "func: setItemRecord", "START");

	try{

	if(!isNullOrEmpty(objSearchResultItem)){

		var arrItemIds = objSearchResultItem.ids;
		var arrItemObj = objSearchResultItem.objs;

		nlapiLogExecution("DEBUG", "func: setItemRecord: arr length ", JSON.stringify(arrItemIds));

		for(var l = 0 ; l < arrItemIds.length ;  l++){

			nlapiLogExecution("DEBUG", "func: setItemRecord: arrItemIds", arrItemIds[l]);

			var itemRecord = nlapiLoadRecord('inventoryitem', arrItemIds[l]);

			if(itemRecord){

	            var vendorItemLines = getItemVendorLines(itemRecord);

			}
		}
	}

	}
	catch(ex){}

}


function getVendorLocCustomerMap(idLocation){

	var objVendors = new Object();
	var arrVendorsObj = new Array(); var arrVendorIds = new Array();

	var strSavedSearchIDVeLuCu = null;
	var arSaveSearchFiltersVeLuCu = new Array();
	var arSavedSearchColumnsVeLuCu = new Array();
	var arSavedSearchResultsVeLuCu = null;

	if(!isNullOrEmpty(idLocation)){
		arSaveSearchFiltersVeLuCu.push(new nlobjSearchFilter( 'custrecord_ka_veculo_location', null, 'anyof', idLocation));
	}

	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('internalid').setSort());
	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('name'));
	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('custrecord_ka_veculo_vendor'));
	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('custrecord_ka_veculo_location'));
	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('custrecord_ka_veculo_customer'));

	var arSavedSearchResultsVeLuCu = nlapiLoadSearch('customrecord_vendcustloc_map', 'customsearch_ka_veculo');

	arSavedSearchResultsVeLuCu.addColumns(arSavedSearchColumnsVeLuCu);
	arSavedSearchResultsVeLuCu.addFilters(arSaveSearchFiltersVeLuCu);

	var resultSet = arSavedSearchResultsVeLuCu.runSearch();

	if(resultSet){

		var results = resultSet.getResults(0,1000);


		for(var i = 0; i < results.length; i++){

			try{
				var idCurrentItem = results[i].getValue('internalid');
				var columns = results[i].getAllColumns();
				var objResult = results[i];

				var objVendor = new Object();
				objVendor.markup = objResult.getValue(columns[4]);
				objVendor.vendor = objResult.getValue(columns[1]);
				objVendor.vendorname = objResult.getText(columns[1]);
				objVendor.currency = objResult.getValue(columns[5]);
				objVendor.currencyTxt = objResult.getText(columns[5]);
				objVendor.subcurrency = objResult.getValue(columns[6]);
				objVendor.subcurrencyTxt = objResult.getText(columns[6]);

				arrVendorsObj.push(objVendor);
				arrVendorIds.push(objVendor.vendor);

			}catch(ex){}

		}

		objVendors.obj = arrVendorsObj;
		objVendors.ids = arrVendorIds;

		nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap", JSON.stringify(objVendors));
		return objVendors;

	}else{
		nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap", null);
		return null;
	}


//	}else{
//		nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap", "no location id");
//		return null;
//	}
}

function getItemVendorLines(itemObj, arrVendors){



	nlapiLogExecution("DEBUG", "func: getItemVendorLines", "START");
	nlapiLogExecution("DEBUG", "func: getItemVendorLines: Item: ", itemObj.getId());

	var intItemVendorCount = itemObj.getLineItemCount('itemvendor');
	nlapiLogExecution("DEBUG", "func: getItemVendorLines: Vendor Count: ", intItemVendorCount);

	for(var m=1; m <= intItemVendorCount; m++){

		var intVendorId = itemObj.getLineItemValue('itemvendor','vendor',m);
		var intVendorName = itemObj.getLineItemText('itemvendor','vendor',m);
		var intVendorName = itemObj.getLineItemText('itemvendor','vendor',m);

		var objVendorObject = new Object();
		objVendorObject.vendorid = intVendorId;
		objVendorObject.vendorname = intVendorName;

		nlapiLogExecution("DEBUG", "func: getItemVendorLines", JSON.stringify(objVendorObject));
	}
}


function runSchedScript(recordId, lineNumber){

    var params = {
    		custscript_cuvp_record_id: recordId ,
    		custscript_cuvp_starting_line_number: lineNumber
	 };

	 var result = nlapiScheduleScript('customscript_erp_vendorintercoprice_ss', null, params);                  // 20 units
     nlapiLogExecution("DEBUG", "func: runSchedScript", result);
}



function getItemAverageCost(arrItemArray){

		nlapiLogExecution("DEBUG", "func: getItemAverageCost: length", arSavedSearchResultsItm.length);

		var strSavedSearchIDItm = null;
		var arSaveSearchFiltersItm = new Array();
		var arSavedSearchColumnsItm = new Array();
		var arSavedSearchResultsItm = null;

		var arrResultObjectArray = new Array();
		var arrIdObjectArray = new Array();

		var objItemResult = new Object();

		if(!isNullOrEmpty(items)){
			arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null , 'anyof', arrItemArray ));
		}

		arSaveSearchFiltersItm.push(new nlobjSearchFilter( 'currency', 'pricing',  'anyof', currency));
		arSaveSearchFiltersItm.push(new nlobjSearchFilter( 'pricelevel', 'pricing',  'is', pricelevel));

		arSavedSearchColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('name'));
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('currency','pricing'));
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('pricelevel','pricing'));
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('unitprice','pricing'));

		arSavedSearchResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchColumnsItm);


		if(!isNullOrEmpty(arSavedSearchResultsItm)){
			nlapiLogExecution("DEBUG", "func: getItemAverageCost: length", arSavedSearchResultsItm.length);
		}
}



function getItemLocAveCost(items, currency, pricelevel, location){


	var strSavedSearchIDItm = null;
	var arSaveSearchFiltersItm = new Array();
	var arSavedSearchColumnsItm = new Array();
	var arSavedSearchResultsItm = null;

	var arrResultObjectArray = new Array();
	var arrIdObjectArray = new Array();

	var objItemResult = new Object();

	if(!isNullOrEmpty(items)){
		arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null , 'anyof', items ));
	}

	arSaveSearchFiltersItm.push(new nlobjSearchFilter( 'inventorylocation', null ,  'anyof', location));

	arSavedSearchColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('name'));
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('locationaveragecost'));
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('inventorylocation'));

	arSavedSearchResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchColumnsItm);

	if(!isNullOrEmpty(arSavedSearchResultsItm)){

		//Build the result set object lookup and fill the Array Maps
		for (var j = 0 ; j < arSavedSearchResultsItm.length; j++){

			var obj = new Object();
			obj.id = arSavedSearchResultsItm[j].getValue('internalid');
			obj.name = arSavedSearchResultsItm[j].getValue('name');
			obj.locavecost = arSavedSearchResultsItm[j].getValue('locationaveragecost');
			obj.invlocation = arSavedSearchResultsItm[j].getValue('inventorylocation');



			arrResultObjectArray.push(obj);
			arrIdObjectArray.push(obj.id);
		}

		objItemResult.ids = arrIdObjectArray;
		objItemResult.obj = arrResultObjectArray;

		nlapiLogExecution("DEBUG", "func: getItemResultObj : arrResultObjectArray: length", arrResultObjectArray.length);

		return objItemResult;

	}else{

		nlapiLogExecution("DEBUG", "func: getItemResultObj : arrResultObjectArray: length", "null");
		return null;
	}


}



function getItemSublist(){

	var arrItems = new Array();
	var intGetPOlineCount = nlapiGetLineItemCount('item');

	for(var i = 1; i <= intGetPOlineCount; i++){

		var idItemId = nlapiGetLineItemValue('item','item', i);
		var strItemType = nlapiGetLineItemValue('item','itemtype', i);

		if(strItemType == 'InvtPart'){
			nlapiLogExecution("DEBUG", "getItemSublist", idItemId);
			arrItems.push(idItemId);
		}

	}

	nlapiLogExecution('DEBUG', "func: getItemSublist: Item Sublist ", arrItems.length);
	return arrItems;
}


function getItemSublistAS(itemReceipt){

	var arrItems = new Array();
	var intGetPOlineCount = itemReceipt.getLineItemCount('item');

	for(var i = 1; i <= intGetPOlineCount; i++){

		var idItemId =  itemReceipt.getLineItemValue('item','item', i);
		var strItemType =  itemReceipt.getLineItemValue('item','itemtype', i);

		if(strItemType == 'InvtPart'){
			//nlapiLogExecution("DEBUG", "getItemSublist", idItemId);
			arrItems.push(idItemId);
		}

	}

	nlapiLogExecution('DEBUG', "func: getItemSublistAS: Item Sublist ", arrItems.length);
	return arrItems;
}


function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}

