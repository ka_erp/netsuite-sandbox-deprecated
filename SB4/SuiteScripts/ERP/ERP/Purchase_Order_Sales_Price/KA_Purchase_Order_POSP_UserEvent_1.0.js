/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 Aug 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
 
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */

function POSP_UE_BeforeLoad(type){
	
//	nlapiLogExecution("DEBUG", "POSP_UE_BeforeLoad : TYPE  ======================================================== ", type);
	
	var pospRef = nlapiGetFieldValue('custbody_erp_posp_reference');
	
	if(type == 'copy'){

			var intGetPOlineCount = nlapiGetLineItemCount('item'); 
//			nlapiLogExecution("DEBUG", "POSP_UE_BeforeLoad : line count ", intGetPOlineCount);
//			nlapiSetLineItemDisabled('item','custcol_erp_posp_retail_currency', false);
			nlapiGetLineItemField('item', 'custcol_erp_posp_retail_currency').setDisplayType('hidden');
			nlapiGetLineItemField('item', 'custcol_erp_posp_override_retail_pric').setDisplayType('hidden');
			nlapiGetLineItemField('item', 'custcol_erp_posp_override_retail_pric').setDisplayType('hidden');
			
			for(var z = 1; z <= intGetPOlineCount; z++){
				strItemType == 'InvtPart'
					
//				var idItemId = nlapiGetLineItemValue('item','item', z); 
				var strItemType = nlapiGetLineItemValue('item','itemtype', z);
//				nlapiLogExecution("DEBUG", "POSP_UE_BeforeLoad : strItemType ", strItemType);
				
				if(strItemType == 'InvtPart'){
					nlapiSetLineItemValue('item', 'custcol_erp_posp_override_retail_pric', z, 'F'); 
					nlapiSetLineItemValue('item', 'custcol_ka_po_retail_value', z, 0.00);
					nlapiSetLineItemValue('item', 'custcol_erp_posp_retail_currency', z, null);
				}
				
			}
//			nlapiSetLineItemDisabled('item','custcol_erp_posp_retail_currency', true);
			
	}else if(isNullOrEmpty(pospRef)){		
		nlapiGetField('custbody_erp_posp_reference').setDisplayType('normal');
	}
	
	
}

function PSOSP_UE_BeforeSubmit(type){
	
//	try{
		
		nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : TYPE  ======================================================== ", type);
		
		var formIds = nlapiGetContext().getSetting('SCRIPT', 'custscript_posp_forms');
		nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : arrForms1: ", formIds);
		
		var arrForms = formIds.split(',');
		nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : arrForms2: ", JSON.stringify(arrForms));
		
		var idOrderStatus = nlapiGetFieldValue('orderstatus');
		
		nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : idOrderStatus  ======================================================== ", idOrderStatus  + " " + (idOrderStatus == ""));
		
		//Check the forms first
		if(!isNullOrEmpty(arrForms) && (idOrderStatus == "A" || idOrderStatus == "B" || isNullOrEmpty(idOrderStatus))){
			var currentForm = nlapiGetFieldValue('customform'); 			
			if(arrForms.indexOf(currentForm) >= 0 ){
				
				
				var idPOSP = nlapiGetFieldValue('custbody_erp_posp_reference');	
				var shiptocountry = nlapiGetFieldValue('shipcountry'); 
				var bRunSalePriceExecute = false; 
				
				nlapiLogExecution("AUDIT", "PSOSP_UE_BeforeSubmit : bRunSalePriceExecute1 : shipCountry ", shiptocountry );
				
				nlapiLogExecution("AUDIT", "PSOSP_UE_BeforeSubmit : bRunSalePriceExecute (Pre Lookup 1) : Price Execute", bRunSalePriceExecute );
				
				//If there is nothing set in the POSP Reference, try to lookup the country		
				objRunSalePriceExecute = runPriceLookup(idPOSP, shiptocountry); 
				nlapiLogExecution("AUDIT", "PSOSP_UE_BeforeSubmit : bRunSalePriceExecute (Second Lookup 2)", JSON.stringify(objRunSalePriceExecute));
										
				// Actual Execution of the Price Retrieval		
				if(objRunSalePriceExecute.run){
					
					
					//Gets the POSP record. Used to retrieved the correct base price and price level 	
					var recPOSP = nlapiLoadRecord('customrecord_po_sales_price' , objRunSalePriceExecute.id); 
					var pospPriceLevel = recPOSP.getFieldValue('custrecord_erp_posp_price_level');
					var pospCurrency = recPOSP.getFieldValue('custrecord_erp_posp_currency');	
					var arrCurr = new Array(); arrCurr.push(pospCurrency); 
					
					nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : running pricing", " price level" + pospPriceLevel  + " currency:" + pospCurrency);
					
					
					//Get the Items on the Purchas Order
					var arrItemArray = getPOItemArray(); 	
									
					//Get the Price Array with all the Sales Price for the items
					var resultSet = searchItem_Currency_PriceLevel( arrItemArray, arrCurr, pospPriceLevel); 
				    
					
					// Create the Array Maps			
					var arrResultObjectArray = new Array();  
					var arrIdObjectArray = new Array(); 
					
					
					//Build the result set object lookup and fill the Array Maps
					if(resultSet){
					
						for (var j = 0 ; j < resultSet.length; j++){
							
							var obj = new Object(); 
							obj.id = resultSet[j].getValue('internalid');
							obj.name = resultSet[j].getValue('name');
							obj.salesprice = resultSet[j].getValue('unitprice','pricing');
							obj.currency = resultSet[j].getValue('currency','pricing');
							obj.pricelevel = resultSet[j].getValue('pricelevel','pricing');
							
							arrResultObjectArray.push(obj);				
							arrIdObjectArray.push(obj.id); 					
						}				
						
						nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : arrResultObjectArray", JSON.stringify(arrResultObjectArray));
						nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : arrIdObjectArray", JSON.stringify(arrIdObjectArray));
						
					}else{
				
						nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : Resultset", resultSet);
					}
					
					
					
					
					// Set the Sales Price on the lines
					var intGetPOlineCount = nlapiGetLineItemCount('item'); 
					nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : line count ", intGetPOlineCount);
					
					
					var bLineError = false; 
					
					for(var z = 1; z <= intGetPOlineCount; z++){
						
						//nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : traversal", z);
						
						var idItemId = nlapiGetLineItemValue('item','item', z); 
						var strItemType = nlapiGetLineItemValue('item','itemtype', z);
						var bOverrideRetailValue = nlapiGetLineItemValue('item','custcol_erp_posp_override_retail_pric', z);
						
						
						
						if(strItemType == 'InvtPart' && bOverrideRetailValue == "T" && type == 'create'){
						
							nlapiSetLineItemValue('item', 'custcol_erp_posp_override_retail_pric', z, 'F'); 
							bOverrideRetailValue = "F";
						}
						
						
						
						//nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : traversal", z);
						nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : bOverrideRetailValue: ", bOverrideRetailValue);
						if(strItemType == 'InvtPart' && bOverrideRetailValue != "T"){
							var index = arrIdObjectArray.indexOf(idItemId);
							
							
							nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : finding the index index of " + idItemId , index);
							
							if(index >= 0 ){
								var obj = arrResultObjectArray[index]; 			
								nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : i am setting the price to ", obj.salesprice);
//								nlapiSetLineItemValue('item', 'custcol_erp_posp_sales_price', i, obj.salesprice);
								nlapiSetLineItemValue('item', 'custcol_ka_po_retail_value', z, obj.salesprice);
								
							}else{
//								nlapiSetLineItemValue('item', 'custcol_erp_posp_sales_price', i, 0.00);
								nlapiSetLineItemValue('item', 'custcol_ka_po_retail_value', z, 0.00);
							}
						}
						//nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : traversal", z);
						
						if(strItemType == 'InvtPart'){
//							var currSalesPrice = nlapiGetLineItemValue('item', 'custcol_erp_posp_sales_price', i);
							var currSalesPrice = nlapiGetLineItemValue('item', 'custcol_ka_po_retail_value', z);
							nlapiSetLineItemValue('item', 'custcol_erp_posp_retail_currency', z, pospCurrency);
							
							
							if(currSalesPrice <= 0){						
								bLineError = true; 
								nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : line (" + z + ") is zero", currSalesPrice);
							}
						}
						nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : traversal", z);													
						//nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : getPOItemArray: Item", strItemType);
					}
					
					nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : line is zero for some? ", bLineError);
					if(bLineError){
						nlapiSetFieldValue('custbody_posp_custom_status', 2); 
						nlapiSetFieldValue('orderstatus', 'A');
						nlapiSetFieldValue('approvalstatus', 1);
					}else{
						nlapiSetFieldValue('custbody_posp_custom_status', 3); 
						nlapiSetFieldValue('orderstatus', 'B');
						nlapiSetFieldValue('approvalstatus', 2);
						
					}	
					
				}
				
			}						
		}
				
		
//	}catch(ex){
//		
//	}

}


function getPOItemArray(){
	
	var arrP0Items = new Array();	
	var intGetPOlineCount = nlapiGetLineItemCount('item'); 
	
	for(var i = 1; i <= intGetPOlineCount; i++){
		
		var idItemId = nlapiGetLineItemValue('item','item', i); 
		var strItemType = nlapiGetLineItemValue('item','itemtype', i);
		
		if(strItemType == 'InvtPart'){
			arrP0Items.push(idItemId);			
		}
		 			
		//nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : getPOItemArray: Item", strItemType);
	}
	
	
	nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : getPOItemArray", arrP0Items.toString());
	return arrP0Items;		
}


function searchItem_Currency_PriceLevel(arrItems, arrCurrency, idPriceLevel){
	
	
	if(isNullOrEmpty(arrCurrency) || isNullOrEmpty(idPriceLevel)){
		
		return null; 
		
	}else{
		
		var arSavedSearchResultsItm = null;  
		
		var arSaveSearchFiltersItm = new Array();
		var arSavedSearchColumnsItm = new Array();
		
		var resultsArray = new Array(); // important array     	
		var idArray = new Array();  // important array	
		
		var strSavedSearchIDItm = null;	
		
		if(!isNullOrEmpty(arrItems)){
			nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : searchItem_Currency_PriceLevel: Item", arrItems);
		
			arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null , 'anyof', arrItems ));
		}
		
		nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : searchItem_Currency_PriceLevel: arrCurrency", arrCurrency);
		nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : searchItem_Currency_PriceLevel: idPriceLevel", idPriceLevel);		
		
		arSaveSearchFiltersItm.push(new nlobjSearchFilter( 'currency', 'pricing',  'anyof', arrCurrency));
		arSaveSearchFiltersItm.push(new nlobjSearchFilter( 'pricelevel', 'pricing',  'is', idPriceLevel));
				
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('name'));
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('currency','pricing'));
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('pricelevel','pricing'));
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('unitprice','pricing'));
			
		arSavedSearchResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchColumnsItm);
		
		if(!isNullOrEmpty(arSavedSearchResultsItm)){
			nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : searchItem_Currency_PriceLevel: Result Length", arSavedSearchResultsItm.length);			
		}else{
			nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : searchItem_Currency_PriceLevel: Result Length", "No Saved Search Result");
		}
		
		
		
				
		return arSavedSearchResultsItm; 
	}
}

function runItem_Currency_PriceLevel(arrItems, arrCurrency, idPriceLevel){
	
	var arrCurrency = [6];   var idPriceLevel = 1; 
	
	var searchField = 748; 
	
	var arSavedSearchColumnsItm = new Array();

	arSavedSearchColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('name'));
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('currency','pricing'));
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('pricelevel','pricing'));
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('unitprice','pricing'));
				
	var arSaveSearchFiltersItm = new Array();
	
	arSaveSearchFiltersItm.push(new nlobjSearchFilter( 'currency', 'pricing',  'anyof', [6]));  //arrCurrency));
	arSaveSearchFiltersItm.push(new nlobjSearchFilter( 'pricelevel', 'pricing',  'is', 1)); //idPriceLevel));			

	var searchField = 'customsearch_erp_posp_sales_price_lookup';
	var itemsSearch = nlapiLoadSearch('inventoryitem', searchField);		
//	itemsSearch.addColumns(arSavedSearchColumnsItm);
//	itemsSearch.addFilters(arSaveSearchFiltersItm);
	
	var resultSet = itemsSearch.runSearch();

	var counter = 0; 
	
	
	
	var intBatch = 500; 
	var start = 0;
	var end = 500; 
	
	do {
		
		results = resultSet.getResults(start, end);
		
		for(var i = 0; i < results.length; i++){
				
			try{
									
				
			}catch(ex){}
		
		}
					
		start += intBatch;
		end += intBatch;
	}while(results.length == intBatch);
	
	
	
	
//	console.log(resultSet.length); 
	
	if (resultSet != null){
		
		
		nlapiLogExecution('DEBUG', 'Run Search Length', actualResult.length ); 
//		var start = 0;
//		var end = intBatch;
//		var results = [];
//		
		/*nlapiLogExecution('ERROR', 'resultSet', JSON.stringify(resultSet));
		nlapiLogExecution('ERROR', 'resultSetlength', results.length);
		nlapiLogExecution('ERROR', 'searchField', searchField);*/
		
		
//		if (resultSet.length > 5000){			
//			//throw nlapiCreateError('ERROR', 'Cannot Process order. \n Search Result must be less than 5000', true);
//		}
	}
}



function runPriceLookup(POSPRefence,ShipCountry){
	
	nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : runPriceLookup : POSPRefence", POSPRefence);
	nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : runPriceLookup : ShipCountry", ShipCountry);
	
	var objExecute = new Object(); var bExecute = false; var foundPOSPRef = ""; 
	
	if(isNullOrEmpty(POSPRefence) && !isNullOrEmpty(ShipCountry)){
		
		nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : shiptocountry", ShipCountry);
		var countryname = getCountryName(ShipCountry);
					
		var strSavedSearchIDPOSP = null;
		var arSavedSearchResultsPOSP = null;
		
		var arSavedSearchColumnsPOSP = new Array();
		arSavedSearchColumnsPOSP.push(new nlobjSearchColumn('internalid'));
		

	    var filters = new Array();
	    filters[0] = new nlobjSearchFilter('formulatext', null, 'startswith', countryname);
	    filters[0].setFormula('{custrecord_erp_posp_country}');

		arSavedSearchResultsPOSP = nlapiSearchRecord('customrecord_po_sales_price', strSavedSearchIDPOSP, filters, arSavedSearchColumnsPOSP);


		if(arSavedSearchResultsPOSP){
							
			nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : Found the POSP", arSavedSearchResultsPOSP[0].getValue("internalid"));
			nlapiSetFieldValue('custbody_erp_posp_reference', arSavedSearchResultsPOSP[0].getValue("internalid"));
			
			bExecute = true; 
			
		}
		
		if(bExecute){
			
			 foundPOSPRef = nlapiGetFieldValue('custbody_erp_posp_reference'); 
			if(!isNullOrEmpty(foundPOSPRef)){
				bExecute = true;
			}else{
				//nlapiSetFieldValue('custbody_erp_posp_reference', ""); 
				nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : POSP Not found 2 "); 
				nlapiSetFieldValue('custbody_posp_custom_status', 1); 
				nlapiSetFieldValue('orderstatus', 'A');
				nlapiSetFieldValue('approvalstatus', 1);		
			}
			
		}
		
	}else if(!isNullOrEmpty(POSPRefence)){
		bExecute = true;
	}
	
	else{
		//nlapiSetFieldValue('custbody_erp_posp_reference', ""); 
		
		nlapiLogExecution("DEBUG", "PSOSP_UE_BeforeSubmit : POSP Not found 1 "); 
		nlapiSetFieldValue('custbody_posp_custom_status', 1); 
		nlapiSetFieldValue('orderstatus', 'A');
		nlapiSetFieldValue('approvalstatus', 1);
		
	}	
			
	if(isNullOrEmpty(POSPRefence) && !isNullOrEmpty(foundPOSPRef)){
		objExecute.id = foundPOSPRef; 
	}
	else{
		objExecute.id = POSPRefence;
	}
	
	objExecute.run = bExecute; 
	
	
	return objExecute;
	
}



function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}



function getCountryName(countryCode){



	  var countries_list = {
	AD:  'Andorra',
	AE:  'United Arab Emirates',
	AF:  'Afghanistan',
	AG:  'Antigua and Barbuda',
	AI:  'Anguilla',
	AL:  'Albania',
	AM:  'Armenia',
	AO:  'Angola',
	AQ:  'Antarctica',
	AR:  'Argentina',
	AS:  'American Samoa',
	AT:  'Austria',
	AU:  'Australia',
	AW:  'Aruba',
	AX:  'Aland Islands',
	AZ:  'Azerbaijan',
	BA:  'Bosnia and Herzegovina',
	BB:  'Barbados',
	BD:  'Bangladesh',
	BE:  'Belgium',
	BF:  'Burkina Faso',
	BG:  'Bulgaria',
	BH:  'Bahrain',
	BI:  'Burundi',
	BJ:  'Benin',
	BL:  'Saint Barth�lemy',
	BM:  'Bermuda',
	BN:  'Brunei Darrussalam',
	BO:  'Bolivia',
	BQ:  'Bonaire, Saint Eustatius, and Saba',
	BR:  'Brazil',
	BS:  'Bahamas',
	BT:  'Bhutan',
	BV:  'Bouvet Island',
	BW:  'Botswana',
	BY:  'Belarus',
	BZ:  'Belize',
	CA:  'Canada',
	CC:  'Cocos (Keeling) Islands',
	CD:  'Congo, Democratic People\'s Republic',
	CF:  'Central African Republic',
	CG:  'Congo, Republic of',
	CH:  'Switzerland',
	CI:  'Cote d\'Ivoire',
	CK:  'Cook Islands',
	CL:  'Chile',
	CM:  'Cameroon',
	CN:  'China',
	CO:  'Colombia',
	CR:  'Costa Rica',
	CU:  'Cuba',
	CV:  'Cape Verde',
	CW:  'Curacao',
	CX:  'Christmas Island',
	CY:  'Cyprus',
	CZ:  'Czech Republic',
	DE:  'Germany',
	DJ:  'Djibouti',
	DK:  'Denmark',
	DM:  'Dominica',
	DO:  'Dominican Republic',
	DZ:  'Algeria',
	EA:  'Ceuta and Melilla',
	EC:  'Ecuador',
	EE:  'Estonia',
	EG:  'Egypt',
	EH:  'Western Sahara',
	ER:  'Eritrea',
	ES:  'Spain',
	ET:  'Ethiopia',
	FI:  'Finland',
	FJ:  'Fiji',
	FK:  'Falkland Islands',
	FM:  'Micronesia, Federal State of',
	FO:  'Faroe Islands',
	FR:  'France',
	GA:  'Gabon',
	GB:  'United Kingdom (GB)',
	GD:  'Grenada',
	GE:  'Georgia',
	GF:  'French Guiana',
	GG:  'Guernsey',
	GH:  'Ghana',
	GI:  'Gibraltar',
	GL:  'Greenland',
	GM:  'Gambia',
	GN:  'Guinea',
	GP:  'Guadeloupe',
	GQ:  'Equatorial Guinea',
	GR:  'Greece',
	GS:  'South Georgia',
	GT:  'Guatemala',
	GU:  'Guam',
	GW:  'Guinea-Bissau',
	GY:  'Guyana',
	HK:  'Hong Kong',
	HM:  'Heard and McDonald Islands',
	HN:  'Honduras',
	HR:  'Croatia/Hrvatska',
	HT:  'Haiti',
	HU:  'Hungary',
	IC:  'Canary Islands',
	ID:  'Indonesia',
	IE:  'Ireland',
	IL:  'Israel',
	IM:  'Isle of Man',
	IN:  'India',
	IO:  'British Indian Ocean Territory',
	IQ:  'Iraq',
	IR:  'Iran (Islamic Republic of)',
	IS:  'Iceland',
	IT:  'Italy',
	JE:  'Jersey',
	JM:  'Jamaica',
	JO:  'Jordan',
	JP:  'Japan',
	KE:  'Kenya',
	KG:  'Kyrgyzstan',
	KH:  'Cambodia',
	KI:  'Kiribati',
	KM:  'Comoros',
	KN:  'Saint Kitts and Nevis',
	KP:  'Korea, Democratic People\'s Republic',
	KR:  'Korea, Republic of',
	KW:  'Kuwait',
	KY:  'Cayman Islands',
	KZ:  'Kazakhstan',
	LA:  'Lao People\'s Democratic Republic',
	LB:  'Lebanon',
	LC:  'Saint Lucia',
	LI:  'Liechtenstein',
	LK:  'Sri Lanka',
	LR:  'Liberia',
	LS:  'Lesotho',
	LT:  'Lithuania',
	LU:  'Luxembourg',
	LV:  'Latvia',
	LY:  'Libyan Arab Jamahiriya',
	MA:  'Morocco',
	MC:  'Monaco',
	MD:  'Moldova, Republic of',
	ME:  'Montenegro',
	MF:  'Saint Martin',
	MG:  'Madagascar',
	MH:  'Marshall Islands',
	MK:  'Macedonia',
	ML:  'Mali',
	MM:  'Myanmar',
	MN:  'Mongolia',
	MO:  'Macau',
	MP:  'Northern Mariana Islands',
	MQ:  'Martinique',
	MR:  'Mauritania',
	MS:  'Montserrat',
	MT:  'Malta',
	MU:  'Mauritius',
	MV:  'Maldives',
	MW:  'Malawi',
	MX:  'Mexico',
	MY:  'Malaysia',
	MZ:  'Mozambique',
	NA:  'Namibia',
	NC:  'New Caledonia',
	NE:  'Niger',
	NF:  'Norfolk Island',
	NG:  'Nigeria',
	NI:  'Nicaragua',
	NL:  'Netherlands',
	NO:  'Norway',
	NP:  'Nepal',
	NR:  'Nauru',
	NU:  'Niue',
	NZ:  'New Zealand',
	OM:  'Oman',
	PA:  'Panama',
	PE:  'Peru',
	PF:  'French Polynesia',
	PG:  'Papua New Guinea',
	PH:  'Philippines',
	PK:  'Pakistan',
	PL:  'Poland',
	PM:  'St. Pierre and Miquelon',
	PN:  'Pitcairn Island',
	PR:  'Puerto Rico',
	PS:  'Palestinian Territories',
	PT:  'Portugal',
	PW:  'Palau',
	PY:  'Paraguay',
	QA:  'Qatar',
	RE:  'Reunion Island',
	RO:  'Romania',
	RS:  'Serbia',
	RU:  'Russian Federation',
	RW:  'Rwanda',
	SA:  'Saudi Arabia',
	SB:  'Solomon Islands',
	SC:  'Seychelles',
	SD:  'Sudan',
	SE:  'Sweden',
	SG:  'Singapore',
	SH:  'Saint Helena',
	SI:  'Slovenia',
	SJ:  'Svalbard and Jan Mayen Islands',
	SK:  'Slovak Republic',
	SL:  'Sierra Leone',
	SM:  'San Marino',
	SN:  'Senegal',
	SO:  'Somalia',
	SR:  'Suriname',
	SS:  'South Sudan',
	ST:  'Sao Tome and Principe',
	SV:  'El Salvador',
	SX:  'Sint Maarten',
	SY:  'Syrian Arab Republic',
	SZ:  'Swaziland',
	TC:  'Turks and Caicos Islands',
	TD:  'Chad',
	TF:  'French Southern Territories',
	TG:  'Togo',
	TH:  'Thailand',
	TJ:  'Tajikistan',
	TK:  'Tokelau',
	TM:  'Turkmenistan',
	TN:  'Tunisia',
	TO:  'Tonga',
	TP:  'East Timor',
	TR:  'Turkey',
	TT:  'Trinidad and Tobago',
	TV:  'Tuvalu',
	TW:  'Taiwan',
	TZ:  'Tanzania',
	UA:  'Ukraine',
	UG:  'Uganda',
	UM:  'US Minor Outlying Islands',
	US:  'United States',
	UY:  'Uruguay',
	UZ:  'Uzbekistan',
	VA:  'Holy See (City Vatican State)',
	VC:  'Saint Vincent and the Grenadines',
	VE:  'Venezuela',
	VG:  'Virgin Islands (British)',
	VI:  'Virgin Islands (USA)',
	VN:  'Vietnam',
	VU:  'Vanuatu',
	WF:  'Wallis and Futuna Islands',
	WS:  'Samoa',
	XK:  'Kosovo',
	YE:  'Yemen',
	YT:  'Mayotte',
	ZA:  'South Africa',
	ZM:  'Zambia',
	ZW:  'Zimbabwe'};


	var strCountryName = "";
	strCountryName = countries_list[countryCode];
	if(strCountryName){
	  return strCountryName;
	}else{
	  return null;
	}

	}