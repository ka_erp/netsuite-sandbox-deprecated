/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Oct 2015     casual
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function removeZero_userEventAfterSubmit(type) {

	nlapiLogExecution("DEBUG", "Count", count);

	if (type != 'delete') {

		var transactionObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());

		var status = transactionObj.getFieldValue('orderstatus');
		nlapiLogExecution("DEBUG", "orderstatus", status);

		if (status == "B" || status == "H") {

			var count = transactionObj.getLineItemCount('item'); var totalcommitted = 0; 

			nlapiLogExecution("DEBUG", "Count", count);
          
            for (var i = 1; i <= count; i++) {
             	 var committed = transactionObj.getLineItemValue('item','quantitycommitted', i);
              	 if (committed > 0){
                   totalcommitted++; 
                 }
              
            }  
          
          
            if(totalcommitted  > 0 ){
          

                for (var i = 1; i <= count; i++) {

                    var committed = transactionObj.getLineItemValue('item','quantitycommitted', i);
                    nlapiLogExecution("DEBUG", "Count " + i, committed);

                    if (committed == 0) {
                        nlapiLogExecution("DEBUG", "Count " + i, 'removing this');
                        transactionObj.removeLineItem('item', i);
                        i--;
                        count--;
                    }
                }

                nlapiSubmitRecord(transactionObj);

           }   else{
             
               for (var i = 1; i <= count; i++) {
					
					var origQty = transactionObj.getLineItemValue('item','quantity', i);
					
					transactionObj.setLineItemValue('item','isclosed', i,'T');
					transactionObj.setLineItemValue('item','custcol_erp_backorder', i,origQty);
					transactionObj.setLineItemValue('item','custcol_erp_orig_qty', i,origQty);
					
               }
             
             	//nlapiSetFieldValue('orderstatus','G'); 
                nlapiSubmitRecord(transactionObj);
             
           }
		}

	}

}