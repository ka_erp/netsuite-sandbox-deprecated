/**
 *	File Name		:	ERP_3PL_IntegrationLogs_UE.js
 *	Function		:	Displays a sublist of Integration logs 
 *						this is a workaround for TO because while Custom Sublist can be applied to SO and PO, it cannot be applied to TO
 *						this is a workaround for IF because the search filter is different for IF
 * 	Remarks			:	
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{
	var LOGS_SEARCH = SCRIPTCONFIG.getScriptConfigValue('Integration Logs: Saved Search');
	var LOGS_SUBTAB = SCRIPTCONFIG.getScriptConfigValue('Integration Logs: Integrations Subtab');
	
	var TEXT_MAXLEN = 150;
	
	var LogList;
	var LogCols;
}


/*
*	name: beforeLoad_IntegrationLogs
*	descr: 	1) Display Integration Logs sublist
*
*	author: apple.villanueva@kitandace.com
*	@param: {type} type of operation on the record
*   @param: {form} the form the scirpt is deployed on 
*   @param: {request} the request sent to the script
*
*/
function beforeLoad_IntegrationLogs(type, form, request){

	if(nlapiGetContext().getExecutionContext() == 'userinterface' && (type == 'edit' || type == 'view')){
		
		//Get data from saved search
		var arResults;
		if(LOGS_SEARCH){
			
			var search = nlapiLoadSearch('', LOGS_SEARCH);
			
			//Add Search Filter
			if(nlapiGetRecordType() == 'itemfulfillment'){ 
				//Find logs for IF's Created From transaction
				search.addFilters([new nlobjSearchFilter('custrecord_transaction_source', 'custrecord_parentid', 'anyof', nlapiGetFieldValue('createdfrom'))]);
			} else{
				//Find logs for this transaction
				search.addFilters([new nlobjSearchFilter('custrecord_transaction_source', 'custrecord_parentid', 'anyof', nlapiGetRecordId())]);
			}
			
			//Run search
			var resultSet = search.runSearch();
			if(resultSet){
				arResults = getAllSearchResults(resultSet);
			}
					
			//Dynamically create and populate a sublist based on saved search
			populateLogsList(form, search, arResults);
		}
	}
}

function populateLogsList(form, objSearch, arResults){
	
	//Sublist
	createLogList(form, objSearch);
	
	var arCols;
	if(arResults){			
		for(var i = 0; i < arResults.length; i++){	
		
			//Populate Edit column
			LogList.setLineItemValue('custpage_edit', i+1, 
				'<a href="' 
				+ nlapiResolveURL('RECORD', 'customrecord_integrationlogs_child', arResults[i].getId(), 'EDIT') 
				+ '" target="_blank">Edit</a>');
				
			//Populate Columns from saved search
			arCols = arResults[i].getAllColumns();
			for(var j = 0; j < arCols.length; j++){
				LogList.setLineItemValue('custpage_' + arCols[j].getName(), i+1, getDisplayLogListColValue(arResults[i], arCols[j]));
			}
		}		
	}
}

function createLogList(form, objSearch){
	
	//Columns mapping
	LogCols = {};
	
	//Sublist
	LogList = form.addSubList('custpage_list_integrationlogs', 'list', 'Integration Logs', LOGS_SUBTAB);
	
	//Edit column
	LogList.addField('custpage_edit', 'text', 'Edit');
	
	//Columns from saved search
	var colId;
	var colLabel;
	var colType;
	var arCols = objSearch.getColumns();
	for(var i = 0; i < arCols.length; i++){	
	
		colId = arCols[i].getName();
		colLabel = arCols[i].getLabel();
		colType = arCols[i].getType();
		
		//Store column properties
		//Get Text of result if column is of type Select
		LogCols[colId] = {label: colLabel, type: colType, display: colType == 'select' ? 'text' : 'value'};
	
		//Special case for column of type Transaction
		if(colLabel.toUpperCase().indexOf('TRANSACTION') > -1){
			
			LogList.addField('custpage_' + colId, 'select', colLabel, 'transaction');
			
			//Get Value of result
			LogCols[colId].display = 'value';
			
		} else {
			LogList.addField('custpage_' + colId, mapSSColType(arCols[i].getType()), colLabel);
		}
	}
}

function mapSSColType(ssColType){
	
	var sublistFieldType = '';
	
	switch(ssColType){
		case 'datetime':
		case 'text':
		case 'select':
			sublistFieldType = 'text';
			break;
		case 'clobtext':
			sublistFieldType = 'textarea';
			break;
		default:
			sublistFieldType = 'text';
			break;			
	}
	
	return sublistFieldType;
}

function getDisplayLogListColValue(objResult, objCol){
	
	var displayValue;
	
	if(LogCols[objCol.getName()].display == 'text'){
		displayValue = objResult.getText(objCol.getName(), objCol.getJoin());
	} else {
		displayValue = objResult.getValue(objCol.getName(), objCol.getJoin());
	}
	
	if(LogCols[objCol.getName()].type == 'clobtext'){
		displayValue = getDisplayText(displayValue);
	}
	
	return displayValue;
}

function getDisplayText(stText){
	if(stText.length > TEXT_MAXLEN){
		stText = stText.substring(0, TEXT_MAXLEN-1) + '(more...)';
	}
	return stText;
}
