/**
 *	File Name		:	ERP_3PL_Button_CL.js
 *	Function		:	
 * 	Remarks			:	
 *	Prepared by		:	christopher.neal@kitandace.com, apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/


/*
*	name: callButtonSuitelet
*	descr:  Function which is triggered when the 'Send to 3PL' button is pushed on a 'Sales Order' or 'Transfer Order'
*			Calls a suitelet to send a message to the 3PL
*
*	author: Wipro Ltd.
*	updated: christopher.neal@kitandace.com
*
*/
function callButtonSuitelet_sendTo3pl(){

	nlapiDisableField('custpage_sendto3pl_btn', true);

	var url 	 = nlapiResolveURL('SUITELET', 'customscript_erp_3pl_webstoresuitelet_su', 'customdeploy_erp_3pl_webstoresuitelet_su'); 
																																			
	var TranNo 	 = nlapiLookupField(nlapiGetRecordType(), nlapiGetRecordId(), 'tranid');
	var response = nlapiRequestURL(url + '&type=custom&rectype=' + nlapiGetRecordType() + '&recid=' + nlapiGetRecordId() + '&tranno=' + TranNo + '&stats=' + encodeURI(nlapiGetFieldValue('status')), null, null);

	//nlapiLogExecution('DEBUG', 'Response Body', response.getBody());
	nlapiDisableField('custpage_sendto3pl_btn', false);
	
	if(response.getBody() == 'T'){
		alert('Message sent to Cloudhub successfully');
	} else {
		alert('Message to Cloudhub failed. Please check Integration logs.');
	}

	location.reload(true);
}
