
function getItemType(type) {
    var itemType = "";

    //var itemTypePlaceholder = nlapiLookupField('item', itemId, 'type');
    var itemTypePlaceholder = type;

    switch (itemTypePlaceholder) {
        case "InvtPart":
            itemType = "inventoryitem";
            break;
        case "NonInvtPart":
            itemType = "noninventoryitem";
            break;
        case "Group":
            itemType = "itemgroup";
            break;
        case "Kit":
            itemType = "kititem";
            break;
        case "Assembly":
            itemType = "assemblyitem";
            break;
        case "Service":
            itemType = "serviceitem";
            break;
        case "Description":
            itemType = "descriptionitem";
            break;
        case "Discount":
            itemType = "discountitem";
            break;
        case "OthCharge":
            itemType = "otherchargeitem";
            break;
        case "GiftCert":
            itemType = "giftcertificateitem";
            break;
        case "Markup":
            itemType = "markupitem";
            break;
        case "Payment":
            itemType = "paymentitem";
            break;
        case "Subtotal":
            itemType = "subtotalitem";
            break;
    }

    return itemType;
}




