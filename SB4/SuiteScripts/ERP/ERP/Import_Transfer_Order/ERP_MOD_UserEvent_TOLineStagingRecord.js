/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_UserEvent_TOLineStagingRecord
 * Version            1.0.0.0
 **/

function onBeforeLoad(type, form, request) {

}

function onBeforeSubmit(type) {

	Log.d("onAfterSubmit type", type);
	
    if ( type == 'delete' ) {
    	
    	
    	parentID = nalpiGetFieldValue('custrecord_transfer_stage_header');
    	 Log.d("onAfterSubmit", "Record ID:  " + parentID + "trying to reset the header transfer order message");
    	
    	parentID = Number(parentID); 
    	nlapiSubmitField('customrecord_tran_order_staging_header',parentID, 'custrecord_transfer_error_message', "");
    }
}

function onAfterSubmit(type) {

	var recordID = nlapiGetRecordId();
    if ( type != 'delete' ) {

        
        Log.d("onAfterSubmit", "Record ID:  " + recordID);
        var rec = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());

        // If a record is inactivated - make sure the status is set to Error-Cancel
        if ( rec.getFieldValue("isinactive") == "T" ){
            Log.d("onAfterSubmit -- Record ID:  " + recordID, "Record is Inactive - setting status to Error-Cancel");
            rec.setFieldValue("custrecord_transfer_stage_line_status", TO_LineStagingStatus.ErrorCancel);
        }

        // If the adjustment quantity is set to 0 - then set the status to Completed.  No 0 quantity adjustments will be done.
        if ( rec.getFieldValue("custrecord_transfer_quantity") == 0 ){
            Log.d("onAfterSubmit -- Record ID:  " + recordID, "Record has no Transfer Quantity - setting status to Error-Cancel");
            rec.setFieldValue("custrecord_transfer_stage_line_status", TO_LineStagingStatus.ErrorCancel);
        }

        var id = nlapiSubmitRecord(rec, true, true);
        Log.d("onAfterSubmit", "Re-saving Record ID:  " + id);
    }

}

Log.AutoLogMethodEntryExit(null, true, true, true);