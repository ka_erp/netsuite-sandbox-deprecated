/**
 *	File Name		:	ERP_WPORT_SalesOrder_ICOAutomation_UE.js
 *	Function		:	ICSO Automation
 * 	Remarks			:	Relevant scripts isolated from WTKA_NetSuitetoMulesoft.js
 *	Prepared by		:	A.Villanueva
 *	Release Dates	:	06-Jun-2016
 * 	Current Version	:	1.0
**/

{
	var ErrorObj 		 		= new Object();
	ErrorObj.status				= "Error";
	ErrorObj.messages 	 		= new Array();

	var finalResponse			= new Object();
	finalResponse.status 		= "";
	finalResponse.records		= new Array();

	var finalMessage 		 	= new Object();
	finalMessage.records	 	= new Array();

	var orderDet	 = new Object(), orderDetails	= new Array();
	var rollbackFlag = false,		 sendEmail 		= true; //false;
	var record, 					 location;
	var ccList = null; //['']; //Enter CC Addresses as array
	var toList = ['3PL_eComm_Integration@kitandace.com'];
}

function userEventAfterSubmit_ICSO(type)
{
	var recType 		= nlapiGetRecordType();
	var order_context 	= nlapiGetContext().getExecutionContext();
	var subject, body;
	if((recType == 'salesOrder' || recType == 'salesorder') && (order_context == 'webstore' || order_context == 'userinterface' || order_context == 'scheduled' || order_context == 'userevent'))
	{
		if(type == 'create' || type == 'edit' || type == 'xedit')
		{
			var record = nlapiLoadRecord(recType, nlapiGetRecordId());
			//Restrict call to 3PL for Hybris Orders based on custbody_wtka_extsys_order_number field value
			var extOrder = record.getFieldValue('custbody_wtka_extsys_order_number');
			extOrder = (extOrder != null && extOrder.length > 0) ? extOrder : null;

			if(extOrder == null)
			{
				if(order_context == 'userevent')
				{
					var sendTo3PL = ICSO_Process(record);
					if(sendTo3PL)	nlapiLogExecution('DEBUG', 'REM UNITS', nlapiGetContext().getRemainingUsage());
				}
			}
		}
		else
		{
			nlapiLogExecution('DEBUG', 'ELSE - order_context: ' + order_context, 'type: ' + type);
		}
	}
}

function ICSO_Process(record) // On after submit of ICSO
{
	try
	{
		var poCreatedFrom, customer = 0, vendor;
		var soNumber 		 = record.getFieldValue('tranid');
		var soStatus 		 = record.getFieldValue('statusRef');
		var icsoTranDate 	 = record.getFieldValue('trandate');
		var extOrder 		 = record.getFieldValue('custbody_wtka_extsys_order_number');
		var extOrderHybris   = record.getFieldValue('custbody_wtka_extsys_hybris_order');

		extOrder 			 = (extOrder != null && extOrder.length > 0) ? extOrder : null;
		var icsoRecterms 	 = record.getFieldValue('terms');
		var icsoRecpaymeth 	 = record.getFieldValue('paymentmethod');
		var icsoFinalTransformType = ((icsoRecterms != null && icsoRecterms.length > 0) || (icsoRecpaymeth != null && icsoRecpaymeth.length > 0)) ? 'cashsale' : 'invoice';
		var interCompanyTrans = (record.getFieldValue('intercotransaction') > 0) ? record.getFieldValue('intercotransaction') : 0;

		var orderIdExternal = nlapiLookupField('purchaseorder', interCompanyTrans, 'custbody_wtka_extsys_hybris_order'); //Get the external ID from the Purchase Order (Chris Neal: for some reason I can't get it from the Sales Order)
		nlapiLogExecution('DEBUG', 'orderIdExternal', orderIdExternal);

		var wtkaExternalOrderInternalId = getWTKAExternalOrderId(orderIdExternal); //get the Netsuite Internal ID of the WTKA_External_Order record

		var logMessage = 'ICSO) Started Inter Company Sales Order for PO: ' + interCompanyTrans;
		var logId = createTransactionLog(null, orderIdExternal, null, null, null, 15, SYNC_STATUS.CREATED, logMessage, interCompanyTrans, 60); //Library call to WTKA_Library.js //Record Type: 15 is Purchase Order
		
		// Need to be processed only when ICSO is performed and when PO & SO are linked
		if((type == 'create' || type == 'edit') && soStatus == 'pendingFulfillment' && interCompanyTrans > 0)
        {
			var requestObject = '';
			if(extOrder == null) //Update the ICSO with custom field values
			{
				//Fetch extOrder from Post
				var poRecord 	= nlapiLoadRecord('purchaseorder', interCompanyTrans);
				vendor			= poRecord.getFieldValue('entity');
				extOrder 		= poRecord.getFieldValue('custbody_wtka_extsys_order_number');
				if(extOrder == null || (extOrder != null && extOrder.length <= 0)) return false; 
				var extOrderID 	= poRecord.getFieldValue('custbody_wtka_extsys_hybris_order');
				poCreatedFrom 	= poRecord.getFieldValue('createdfrom');
				if(poCreatedFrom == null || (poCreatedFrom != null && poCreatedFrom.length <= 0)) return false; //not drop shipped;
				requestObject	= poRecord.getFieldValue('custbody_wtka_request_data');
				if(requestObject == null || (requestObject != null && requestObject.length <= 0)) return false; //no custom record entry;
				
				nlapiLogExecution('DEBUG', 'Intercompany SO automation');
				var soRec 		= nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
				customer		= soRec.getFieldValue('entity');
				soRec.setFieldValue('custbody_wtka_extsys_order_number', extOrder);
				soRec.setFieldValue('custbody_wtka_extsys_hybris_order', extOrderID);
				soRec.setFieldValue('custbody_wtka_request_data', 		 requestObject);
				var soRecId = nlapiSubmitRecord(soRec, false, true);

				var logMessage = 'ICSO) Created Sales Order: ' + soRecId;
				createTransactionLog(null, orderIdExternal, null, null, null, 31, SYNC_STATUS.CREATED, logMessage, soRecId, 62); //Library call to WTKA_Library.js //Record Type: 31 is Sales Order

			}
			try
			{
				finalMessage.orderNumber 	= extOrder;
				finalMessage.nsICSONumber 	= soNumber;
				var orderDet				= new Object();
				orderDet.icso				= true;
				orderDet.orderId 			= poCreatedFrom;
				orderDet.poId				= interCompanyTrans;
				orderDet.icsoId				= soRecId;

				var ICSOInvoice = extOrder;

				try
				{
					//Load Originating Sales Order Items and Populate Array  - This will be used to check if UPC code available at the time of fulfillment
					var soOriginatingRec = nlapiLoadRecord('salesorder', poCreatedFrom);
					var arrayOriginatingSORec= new Array();
					for(var itemIndex=0; itemIndex < soOriginatingRec.getLineItemCount('item'); itemIndex++)
					{
						var originalOrderDetail	= new Object();
						originalOrderDetail.quantityFulfilledOriginatingSORec = parseInt(soOriginatingRec.getLineItemValue('item', 'quantityfulfilled', itemIndex+1));
						originalOrderDetail.upcCodeOriginatingSORec=soOriginatingRec.getLineItemValue('item', 'custcol_wtka_upccode', itemIndex+1);		
						arrayOriginatingSORec[itemIndex] = originalOrderDetail;						
					}
										
					// 1. Create Item Fulfillment on SO
					nlapiLogExecution('DEBUG', 'Fulfilling ICSO');
					var icsoItemFulfill = nlapiTransformRecord(nlapiGetRecordType(), nlapiGetRecordId(), 'itemfulfillment');
					for(var i=0; i < icsoItemFulfill.getLineItemCount('item'); i++)
					{
						for (var originalSOItemCount=0; originalSOItemCount<arrayOriginatingSORec.length; originalSOItemCount++ )
						{
							/*Fetch Item UPC code */
							var fulfillItemUPCCode=icsoItemFulfill.getLineItemValue('item', 'custcol_wtka_upccode', i+1);	
							if (arrayOriginatingSORec[originalSOItemCount].upcCodeOriginatingSORec== fulfillItemUPCCode)
							{
								if(arrayOriginatingSORec[originalSOItemCount].quantityFulfilledOriginatingSORec>0)
								{
									icsoItemFulfill.setLineItemValue('item', 'fulfill', i+1, 'T');
									icsoItemFulfill.setLineItemValue('item', 'quantity',  i+1, arrayOriginatingSORec[originalSOItemCount].quantityFulfilledOriginatingSORec);
								}
								else
								{
									icsoItemFulfill.setLineItemValue('item', 'fulfill', i+1, 'F');
									icsoItemFulfill.setLineItemValue('item', 'quantity',  i+1, 0);
									icsoItemFulfill.setLineItemValue('item', 'quantity',  i+1, 0);
								}								
								break;
							}
						}
					}
					icsoItemFulfill.setFieldValue('trandate', 	icsoTranDate);
					icsoItemFulfill.setFieldText('shipstatus', 	'shipped');

					var icsoFulfillmentId 		= nlapiSubmitRecord(icsoItemFulfill, true);
					var logMessage = 'ICSO) Created Item Fulfillment: ' + icsoFulfillmentId;
					createTransactionLog(null, orderIdExternal, null, null, null, 32, SYNC_STATUS.CREATED, logMessage, icsoFulfillmentId, 63); //Library call to WTKA_Library.js //Record Type: 32 is Item Fulfillment

					orderDet.icsoFulfillmentId	= icsoFulfillmentId;

					var fulfillmentObject 				= new Object();
					fulfillmentObject.status 			= "Success";
					fulfillmentObject.transactiontype 	= "ICSO Item Fulfillment";
					fulfillmentObject.transactionid 	= icsoFulfillmentId;
					fulfillmentObject.transactionNumber = nlapiLookupField('itemfulfillment', icsoFulfillmentId, 'tranid');
					fulfillmentObject.transactiondate 	= icsoTranDate;
					finalMessage.records.push(fulfillmentObject);
				}
				catch(icso_fulfill)
				{
					nlapiLogExecution('DEBUG', 'ERR - icso_fulfill', icso_fulfill);
					rollbackFlag 	 		= true;
					var  errObject 			= new Object();
					errObject.status  		= "Exception";
					errObject.messagetype 	= "Intercompany Sale Order Item Fulfillment Failure";
					errObject.message 		= 'Intercompany Sale Order Item Fulfillment record could not be created. Details as below.';
					errObject.details 		= icso_fulfill.getCode() + ' : ' + icso_fulfill.getDetails();
					finalMessage.records.push(errObject);
					
					createTransactionLog(null, orderIdExternal, null, null, null, 32, SYNC_STATUS.ERROR, JSON.stringify(errObject), null, 68); //Library call to WTKA_Library.js //Record Type: 32 is Item Fulfillment
				}
				try
				{
					if(!rollbackFlag)
					{
						// 2. Create Invoice for ICSO
						nlapiLogExecution('DEBUG', 'Billing ICSO');
						var icsoInvoice   			= nlapiTransformRecord(nlapiGetRecordType(), nlapiGetRecordId(), icsoFinalTransformType);
						icsoInvoice.setFieldValue('trandate', icsoTranDate);
						var icsoAccount				= nlapiLookupField('entity', customer, 'custentity_erp_account_flagship');
						if(icsoAccount != '')	icsoInvoice.setFieldValue('account',  icsoAccount);
						var icsoFinalRecordId 		= nlapiSubmitRecord(icsoInvoice, true, true);

						var logMessage = 'ICSO) Created Invoice: ' + icsoFinalRecordId;
						createTransactionLog(null, orderIdExternal, null, null, null, 7, SYNC_STATUS.CREATED, logMessage, icsoFinalRecordId, 64); //Library call to WTKA_Library.js //Record Type: 7 is Invoice

						orderDet.icsoInvoiceId		= icsoFinalRecordId;
						orderDet.icsoTransformType	= icsoFinalTransformType;

						var invoiceObject 					= new Object();
						invoiceObject.status 				= "Success";
						invoiceObject.transactiontype 		= (icsoFinalTransformType == 'cashsale') ? "ICSO Cash Sale" : "ICSO Invoice";
						invoiceObject.transactionid 		= icsoFinalRecordId;
						invoiceObject.transactionNumber 	= nlapiLookupField(icsoFinalTransformType, icsoFinalRecordId, 'tranid');
						ICSOInvoice 						= invoiceObject.transactionNumber;
						invoiceObject.transactiondate 		= icsoTranDate;
						finalMessage.records.push(invoiceObject);
					}
				}
				catch(icso_invoice)
				{
					nlapiLogExecution('DEBUG', 'ERR - icso_invoice', icso_invoice);
					rollbackFlag 	 		= true;
					var  errObject 			= new Object();
					errObject.status  		= "Exception";
					errObject.messagetype 	= "Intercompany Sale Order Cash Sale / Invoice Failure";
					errObject.message 		= 'Intercompany Sale Order Cash Sale/Invoice record could not be created. Details as below.';
					errObject.details 		= icso_invoice.getCode() + ' : ' + icso_invoice.getDetails();
					finalMessage.records.push(errObject);
					
					createTransactionLog(null, orderIdExternal, null, null, null, 7, SYNC_STATUS.ERROR, JSON.stringify(errObject), null, 69); //Library call to WTKA_Library.js //Record Type: 7 is Invoice
				}

				try
				{
					if(!rollbackFlag)
					{
						// 3. Bill ICPO
						nlapiLogExecution('DEBUG', 'Billing PO');
						var poInvoice   			= nlapiTransformRecord('purchaseorder', interCompanyTrans, 'vendorbill');
						var poAccount				= nlapiLookupField('entity', vendor, 'custentity_erp_account_flagship');
						if(poAccount != '')	poInvoice.setFieldValue('account',   poAccount);
						poInvoice.setFieldValue('trandate',  icsoTranDate);
						poInvoice.setFieldValue('tranid', 	 ICSOInvoice);
						var poFinalRecordId 		= nlapiSubmitRecord(poInvoice, true, true);

						var logMessage = 'ICSO) Billed Purchase Order: ' + poFinalRecordId;
						createTransactionLog(null, orderIdExternal, null, null, null, 15, SYNC_STATUS.CREATED, logMessage, poFinalRecordId, 65); //Library call to WTKA_Library.js //Record Type: 15 is Purchase Order

						orderDet.poInvoiceId		= poFinalRecordId;
						
						var invoiceObject 					= new Object();
						invoiceObject.status 				= "Success";
						invoiceObject.transactiontype 		= "PO Vendor Bill";
						invoiceObject.transactionid 		= poFinalRecordId;
						invoiceObject.transactionNumber 	= extOrder;
						invoiceObject.transactiondate 		= icsoTranDate;
						finalMessage.records.push(invoiceObject);
					}
				}
				catch(po_invoice)
				{
					nlapiLogExecution('DEBUG', 'ERR - po_invoice', po_invoice);
					rollbackFlag 	 		= true;
					var  errObject 			= new Object();
					errObject.status  		= "Exception";
					errObject.messagetype 	= "Purchase Order Billing Failure";
					errObject.message 		= 'Purchase Order Vendor Bill record could not be created. Details as below.';
					errObject.details 		= po_invoice.getCode() + ' : ' + po_invoice.getDetails();
					finalMessage.records.push(errObject);
					
					createTransactionLog(null, orderIdExternal, null, null, null, 15, SYNC_STATUS.ERROR, JSON.stringify(errObject), null, 70); //Library call to WTKA_Library.js //Record Type: 15 is Purchase Order
				}
				
				try
				{
					/*4.  Closing partial fulfilled ICSO sales Orders and Purchase Orders FI973 */
					
					if(!rollbackFlag)
					{	
						nlapiLogExecution('DEBUG', 'Closing Partial Records');
						//Close PO and ICSO
						if(orderDet.icsoId > 0)
						{
							var icsoSalesOrder  = nlapiLoadRecord('salesorder', orderDet.icsoId);
							var icsoStatus=  icsoSalesOrder.getFieldValue('status');
							/* Only set items as closed those are not fulfilled*/	
							if (icsoStatus=='Partially Fulfilled') 
							{
								for(var lineItemIndex = 0; lineItemIndex<icsoSalesOrder.getLineItemCount('item'); lineItemIndex++)
								{
									icsoSalesOrder.setLineItemValue('item', 'isclosed', lineItemIndex+1, 'T');
								}
								var icsoOrderInternalID = nlapiSubmitRecord(icsoSalesOrder, false, true);

								var logMessage = 'ICSO) Closed Sales Order: ' + icsoOrderInternalID;
								createTransactionLog(null, orderIdExternal, null, null, null, 31, SYNC_STATUS.CREATED, logMessage, icsoOrderInternalID, 66); //Library call to WTKA_Library.js //Record Type: 31 is Sales Order
							}
						}
						if(orderDet.poId > 0)
						{
							var purchaseRecord = nlapiLoadRecord('purchaseorder', orderDet.poId);
							var poStatus=  purchaseRecord.getFieldValue('status');
							
							if (poStatus=='Partially Received') 
							{
								for(var lineItemIndex = 0; lineItemIndex<purchaseRecord.getLineItemCount('item'); lineItemIndex++)
								{
								   purchaseRecord.setLineItemValue('item', 'isclosed', lineItemIndex+1, 'T');
								}
								var purchaseOrderInternalID = nlapiSubmitRecord(purchaseRecord, false, true);

								var logMessage = 'ICSO) Closed Purchase Order: ' + purchaseOrderInternalID;
								createTransactionLog(null, orderIdExternal, null, null, null, 15, SYNC_STATUS.CREATED, logMessage, purchaseOrderInternalID, 67); //Library call to WTKA_Library.js //Record Type: 15 is Purchase Order
							}
						}
						
					}					
					
				}
				catch(close_partial_fulfilled)
				{
					nlapiLogExecution('DEBUG', 'ERR - close_partial_fulfilled', close_partial_fulfilled);
					rollbackFlag 	 		= true;
					var  errObject 			= new Object();
					errObject.status  		= "Exception";
					errObject.messagetype 	= " Closing Sales Order or Purchase Order Failure";
					errObject.message 		= 'Closing Sales Order or Purchase Order. Details as below.';
					errObject.details 		= close_partial_fulfilled.getCode() + ' : ' + close_partial_fulfilled.getDetails();
					finalMessage.records.push(errObject);	

					createTransactionLog(null, orderIdExternal, null, null, null, null, SYNC_STATUS.ERROR, JSON.stringify(errObject), null, 71); //Library call to WTKA_Library.js
				}

				requestObject 	= JSON.parse(requestObject);
				var fields 		= ['custrecord_wtka_final_response', 'custrecord_wtka_processed_ids'];
				var custValues  = nlapiLookupField('customrecord_wtka_external_orders', requestObject.id, fields);
				var resp 		= custValues.custrecord_wtka_final_response;
				if(resp != null)	finalResponse = JSON.parse(resp);
				resp 			= custValues.custrecord_wtka_processed_ids;
				if(resp != null)	orderDetails  = JSON.parse(resp);

				if(!rollbackFlag)
				{
					orderDetails.push(orderDet);
				}
				else
				{
					finalResponse.status = "Error";
					/*var rollbackStatus = rollbackICSO(orderDet);
					var newObj 	 = new Object();
					if(rollbackStatus)
					{
						newObj.status	= 'Rollback Success';
						newObj.message	= 'The process has been successfully rolled back and all the transactions listed above have been closed and deleted.';
					}
					else
					{
						newObj.status 	= 'Rollback Failure';
						newObj.message 	= 'The process failed to rollback.';
					}
					finalMessage.records.push(newObj);*/
				}
				finalResponse.records.push(finalMessage);
				nlapiLogExecution('debug', 'Final State of ICTO Flow', JSON.stringify(finalResponse));
				var status 		 = (rollbackFlag) ? 6 : 4;
				var rollbackStat = null;

				setRecordFieldValues(requestObject.id, orderDetails, finalResponse, rollbackStat, status, null, -1);
			}
			catch(other_err)
			{
				nlapiLogExecution('debug', 'Error in Processing ICTO', other_err);
				var finalResp	  = new Object();
				finalResp.status  = "Exception";
				finalResp.message = "Unable to Process Orders. Error Details: ";
				finalResp.message = other_err;
				finalResponse.status = "Error";
				finalResponse.records.push(finalResp);
				setRecordFieldValues(requestObject.id, null, finalResponse, null, 6);
				linkTransactionsToExternalWTKA(wtkaExternalOrderInternalId);
				
				createTransactionLog(null, orderIdExternal, null, null, null, null, SYNC_STATUS.ERROR, JSON.stringify(finalResp), null, 72); //Library call to WTKA_Library.js
				
				return false;
			}
			linkTransactionsToExternalWTKA(wtkaExternalOrderInternalId);
		}
		else
		{
			return false;
		}
	}
	catch(icso_err)
	{
		nlapiLogExecution('debug', 'Unable to Process ICTO Orders', icso_err);
		var subject = 'Intercompany Order processing failure';
		var body = 'Hello,<br><br>';
		body += 'Intercompany Order processing failed in NetSuite due to below error.<br>';
		body += '<br><b>Error Details: </b><br>' + JSON.stringify(icso_err);
		body += '<br><br>Process needs to be handled manually through user interface after corrections.';
		body += '<br><br><br><br><br><br><br>Thanks';
		body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
		nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
		
		//Use Script Config setting
		var stCCList = SCRIPTCONFIG.getScriptConfigValue('Hybris: Email Notification: CC List');
		if(stCCList) ccList = JSON.parse(stCCList);
		
		var stToList = SCRIPTCONFIG.getScriptConfigValue('Hybris: Email Notification: To List');
		if(stToList) toList = JSON.parse(stToList);
		
		var emailAuthor = SCRIPTCONFIG.getScriptConfigValue('Hybris: Email Notification: Author');
		
		if(sendEmail)	nlapiSendEmail(emailAuthor, toList, subject, body, ccList);
		
		createTransactionLog(null, orderIdExternal, null, null, null, null, SYNC_STATUS.ERROR, JSON.stringify(icso_err), null, 73); //Library call to WTKA_Library.js

		linkTransactionsToExternalWTKA(wtkaExternalOrderInternalId);

		return false;
	}
	return true;
}