/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Dec 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function erp_main_transaction_ue_bl(type, form, request){
 
	calculateTransactionTotalQuantity(); 
}


function erp_main_transaction_ue_bs(type){
	
	//fieldValidations(); This was the old code
	
	var recType = nlapiGetRecordType(); 
	
	if(recType == 'transferorder'){
		calculateTransactionTotalQuantity();	//total
	}
	else if(recType  == 'salesorder'){		
		fieldValidations();
	}
			
}

function erp_main_transaction_ue_as(type){
 
}




function fieldValidations(){
	
	validateLocationField();	//Validate Location  // This is not hardcoded to check sales order, but is currently deployed in the SO record
	
}



function fieldCalculations(){
	
	
	
}

function columnValidations(){
	
}

function columnCalculations(){
	
}

function validateLocationField(){
				
		var exec_context = nlapiGetContext().getExecutionContext();
		
		nlapiLogExecution("AUDIT", "func: validateLocationField: CONTEXT", JSON.stringify(exec_context));		
		nlapiLogExecution('AUDIT','func: validateLocationField: TYPE', type); 
		 		
			
		if(exec_context == "userevent" && type == 'create' ){
		
			var location = nlapiGetFieldValue('location');
			
			if(location == null || location == ''){
				throw nlapiCreateError('ERROR', 'Cannot create this "Sales Order".\n Sales Order "Location" must be set', true);
						
			}
			
		}		
	
}

function calculateTransactionTotalQuantity(){
	
	
	try{
		
		var a = nlapiGetLineItemCount('item'); 
		
		nlapiLogExecution("AUDIT", "func: calculateTransactionTotalQuantity", 'setting the qty');	
		
		if(a > 0){
			var b = 0;  
			
			for(var i = 1; i <= a; i++){ 
				b  = b + Number(nlapiGetLineItemValue('item','quantity',i)); 
			} 
			
			nlapiSetFieldValue('custbody_erp_total_qty_excolor', b); 
			
		}
		
	}catch(ex){
		
	}
}