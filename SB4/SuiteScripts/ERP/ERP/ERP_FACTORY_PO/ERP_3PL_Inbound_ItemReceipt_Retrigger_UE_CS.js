/**
 *	File Name		:	ERP_3PL_Inbound_ItemReceipt_Retrigger_UE_CS.js
 *	Function		:	Performs retrigger for Inbound Item Receipt Message staging record
 * 	Remarks			:	
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

/**
 * BEFORE LOAD
 */
function beforeLoad_ShowRetriggerButton(type, form, request){
	
	if(nlapiGetContext().getExecutionContext() == 'userinterface' && type == 'view'){
		
		//If Retrigger checkbox is checked
		if(nlapiGetFieldValue('custrecord_itemreceipt_retrigger') == 'T'){
			
			var STATUS_TOPROCESS = SCRIPTCONFIG.getScriptConfigValue('IR Processed Status: To Process');
			
			//Show button
			form.addButton('custpage_retrigger_btn', 'Retrigger', 'client_Retrigger(' + nlapiGetRecordId() + ',' + STATUS_TOPROCESS + ')');
			form.setScript('customscript_erp_3pl_irmsg_retrigger_cs');
		}		
	}	
}

/**
 * CLIENT
 */
function client_Retrigger(stRecId, stStatus){
	
	//Change Status to trigger UE After Submit script
	nlapiSubmitField('customrecord_erp_3pl_inbound_itemreceipt', stRecId, 'custrecord_itemreceipt_status', stStatus);
	
	alert('Retrigger done. Please check Integration logs.');
	
	//Reload record
	location.reload(true);
}
