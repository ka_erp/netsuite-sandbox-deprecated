/**
 *	File Name		:	ERP_PurchaseOrder_RS.js
 *	Function		:	
 * 	Remarks			:	
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{
	var STATUS_ACCEPTED = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Accepted');
    var STATUS_REJECTED = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Rejected');
	var STATUS_READY = SCRIPTCONFIG.getScriptConfigValue('IR Processed Status: Ready');
	var STATUS_TOPROCESS = SCRIPTCONFIG.getScriptConfigValue('IR Processed Status: To Process');
	
	var ITEM_RECEIPT_FOLDER_ID = SCRIPTCONFIG.getScriptConfigValue('Factory PO: IR Message Folder ID'); //682322 is the folder ID in SB3
	
	var LogDirection_Outbound 	= 2; //2 - Outbound Call, 1 - Inbound call
	var LogDirection_Inbound 	= 1; //2 - Outbound Call, 1 - Inbound call
		
	var MESSAGE_OUTBOUND_PROCESS_FAIL = 'NetSuite failed to generate PO object';
	var MESSAGE_INBOUND_PROCESS_FAIL = 'NetSuite failed to process the inbound message';
	
	var PO_INTERNAL_ID;
}

/**
 * GET CALL: REST API Inbound call for generating PO data object model
 */
function getPurchaseOrderObject(dataIn){ 
	var objReturn = {};

	try{
		objReturn = do_GetPurchaseOrderObject(dataIn);
	} catch(error){
		//Create error object
		var errorObj = FactoryPOLib.generateErrorObject(MESSAGE_OUTBOUND_PROCESS_FAIL, error);		
		nlapiLogExecution('DEBUG', 'getPurchaseOrderObject Error', JSON.stringify(errorObj));
		
		//Logging
		FactoryPOLib.log(LogDirection_Outbound, (PO_INTERNAL_ID ? PO_INTERNAL_ID : 0), 0, dataIn, FactoryPOLib.getLogStatusID(errorObj.status), errorObj, 'F');	
		
		objReturn = errorObj;
	}
	
	return objReturn;
}

/**
 * Processing of GET request
 */
function do_GetPurchaseOrderObject(dataIn){
	
	nlapiLogExecution('DEBUG', 'GET dataIn', JSON.stringify(dataIn));
	
	var poId;
	var tranId;
	
	//Validation - check dataIn if empty
	var objResult = FactoryPOLib.isEmptyRequestObject(dataIn, 'Inbound');	
	if(objResult.isEmpty){
		return outboundPOErrorFlow(poId, tranId, dataIn, objResult.error);
	}
	
	//Validation - check order id
	tranId = dataIn.id;
	objResult = FactoryPOLib.validateTranId(tranId);
	poId = objResult.internalid;
	PO_INTERNAL_ID = poId;
	if(!objResult.success){
		return outboundPOErrorFlow(poId, tranId, dataIn, objResult.error);
	}
		
	//Generate PO object
	var record = nlapiLoadRecord('purchaseorder', poId);
	var CanonObject = generatePOCanonObject(record);
	nlapiLogExecution('DEBUG', 'Canonical Object', JSON.stringify(CanonObject)); 
	if(CanonObject.status == 'Exception'){
		return outboundPOErrorFlow(poId, tranId, dataIn, CanonObject);
	}
	
	//Generate Journal Entry to track PO amount
	if(!nlapiLookupField('purchaseorder', poId, 'custbody_3pl_journalentry')){
		
		var stProcessDesc_JECreation = 'Creating Journal Entry';
		var stProcessDesc_JELinking = 'Linking Journal Entry to PO';
		
		objResult = FactoryPOLib.createJournalEntry(
						tranId, 
						record.getFieldValue('total'), 
						record.getFieldValue('subsidiary'), 
						record.getFieldValue('currency'), 
						record.getFieldValue('exchangerate'));
						
		if(!objResult.success){
			return outboundPOErrorFlow(poId, tranId, dataIn, objResult.error);
		} else {
				
			//Logging for journal entry
			FactoryPOLib.log(LogDirection_Outbound, poId, objResult.journalid, stProcessDesc_JECreation, 
				FactoryPOLib.getLogStatusID('SUCCESS'), 'Journal Entry created: ' + objResult.journalid, 'F');	
			
			try{
				//Link Journal Entry to PO
				nlapiSubmitField('purchaseorder', poId, 'custbody_3pl_journalentry', objResult.journalid);
			} catch(error){
				nlapiLogExecution('DEBUG', 'getPurchaseOrderObject - Error', error.toString());

				//Check if error = SSS_REQUEST_TIME_EXCEEDED 
				var bTimeExceededError = handleTimeExceededError('getPurchaseOrderObject: ' + error.toString(), LogDirection_Outbound, 
											poId, objResult.journalid, stProcessDesc_JELinking, null);
									
				//Return error if it is other than SSS_REQUEST_TIME_EXCEEDED
				if(!bTimeExceededError){
					return outboundPOErrorFlow(poId, tranId, dataIn, 'getPurchaseOrderObject: ' + error);
				}
			}		
		}
	}
	
	//Logging for PO object
	FactoryPOLib.log(LogDirection_Outbound, poId, 0, dataIn, FactoryPOLib.getLogStatusID('SUCCESS'), CanonObject, 'T');

	return CanonObject;		
}

/**
 * Error flow for outbound PO
 */
function outboundPOErrorFlow(poId, tranId, dataIn, objOrigError){
			
	//Create error object
	var errorObj = FactoryPOLib.generateErrorObject(MESSAGE_OUTBOUND_PROCESS_FAIL, objOrigError);
	nlapiLogExecution('DEBUG', 'Error Object', JSON.stringify(errorObj));

	//Logging
	FactoryPOLib.log(LogDirection_Outbound, (poId ? poId : 0), 0, dataIn, FactoryPOLib.getLogStatusID(errorObj.status), errorObj, 'F');
	
	//Send Error Mail
	FactoryPOLib.sendIntegrationErrorMail(dataIn, 'Outbound PO processing failure', tranId, errorObj, true);
	
	return errorObj;
}

/**
 * Generation of data object model for PO
 */
function generatePOCanonObject(record){

	try{
		
		//Get FromLocation info based on Factory ID field
		var objFactoryInfo = getPO_FactoryInfo(record.getFieldValue('custbody_ka_factory_id'));
		
		//Get ToLocation info based on Ship To field
		var objToLoc = getPO_ToLocation(record.getFieldValue('shipto'));
		
		//PO Object
		var purchaseOrderSchema = {
				"purchaseOrder": {
					"orderHeader":{
						"orderId":				readValue(record.getFieldValue('tranid')),
						"childSourceSystem": 	"Netsuite",
						"currentHandoverDt": 	readValue(record.getFieldValue('custbody_ka_current_hod')),
						"orderStatus": 			readValue(record.getFieldText('approvalstatus')),
						"transactionType": 		"PURCHASE ORDER",
						"orderHeaderMemo":		readValue(record.getFieldValue('memo')),
						"fromLocation":{		//Factory ID
							"locationId":		readValue(record.getFieldValue('custbody_ka_factory_id')),
							"locationName":		readValue(objFactoryInfo.addressee),
							"addressLine1":		readValue(objFactoryInfo.address1),
							"addressLine2":		readValue(objFactoryInfo.address2),
							"city":				readValue(objFactoryInfo.city),
							"poBox":			readValue(objFactoryInfo.pobox),
							"province":			readValue(objFactoryInfo.province),
							"country":			readValue(objFactoryInfo.country),
							"postalCode":		readValue(objFactoryInfo.zip),
							"phoneNumber":		readValue(objFactoryInfo.phone),
							"emailAddress":		readValue(objFactoryInfo.email)
						},
						"toLocation":{			//Ship To
							"locationId":		readValue(objToLoc.locationcode),
							"locationName":		readValue(objToLoc.addressee),
							"addressLine1":		readValue(objToLoc.address1),
							"addressLine2":		readValue(objToLoc.address2),
							"city":				readValue(objToLoc.city),
							"poBox":			readValue(objToLoc.pobox),
							"province":			readValue(objToLoc.province),
							"country":			readValue(objToLoc.country),
							"postalCode":		readValue(objToLoc.zip),
							"phoneNumber":		readValue(objToLoc.phone),
							"emailAddress":		readValue(objToLoc.email)
						},
						"paymentInfo":{
							"currencyCode":		readValue(record.getFieldValue('currencyname'))
						}
					},
					"orderDetail": 				getPO_OrderDetails(record),
					"shipment":					getPO_Shipment(record)
			   }
			};
				
	} catch(po_error){
        nlapiLogExecution('DEBUG', 'error message', po_error.toString());
		
		var errorObj = {};					
		errorObj.status = 'Exception';
		errorObj.orderid = record.getId();		
		errorObj.message = 'Purchase Order Error: ' + po_error;
		
		purchaseOrderSchema = errorObj;
	}
	
	return purchaseOrderSchema;
}

/**
 * Returns Order Details array
 */
function getPO_OrderDetails(record){
	
	//var recordId = record.getId();						
	var recordId = record.getFieldValue('tranid');	
	var arOrderDetails = [];
	
	//Create Order Details array
	var stSkuCode;
	for(var i = 0; i < record.getLineItemCount('item'); i++){
		
		stSkuCode = record.getLineItemText('item', 'item', i+1);//ex. 'KW031197M : KW031197-10068-0 Atal Long Sleeve-HTHR Charcoal-0'
		if(stSkuCode.indexOf(":") > -1){
			stSkuCode = stSkuCode.split(":")[1];				//ex. ' KW031197-10068-0 Atal Long Sleeve-HTHR Charcoal-0'
			if(stSkuCode.indexOf(" ") > -1){
				stSkuCode = stSkuCode.split(" ")[1];			//ex. 'KW031197-10068-0'
			}
		}								
		
		arOrderDetails.push({
			"orderId": 				recordId,
			"orderLineNumber":		recordId + "_" + parseInt(record.getLineItemValue('item', 'line', i+1)), //record.getLineItemValue('item', 'id', i+1),
			"countItemOrdered":		parseInt(record.getLineItemValue('item', 'quantity', i+1)),     
			"item":{
				"SKUCode":			stSkuCode,
				"SKUId":			record.getLineItemValue('item', 'custcol_wtka_upccode', i+1)         
			},
			"amtItemPrice":{
				"amtCurrentPrice":	parseFloat(record.getLineItemValue('item', 'rate', i+1))
			}
		});
	}
	
	return arOrderDetails;
}

/**
 * Returns Shipment array
 */
function getPO_Shipment(record){
	
	var arShipment = [];
			
	arShipment.push({
		"shipmentHeader":{
			"shipmentId":			readValue(record.getFieldValue('tranid')),
			"shipmentCode":			readValue(record.getFieldText('custbody_ka_shipping_method')), //TODO: Same as shippingMethod ?
			"shippingMethod":		readValue(record.getFieldText('custbody_ka_shipping_method')),
			"trackingNumber":		"",
			"shipmentStatus":		"CR",
			"actualInDcDt":			"", //readValue(record.getFieldValue('custbody_ka_actual_in_dc')),
			"currentInDcDt":		readValue(record.getFieldValue('custbody_ka_current_in_dc')),
			"handlingInstructions":	""
		},
		"shipmentDetail":			getPO_ShipmentDetails(record)
	});
	
	return arShipment;
}

/**
 * Returns Shipment Details array
 */
function getPO_ShipmentDetails(record){
	
	var arShipmentDetails = [];
	var recordId = record.getFieldValue('tranid');	
	
	for(var i = 0; i < record.getLineItemCount('item'); i++){
		arShipmentDetails.push({
			"orderDetailId":	recordId + "_" + parseInt(record.getLineItemValue('item', 'line', i+1)), //readValue(record.getLineItemValue('item', 'id', i+1)),
			"quantityOrdered":	parseInt(record.getLineItemValue('item', 'quantity', i+1)),
			"quantityReceived":	parseInt(record.getLineItemValue('item', 'quantityreceived', i+1))
		});
	}
	
	return arShipmentDetails;
}

/**
 * Returns Factory address info
 */
function getPO_FactoryInfo(stFactoryId){
	
	var objFactoryInfo = {};
		
	if(stFactoryId){
		
		//This is for when the State/Province is in Chinese chars
		//(ex. State/Province of Vendor "11311 Shanghai Brothers Qidong Knitting Wear Co.,Ltd.")
		var objStateColumn = new nlobjSearchColumn('formulatext').setFormula('{shippingaddress.statedisplayname}');
	
		var arResult = nlapiSearchRecord('vendor', null,
						[new nlobjSearchFilter('internalid', null, 'is', stFactoryId)],
						[new nlobjSearchColumn('internalid'),
						 new nlobjSearchColumn('shipaddressee'),
						 new nlobjSearchColumn('shipaddress1'),
						 new nlobjSearchColumn('shipaddress2'),
						 new nlobjSearchColumn('shipcity'),
						 new nlobjSearchColumn('shipcountry'),
						 new nlobjSearchColumn('shipzip'),
						 new nlobjSearchColumn('shipphone'),
						 new nlobjSearchColumn('email'),						 
						 new nlobjSearchColumn('shipstate'),	//this returns Chinese chars for some vendors
						 objStateColumn]);						//this returns State/Province in english
						 
		if(arResult){
			for(var i = 0; i < arResult.length; i++){
				objFactoryInfo = {
					addressee: 	arResult[i].getValue('shipaddressee'),
					address1: 	arResult[i].getValue('shipaddress1'),
					address2: 	arResult[i].getValue('shipaddress2'),
					city: 		arResult[i].getValue('shipcity'),
					province: 	arResult[i].getValue(objStateColumn),
					country: 	arResult[i].getText('shipcountry'),
					zip: 		arResult[i].getValue('shipzip'),										
					phone: 		arResult[i].getValue('shipphone'),
					email: 		arResult[i].getValue('email'),
					pobox: 		""
				};
				break;
			}
		}
	}

	return objFactoryInfo;
}

/**
 * Returns toLocation object
 */
function getPO_ToLocation(stShipTo){
	var objToLoc = {};
	
	if(stShipTo){

		var arCustomer = nlapiSearchRecord('customer', null,
						[new nlobjSearchFilter('internalid', null, 'is', stShipTo)],
						[new nlobjSearchColumn('externalid', 'custentity_ka_location'),			// Location Code
						 new nlobjSearchColumn('custentity_ka_location'),
						 new nlobjSearchColumn('shipaddressee'),
						 new nlobjSearchColumn('shipaddress1'),
						 new nlobjSearchColumn('shipaddress2'),
						 new nlobjSearchColumn('shipcity'),
						 new nlobjSearchColumn('shipcountry'),
						 new nlobjSearchColumn('shipzip'),
						 new nlobjSearchColumn('shipphone'),
						 new nlobjSearchColumn('email'),						 
						 new nlobjSearchColumn('shipstate')]); 
		
		if(arCustomer && arCustomer.length > 0){	
		
			objToLoc = {
				location:		arCustomer[0].getValue('custentity_ka_location'),
				locationcode:	arCustomer[0].getValue('externalid', 'custentity_ka_location'),
			
				shipto:			stShipTo,			
				addressee: 		arCustomer[0].getValue('shipaddressee'),
				address1: 		arCustomer[0].getValue('shipaddress1'),
				address2: 		arCustomer[0].getValue('shipaddress2'),
				city: 			arCustomer[0].getValue('shipcity'),
				province: 		arCustomer[0].getValue('shipstate'),
				country: 		arCustomer[0].getText('shipcountry'),
				zip: 			arCustomer[0].getValue('shipzip'),										
				phone: 			arCustomer[0].getValue('shipphone'),
				email: 			arCustomer[0].getValue('email'),
				pobox: 			""				
			};
		}
	}
	
	return objToLoc;
}

/**
 * PUT CALL: REST API Inbound call
 */
function putPurchaseOrder(dataIn){
	var objReturn = {};

	try{
		objReturn = do_PutPurchaseOrder(dataIn);
	} catch(error){
		//Create error object
		var errorObj = FactoryPOLib.generateErrorObject(MESSAGE_INBOUND_PROCESS_FAIL, error);		
		nlapiLogExecution('DEBUG', 'putPurchaseOrder Error', JSON.stringify(errorObj));
		
		//Logging
		FactoryPOLib.log(LogDirection_Inbound, (PO_INTERNAL_ID ? PO_INTERNAL_ID : 0), 0, dataIn, FactoryPOLib.getLogStatusID(errorObj.status), errorObj, 'F');	
		
		objReturn = errorObj;
	}
	
	return objReturn;
}

/**
 * Processing of PUT request
 */
function do_PutPurchaseOrder(dataIn){
	
	nlapiLogExecution('DEBUG', 'PUT dataIn', JSON.stringify(dataIn));
	
	var responseObj = {};	
	responseObj.status = 'SUCCESS';
	responseObj.message = 'NetSuite accepted the inbound message';
	
	//1) VALIDATION
	//check if relevant data are empty
	var objResult = validateInboundPODataIn(dataIn);
	nlapiLogExecution('DEBUG', 'validateInboundPODataIn', JSON.stringify(objResult));
	if(!objResult.success){
		var errorObj = FactoryPOLib.generateErrorObject(MESSAGE_INBOUND_PROCESS_FAIL, objResult.error);
		
		//Logging - for invalid dataIn
		FactoryPOLib.log(LogDirection_Inbound, objResult.poId ? objResult.poId : 0, 0, dataIn, FactoryPOLib.getLogStatusID(errorObj.status), errorObj, 'F');
		
		return errorObj;
	} 
	
	var	poId = objResult.poId;
	var	orderId = dataIn.purchaseOrder.orderHeader.orderId;
	var	orderStatus = dataIn.purchaseOrder.orderHeader.orderStatus;
	PO_INTERNAL_ID = poId;
	
	//Logging - Inbound PO is valid
	FactoryPOLib.log(LogDirection_Inbound, poId, 0, dataIn, FactoryPOLib.getLogStatusID('SUCCESS'), 'Inbound PO JSON passed validation', 'F');
	
	//2) PROCESSING
	//Process based on orderStatus and shipmentStatus
	var stOrderStatus = dataIn.purchaseOrder.orderHeader.orderStatus;
	var stShipStatus = dataIn.purchaseOrder.shipment[0].shipmentHeader.shipmentStatus;
	
	if(stShipStatus.toUpperCase() == 'CREATED'){
		if(stOrderStatus.toUpperCase() == 'RECEIVED' || stOrderStatus.toUpperCase() == 'REJECTED'){		
			//Update PO 'EXPORT 3PL STATUS'
			var objUpdateStatus = updateExport3PLStatus(dataIn, poId);
			if(objUpdateStatus.error){			
				nlapiLogExecution('debug', 'updateExport3PLStatus', JSON.stringify(objUpdateStatus.error));
				responseObj = objUpdateStatus.error; //Return error object
			}
		}
	} else if(stShipStatus.toUpperCase() == 'RECEIVED'){
		//Update Actual In DC Date
		var objUpdateStatus = updateActualInDCDate(dataIn, poId);
		
		if(!objUpdateStatus.success && objUpdateStatus.error){				
			nlapiLogExecution('debug', 'updateActualInDCDate', JSON.stringify(objUpdateStatus.error));
			responseObj = objUpdateStatus.error; //Return error object			
		} else {						
			//Process Item Receipt - creation of Inbound IR Message custom record
			responseObj = put_ItemReceipt(dataIn, poId);
		}		
	}
								
	//Return response to ESB
	return responseObj;
}

/**
 * Validation for Inbound PO request object
 */
function validateInboundPODataIn(dataIn){
	
	var stTranId = null;
	var stInternalId = null;
	
	//Check dataIn if empty
	var objResult = FactoryPOLib.isEmptyRequestObject(dataIn, 'Inbound');
	if(objResult.isEmpty){
		return {success: false, poId: stInternalId, error: objResult.error};
	}
	
	//Check purchaseOrder if empty
	objResult = FactoryPOLib.isEmptyRequestObject(dataIn.purchaseOrder, 'purchaseOrder');
	if(objResult.isEmpty){
		return {success: false, poId: stInternalId, error: objResult.error};
	}
			
	//Check orderHeader if empty
	objResult = FactoryPOLib.isEmptyRequestObject(dataIn.purchaseOrder.orderHeader, 'purchaseOrder.orderHeader');
	if(objResult.isEmpty){
		return {success: false, poId: stInternalId, error: objResult.error};
	}
	
	//Check orderHeader.orderId if empty
	stTranId = dataIn.purchaseOrder.orderHeader.orderId; //Set tranid
	objResult = FactoryPOLib.isEmptyRequestObject(stTranId, 'purchaseOrder.orderHeader.orderId', stTranId);
	if(objResult.isEmpty){
		return {success: false, poId: stInternalId, error: objResult.error};
	}
		
	//Check if orderHeader.orderId is valid
	objResult = FactoryPOLib.validateTranId(stTranId);
	if(!objResult.success){
		return {success: false, poId: stInternalId, error: objResult.error};
	}
	stInternalId = objResult.internalid; //Set internalid
	
	//Check orderHeader.orderStatus if empty
	objResult = FactoryPOLib.isEmptyRequestObject(dataIn.purchaseOrder.orderHeader.orderStatus, 'purchaseOrder.orderHeader.orderStatus', 
					stTranId);
	if(objResult.isEmpty){
		return {success: false, poId: stInternalId, error: objResult.error};
	}
	
	//Check orderDetail if empty
	objResult = FactoryPOLib.isEmptyRequestObject(dataIn.purchaseOrder.orderDetail, 'purchaseOrder.orderDetail', stTranId);
	if(objResult.isEmpty){
		return {success: false, poId: stInternalId, error: objResult.error};
	}
	
	//Check shipment if empty
	objResult = FactoryPOLib.isEmptyRequestObject(dataIn.purchaseOrder.shipment, 'purchaseOrder.shipment', stTranId);
	if(objResult.isEmpty){
		return {success: false, poId: stInternalId, error: objResult.error};
	}
	
	//Check shipment[i].shipmentHeader if empty
	for(var i = 0; i < dataIn.purchaseOrder.shipment.length; i++){		
		objResult = FactoryPOLib.isEmptyRequestObject(dataIn.purchaseOrder.shipment[i].shipmentHeader, 'purchaseOrder.shipment[' + i + '].shipmentHeader',
						stTranId);
		if(objResult.isEmpty){
			return {success: false, poId: stInternalId, error: objResult.error};
		}
	}
	
	//Check shipment[i].shipmentHeader.shipmentStatus if empty
	for(var i = 0; i < dataIn.purchaseOrder.shipment.length; i++){
		objResult = FactoryPOLib.isEmptyRequestObject(dataIn.purchaseOrder.shipment[i].shipmentHeader.shipmentStatus, 'purchaseOrder.shipment[' + i + '].shipmentHeader.shipmentStatus',
						stTranId);
		if(objResult.isEmpty){
			return {success: false, poId: stInternalId, error: objResult.error};
		}
	}
	
	//Check shipment[i].shipmentHeader.shipmentId if empty
	for(var i = 0; i < dataIn.purchaseOrder.shipment.length; i++){
		objResult = FactoryPOLib.isEmptyRequestObject(dataIn.purchaseOrder.shipment[i].shipmentHeader.shipmentId, 'purchaseOrder.shipment[' + i + '].shipmentHeader.shipmentId',
						stTranId);
		if(objResult.isEmpty){
			return {success: false, poId: stInternalId, error: objResult.error};
		}
	}
	
	//Check quantityReceived if shipmentStatus = Received
	objResult = FactoryPOLib.validateQuantityReceived(dataIn);
	if(!objResult.success){
		return {success: false, poId: stInternalId, error: objResult.error};
	}

	//Success
	return {success: true, poId: stInternalId};
}

/**
 * Updates the PO whether the 3PL accepted or rejected the PO
 */
function updateExport3PLStatus(dataIn, poId){
	
	var stProcessDesc = 'Updating PO Export 3PL Status';
	
	var orderId = dataIn.purchaseOrder.orderHeader.orderId;
	var orderStatus = dataIn.purchaseOrder.orderHeader.orderStatus;
	
	var bIsReceived;
	var stStatus;
	
	switch(orderStatus.toUpperCase()){
		case 'RECEIVED':
			stStatus = STATUS_ACCEPTED;
			bIsReceived = true;
			nlapiLogExecution('DEBUG', 'Updated PO: ' + poId, 'ACCEPTED');
			break;
		case 'REJECTED':
			stStatus = STATUS_REJECTED;
			nlapiLogExecution('DEBUG', 'Updated PO: ' + poId, 'REJECTED');
			break;
		default:
			break;
	}
	
	var stMessage;
	
	if(stStatus){
		try{
			nlapiSubmitField('purchaseorder', poId, 'custbody_export_3pl_status', stStatus);
		} catch(error){
			nlapiLogExecution('DEBUG', 'updateExport3PLStatus - Error', error.toString());

			//Check if error = SSS_REQUEST_TIME_EXCEEDED 
			var bTimeExceededError = handleTimeExceededError('updateExport3PLStatus: ' + error.toString(), LogDirection_Inbound, poId, 0, dataIn, null);
								
			//Return error if it is other than SSS_REQUEST_TIME_EXCEEDED
			if(!bTimeExceededError){
				//Return error
				var errorObj = FactoryPOLib.generateErrorObject(MESSAGE_INBOUND_PROCESS_FAIL, 'updateExport3PLStatus: ' + error);
			
				//Logging
				FactoryPOLib.log(LogDirection_Inbound, (poId ? poId : 0), 0, stProcessDesc, FactoryPOLib.getLogStatusID('ERROR'), 
					errorObj, 'F');
			
				return {received: false, error: errorObj};
			}
		}		
		stMessage = 'Updated PO ' + orderId + ' export status to ' + orderStatus;		
	}else{
		stMessage = 'Not able to update PO ' + orderId + ' with export status ' + orderStatus;		
	}
	
	//Logging
	FactoryPOLib.log(LogDirection_Inbound, poId, 0, stProcessDesc, FactoryPOLib.getLogStatusID('SUCCESS'), stMessage, 'F');			
	
	return {received: bIsReceived};
}

/**
 * Updates Actual In DC field
 */
function updateActualInDCDate(dataIn, poId){
	
	var orderId = dataIn.purchaseOrder.orderHeader.orderId;
	var stActualInDCDate = dataIn.purchaseOrder.shipment[0].shipmentHeader.actualInDcDt;
	var stProcessDesc = 'Updating Actual In DC';
	
	try{
	
		if(stActualInDCDate){				
			stActualInDCDate = nlapiDateToString(new Date(stActualInDCDate), 'date');
		} else {
			stActualInDCDate = '';
		}
		
		nlapiSubmitField('purchaseorder', poId, 'custbody_ka_actual_in_dc', stActualInDCDate);
		
	} catch(error){
		nlapiLogExecution('DEBUG', 'updateActualInDCDate - Error', error.toString());

		//Check if error = SSS_REQUEST_TIME_EXCEEDED 
		var bTimeExceededError = handleTimeExceededError('updateActualInDCDate: ' + error.toString(), LogDirection_Inbound, 
									poId, 0, stProcessDesc, null);
							
		if(!bTimeExceededError){
			//Return error
			var responseObj = FactoryPOLib.generateErrorObject(MESSAGE_INBOUND_PROCESS_FAIL, 'updateActualInDCDate: ' + error);
		
			//Logging
			FactoryPOLib.log(LogDirection_Inbound, (poId ? poId : 0), 0, stProcessDesc, FactoryPOLib.getLogStatusID('ERROR'), 
				responseObj, 'F');
		
			return {success: false, error: responseObj};
		}
	}
	
	//Logging
	FactoryPOLib.log(LogDirection_Inbound, (poId ? poId : 0), 0, stProcessDesc, FactoryPOLib.getLogStatusID('SUCCESS'), 
		'Updated PO ' + orderId + ' Actual In DC to ' + stActualInDCDate, 'F');
		
	return {success: true};
}

/**
 * Item Receipt processing
 */
function put_ItemReceipt(dataIn, poId){

	var responseObj = {};
	
	var tranId = dataIn.purchaseOrder.orderHeader.orderId;
	var arShipmentIds = [];
	var arExistingStaging = [];
	
	//Store shipment IDs, will be used for duplicate checking
	for(var i = 0; i < dataIn.purchaseOrder.shipment.length; i++){
		arShipmentIds.push(dataIn.purchaseOrder.shipment[i].shipmentHeader.shipmentId);
	}
	
	//Check if an Inbound Item Receipt Message already exists for the PO
	//Needs to search for duplicate before creation of staging record
	//Relying on setting externalid for duplicate check will not prevent the Inbound Message file in File Cabinet from being overwritten
	var arResults = nlapiSearchRecord('customrecord_erp_3pl_inbound_itemreceipt', null,
					[new nlobjSearchFilter('isinactive', null, 'is', 'F'),
					 new nlobjSearchFilter('externalid', null, 'anyof', arShipmentIds)],
					[new nlobjSearchColumn('internalid'),
					 new nlobjSearchColumn('externalid')]);
	if(arResults){
		for(var i = 0; i < arResults.length; i++){		
			arExistingStaging.push(arResults[i].getValue('externalid'));	
		}
	}
	
	nlapiLogExecution('debug', 'arShipmentIds', arShipmentIds);
	nlapiLogExecution('debug', 'arExistingStaging', arExistingStaging);
	
	//If all shipments are already in NS, return Duplicate error
	var bEqualArray = true;
	for(var i = 0; i < arShipmentIds.length; i++){
		if(arExistingStaging.indexOf(arShipmentIds[i]) < 0){
			bEqualArray = false;
			break;
		}
	}	
	if(bEqualArray){		
		responseObj = FactoryPOLib.generateErrorObject(MESSAGE_INBOUND_PROCESS_FAIL, 
						{status: "Exception",
						 orderid: tranId,
						 message: 'Duplicate Item Receipt request for the Purchase Order'
						});

		//Logging - Inbound IR Message already exists
		FactoryPOLib.log(LogDirection_Inbound, (poId ? poId : 0), 0, 'Checking duplicate Inbound Item Receipt Message', 
			FactoryPOLib.getLogStatusID(responseObj.status), responseObj, 'F');
		
		return responseObj;
	}
	
	var arErrorDetails = [];	//Consolidated error details
	var bHasError;				//Error flag
	
	//Create staging record only for new shipments
	for(var i = 0; i < arShipmentIds.length; i++){	
	
		if(arExistingStaging.indexOf(arShipmentIds[i]) < 0){
			
			var externalId = arShipmentIds[i]; //externalid for staging record

			//Create Inbound IR Message
			var stProcessDesc = 'Creating Inbound Item Receipt Message for shipment: ' + externalId;
			try {	
			
				//1)Create Custom Record instance in Netsuite (Inbound Item Receipt Message - customrecord_erp_3pl_inbound_itemreceipt) 
				var ItemReceiptMsg = nlapiCreateRecord('customrecord_erp_3pl_inbound_itemreceipt');

				ItemReceiptMsg.setFieldValue('custrecord_itemreceipt_po', poId);
				ItemReceiptMsg.setFieldValue('custrecord_itemreceipt_linkedtransaction', poId);
				
				//Set PO in Processed Transactions field
				ItemReceiptMsg.setFieldValue('custrecord_itemreceipt_processed_tran', FactoryPOLib.updateInboundMessageTranLinks('', 'PO', poId, tranId));

				//Store in file cabinet
				var newFile = nlapiCreateFile(externalId, 'PLAINTEXT', JSON.stringify(dataIn)); //Make filename unique in case of multiple receipt requests
				newFile.setFolder(ITEM_RECEIPT_FOLDER_ID);
				var fileId = nlapiSubmitFile(newFile);	
				ItemReceiptMsg.setFieldValue('custrecord_itemreceipt_inboundfile', fileId);

				//Store in long text field
				var msgString = JSON.stringify(dataIn).substring(0, 999999); //to prevent error if dataIn is too long
				ItemReceiptMsg.setFieldValue('custrecord_itemreceipt_inboundmessage', msgString);

				//Set status to 'ToProcess', this will immediately trigger the ERP_3PL_Transform_PO_to_IR_UE script
				ItemReceiptMsg.setFieldValue('custrecord_itemreceipt_status', STATUS_TOPROCESS);
				
				//Setting the externalid will cause an error in case of duplication
				ItemReceiptMsg.setFieldValue('externalid', externalId);

				var ItemReceiptMsgId = nlapiSubmitRecord(ItemReceiptMsg);
				nlapiLogExecution('DEBUG', 'Created Item Receipt Message Object', 'Record ID: ' + ItemReceiptMsgId);

			} catch (error){		
				nlapiLogExecution('DEBUG', 'put_ItemReceipt - Error', error.toString());
				
				//Check if error = SSS_REQUEST_TIME_EXCEEDED 
				var bTimeExceededError = handleTimeExceededError('put_ItemReceipt: ' + error.toString(), LogDirection_Inbound, poId, 0, stProcessDesc, null);
				
				//Return error if it is other than SSS_REQUEST_TIME_EXCEEDED
				if(!bTimeExceededError){
					//Error detail
					responseObj = FactoryPOLib.generateErrorObject('Shipment: ' + externalId, 'put_ItemReceipt: ' + error);				
					arErrorDetails.push(responseObj);
				
					//Logging
					FactoryPOLib.log(LogDirection_Inbound, (poId ? poId : 0), 0, stProcessDesc, FactoryPOLib.getLogStatusID('ERROR'), 
						responseObj, 'F');
					
					bHasError = true;
				}
			}
			
			//Successful creation of staging record
			//Logging
			FactoryPOLib.log(LogDirection_Inbound, (poId ? poId : 0), 0, stProcessDesc, FactoryPOLib.getLogStatusID('SUCCESS'), 
				'Created Inbound Item Receipt Message: ' + ItemReceiptMsgId, 'F', ItemReceiptMsgId);
		}
	}
	
	if(bHasError){
		//Creation of staging record got an error
		responseObj = FactoryPOLib.generateErrorObject(MESSAGE_INBOUND_PROCESS_FAIL, arErrorDetails);
	} else{		
		//Success response message
		responseObj.status = 'SUCCESS';
		responseObj.message = 'NetSuite accepted the inbound message';
	}
		
	return responseObj;
}

/**
 * Action for SSS_REQUEST_TIME_EXCEEDED error
 */
function handleTimeExceededError(stErrorMsg, stLogDir, stPoId, stCreatedRecId, stActionDesc, stIRMsgRecId){
	//Ignore if error = SSS_REQUEST_TIME_EXCEEDED,as it means that 
	//the subsequent scripts (UE > SL > UE > SL > so on..) have taken too long
	//but not necessarily have failed in processing
	if(stErrorMsg.toString().indexOf('SSS_REQUEST_TIME_EXCEEDED') >= 0){
		
		//Logging - for record keeping
		FactoryPOLib.log(stLogDir, (stPoId ? stPoId : 0), stCreatedRecId, stActionDesc, FactoryPOLib.getLogStatusID('SUCCESS'), 
			stErrorMsg, 'F', stIRMsgRecId);
			
		return true;		
	} 
	
	return false;
}
