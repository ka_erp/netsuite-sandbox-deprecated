/**
 *@NApiVersion 2.x
 *@NScriptType MapReduceScript
 *@NModuleScope Public
 */

//File: ERP_3PLProcessItemReceipts_MR.js 

define(['N/search',
        'N/log',
        'N/record',
        '../ERP_Utility/lib_script_config',
        '../ERP_Utility/lib_ka_functions'
        ],
    ir_mass_process);


function ir_mass_process(search, log, record, lib_script_config, lib_ka_functions){
    
	function getInputData()
    {
		log.debug('Getting Input Data...');		
		
        var poSearch = search.load({
            id: 'customsearch_itemreceipt_toprocess' //Saved Search: 'Inbound Item Receipts to Process'
        });

		return poSearch;
    }	

    function map(context)
    {
        var searchResult = JSON.parse(context.value);
    	var customItemReceiptRecordId = searchResult.id;

    	log.debug('(Mapping) customItemReceiptRecordId: ' + customItemReceiptRecordId + ', 1');
    	
    	context.write(customItemReceiptRecordId, 1); //Key-Value pair
    }

    function reduce(context)
    {
        var scobj = lib_script_config.createInstance();
        var STATUS_TOPROCESS = scobj.getScriptConfigValue('IR Processed Status: To Process');
    	
    	var customItemReceiptRecordId = context.key; 
    	
        log.debug('(Reducing) customItemReceiptRecordId: ' + customItemReceiptRecordId);

        try {
            var id = record.submitFields({
                type: 'customrecord_erp_3pl_inbound_itemreceipt', 
                id: customItemReceiptRecordId,
                values: {
                    custrecord_itemreceipt_status: STATUS_TOPROCESS
                },
                options: {
                    enableSourcing: false,
                    ignoreMandatoryFields : true
                }
            });
        } catch (e) {
            log.debug('error submitting field');            
            log.debug(e.name);
            log.debug(e);
        }
    }

    function summarize(summary)
    {
    	log.debug('Summarizing...');
        var type = summary.toString();
        log.debug(type + ' Usage Consumed', summary.usage);
        log.debug(type + ' Number of Queues', summary.concurrency);
        log.debug(type + ' Number of Yields', summary.yields);
        log.debug('...Done!');
    }

    return {
        getInputData: getInputData,
        map: map,
        reduce: reduce,
        summarize: summarize
    };
}

