/**
 *	File Name		:	ERP_PurchaseOrder_ID_Validation_UE_CS_SL.js
 *	Function		:	
 * 	Remarks			:	
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{
	var MSG_INVALID_PO_ID = "Please enter an alphanumeric value for PO#/Name";
	
	var OBJ_DOCID_FIELD_MAP = {
			'purchaseorder' : 'tranid',
			'customrecord_erp_po_staging' : 'name'
		};
		
	var ST_APPLICABLE_FORMS;
	var B_SCRIPTCONFIG_QUERY_DONE;
}

/**
 * USER EVENT : BEFORE SUBMIT
 * @param {String} type operation
 */
function beforeSubmit_ValidatePOId(type){
	
	if(type == 'create' || type == 'edit' || type == 'xedit'){
			
		nlapiLogExecution('debug', 'type', type);
		nlapiLogExecution('debug', 'internal id', nlapiGetRecordId());
		nlapiLogExecution('debug', 'record type', nlapiGetRecordType());
						
		var bDoValidate = true;
		var stDocId;
		var stDocIdFieldId = OBJ_DOCID_FIELD_MAP[nlapiGetRecordType()];
		
		if(stDocIdFieldId){
			stDocId = nlapiGetFieldValue(stDocIdFieldId);
			nlapiLogExecution('debug', 'document id', stDocId);
			
			if(type == 'xedit'){
				
				bDoValidate = false;
				
				//get updated fields
				var fields = nlapiGetNewRecord().getAllFields();
				
				//check if the document id field was updated
				for(var i = 0; i < fields.length; i++){
					if(fields[i] == stDocIdFieldId){
						bDoValidate = true;
					}
				}
			}
			
			//Do validation
			if(bDoValidate){
				
				var bApplicableForm = true;
				
				//Additional checking for PO based on form used
				if(nlapiGetRecordType() == 'purchaseorder'){
					
					//Get applicable forms from script config
					ST_APPLICABLE_FORMS = SCRIPTCONFIG.getScriptConfigValue('PO: Form: Factory Forms');
					nlapiLogExecution('debug', 'ST_APPLICABLE_FORMS', ST_APPLICABLE_FORMS);
					
					var stFormId = nlapiGetFieldValue('customform');					
					nlapiLogExecution('debug', 'customform', stFormId);
					
					bApplicableForm = isApplicableForm(stFormId);
				}
							
				if(bApplicableForm && !isValid_alphanumeric(stDocId)){
					//Throw error
					throw nlapiCreateError('INVALID_PO_ID', MSG_INVALID_PO_ID);
				}
			}
		}		
	}	
}

/**
 * CLIENT : FIELD CHANGED
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @returns {Void}
 */
function validateField_ValidatePOId(type, name){
	
	var stDocIdFieldId = OBJ_DOCID_FIELD_MAP[nlapiGetRecordType()];
	
	if(name == stDocIdFieldId){
		
		var stDocId = nlapiGetFieldValue(stDocIdFieldId);
		
		//Do validation
		if(isApplicableForm_Client() && !isValid_alphanumeric(stDocId)){
			//Alert error
			alert(MSG_INVALID_PO_ID);
			
			return false;
		}
	}
	
	return true;
}

/**
 * CLIENT : SAVE RECORD
 * @returns {Boolean}
 */
function saveRecord_ValidatePOId(){
	
	var stDocIdFieldId = OBJ_DOCID_FIELD_MAP[nlapiGetRecordType()];
	var stDocId = nlapiGetFieldValue(stDocIdFieldId);
	
	//Do validation
	if(isApplicableForm_Client() && !isValid_alphanumeric(stDocId)){
		//Alert error
		alert(MSG_INVALID_PO_ID);
		
		return false;
	}
	
	return true;
}

/**
 * SUITELET
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
 function suitelet_ValidatePOId(request, response){
	 
	if(request.getMethod() == 'GET'){
		
		//Get Script Config for Appeasement
		if(request.getParameter('act') == 'scriptconfig'){
			
			var objScriptConfig = {
				'form' : SCRIPTCONFIG.getScriptConfigValue('PO: Form: Factory Forms')
			};
			
			response.write(JSON.stringify(objScriptConfig));
		}		
	}	 
 }

/**
 * Alphanumeric text validation
 */
function isValid_alphanumeric(stText){
	
	var regex = /^[a-z0-9]+$/i;
	
	if(stText){
		return regex.test(stText);
	}
	
	return false;
}

/**
 * Checks if validation should apply to current PO form
 */
function isApplicableForm(stFormId){
	
	if(ST_APPLICABLE_FORMS){
		
		var arForms = ST_APPLICABLE_FORMS.split(",");

		if(arForms){
			for(var i = 0; i < arForms.length; i++){
				if(arForms[i] == stFormId){
					return true;
				}
			}	
		}
	}
	
	return false;
}

/**
 * Checks if validation should apply to current PO form
 * Client script interface
 */
function isApplicableForm_Client(){
	
	//Additional checking for PO based on form used
	if(nlapiGetRecordType() == 'purchaseorder'){
		
		//For client script, request Script Config from server only once
		if(!B_SCRIPTCONFIG_QUERY_DONE){
			//Get Script Config value via suitelet
			var stUrl = nlapiResolveURL('SUITELET', 'customscript_erp_po_id_validate_sl', 'customdeploy_erp_po_id_validate_sl') 
						+ '&act=scriptconfig';
			var reply = nlapiRequestURL(stUrl);
			if(reply.getCode() == '200'){
				var stBody = reply.getBody();
				if(stBody){
					var objScriptConfig = JSON.parse(stBody);
					if(objScriptConfig){
						ST_APPLICABLE_FORMS = objScriptConfig['form'];
					}
				}
				
				B_SCRIPTCONFIG_QUERY_DONE = true;
			}	
		}
		
		return isApplicableForm(nlapiGetFieldValue('customform'));
	}
	
	return true;
}