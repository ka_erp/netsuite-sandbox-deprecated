/**
 *	File Name		:	ERP_3PL_Resubmit_IR_SL.js
 *	Function		:	Loads then submits an Item Receipt
 * 	Remarks			:	Developed to trigger User Event scripts deployed to IR
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

/**
 * SUITELET Main
 */
function suitelet_resubmitIR(request, response){
	
	var irId = request.getParameter('irid'); //IR Id	
	nlapiLogExecution('DEBUG', 'irId', irId);

	var objStatus = {};
	
	try{		
		var recIR = nlapiLoadRecord('itemreceipt', irId);		
		nlapiSubmitRecord(recIR, false, true);		
		objStatus.success = true;
	} catch(ex){
		var stError = error.toString();
		nlapiLogExecution('DEBUG', 'error', stError);	
		objStatus.success = false;
		objStatus.error = stError;
	}
	
	response.write(JSON.stringify(objStatus));
}