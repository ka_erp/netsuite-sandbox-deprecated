/**
 *	File Name		:	ERP_WPORT_HYB_RollbackOrders_003_RL.js
 *	Function		:	Order Process - Step 3 (Rollback)
 * 	Remarks			:	Relevant scripts isolated from WTKA_NetSuite_ICTO_Process.js
 *	Prepared by		:	A.Villanueva
 *	Release Dates	:	01-Jun-2016
 * 	Current Version	:	1.0
**/

function rollbackRestlet(requestObject)
{
	var recID    = requestObject.id;
	var orderDet = requestObject.orderDetails;
	var newObj 	 = new Object();
	try
	{
		if(orderDet != null && orderDet.length > 0)
		{
			nlapiLogExecution('debug', recID, 'Rolling back...');
			var rollbackstatus  = rollbackOrderProcess(orderDet); // Rollback all processes
			
			if(rollbackstatus)
			{
				newObj.status	= 'Rollback Success';
				newObj.message	= 'The process has been successfully rolled back and all the transactions listed above have been deleted.';
			}
			else
			{
				newObj.status 	= 'Rollback Failure';
				newObj.message 	= 'The process failed to rollback.';
			}
		}
	}
	catch(rollback_err)
	{
		nlapiLogExecution('debug', recID + ': rollbackRestlet failed', rollback_err);
		newObj.status 	= 'Rollback Failure';
		newObj.message 	= 'The process failed to rollback.';
		newObj.details 	= rollback_err;
		finalResponse.records.push(newObj); //Request Object
	}
	return newObj;
}

function rollbackOrderProcess(objectDetails)
{
	var recordCount = 0;
	nlapiLogExecution('debug', 'objectDetails', JSON.stringify(objectDetails));
	for(var i=0; objectDetails != null &&  i < objectDetails.length; i++)
	{
		try
		{
			var icsoFlag = objectDetails[i].icso;
			var orderId 		= objectDetails[i].orderId;
			var fulfillmentId 	= objectDetails[i].fulfillmentId;
			var invoiceId 		= objectDetails[i].invoiceId;
			var transformtype	= objectDetails[i].transformtype;
			var logId			= objectDetails[i].logId;
			
			if(icsoFlag)
			{
				var poId 		= objectDetails[i].poId;
				if(poId > 0)	var fulfillmentRecord 	= nlapiDeleteRecord('purchaseorder', 	poId, 	 {deletionreason: 8}); //External System Rollback
				nlapiLogExecution('debug', 'Rollback record details', 'orderId: ' + orderId + ' | poId: ' + poId);
			}
			
			// var logStatus 		= deleteLogs(orderId); //Delete Logs
			if(logId != 0)			var logStatus 			= deleteLogsFromIds(logId); //Delete Logs
			if(invoiceId > 0)		var invoiceRecord 		= nlapiDeleteRecord(transformtype, 		invoiceId, 		{deletionreason: 8}); //External System Rollback
			if(fulfillmentId > 0)	var fulfillmentRecord 	= nlapiDeleteRecord('itemfulfillment', 	fulfillmentId, 	{deletionreason: 8}); //External System Rollback
			if(orderId > 0)			var orderRecord 		= nlapiDeleteRecord('salesorder', 		orderId, 		{deletionreason: 8}); //External System Rollback
			nlapiLogExecution('debug', 'Rollback record details', 'orderId: ' + orderId + ' | fulfillmentId: ' + fulfillmentId + ' | invoiceId: ' + invoiceId);
		}
		catch(err_rollback)
		{
			nlapiLogExecution('debug', 'Rollback Failure', 'orderId: ' + orderId + ' || Error: ' + err_rollback);
			return false;
		}
	}
	nlapiLogExecution('debug', 'Rollback Success');
	return true;
}