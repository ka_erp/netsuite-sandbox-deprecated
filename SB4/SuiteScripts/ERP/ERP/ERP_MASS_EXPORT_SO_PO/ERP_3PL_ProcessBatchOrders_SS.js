/**
 *	File Name		:	ERP_3PL_ProcessBatchOrders_SS.js
 *	Function		:	Scheduled Script triggered when a User starts the mass export of SOs/TOs.
 *	Prepared by		:	christopher.neal@kitandace.com, 
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{	
	STATUS_SEND = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Send to 3PL');
}


function processTransactions(type){

	var context = nlapiGetContext();
	var processObject = context.getSetting('SCRIPT', 'custscript_processobject'); //get script parameter values, holds the recordType and list of transactions

	nlapiLogExecution('DEBUG', 'processObject', processObject);
	processObject = JSON.parse(processObject);

	var transactions = processObject.transactions;
	nlapiLogExecution('DEBUG', 'transactions', transactions);

	for (var i = 0; i < transactions.length; i++){ 
        nlapiLogExecution('DEBUG', i + ') Processing ' + processObject.recType, transactions[i]);
        nlapiSubmitField(processObject.recType, transactions[i], 'custbody_export_3pl_status', STATUS_SEND); //set the status on the order, triggers ERP_3PL_Outbound_MassExport_UE
    }
}


