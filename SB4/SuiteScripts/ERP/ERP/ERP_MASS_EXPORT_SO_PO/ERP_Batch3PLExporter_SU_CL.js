/**
 *	File Name		:	ERP_Batch3PLExporter_SU_CL.js
 *	Function		:	-Suitelet to select a number of transactions to send to a 3PL for processsing.
 *                      -Also must be deployed as a client side script to allow for Button press events.
 *	Authors			:	christopher.neal@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{
    STATUS_SEND = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Send to 3PL');
    STATUS_EXPORTED = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Completed');
    STATUS_FAIL = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Failed');
    
    MAX_RESULTS = 1000;
    KA_ALLOCATION_ROLE = 1008;
}

/*
*	name: ERP_Send3PLBatch_SU
*	descr: Entry point into script. Create the GUI form and rerturn it as a response.
*
*	author: chrisotpher.neal@kitandace.com
*	@param: {obj} request - request to the suitelet. Will be 'GET' when the form is entered via the GUI, is 'POST' when the 'Refresh Search' button is pressed
*	@return: {obj} response - response returned from the suitelet
*	
*/
function ERP_Batch3PLExporter_SU(request, response){

    var method = request.getMethod();
    nlapiLogExecution('DEBUG', 'method', method);

    var form = nlapiCreateForm('Batch Send to 3PL'); //Initialize the GUI form
    form.setScript('customscript_erp_send3plbatch_cl'); //set client side script for 1)button calls and 2)detect when page init occurs
    form = makeForm(form, method, request);

    nlapiLogExecution('DEBUG', 'Done', 'Write page as response.');
    response.writePage(form); //return the GUI form

}

/*
*   name: makeForm
*   descr: Make the GUI which will be visible to the user
*
*   author: chrisotpher.neal@kitandace.com
*   @param: {obj} form - the form which the user will see
*   @param: {String} method - type of request, GET or POST
*   @param: {obj} request - request to the suitelet
*   @return: {obj} form - the form which the user will interact with
*/
function makeForm(form, method, request){

    //Add the buttons
    form.addSubmitButton('Refresh Search');
    form.addButton('custpage_sendbutton', 'Batch Send to 3PL', 'sendTo3PL();'); //Add button and the associated function call on the button is pressed

    //add Transaction Types
    var recordTypesField = form.addField('custpage_recordtypes', 'select', 'Transaction Type');
    recordTypesField.addSelectOption('SO', 'Sales Orders');
    recordTypesField.addSelectOption('TO', 'Transfer Orders');

    if (isNotBlank(request.getParameter('custpage_recordtypes'))){ //if the value has been set by the user, set it here
        form.getField('custpage_recordtypes', request.getParameter('custpage_recordtypes')).setDefaultValue(request.getParameter('custpage_recordtypes'));
    }

    //add Include Exported radio field
    form.addField('wasexportedlabel', 'label', 'Include Exported?').setLayoutType('startrow');
    form.addField('wasexported', 'radio', 'No', 'F').setLayoutType('midrow'); //set this to default
    form.addField('wasexported', 'radio', 'Yes', 'T').setLayoutType('midrow');
    form.addField('wasexported', 'radio', 'Both', 'E').setLayoutType('endrow');
    if (isNotBlank(request.getParameter('wasexported'))) {
        form.getField('wasexported', request.getParameter('wasexported')).setDefaultValue(request.getParameter('wasexported'));
    } else {
        form.getField('wasexported', 'F').setDefaultValue('F');
    }

    //add Ship From 
    var cols = new Array();
    cols.push(new nlobjSearchColumn('name'));
    var filters = new Array();
    filters.push(new nlobjSearchFilter('custrecord_perform_sendto3pl', null, 'is', 'T'));
    var shipFromLocations = nlapiSearchRecord('location', null, filters, cols);  //get all the locations with 'custrecord_perform_sendto3pl' == 'T'

    var shipFrom = form.addField('custpage_shipfrom', 'select', 'Warehouse Ship From').setMandatory(true); 
    if (shipFromLocations != null){
        for (var i = 0; i < shipFromLocations.length; i++){
            locationId = shipFromLocations[i].id;
            locationName = shipFromLocations[i].getValue('name');

            locationName = locationName.replace(/[^]*: /i, ""); //regular expression. Remove all the text before the last ': '

            shipFrom.addSelectOption(locationId, locationName); //iteratively add the locations to the select field
        }
    }

    if (isNotBlank(request.getParameter('custpage_shipfrom'))){ //if the value has been set by the user, set it here
        form.getField('custpage_shipfrom', request.getParameter('custpage_shipfrom')).setDefaultValue(request.getParameter('custpage_shipfrom'));
    }

    //add Ship To
    var shipTo = form.addField('custpage_shipto', 'select', 'Warehouse Ship To', 'location');

    if (isNotBlank(request.getParameter('custpage_shipto'))){ //if the value has been set by the user, set it here
        form.getField('custpage_shipto', request.getParameter('custpage_shipto')).setDefaultValue(request.getParameter('custpage_shipto'));
    }

    //add employee list    
    var allocationEmployees = form.addField('allocationemployees', 'select', 'Allocation Employee');

    var search = nlapiLoadSearch('employee', 'customsearch_ka_allocation_employees'); //K&A Allocation Employees //customsearch1461
    var searchResultSet = search.runSearch(); //run the search
    var searchResultsArray = searchResultSet.getResults(0, MAX_RESULTS); //get the results

    var allocationHash = {}; //initialize a hash table, to keep a dictionary of the EMPLOYEE ENTITY ID to EMPLOYEE INTERNAL ID

    if (searchResultsArray != null){
        allocationEmployees.addSelectOption('', '');        
        for (var curEmp = 0; curEmp < searchResultsArray.length; curEmp++){
            var employeeId = searchResultsArray[curEmp].id;
            var employeeEntity = searchResultsArray[curEmp].getValue('entityid');
            var employeeName =  searchResultsArray[curEmp].getValue('altname');

            if (employeeName.length > 0){
                allocationEmployees.addSelectOption(employeeId, employeeName);
            } else {
                allocationEmployees.addSelectOption(employeeId, employeeEntity);    
            }

            allocationHash[employeeEntity] = employeeId;
        }        
    }

    nlapiLogExecution('DEBUG', 'allocationHash', JSON.stringify(allocationHash));

    if (isNotBlank(request.getParameter('allocationemployees'))){ //if the value has been set by the user, set it here
        form.getField('allocationemployees', request.getParameter('allocationemployees')).setDefaultValue(request.getParameter('allocationemployees'));
    }

    //add Start Date & End Date
    form.addField('daterangelabel', 'label', 'Show transactions:');
    
    form.addField('startdate', 'date', 'After Create Date');
    if (isNotBlank(request.getParameter('startdate'))){ //if the value has been set by the user, set it here
        form.getField('startdate', request.getParameter('startdate')).setDefaultValue(request.getParameter('startdate'));
    }

    form.addField('enddate', 'date', 'Before Create Date');
    if (isNotBlank(request.getParameter('enddate'))){ //if the value has been set by the user, set it here
        form.getField('enddate', request.getParameter('enddate')).setDefaultValue(request.getParameter('enddate'));
    }

    //Add Transfer type
    form.addField('custpage_transfertype', 'select', 'Transfer Type', 'customrecord_ka_order_type_list');
    if (isNotBlank(request.getParameter('custpage_transfertype'))){ //if the value has been set by the user, set it here
        form.getField('custpage_transfertype', request.getParameter('custpage_transfertype')).setDefaultValue(request.getParameter('custpage_transfertype'));
    }


    //add list to the form
    var subList = form.addSubList('tranlist', 'list', 'Transactions');
    subList.addMarkAllButtons(); //add button to select all orders

    subList.addField('id', 'integer', 'id').setDisplayType('hidden');
    subList.addField('include', 'checkbox', 'Include');        
    subList.addField('trandate', 'date', 'Created Date');
    subList.addField('type', 'text', 'Type').setDisplayType('hidden');
    subList.addField('transactionnumber', 'text', 'Tran#', 'transaction');
    subList.addField('locationshort', 'text', 'From Location').setDisplayType('inline');
    subList.addField('location', 'select', 'From Location', 'location').setDisplayType('inline').setDisplayType('hidden');
    subList.addField('transferlocationshort', 'text', 'To Location').setDisplayType('inline');;
    subList.addField('transferlocation', 'select', 'To Location', 'location').setDisplayType('inline').setDisplayType('hidden');
    subList.addField('custbody_ka_to_expected_recv_date', 'date', 'Expected Receive Date');
    subList.addField('memo', 'text', 'Memo');
    subList.addField('custbody_export_3pl_status', 'select', '3PL Export Status', 'customlist_3pl_export_statis').setDisplayType('inline');
    subList.addField('formulatext', 'text', 'Status', null).setDisplayType('inline');
    subList.addField('custbody_ka_po_employee', 'text', 'Employee').setDisplayType('hidden');
    subList.addField('employeefield', 'select', 'Employee', 'employee').setDisplayType('inline');
    subList.addField('ordertype', 'select', 'Transfer Type', 'customrecord_ka_order_type_list').setDisplayType('inline');
    subList.addField('lastmodifieddate','text', 'Last Modified');

    //If the 'Refresh Search' button has been pressed, get the search results and put them into the sublist 
    if (method == 'POST'){
        var results = getTransactionsFromSearch(request);

        if (results != null){
            for (var curLine = 1; curLine <= results.length; curLine++){
                nlapiLogExecution('DEBUG', 'Adding line', curLine);

                var curOrder = results[curLine-1];

                nlapiLogExecution('DEBUG', 'curOrder', JSON.stringify(curOrder));

                subList.setLineItemValue('id', curLine, curOrder.id);
                subList.setLineItemValue('custbody_export_3pl_status', curLine, curOrder.getValue('custbody_export_3pl_status'));
                subList.setLineItemValue('trandate', curLine, curOrder.getValue('trandate'));
                subList.setLineItemValue('lastmodifieddate', curLine, curOrder.getValue('lastmodifieddate'));
                subList.setLineItemValue('type', curLine, curOrder.getValue('type'));
                subList.setLineItemValue('transactionnumber', curLine, curOrder.getValue('transactionnumber'));
                
                var str1 = curOrder.getText('location');
                if (isNotBlank(str1)){
                    var res1 = str1.replace(/[^]*: /i, ""); //regular expression. Remove all the text before the last ': 
                    subList.setLineItemValue('locationshort', curLine, res1);
                    subList.setLineItemValue('location', curLine, curOrder.getValue('location'));                    
                }
                
                var str2 = curOrder.getText('transferlocation');
                if (isNotBlank(str2)){
                    var res2 = str2.replace(/[^]*: /i, ""); //regular expression. Remove all the text before the last ': 
                    subList.setLineItemValue('transferlocationshort', curLine, res2);
                    subList.setLineItemValue('transferlocation', curLine, curOrder.getValue('transferlocation'));                    
                }

                subList.setLineItemValue('custbody_ka_to_expected_recv_date', curLine, curOrder.getValue('custbody_ka_to_expected_recv_date'));
                subList.setLineItemValue('memo', curLine, curOrder.getValue('memo'));
                subList.setLineItemValue('formulatext', curLine, curOrder.getValue('formulatext'));

                subList.setLineItemValue('custbody_ka_po_employee', curLine, curOrder.getValue('custbody_ka_po_employee'));

                var orderType = curOrder.getValue('custbody_ka_order_type');
                nlapiLogExecution('DEBUG', 'orderType', JSON.stringify(orderType));

                subList.setLineItemValue('ordertype', curLine, curOrder.getValue('custbody_ka_order_type'));

                var employeeEntityId = curOrder.getValue('custbody_ka_po_employee');
                var employeeId = allocationHash[employeeEntityId];

                nlapiLogExecution('DEBUG', 'allocationHash[' + employeeEntityId + ']', employeeId);

                if (employeeId != undefined){ //check if the employee is part of the Allocations list
                    subList.setLineItemValue('employeefield', curLine, employeeId);    
                }
            }
        }
    }
    
    return form;
}

/*
*   name: getTransactionsFromSearch
*   descr:  Once the user has pressed 'Refresh Search', a new request is made to the Suitelet and the filter
*           options are submitted. When the page reloads this functions add the filters to the saved search
*           and retrieves the results. The results are returned by this function to be added to the sublist.
*
*   author: chrisotpher.neal@kitandace.com
*   @param: {obj} request - request to the suitelet
*   @return: {obj} searchResultsArray - The records from the saved search
*/
function getTransactionsFromSearch(request){

    var search = nlapiLoadSearch('transaction', 'customsearch_batch_3pl_exporter_search'); //Batch 3PL Exporter Search

    //Add record type filter
    if (isNotBlank(request.getParameter('custpage_recordtypes'))){ 

        var typeFilter = request.getParameter('custpage_recordtypes');
        nlapiLogExecution('DEBUG', 'typeFilter', typeFilter);

        var recType = '';
        if (typeFilter == 'SO'){
            recType = 'SalesOrd';        
        } else if (typeFilter == 'TO'){
            recType = 'TrnfrOrd';
        }

        search.addFilter(new nlobjSearchFilter('type', null, 'anyof', recType));
    }

    //Add ship from filter
    if (isNotBlank(request.getParameter('custpage_shipfrom'))){ 

        var fromLocationFilter = request.getParameter('custpage_shipfrom');
        nlapiLogExecution('DEBUG', 'fromLocationFilter', fromLocationFilter);

        search.addFilter(new nlobjSearchFilter('location', null, 'anyof', fromLocationFilter));
    }

    //Add ship to filter
    if (isNotBlank(request.getParameter('custpage_shipto'))){ 

        var toLocationFilter = request.getParameter('custpage_shipto');
        nlapiLogExecution('DEBUG', 'toLocationFilter', toLocationFilter);

        search.addFilter(new nlobjSearchFilter('transferlocation', null, 'anyof', toLocationFilter));
    }

    //Add start date filter
    if (isNotBlank(request.getParameter('startdate'))){ 

        var startdateFilter = request.getParameter('startdate');
        nlapiLogExecution('DEBUG', 'startdateFilter', startdateFilter);

        search.addFilter(new nlobjSearchFilter('trandate', null, 'after', startdateFilter));
    }


    //Add end date filter
    if (isNotBlank(request.getParameter('enddate'))){ 

        var enddateFilter = request.getParameter('enddate');
        nlapiLogExecution('DEBUG', 'enddateFilter', enddateFilter);

        search.addFilter(new nlobjSearchFilter('trandate', null, 'before', enddateFilter));
    }

    //Add 'Include Exported' filter
    if (isNotBlank(request.getParameter('wasexported'))){ 

        var wasexportedFilter = request.getParameter('wasexported');
        nlapiLogExecution('DEBUG', 'wasexported', wasexportedFilter);

        if (wasexportedFilter == 'F') {
            search.addFilter(new nlobjSearchFilter('custbody_export_3pl_status', null, 'noneof', STATUS_EXPORTED)); //Don't show the exported Orders

        } else if (wasexportedFilter == 'T') {
            search.addFilter(new nlobjSearchFilter('custbody_export_3pl_status', null, 'anyof', STATUS_EXPORTED)); //Only show the exported Orders

        } else {
            //no filter, show all orders
        }
    }

    //Add Transaction Type Filter
    if (isNotBlank(request.getParameter('custpage_transfertype'))){
        var transferTypeId = request.getParameter('custpage_transfertype');
        search.addFilter(new nlobjSearchFilter('custbody_ka_order_type', null, 'is', transferTypeId));
    }

    //Add employee filter
    if (isNotBlank(request.getParameter('allocationemployees'))){
        var employeeId = request.getParameter('allocationemployees');
        search.addFilter(new nlobjSearchFilter('employee', null, 'is', employeeId));
    }

    //Filter out B2C (Hybris) Sales Orders
    if(recType == 'SalesOrd'){
        search.addFilter(new nlobjSearchFilter('custbody_wtka_extsys_hybris_order', null, 'is', ''));
    }    
    
    var searchResultSet = search.runSearch(); //run the search
    var searchResultsArray = searchResultSet.getResults(0, MAX_RESULTS); //get the results

    return searchResultsArray; //return the results
}

/*
*   name: sendTo3PL
*   descr:  Loop throught the sublist and identify transactions to be sent.
*           Send the transactions to be processed.
*
*   author: chrisotpher.neal@kitandace.com
*
*/
function sendTo3PL(){    

    //see if there has been any transactions selected
    var count = nlapiGetLineItemCount('tranlist'); //number of transactions in the list
    var ordersToSend = false;
    for (var k = 1; k <= count; k++){
        if (nlapiGetLineItemValue('tranlist', 'include', k) == 'T'){ //see whether to export this line            
            ordersToSend = true; 
            break;
        }
    }

    if (ordersToSend == true){
        alert("Sending transactions to 3PL. This may take a few minutes. Press 'OK'.");
    
        var transactionType = nlapiGetFieldValue('custpage_recordtypes');

        //determine the transaction type
        if (transactionType == 'SO'){
            transactionType = 'salesorder';
        } else if (transactionType == 'TO'){
            transactionType = 'transferorder';
        }

        //loop through all the lines
        var transactions = [];
        var transactionList = ''; //create a string of transaction ids
        for (var i = 1; i <= count; i++){
            
            if (nlapiGetLineItemValue('tranlist', 'include', i) == 'T'){ //see whether to export this line
                var recid = nlapiGetLineItemValue('tranlist', 'id', i);
                var tranId = nlapiGetLineItemValue('tranlist', 'transactionnumber', i);
                transactions.push(recid);

                transactionList = transactionList + recid + '$';
            }
        }
        transactionList = transactionList.slice(0, -1); //remove last character

        var curEnochTime = new Date().getTime();        

        console.log(viewerUrl);

        var startProcessUrl = nlapiResolveURL('SUITELET', 'customscript_batch3plexporter_startproce', 'customdeploy_batch3plexporter_startproce') + '&time=' + curEnochTime + '&recordtype=' + transactionType + '&transactions=' + transactionList; //create the URL to trigger the proessing of the records
        var status = nlapiRequestURL(startProcessUrl); //trigger the processing of the records

        var viewerUrl =  nlapiResolveURL('SUITELET','customscript_erp_batch3pl_viewer_su','customdeploy_erp_batch3pl_viewer_su',null) + '&time=' + curEnochTime + '&recordtype=' + transactionType + '&transactions=' + transactionList; //create the URL to view the process of the export

        console.log(status);
        window.open(viewerUrl, '_self', false); //Open the page to see the progress of the export

    } else {
        alert('No transactions were selected. Select transactions to export.');
    }
}

/*
*   name: startScheduleScript
*   descr: Entry point into script. Receives a list of transactions and passes them to a scheduled script.
*
*   author: chrisotpher.neal@kitandace.com
*   @param: {obj} request - request to the suitelet. Will contain the type of records to be processed and a list of IDs to process.
*   @return: {obj} response - response returned from the suitelet, will be the status of starting the scheduled script.
*   
*/
function startScheduleScript(request, response){

    var method = request.getMethod();
    nlapiLogExecution('DEBUG', 'method', method);

    var recordType = request.getParameter('recordtype'); //get the record type from the parameters
    nlapiLogExecution('DEBUG', 'recordType', recordType);

    var obj = {};
    obj.recType = recordType;

    var transactionsArray = [];
    nlapiLogExecution('DEBUG', 'transaction params', request.getParameter('transactions')); //get all the transactions, note that they are seperated by the character '$'
    transactionsArray = request.getParameter('transactions').split('$'); //remove '$' and puts into an array
    obj.transactions = transactionsArray;

    nlapiLogExecution('DEBUG', 'JSON.stringify(obj)', JSON.stringify(obj));

    var paramArr = [];
    paramArr['custscript_processobject'] =  JSON.stringify(obj); //object to pass in as a paramter to the scheduled script

    var status = nlapiScheduleScript('customscript_3pl_processbatchorders_ss', 'customdeploy_3pl_processbatchorders_ss', paramArr); //set the scheduled script to run
    nlapiLogExecution('DEBUG', 'status', status);

    return status;
}


/*
*   name: clientPageInit_disablePendingApprovals
*   descr:  Client side function (deployed into its own client script), to detect when an order
*           is 'Pending Approval' in the sublist and prevent the user from selecting that row in the sublist
*
*   author: chrisotpher.neal@kitandace.com
*/
function clientPageInit_disablePendingApprovals(type){
    
    var numOrders = nlapiGetLineItemCount('tranlist');
    for(var i = 1; i <= numOrders; i++){
        
        var curStatus = nlapiGetLineItemValue('tranlist', 'formulatext', i);

        if (curStatus == "Pending Approval"){
            nlapiSetLineItemDisabled('tranlist','include', true, i);            
        }
    }
}


/*
*   name: isBlank
*   descr: Determines whether a variable has been set
*
*   author: chrisotpher.neal@kitandace.com (copied from Purchase_OrderAggregate.js)
*/
function isBlank(s) {
    return s === undefined || s === null || s === '' || s.length < 1;
}

/*
*   name: isNotBlank
*   descr: Determines whether a variable has NOT been set
*
*   author: chrisotpher.neal@kitandace.com (copied from Purchase_OrderAggregate.js)
*/
function isNotBlank(s) {
    return !isBlank(s);
}