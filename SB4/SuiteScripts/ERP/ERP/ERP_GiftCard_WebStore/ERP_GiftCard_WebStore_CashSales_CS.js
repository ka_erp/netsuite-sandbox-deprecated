/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Oct 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */


function copyGCstuff(type, form){
	
	//alert("testing");

	
	if(false){
	
	var invFulfilled = nlapiGetFieldValue("custbody_erp_inv_fulfilled");
	
	if(type == 'edit' && invFulfilled == 'T'){
				
		var count = nlapiGetLineItemCount("item");
		
		nlapiLogExecution('DEBUG', 'Gift card object line: count', count); 			
		var arrLineObj = new Array();
		
		for(var i=1; i<= count; i++){
			
			
			
			
			var itemtype = nlapiGetLineItemValue('item', 'itemtype', i); 
			//var itemname = nlapiGetLineItemText('item', 'item', i); 
			var itemid = nlapiGetLineItemValue('item', 'item', i); 
			
			
			nlapiLogExecution("DEBUG", "item", "item: " +  itemid + " type:" + itemtype );
			

			
						
			if(itemtype == 'GiftCert'){
				
				var lineItemAmount = nlapiGetLineItemValue('item', 'amount',i);	
				var lineItemRate = nlapiGetLineItemValue('item', 'rate',i);
				var lineItemItem = nlapiGetLineItemValue('item', 'item',i);
				var lineItemQty = nlapiGetLineItemValue('item','quantity',i);
				var lineGiftcertkey = nlapiGetLineItemValue('item','giftcertkey',i);
				var lineMemo = nlapiGetLineItemValue('item','custcol_erp_memo',i);
				
				var soEmail = nlapiGetFieldValue('email');
				var fName =  nlapiGetFieldValue('custbody_ava_customerfirstname');
				var lName = nlapiGetFieldValue('custbody_ava_customerlastname');
				var giftcertkey = nlapiGetFieldValue('giftcertkey');
				
				
				var objLineObject = new Object();
				objLineObject.itemtype = itemtype;
				objLineObject.amount = lineItemAmount;
				objLineObject.rate  = lineItemRate;
				objLineObject.item  = lineItemItem;
				objLineObject.qty   = lineItemQty;
				objLineObject.email = soEmail;
				objLineObject.fName = fName;
				objLineObject.lName = lName;
				objLineObject.gkey = lineGiftcertkey;
				objLineObject.memo =lineMemo;
				
				//console.log(JSON.stringify(objLineObject)); 
				arrLineObj.push(objLineObject);
																		
				console.log("writing this down: " + JSON.stringify(objLineObject)); 
				
				nlapiLogExecution("DEBUG", "removing the cert", i + " item:" + lineItemItem);
				
				
				nlapiRemoveLineItem('item', i); i--; count--;
//				
//				var randomCode = Math.random().toString(36).substring(11);  randomCode = "GC" + randomCode; 
//				
				
								
			}
			
		}
		
		nlapiLogExecution("AUDIT", "creating new GCs", JSON.stringify(arrLineObj));
		for(var j = 0; j < arrLineObj.length; j++){
//			
			var objLine = arrLineObj[j]; console.log("writing this down (for): " + JSON.stringify(objLine)); 
//			
			nlapiSelectNewLineItem('item');
			nlapiSetCurrentLineItemValue('item','item', 3048, true, true);
			nlapiSetCurrentLineItemValue('item','giftcertfrom', objLine.fName + " " + objLine.lName, true, true);
			nlapiSetCurrentLineItemValue('item','giftcertrecipientemail', objLine.email, true, true);
			nlapiSetCurrentLineItemValue('item','giftcertrecipientname', objLine.fName + " " + objLine.lName, true, true);							
			nlapiSetCurrentLineItemValue('item','amount', objLine.rate, true, true);
			nlapiSetCurrentLineItemValue('item','rate', objLine.rate, true, true);	
			nlapiSetCurrentLineItemValue('item','giftcertkey', objLine.gkey, true, true);	
			nlapiSetCurrentLineItemValue('item','custcol_erp_memo', objLine.memo, true, true);	
			
			
			nlapiCommitLineItem("item");
			//i++; count++; 
//			
//			
		}
		
		//item_machine.copyline(); 
		//item_machine.doAddEdit(); //return false; //nlapiCommitLineItem("item");
		//i++; count++;
		
	}
	
	
	}
}


function magstrip_Parser(){}



function SwipeParserObj(strParse)
{

	var objParsed = new Object(); 
	
	console.log('Testing '); 
    ///////////////////////////////////////////////////////////////
    ///////////////////// member variables ////////////////////////
    objParsed.input_trackdata_str = strParse;
    objParsed.account_name = null;
    objParsed.surname = null;
    objParsed.firstname = null;
    objParsed.acccount = null;
    objParsed.exp_month = null;
    objParsed.exp_year = null;
    objParsed.track1 = null;
    objParsed.track2 = null;
    objParsed.hasTrack1 = false;
    objParsed.hasTrack2 = false;
    /////////////////////////// end member fields /////////////////
    
    
    sTrackData = objParsed.input_trackdata_str;     //--- Get the track data
    
  //-- Example: Track 1 & 2 Data
  //-- %B1234123412341234^CardUser/John^030510100000019301000000877000000?;1234123412341234=0305101193010877?
  //-- Key off of the presence of "^" and "="

  //-- Example: Track 1 Data Only
  //-- B1234123412341234^CardUser/John^030510100000019301000000877000000?
  //-- Key off of the presence of "^" but not "="

  //-- Example: Track 2 Data Only
  //-- 1234123412341234=0305101193010877?
  //-- Key off of the presence of "=" but not "^"

    console.log('YEY!'); 
    
  if ( strParse != '' )
  {
    // alert(strParse);

    //--- Determine the presence of special characters
    nHasTrack1 = strParse.indexOf("^");
    nHasTrack2 = strParse.indexOf("=");
    
    console.log(nHasTrack1);
    console.log(nHasTrack2);

    //--- Set boolean values based off of character presence
    objParsed.hasTrack1 = bHasTrack1 = false;
    objParsed.hasTrack2 = bHasTrack2 = false;
    if (nHasTrack1 > 0) { objParsed.hasTrack1 = bHasTrack1 = true; }
    if (nHasTrack2 > 0) { objParsed.hasTrack2 = bHasTrack2 = true; }

    //--- Test messages
    // alert('nHasTrack1: ' + nHasTrack1 + ' nHasTrack2: ' + nHasTrack2);
    // alert('bHasTrack1: ' + bHasTrack1 + ' bHasTrack2: ' + bHasTrack2);    

    //--- Initialize
    bTrack1_2  = false;
    bTrack1    = false;
    bTrack2    = false;

    
    console.log(1); 
    //--- Determine tracks present
    if (( bHasTrack1) && ( bHasTrack2)) { bTrack1_2 = true; }
    if (( bHasTrack1) && (!bHasTrack2)) { bTrack1   = true; }
    if ((!bHasTrack1) && ( bHasTrack2)) { bTrack2   = true; }

    console.log(2); 
    //--- Test messages
    // alert('bTrack1_2: ' + bTrack1_2 + ' bTrack1: ' + bTrack1 + ' bTrack2: ' + bTrack2);

    //--- Initialize alert message on error
    bShowAlert = false;
    
    //-----------------------------------------------------------------------------    
    //--- Track 1 & 2 cards
    //--- Ex: B1234123412341234^CardUser/John^030510100000019301000000877000000?;1234123412341234=0305101193010877?
    //-----------------------------------------------------------------------------    
    if (bTrack1_2)
    { 
//      alert('Track 1 & 2 swipe');

      strCutUpSwipe = '' + strParse + ' ';
      arrayStrSwipe = new Array(4);
      arrayStrSwipe = strCutUpSwipe.split("^");
  
      var sAccountNumber, sName, sShipToName, sMonth, sYear;
  
      
      console.log(3); 
      if ( arrayStrSwipe.length > 2 )
      {
        objParsed.account = stripAlpha( arrayStrSwipe[0].substring(1,arrayStrSwipe[0].length) );
        objParsed.account_name          = arrayStrSwipe[1];
        objParsed.exp_month         = arrayStrSwipe[2].substring(2,4);
        objParsed.exp_year          = '20' + arrayStrSwipe[2].substring(0,2);
        
        objParsed.array = arrayStrSwipe;         
        
        objParsed.ignorethis =  arrayStrSwipe[2].substring(0,5);
        objParsed.goldnugget =  arrayStrSwipe[2].substring(5,14);
        
        //console.log(JSON.stringify(objParsed));
        
        console.log(4); 
        
        //--- Different card swipe readers include or exclude the % in the front of the track data - when it's there, there are
        //---   problems with parsing on the part of credit cards processor - so strip it off
        if ( sTrackData.substring(0,1) == '%' ) {
            sTrackData = sTrackData.substring(1,sTrackData.length);
        }

           var track2sentinel = sTrackData.indexOf(";");
           if( track2sentinel != -1 ){
               objParsed.track1 = sTrackData.substring(0, track2sentinel);
               objParsed.track2 = sTrackData.substring(track2sentinel);
           }

        //--- parse name field into first/last names
        var nameDelim = objParsed.account_name.indexOf("/");
        if( nameDelim != -1 ){
            objParsed.surname = objParsed.account_name.substring(0, nameDelim);
            objParsed.firstname = objParsed.account_name.substring(nameDelim+1);
        }
        
        //return objParsed;
      }
      else  //--- for "if ( arrayStrSwipe.length > 2 )"
      { 
        bShowAlert = true;  //--- Error -- show alert message
      }
    }
    
    //-----------------------------------------------------------------------------
    //--- Track 1 only cards
    //--- Ex: B1234123412341234^CardUser/John^030510100000019301000000877000000?
    //-----------------------------------------------------------------------------    
    if (bTrack1)
    {
//      alert('Track 1 swipe');

      strCutUpSwipe = '' + strParse + ' ';
      arrayStrSwipe = new Array(4);
      arrayStrSwipe = strCutUpSwipe.split("^");
  
      var sAccountNumber, sName, sShipToName, sMonth, sYear;
  
      if ( arrayStrSwipe.length > 2 )
      {
        objParsed.account = sAccountNumber = stripAlpha( arrayStrSwipe[0].substring( 1,arrayStrSwipe[0].length) );
        objParsed.account_name = sName    = arrayStrSwipe[1];
        objParsed.exp_month = sMonth    = arrayStrSwipe[2].substring(2,4);
        objParsed.exp_year = sYear    = '20' + arrayStrSwipe[2].substring(0,2); 
        objParsed.ignorethis =  arrayStrSwipe[2].substring(0,5);
        objParsed.goldnugget =  arrayStrSwipe[2].substring(6,11);
        
        //--- Different card swipe readers include or exclude the % in
        //--- the front of the track data - when it's there, there are
        //---   problems with parsing on the part of credit cards processor - so strip it off
        if ( sTrackData.substring(0,1) == '%' ) { 
            objParsed.track1 = sTrackData = sTrackData.substring(1,sTrackData.length);
        }
  
        //--- Add track 2 data to the string for processing reasons
//        if (sTrackData.substring(sTrackData.length-1,1) != '?')  //--- Add a ? if not present
//        { sTrackData = sTrackData + '?'; }
        objParsed.track2 = ';' + sAccountNumber + '=' + sYear.substring(2,4) + sMonth + '111111111111?';
        sTrackData = sTrackData + objParsed.track2;
  
        //--- parse name field into first/last names
        var nameDelim = objParsed.account_name.indexOf("/");
        if( nameDelim != -1 ){
            objParsed.surname = objParsed.account_name.substring(0, nameDelim);
            objParsed.firstname = objParsed.account_name.substring(nameDelim+1);
        }
        
        return objParsed;

      }
      else  //--- for "if ( arrayStrSwipe.length > 2 )"
      { 
        bShowAlert = true;  //--- Error -- show alert message
      }
    }
    
    //-----------------------------------------------------------------------------
    //--- Track 2 only cards
    //--- Ex: 1234123412341234=0305101193010877?
    //-----------------------------------------------------------------------------    
    if (bTrack2)
    {
//      alert('Track 2 swipe');
    
      nSeperator  = strParse.indexOf("=");
      sCardNumber = strParse.substring(1,nSeperator);
      sYear       = strParse.substr(nSeperator+1,2);
      sMonth      = strParse.substr(nSeperator+3,2);

      // alert(sCardNumber + ' -- ' + sMonth + '/' + sYear);

      objParsed.account = sAccountNumber = stripAlpha(sCardNumber);
      objParsed.exp_month = sMonth        = sMonth;
      objParsed.exp_year = sYear            = '20' + sYear; 
        
      //--- Different card swipe readers include or exclude the % in the front of the track data - when it's there, 
      //---  there are problems with parsing on the part of credit cards processor - so strip it off
      if ( sTrackData.substring(0,1) == '%' ) {
        sTrackData = sTrackData.substring(1,sTrackData.length);
      }
  
    }
    
    //-----------------------------------------------------------------------------
    //--- No Track Match
    //-----------------------------------------------------------------------------    
    if (((!bTrack1_2) && (!bTrack1) && (!bTrack2)) || (bShowAlert))
    {
      //alert('Difficulty Reading Card Information.\n\nPlease Swipe Card Again.');
    }

//    alert('Track Data: ' + document.formFinal.trackdata.value);
    
    //document.formFinal.trackdata.value = replaceChars(document.formFinal.trackdata.value,';','');
    //document.formFinal.trackdata.value = replaceChars(document.formFinal.trackdata.value,'?','');

//    alert('Track Data: ' + document.formFinal.trackdata.value);

  } //--- end "if ( strParse != '' )"


  	return objParsed; 
  	
    objParsed.dump = function(){
        var s = "";
        var sep = "\r"; // line separator
        s += "Name: " + objParsed.account_name + sep;
        s += "Surname: " + objParsed.surname + sep;
        s += "first name: " + objParsed.firstname + sep;
        s += "account: " + objParsed.account + sep;
        s += "exp_month: " + objParsed.exp_month + sep;
        s += "exp_year: " + objParsed.exp_year + sep;
        s += "has track1: " + objParsed.hasTrack1 + sep;
        s += "has track2: " + objParsed.hasTrack2 + sep;
        s += "TRACK 1: " + objParsed.track1 + sep;
        s += "TRACK 2: " + objParsed.track2 + sep;
        s += "Raw Input Str: " + objParsed.input_trackdata_str + sep;
        
        return s;
    }

    
    
    //return objParsed;

}


function stripAlpha(sInput){
    if( sInput == null )    return '';
    return sInput.replace(/[^0-9]/g, '');
}

function validateField(type,name){
	
	/// alert('name' + name);
	
	console.log("name" + name); 
	
	if(name == 'custbody_erp_magstrip'){
	
		
		
		var magStrip = nlapiGetFieldValue('custbody_erp_magstrip'); 
		
		var objSwipedObject = SwipeParserObj(magStrip); 
		
		//console.log("magStrip" + magStrip + " json " + JSON.stringify(objSwipedObject));
		
		if(objSwipedObject){
			
			if(objSwipedObject.goldnugget){
				var authcode = objSwipedObject.goldnugget; 
				
				console.log(authcode); 
				
				var line = nlapiFindLineItemValue('item', 'itemtype', 'GiftCert');
				
				
				
				console.log(line);
				
				nlapiSelectLineItem('item',line);
				nlapiSetCurrentLineItemValue('item', 'giftcertnumber',  authcode);
				//nlapiCommitLineItem('item');
				//nlapiSetLineItemValue('item', 'giftcertnumber', line, authcode);
				
				console.log(line + " " + authcode);
				nlapiSetFieldValue('custbody_erp_magstrip', "");
				
				
				setTimeout(function(){ jQuery('#item_addedit').click(); }, 1000);
				
				
			}
		}
	}
	
	
	return true;
}


function isolate_gc_pageinit(type,form){

	console.log('AUDIT', 'isolate_gc_pageinit', type);
	console.log('AUDIT', 'isolate_gc_pageinit', form);
	
//	var formid = nlapiGetFieldValue('customform');
//	var gcFulfilled = nlapiGetFieldValue('custbody_erp_allgc_fulfilled');
//	
//	if(gcFulfilled ==  'F' && formid != 108){
//		
//		nlapiSetFieldValue('customform', 108);
//		
//	}else if(type == 'copy'){
//				
//		var hasGC = nlapiGetFieldValue('custbody_erp_so_hasgc');
//                var formid = nlapiGetFieldValue('customform');
//		var exec_context = nlapiGetContext().getExecutionContext();
//		
//		console.log("AUDIT", "CONTEXT", JSON.stringify(exec_context));
//		console.log("AUDIT", "FORMID", formid);					
//		
//		if(exec_context == "userinterface" && formid == 108 && hasGC == 'T'){
//			
//			var count = nlapiGetLineItemCount("item"); nlapiLogExecution("DEBUG", "count", "item count: " + count);
//			var cert = "";
//			
//			try{
//			
//				for(var i=1; i<= count; i++){
//						
//					nlapiSelectLineItem('item',i);
//					cert = cert + " " +  nlapiGetCurrentLineItemValue('item', 'giftcertnumber');
//					
//					var itemtype = nlapiGetCurrentLineItemValue('item', 'itemtype');
//					var itemname = nlapiGetCurrentLineItemText('item', 'item');
//					var itemid = nlapiGetCurrentLineItemValue('item', 'item');
//					
//					
//					nlapiLogExecution("DEBUG", "item", "item: " +  itemid + " type:" + itemtype );
//					
//					if(itemtype != 'GiftCert'){
//						nlapiLogExecution("DEBUG", "removing the cert", itemname);
//						nlapiRemoveLineItem('item', i); i--; count--;
//					}else{
//						nlapiSetCurrentLineItemValue('item', 'custcol_erp_memo', 'test');
//						nlapiCommitLineItem('item');
//					}					
//				}
//				
//				nlapiSetFieldValue('custbody_first_gift_cert_code',cert);
//			
//			}catch(ex){}
//	
//		}
//		
//	}else{}
	
}

function isolate_gc_fieldchange(type,form){
	
	page_reset();
}