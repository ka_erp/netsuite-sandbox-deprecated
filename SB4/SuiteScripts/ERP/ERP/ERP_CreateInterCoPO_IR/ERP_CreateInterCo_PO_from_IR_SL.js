/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       31 Aug 2015     Ivan
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function intercoPOCreation_RunPO_UE_SL(request, response){
	
	
	nlapiLogExecution('DEBUG', 'intercoPOCreation_RunPO_UE_SL', 'enter this'); 
	var transID = request.getParameter( 'transactionRecord' );
	nlapiLogExecution('DEBUG', 'intercoPOCreation_RunPO_UE_SL transID', transID);
	
	try{
		
		if(transID){
	 		  
			var purchaseOrder = nlapiLoadRecord('purchaseorder', transID );	
			purchaseOrder.setFieldValue('memo', 'run execution'); 
			nlapiSubmitRecord(purchaseOrder); 
                        
			nlapiLogExecution('DEBUG', 'intercoPOCreation_RunPO_UE_SL', "after calling " +  new Date().toLocaleString()); 
			nlapiSetRedirectURL('RECORD', 'purchaseorder', transID, false);					    
		    
		}		
	}catch(ex){		
		nlapiLogExecution('DEBUG', 'intercoPOCreation_RunPO_UE_SL: Error', ex);
	}	
	
}



function isNullOrEmpty(valueStr){
    return(valueStr == null || valueStr == "" || valueStr == undefined);
}
