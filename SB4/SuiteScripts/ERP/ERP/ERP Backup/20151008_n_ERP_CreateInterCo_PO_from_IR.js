function createPurchaseOrderRecordAS(type) {
	
	
	// Only run this script if the transaction was generated from a Inventory
	// purchase order

	
	nlapiLogExecution('DEBUG', 'Type', type);
	
	if (type != 'delete') {
	
		// Only start when the purchase orer form 100 is used, the PO field is
		// empty on the item receipt & the customer is not

		var ItemReceipt = nlapiLoadRecord('itemreceipt', nlapiGetRecordId());
		var intercoPO = ItemReceipt.getFieldValue('custbody_ka_intercompany_po');
		
		if(isNullOrEmpty(intercoPO)){
			
			
			var customForm = nlapiLookupField('purchaseorder',ItemReceipt.getFieldValue('createdfrom'), 'customform');		
			var shipTo = nlapiLookupField('purchaseorder',ItemReceipt.getFieldValue('createdfrom'), 'shipto');

			nlapiLogExecution('DEBUG', 'createPurchaseOrderRecord', customForm + " " + intercoPO + " " + shipTo);

			
			var idCustomForm = nlapiLookupField('purchaseorder', ItemReceipt.getFieldValue('createdfrom'), 'customform');
			var bInterCo = isNullOrEmpty(intercoPO);
			
			
			if (idCustomForm == 100  &&  bInterCo
					// location 128321 is the Canada Warehouse K&N
					&& shipTo != 128321) {

				var purchaseOrder = nlapiCreateRecord('purchaseorder', {
					recordmode : 'dynamic'
				});
				// Get the vendor record to be used on PO creation from the customer
				// record on the PO
				// Check for empty or invalid fields

				var customerVendorID = nlapiLookupField('customer', shipTo,
						'custentity_ka_vendor_customer');
				// checking for empty values

				var customerVendorLocation = nlapiLookupField('customer', shipTo,
						'custentity_ka_location');
				var customerVendorSubsidiary = nlapiLookupField('vendor',
						customerVendorID, 'subsidiary');
				var taxCode = nlapiLoadRecord('vendor', customerVendorID)
						.getFieldValue('taxitem');
				
				nlapiLogExecution('DEBUG', 'createPurchaseOrderRecord', customerVendorLocation
						+ " " + customerVendorSubsidiary + " " + taxCode +" " + customerVendorID);
				
				try {
					// set the form with default values
					purchaseOrder.setFieldValue('customform', 159);
					purchaseOrder.setFieldValue('trandate', ItemReceipt
							.getFieldValue('trandate'));
					purchaseOrder.setFieldValue('entity', customerVendorID);
					purchaseOrder.setFieldValue('employee', nlapiGetUser());
					purchaseOrder.setFieldValue('subsidiary',
							customerVendorSubsidiary);
					purchaseOrder.setFieldValue('location', customerVendorLocation);
					purchaseOrder.setFieldValue('location', customerVendorLocation);
					purchaseOrder.setFieldValue('custbody_ka_item_receipt_number', nlapiGetRecordId());
					
					
					var ItemReceiptLineCount = ItemReceipt.getLineItemCount('item');

					for (var j = 1; j <= ItemReceiptLineCount; j++) {
						ItemReceipt.selectLineItem('item', j);
						var qty = ItemReceipt.getLineItemValue('item', 'quantity',
								j);
						var itm = ItemReceipt.getLineItemValue('item', 'item', j);
						purchaseOrder.selectNewLineItem('item');
						purchaseOrder.setCurrentLineItemValue('item', 'item', itm);
						purchaseOrder.setCurrentLineItemValue('item', 'quantity',
								qty);
						purchaseOrder.setCurrentLineItemValue('item', 'taxcode',
								taxCode);
						purchaseOrder.commitLineItem('item');
					}

			

					if (isNullOrEmpty(intercoPO)) {

						var id = nlapiSubmitRecord(purchaseOrder);
						nlapiLogExecution('DEBUG', 'createPurchaseOrderRecord: purchaseOrder submit', id); 
						
						ItemReceipt.setFieldValue('custbody_ka_intercompany_po', id);
	//
//						var url = nlapiResolveURL('SUITELET',
//								'customscript_erp_intercopo_runpo_ue',
//								'customdeploy_erp_intercopo_runpo_ue', true);
//						var params = new Array();
//						params['transactionRecord'] = id;
	//
//						nlapiLogExecution('DEBUG',
//								'intercoPOCreation_RunPO_UE_SL url', url);
//						nlapiLogExecution('DEBUG',
//								'intercoPOCreation_RunPO_UE_SL params', params);

					}

					nlapiSubmitRecord(ItemReceipt);
					
				} catch (ex) {
					
					var errorStr = (ex.getCode != null) ? ex.getCode() + '\n' + ex.getDetails() + '\n' + ex.getStackTrace().join('\n') : ex.toString();
					nlapiLogExecution('debug', 'userEventAfterSubmit Exception error', errorStr);
					
				}
			}
	
		}
	}
}

function createPurchaseOrderRecord(type) {
	nlapiLogExecution('DEBUG', 'Type', type);

	if (type == 'delete') {
		var itemReceiptId = nlapiGetRecordId();
		var purchaseOrderRecord = nlapiLookupField('itemreceipt',
				itemReceiptId, 'custbody_ka_intercompany_po');

		if (!isNullOrEmpty(purchaseOrderRecord)) {
			throw nlapiCreateError(
					"PO PURCHASE ORDER EXISTS",
					"PO Linked to item Receipt - Delete Purchase order before deleting item receipt");
		}
	}

}

function linkPOAfterSubmit(type) {
	if (type != 'delete') {
		var itemReceiptId = nlapiGetRecordId();
		if (!isNullOrEmpty(itemReceiptId)) {
			var purchaseOrderRecord = nlapiLookupField('itemreceipt',
					itemReceiptId, 'custbody_ka_intercompany_po');

			if (!isNullOrEmpty(purchaseOrderRecord)) {
				nlapiSubmitField('purchaseorder', purchaseOrderRecord,
						'custbody_ka_interco_po_item_rec', itemReceiptId, true);
			}

		}
	}
}

function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}