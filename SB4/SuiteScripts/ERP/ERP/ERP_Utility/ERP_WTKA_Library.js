/**
 *	File Name		:	ERP_WTKA_Library.js
 *	Function		:	Library functions file		 
 * 	Authors			:	Rini Thomas, Vinod Kumar
 *	Company			:	Wipro Limited
 *	Release Dates	:	20-Jan-2016 (v1.0), 27-Jan-2016 (v1.1), 5-May-2016 (v2.0)
 * 	Current Version	:	2.0
**/

{
	var SYNC_SYSTEM = {};
	var DIRECTION = {};
	var SYNC_STATUS = {};

	getIntegrationLoggingGlobals();
}

/*
*	name: getIntegrationLoggingGlobals
*	descr:  Get the global variables needed for to create transaction logs. 
*			Note that not all the functions in this library need these globals.
*
*			In order to use the SCRIPTCONFIG feature the libraries ERP_Kit_and_Ace_Functions_LIB.js & ERP_ScriptConfi_LIB.js
*			need to be added. If they aren't added the try-catch in this function will prevent the script from crashing.  
*
*	author: chrisotpher.neal@kitandace.com
*	
*/
function getIntegrationLoggingGlobals()
{
	try {
		SYNC_SYSTEM.NETSUITE = SCRIPTCONFIG.getScriptConfigValue('SYNC_SYSTEM.NETSUITE');
		SYNC_SYSTEM.WFX = SCRIPTCONFIG.getScriptConfigValue('SYNC_SYSTEM.WFX');
		SYNC_SYSTEM.ESB = SCRIPTCONFIG.getScriptConfigValue('SYNC_SYSTEM.ESB');

		DIRECTION.IN = SCRIPTCONFIG.getScriptConfigValue('DIRECTION.IN');
		DIRECTION.OUT = SCRIPTCONFIG.getScriptConfigValue('DIRECTION.OUT');

		SYNC_STATUS.CREATED = SCRIPTCONFIG.getScriptConfigValue('SYNC_STATUS.CREATED');
		SYNC_STATUS.PENDING = SCRIPTCONFIG.getScriptConfigValue('SYNC_STATUS.PENDING');
		SYNC_STATUS.ERROR = SCRIPTCONFIG.getScriptConfigValue('SYNC_STATUS.ERROR');

		nlapiLogExecution('DEBUG', 'Configs: ', JSON.stringify(SYNC_SYSTEM));
		nlapiLogExecution('DEBUG', 'Configs: ', JSON.stringify(DIRECTION));
		nlapiLogExecution('DEBUG', 'Configs: ', JSON.stringify(SYNC_STATUS));
	} catch (err) {
		nlapiLogExecution('DEBUG', 'ERP_ScriptConfig_LIB.js is not added to this script record.', err.toString());
	}
}



function LogCreation(ediType, ediNumber, sourceTransaction, recordId, dataIn, ediStatus, ediResponse, callFlag, linkedInboundIRMessage)
{
	var logArray = new Array();
	
	try{
		var parentId = 0;
		var cols 	 = new Array();
		cols[0]  	 = new nlobjSearchColumn('custrecord_transaction_source');
		cols[1]  	 = new nlobjSearchColumn('custrecord_editype');
		cols[2]  	 = new nlobjSearchColumn('custrecord_edinumber');
		cols[3]  	 = new nlobjSearchColumn('custrecord_edicallstatus');

		var filters  = new Array();
		filters[0]   = new nlobjSearchFilter('custrecord_transaction_source', 	null, 'anyof', sourceTransaction);
		filters[1]   = new nlobjSearchFilter('custrecord_editype', 				null, 'anyof', ediType);
		filters[2]   = new nlobjSearchFilter('custrecord_edinumber', 			null, 'anyof', ediNumber);

		var searchRecord = nlapiSearchRecord('customrecord_integrationlogs', null, filters, cols);
		if(searchRecord != null)
		{
			for(var i=0;  i < searchRecord.length ; i++)
			{
				parentId = searchRecord[i].getId()
				if(callFlag != searchRecord[i].getValue('custrecord_edicallstatus')) nlapiSubmitField('customrecord_integrationlogs', parentId, 'custrecord_edicallstatus', callFlag);
			}
		}
		else
		{
			var logRecord = nlapiCreateRecord('customrecord_integrationlogs');
			logRecord.setFieldValue('custrecord_editype', 	ediType);
			logRecord.setFieldValue('custrecord_edinumber', ediNumber);
			if(sourceTransaction > 0) logRecord.setFieldValue('custrecord_transaction_source',  sourceTransaction);
			if(ediType == 2) 		  logRecord.setFieldValue('custrecord_edicallstatus', 		callFlag);
			parentId = nlapiSubmitRecord(logRecord);
		}

		var logRecordChild = nlapiCreateRecord('customrecord_integrationlogs_child');

		if(recordId > 0) logRecordChild.setFieldValue('custrecord_netsuitetransaction', recordId);
		logRecordChild.setFieldValue('custrecord_payload', 		JSON.stringify(dataIn));
		logRecordChild.setFieldValue('custrecord_edistatus', 	ediStatus);
		logRecordChild.setFieldValue('custrecord_response', 	JSON.stringify(ediResponse));
		logRecordChild.setFieldValue('custrecord_author', 		nlapiGetContext().getName());
		logRecordChild.setFieldValue('custrecord_parentid', 	parentId);
		
		//For Factory PO logging
		if(linkedInboundIRMessage) logRecordChild.setFieldValue('custrecord_inbound_ir_message', 	linkedInboundIRMessage);
		
		var childId = nlapiSubmitRecord(logRecordChild);

		logArray[0]  = parentId;
		logArray[1]  = childId;
	
	} catch(ex){		
		//In case of error, log the error too
		logArray = 	LogCreation(ediType, ediNumber, sourceTransaction, recordId, dataIn, ediStatus, JSON.stringify(ex), callFlag, linkedInboundIRMessage);	
	}
	
	return logArray;
}

function GetAccessToken()
{
	var callFlag 			= 'F';
	var AccessToken 		= new Array();
	var headers 			= new Array();
	headers['Accept'] 		= 'application/x-www-form-urlencoded';
	headers['Content-Type'] = 'application/x-www-form-urlencoded';
	headers['username'] 	= 'erpuser';
	headers['password']		= 'erppassword';

	//TODO - Update the Token URL
	// https://kitandace-process-api-staging.cloudhub.io/kitandace/api/token
	var URL = (nlapiGetContext().getEnvironment() == 'PRODUCTION') ? 'http://mule-worker-kitandace-edi-gateway.cloudhub.io:8082/' : 'http://mule-worker-kitandace-3pl-gateway.cloudhub.io:8082/';
	URL += 'kitandace/api/token?client_id=66c90e1bc6d2468a822b2e7783d748e4&scope=POST_RESOURCE&';
	URL += 'client_secret=f0a79b432f46401884200FA05FDE902E&grant_type=client_credentials';

	nlapiLogExecution('DEBUG','OAUTH URL', URL);

	//Retry upto 5 times with 5 seconds delay only in case of HTTP status code 404/405
	try
	{
		for(var i=0; i < 5; i++)
		{
			var response 	= nlapiRequestURL(URL, null, headers);
			var StatusCode 	= response.code;
			if (StatusCode == 200)
			{
				var Jsonresponse = JSON.parse(response.body);
				AccessToken[0] = '0';
				AccessToken[1] = Jsonresponse.access_token;
				break;
			}
			else if(StatusCode == 404 || StatusCode == 405)
			{
				edistatus 	= 3;
				nlapiLogExecution('DEBUG', 'Time-before', new Date());
				sleepForDuration(3000); //3 seconds
				nlapiLogExecution('DEBUG', 'Time-After', new Date());
			}
			else
			{
				callFlag = 'T';
				var resbody = JSON.parse(response.body);
				AccessToken[0] = '-1';
				AccessToken[1] = 'Failed to fetch token because of ' + resbody.error_description + ' sent in request.';
				break;
			}
		}

		if(!callFlag)
		{
			AccessToken[0] = '-2'; //Failed to reach Cloudhub
			AccessToken[1] = 'Failed to fetch token because of ' + resbody.error_description + ' sent in request.';
		}
	}
	catch(err)
	{
		AccessToken[0] = '-1';
		AccessToken[1] = 'Error Code - ' + err.getCode();
		AccessToken[2] = 'Error Details - ' + err.getDetails()
	}

	return AccessToken;
}

function GetProcessAPIAccessToken(RecType)
{
	var callFlag 			= 'F';
	var AccessToken 		= new Array();
	var headers 			= new Array();

	headers['Accept'] 		= 'application/x-www-form-urlencoded';
	headers['Content-Type'] = 'application/x-www-form-urlencoded';
	headers['username'] 	= 'erpuser';
	headers['password']		= 'erppassword';

	//Domain
	var URL = (nlapiGetContext().getEnvironment() == 'PRODUCTION') ? 
				'https://kitandace-processapi.cloudhub.io/' :
				'https://kitandace-process-api-staging.cloudhub.io/';
	
	if (RecType == 'purchaseorder'){
		//Below url is used for dev
		//URL = 'https://po-process-api-dev.cloudhub.io:443/';	
	}

	URL += 'kitandace/api/token?client_id=66c90e1bc6d2468a822b2e7783d748e4&scope=POST_RESOURCE&';
	URL += 'client_secret=f0a79b432f46401884200FA05FDE902E&grant_type=client_credentials';

	nlapiLogExecution('DEBUG','GetProcessAPIAccessToken: headers', JSON.stringify(headers));
    nlapiLogExecution('DEBUG','GetProcessAPIAccessToken: OAUTH URL', URL);

	//Retry upto 5 times with 5 seconds delay only in case of HTTP status code 404/405
	try
	{
		for(var i=0; i < 5; i++)
		{
			var response 	= nlapiRequestURL(URL, null, headers);
			var StatusCode 	= response.code;
			if (StatusCode == 200)
			{
				var Jsonresponse = JSON.parse(response.body);
				AccessToken[0] = '0';
				AccessToken[1] = Jsonresponse.access_token;
				break;
			}
			else if(StatusCode == 404 || StatusCode == 405)
			{
				edistatus 	= 3;
				nlapiLogExecution('DEBUG', 'Time-before', new Date());
				sleepForDuration(3000); //3 seconds
				nlapiLogExecution('DEBUG', 'Time-After', new Date());
			}
			else
			{
				callFlag = 'T';
				var resbody = JSON.parse(response.body);
				AccessToken[0] = '-1';
				AccessToken[1] = 'Failed to fetch token because of ' + resbody.error_description + ' sent in request.';
				break;
			}
		}

		if(!callFlag)
		{
			AccessToken[0] = '-2'; //Failed to reach Cloudhub
			AccessToken[1] = 'Failed to fetch token because of ' + resbody.error_description + ' sent in request.';
		}
	}
	catch(err)
	{
		AccessToken[0] = '-1';
		AccessToken[1] = 'Error Code - ' + err.getCode();
		AccessToken[2] = 'Error Details - ' + err.getDetails()
	}

	return AccessToken;
}

function sleepForDuration(sleepDuration)
{
    var now = new Date().getTime();
    while(new Date().getTime() < now + sleepDuration) { /*Do nothing*/ }
}

function createLogs(form, searchResult)
{
	var edinumber = searchResult[0].getText('custrecord_edinumber');

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_parentid', null, 'anyof', searchResult[0].getId())); //Parent Id

	var cols = new Array();
	cols.push(new nlobjSearchColumn('custrecord_author'));
	cols.push(new nlobjSearchColumn('custrecord_edistatus'));
	cols.push(new nlobjSearchColumn('custrecord_response'));
	cols.push(new nlobjSearchColumn('custrecord_parentid'));
	cols.push(new nlobjSearchColumn('created'));

	var searchResultChild = nlapiSearchRecord('customrecord_integrationlogs_child', null, filters, cols);
	if(searchResultChild != null)
	{
		form.addTab('custpage_wtkalogs', 'Integration Logs');
		var LogList = form.addSubList('custpage_wtkaloglist', 'list', searchResult[0].getText('custrecord_edinumber'), 'custpage_wtkalogs');
		LogList.addField('custpage_wtkachildid', 	'text', 		'Request/Response');
		LogList.addField('custpage_wtkacreated', 	'date', 		'Date Created');
		LogList.addField('custpage_wtkatime', 		'timeofday', 	'Time');
		LogList.addField('custpage_wtkaauthor', 	'text', 		'Author');
		LogList.addField('custpage_wtkaedinumber', 	'text', 		'EDI Number');
		LogList.addField('custpage_wtkaedistatus', 	'text', 		'EDI Status');
		//LogList.addField('custpage_wtkaresponse', 	'textarea', 	'Response');

		for(var j=0; searchResultChild != null && j<searchResultChild.length; j++)
		{
			var URL = nlapiResolveURL('SUITELET', 'customscript_wtka_viewrequest', 'customdeploy_wtka_viewrequest', false);
			URL = URL + '&edi=' + searchResult[0].getText('custrecord_edinumber') +'&cid=' + searchResultChild[j].getId();

			var FinalURL = '<a href="' + URL + '" target="_blank">View</a>';

			LogList.setLineItemValue('custpage_wtkachildid',	j+1, FinalURL);

			var createdDate = new Date (searchResultChild[j].getValue('created'));

			LogList.setLineItemValue('custpage_wtkacreated',	j+1, nlapiDateToString(createdDate, 'date'));
			LogList.setLineItemValue('custpage_wtkatime',	    j+1, nlapiDateToString(createdDate, 'timeofday'));
			LogList.setLineItemValue('custpage_wtkaauthor', 	j+1, searchResultChild[j].getValue('custrecord_author'));
			LogList.setLineItemValue('custpage_wtkaedinumber', 	j+1, searchResult[0].getText('custrecord_edinumber'));
			LogList.setLineItemValue('custpage_wtkaedistatus',  j+1, searchResultChild[j].getText('custrecord_edistatus'));
			//LogList.setLineItemValue('custpage_wtkaresponse', 	j+1, searchResultChild[j].getValue('custrecord_response'));
		}

		if(searchResult.length > 1)
		{
			var filters1 = new Array();
			filters1.push(new nlobjSearchFilter('custrecord_parentid', null, 'anyof', searchResult[1].getId())); //Parent Id

			var searchResultChild1 = nlapiSearchRecord('customrecord_integrationlogs_child', null, filters1, cols);
			if(searchResultChild1 != null)
			{
				edinumber 	 = searchResult[1].getText('custrecord_edinumber');
				var LogList1 = form.addSubList('custpage_wtkaloglist1', 'staticlist', edinumber, 'custpage_wtkalogs');
				LogList1.addField('custpage_wtkachildid1', 		'text', 		'Request/Response');
				LogList1.addField('custpage_wtkacreated1', 		'date', 		'Date Created');
				LogList1.addField('custpage_wtkatime1', 		'timeofday', 	'Time');
				LogList1.addField('custpage_wtkaauthor1', 		'text', 		'Author');
				LogList1.addField('custpage_wtkaedinumber1', 	'text', 		'EDI Number');
				LogList1.addField('custpage_wtkaedistatus1', 	'text', 		'EDI Status');
				//LogList1.addField('custpage_wtkaresponse1', 	'textarea', 	'Response');

				for(var j=0; j < searchResultChild1.length; j++)
				{
					var URL = nlapiResolveURL('SUITELET', 'customscript_wtka_viewrequest', 'customdeploy_wtka_viewrequest', false);
					URL += '&edi=' + edinumber +'&cid=' + searchResultChild1[j].getId();

					var FinalURL = '<a href="' + URL + '" target="_blank">View</a>';

					LogList1.setLineItemValue('custpage_wtkachildid1',	j+1, FinalURL);

					var createdDate = new Date (searchResultChild1[j].getValue('created'));

					LogList1.setLineItemValue('custpage_wtkacreated1',		j+1, nlapiDateToString(createdDate, 'date'));
					LogList1.setLineItemValue('custpage_wtkatime1',	    	j+1, nlapiDateToString(createdDate, 'timeofday'));
					LogList1.setLineItemValue('custpage_wtkaauthor1', 		j+1, searchResultChild1[j].getValue('custrecord_author'));
					LogList1.setLineItemValue('custpage_wtkaedinumber1', 	j+1, edinumber);
					LogList1.setLineItemValue('custpage_wtkaedistatus1',  	j+1, searchResultChild1[j].getText('custrecord_edistatus'));
					//LogList1.setLineItemValue('custpage_wtkaresponse1', 	j+1, searchResultChild1[j].getValue('custrecord_response'));
				}
			}
		}
	}
}

function ViewRequestResponse(request, response)
{
	if(request.getMethod() == 'GET')
	{
		var viewRequest = nlapiCreateForm('View EDI Request');
		viewRequest.setTitle('View EDI Request/Response');

		var primarygroup = viewRequest.addFieldGroup('primarygroup', 'Primary Information');

		var dateCreated = viewRequest.addField('wtka_datecreated', 		'date', 		'Date Created', 	null, 'primarygroup');
		dateCreated.setDisplayType('inline');

		var timeCreated = viewRequest.addField('wtka_timecreated', 		'timeofday', 	'Time', 			null, 'primarygroup');
		timeCreated.setDisplayType('inline');

		var author 		= viewRequest.addField('wtka_author', 			'text', 		'Author', 			null, 'primarygroup');
		author.setDisplayType('inline');

		var edinumber   = viewRequest.addField('wtka_edinumber', 		'text', 		'EDI Number', 		null, 'primarygroup');
		edinumber.setDisplayType('inline');

		var edistatus 	= viewRequest.addField('wtka_edistatus', 		'text', 		'EDI Status', 		null, 'primarygroup');
		edistatus.setDisplayType('inline');

		var requestgroup = viewRequest.addFieldGroup('requestgroup', 'Request');
		var edirequest 	= viewRequest.addField('wtka_request', 			'longtext', 	'Request Payload', 	null, 'requestgroup');
		edirequest.setDisplayType('inline');

		var responsegroup = viewRequest.addFieldGroup('responsegroup', 'Response');
		var ediresponse = viewRequest.addField('wtka_response', 		'longtext', 	'Response', 		null, 'responsegroup');
		ediresponse.setDisplayType('inline');

		//Fetch data based on Request parameters
		var filters = new Array();
		filters.push(new nlobjSearchFilter('internalid', null, 'anyof', request.getParameter('cid'))); //Child Id

		var cols = new Array();
		cols.push(new nlobjSearchColumn('custrecord_author'));
		cols.push(new nlobjSearchColumn('custrecord_payload'));
		cols.push(new nlobjSearchColumn('custrecord_edistatus'));
		cols.push(new nlobjSearchColumn('custrecord_response'));
		cols.push(new nlobjSearchColumn('custrecord_parentid'));
		cols.push(new nlobjSearchColumn('created'));

		var searchResultChild = nlapiSearchRecord('customrecord_integrationlogs_child', null, filters, cols);
		for(var j=0; j < searchResultChild.length; j++)
		{
			var createdDate = new Date (searchResultChild[j].getValue('created'));
			dateCreated.setDefaultValue(nlapiDateToString(createdDate, 'date'));
			timeCreated.setDefaultValue(nlapiDateToString(createdDate, 'timeofday'));
			author.setDefaultValue(searchResultChild[j].getValue('custrecord_author'));
			edinumber.setDefaultValue(request.getParameter('edi'));
			edistatus.setDefaultValue(searchResultChild[j].getText('custrecord_edistatus'));
			edirequest.setDefaultValue(searchResultChild[j].getValue('custrecord_payload'));
			ediresponse.setDefaultValue(searchResultChild[j].getValue('custrecord_response'));
		}
		response.writePage(viewRequest);
	}
}

function FullTrim(str)
{
	//RightTrim
	for(var i = str.length-1; ((str.charAt(i)<=" ")&&(str.charAt(i)!="")); i--);
	var str1 = str.substring(0,i+1);

	//LeftTrim
	for(var i = 0; ((str1.charAt(i)<=" ")&&(str1.charAt(i)!="")); i++);
	var str_trim = str1.substring(i,str1.length);

	return str_trim;
}

function emptyInbound(dataIn)
{
	//Check if dataIn is empty
	if(JSON.stringify(dataIn) == '{}')
	{
		ErrorObj.messages[0] 			 	= new Object();
		ErrorObj.messages[0].messagetype 	= "Bad Request";
		ErrorObj.messages[0].message 		= "Inbound data is empty";
		invalid_orderid = 'T';
		return false;
	}
	return true;
}

function emptyObject(objectVar, objectName)
{
	//Check if dataIn has header level objects values set
	if(objectVar == null || String(objectVar) == '')
	{
		if(objectName == "Order" || objectName == "Orderheader" || objectName == "Orderid")	invalid_orderid = 'T';
		ErrorObj.messages[0] 			 = new Object();
		ErrorObj.messages[0].messagetype = "Bad Request";
		ErrorObj.messages[0].message 	 = objectName + " data is empty";
		return false;
	}
	return true;
}

function containLines(objectVar, objectName)
{
	//Check if dataIn has line items
	if(objectVar.length < 0)
	{
		ErrorObj.messages[0] 			 = new Object();
		ErrorObj.messages[0].messagetype = "Bad Request";
		ErrorObj.messages[0].message 	 = "No " + objectName + " in the request for processing";
		return false;
	}
	return true;
}

function invalidOrder()
{
	//Invalid Order
	invalid_orderid 				 = 'T';
	ErrorObj.status 				 = "Exception";
	ErrorObj.messages[0] 			 = new Object();
	ErrorObj.messages[0].messagetype = "Invalid order";
	ErrorObj.messages[0].message 	 = "Order cannot be processed as it contains an invalid orderid";
	return false;
}

function getLocationData(recordid)
{
	var fields   = ['address1', 'address2', 'city', 'state', 'country', 'zip', 'phone', 'custrecord_ka_store_opening_date', 'isinactive', 'externalid', 'name'];
	var location = nlapiLookupField('location', recordid, fields);
	location.custrecord_ra_loctype = nlapiLookupField('location', recordid, 'custrecord_ra_loctype', true);
	return location;
}

function getCustomerData(recordid)
{
	var fields   = ['salutation', 'firstname', 'lastname', 'phone', 'email', 'isperson', 'companyname'];
	var customer = nlapiLookupField('customer', recordid, fields);
	if(customer.isperson == 'F')
	{
		customer.firstname = customer.companyname;
		customer.lastname  = customer.salutation = null;
	}
	return customer;
}

function getShippingData(record)
{
	var shipData 			= new Object();
	shipData.shipphone 		= record.getFieldValue('shipphone');
	shipData.shipaddr1 		= record.getFieldValue('shipaddr1');
	shipData.shipaddr2 		= record.getFieldValue('shipaddr2');
	shipData.shipcity 		= record.getFieldValue('shipcity');
	shipData.shipzip 		= record.getFieldValue('shipzip');
	shipData.shipstate 		= record.getFieldValue('shipstate');
	shipData.shipcountry 	= record.getFieldValue('shipcountry');
	return shipData;
}

function getBillingData(record)
{
	var billData 			= new Object();
	billData.billphone 		= record.getFieldValue('billphone');
	billData.billaddr1 		= record.getFieldValue('billaddr1');
	billData.billaddr2 		= record.getFieldValue('billaddr2');
	billData.billcity		= record.getFieldValue('billcity');
	billData.billzip 		= record.getFieldValue('billzip');
	billData.billstate 		= record.getFieldValue('billstate');
	billData.billcountry 	= record.getFieldValue('billcountry');
	return billData;
}

function ErrorProcess(dataIn)
{
	var errorStatus = (ErrorObj.status == "Error") ? 2 : 3; // 2- Error, 3-Exception
	if(invalid_orderid == 'F')
	{
		var orderid 	= ErrorObj.orderid;
		var ErrorLogs 	= LogCreation(1, 2, orderid, 0, dataIn, errorStatus, ErrorObj, 'F'); //0 - Outbound Call, 1 - Inbound call
		nlapiLogExecution('DEBUG', 'Error Object', JSON.stringify(ErrorObj));
	}
	var subject = 'Inbound 945 call processing failure';
	var body 	= 'Hello,<br><br>';
	if(invalid_orderid == 'T')	body += 'Process failed in NetSuite. Data Received from Mulesoft<br>';
	else	body += 'Process against <b>' + ErrorObj.ordernumber + '</b> failed in NetSuite. <br><b>Data Received from Mulesoft: </b><br><br>';
	//else	body += 'Process against <b>' + dataIn.shipmentconfirmation.ordernumber + '</b> failed in NetSuite. <br><b>Data Received from Mulesoft: </b><br><br>';
	body += JSON.stringify(dataIn) + '<br><br><br>';
	body += '<b>NetSuite Response: </b><br><br>';
	body += JSON.stringify(ErrorObj);
	body += '<br><br><br><br><br><br><br>Thanks';
	body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
	if(sendEmail)	nlapiSendEmail(243423, toList, subject, body, ccList);
}

function readValue(objectValue)
{
	if(objectValue != null && objectValue.length > 0) return objectValue
	else return "";
}

function FetchOrderId(type, InboundOrderNumber, extsys, addCols, nonIcto)
{
	try
	{
		var filters = new Array();
		var cols 	= new Array();
		cols.push(new nlobjSearchColumn('custbody_wtka_extsys_order_number'));
		cols.push(new nlobjSearchColumn('tranid'));
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
		if(extsys  != null)		filters.push(new nlobjSearchFilter('custbody_wtka_extsys_order_number',  null, 'haskeywords', InboundOrderNumber));		
		if(type != 'inventorytransfer'){
			if(nonIcto != null)		filters.push(new nlobjSearchFilter('intercotransaction',  				 null, 'anyof', 	  '@NONE@'));
			else					filters.push(new nlobjSearchFilter('tranid', 				 			 null, 'haskeywords', InboundOrderNumber));
		}

		if(addCols != null)	cols.push(new nlobjSearchColumn(addCols));

		var searchResult = nlapiSearchRecord(type, null, filters, cols);
		if(searchResult != null)
		{
			for(var i=0; i <searchResult.length; i++)
			{
				if(extsys != null)
				{
					if(searchResult[i].getValue('custbody_wtka_extsys_order_number') == InboundOrderNumber)
					{
						if(addCols != null)
						{
							var ret = new Object();
							ret.id  = searchResult[i].getId();
							ret.col = searchResult[i].getValue(addCols);
							return ret;
						}
						else	return searchResult[i].getId();
					}
				}
				else
				{
					if(searchResult[i].getValue('tranid') == InboundOrderNumber)
					{
						if(addCols != null)
						{
							var ret = new Object();
							ret.id  = searchResult[i].getId();
							ret.col = searchResult[i].getValue(addCols);
							return ret;
						}
						else	return searchResult[i].getId();
					}
				}
			}
			return 0;
		}
		else						return 0;
	}
	catch(funerr)
	{
		nlapiLogExecution('DEBUG', 'Unable to Fetch Order ID', funerr.message);
		return -1;
	}
}

function FetchInternalId(type, name, extsys, addCols)
{
	var filters = new Array();
	var columns = new Array();
	if(type == 'customer')
	{
		if(extsys)
		{
			filters.push(new nlobjSearchFilter('firstname', null, 'is', 'Anonymous'));
			filters.push(new nlobjSearchFilter('lastname',  null, 'is', name));
		}
		if(addCols != null)	columns.push(new nlobjSearchColumn(addCols));
	}
	else	filters.push(new nlobjSearchFilter('name', null, 'contains', name));

	var searchResult = nlapiSearchRecord(type, null, filters, columns)
	if(searchResult != null)
	{
		if(addCols != null)
		{
			var newObject = new Object();
			newObject.col = searchResult[0].getValue(addCols);
			newObject.Id  = searchResult[0].getId();
			return newObject;
		}
		else	return searchResult[0].getId();

	}
	else
	{
		ErrorObj.messages[0] 			 = new Object();
		ErrorObj.messages[0].messagetype = type + " error";
		ErrorObj.messages[0].message 	 = "Invalid query. Error: " + type + ": ";
		if(type == 'customer' && extsys) ErrorObj.messages[0].message 	 += "Anonymous "
		ErrorObj.messages[0].message 	 += name + " cannot be found.";
		if(addCols != null)
		{
			var newObject = new Object();
			newObject.col = 0;
			newObject.Id  = 0;
			return newObject;
		}
		else	return 0;
	}
}

function FetchIdFromName(type, InArray)
{
	if(!(InArray.constructor == Array)) //Not array
	{
		var tempVar = InArray;
		if(typeof tempVar == 'number')	tempVar = String(tempVar);
		InArray 	= new Array();
		InArray[0] 	= tempVar;
	}
	//nlapiLogExecution('DEBUG', 'type: ' + type, 'InArray: ' + InArray);
	var OutArray = new Array();

	try
	{
		/* Generating the filter condition in Array form*/
		var filters = new Array();
		filters.push(generateMultiValueFilterExpression(InArray, type));

		var cols = new Array();
		if(type == 'item')	cols.push(new nlobjSearchColumn('upccode'));

		var searchResult = nlapiSearchRecord(type, null, filters, cols);
		if(searchResult == null)
		{
			if(type == 'item')
			{
				ErrorObj.messages[0] 			 = new Object();
				ErrorObj.messages[0].messagetype = "Item error";
				ErrorObj.messages[0].message 	 = "Invalid query. Error: Items with UPC Codes " + InArray + " cannot be found.";
			}
			else if(type == 'location')
			{
				ErrorObj.messages[0] 			 = new Object();
				ErrorObj.messages[0].messagetype = "Location error";
				ErrorObj.messages[0].message 	 = "Invalid query. Error: Locations with Location Codes " + InArray + " cannot be found.";
			}
			OutArray[0] = 'Error';
		}
		else
		{
			if(type == 'item')
			{
				for(var i=0; searchResult != null && i<searchResult.length; i++)
				{
					var itemObject 		= new Object();
					itemObject.UPCCode  = searchResult[i].getValue('upccode');
					itemObject.Id 		= searchResult[i].getId();
					OutArray.push(itemObject);
				}
			}
			else
			{
				for(var i=0; searchResult != null && i<searchResult.length; i++)
				{
					OutArray[OutArray.length] = searchResult[i].getId();
				}
			}
		}
		if(InArray.length > 0 && OutArray.length != InArray.length)
		{
			var checkFlag = 0;
			for(var i=0; i<InArray.length; i++)
			{
				for(j=0; j<OutArray.length; j++)
				{
					if(OutArray[j].UPCCode == InArray[i])
					{
						checkFlag++;
						break;
					}
				}
			}

			if(checkFlag < InArray.length)
			{
				ErrorObj.messages[0] 			 = new Object();
				if(type == 'item')				ErrorObj.messages[0].messagetype = "Item error";
				else if(type == 'location')		ErrorObj.messages[0].messagetype = "Location error";
				else							ErrorObj.messages[0].messagetype = "Fetch ID error";
				ErrorObj.messages[0].message 	 = "Invalid query. Error: One or more of codes in [" + InArray + "] cannot be found. ";
				OutArray[0]					 	 = 'Error';
			}
		}
	}
	catch(err)
	{
		ErrorObj.messages[0] 			 = new Object();
		if(type == 'item')				ErrorObj.messages[0].messagetype = "Item error";
		else if(type == 'location')		ErrorObj.messages[0].messagetype = "Location error";
		else							ErrorObj.messages[0].messagetype = "Fetch ID error";
		ErrorObj.messages[0].message 	 = "Invalid query. Error: [" + InArray + "] cannot be found. " + err;
		OutArray[0]					 	 = 'Error';
	}
	return OutArray;
}

function FetchSubsidiaryId(type, name)
{
	var subsid = new Object();
	var Filter = new Array();
	Filter.push(new nlobjSearchFilter(type,   null, 'anyof', 	name));
	Filter.push(new nlobjSearchFilter('name', null, 'contains', 'Kit and Ace Operating'));

	var Cols = new Array();
	Cols.push(new nlobjSearchColumn('name'));
	Cols.push(new nlobjSearchColumn('internalid'));
	Cols.push(new nlobjSearchColumn('country'));

	var record = nlapiSearchRecord('subsidiary', null, Filter, Cols);
	if(record != null)
	{
		subsid.code 	= record[0].getValue('internalid');
		subsid.name 	= record[0].getValue('name').split(':')[1];
		subsid.country 	= record[0].getText('country');
		return subsid;
	}
	else
	{
		return -1;
	}
}

function generateMultiValueFilterExpression(inArray, type)
{
	var filterExp = new Array();
	for(var i=0; inArray != null && i<2*inArray.length && (i+1)<2*inArray.length; i++)
	{
		if((i+1)%2 == 0)
		{
			filterExp[i] = 'OR';
		}
		else
		{
			filterExp[i] = new Array();
			if(type == 'item')
			{
				filterExp[i][0] = 'upccode';
				filterExp[i][1] = 'is';
			}
			else if(type == 'location')
			{
				filterExp[i][0] = 'name';
				filterExp[i][1] = 'contains';
			}
			else
			{
				filterExp[i] 	= new Array();
				filterExp[i][0] = type;
				filterExp[i][1] = 'anyof';
			}
			filterExp[i][2] = inArray[(i/2)];
		}
	}
	return filterExp;
}

function updateCustomRecord()
{
	var fields = new Array();
	var values = new Array();

	fields.push('custrecord_wtka_extsys_sync_filecounter');
	fields.push('custrecord_wtka_extsys_sync_status');
	fields.push('custrecord_wtka_extsys_sync_lastprocess');
	fields.push('custrecord_wtka_extsys_sync_timeoutcount');

	values.push(fileCounter);
	values.push(crStatus);
	values.push(lastProcessedId);
	values.push(crTimeOutCount);

	crFileCounter   = fileCounter;
	crLastProcessed = lastProcessedId;
	nlapiSubmitField('customrecord_wtka_extsys_sync', crRequestId, fields, values, false);
}

function quantityParse(qtyField, type)
{
	if(type == 'int')	qtyField = (qtyField != null && qtyField.length > 0) 		? parseInt(qtyField) : 0;
	else				qtyField = (qtyField != null && parseFloat(qtyField) > 0) 	? parseFloat(qtyField) : 0.00;
	return qtyField;
}

function updateStatus(type) // Before Submit
{
	if(type != 'delete')
	{
		var status_id = nlapiGetFieldValue('custrecord_wtka_extsys_sync_status');
		var file 	  = parseInt(nlapiGetFieldValue('custrecord_wtka_extsys_sync_file'));
		var status;
		switch(status_id)
		{
			case '0':
			case '1':
			case '2':
			case '3':
				status = parseInt(status_id) + 1; // Index starts from 1 in List
				break;
			default:
				status = 5;
		}
		if(status_id != null)	nlapiSetFieldValue('custrecord_wtka_extsys_sync_status_name', status);
		try
		{
			if(file > 0)		nlapiSetFieldValue('custrecord_wtka_extsys_sync_file_link', file);
		}
		catch(err)
		{
			nlapiLogExecution('debug', 'Unable to link file', err);
		}
	}
}

function triggerScheduledScript(type)  // After Submit
{
	if(type == 'create')
	{
		SwitchScheduleScript(parseInt(nlapiGetFieldValue('custrecord_wtka_extsys_sync_type')));
	}
}

function SwitchScheduleScript(syncType)
{
	var status;
	switch(syncType)
	{
		case 1:
			status = nlapiScheduleScript('customscript_wtka_sync_inventory', 'customdeploy_wtka_ns_inventory_sync_sc');
			break;

		case 2:
			status = nlapiScheduleScript('customscript_wtka_sync_product', 'customdeploy_wtka_ns_product_sync_sc');
			break;

		default:
			nlapiLogExecution('debug', 'Default Script');
			break;
	}
	nlapiLogExecution('debug', 'Script', status);
}

function cleanUpSyncRecords(type)
{
	var numberOfDays 		= nlapiGetContext().getSetting('SCRIPT', 'custscript_wtka_cleanup_days');
	var NumberOfDaysPrior 	= (numberOfDays != null && numberOfDays != 'Error' && numberOfDays.length > 0) ? (0-numberOfDays) : 0;
	var cleanUpDate 		= nlapiAddDays(new Date(), NumberOfDaysPrior);
	cleanUpDate 			= nlapiDateToString(cleanUpDate, 'datetime');

	var filters  	 = new Array();
	filters.push(new nlobjSearchFilter('created', null, 'before', cleanUpDate));
	var updateRecord = nlapiSearchRecord('customrecord_wtka_extsys_sync', null, filters, null);
	var counter		 = (updateRecord != null) ? updateRecord.length : 0;
	for(var i=0; i<counter; i++)
	{
		nlapiLogExecution('debug', 'Record ID', updateRecord[i].getId());
		try
		{
			nlapiDeleteRecord('customrecord_wtka_extsys_sync', updateRecord[i].getId());
		}
		catch(err)
		{
			nlapiLogExecution('debug', 'Error in External System Synchronization Records Cleanup', err);
		}
	}

	/* Cleanup Additional Files */
	var folderName	= 'External System Synchronization';
	var filter 		= new Array();
	filter.push(new nlobjSearchFilter('name', null, 'is', folderName));
	var fileCabinet = nlapiSearchRecord('folder', null, filter, null);
	var folderId	= (fileCabinet != null) ? fileCabinet[0].getId() : 0;

	filters.push(new nlobjSearchFilter('folder', null, 'is', folderId));
	var FileList 	= nlapiSearchRecord('file', null, filters, null);
	var fileCounter = (FileList != null) ? FileList.length : 0;

	for(var i=0; i<fileCounter; i++)
	{
		try
		{
			var file_del = nlapiDeleteFile(FileList[i].getId());
			nlapiLogExecution('debug', 'Counter: ' + i, 'del: ' + file_del);
		}
		catch(err)
		{
			nlapiLogExecution('debug', 'Error in File Cleanup', err);
		}
	}

}

function validateISODate(dateValue, type)
{
	var dateVar = (type != null) ? type : 'fromDate';
	try
	{
		/* Validate date format */
		var invalidDateFlag = false;
		var utcformat 		= false;
		var extformat 		= false;
		var dateFormat		= "YYYY-MM-DDTHH:MM:SSZ or YYYY-MM-DDTHH:MM:SS+/-HH:MM";
		if(dateValue.match(/^\d{4}\-\d{1,2}\-\d{1,2}T\d{1,2}:\d{2}:\d{2}Z/) != null) 				 utcformat = true;
		if(dateValue.match(/^\d{4}\-\d{1,2}\-\d{1,2}T\d{1,2}:\d{2}:\d{2}[+-]\d{1,2}:\d{2}/) != null) extformat = true;

		if(extformat || utcformat) //Check Date
		{
			var checkDate = moment(dateValue).format('l LT');
			if(checkDate == "Invalid date")		invalidDateFlag = true;
		}
		else
		{
			invalidDateFlag = true;
		}

		if(!invalidDateFlag) //Valid date format
		{
			if(type == null)
			{
				var fromDate;
				fromDate		= moment(dateValue).format('l LT');
				var currentDate = new Date();
				if(fromDate > currentDate) //Future Date/Time
				{
					ErrorObj.messages[0] 			 = new Object();
					ErrorObj.messages[0].messagetype = "Bad Request";
					ErrorObj.messages[0].message 	 = "Invalid Date " + dateValue + "." + dateVar + " cannot be greater than the current date. Please try again with a different date.";
					return false;
				}
			}
			else return true;
		}
		else
		{
			if(dateValue != null)
			{
				var formatCheck = dateValue.split('Z');
				if(formatCheck.length > 1)	dateFormat = "YYYY-MM-DDTHH:MM:SSZ";
				else if(formatCheck != '')	dateFormat = "YYYY-MM-DDTHH:MM:SS+/-HH:MM";
			}

			ErrorObj.messages[0] 			 = new Object();
			ErrorObj.messages[0].messagetype = "Bad Request";
			ErrorObj.messages[0].message 	 = "Invalid Date format " + dateValue + " for " + dateVar + ". Please try again with the date in the format " + dateFormat;
			return false;
		}
		return true;
	}
	catch(err)
	{
		ErrorObj.messages[0] 			 = new Object();
		ErrorObj.messages[0].messagetype = "Bad Request";
		ErrorObj.messages[0].message 	 = "Invalid Date " + dateValue + " for " + dateVar + ". Please try again with a different date.";
		ErrorObj.messages[0].message 	 += err;
		return false;
	}
}

function deleteLogs(sourceTransaction)
{
	try
	{
		if(sourceTransaction > 0)
		{
			var filters_source = new Array();
			filters_source[0]  = new nlobjSearchFilter('custrecord_transaction_source', null, 'anyof', 		  sourceTransaction);
			var searchResult   = nlapiSearchRecord('customrecord_integrationlogs', 		null, filters_source, null);
			for(var i=0; searchResult != null && i<searchResult.length; i++)
			{
				var filters_child = new Array();
				filters_child[0]  = new nlobjSearchFilter('custrecord_parentid', 				null, 'anyof', 		 searchResult[i].getId()); //Parent Id
				var searchResultChild = nlapiSearchRecord('customrecord_integrationlogs_child', null, filters_child, null);
				for(var j=0; searchResultChild != null && j < searchResultChild.length; j++)
				{
					nlapiDeleteRecord('customrecord_integrationlogs_child', searchResultChild[j].getId());
				}
				nlapiDeleteRecord('customrecord_integrationlogs', searchResult[i].getId());
			}
		}
	}
	catch(err_logs)
	{
		nlapiLogExecution('debug', 'Unable to clear logs', err_logs);
		return false;
	}
	return true;
}

function deleteLogsFromIds(logIdArray)
{
	try
	{
		if(logIdArray != null)
		{
			try
			{
				nlapiDeleteRecord('customrecord_integrationlogs_child', logIdArray[1]); //Child
				nlapiDeleteRecord('customrecord_integrationlogs', 		logIdArray[0]); // Parent
			}
			catch(log_err)
			{
				if(log_err.code == 'THIS_RECORD_CANNOT_BE_DELETED_BECAUSE_IT_HAS_DEPENDENT_RECORDS')
				{
					nlapiDeleteRecord('customrecord_integrationlogs_child', logIdArray[0]); //Child
					nlapiDeleteRecord('customrecord_integrationlogs', 		logIdArray[1]); // Parent
				}
			}
		}
	}
	catch(err_logs)
	{
		nlapiLogExecution('debug', 'Unable to clear logs', err_logs);
		return false;
	}
	return true;
}

function getNetSuiteSyncData(dataIn, type)
{
	var funReturnObject   		= new Object();
	funReturnObject.response  	= new Object();

	var title 	= (type == 1) ? 'Inventory' : 'Product';
	var subject = title + ' Synchronization failure';
	var body 	= 'Hello,<br><br>';
	body += title + ' Synchronization failed in NetSuite due to below error.<br>';
	var resourceId = dataIn.resourceId;

	if(resourceId == null)
	{
		funReturnObject.response.message = "Resource ID missing. Please enter Resource ID and try again.";
		var message = 'Code: INVALID_RESOURCEID : Details: Resource ID missing. Please enter Resource ID and try again.';
		sendSyncMail(sendEmail, subject, body, message, dataIn);
	}
	else
	{
		try
		{
			var requestCheck 	= resourceId.split('_');
			/*var custResourceId 	= '';
			if(requestCheck.length > 1)
			{
				for(var r=0; r<requestCheck.length-1; r++)
				{
					custResourceId 	+= requestCheck[r];
				}
			}
			else
			{
				custResourceId	= resourceId;
			}*/
			var custResourceId;
			if(requestCheck.length > 1)	custResourceId 	= requestCheck[0];
			else						custResourceId	= resourceId;
			var cols = new Array();
			cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_file'));
			cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_filecounter'));
			cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_status'));
			cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_resourceid'));

			var filters = new Array();
			filters.push(new nlobjSearchFilter('custrecord_wtka_extsys_sync_resourceid', 	null, 'is', custResourceId));
			filters.push(new nlobjSearchFilter('custrecord_wtka_extsys_sync_type', 			null, 'is', type)); //1-Inventory, 2-Product

			var updateRecord = nlapiSearchRecord('customrecord_wtka_extsys_sync', null, filters, cols);

			if(updateRecord == null)
			{
				funReturnObject.response.message = "Invalid Resource ID: (" + resourceId + "). Please try again with a different Resource ID";
				var message = 'Code: INVALID_RESOURCEID : Details: Resource ID missing. Please enter Resource ID and try again.';
				sendSyncMail(sendEmail, subject, body, message, dataIn);
			}
			else
			{
				if(requestCheck.length > 1) //Return File Payload
				{
					try
					{
						var fileIndex 	= parseInt(requestCheck[requestCheck.length-1]);
						var filepath	= folderName + '/';
						filepath 		+= (fileIndex > 0) ? resourceId : custResourceId;
						var fileLoad  	= nlapiLoadFile(filepath);
						var extraFile 	= fileLoad.getValue();
						if(extraFile.length > 0)	funReturnObject = JSON.parse(extraFile);
						else						funReturnObject = [];
					}
					catch(err)
					{
						funReturnObject.response.message = "ERROR: ", err;
						sendSyncMail(sendEmail, subject, body, err, dataIn);
						nlapiLogExecution('debug', 'File read failed', err);
					}
				}
				else // Lookup Resource
				{
					var status = updateRecord[0].getValue('custrecord_wtka_extsys_sync_status'); //0 = not started / In Queue, // 1 = In Progress, // 2 = Completed, // 3 = Error
					if(status == 2) // Completed
					{
						var counter = parseInt(updateRecord[0].getValue('custrecord_wtka_extsys_sync_filecounter'));
						if(counter > 1) //Multiple Files
						{
							funReturnObject.response.pages = new Array();
							for(var f=0; f<counter; f++)	funReturnObject.response.pages.push(custResourceId + '_' + f);
						}
						else //Single File
						{
							try
							{
								var filepath  = folderName + '/' + custResourceId;
								var fileLoad  = nlapiLoadFile(filepath);
								var extraFile = fileLoad.getValue();
								if(extraFile.length > 0)	funReturnObject = JSON.parse(extraFile);
								else						funReturnObject = [];
							}
							catch(fileread_err)
							{
								funReturnObject.response.message = "ERROR: " + fileread_err;
								sendSyncMail(sendEmail, subject, body, fileread_err, dataIn);
								nlapiLogExecution('debug', 'File read failed', fileread_err);
							}
						}
					}
					else
					{
						var processStatus;
						switch(status)
						{
							case '0': // Queued
							case '1': //In Progress
								funReturnObject.response.status 	= "PENDING";
								break;
							default : //Error
								funReturnObject.response.status 	= "ERROR";
								break;
						}
					}
					nlapiLogExecution('debug', 'File', updateRecord[0].getValue('custrecord_wtka_extsys_sync_file'));
				}
			}

		}
		catch(fun_err)
		{
			funReturnObject = "Exception: " + fun_err;
			sendSyncMail(sendEmail, subject, body, fun_err, dataIn);
			nlapiLogExecution('debug', 'Get-Exception', fun_err);
		}
	}
	nlapiLogExecution('debug', 'funReturnObject', JSON.stringify(funReturnObject));
	return funReturnObject;
}

function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}


function FetchTaxData(orderLineDetails, type)
{
	var taxCountry 		= orderLineDetails.tax[0].taxCountry;
	var itemTax 		= new Object();
	var taxAmount1 		= 0.00, taxAmount2  = 0.00,   taxRate1 = 0.0, taxRate2 = 0.0;
	itemTax.dualTax 	= false;
	itemTax.taxCountry 	= taxCountry;
	if(taxCountry == 'US')
	{
		/* //Calculate taxRate from the amtTaxApplied value
		for(var i in orderLineDetails.tax)
		{
			taxAmount1 	+= quantityParse(orderLineDetails.tax[i].amtTaxApplied, 'float');
		}
		var itemPrice 		= (type == 'order') ? parseFloat(orderLineDetails.amtItemPrice.amtCurrentPrice) : parseFloat(orderLineDetails.amountItemPrice.amtCurrentPrice);
		var totalOrderQty	= (type == 'order') ? parseInt(orderLineDetails.countItemOrdered) : parseInt(orderLineDetails.countItemReturned);
		var taxRateCalc		= (itemPrice > 0 && totalOrderQty > 0) ? (taxAmount1*100)/(itemPrice*totalOrderQty) : 0.00;
		taxRate1			= quantityParse(taxRateCalc.toFixed(2), 'float');
		*/
		
		//Add up taxRates and taxAmounts
		for(var i in orderLineDetails.tax)
		{
			taxRate1 	+= quantityParse(orderLineDetails.tax[i].taxRate, 	  	'float');
			taxAmount1 	+= quantityParse(orderLineDetails.tax[i].amtTaxApplied, 'float');
		}
		itemTax.taxRate1   = taxRate1;
		itemTax.taxRate2   = taxRate2;
		itemTax.tax1Amount = taxAmount1.toFixed(2);
		itemTax.tax2Amount = taxAmount2;
	}
	else
	{
		taxRate1 	= quantityParse(orderLineDetails.tax[0].taxRate, 	   'float');
		taxAmount1  = quantityParse(orderLineDetails.tax[0].amtTaxApplied, 'float');
		if(orderLineDetails.tax.length > 1) // Secondary tax
		{
			itemTax.dualTax = true;
			taxRate2 		= quantityParse(orderLineDetails.tax[1].taxRate, 	   'float');
			taxAmount2  	= quantityParse(orderLineDetails.tax[1].amtTaxApplied, 'float');
		}
	}
	itemTax.taxRate1    = taxRate1;
	itemTax.taxRate2    = taxRate2;
	itemTax.tax1Amount  = taxAmount1;
	itemTax.tax2Amount  = taxAmount2;
	// nlapiLogExecution('DEBUG', 'itemTax', JSON.stringify(itemTax));
	return itemTax;
}

function setRecordFieldValues(recordId, orderDetails, finalResponse, rollback, status, shipmentArray, lastprocessed, icsoFlag, completedFlag)
{
	var fields 	= new Array();
	var values  = new Array();
	var setFlag = false;

	if(orderDetails != null)
	{

		// nlapiLogExecution('DEBUG', 'Setting Order Logs', 'testing');
		// nlapiLogExecution('DEBUG', 'recordId', recordId);

		// var orderId = orderDetails[0].orderId;
		// var fulfillmentId = orderDetails[0].fulfillmentId;
		// var invoiceId = orderDetails[0].invoiceId;
		// nlapiLogExecution('DEBUG', 'orderId', orderId);
		// nlapiLogExecution('DEBUG', 'fulfillmentId', fulfillmentId);
		// nlapiLogExecution('DEBUG', 'invoiceId', invoiceId);

		// transactionRecordsArray = [];
		// transactionRecordsArray.push(orderId);
		// transactionRecordsArray.push(fulfillmentId);
		// transactionRecordsArray.push(invoiceId);

		// linkTransactionsToExternalWTKA(recordId, transactionRecordsArray);

		var data = JSON.stringify(orderDetails);
		fields.push('custrecord_wtka_processed_ids');
		values.push(data);
		setFlag = true;
	}
	if(finalResponse != null)
	{
		var response = JSON.stringify(finalResponse);
		fields.push('custrecord_wtka_final_response');
		values.push(response);
		setFlag = true;
	}
	if(rollback != null)
	{
		var rollbackValue = rollback;
		fields.push('custrecord_wtka_rollback');
		values.push(rollback);
		setFlag = true;
	}
	if(shipmentArray != null)
	{
		var shipment = JSON.stringify(shipmentArray);
		fields.push('custrecord_wtka_orderid_shipment');
		values.push(shipment);
		setFlag = true;
	}
	if(status != null)
	{
		var ordStatus = status;
		fields.push('custrecord_wtka_order_status');
		values.push(ordStatus);
		setFlag = true;
		nlapiLogExecution('debug', 'Status set to', ordStatus);
		
		if(status == 6)
		{
			var subject = 'Order processing failure';
			var body 	= 'Hello,<br><br>';
			body += 'Order request failed in NetSuite due to below error.<br>';
			body += '<br><b>Error Details: </b><br>' + JSON.stringify(finalResponse);
			body += '<br><br>Process needs to be resubmitted or handled manually through user interface after corrections.';
			body += '<br><br><br><br><br><br><br>Thanks';
			body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
			nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
			if(sendEmail)	nlapiSendEmail(243423, toList, subject, body, ccList);
		}
	}
	if(lastprocessed != null)
	{
		var lastprocessedId = lastprocessed;
		fields.push('custrecord_wtka_order_lastprocessed');
		values.push(lastprocessedId);
		setFlag = true;
	}
	if(icsoFlag != null)
	{
		var icso = (icsoFlag) ? 'T' : 'F';
		fields.push('custrecord_wtka_icto_flag');
		values.push(icso);
		setFlag = true;
	}
	
	if(completedFlag != null)
	{
		var completed = (completedFlag) ? 'T' : 'F';
		fields.push('custrecord_wtka_completed_request');
		values.push(completed);
		setFlag = true;
	}
	if(setFlag)	nlapiSubmitField('customrecord_wtka_external_orders', recordId, fields, values, false);
}

function InvokeCloudhub(RecType, RecId, TranNo, RecStatus, inventoryFlow)
{
    nlapiLogExecution("DEBUG","InvokeCloudhub: AFTER SUBMIT","STARTING WORKFLOW TO SEND SO/TO/PO");
	var respBody;
	callFlag = 'F';
	var recordTypeId = '';

	nlapiLogExecution("DEBUG","InvokeCloudhub: RecType", RecType);
    nlapiLogExecution("DEBUG","InvokeCloudhub: RecId", RecId);
    nlapiLogExecution("DEBUG","InvokeCloudhub: TranNo", TranNo);
    nlapiLogExecution("DEBUG","InvokeCloudhub: RecStatus", RecStatus);

	var AccessToken = GetProcessAPIAccessToken(RecType);

	var headers 			= new Array();
	headers['Accept'] 		= 'application/json';
	headers['Content-Type'] = 'application/json';

	//Domain
	var URL = (nlapiGetContext().getEnvironment() == 'PRODUCTION') ? 
				'https://kitandace-processapi.cloudhub.io/' : 
				'https://kitandace-process-api-staging.cloudhub.io/';
	
	if (RecType == 'purchaseorder'){ //Logic for 'purchaseorder'
		
		//Actual
		URL += 'v1.0/purchaseorder/' + TranNo;
		
		//Below url is used for dev
		//URL = 'https://po-process-api-dev.cloudhub.io/v1.0/purchaseorder/' + TranNo; //e.g. https://po-process-api-dev.cloudhub.io/v1.0/purchaseorder/000245303
		
		recordTypeId = 15; //internal id of the record type 'Purchase Order'

	} else { //Logic for 'salesorder' and 'transferorder'

		// Build payload in JSON format
		var BodyPayLoad = WTKA_BuildTOSOPayLoad(RecType, TranNo, RecStatus);
		nlapiLogExecution('DEBUG', 'BodyPayLoad', JSON.stringify(BodyPayLoad));
		
		if(inventoryFlow != null)	URL += 'v1.0/b2c/order/ship';
		else						URL += (RecType == 'salesorder') ? 'v1.0/b2b/order/ship' : 'v1.0/b2b/transferorder/ship';

		if (RecType == 'salesorder'){
			recordTypeId = 31; //internal id of the record type 'Sales Order'
		}
	}
		
    URL += '?access_token=' + AccessToken[1];

    nlapiLogExecution('DEBUG','Headers', JSON.stringify(headers));
    nlapiLogExecution('DEBUG','Access URL', URL);

    var integrationLogMessage = {}; //initialize an object to set on the Integration Log
    integrationLogMessage.URL = URL;
    integrationLogMessage.BodyPayLoad = BodyPayLoad;
    integrationLogMessage.Responses = [];
    integrationLogMessage.RecType = RecType;
    integrationLogMessage.RecId = RecId;
    integrationLogMessage.TranNo = TranNo;
    integrationLogMessage.RecStatus = RecStatus;
    integrationLogMessage.inventoryFlow = inventoryFlow;

	//Retry upto 5 times with 5 seconds delay only in case of HTTP status code 404/405
	try
	{
		for(var i=0; i < 5; i++)
		{
			var response = {};
			if (RecType == 'purchaseorder'){
				response = nlapiRequestURL(URL, null, null, 'POST');
			} else { //'transferorder' OR 'salesorder'
				response = nlapiRequestURL(URL, JSON.stringify(BodyPayLoad), headers);
			}
			
			var StatusCode 	= response.code;
			nlapiLogExecution('DEBUG','StatusCode', StatusCode);

			var StatusBody 	= response.body;
			nlapiLogExecution('DEBUG', 'StatusBody', JSON.stringify(StatusBody));

			integrationLogMessage.Responses[i] = {};
			integrationLogMessage.Responses[i].StatusCode = StatusCode;
			
			respBody = StatusCode + ':' + JSON.stringify(StatusBody);

			if (StatusCode == 200 || StatusCode == 202){				
				edistatus = 1;
				nlapiLogExecution('DEBUG', 'Success', respBody);
				callFlag = 'T';
				integrationLogMessage.Responses[i].respBody = respBody;
				break;
			} else if(StatusCode == 404 || StatusCode == 405) {
				edistatus = 3;
				nlapiLogExecution('DEBUG', 'Time-before', new Date());
				sleepForDuration(3000); //3 seconds
				nlapiLogExecution('DEBUG', 'Time-After', new Date());
			} else {
				edistatus = 2;
				//callFlag = 'T';
				nlapiLogExecution('DEBUG', 'Failure', response.code);
				break;
			}
		}

		nlapiLogExecution('DEBUG', 'callFlag', callFlag);

		if(callFlag == 'F')
		{
			var subject = 'Outbound 940 Call Failure';
			var body 	= 'Hello,<br><br>';
			body 	+= 'Transaction failed for order ID <b>' + TranNo + '</b> and will be retried after 30 mins. No action required at this point of time.'
			body 	+= '<br><br><br><br><br><br><br>Thanks';
			body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
			nlapiLogExecution('DEBUG', 'Call Failure-Body', body);
			if(sendEmail){
				//nlapiSendEmail(243423, toList, subject, body, ccList);				
			}				
		}
		
		var objLogMsg = {url: URL, response: respBody};
					
		if(RecType == 'purchaseorder'){
			//Purchase Order logging
			FactoryPOLib.log(2, RecId, 0, TranNo, edistatus, JSON.stringify(objLogMsg), callFlag);		
		} else {
			var orderLogs = LogCreation(2, 1, RecId, 0, BodyPayLoad, edistatus, JSON.stringify(objLogMsg), callFlag);		
			nlapiLogExecution('debug', 'orderLogs', orderLogs);
			integrationLogMessage.orderLogs = orderLogs;

			//Integration Log
			nlapiLogExecution('DEBUG', 'Integration Logging', JSON.stringify(integrationLogMessage));		

			//Integration Log
			nlapiLogExecution('DEBUG', 'Integration Logging', JSON.stringify(integrationLogMessage));
			createTransactionLog(null, TranNo, DIRECTION.OUT, SYNC_SYSTEM.NETSUITE, SYNC_SYSTEM.ESB, recordTypeId, SYNC_STATUS.CREATED, JSON.stringify(integrationLogMessage), null, 99);
		}
	}
	catch(err)
	{
		var subject = 'Outbound 940 Call Failure';
		var body = 'Hello,<br><br>';
		body += 'Transaction failed for order number <b>' + RecId + '</b>, details as below:<br>'
		body += err;
		body += '<br><br><br>Thanks';
		body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
		nlapiLogExecution('DEBUG', 'Try/Catch-Body', body);
		
		if(sendEmail){
			//nlapiSendEmail(243423, toList, subject, body, ccList);
		}

		if(RecType == 'purchaseorder'){
			//Purchase Order logging
			FactoryPOLib.log(2, RecId, 0, TranNo, 3, 'Outbound PO Call Failure : ' + err, callFlag);
		} else {
			//Integration Log
			integrationLogMessage.err = err;
			nlapiLogExecution('DEBUG', 'Integration Logging', JSON.stringify(integrationLogMessage));		
			createTransactionLog(null, TranNo, DIRECTION.OUT, SYNC_SYSTEM.NETSUITE, SYNC_SYSTEM.ESB, recordTypeId, SYNC_STATUS.ERROR, JSON.stringify(integrationLogMessage), null, 98);
			
			//For additional logging - START
			//Log Exception
			LogCreation(2, 1, RecId, 0, BodyPayLoad, 3, subject + ' : ' + err, callFlag);
			//For additional logging - END
		}
	}

	return callFlag;
}


function WTKA_BuildTOSOPayLoad(Rectype, TranNo, RecStatus)
{
	nlapiLogExecution('DEBUG', 'recType: ' + Rectype, 'TranNo: ' + TranNo);
	var JSONPayload 								= new Object();
	JSONPayload.order 								= new Object();
	JSONPayload.order.orderheader 					= new Object();
	JSONPayload.order.orderheader.orderId 			= TranNo;
	JSONPayload.order.orderheader.orderStatus 		= RecStatus;
	return JSONPayload;
}

function FetchTaxCode(taxCountry, province)
{
	var taxCode 	= -1;
	if(taxCountry == 'US')
	{
		taxCode = Taxcode_US;
	}
	else
	{
		var name = 'CA_GR_' + province;
		var filters  = new Array();
		filters[0]   = new nlobjSearchFilter('itemid', 		null, 'contains', 	name);
		filters[1]   = new nlobjSearchFilter('isinactive', 	null, 'is', 		'F');
		var searchRecord = nlapiSearchRecord('taxgroup', 	null, filters, 		null);
		if(searchRecord != null & searchRecord.length == 1)
		{
			taxCode = searchRecord[0].getId();
		}
	}
	return taxCode;
}

function getEnvironmentValues()
{
	var company = nlapiGetContext().getCompany();
	var environ	= nlapiGetContext().getEnvironment();
	
	CA_US_Vendor = 454534; //SW US Inc. - Inventory - Canada (Inventory)
	US_CA_Vendor = 454664; //Kit and Ace Designs Inc. - USA (Inventory)
	
	CA_EcommDC = 7;  // 1901;
	US_EcommDC = 50; // 2901;
	CA_Virtual = 24; // 1902
	US_Virtual = 51; // 2902
	
	if(environ == 'PRODUCTION')	
	{
		Ext_Payment					= 36; // External System Payment
		InvTrfURL 					= 'https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=448&deploy=1'; //WTKA ICTO InventoryTransfer Process
		OrderURL 					= 'https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=449&deploy=1'; //WTKA_Process_Orders
		RollbkURL					= 'https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=450&deploy=1'; //WTKA_Rollback_Orders
		Fedex_CA					= 33626;
		Fedex_US					= 33625;
		UPS_CA						= 3207;
		UPS_US						= 15771;
		Taxcode_US					= 47013; // External System - US
	}
	else
	{
		if(company == '3883338_SB2') //SB2
		{
			Ext_Payment					= 34; // External System Payment
			InvTrfURL 					= 'https://rest.sandbox.netsuite.com/app/site/hosting/restlet.nl?script=465&deploy=1'; //WTKA ICTO InventoryTransfer Process
			OrderURL 					= 'https://rest.sandbox.netsuite.com/app/site/hosting/restlet.nl?script=466&deploy=1'; //WTKA_Process_Orders
			RollbkURL					= 'https://rest.sandbox.netsuite.com/app/site/hosting/restlet.nl?script=467&deploy=1'; //WTKA_Rollback_Orders
			Fedex_CA					= 232;
			Fedex_US					= 32636;
			UPS_CA						= 3207;
			UPS_US						= 15771;
			Taxcode_US					= 32637; // External System - US
			
		}
		else if(company == '3883338') //SB1
		{
			Ext_Payment					= 67; // External System Payment
			InvTrfURL 					= 'https://rest.sandbox.netsuite.com/app/site/hosting/restlet.nl?script=459&deploy=1'; //WTKA ICTO InventoryTransfer Process
			OrderURL 					= 'https://rest.sandbox.netsuite.com/app/site/hosting/restlet.nl?script=457&deploy=1'; //WTKA_Process_Orders
			RollbkURL					= 'https://rest.sandbox.netsuite.com/app/site/hosting/restlet.nl?script=460&deploy=1'; //WTKA_Rollback_Orders
			Fedex_CA					= 33626;
			Fedex_US					= 33625;
			UPS_CA						= 3207;
			UPS_US						= 15771;
			Taxcode_US					= 43692; // External System - US
		}
	}
}

function sendSyncMail(sendEmail, subject, body, message, request)
{
	body += '<br><b>Error Details: </b><br>' + JSON.stringify(message);
	body 	+= '<br><br><b>Data received in NetSuite: </b><br><br>' + JSON.stringify(request) + '<br><br><br>';
	body += '<br><br><br><br><br><br><br>Thanks';
	body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
	nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
	if(sendEmail)	nlapiSendEmail(243423, toList, subject, body, ccList);
}

/*
*	name: createTransactionLog
*	descr:  Creates an Integration Log custom record. The parameters are used to set information on the record. 
*			By passing in a value for the field 'extTransactionId', the created log will be attached to the WTKA_External_Order
*			record with the same value on the field 'ORDER ID' (custrecord_wtka_orderid)
*
*			Note that all fields are optional.
*
*	author: chrisotpher.neal@kitandace.com
*	@param: {obj} dataIn 				- JSON object passed into Netsuite
*	@param: {string} extTransactionId 	- Transaction name passed into Netsuite
*	@param: {int} direction  			- Reference of the direction of the flow. (List: IN/OUT)
*	@param: {int} fromSystem        	- Reference of the systemn inititating the message (List: Router Integration Systems)
*	@param: {int} toSystem          	- Reference of the system receiving the message (List: Router Integration Systems)   
*	@param: {int} recordType        	- Reference of the record being type being synced (List: Transactions) 
*	@param: {int} status            	- Reference of the status of the record sync(List: Integration Status)
*	@param: {string} logMessage     	- text for the log message
*	@param: {id} nsTranId 				- Netsuite Internal id of a transaction
*	@param: {int} index 				- index value of the log (a general)
*	@return: {int} logRecordId 			- Netsuite internal id of the log record
*	
*/
function createTransactionLog(dataIn, extTransactionId, direction, fromSystem, toSystem, recordType, status, logMessage, nsTranId, index){

	//Ensure the data passed in is valid (if it is empty/null/undefined set to '')
	var dataIn_val 				= isEmpty(dataIn) ? {} : dataIn;
	var extTransactionId_val 	= isEmpty(extTransactionId) ? '' : extTransactionId;
	var direction_val 			= isEmpty(direction) ? '' : direction;
	var fromSystem_val 			= isEmpty(fromSystem) ? '' : fromSystem;
	var toSystem_val 			= isEmpty(toSystem) ? '' : toSystem;
	var recordType_val 			= isEmpty(recordType) ? '' : recordType;
	var status_val 				= isEmpty(status) ? '' : status;
	var logMessage_val 			= isEmpty(logMessage) ? '' : logMessage;
	var nsTranId_val 			= isEmpty(nsTranId) ? '' : nsTranId;
	var index_val 				= isEmpty(index) ? '' : index;

	if (false){		
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn_val));
		nlapiLogExecution('debug', 'extTransactionId_val', extTransactionId_val);
		nlapiLogExecution('debug', 'direction', direction_val);
		nlapiLogExecution('debug', 'fromSystem', fromSystem_val);
		nlapiLogExecution('debug', 'toSystem', toSystem_val);
		nlapiLogExecution('debug', 'recordType', recordType_val);
		nlapiLogExecution('debug', 'status', status_val);
		nlapiLogExecution('debug', 'logMessage', logMessage_val);
		nlapiLogExecution('debug', 'nsTranId_val', nsTranId_val);
		nlapiLogExecution('debug', 'index_val', index_val);
	}

	//Create the log record	
	var logRecord = nlapiCreateRecord('customrecord_integrations_log');

	logRecord.setFieldValue('custrecord_intlog_transaction_number', extTransactionId_val);
	logRecord.setFieldValue('custrecord_intlog_in_out', direction_val);
	logRecord.setFieldValue('custrecord_intlog_log_from', fromSystem_val);
	logRecord.setFieldValue('custrecord_intlog_log_to', toSystem_val);
	logRecord.setFieldValue('custrecord_intlog_record_type', recordType_val);
	logRecord.setFieldValue('custrecord_intlog_index', index_val);
	logRecord.setFieldValue('custrecord_intlog_transaction_id', nsTranId_val);

	var startDate = getCurrentDateAsString();
	var endDate = getCurrentDateAsString();

	logRecord.setFieldValue('custrecord_intlog_start', startDate);
	logRecord.setFieldValue('custrecord_intlog_end', endDate);
	
	logRecord.setFieldValue('custrecord_intlog_status', status_val);
	logRecord.setFieldValue('custrecord_intlog_log_message', logMessage_val);

	if (isEmpty(extTransactionId) == false){
		var WTKA_External_Order_id = getWTKAExternalOrderId(extTransactionId);//Get the parent WTKA_Exteranl_Order ID (if it exists)			
		if (WTKA_External_Order_id != -1){
			logRecord.setFieldValue('custrecord_wtka_external_orders_link', WTKA_External_Order_id);
		}
	}
	var logRecordId = nlapiSubmitRecord(logRecord);

	nlapiLogExecution('DEBUG', 'Created Intgration Log', 'Log: ' + logRecordId);

	return logRecordId;
}

/*
*	name: linkTransactionsToExternalWTKA
*	descr:  Pass in the Internal ID of the WTKA_External_Order record. The function will perform
*			a saved search to find the Netsuite Transaction Records attached to the Integration Logs associated with the WTKA_External_Order record.
*			Then attach the Transaction records onto the WTKA_External_Order record.
*
*	author: chrisotpher.neal@kitandace.com
*	@param: {id} wtkaRecordId - Internal ID of the WTKA_External_Order record
*	
*/
function linkTransactionsToExternalWTKA(wtkaRecordId){

	//APPLE V. 2016/10/18 : Operational Issue : Surround with try-catch
	try{
		nlapiLogExecution('debug', 'Linking Transactions on the WTKA_External_Order...', 'wtkaRecordId: ' + wtkaRecordId);

		//get all the transactions related to the WTKA_External_Order record
		var cols = new Array();
		cols.push(new nlobjSearchColumn('custrecord_intlog_transaction_id'));
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_wtka_external_orders_link', null, 'is', wtkaRecordId));
		var intLogs = nlapiSearchRecord('customrecord_integrations_log', null, filters, cols);	
		
		var allTransactionIds = [];
		if (intLogs != null){
			for (var i = 0; i < intLogs.length; i++){
				if (intLogs[i].getValue('custrecord_intlog_transaction_id') != ''){
					allTransactionIds.push(intLogs[i].getValue('custrecord_intlog_transaction_id'));
				}
			}		
		}

		//Check if there are no transaction IDs on the record
		if (isEmpty(allTransactionIds) == false){

			nlapiLogExecution('debug', 'allTransactionIds', JSON.stringify(allTransactionIds)); 

			var WTKA_External_Order = nlapiLoadRecord('customrecord_wtka_external_orders', wtkaRecordId); 

			if (allTransactionIds.length == 1){
				WTKA_External_Order.setFieldValue('custrecord_external_transactions_links', allTransactionIds); //use 'setFieldValue' when there is only 1 transaction to set
			} else {
				WTKA_External_Order.setFieldValues('custrecord_external_transactions_links', allTransactionIds); //use 'setFieldValues' when there is 2 or more transactions to set
			}		

			wtkaRecordId = nlapiSubmitRecord(WTKA_External_Order); //save the changes to the record

			nlapiLogExecution('debug', 'Linking transaction to WTKA_External_Order Succeeded', 'wtkaRecordId: ' + wtkaRecordId + ', allTransactionIds: ' + JSON.stringify(allTransactionIds)); 
		} else {
			nlapiLogExecution('debug', 'No transactions to link to WTKA_External_Order ', 'wtkaRecordId: ' + wtkaRecordId + ', allTransactionIds: ' + JSON.stringify(allTransactionIds)); 
		}
	
	} catch(error){
		nlapiLogExecution('debug', 'linkTransactionsToExternalWTKA error', error.toString());
	}
}

/*
*	name: getCurrentDateAsString
*	descr:  get the current date and format it to the valid format to be set to a date field
*
*	author: chrisotpher.neal@kitandace.com
*	@return: {string} curDateString - current date as a string (format: 6/17/2016 3:17:38 PM)
*	
*/
function getCurrentDateAsString(){

	var curDateString = nlapiDateToString(new Date, 'datetimetz');
	
	curDateString = curDateString.replace('pm', 'PM'); //replace the 'pm' with 'PM' (if it exists)
	curDateString = curDateString.replace('am', 'AM'); //replace the 'am' with 'AM' (if it exists)

	return curDateString;
}


/*
*	name: getWTKAExternalOrderId
*	descr:  Get the Netsuite ID of the WTKA_External_Order using the External Order ID
*
*	author: chrisotpher.neal@kitandace.com
*	@param: {string} extTransactionId 		- Transaction name passed into Netsuite
*	@return: {id} WTKA_External_Order_id	- the ID of the WTKA_External_Order if it exists, -1 if it does not exist
*	
*/
function getWTKAExternalOrderId(extTransactionId){

	var WTKA_External_Order_id = -1;
		
	//Get WTKA_External_Order 
	var cols = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wtka_orderid', null, 'is', extTransactionId));
	WTKA_External_Order = nlapiSearchRecord('customrecord_wtka_external_orders', null, filters, cols);	

	if (WTKA_External_Order != null){
		WTKA_External_Order_id = WTKA_External_Order[0].id; //Note: there will only be 1 WTKA_External_Order for each external ID
	}	

	return WTKA_External_Order_id;
}