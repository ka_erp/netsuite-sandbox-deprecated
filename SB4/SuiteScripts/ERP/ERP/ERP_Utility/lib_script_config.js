//Script Configuration Utility
/**
*@NApiVersion 2.x
*@NModuleScope Public
*/
define(['N/search', 
		'N/runtime',
		'./lib_ka_functions',
		'./lib_script_config_obj',
		'N/log'
	   ],
	scriptconfig);

function scriptconfig (search, runtime, lib_ka_functions, scobject, log) {
		
	return {
		createInstance : function(){
			return new function() {
				var stEnviron = runtime.envType;
				var stAccountID = runtime.accountId;
				var stScriptId = runtime.getCurrentScript().id;
				var objScriptConfig = {};
				
				//Fetch Script Config records data
				var scSearch = search.create({
					type: 'customrecord_erp_script_configuration',
					filters: [['isinactive', search.Operator.IS, 'F']],
					columns: [search.createColumn({
									name: 'internalid',
									sort: search.Sort.DESC
								}), 
							  'name', 
							  'custrecord_erp_ssconfig_type',
							  'custrecord_erp_ssconfig_scriptrecordtype',
							  'custrecord_erp_ssconfig_scriptname',
							  'custrecord_erp_ssconfig_environment',
							  'custrecord_erp_ssconfig_accountid',
							  'custrecord_erp_ssconfig_scriptvalue']
				});
				
				var resultSet = scSearch.run();
				var results = null;
				if(resultSet){
					results = lib_ka_functions.getAllSearchResults(resultSet);
				}

				//Store results as object
				var key;
				if(results){		
					for(var i = 0; i < results.length; i++){	

						key = results[i].getValue('name').toUpperCase();
						
						//Environment must match if Environment is specified
						if(results[i].getValue('custrecord_erp_ssconfig_environment')
							&& results[i].getValue('custrecord_erp_ssconfig_environment').toUpperCase() != stEnviron.toUpperCase()){
							continue;
						}
						
						//Account ID must match if Account ID is specified
						if(results[i].getValue('custrecord_erp_ssconfig_accountid')
							&& results[i].getValue('custrecord_erp_ssconfig_accountid').toUpperCase() != stAccountID.toUpperCase()){
							continue;
						}
						
						//Script ID must match if Script ID is specified
						if(results[i].getValue('custrecord_erp_ssconfig_scriptname')
							&& results[i].getValue('custrecord_erp_ssconfig_scriptname').toUpperCase() != stScriptId.toUpperCase()){
							continue;
						}
						
						//Do not replace existing data
						if(objScriptConfig[key]){
							continue;
						}
						
						objScriptConfig[key] = 
							{id : results[i].id,
							 type : results[i].getValue('custrecord_erp_ssconfig_type'),
							 rectype : results[i].getValue('custrecord_erp_ssconfig_scriptrecordtype'),
							 scriptname : results[i].getValue('custrecord_erp_ssconfig_scriptname'),
							 environment : results[i].getValue('custrecord_erp_ssconfig_environment'),
							 account : results[i].getValue('custrecord_erp_ssconfig_accountid'),
							 value : results[i].getValue('custrecord_erp_ssconfig_scriptvalue')}; 
					}		
				}

				scobject.apply(this, [objScriptConfig]);
				
			};
		}
	};
}