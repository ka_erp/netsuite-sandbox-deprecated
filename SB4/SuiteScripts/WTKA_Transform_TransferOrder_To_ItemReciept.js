/**
 *	File Name		:	WTKA_TransferOrder_To_ItemReciept.js
 *	Function		:	Suitelet to submit transfer order to transform them in to Item Reciept.
 * 	Authors			:	Manish Prajapati
 *	Company			:	Wipro Limited
 *	Release Dates	:	22-July-2016
 * 	Current Version	:	1.0
**/
var failed_TO_array = new Array();
var trasformation_failed_flag = false;

function transformTransferOrderToItemReciept(request,response)
{
	try
	{
		if ( request.getMethod() == 'GET' )
		{
			/*var contextObj= nlapiGetContext();
			nlapiLogExecution('DEBUG', 'contextObj Return' ,contextObj);

			var user_locat = contextObj.getLocation();
			nlapiLogExecution('DEBUG', 'user_location from context',user_locat);*/

			var user_location = nlapiGetLocation();
			nlapiLogExecution('DEBUG', 'user_location from api',user_location);
			
			var from_date_filter = request.getParameter("from_date_param");
			nlapiLogExecution('DEBUG', 'from_date_filter' ,from_date_filter);
			var to_date_filter = request.getParameter("to_date_param");
			nlapiLogExecution('DEBUG', 'to_date_filter parameter' ,to_date_filter);
			
			var form = nlapiCreateForm('Receive Multiple Transfer Orders');
			var alertmsg_field = form.addField('custpage_alertmsg','richtext', '').setLayoutType('outside', 'startrow').setDisplayType('inline');
			
			var from_date_fld;
			var to_date_fld;
			//var location_fld = form.addField('custpage_location','select', 'Location', 'location');
			
			/* Get data from Saved search.*/
			/*
					 
			var returncols = new Array();
			returncols[0] = new nlobjSearchColumn('internalid');
			returncols[1] = new nlobjSearchColumn('status');
			returncols[2] = new nlobjSearchColumn('tranid');//documentnumber
			returncols[3] = new nlobjSearchColumn('transferlocation');// to Location
			returncols[4] = new nlobjSearchColumn('location'); //From Location
			returncols[5] = new nlobjSearchColumn('trandate'); //Date
			*/	 
			 // Define search filters
			var filters = new Array();
			filters[0] =new nlobjSearchFilter('mainline',null,'is','T');
			//filters[1] = new nlobjSearchFilter( 'status', null, 'is', 'TrnfrOrd:F' );
			filters[1] = new nlobjSearchFilter( 'transferlocation', null, 'anyof', user_location);	
			
			if(from_date_filter !=null && from_date_filter!='' && to_date_filter !=null && to_date_filter!='')
			{
				//from_date_fld.setDefaultValue(from_date_filter); //APPLE V. 2016/10/13: Commented out
				//to_date_fld.setDefaultValue(to_date_filter); //APPLE V. 2016/10/13: Commented out
				filters.push(new nlobjSearchFilter( 'trandate', null, 'within', from_date_filter,to_date_filter));
			}
			else if((from_date_filter !=null && from_date_filter!='') && (to_date_filter ==null || to_date_filter==''))
			{
				//from_date_fld.setDefaultValue(from_date_filter); //APPLE V. 2016/10/13: Commented out
				filters.push(new nlobjSearchFilter( 'trandate', null, 'onorafter', from_date_filter));
			}
			else if((from_date_filter ==null || from_date_filter=='') && (to_date_filter !=null && to_date_filter!=''))
			{
				//to_date_fld.setDefaultValue(to_date_filter);	//APPLE V. 2016/10/13: Commented out
				filters.push(new nlobjSearchFilter('trandate',null,'onorbefore',to_date_filter));
			}
			else
			{
				//alertmsg_field.setDefaultValue('<font color="red">Please set some filter criteria(s).</font>');
			}
				
			/*if(location_filter !=null && location_filter!='')
			{
				location_fld.setDefaultValue(location_filter);
				filters.push(new nlobjSearchFilter( 'location', null, 'anyof', location_filter));	
			}
			*/	
			
			//nlapiLogExecution('DEBUG', 'filters Result Return' ,filters.length);
			
			var searchResults = nlapiSearchRecord('transferorder','customsearch_transfer_order_submission',filters, null);
			nlapiLogExecution('DEBUG', 'Search Result Return' ,JSON.stringify(searchResults));
			if(searchResults==null)
			{
				alertmsg_field.setDefaultValue('<font color="red">No Transfer Orders available to receive at your current location. </font>');
			}
			
			if(searchResults!=null)
			{
				
				from_date_fld = form.addField('custpage_fromdatefield','date', 'From Date');
				to_date_fld = form.addField('custpage_todatefield','date', 'To Date');
				
				/* START: APPLE V. 2016/10/13: Set Date Filter field values here instead of before field init */	
				if(from_date_filter) from_date_fld.setDefaultValue(from_date_filter);
				if(to_date_filter) to_date_fld.setDefaultValue(to_date_filter);
				/* END: APPLE V. 2016/10/13: Set Date Filter field values here instead of before field init*/	
				
				var columns = searchResults[0].getAllColumns();
				//create sublist to show results
				sublist = form.addSubList('custpage_sublist_id', 'list', 'Transfer Orders');
			 
				sublist.addField('custfield_selected', 'checkbox', 'Selected');
				// Add the search column names to the sublist field
				for ( var i=0; i< columns.length; i++ )
				{
					nlapiLogExecution('DEBUG', 'Column Label',columns[i].getName().toUpperCase() +' columns[i].getName()= ' +columns[i].getName());
					if(columns[i].getName()=='location')
					{
						sublist.addField(columns[i].getName() ,'select', columns[i].getLabel(),'location').setDisplayType('inline'); 
					}
					/*else if(columns[i].getName()=='statusref')
					{
						nlapiLogExecution('DEBUG', 'Column status','status::'+searchResults[0].getValue(columns[i]));
						sublist.addField(columns[i].getName() ,'select', columns[i].getLabel(),'TransactionStatus').setDisplayType('inline'); 
					}*/
					else
					{
						sublist.addField(columns[i].getName() ,'text',columns[i].getLabel()) 
					}
					nlapiLogExecution('DEBUG', 'Column Label',columns[i].getLabel() +' columns[i].getName()= ' +columns[i].getName());
				}
				sublist.setLineItemValues(searchResults);
				//additional sublist fields
				//sublist.addMarkAllButtons();
				form.setScript('customscript_wtka_supportingclientscript');
				form.addButton('custombutton_filter', 'Filter', "filterTranferOrders()");
				form.addButton('custombutton_reset', 'Reset', "resetCall()");
				form.addSubmitButton('Receive Transfer Orders');
			}
			
			response.writePage( form );
		}
		else
		{			
			nlapiLogExecution('DEBUG', 'POST call','POST call' );
			var itemsublist_count = request.getLineItemCount('custpage_sublist_id');
			nlapiLogExecution('DEBUG', 'POST call','itemsublist_count::'+itemsublist_count );
			
			var to_ir_data = new Array();
			var isTransformed=false;
			for(var i=1;i<=itemsublist_count;i++)
			{
				
				var selectCheckbox_value = request.getLineItemValue('custpage_sublist_id', 'custfield_selected',i);
				//nlapiLogExecution('DEBUG', 'POST call','selectCheckbox_value::'+selectCheckbox_value );
				if(selectCheckbox_value=='T')
				{					
					//var requited_data = new Object();
					var requited_data = new Array();
					var transferOrderId = request.getLineItemValue('custpage_sublist_id', 'internalid', i);
					var transferOrderName = request.getLineItemValue('custpage_sublist_id', 'tranid', i);
					requited_data.transfer_order =  transferOrderName
					
					transOrderUrl = nlapiResolveURL('RECORD', 'transferorder');
					requited_data.to_url =transOrderUrl+"?id="+ request.getLineItemValue('custpage_sublist_id', 'internalid', i); 
					nlapiLogExecution('DEBUG', 'POST call','transfer_order url::'+requited_data.to_url );
					
					
					var ir_id = transformTrnasferOrder(transferOrderId,transferOrderName);	
					requited_data.ir_internal_id= ir_id; 
					
					itemReceiptUrl = nlapiResolveURL('RECORD', 'itemreceipt');
					requited_data.ir_url =itemReceiptUrl+"?id="+ir_id; 
					nlapiLogExecution('DEBUG', 'POST call','item receipt url::'+requited_data.ir_url );
					
					to_ir_data.push(requited_data);
					
					isTransformed= true;			
					
				}
					
			}
			var form = nlapiCreateForm('Transform TransferOrder to Item Reciept');
			var alertmsg_field = form.addField('custpage_alertmsg','richtext', '').setLayoutType('outside', 'startrow').setDisplayType('inline');
			if (isTransformed)
			{
				if(to_ir_data.length>0)
				{
					alertmsg_field.setDefaultValue('<font color="blue">Selected Transfer Orders has been received.</font>');			
					
					//sublist to display Transformed TOs and IRs
					to_ir_sublist = form.addSubList('custpage_to_ir_sublist', 'list', ' Transformed Transfer Orders');
					
					to_ir_sublist.addField("custpage_transferorderurl", "url", "View Transfer Order" ).setLinkText( "View Transfer Order");
					to_ir_sublist.addField('custpage_transferorder' ,'text', 'Transfer Order') 			
					to_ir_sublist.addField('custpage_itemrecipt' ,'text', 'Item Receipt') 	
					to_ir_sublist.addField("custpage_itemreceipturl", "url", "View Item Receipt" ).setLinkText( "View Item Receipt");
					
					for(var k=1;k<=to_ir_data.length;k++)
					{
						if(to_ir_data[k-1].ir_internal_id !=null && to_ir_data[k-1].ir_internal_id!= undefined)
						{
							to_ir_sublist.setLineItemValue('custpage_transferorderurl',k,to_ir_data[k-1].to_url);
							to_ir_sublist.setLineItemValue('custpage_transferorder',k,to_ir_data[k-1].transfer_order);
							to_ir_sublist.setLineItemValue('custpage_itemrecipt',k,to_ir_data[k-1].ir_internal_id);
							to_ir_sublist.setLineItemValue('custpage_itemreceipturl',k,to_ir_data[k-1].ir_url);
						}
						
					}		
				}
					
				if(trasformation_failed_flag)
				{
					nlapiLogExecution('DEBUG', 'POST call','transformation Failed for some TOs' );
					var failmsg_field = form.addField('custpage_failmsg','richtext', '').setDisplayType('inline');
					failmsg_field.setDefaultValue('<font color="red">Transformation failed for some Transfer Orders. Please check failure reason on "Transformation Failed Transfer Orders" tab.</font>');		
					failed_to_sublist = form.addSubList('custpage_failed_to_sublist', 'list', ' Transformation Failed Transfer Orders');
					failed_to_sublist.addField("custpage_failedtransferorderurl", "url", "View Transfer Order" ).setLinkText( "View Transfer Order");
					failed_to_sublist.addField('custpage_failedtransferorder' ,'text', 'Transfer Order') 				
					failed_to_sublist.addField("custpage_errormsg", "text", "Failure Reason" );
					
					transOrderUrl = nlapiResolveURL('RECORD', 'transferorder');
					nlapiLogExecution('DEBUG', 'POST call','transOrderUrl url::'+transOrderUrl );
					
					for(var k=1;k<=failed_TO_array.length;k++)
					{
						//to_url_fld.setLinkText(to_ir_data[k-1].transfer_order)
						failed_TO_URL = transOrderUrl +"?id="+failed_TO_array[k-1].to_internalid;
						failed_to_sublist.setLineItemValue('custpage_failedtransferorderurl',k,failed_TO_URL);
						failed_to_sublist.setLineItemValue('custpage_failedtransferorder',k,failed_TO_array[k-1].to_name);
						failed_to_sublist.setLineItemValue('custpage_errormsg',k,failed_TO_array[k-1].error_msg);
					}		
					
				}			
			}
			else
			{
				
				alertmsg_field.setDefaultValue('<font color="red">You must select atleast one  Transfer Order to process.</font>');	
				//response.writeLine( "You must select atleast one  Transfer Order to process" );
			}
			var suite_url = nlapiResolveURL('SUITELET', 'customscript_transfer_order_to_ir', 'customdeploy_transfer_order_to_ir');
			var redirectToMainPage = "window.location.href = '"+suite_url+"'";
			form.addButton('custpage_goback_btn','Go Back',redirectToMainPage);
			response.writePage( form );
		}	
		
	}
	catch(err)
	{
		nlapiLogExecution('Debug','Error', err);
	}
}


function transformTrnasferOrder(internalId,transfer_orderName)
{
	try
	{
		//internalId="0001";
		var ir_object = nlapiTransformRecord('transferorder',internalId,'itemreceipt');
		nlapiLogExecution('DEBUG', 'StransformTrnasferOrder function','ir_object::'+ir_object );
		var itemReceiptID = nlapiSubmitRecord(ir_object);
		nlapiLogExecution('DEBUG', 'transformTrnasferOrder function','itemReceiptID::'+itemReceiptID );
		
		return itemReceiptID;
	}
	catch(err)
	{
		trasformation_failed_flag = true;
		nlapiLogExecution('Debug','Error', err.getDetails());
		var failed_to_data = new Object();
		failed_to_data.to_internalid = internalId;
		failed_to_data.to_name = transfer_orderName;
		failed_to_data.error_msg =err.getDetails();
		failed_TO_array.push(failed_to_data);
		nlapiLogExecution('Debug','Error catch', "failed data:: "+failed_TO_array);
	}
    
}