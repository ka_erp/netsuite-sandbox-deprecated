/**
 * Company            Explore Consulting
 * Copyright          2015 Explore Consulting, LLC
 * Type               NetSuite EC_Suitelet_Get_Item_Available_Qty
 * Version            1.0.0.0
 **/


// Function to get an item's field values based on the internal id of each field
function onStart(request, response)
{
    Log.d('onStart', 'Entering');

    try{
        // Retrieve parameters
        var params = request.getAllParameters();
        var internalids = params['internalid'] ? params['internalid'].split(',') : '';
        var inputLocation = params['location'] ? parseInt(params['location']) : 24;

        // Logs
        Log.d('internalids', internalids);
        Log.d('inputLocation', inputLocation);

        var returnCollection = [];

        // For each item, load the record and retrieve the location item quantity available
        _.each(internalids, function(itemId){

            var itemRecord = nsdal.loadObject('inventoryitem', itemId, []).withSublist('locations', ['location', 'quantityavailable']);

            // Iterate through all of the line level locations to find the matching location. Once a match is found we want to get the quantity avaialble
            _.each(itemRecord.locations, function(lineLocation){

                // If if the line location ID matches the input location
                if(inputLocation == lineLocation.location)
                    returnCollection.push({ itemId:itemId, quantityAvailable:lineLocation.quantityavailable, location:inputLocation});
                
            });
        });


        Log.d('returnCollection', returnCollection);
        // Write the response to
        response.write(JSON.stringify(returnCollection));
        
    }
    catch(e){
        Log.e('Unexpected Error', 'Unexpected Error occurred while processing:  ' + e);
    }
}