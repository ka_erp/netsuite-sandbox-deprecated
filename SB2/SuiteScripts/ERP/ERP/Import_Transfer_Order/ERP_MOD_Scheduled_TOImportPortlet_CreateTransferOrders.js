/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_Scheduled_TOImportPortlet_CreateTransferOrders
 * Version            1.0.0.0
 **/

var Governance_Overhead_Needs = 400;
var ScriptName = "EC_Scheduled_TOImportPortlet_CreateTransferOrders";

function onStart() {
    //EC.ProcessTransfers();
	Log.a("NEW ProcessTransfers", "START  ");
	
	var runERP = nlapiGetContext().getSetting('SCRIPT', 'custscript_erp_version');
	
	Log.a("NEW Version : Run ERP", runERP);
	if(runERP == 'T'){		
		EC.ProcessTransferserERP();
	}else{
		EC.ProcessTransfers();
	}
	  
	
}


EC.ProcessTransferserERP = function(){

    try{

        // Get TO Parent Staging Records for Processing
        var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"),
            new nlobjSearchFilter("custrecord_tranfer_stage_status", null, "anyof", [TO_HeaderStagingStatus.Approved])];
        var columns = [new nlobjSearchColumn("custrecord_tranfer_stage_status"), new nlobjSearchColumn("custrecord_expected_receipt_date"),
            new nlobjSearchColumn("custrecord_transf_stage_expe_ship_date"), new nlobjSearchColumn("custrecord_transfer_department"),
            new nlobjSearchColumn("custrecord_transfer_employee"), new nlobjSearchColumn("custrecord_transfer_from_location"),
            new nlobjSearchColumn("custrecord_transfer_reference_number"), new nlobjSearchColumn("custrecord_transfer_stage_date"),
            new nlobjSearchColumn("custrecord_transfer_subsidiary"), new nlobjSearchColumn("custrecord_transfer_to_location")];
        var results = nlapiSearchRecord("customrecord_tran_order_staging_header", null, filters, columns);
        var timesUp = false;
        var currentID;
        var reprocess = false;

        if ( results ) {
            Log.d("NEW ProcessTransfers", "Number of Search Results:  " + results.length);

            for (var i=0; i < results.length && !timesUp; i++) {
				currentID = results[i].getId();
				
                try{
					
					Log.d("NEW Processing TO Header Staging records - result #" + i, "Current Record ID:  " + currentID);
					nlapiSubmitField('customrecord_tran_order_staging_header', currentID , 'custrecord_erp_create_transfer_order', 'T'); 
										                    
                }
                catch(e)
                {
                    Log.d("ProcessTransfers - " + currentID, "Error was throwing during processing:  " + e);
                    var fields7 = ["custrecord_tranfer_stage_status", "custrecord_transfer_error_message"];
                    var values7 = [TO_HeaderStagingStatus.Error_ToBeReviewed, e];
                    nlapiSubmitField("customrecord_tran_order_staging_header", currentID, fields7, values7);

                    EC.SendEmailAlert(e, ScriptName);
                }
            }
        }
    }
    catch(e)
    {
        Log.d("ProcessTransfers", "Unexpected Error:  " + e);
        EC.SendEmailAlert(e, ScriptName);
    }
};


EC.ProcessTransfers = function(){

    try{

        // Get TO Parent Staging Records for Processing
        var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"),
            new nlobjSearchFilter("custrecord_tranfer_stage_status", null, "anyof", [TO_HeaderStagingStatus.Approved, TO_HeaderStagingStatus.CreatingTransfers])];
        var columns = [new nlobjSearchColumn("custrecord_tranfer_stage_status"), new nlobjSearchColumn("custrecord_expected_receipt_date"),
            new nlobjSearchColumn("custrecord_transf_stage_expe_ship_date"), new nlobjSearchColumn("custrecord_transfer_department"),
            new nlobjSearchColumn("custrecord_transfer_employee"), new nlobjSearchColumn("custrecord_transfer_from_location"),
            new nlobjSearchColumn("custrecord_transfer_reference_number"), new nlobjSearchColumn("custrecord_transfer_stage_date"),
            new nlobjSearchColumn("custrecord_transfer_subsidiary"), new nlobjSearchColumn("custrecord_transfer_to_location")];
        var results = nlapiSearchRecord("customrecord_tran_order_staging_header", null, filters, columns);
        var timesUp = false;
        var currentID;
        var reprocess = false;

        if ( results ) {
            Log.d("ProcessTransfers", "Number of Search Results:  " + results.length);

            for (var i=0; i < results.length && !timesUp; i++) {

                currentID = results[i].getId();
                Log.d("Processing TO Header Staging records - result #" + i, "Current Record ID:  " + currentID);
                reprocess = false;
                var detailsToUpdate = [];

                try{
                    // Process this Header TO Line Records
                    nlapiSubmitField("customrecord_tran_order_staging_header", results[i].getId(), "custrecord_tranfer_stage_status", TO_HeaderStagingStatus.CreatingTransfers);

                    // Find all Detail records for this Header record
                    var filters2 = [new nlobjSearchFilter("isinactive", null, "is", "F"),
                        new nlobjSearchFilter("custrecord_transfer_stage_header", null, "anyof", currentID),
                        new nlobjSearchFilter("custrecord_transfer_stage_line_status", null, "anyof", [TO_LineStagingStatus.ToBeProcessed, TO_LineStagingStatus.ErrorReprocess])];
                    var columns2 = [new nlobjSearchColumn("custrecord_transfer_stage_item"),
                        new nlobjSearchColumn("custrecord_transfer_quantity"),
                        new nlobjSearchColumn("custrecord_transfer_type")];

                    var details = runSearch("customrecord_transfer_order_staging_line", columns2, filters2);

                    if ( details && details.length > 0 ) {
                        Log.d("ProcessTransfers", "Number of TO Line Search Results:  " + details.length);

                        // Create a Transfer Order record for these TO Lines
                        var transferOrder = nsdal.createObject("transferorder", TransferOrderProperties)
                            .withSublist("item", TransferOrderLineProperties);

                        transferOrder.trandate = results[i].getValue('custrecord_transfer_stage_date');
                        Log.d("ProcessTransfers - Creating Transfer Order", "Date:  " + results[i].getValue('custrecord_transfer_stage_date'));
                        transferOrder.location = results[i].getValue('custrecord_transfer_from_location');
                        Log.d("ProcessTransfers - Creating Transfer Order", "From Location:  " + results[i].getValue('custrecord_transfer_from_location'));
                        transferOrder.transferlocation = results[i].getValue('custrecord_transfer_to_location');
                        Log.d("ProcessTransfers - Creating Transfer Order", "To Location:  " + results[i].getValue('custrecord_transfer_to_location'));
                        transferOrder.department = results[i].getValue('custrecord_transfer_department');
                        Log.d("ProcessTransfers - Creating Transfer Order", "Dept:  " + results[i].getValue('custrecord_transfer_department'));
                        transferOrder.employee = results[i].getValue('custrecord_transfer_employee');
                        Log.d("ProcessTransfers - Creating Transfer Order", "Employee:  " + results[i].getValue('custrecord_transfer_employee'));
                        transferOrder.subsidiary = results[i].getValue('custrecord_transfer_subsidiary');
                        Log.d("ProcessTransfers - Creating Transfer Order", "Subsidiary:  " + results[i].getValue('custrecord_transfer_subsidiary'));
                        transferOrder.memo = results[i].getValue('custrecord_transfer_reference_number');
                        Log.d("ProcessTransfers - Creating Transfer Order", "Memo:  " + results[i].getValue('custrecord_transfer_reference_number'));
                        transferOrder.custbody_ka_order_type = details[0].getValue('custrecord_transfer_type');
                        Log.d("ProcessTransfers - Creating Transfer Order", "Transfer Type:  " + details[0].getValue('custrecord_transfer_type'));

                        for (var d=0; d<details.length; d++) {

                            detailsToUpdate.push(details[d].getId());

                            // Add this line to the Transfer Order
                            var newLine = transferOrder.item.addLine();
                            newLine.item = details[d].getValue('custrecord_transfer_stage_item');
                            newLine.quantity = details[d].getValue('custrecord_transfer_quantity');
                            newLine.expectedreceiptdate = results[i].getValue('custrecord_expected_receipt_date');
                            newLine.expectedshipdate = results[i].getValue('custrecord_transf_stage_expe_ship_date');
                        }

                        transferOrder.custbody_update_pricing_from_script = true;

                        var id = transferOrder.save(true, true);
                        Log.d("ProcessTransfers", "New Transfer Order Record created:  " + id);

                        // Connect this TO Header Staging Record to the new Transfer Order
                        nlapiSubmitField("customrecord_tran_order_staging_header", results[i].getId(), "custrecord_transfer_order", id);

                        if ( id ) {
                            // Now update all TO line records to close them out
                            for (var r=0; r < detailsToUpdate.length; r++) {
                                var fields3 = ["custrecord_transfer_stage_line_status", "custrecord_transfer_stage_line_msg"];
                                var values3 = [TO_LineStagingStatus.Completed, ""];
                                nlapiSubmitField("customrecord_transfer_order_staging_line", detailsToUpdate[r], fields3, values3);
                            }
                        }
                    }
                }
                catch(e)
                {
                    Log.d("ProcessTransfers - " + currentID, "Error was throwing during processing:  " + e);
                    var fields7 = ["custrecord_tranfer_stage_status", "custrecord_transfer_error_message"];
                    var values7 = [TO_HeaderStagingStatus.Error_ToBeReviewed, e];
                    nlapiSubmitField("customrecord_tran_order_staging_header", currentID, fields7, values7);

                    EC.SendEmailAlert(e, ScriptName);
                }

                // Just got done processing this TO Header Staging Record.  Check to see if all TO Line Staging Records
                //      got updated.  If they did, then set the status accordingly.
                var finished = EC.AllRecordsComplete(currentID);
                if ( finished ) {
                    var fields4 = ["custrecord_tranfer_stage_status", "custrecord_transfer_error_message"];
                    var values4 = [TO_HeaderStagingStatus.Completed, ""];
                    nlapiSubmitField("customrecord_tran_order_staging_header", currentID, fields4, values4);
                }
                /*else if ( !finished ){
                    // DON't need to set this again - status is already set to this
                    // We ran out of governance and there are still records to process
                    nlapiSubmitField("customrecord_tran_order_staging_header", currentID, "custrecord_tranfer_stage_status", TO_HeaderStagingStatus.CreatingTransfers);
                } */

                timesUp = EC.RunTimeScriptCheckers(Script_GovernanceThreshold);
            }
        }
        else
        {
            Log.d("ProcessTransfers", "No Header Staging Records are ready for Processing");
        }


        if ( timesUp )
        {
            Log.d('Scheduling Script to continue updating');
            var result = nlapiScheduleScript('customscript_ec_sch_toimp_createtra_2', null, null);                  // 20 units
            Log.d('Scheduling Script RESULT:  ' + result);
            if (result != 'QUEUED') {
                Log.d('scheduled script not queued',
                    'expected QUEUED but scheduled script api returned ' + result);
                var emailBody5 = "Script: EC_Scheduled_InvAdjPortlet_CreateInvAdjustments.js\nFunction: ProcessTransfers\nError: There was an issue re-scheduling the scheduled script in this script";
                EC.SendEmailAlert("Error: There was an issue re-scheduling the scheduled script in this script.", ScriptName);
            }
            else {
                Log.d('ProcessTransfers -- Re-Scheduling Script Status', "Result:  " + result);
            }
        }
    }
    catch(e)
    {
        Log.d("ProcessTransfers", "Unexpected Error:  " + e);
        EC.SendEmailAlert(e, ScriptName);
    }
};

EC.CalculateGovernanceNeeds = function(numberLeftToProcess){
    var govNeed = (parseInt(numberLeftToProcess) * 2) + parseInt(Governance_Overhead_Needs);        // 2 = nlapiSubmitField to a custom record
    Log.d("CalculateGovernanceNeeds", "govNeed:  " + govNeed + "   Remaining Gov:  " + nlapiGetContext().getRemainingUsage());
    return nlapiGetContext().getRemainingUsage() < govNeed || moment().isAfter(maxEndTime);
};

EC.AllRecordsComplete = function(currentID){

    var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"),
        new nlobjSearchFilter("custrecord_transfer_stage_header", null, "anyof", currentID),
        new nlobjSearchFilter("custrecord_transfer_stage_line_status", null, "noneof", [TO_LineStagingStatus.Completed, TO_LineStagingStatus.ErrorCancel])];
    var columns = [new nlobjSearchColumn("internalid")];
    var detailLeft = nlapiSearchRecord("customrecord_transfer_order_staging_line", null, filters, columns);

    if ( detailLeft ) {
        Log.d("AllRecordsComplete", "There are still " + detailLeft.length + " TO line records to process for TO Header Record ID " + currentID);
        return false;
    }
    else return true;
};

Log.AutoLogMethodEntryExit(null, true, true, true);