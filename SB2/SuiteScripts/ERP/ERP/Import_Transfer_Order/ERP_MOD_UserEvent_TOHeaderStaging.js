	/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_UserEvent_TOHeaderStaging
 * Version            1.0.0.0
 **/


//var TO_HeaderStagingStatus = {
//    CreateLineRecords: 2,       // To Be Processed
//    Processing: 3,
//    PendingApproval: 1,         // Awaiting Approval
//    Error_ToBeReviewed: 4,
//    Error_Reprocess: 5,
//    Error_Cancel: 6,
//    Completed: 7,
//    Approved: 8,
//    CreatingTransfers: 9
//};
//
//var TO_LineStagingStatus.Completed = {
//    WaitingApproval: 6,
//    ToBeProcessed: 1,
//    InProcess: 7,
//    ErrorReprocess: 5,
//    ErrorReview: 3,
//    ErrorCancel: 4,
//    Completed: 2
//};


function onBeforeLoad(type, form, request) {

	
	var bApproveTO = nlapiGetFieldValue('custrecord_erp_approve_lines');
	var bCreateTO = nlapiGetFieldValue('custrecord_erp_create_transfer_order');  
	
	
	if(bApproveTO == 'T'){
		nlapiGetField('custrecord_erp_approve_lines').setDisplayType('disabled');
	}
	
	if(bCreateTO == 'T'){	
		nlapiGetField('custrecord_erp_create_transfer_order').setDisplayType('disabled');
	}
	
}

function onBeforeSubmit(type) {
	nlapiSetFieldValue('custrecord_transfer_error_message', "");
}




function onAfterSubmit(type) {

//    if ( nlapiGetFieldValue('custrecord_tranfer_stage_status') != TO_HeaderStagingStatus.PendingApproval ) return;
    if ( type == 'create' || type == 'edit' || type == 'xedit' ) EC.checkChildRecordsForErrors();
    if ( type == 'edit' || type == 'xedit' ) EC.validateHeaderEntry();
    
	 Log.d("Let's log stuff", "onAfterSubmit " + type);	
	 if (type == 'edit' || type == 'xedit') executeTransferOrderStaging();
//	 else return; 
    
}

function executeTransferOrderStaging(){
	
	Log.d("executeTransferOrderStaging", "executeTransferOrderStaging " +  new Date().toLocaleString()); 
	
	var recTOHeadStaging = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
	
	var bApproveTO = recTOHeadStaging.getFieldValue('custrecord_erp_approve_lines');
	var bCreateTO =recTOHeadStaging.getFieldValue('custrecord_erp_create_transfer_order');
	var toStatus = recTOHeadStaging.getFieldValue('custrecord_tranfer_stage_status');
	
	Log.d("executeTransferOrderStaging","approve:" + bApproveTO + " create:" + bCreateTO + " toStatusID:" + toStatus); 
	
	if(recTOHeadStaging){
								
//		EC.validateHeaderEntry; 
		
		var  arrErrorLineStatus  = [1, 6]; var process = false;		//,3,4,5
		toStatus = Number(toStatus); 	
		if (arrErrorLineStatus.indexOf(toStatus) >= 0){
			process = true;
		}
		
		
		if(bApproveTO == 'T' && bCreateTO == 'F' && process){
			approveChildren(recTOHeadStaging); 
		}
		
//		if(bApproveTO == 'T' && bCreateTO == 'T' && toStatus == 8){	
		var created_to = recTOHeadStaging.getFieldValue('custrecord_transfer_order');
		
		if(isNullOrEmpty(created_to)){
		if(bApproveTO == 'T' && bCreateTO == 'T'){	

			var idTO = createTransferOrder(recTOHeadStaging);
			
			if(!isNullOrEmpty(idTO)){
				
				Log.d("executeTransferOrderStaging","after TO create"); 
				Log.d("executeTransferOrderStaging idTO",idTO); 
				Log.d("executeTransferOrderStaging status", 7); 
				
//				nlapiSubmitField('customrecord_tran_order_staging_header',nlapiGetRecordId(), 'custrecord_transfer_order', idTO); 
//				nlapiSubmitField('customrecord_tran_order_staging_header',nlapiGetRecordId(), 'custrecord_tranfer_stage_status', TO_HeaderStagingStatus.Complete);
				
				recTOHeadStaging.setFieldValue('custrecord_transfer_order', idTO); // To be reviewed
				recTOHeadStaging.setFieldValue('custrecord_tranfer_stage_status', 7); // To be reviewed
				
				setLinesComplete(recTOHeadStaging);
				
				Log.a("run misc job in TO", "runTOExecution" + " idTO" + idTO +  " nlapiGetRecordId()" + nlapiGetRecordId()); 
				runTOExecution(idTO, nlapiGetRecordId());
			}
			
					
			//runTOExecuteSoap();
		 }
		}else{
			
			recTOHeadStaging.setFieldValue('custrecord_tranfer_stage_status', 7); // To be reviewed
			var success = nlapiSubmitRecord(recTOHeadStaging); 
			
			Log.a("Submitting", success);	
			Log.a("bypass creation of new TO", "method end " +  new Date().toLocaleString());
		}
//		}
		
		
		
	}
			
}


function setLinesComplete(recTOStaging){
	
	try{
		
	    Log.a("setLinesComplete", "method start " +  new Date().toLocaleString()); 
		
		var linestatus = 1; //To be Processed
		var recTOHeadStaging = recTOStaging; //nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());

		var lineCount = recTOHeadStaging.getLineItemCount('recmachcustrecord_transfer_stage_header');	
		var approveLines = recTOHeadStaging.getFieldValue('custrecord_erp_approve_lines');  
		var bLineErrorFound = false; 
			
		for (var j = 1; j <= lineCount; j++){								
			//if(approveLines == 'T'){ } //else{ linestatus = 4; }
												
			recTOHeadStaging.selectLineItem('recmachcustrecord_transfer_stage_header', j);
			recTOHeadStaging.setCurrentLineItemValue('recmachcustrecord_transfer_stage_header', 'custrecord_transfer_stage_line_status', 2);
			recTOHeadStaging.commitLineItem('recmachcustrecord_transfer_stage_header'); 		
		}	
						
		var success = nlapiSubmitRecord(recTOHeadStaging); 
		
		Log.a("Submitting", success);	
		Log.a("setLinesComplete", "method end " +  new Date().toLocaleString());
		
		
	}
	catch(e){
	        Log.d("approveChildren", "Unexpected error approveChildren:  " + e);
	}
}


function runTOExecution(idTransferOrder , idLink){
	Log.a("runTOExecution", "id: " + idTransferOrder); 
	
//	var params = [];
//	params['transactionRecord'] = idTransferOrder; 
//	params['stagingLink'] = idLink; 
	
	var b = new Array();		
	b['User-Agent-x'] = 'SuiteScript-Call';
	
	var a = nlapiResolveURL('SUITELET','customscript_erp_create_to_import_sl','customdeploy_erp_create_to_import_sl', true);  
    
//    Log.d("runTOExecution", "url: " + a); 
//    Log.d("runTOExecution", "params: " + params); 
    
    Log.a("runTOExecution", "before calling " +  new Date().toLocaleString()); 
   
    //nlapiRequestURL(a, params, b); 
    
//    var url = "https://forms.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=337&deploy=1&compid=3883338_SB3&h=44666723529f5da8324e";
    
    var params = [];
	params['transactionRecord'] = idTransferOrder; 
	params['stagingLink'] = idLink; 
	
//    nlapiSetRedirectURL(a, params);
	
	if(type == 'xedit'){
		nlapiRequestURL(a, params, b); 
	}else{
		nlapiSetRedirectURL('suitelet', 'customscript_erp_create_to_import_sl','customdeploy_erp_create_to_import_sl',null,params);
	}
	
    
	//nlapiSetRedirectURL('EXTERNAL', url + "&transactionRecord="+ idTransferOrder + "&stagingLink=" +idLink); //'customscript_erp_create_to_import_sl','customdeploy_erp_create_to_import_sl',params);
    Log.a("runTOExecution", "after calling " +  new Date().toLocaleString());
	
}


function callTransferOrderSL(){
	
//	recTOStaging
	
	try{
	var idTransId = nlapiGetRecordId(); //recTOStaging.getFieldValue('internalid');nlapiRequestURL
	Log.d("callTransferOrderSL", "id: " + idTransId); 
	
	if(idTransId){
		

//		var params = new Array();
//		params['transactionRecord']  = idTransId; 
//		
//		var url = nlapiResolveURL('SUITELET','customscript_erp_create_to_import_sl','customdeploy_erp_create_to_import_sl', true);  //+ '&transactionRecord=' + idTransId;
//		
//		url = "https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=337&deploy=1" //&transactionRecord="+idTransId; 
//		Log.d("callTransferOrderSL", "url: " + url); 
//		
//		var slResponse = nlapiRequestURL(url, params);
		
//		var slResponse = nlapiRequestURL(url);
		
//		var slResult = slResponse.getBody();
		
//		if (slResponse.getCode() == 200){
//			
//			//TODO: Process result?
//			Log.d("callTransferOrderSL", "slResult: 200 " + slResult);
//		}else{
//			Log.d("callTransferOrderSL", "slResult: " + slResult); 
//		}
		
		var params = [];
		params['transactionRecord'] = nlapiGetRecordId(); 
//		
		
//		var params = new Array();
//		params['transactionRecord']  = 4412; 
		
		var b = new Array();		
		b['User-Agent-x'] = 'SuiteScript-Call';
		
		var a = nlapiResolveURL('SUITELET','customscript_erp_create_to_import_sl','customdeploy_erp_create_to_import_sl', true);  
	    
	    Log.d("callTransferOrderSL", "url: " + a); 
	    Log.d("callTransferOrderSL", "params: " + params); 
	    
	    Log.d("callTransferOrderSL", "before calling " +  new Date().toLocaleString()); 
	   
	    nlapiRequestURL(a, params, b); 
//	    var response = nlapiRequestURL(a, params); 
//	    Log.d("callTransferOrderSL", "response" +  response);
	    
	    Log.d("callTransferOrderSL", "after calling " +  new Date().toLocaleString());

//	    Log.d("callTransferOrderSL", "url: " + ab.getBody()); 
//		var slResponse = 
	}
	}catch(e){
	        Log.d("callTransferOrderSL", "Unexpected error callTransferOrderSL:  " + e);
	}
		
}


function approveChildren(recTOStaging){
	
	
	try{
		
		Log.a("approveChildren", "method start " +  new Date().toLocaleString()); 
		
		var linestatus = 1; //To be Processed
		var recTOHeadStaging = recTOStaging; //nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
		
		
		
		var lineCount = recTOHeadStaging.getLineItemCount('recmachcustrecord_transfer_stage_header');	
		var approveLines = recTOHeadStaging.getFieldValue('custrecord_erp_approve_lines');  
		var bLineErrorFound = false; 
			
		for (var j = 1; j <= lineCount; j++){
									
			//if(approveLines == 'T'){ } //else{ linestatus = 4; }
												
			recTOHeadStaging.selectLineItem('recmachcustrecord_transfer_stage_header', j);
			
			
			var lineStat = recTOHeadStaging.getCurrentLineItemValue('recmachcustrecord_transfer_stage_header', 'custrecord_transfer_stage_line_status');
			Log.a("lineStat", lineStat);
			if(lineStat == 6){
				recTOHeadStaging.setCurrentLineItemValue('recmachcustrecord_transfer_stage_header', 'custrecord_transfer_stage_line_status', linestatus);
				recTOHeadStaging.commitLineItem('recmachcustrecord_transfer_stage_header'); 
			}			
			else if(lineStat == 3 || lineStat == 4 || lineStat == 5){
				bLineErrorFound = true;
				break;
			}									
		}
		
		if(bLineErrorFound){
//			recTOHeadStaging.setFieldValue('custrecord_transfer_error_message', 'there are line errors');
			recTOHeadStaging.setFieldValue('custrecord_tranfer_stage_status', 4); // To be reviewed
		}else{
			recTOHeadStaging.setFieldValue('custrecord_tranfer_stage_status', 8); // TO_LineStagingStatus.ToBeProcessed); 
			Log.a("recTOHeadStaging", 'status 8');
		}
						
		var success = nlapiSubmitRecord(recTOHeadStaging); 
		
		Log.a("Submitting", success);
		
		Log.a("approveChildren", "method end " +  new Date().toLocaleString()); 
	}
	catch(e){
	        Log.d("approveChildren", "Unexpected error approveChildren:  " + e);
	}
	
} 


function getChildRecordErrors(){
	
	var objChildError = new Object; 
	
	var lineCount = nlapiGetLineItemCount('recmachcustrecord_transfer_stage_header');  	
	var  arrErrorLineStatus  = [3,4,5]; //arrErrorLineStatus.indexOf(lineStatus);
	
	
	objChildError.error = false; 
	objChildError.errMessage = ""; 
	
	if(lineCount <= 0){						
		objChildError.error = true;	
		objChildError.errMessage = "Line Error: \n  There are no lines for this record."; 
	}
	
		
	for (var j = 1; j <= lineCount; j++){
				
		var lineStatus = nlapiGetLineItemValue('recmachcustrecord_transfer_stage_header', 'custrecord_transfer_stage_line_status', j);
		var fileNumber = nlapiGetLineItemValue('recmachcustrecord_transfer_stage_header', 'custrecord_file_line_number', j);
		var lineErrorMessage = nlapiGetLineItemValue('recmachcustrecord_transfer_stage_header', 'custrecord_transfer_stage_line_msg', j);
		
		
		
		lineStatus = Number(lineStatus); 	
		
		
		
		if (arrErrorLineStatus.indexOf(lineStatus) >= 0){
			objChildError.error = true;
			objChildError.errMessage = "Line Error: \n One or more lines has an error. Please review" + 
										"\n File Line: " + fileNumber  + 
										"\n Error: " + lineErrorMessage;
			
			Log.d("setChildRecordErrors", lineStatus ); 
			break;			
		}
	}
		
	return objChildError; 	
} 

function isNullOrEmpty(valueStr){
	//TODO
//	Log.d("isNullOrEmpty enter", valueStr ); 
//	if(valueStr == null){Log.d("isNullOrEmpty valueStr", "null" );}
//	if(valueStr == ""){Log.d("isNullOrEmpty valueStr", "quotes" );}
//	if(valueStr == undefined){Log.d("isNullOrEmpty valueStr", "undefined" );}
//	if(isNaN(valueStr)){Log.d("isNullOrEmpty valueStr", "nan" );}
	
    return(valueStr == null || valueStr == "" || valueStr == undefined);
}


function isNullOrEmptyDate(valueStr){
	return(valueStr == null || valueStr == "" || valueStr == undefined || isNaN(valueStr));
}

function getRecordErrors(){
	
	
	try
    {
		
		Log.d("getRecordErrors ", "record Id" + nlapiGetRecordId());
		
		
		var objParentError = new Object; 
		
		objParentError.error = false; 
		objParentError.errMessage = ""; 
       
		var transfer_reference_number 		= nlapiGetFieldValue('custrecord_transfer_reference_number');
		var transfer_stage_date 			= nlapiGetFieldValue('custrecord_transfer_stage_date');
		var transfer_subsidiary 			= nlapiGetFieldValue('custrecord_transfer_subsidiary');
		var transfer_from_location 			= nlapiGetFieldValue('custrecord_transfer_from_location');
		var transfer_to_location	 		= nlapiGetFieldValue('custrecord_transfer_to_location');
		var transf_stage_expe_ship_date 	= nlapiGetFieldValue('custrecord_transf_stage_expe_ship_date');
		var expected_receipt_date 			= nlapiGetFieldValue('custrecord_expected_receipt_date');		
		
//		var transfer_reference_number 		= nlapiGetFieldText('custrecord_transfer_reference_number');
//		var transfer_stage_date 			= nlapiGetFieldText('custrecord_transfer_stage_date');
//		var transfer_subsidiary 			= nlapiGetFieldText('custrecord_transfer_subsidiary');
//		var transfer_from_location 			= nlapiGetFieldText('custrecord_transfer_from_location');
//		var transfer_to_location	 		= nlapiGetFieldText('custrecord_transfer_to_location');
//		var transf_stage_expe_ship_date 	= nlapiGetFieldText('custrecord_transf_stage_expe_ship_date');
//		var expected_receipt_date 			= nlapiGetFieldText('custrecord_expected_receipt_date');		
		
		
		Log.d("getRecordErrors ", "transfer_reference_n" + transfer_reference_number); 	
		Log.d("getRecordErrors ", "transfer_stage_date " + transfer_stage_date); 		
		Log.d("getRecordErrors ", "transfer_subsidiary " + transfer_subsidiary );		
		Log.d("getRecordErrors ", "transfer_from_locati" + transfer_from_location );		 
		Log.d("getRecordErrors ", "transfer_to_location" + transfer_to_location	 	); 
		Log.d("getRecordErrors ", "transf_stage_expe_sh" + transf_stage_expe_ship_date ); 
		Log.d("getRecordErrors ", "expected_receipt_dat" + expected_receipt_date 	);	
		
			var errorMsg = ""; 
			
			if ( isNullOrEmpty(transfer_reference_number)){
                errorMsg += "No Transfer Reference Number found.  ";
			}
            if ( isNullOrEmpty(transfer_stage_date)){
                errorMsg += "No Transfer Stage Date found.  ";
			}
            if ( isNullOrEmpty(transfer_subsidiary)){
                errorMsg += "Invalid Subsidiary id.  ";
			}
            if ( isNullOrEmpty(transfer_from_location)){
                errorMsg += "Invalid From Location external id.  ";
			}
            if ( isNullOrEmpty(transfer_to_location)){
                errorMsg += "Invalid To Location external id.  ";
			}
            if ( isNullOrEmpty(transf_stage_expe_ship_date)){
                errorMsg += "No Expected Ship Date found.  ";
			}
            if ( isNullOrEmpty(expected_receipt_date)){
                errorMsg += "No Expected Receipt Date found.  ";
			}	

            if(errorMsg != "" ){
            	objParentError.error = true;
            	objParentError.errMessage = errorMsg; 

            }
            
            
           
//                nlapiGetFieldValue('custrecord_tranfer_stage_status = TO_HeaderStagingStatus.Error_ToBeReviewed;
//                nlapiGetFieldValue('custrecord_transfer_error_message = errorMsg;
           

//            var id = nlapiGetFieldValue('save(true, true);
//            Log.d("validateHeaderEntry", "Re-saved record:  " + id);
            
            setRecordError(objParentError)
    }
    catch(e){
        Log.d("validateHeaderEntry", "Unexpected error while validating entry:  " + e);
    }
	

	
	
	
}

function setRecordError(objChildError){
	
	Log.d("setRecordError", JSON.stringify(objChildError) ); 
	
	
	var oldMessage = nlapiGetFieldValue('custrecord_transfer_error_message'); 
	
	nlapiSetFieldValue('custrecord_transfer_error_message', oldMessage + "\n \n " + objChildError.errMessage);
		
}



EC.checkChildRecordsForErrors = function(){
    try
    {
        var rec = nsdal.loadObject(nlapiGetRecordType(), nlapiGetRecordId(), HeaderStagingRecordProperties);

        if ( !rec.custrecord_transfer_error_message )
            var errorMsg = "";
        else if ( rec.custrecord_transfer_error_message && rec.custrecord_transfer_error_message == "There are no TO Line Staging records found at all for this TO Header Record.  Please review." )
            var errorMsg = "";
        else
            var errorMsg = rec.custrecord_transfer_error_message;

        if ( !EC.AnyLineRecordsFound(rec.id) ){
            errorMsg += "There are no TO Line Staging records found at all for this TO Header Record.  Please review.  ";
            Log.d("AnyLineRecordsFound - No TO Line Staging records found.", msg);

            rec.custrecord_tranfer_stage_status = TO_HeaderStagingStatus.Error_ToBeReviewed;
            rec.custrecord_transfer_error_message = errorMsg;
            var id2 = rec.save(true, true);
            Log.d("checkChildRecordsForErrors", "Re-saved record:  " + id2);
        }
        else
        {
            // Since we know there are now line records connected, remove the error
            if ( rec.custrecord_transfer_error_message ) {
                var newmsg = rec.custrecord_transfer_error_message.replace("There are no TO Line Staging records found at all for this TO Header Record.  Please review.", "");
                rec.custrecord_transfer_error_message = newmsg;
                rec.custrecord_tranfer_stage_status = TO_HeaderStagingStatus.PendingApproval;
            }

            var msg = EC.AllRecordsComplete(rec.id);

            if ( msg ) {
                if (errorMsg)
                    errorMsg += "  " + msg;
                else
                    errorMsg = msg;
            }

            if ( errorMsg != "" )
            {
                rec.custrecord_tranfer_stage_status = TO_HeaderStagingStatus.Error_ToBeReviewed;
                rec.custrecord_transfer_error_message = errorMsg;
            }

            var id = rec.save(true, true);
            Log.d("checkChildRecordsForErrors", "Re-saved record:  " + id);
        }

    }
    catch(e){
        Log.d("checkChildRecordsForErrors", "Unexpected error while checking child records:  " + e);
    }
};




function createTransferOrder(recTOHeadStaging){
	
	
	try{
		
		createTransferOrder = recTOHeadStaging.getFieldValue('custrecord_erp_create_transfer_order'); 
		
		
		if(createTransferOrder == 'T'){
		var id = "";
		
		if(recTOHeadStaging){
		
		
			   var transferOrder = nsdal.createObject("transferorder", TransferOrderProperties)
			   .withSublist("item", TransferOrderLineProperties);
	
			   transferOrder.externalid = nlapiGetRecordId();   																		//20151217 add externalid
			   transferOrder.trandate = recTOHeadStaging.getFieldValue('custrecord_transfer_stage_date');   
			   transferOrder.location = recTOHeadStaging.getFieldValue('custrecord_transfer_from_location');  
			   transferOrder.transferlocation = recTOHeadStaging.getFieldValue('custrecord_transfer_to_location');
			   transferOrder.department = recTOHeadStaging.getFieldValue('custrecord_transfer_department');      
			   transferOrder.employee =   nlapiGetUser(); 																				// 12172015 recTOHeadStaging.getFieldValue('custrecord_transfer_employee');      
			   transferOrder.subsidiary =  recTOHeadStaging.getFieldValue('custrecord_transfer_subsidiary');   
			   transferOrder.memo =  recTOHeadStaging.getFieldValue('custrecord_transfer_reference_number');   
transferOrder.orderstatus =  "A"; //02162016 
			  
			   //transferOrder.custbody_ka_order_type =  recTOHeadStaging.getFieldValue('custrecord_transfer_type');
			   
			   var rec_date = recTOHeadStaging.getFieldValue('custrecord_expected_receipt_date');
			   var ship_date =  recTOHeadStaging.getFieldValue('custrecord_transf_stage_expe_ship_date');
			   
				transferOrder.custbody_ka_to_expected_ship_date = ship_date;
				transferOrder.custbody_ka_to_expected_recv_date = rec_date;

			   
	
			   Log.d("ProcessTransfers - Creating Transfer Order", "Date:  " + recTOHeadStaging.getFieldValue('custrecord_transfer_stage_date'));
				   				   	
				   var lineCount = recTOHeadStaging.getLineItemCount('recmachcustrecord_transfer_stage_header');	
				   Log.d("ProcessTransfers - Line", "lineCount:  " + lineCount);
				   var approveLines = recTOHeadStaging.getFieldValue('custrecord_erp_approve_lines');  
						
					for (var k = 1; k <= lineCount; k++){
																						 
						  recTOHeadStaging.selectLineItem('recmachcustrecord_transfer_stage_header', k);  				
						  var newLine = transferOrder.item.addLine();					  					   					  
						  				  
					      newLine.item = recTOHeadStaging.getCurrentLineItemValue('recmachcustrecord_transfer_stage_header', 'custrecord_transfer_stage_item');
					      newLine.quantity = recTOHeadStaging.getCurrentLineItemValue('recmachcustrecord_transfer_stage_header', 'custrecord_transfer_quantity');
//					      newLine.expectedreceiptdate = rec_date;
					      newLine.expectedshipdate = ship_date;
					      					      
					}
					
					//check
					if(!isNullOrEmpty(lineCount)){
						recTOHeadStaging.selectLineItem('recmachcustrecord_transfer_stage_header', 1);
						transferOrder.custbody_ka_order_type = recTOHeadStaging.getCurrentLineItemValue('recmachcustrecord_transfer_stage_header', 'custrecord_transfer_type'); //allocation																		
						//recTOHeadStaging.getCurrentLineItemValue('recmachcustrecord_transfer_stage_header', 'custrecord_transfer_stage_item'); //expe shipdate
					}									
		
					 Log.d("ProcessTransfers - Line", "JSON transferOrder:  " + JSON.stringify(transferOrder));
					
					 id = transferOrder.save(true, false); 																				//20151217 change 2nd paramter to false
					
				   if(id){
					   return id;
				   }else{
					   return null; 
				   }
				   
			}else{
				return null; 
			}
		}
	}catch(e){
        Log.d("createTransferOrder", "Unexpected error createTransferOrder:  " + e);
        
        recTOHeadStaging.setFieldValue('custrecord_transfer_error_message', e); // To be reviewed
		recTOHeadStaging.setFieldValue('custrecord_tranfer_stage_status', TO_HeaderStagingStatus.Error_ToBeReviewed); // To be reviewed
		
		nlapiSubmitRecord(recTOHeadStaging); 
	}

	
}


EC.validateHeaderEntry = function(){
    try
    {
        var rec = nsdal.loadObject(nlapiGetRecordType(), nlapiGetRecordId(), HeaderStagingRecordProperties);
        var errorMsg = rec.custrecord_transfer_error_message || "";

    	//TODO
        
        Log.d("validateHeaderEntry", "custrecord_transfer_reference_number:  " + rec.custrecord_transfer_reference_number);
        Log.d("validateHeaderEntry", "custrecord_transfer_stage_date:  " + rec.custrecord_transfer_stage_date);
        Log.d("validateHeaderEntry", "custrecord_transfer_subsidiary:  " + rec.custrecord_transfer_subsidiary);
        Log.d("validateHeaderEntry", "custrecord_transfer_from_location:  " + rec.custrecord_transfer_from_location);
        Log.d("validateHeaderEntry", "custrecord_transfer_to_location:  " + rec.custrecord_transfer_to_location);
        Log.d("validateHeaderEntry", "custrecord_transf_stage_expe_ship_date:  " + rec.custrecord_transf_stage_expe_ship_date);
        Log.d("validateHeaderEntry", "custrecord_expected_receipt_date:  " + rec.custrecord_expected_receipt_date);
        

		if ( isNullOrEmptyDate(rec.custrecord_transfer_stage_date ))
           {errorMsg += "No Transfer Stage Date found.  ";}

        if ( isNullOrEmpty(rec.custrecord_transfer_reference_number ))
            {errorMsg += "No Transfer Reference Number found.  ";}        

        if ( isNullOrEmpty(rec.custrecord_transfer_subsidiary ))
            {errorMsg += "Invalid Subsidiary id.  ";}

        if ( isNullOrEmpty(rec.custrecord_transfer_from_location ))
            {errorMsg += "Invalid From Location external id.  ";}

        if ( isNullOrEmpty(rec.custrecord_transfer_to_location ))
            {errorMsg += "Invalid To Location external id.  ";}

        if ( isNullOrEmptyDate(rec.custrecord_transf_stage_expe_ship_date ))
            {errorMsg += "No Expected Ship Date found.  ";}

        if ( isNullOrEmptyDate(rec.custrecord_expected_receipt_date ))
            {errorMsg += "No Expected Receipt Date found.  ";}

        Log.d("validateHeaderEntry", "errorMsg before we update the record:  " + errorMsg);
        

        if ( errorMsg != "" )
        {
            rec.custrecord_tranfer_stage_status = TO_HeaderStagingStatus.Error_ToBeReviewed;
            rec.custrecord_transfer_error_message = errorMsg;
        }else{
        	rec.custrecord_tranfer_stage_status = TO_HeaderStagingStatus.PendingApproval;
        	
        }

        var id = rec.save(true, true);
        Log.d("validateHeaderEntry", "Re-saved record:  " + id);
    }
    catch(e){
        Log.d("validateHeaderEntry", "Unexpected error while validating entry:  " + e);
    }
};

EC.AllRecordsComplete = function(currentID){

    var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"),
        new nlobjSearchFilter("custrecord_transfer_stage_header", null, "anyof", currentID),
        new nlobjSearchFilter("custrecord_transfer_stage_line_status", null, "anyof", [TO_LineStagingStatus.ErrorReview])];
    var columns = [new nlobjSearchColumn("internalid"), new nlobjSearchColumn("custrecord_transfer_stage_line_msg")];
    var detailLeft = nlapiSearchRecord("customrecord_transfer_order_staging_line", null, filters, columns);

    if ( detailLeft ) {
        var msg = "There are still " + detailLeft.length + " TO line records to review.  Details:\n";
        for ( var d=0; d<detailLeft.length; d++ ){
            msg += detailLeft[d].getId() + " (" + detailLeft[d].getValue("custrecord_transfer_stage_line_msg") + ")   ";
        }

        Log.d("AllRecordsComplete - Details from Line records that aren't yet complete.", msg);
        return msg;
    }
    else return undefined;
};

EC.AnyLineRecordsFound = function(currentID){

    var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"),
        new nlobjSearchFilter("custrecord_transfer_stage_header", null, "anyof", currentID)];
    var columns = [new nlobjSearchColumn("internalid")];
    var detailLeft = nlapiSearchRecord("customrecord_transfer_order_staging_line", null, filters, columns);

    if ( detailLeft && detailLeft.length > 0 )
        return true;
    else
        return false;
};


