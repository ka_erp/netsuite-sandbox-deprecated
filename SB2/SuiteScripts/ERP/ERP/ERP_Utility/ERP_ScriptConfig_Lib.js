/**
 *	File Name		:	ERP_ScriptConfig_Lib.js
 *	Function		:	Script Config utility
 *	Authors			:	A.Villanueva
 *	Release Dates	:	08-Jun-2016
 * 	Current Version	:	1.0
**/

function ScriptConfig(){
	
	var objContext = nlapiGetContext();
	
	this.stEnviron = objContext.getEnvironment();
	this.stAccountID = objContext.getCompany();
	this.stScriptId = objContext.getScriptId();
	this.objScriptConfig = {};

	//Fetch Script Config records data
	var arFilters = [new nlobjSearchFilter('isinactive', null, 'is', 'F')];
	
	var arColumns = [new nlobjSearchColumn('internalid').setSort(true),	//Use latest record
					 new nlobjSearchColumn('name'),
					 new nlobjSearchColumn('custrecord_erp_ssconfig_type'),
					 new nlobjSearchColumn('custrecord_erp_ssconfig_scriptrecordtype'),
					 new nlobjSearchColumn('custrecord_erp_ssconfig_scriptname'),
					 new nlobjSearchColumn('custrecord_erp_ssconfig_environment'),
					 new nlobjSearchColumn('custrecord_erp_ssconfig_accountid'),
					 new nlobjSearchColumn('custrecord_erp_ssconfig_scriptvalue')]; 
	
	var search = nlapiCreateSearch('customrecord_erp_script_configuration', arFilters, arColumns);
	var resultSet = search.runSearch();	
	var arResults = null;
	if(resultSet){
		arResults = getAllSearchResults(resultSet);
	}
	
	//Store results as object
	var key;
	if(!isEmpty(arResults)){		
		for(var i = 0; i < arResults.length; i++){			
			key = arResults[i].getValue('name').toUpperCase();
			
			//Environment must match if Environment is specified
			if(!isEmpty(arResults[i].getValue('custrecord_erp_ssconfig_environment'))
				&& arResults[i].getValue('custrecord_erp_ssconfig_environment').toUpperCase() != this.stEnviron.toUpperCase()){
				continue;
			}
			
			//Account ID must match if Account ID is specified
			if(!isEmpty(arResults[i].getValue('custrecord_erp_ssconfig_accountid'))
				&& arResults[i].getValue('custrecord_erp_ssconfig_accountid').toUpperCase() != this.stAccountID.toUpperCase()){
				continue;
			}
			
			//Script ID must match if Script ID is specified
			if(!isEmpty(arResults[i].getValue('custrecord_erp_ssconfig_scriptname'))
				&& arResults[i].getValue('custrecord_erp_ssconfig_scriptname').toUpperCase() != this.stScriptId.toUpperCase()){
				continue;
			}
			
			//Do not replace existing data
			if(this.objScriptConfig[key]){
				continue;
			}
			
			this.objScriptConfig[key] = 
				{id : arResults[i].getValue('internalid'),
				 type : arResults[i].getValue('custrecord_erp_ssconfig_type'),
				 rectype : arResults[i].getValue('custrecord_erp_ssconfig_scriptrecordtype'),
				 scriptname : arResults[i].getValue('custrecord_erp_ssconfig_scriptname'),
				 environment : arResults[i].getValue('custrecord_erp_ssconfig_environment'),
				 account : arResults[i].getValue('custrecord_erp_ssconfig_accountid'),
				 value : arResults[i].getValue('custrecord_erp_ssconfig_scriptvalue')}; 
		}		
	}
}

ScriptConfig.prototype = {

	//Returns Value given the Name of Script Config
	getScriptConfigValue : function(stName){

		var stValue = null;
	
		if(!isEmpty(stName)){
			if(this.objScriptConfig[stName.toUpperCase()]){
				
				stValue = this.objScriptConfig[stName.toUpperCase()].value;
			}
		}
		
		nlapiLogExecution('debug', 'getScriptConfigValue', stName + ' = ' + stValue);
		return stValue;
	}
};

/******************
ACTUAL Script Config object
Use this object in your script
*******************/
var SCRIPTCONFIG = new ScriptConfig();