/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       08 Oct 2015     ivan.sioson
 *
 */

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function erp_UpdateUtility_scheduled(type) {

	
	var context = nlapiGetContext();
    var searchID = context.getSetting("SCRIPT", "custscript_util_update_search");
    var tranType = context.getSetting("SCRIPT", "custscript_util_update_tran_type");
    var toUpdate = context.getSetting("SCRIPT", "custscript_util_update_field_toupdate");
    
    nlapiLogExecution("DEBUG", "func: searchID", searchID);
    nlapiLogExecution("DEBUG", "func: tranType", tranType);
    nlapiLogExecution("DEBUG", "func: toUpdate", toUpdate);
	
		
	var strSavedSearchIDVeLuCu = null;
	var arSaveSearchFiltersVeLuCu = new Array();
	var arSavedSearchColumnsVeLuCu = new Array();
	var arSavedSearchResultsVeLuCu = null; 
//	arSaveSearchFiltersVeLuCu.push(new nlobjSearchFilter( 'custrecord_ka_veculo_location', null, 'anyof', idLocation));

//	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('internalid').setSort());
//	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('name'));
//	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('custrecord_ka_veculo_vendor'));
//	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('custrecord_ka_veculo_location'));
//	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('custrecord_ka_veculo_customer'));
//
	var arSavedSearchResultsVeLuCu = nlapiLoadSearch(tranType, searchID);	
//	
//	arSavedSearchResultsVeLuCu.addColumns(arSavedSearchColumnsVeLuCu);
//	arSavedSearchResultsVeLuCu.addFilters(arSaveSearchFiltersVeLuCu);
	
	var resultSet = arSavedSearchResultsVeLuCu.runSearch();
	
	if(resultSet){
		
		var results = resultSet.getResults(0,1000);
				
		for(var i = 0; i < results.length; i++){

			try{
				
				
//				if(false){
//					
//		//				var idCurrentItem = results[i].getValue('internalid');
//						var columns = results[i].getAllColumns();
//						var objResult = results[i];
//						
//		//				nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap : 4 ",objResult.getValue(columns[4]));  
//		//				nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap : 1 ",objResult.getValue(columns[1]));
//						
//		//				var objVendor = new Object();
//		//				objVendor.markup = objResult.getValue(columns[4]);
//		//				objVendor.vendor = objResult.getValue(columns[1]);
//		//				objVendor.vendorname = objResult.getText(columns[1]);
//		//				objVendor.currency = objResult.getValue(columns[5]);
//		//				objVendor.currencyTxt = objResult.getText(columns[5]);
//						
//						var idCurrentItem = objResult.getValue(columns[0]);
//						var memo =  objResult.getValue(columns[1]);
//						
//						nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap : tranType ",tranType);
//						nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap : idCurrentItem ",idCurrentItem);
//						nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap : toUpdate ", toUpdate);
//						nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap : memo ",memo);
//						
//						if(toUpdate){
//							nlapiSubmitField(tranType, idCurrentItem, toUpdate, memo); 
//						}
//		//				arrVendorsObj.push(objVendor);
//		//				arrVendorIds.push(objVendor.vendor);
//				
//				}
				
				
				if(true){
					var columns = results[i].getAllColumns();
					var objResult = results[i];
					
					var idCurrentItem = objResult.getValue(columns[0]);					
					var objRecord = nlapiLoadRecord(tranType, idCurrentItem);
					
					nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap : tranType ",tranType);
					nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap : idCurrentItem ",idCurrentItem);
															
					
					for (var j = 1; j <= objRecord.getLineItemCount('item'); j++) {
						objRecord.selectLineItem('item',j);
						var itemid = objRecord.getCurrentLineItemValue('item', 'item');						 						
						var b = nlapiLookupField('item', itemid, 'custitem3');  												
						objRecord.setCurrentLineItemValue('item', 'custcol_item_color', b);
						objRecord.commitLineItem('item'); 
					 }
					
					var transactionprocessed = nlapiSubmitRecord(objRecord);
					nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap : record ",transactionprocessed);
					
					
				}
				
											
			}catch(ex){
				
				nlapiLogExecution("DEBUG", "func: ex ",ex.toString());
			}		
		}
	}		
}
