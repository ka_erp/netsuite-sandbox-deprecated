/**
 *	File Name		:	ERP_Kit_and_Ace_Functions_LIB.js
 *	Function		:	
 * 	Remarks			:	A library script to hold functions that are commonly used in Kit and Ace 's Netsuite development
 *	Prepared by		:	Christopher Neal
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

/*
*	name: isEmpty
*	descr:  determine whethere a variable has a value set
*	
*   author: christopher.neal@kitandace.com
*	date: 14 June 2016
*	@param: {obj} val - any type of input variable
*	@return: {boolean} true: if val is set, false: if val is not set
*	
*/
function isEmpty(val){
    return (val === undefined || val == null || val.length <= 0) ? true : false;
}

/*
*	name: getAllSearchResults
*	descr:  returns complete search results
*	
*   author: apple.villanueva@kitandace.com
*	date: 15 June 2016
*	@param: {nlobjSearchResultSet} searchResults
*	@return: {array} - contains complete search results
*	
*/
function getAllSearchResults(searchResults){
	 
	 //complete results
	 var completeResultSet = new Array(); 
	 
	 // resultIndex points to record starting current resultSet in the entire results array 
	 var resultIndex = 0; 
	 var resultStep = 1000; // Number of records returned in one step (maximum is 1000)
	 var resultSet; // temporary variable used to store the result set
	 
	 do {
	     // fetch one result set
	     resultSet = searchResults.getResults(resultIndex, resultIndex + resultStep);

	     // increase pointer
	     resultIndex = resultIndex + resultStep;
	     
	     //store to complete results set
	     completeResultSet = completeResultSet.concat(resultSet);
	     
	 // once no records are returned we already got all of them
	 } while (resultSet.length > 0) 	
		 
	 return completeResultSet;
}

/*
*	name: getItemSKUCodes
*	descr:  Returns an object of Item SKU Codes
*	
*   author: apple.villanueva@kitandace.com
*	date: 10 Aug 2016
*	@param: {array} arItems - array of item internal ids
*	@return: {object} 		- KEY = item internal id, VALUE = SKU Code
*	
*/
function getItemSKUCodes(arItems){
	
	var arResults = nlapiSearchRecord('inventoryitem', null,
					[new nlobjSearchFilter('internalid', null, 'anyof', arItems)],
					[new nlobjSearchColumn('internalid'),
					 new nlobjSearchColumn('itemid')]); //returns the full SKU code (e.g. 'KM031052S : KM031052-10022-M')
	
	var objItemSKUCodes = {};
	if(arResults){
		for(var i = 0; i < arResults.length; i++){
			objItemSKUCodes[arResults[i].getValue('internalid')] = cleanSKUCode(arResults[i].getValue('itemid'));
		}		
	}				 
	
	return objItemSKUCodes;
}

/*
*	name: cleanSKUCode
*	descr:  Returns Item SKU Code without hierarchy
*	
*   author: apple.villanueva@kitandace.com
*	date: 10 Aug 2016
*	@param: {string} itemFullSkuCode	- Item SKU Code with hierarchy
*	@return: {string} 					- Item SKU Code without hierarchy
*	
*/
function cleanSKUCode(itemFullSkuCode){
	
	var itemSkuCode = itemFullSkuCode.split(':')[1]; //perform split and get after the colon (e.g. ' KM031052-10022-M')

	if (itemSkuCode[0] == ' '){
		itemSkuCode = itemSkuCode.substring(1); //remove 1st character from the string, it is a whitespace (e.g. 'KM031052-10022-M')					
	}
	
	return itemSkuCode;
}
