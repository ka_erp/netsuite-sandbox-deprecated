/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       06 Oct 2015     casual
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function preventBackOrder_AS(type) {

	nlapiLogExecution("DEBUG", "Count", count);

	if (type != 'delete') {

		var transactionObj = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());

		var status = transactionObj.getFieldValue('orderstatus');
		nlapiLogExecution("DEBUG", "orderstatus", status);

		if (status == "B") {

			var count = transactionObj.getLineItemCount('item'); 	nlapiLogExecution("DEBUG", "Count", count);
			
			//CHECK IF ZERO AND REMOVE IT
			

			for (var i = 1; i <= count; i++) {

				var committed = transactionObj.getLineItemValue('item','quantitycommitted', i);
				
				nlapiLogExecution("DEBUG", "Count " + i, committed);

				if (committed == 0) {
					nlapiLogExecution("DEBUG", "Count " + i, 'removing this');
					transactionObj.removeLineItem('item', i);
					i--;
					count--;
				}

			}

			nlapiSubmitRecord(transactionObj);

		}

	}

}
