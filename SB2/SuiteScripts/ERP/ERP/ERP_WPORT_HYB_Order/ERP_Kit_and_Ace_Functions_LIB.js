/**
 *	File Name		:	ERP_Kit_and_Ace_Functions_LIB.js
 *	Function		:	
 * 	Remarks			:	A library script to hold functions that are commonly used in Kit and Ace 's Netsuite development
 *	Prepared by		:	Christopher Neal
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

/*
*	name: isEmpty
*	descr:  determine whethere a variable has a value set
*	
*   author: christopher.neal@kitandace.com
*	date: 14 June 2016
*	@param: {obj} val - any type of input variable
*	@return: {boolean} true: if val is set, false: if val is not set
*	
*/
function isEmpty(val){
    return (val === undefined || val == null || val.length <= 0) ? true : false;
}

/*
*	name: getAllSearchResults
*	descr:  returns complete search results
*	
*   author: apple.villanueva@kitandace.com
*	date: 15 June 2016
*	@param: {nlobjSearchResultSet} searchResults
*	@return: {array} - contains complete search results
*	
*/
function getAllSearchResults(searchResults){
	 
	 //complete results
	 var completeResultSet = new Array(); 
	 
	 // resultIndex points to record starting current resultSet in the entire results array 
	 var resultIndex = 0; 
	 var resultStep = 1000; // Number of records returned in one step (maximum is 1000)
	 var resultSet; // temporary variable used to store the result set
	 
	 do {
	     // fetch one result set
	     resultSet = searchResults.getResults(resultIndex, resultIndex + resultStep);

	     // increase pointer
	     resultIndex = resultIndex + resultStep;
	     
	     //store to complete results set
	     completeResultSet = completeResultSet.concat(resultSet);
	     
	 // once no records are returned we already got all of them
	 } while (resultSet.length > 0) 	
		 
	 return completeResultSet;
}



