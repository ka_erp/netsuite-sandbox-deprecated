/**
 * Appeasement form automation - suitelet companion
 *
 * Version    Date            Author           Remarks
 * 1.00       29 Jul 2016     apple.villanueva
 *
 */
 
 /**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
 function suitelet_Appeasement(request, response){
	 
	if(request.getMethod() == 'GET'){
		
		//Get Script Config for Appeasement
		if(request.getParameter('act') == 'scriptconfig'){
			
			var objScriptConfig = {
				'form' : SCRIPTCONFIG.getScriptConfigValue('Appeasement: Form'),
				'location' : SCRIPTCONFIG.getScriptConfigValue('Appeasement: Locations'),
				'account' : SCRIPTCONFIG.getScriptConfigValue('Appeasement: Account')
			};
			
			response.write(JSON.stringify(objScriptConfig));
		}		
	}	 
 }