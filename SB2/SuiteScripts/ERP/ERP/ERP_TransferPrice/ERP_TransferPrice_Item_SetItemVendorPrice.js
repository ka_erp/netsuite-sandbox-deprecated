
function execute_ItemVendor_Price_AS(){

	var objVendors = getVendorLocCustomerMap();
	
	createVendors_as(objVendors);
	
	
}

function execute_ItemVendor_Price(){


	
	var objVendors = getVendorLocCustomerMap();
	
	var itemvendorcount= nlapiGetLineItemCount('itemvendor'); 
	
	
//	for(var j = 1 ; j <= itemvendorcount; j++){
//		
//		nlapiRemoveLineItem('itemvendor', 1);
//	}
//	
	
	if(!isNullOrEmpty(objVendors)){
		
		
		//Delete First
		if(itemvendorcount > 0){		
			var arrVendorIds = objVendors.ids;
			var foundAny = false; 
			
			for(var b = 0; b < arrVendorIds.length; b++){
				
				var thisVendor = arrVendorIds[b];
				var found = nlapiFindLineItemValue('itemvendor', 'vendor', thisVendor);
				if(found > -1){
					foundAny = true; 
					break;
				}
			}
			
			nlapiLogExecution('DEBUG', 'Finding Vendors foundAny', foundAny); 
			
			
			if(foundAny){ //remove if not there
				
				for(var c = 0; c < arrVendorIds.length; c++){
				
					var thisVendor = arrVendorIds[c];  					
					var found = nlapiFindLineItemValue('itemvendor', 'vendor', thisVendor);
					if(found > -1){
					nlapiRemoveLineItem('itemvendor', found);
					}
					
				}
				nlapiSetFieldValue('isdropshipitem', 'F');
				nlapiSetFieldValue('custitem_erp_vendorprice_reset', 'T');
				
				
				
			}else{ //create now
				
				//createVendors(objVendors);
				
			}
			
		}else{
			
			//createVendors(objVendors);
		}
		
		
		
//		var arrVendorsObj =objVendors.obj;
//		var arrVendorIds = objVendors.ids;
//		
//		for(var i = 0; i < arrVendorIds.length; i++ ){
//			
//			var thisVendor = arrVendorIds[i]; 
//			var thisVendorObj = arrVendorsObj[i];
//			
//			var vendorName = thisVendorObj.vendorname; 				
//			var vendorline = nlapiFindLineItemValue('itemvendor', 'vendor', thisVendor);
//	
//			var fxRate = convertCurrency(lastFCA, lastFCACurrency, thisVendorObj.currency); fxRate  = addMarkup(fxRate, thisVendorObj.vendormarkup); 
//				
//
//			nlapiSelectNewLineItem('itemvendor');
//			
//	
//			
//			if(thisVendor == prefVendorCAN || thisVendor == prefVendorUSA){
//				nlapiSetCurrentLineItemValue("itemvendor", 'preferredvendor', 'T');
//			}
//			
//			
//			nlapiSetCurrentLineItemValue("itemvendor", "vendor", thisVendor);			
//			nlapiSetCurrentLineItemValue("itemvendor", "purchaseprice", fxRate);						
//			nlapiCommitLineItem("itemvendor");					
//			
//			nlapiLogExecution('DEBUG', 'Finding Vendor :  ' + vendorName, "LINE "+ vendorline + " FCA " +  lastFCA + " FXRATE "+ fxRate  + " vendorlines "+vendorlinescount); 
//			
//		}
	}
}


function createVendors(objVendors){
	
	
	try{}catch(ex){}
	
	var lastFCA = nlapiGetFieldValue('custitem_erp_vendorprice_last_fca');
	var lastFCACurrency = nlapiGetFieldValue('custitem_erp_vendorprice_last_currency');
	
	if(!isNullOrEmpty(lastFCACurrency)){
		lastFCACurrency = 2;		
	}
	
	if(!isNullOrEmpty(lastFCA)){
		
		var prefVendorCAN = 454534;
		var prefVendorUSA = 454664;
		
		var arrVendorsObj =objVendors.obj;
		var arrVendorIds = objVendors.ids;
		
		var vendorlinescount = '';
		
		for(var i = 0; i < arrVendorIds.length; i++ ){
			
			var thisVendor = arrVendorIds[i]; 
			var thisVendorObj = arrVendorsObj[i];
			
			var vendorName = thisVendorObj.vendorname;
			var vendorMarkup = thisVendorObj.markup;
			
			var vendorline = nlapiFindLineItemValue('itemvendor', 'vendor', thisVendor);

			var fxRate = convertCurrency(lastFCA, lastFCACurrency, thisVendorObj.currency); fxRate  = addMarkup(fxRate, vendorMarkup); 
				

			nlapiSelectNewLineItem('itemvendor');												
			if(thisVendor == prefVendorCAN || thisVendor == prefVendorUSA){
				nlapiSetCurrentLineItemValue("itemvendor", 'preferredvendor', 'T');
			}										
			nlapiSetCurrentLineItemValue("itemvendor", "vendor", thisVendor);			
			nlapiSetCurrentLineItemValue("itemvendor", "purchaseprice", fxRate);						
			nlapiCommitLineItem("itemvendor");					
			
			nlapiLogExecution('DEBUG', 'Execute Vendor :  ' + vendorName, "LINE "+ vendorline + " FCA " +  lastFCA + " FXRATE "+ fxRate  + " vendorlines "+vendorlinescount); 
			
		}
		
		nlapiSetFieldValue('custitem_erp_vendorprice_reset', 'F');
		nlapiSetFieldValue('isdropshipitem', 'T');
		nlapiSetFieldValue('dropshipexpenseaccount', 132);
		//nlapiSetFieldValue('isdropship', 'T');
		
	}
	
	
}


function createVendors_as(objVendors){
	
	
	try{}catch(ex){}
	
	var recItem = nlapiLoadRecord('inventoryitem', nlapiGetRecordId());
	
	
	
	var lastFCA = recItem.getFieldValue('custitem_erp_vendorprice_last_fca');
	var lastFCACurrency = recItem.getFieldValue('custitem_erp_vendorprice_last_currency');
	
	if(!isNullOrEmpty(lastFCACurrency)){
		lastFCACurrency = 2;		
	}
	
	if(!isNullOrEmpty(lastFCA)){
		
		var prefVendorCAN = 454534;
		var prefVendorUSA = 454664;
		
		var arrVendorsObj =objVendors.obj;
		var arrVendorIds = objVendors.ids;
		
		var vendorlinescount = '';
		
		for(var i = 0; i < arrVendorIds.length; i++ ){
			
			var thisVendor = arrVendorIds[i]; 
			var thisVendorObj = arrVendorsObj[i];
			
			var vendorName = thisVendorObj.vendorname;
			var vendorMarkup = thisVendorObj.markup;
			
			var vendorline = recItem.findLineItemValue('itemvendor', 'vendor', thisVendor);

			var fxRate = convertCurrency(lastFCA, lastFCACurrency, thisVendorObj.currency); fxRate  = addMarkup(fxRate, vendorMarkup); 
				
			recItem.selectNewLineItem('itemvendor');
			
			if(thisVendor == prefVendorCAN || thisVendor == prefVendorUSA){
				recItem.setCurrentLineItemValue("itemvendor", 'preferredvendor', 'T');			
			}			
			
			recItem.setCurrentLineItemValue("itemvendor", "vendor", thisVendor);			
			recItem.setCurrentLineItemValue("itemvendor", "purchaseprice", fxRate);						
			recItem.commitLineItem("itemvendor");					
			
			nlapiLogExecution('DEBUG', 'Execute Vendor :  ' + vendorName, "LINE "+ vendorline + " FCA " +  lastFCA + " FXRATE "+ fxRate  + " vendorlines "+vendorlinescount); 
			
		}
		
		recItem.setFieldValue('custitem_erp_vendorprice_reset', 'F');
		recItem.setFieldValue('isdropshipitem', 'T');
		recItem.setFieldValue('dropshipexpenseaccount', 132);
		//nlapiSetFieldValue('isdropship', 'T');
		
		nlapiSubmitRecord(recItem); 
		
	}
	
	
}



function convertCurrency( amount , sourceCurrency, vendorCurrency){

	var fxRate = nlapiExchangeRate(sourceCurrency, vendorCurrency); 	
	var conversion = amount; 
		
	conversion =  fxRate * amount; 

//	var ids = currencyObject.ids;
//	var obj = currencyObject.obj;
//	var conversion = amount;
//
//	if(!isNullOrEmpty(ids)){
//
//		var index = ids.indexOf(currencyId);
//		var curr = obj[index];
//		conversion = curr.rate * amount;
//
//	}

	return conversion;

}


function addMarkup(currAveCost, vendorMarkup){

	
	nlapiLogExecution("AUDIT", "addMarkup", currAveCost + " " + vendorMarkup);
	if(!isNullOrEmpty(vendorMarkup)){

		var markuppercentage = vendorMarkup.replace("%","");
		markuppercentage = Number(markuppercentage);

		nlapiLogExecution("AUDIT", "markuppercentage", markuppercentage);
		
		var costMarkup = (1 + (markuppercentage/100) ) * currAveCost;
		
		nlapiLogExecution("AUDIT", "costMarkup", currAveCost + " " + vendorMarkup + " " + costMarkup );
		
		
		return costMarkup;

	}else{
		return currAveCost;
	}

}

function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}

function getVendorLocCustomerMap(idLocation){

	var objVendors = new Object();
	var arrVendorsObj = new Array(); var arrVendorIds = new Array();

	var strSavedSearchIDVeLuCu = null;
	var arSaveSearchFiltersVeLuCu = new Array();
	var arSavedSearchColumnsVeLuCu = new Array();
	var arSavedSearchResultsVeLuCu = null;

	//if(!isNullOrEmpty(idLocation)){
	//	arSaveSearchFiltersVeLuCu.push(new nlobjSearchFilter( 'custrecord_ka_veculo_location', null, 'anyof', idLocation));
	//}

	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('internalid').setSort());
	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('name'));
	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('custrecord_ka_veculo_vendor'));
	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('custrecord_ka_veculo_location'));
	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('custrecord_ka_veculo_customer'));

	var arSavedSearchResultsVeLuCu = nlapiLoadSearch('customrecord_vendcustloc_map', 'customsearch_ka_veculo');

	arSavedSearchResultsVeLuCu.addColumns(arSavedSearchColumnsVeLuCu);
	arSavedSearchResultsVeLuCu.addFilters(arSaveSearchFiltersVeLuCu);

	var resultSet = arSavedSearchResultsVeLuCu.runSearch();

	if(resultSet){

		var results = resultSet.getResults(0,1000);


		for(var i = 0; i < results.length; i++){

			try{
				var idCurrentItem = results[i].getValue('internalid');
				var columns = results[i].getAllColumns();
				var objResult = results[i];

				var objVendor = new Object();
				objVendor.markup = objResult.getValue(columns[4]);
				objVendor.vendor = objResult.getValue(columns[1]);
				objVendor.vendorname = objResult.getText(columns[1]);
				objVendor.currency = objResult.getValue(columns[5]);
				objVendor.currencyTxt = objResult.getText(columns[5]);
				objVendor.subcurrency = objResult.getValue(columns[6]);
				objVendor.subcurrencyTxt = objResult.getText(columns[6]);

				arrVendorsObj.push(objVendor);
				arrVendorIds.push(objVendor.vendor);

			}catch(ex){}

		}

		objVendors.obj = arrVendorsObj;
		objVendors.ids = arrVendorIds;

		nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap", JSON.stringify(objVendors));
		return objVendors;

	}else{
		nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap", null);
		return null;
	}



}