function createPurchaseOrderRecordAS(type) {
	
	
	//TODO: Do only when create
	
	// Only run this script if the transaction was generated from a Inventory
	// purchase order

	
	nlapiLogExecution('DEBUG', 'Type', type);
	
	if (type != 'delete') {
	
		// Only start when the purchase orer form 100 is used, the PO field is
		// empty on the item receipt & the customer is not

		var ItemReceipt = nlapiLoadRecord('itemreceipt', nlapiGetRecordId());
		var intercoPO = ItemReceipt.getFieldValue('custbody_ka_intercompany_po');
		var thisShipMethod = ItemReceipt.getFieldValue('custbody_ka_shipping_method');
		
		nlapiLogExecution('AUDIT', 'thisShipment', thisShipMethod + " " + intercoPO);
		
		
		var idIRCurrency = ItemReceipt.getFieldValue('currency');
		var idIRCurrencyTxt = ItemReceipt.getFieldText('currency');
		
		if(isNullOrEmpty(intercoPO)){
			
			try {
			

			nlapiLogExecution('AUDIT', 'this point', 1);
			
			var idIRCreatedFrom =  ItemReceipt.getFieldValue('createdfrom');					
			var shipTo = nlapiLookupField('purchaseorder',idIRCreatedFrom, 'shipto');			
			var idCustomForm = nlapiLookupField('purchaseorder', idIRCreatedFrom , 'customform');
			var bInterCo = isNullOrEmpty(intercoPO);

			
			
			nlapiLogExecution('AUDIT', 'this point', 2);
			
			var objIRPO = new Object();
			
			objIRPO.customform = idCustomForm; 
			objIRPO.shipto = shipTo;
			objIRPO.createdfrom = idIRCreatedFrom;
			objIRPO.intercopo = bInterCo;
			
			nlapiLogExecution('DEBUG', 'createPurchaseOrderRecord', JSON.stringify(objIRPO));								
			//nlapiLogExecution('DEBUG', 'createPurchaseOrderRecord', "idCustomForm: " + idCustomForm + " bInterCo: " + bInterCo + " shipTo:" + shipTo);
			
			
			
			
			nlapiLogExecution('AUDIT', 'this point', 3);
			
			if (idCustomForm == 100  &&  bInterCo && (shipTo == 1785 || shipTo == 49141)) {
				// location 128321 is the Canada Warehouse K&N
					
				
				nlapiLogExecution('AUDIT', 'this point', 4);
				
				var purchaseOrder = nlapiCreateRecord('purchaseorder');  //, {recordmode : 'dynamic'});
				// Get the vendor record to be used on PO creation from the customer
				// record on the PO
				// Check for empty or invalid fields

				var customerVendorID = nlapiLookupField('customer', shipTo, 'custentity_ka_vendor_customer');
				// checking for empty values
				
				var recVendor = nlapiLoadRecord('vendor', customerVendorID); 		
				
				if(!isNullOrEmpty(recVendor)){
													
					//Create Currency Object
					var objSourceCurrency		= 		new  Object();
					objSourceCurrency.id		=		recVendor.getFieldValue('currency');
					objSourceCurrency.name		= 		recVendor.getFieldText('currency');

					//Currency Array
					var arrCurrencyArray = new Array();
					var currObject = new Object();
					currObject.id  = idIRCurrency;
					currObject.name = idIRCurrencyTxt;
					
					arrCurrencyArray.push(currObject);
										
					//Create Currency Exchange
					var currencyObject = createCurrencyExchange(objSourceCurrency, arrCurrencyArray);
																			
				}
				
								
				var customerVendorLocation = nlapiLookupField('customer', shipTo,'custentity_ka_location');
				var customerVendorSubsidiary = nlapiLookupField('vendor',customerVendorID, 'subsidiary');
				
				var strVendorTaxCode = recVendor.getFieldValue('taxitem');
				var idVendorCurrency = recVendor.getFieldValue('currency');
				var idVendorCurrencyTxt = recVendor.getFieldText('currency');
				
				var strVendorRetailFormula = recVendor.getFieldValue('custentity_intercompany_retail_percent');
				strVendorRetailFormula  = eval(strVendorRetailFormula); 
				
				strVendorRetailFormula
				
				var taxCode = recVendor.getFieldValue('taxitem');
				
				objIRPO.customerVendorID = customerVendorID; 
				objIRPO.customerVendorLocation = customerVendorLocation;
				objIRPO.customerVendorSubsidiary = customerVendorSubsidiary;
				objIRPO.taxCode = strVendorTaxCode;
				
							
				nlapiLogExecution('DEBUG', 'createPurchaseOrderRecord', JSON.stringify(objIRPO));
				
			
					// set the form with default values
					purchaseOrder.setFieldValue('customform', 159);
					purchaseOrder.setFieldValue('trandate', ItemReceipt.getFieldValue('trandate'));
					purchaseOrder.setFieldValue('entity', customerVendorID);
					purchaseOrder.setFieldValue('employee', nlapiGetUser());
					purchaseOrder.setFieldValue('subsidiary',customerVendorSubsidiary);
					purchaseOrder.setFieldValue('location', customerVendorLocation);
					purchaseOrder.setFieldValue('location', customerVendorLocation);
					purchaseOrder.setFieldValue('custbody_ka_item_receipt_number', nlapiGetRecordId());
					purchaseOrder.setFieldValue('custbody_erp_price_method',2);
					
					nlapiLogExecution('AUDIT', 'thisShipment2', thisShipMethod);
					purchaseOrder.setFieldValue('custbody_ka_shipping_method',thisShipMethod);
					
					
					
					var ItemReceiptLineCount = ItemReceipt.getLineItemCount('item');

					for (var j = 1; j <= ItemReceiptLineCount; j++) {
						ItemReceipt.selectLineItem('item', j);
						var qty = ItemReceipt.getLineItemValue('item', 'quantity',j);
						var itm = ItemReceipt.getLineItemValue('item', 'item', j);
						var rate = ItemReceipt.getLineItemValue('item', 'rate', j);		
						
						//var newrate = calculateRate(rate, idIRCurrency, idVendorCurrency, strVendorRetailFormula); 									
						//var memo = getRetailMemo(rate, idIRCurrencyTxt, newrate[0], idVendorCurrencyTxt, strVendorRetailFormula, newrate[1]);
						
						
//						var memo = "fca: " + rate + " " + idIRCurrencyTxt 
//									+ "\nfxrate: " + newrate[0] + " " +   idVendorCurrencyTxt 
//									+ "\nMarkup: " +  strVendorRetailFormula
//									+ "\nNew Rate: " + newrate ;
						
						
//						
						var rateObj 				=	new Object; //getRate(rate, idIRCurrency,objSourceCurrency.id, currencyObject, strVendorRetailFormula);
//						
//						rate = rateObj.rate;
//						var memo = rateObj.memo;
						
//						rate = 1;
						var memo = '';
						
						nlapiLogExecution('DEBUG','THIS RATE', JSON.stringify(rateObj)); 
						
						purchaseOrder.selectNewLineItem('item');
						purchaseOrder.setCurrentLineItemValue('item', 'item', itm);
						purchaseOrder.setCurrentLineItemValue('item', 'quantity', qty);
						purchaseOrder.setCurrentLineItemValue('item', 'rate', rate);
						purchaseOrder.setCurrentLineItemValue('item', 'custcol_erp_to_memo', memo);
						purchaseOrder.setCurrentLineItemValue('item', 'taxcode', strVendorTaxCode);
						purchaseOrder.commitLineItem('item');
					}

			

					if (isNullOrEmpty(intercoPO)) {

						var id = nlapiSubmitRecord(purchaseOrder);
						nlapiLogExecution('AUDIT', 'createPurchaseOrderRecord: purchaseOrder submit', id); 
						
						ItemReceipt.setFieldValue('custbody_ka_intercompany_po', id);
						
						
						if(!isNullOrEmpty(id)){
							
							try{
								var url = nlapiResolveURL('SUITELET',
										'customscript_ka_intercompany_po_ir_sl',
										'customdeploy_ka_intercompany_po_ir_sl', true);
								
								var params = new Array();
								params['transactionRecord'] = id;
			
								nlapiLogExecution('DEBUG',
										'intercoPOCreation_RunPO_UE_SL url', url);
								nlapiLogExecution('DEBUG',
										'intercoPOCreation_RunPO_UE_SL params', params);
								
								var slResponse = nlapiRequestURL(url, params);
								
							}
							catch(ex){
								if(!isNullOrEmpty(id)){
									nlapiSubmitRecord(ItemReceipt);
								}
								
								var errorStr = (ex.getCode != null) ? ex.getCode() + '\n' + ex.getDetails() + '\n' + ex.getStackTrace().join('\n') : ex.toString();
								nlapiLogExecution('AUDIT', 'userEventAfterSubmit Exception error', errorStr);
								
							}
						}
					}

					nlapiSubmitRecord(ItemReceipt);
					
				}
			}
			catch (ex) {
				
				var errorStr = (ex.getCode != null) ? ex.getCode() + '\n' + ex.getDetails() + '\n' + ex.getStackTrace().join('\n') : ex.toString();
				nlapiLogExecution('AUDIT', 'userEventAfterSubmit Exception error', errorStr);
				
			}
	
		}
	}
}




function createPurchaseOrderRecord(type) {
	nlapiLogExecution('DEBUG', 'Type', type);

	if (type == 'delete') {
		var itemReceiptId = nlapiGetRecordId();
		var purchaseOrderRecord = nlapiLookupField('itemreceipt',
				itemReceiptId, 'custbody_ka_intercompany_po');

		if (!isNullOrEmpty(purchaseOrderRecord)) {
			throw nlapiCreateError(
					"PO PURCHASE ORDER EXISTS",
					"PO Linked to item Receipt - Delete Purchase order before deleting item receipt");
		}
	}

}

function linkPOAfterSubmit(type) {
	if (type != 'delete') {
		var itemReceiptId = nlapiGetRecordId();
		if (!isNullOrEmpty(itemReceiptId)) {
			var purchaseOrderRecord = nlapiLookupField('itemreceipt',
					itemReceiptId, 'custbody_ka_intercompany_po');

			if (!isNullOrEmpty(purchaseOrderRecord)) {
				nlapiSubmitField('purchaseorder', purchaseOrderRecord,
						'custbody_ka_interco_po_item_rec', itemReceiptId, true);
			}

		}
	}
}

function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}