/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Aug 2016     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function transferprice_main_ue(type){
 
	nlapiSetFieldValue('externalid','');
	
	var iteminternalid = nlapiGetFieldValue('custrecord_erp_tpip_linktoitem'); 
	nlapiLogExecution('AUDIT', 'HERE', iteminternalid);
	
	
//	nlapiSetFieldValues(['externalid'],[iteminternalid],true,true);
	nlapiSetFieldValue('externalid', iteminternalid); 
	
	var iteminternalid2 = nlapiGetFieldValue('externalid'); 
	nlapiLogExecution('AUDIT', 'HERE2', iteminternalid2);
	
}
