/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       30 Sep 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only)
 *                      paybills (vendor payments)
 * @returns {Void}
 */

/*
function erp_Main_ItemReceipt_UE_AS(type, form, request){
	createUpdate_VendorInterCOPriceAS(type);
}

function erp_Main_ItemReceipt_UE_BL(type, form, request){

}


*/


function erp_Main_ItemReceipt_LC_Main(type){

	/*var strOrderType = nlapiGetFieldValue('ordertype');

	setItemToRestock(strOrderType);
	setLandedCost(strOrderType);*/


}


function erp_Main_ItemReceipt_LC_UE_BS(){

	var strOrderType = nlapiGetFieldValue('ordertype');

//	setItemToRestock(strOrderType);
	setLandedCost(strOrderType);

}


function setLandedCost(strOrderType){


	nlapiLogExecution("AUDIT", "func: setLandedCost: type", strOrderType);

	if(strOrderType == 'PurchOrd'){


		var strLandedCostMemo = "";

		var idCreatedFrom = nlapiGetFieldValue('createdfrom');
		var idLocation = nlapiGetFieldValue('location'); idLocation = parseInt(idLocation)

		var shipCountry = nlapiLookupField('purchaseorder', idCreatedFrom, 'shipcountry');
		var customFormPO = nlapiLookupField('purchaseorder', idCreatedFrom, 'customform');
		var shippingMethodPO = nlapiLookupField('purchaseorder', idCreatedFrom, 'custbody_ka_shipping_method');


		var locationCountry = nlapiLookupField('location', idLocation, 'country');


		if(customFormPO != 172){
			


var locWhitelist = [7,50,57,59,58];

strLandedCostMemo = strLandedCostMemo + "locWhitelist " + locWhitelist.toString() + "\n";
strLandedCostMemo = strLandedCostMemo + " Location " + idLocation+ " " + locWhitelist.indexOf() +"\n";

if(locWhitelist.indexOf(idLocation) > -1){



			//strLandedCostMemo = strLandedCostMemo + "ship country from PO : " + shipCountry + "\n";
			//strLandedCostMemo = strLandedCostMemo + "receiving country from IR : " + locationCountry + "\n";


			//if(shipCountry == locationCountry){

				var intCount = nlapiGetLineItemCount('item');
				var fIRTotal = 0.00;

				for (var i = 1; i <= intCount ; i++){

					var qty  = nlapiGetLineItemValue('item', 'quantity', i);
					var rate = nlapiGetLineItemValue('item', 'rate', i);

					nlapiSetLineItemValue('item', 'custcol_landed_cost_po', i, idCreatedFrom);


					qty = parseFloat(qty); if(isNaN(qty)){qty = 0;}
					rate = parseFloat(rate); if(isNaN(rate)){qty = 0;}

					nlapiLogExecution("AUDIT", "func: setLandedCost: qty ====> ", qty);
					nlapiLogExecution("AUDIT", "func: setLandedCost: rate ====> ", rate);

					fIRTotal =  fIRTotal + (qty * rate);

				}


				nlapiLogExecution("AUDIT", "func: setLandedCost: fIRTotal", fIRTotal);

				var subId = nlapiGetFieldValue('subsidiary');
				var subRecord = nlapiLoadRecord('subsidiary' , subId );

				nlapiLogExecution("AUDIT", "func: setLandedCost: Subsidiary", subId);

				var freightPercentage = subRecord.getFieldValue('custrecord_freight_landed_cost');

				var freightPercentage_Ocean = subRecord.getFieldValue('custrecord_erp_freight_ocean');
				var freightPercentage_Air   = subRecord.getFieldValue('custrecord_erp_freight_air');

				var dutyPercentage =  subRecord.getFieldValue('custrecord_duty_landed_cost');


				nlapiLogExecution("AUDIT", "func: setLandedCost: freightPercentage", freightPercentage);
				nlapiLogExecution("AUDIT", "func: setLandedCost: freightPercentage_Ocean", freightPercentage_Ocean);
				nlapiLogExecution("AUDIT", "func: setLandedCost: freightPercentage_Air", freightPercentage_Air);
				nlapiLogExecution("AUDIT", "func: setLandedCost: dutyPercentage", dutyPercentage);

				strLandedCostMemo = strLandedCostMemo + "IR Total : " + fIRTotal + "\n";
				strLandedCostMemo = strLandedCostMemo + "Freight Percentage (Land): " + freightPercentage + "\n";
				strLandedCostMemo = strLandedCostMemo + "Freight Percentage (Ocean): " + freightPercentage_Ocean + "\n";
				strLandedCostMemo = strLandedCostMemo + "Freight Percentage (Air): " + freightPercentage_Air + "\n";

				strLandedCostMemo = strLandedCostMemo + "Duty Percentage : " + dutyPercentage + "\n";

				freightPercentage = eval(freightPercentage);
				dutyPercentage = eval(dutyPercentage);


				//Shipping Method Checks

				nlapiSetFieldValue('landedcostamount1', 0 ); //Ocean
				nlapiSetFieldValue('landedcostamount2', 0 ); //Duty
				nlapiSetFieldValue('landedcostamount3', 0 ); //Air
				nlapiSetFieldValue('landedcostamount4', 0 ); //Ground

				if(shippingMethodPO == 5 || shippingMethodPO == 6 || shippingMethodPO == 1 ||  shippingMethodPO == 2)	{	//Air
						nlapiSetFieldValue('landedcostamount3', fIRTotal * freightPercentage_Air );

				}else if(shippingMethodPO == 3 ){	//Ocean

						nlapiSetFieldValue('landedcostamount1', fIRTotal * freightPercentage_Ocean );

				}else if(shippingMethodPO == 4 ){	//Truck

						nlapiSetFieldValue('landedcostamount4', fIRTotal * freightPercentage ); 

				}

				nlapiSetFieldValue('landedcostamount2', fIRTotal * dutyPercentage  );

if(true){

				nlapiSetFieldValue('landedcostmethod', 'QUANTITY');

				nlapiSetFieldValue('landedcostsource1', 'MANUAL');	 //Freight
				nlapiSetFieldValue('landedcostsource2', 'MANUAL'); 	 //Duty
				nlapiSetFieldValue('landedcostsource3', 'MANUAL'); 	 //Duty
				nlapiSetFieldValue('landedcostsource4', 'MANUAL'); 	 //Duty


}


				strLandedCostMemo = strLandedCostMemo + "Freight Ocean : " + fIRTotal * freightPercentage_Air + "\n";
				strLandedCostMemo = strLandedCostMemo + "Freight Air : " + fIRTotal * freightPercentage_Ocean + "\n";
				strLandedCostMemo = strLandedCostMemo + "Freight Land: " + fIRTotal * freightPercentage + "\n";
				strLandedCostMemo = strLandedCostMemo + "Duty : " + fIRTotal * dutyPercentage + "\n";

			}else{

				strLandedCostMemo = strLandedCostMemo + "ship country and receipt country is not the same : Not processing auto landed cost \n";
			}
		}

		else{

			strLandedCostMemo = strLandedCostMemo + "custom form : 172, not processing auto landed cost \n";

		}

		 nlapiSetFieldValue('custbody_erp_landedcost_memo', strLandedCostMemo);

	}
}










function setItemToRestock(strOrderType){



	if(strOrderType == 'RtnAuth'){

		var intCount = nlapiGetLineItemCount('item');
		for (var i = 1; i <= intCount ; i++){

			nlapiSetLineItemValue('item', 'restock', i, 'T');

		}
	}



}

function createUpdate_VendorInterCOPriceAS(type){

	nlapiLogExecution("AUDIT", "func: createUpdate_VendorInterCOPriceAS: type", type);

	if(type != "delete"){

	if(true){

		var vendorItemLog = true;
		var submitItemLog = false;
		var checkGovernanceLog = false;

		var recId = nlapiGetRecordId();
		nlapiLogExecution("AUDIT", "func: createUpdate_VendorInterCOPriceAS", recId);

		var ItemReceiptRecord = nlapiLoadRecord('itemreceipt', recId);

		//Check the Item Receipt
		var idPriceLevel = 1;
		var idCurrency = ItemReceiptRecord.getFieldValue('currency');
		var arrItemArray = getItemSublistAS(ItemReceiptRecord);

		if(!isNullOrEmpty(arrItemArray)){	if(true){  //if(arrItemArray.length < 40){

			//Check the Item Receipt
			var idPriceLevel = 1;
			var idLocation = null;

			var ordType = ItemReceiptRecord.getFieldValue('ordertype');

			if(ordType == 'PurchOrd'){
				var idIRLocation = ItemReceiptRecord.getFieldValue('location');
				idLocation = idIRLocation;
			}else if(ordType == 'RtnAuth'){
				var idIRLocation = ItemReceiptRecord.getFieldValue('location');
				idLocation = idIRLocation;
			}else if(ordType == 'TrnfrOrd'){
				var createdFrom = ItemReceiptRecord.getFieldValue('createdfrom');
				var idIRTransLocation = nlapiLookupField('transferorder',createdFrom , 'transferlocation');
				idLocation = idIRTransLocation;
			}else {
				var idIRLocation = ItemReceiptRecord.getFieldValue('location');
				idLocation = idIRLocation;
			}

			nlapiLogExecution("AUDIT", "func: createUpdate_VendorInterCOPrice_SS: ordType/loc/tranloc ", "ordType" + ordType + " loc:" + idLocation + " tranloc:"  + idIRTransLocation);



			//Load Vendors to be updated/created
			var objVendorList = getVendorLocCustomerMap(idLocation); 												//Vendors
			var objItemAvePriceLoc = getItemLocAveCost(arrItemArray, idCurrency, idPriceLevel, idLocation);			//Items LocationAverage Price


			var arrItems = objItemAvePriceLoc.ids;
			var arrItemsObj = objItemAvePriceLoc.obj;

			var arrVendors = objVendorList.ids;
			var arrVendorsObj = objVendorList.obj;

			var currencyConversion = retrieveCurrencyExchange(arrVendorsObj);


			if(!isNullOrEmpty(arrItems) && !isNullOrEmpty(arrVendors)){

				var intIRItemCount = ItemReceiptRecord.getLineItemCount('item');
				for(var o = 1 ; o <= intIRItemCount; o++){

					var intItemId  = ItemReceiptRecord.getLineItemValue('item','item',o);

					var ItemRecord = nlapiLoadRecord("inventoryitem", intItemId);
					//var itemVendorCount = ItemRecord.getLineItemCount("itemvendor");
					var updateObject = new Array(); var updateVendorObject = new Array();

					checkGovernance("nlapiLoadRecord(item: " + intItemId + ")", checkGovernanceLog);
					if(vendorItemLog) nlapiLogExecution("AUDIT", "======================== item ", intItemId);

					var logMemo = "";

					for(var q=0; q< arrVendors.length; q++){

						var thisVendor = arrVendors[q];

						var found = false;
						var objVendorMatch = new Object();
						var vendorline = -1;

						if(vendorItemLog) nlapiLogExecution("AUDIT", "================================================ tv " + thisVendor, thisVendor);

						var itemVendorCount = ItemRecord.getLineItemCount("itemvendor");

						for(var r=1;r<=itemVendorCount;r++){

							var idVendor = ItemRecord.getLineItemValue("itemvendor", "vendor" , r);
							if(vendorItemLog) nlapiLogExecution("AUDIT", "======================================================================== iv " + idVendor, idVendor);

							if(thisVendor == idVendor){

								found = true; //nlapiLogExecution("AUDIT", "Finding Vendor Match: " + thisVendor, "found");
								if(vendorItemLog) nlapiLogExecution("AUDIT", "===========================================================================================fo " + found, found);

								vendorline = r;
								break;
							}
						}

						objVendorMatch.exist = found;
						objVendorMatch.itemreceipt = ItemReceiptRecord.getId();
						objVendorMatch.item = ItemRecord.getId();
						objVendorMatch.itemname = ItemRecord.getFieldValue("itemid");
						objVendorMatch.location = idLocation;
						objVendorMatch.vendorid = thisVendor;
						objVendorMatch.vendorname = arrVendorsObj[q].vendorname;
						objVendorMatch.vendormarkup = arrVendorsObj[q].markup;
						objVendorMatch.vendorline = vendorline;
						objVendorMatch.vendorcurrency = arrVendorsObj[q].currency;

						logMemo = logMemo + "N:" + found;
						logMemo = logMemo + " VeId:" + objVendorMatch.vendorid;
						logMemo = logMemo + " VeMuP:" + objVendorMatch.vendormarkup;

						var itemIndex = arrItems.indexOf(intItemId);
						if(itemIndex >= 0){
							objVendorMatch.locavecost =  arrItemsObj[itemIndex].locavecost;

							var currAveCost = convertCurrency( objVendorMatch.locavecost , objVendorMatch.vendorcurrency, currencyConversion);
							var markAveCost = addMarkup(currAveCost, objVendorMatch.vendormarkup);

							objVendorMatch.locavecostincurr =  currAveCost;
							objVendorMatch.locmarkupavecostincurr =  markAveCost;

						}

						if(objVendorMatch.exist){
							ItemRecord.removeLineItem('itemvendor', objVendorMatch.vendorline, true);
							ItemRecord.selectNewLineItem('itemvendor');
							ItemRecord.setCurrentLineItemValue("itemvendor", "purchaseprice", objVendorMatch.locmarkupavecostincurr);
							ItemRecord.setCurrentLineItemValue("itemvendor", "vendor", objVendorMatch.vendorid );
							ItemRecord.commitLineItem("itemvendor");
						}else{
							ItemRecord.selectNewLineItem("itemvendor");
							ItemRecord.setCurrentLineItemValue("itemvendor", "vendor", objVendorMatch.vendorid );
							ItemRecord.setCurrentLineItemValue("itemvendor", "purchaseprice", objVendorMatch.locmarkupavecostincurr);
							ItemRecord.commitLineItem("itemvendor");
						}

						//double check
						var yitemVendorCount = ItemRecord.getLineItemCount("itemvendor");

						for(var y=1;y<=yitemVendorCount;y++){
							var idVendor = ItemRecord.getLineItemValue("itemvendor", "vendor" , y);
							if(vendorItemLog) nlapiLogExecution("AUDIT", "====== ve " + idVendor);

						}



						logMemo = logMemo + " LoId:" +  objVendorMatch.location;
						logMemo = logMemo + " LoAve:" + objVendorMatch.locavecost;
						logMemo = logMemo + " Cost:" +  objVendorMatch.locmarkupavecostincurr;

						logMemo = logMemo + "\n";

						updateObject.push(objVendorMatch);
					}

					nlapiLogExecution("DEBUG", "Item: " + objVendorMatch.item + " Vendor: " + thisVendor, JSON.stringify(updateObject));

					ItemRecord.setFieldValue("custitem_ka_loc_ave_cost_1901",objVendorMatch.locavecost);
					ItemRecord.setFieldValue('custitem_erp_interco_exchange_rate',JSON.stringify(currencyConversion));
					ItemRecord.setFieldValue('custitem_erp_interco_json',logMemo);
					ItemRecord.setFieldValue('custitem_erp_last_interco_ir',objVendorMatch.itemreceipt);
					ItemRecord.setFieldValue('custitem_ec_intercompany_price_updated', nlapiDateToString(new Date(), "datetimetz"));

					var submitid = nlapiSubmitRecord(ItemRecord);

					var rem = checkGovernance("nlapiSubmitRecord(ItemRecord)", checkGovernanceLog);

					if(rem < 100){
						runSchedScript(recId,o);
						break;
					}

					if(submitItemLog) nlapiLogExecution("DEBUG", "Finding Vendor Match: Submit", submitid);
				}
			}
		}
		}
	}
	}
}


function retrieveCurrencyExchange(vendorList){

	var currencyList = new Array();
	var currencyListObject = new Array();
	var currencyFound = new Array();

	var currencyObj = new Object()


	if(!isNullOrEmpty(vendorList)){

		for(var s=0; s < vendorList.length; s++){

			var currencyObject = new Object();

			var vendorObj = vendorList[s];

			var subsidiaryCurrency = vendorObj.subcurrency;
      		var subsidiaryCurrencyName = vendorObj.subcurrencyTxt;
			var vendorCurrency = vendorObj.currency;
			var vendorCurrencyName = vendorObj.currencyTxt;

      		if(currencyFound.indexOf(vendorCurrency) == -1){

	       		currencyFound.push(vendorCurrency);

    	    	var converteCurrency = converCurrency(subsidiaryCurrency, 1, vendorCurrency);

        		currencyObject.baseSubCurrencyId = subsidiaryCurrency;
	        	currencyObject.baseSubCurrencyName = subsidiaryCurrencyName;
  				currencyObject.currencyId =vendorCurrency;
  				currencyObject.currencyName = vendorCurrencyName;
  				currencyObject.rate	= converteCurrency;

  				currencyList.push(vendorCurrency);
  				currencyListObject.push(currencyObject);

     		 }



		}

		currencyObj.ids = currencyList;
		currencyObj.obj = currencyListObject;

		nlapiLogExecution("DEBUG","func: retrieveCurrencyExchange:  Currency Object: ", JSON.stringify(currencyObj));

		return currencyObj;

	}else{
		return null;
	}




}

function convertCurrency( amount , currencyId, currencyObject){

	var ids = currencyObject.ids;
	var obj = currencyObject.obj;
	var conversion = amount;

	if(!isNullOrEmpty(ids)){

		var index = ids.indexOf(currencyId);
		var curr = obj[index];
		conversion = curr.rate * amount;

	}

	return conversion;

}


function checkGovernance(method, log){

	var objContext = nlapiGetContext();
	var remUnits = objContext.getRemainingUsage();

	if(log) nlapiLogExecution("AUDIT", "func: checkGovernance: " + method, remUnits);

	return remUnits;

}


function addMarkup(currAveCost, vendorMarkup){

	if(!isNullOrEmpty(vendorMarkup)){

		var markuppercentage = vendorMarkup.replace("%","");
		markuppercentage = Number(markuppercentage);

		var costMarkup = (1 + (markuppercentage/100) ) * currAveCost;
		return costMarkup;

	}else{
		return currAveCost;
	}

}






function converCurrency(strCurrency, decCost, strVendorCurrency){

    // Pull the USD exchange rate as of today's date

	try{
    //if (strVendorCurrency != 'CAD') {
        var rate = nlapiExchangeRate(strCurrency, strVendorCurrency, nlapiDateToString(new Date()));
        return parseFloat(decCost) * rate;

        nlapiLogExecution("AUDIT", "func: converCurrency", "subcurrency: "+ strCurrency + " vendorcurrency: " + strVendorCurrency + " rate:" + rate);


    //}
    //else{
    //    return parseFloat(decCost);
    //}

	} catch (ex){
                     nlapiLogExecution("AUDIT", "error", ex.toString());
		return parseFloat(decCost);
	}
}

function setItemRecord(objSearchResultItem, arrVendors){

	nlapiLogExecution("DEBUG", "func: setItemRecord", "START");

	try{

	if(!isNullOrEmpty(objSearchResultItem)){

		var arrItemIds = objSearchResultItem.ids;
		var arrItemObj = objSearchResultItem.objs;

		nlapiLogExecution("DEBUG", "func: setItemRecord: arr length ", JSON.stringify(arrItemIds));

		for(var l = 0 ; l < arrItemIds.length ;  l++){

			nlapiLogExecution("DEBUG", "func: setItemRecord: arrItemIds", arrItemIds[l]);

			var itemRecord = nlapiLoadRecord('inventoryitem', arrItemIds[l]);

			if(itemRecord){

	            var vendorItemLines = getItemVendorLines(itemRecord);

			}
		}
	}

	}
	catch(ex){ nlapiLogExecution("AUDIT", "error", ex.toString());}

}


function getVendorLocCustomerMap(idLocation){

	if(!isNullOrEmpty(idLocation)){

	var objVendors = new Object();
	var arrVendorsObj = new Array(); var arrVendorIds = new Array();


	var strSavedSearchIDVeLuCu = null;
	var arSaveSearchFiltersVeLuCu = new Array();
	var arSavedSearchColumnsVeLuCu = new Array();
	var arSavedSearchResultsVeLuCu = null;
	arSaveSearchFiltersVeLuCu.push(new nlobjSearchFilter( 'custrecord_ka_veculo_location', null, 'anyof', idLocation));

	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('internalid').setSort());
	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('name'));
	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('custrecord_ka_veculo_vendor'));
	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('custrecord_ka_veculo_location'));
	arSavedSearchColumnsVeLuCu.push(new nlobjSearchColumn('custrecord_ka_veculo_customer'));

	var arSavedSearchResultsVeLuCu = nlapiLoadSearch('customrecord_vendcustloc_map', 'customsearch_ka_veculo');

	arSavedSearchResultsVeLuCu.addColumns(arSavedSearchColumnsVeLuCu);
	arSavedSearchResultsVeLuCu.addFilters(arSaveSearchFiltersVeLuCu);

	var resultSet = arSavedSearchResultsVeLuCu.runSearch();

	if(resultSet){

		var results = resultSet.getResults(0,1000);


		for(var i = 0; i < results.length; i++){

			try{
				var idCurrentItem = results[i].getValue('internalid');
				var columns = results[i].getAllColumns();
				var objResult = results[i];

				var objVendor = new Object();
				objVendor.markup = objResult.getValue(columns[4]);
				objVendor.vendor = objResult.getValue(columns[1]);
				objVendor.vendorname = objResult.getText(columns[1]);
				objVendor.currency = objResult.getValue(columns[5]);
				objVendor.currencyTxt = objResult.getText(columns[5]);
				objVendor.subcurrency = objResult.getValue(columns[6]);
				objVendor.subcurrencyTxt = objResult.getText(columns[6]);

				arrVendorsObj.push(objVendor);
				arrVendorIds.push(objVendor.vendor);

			}catch(ex){ nlapiLogExecution("AUDIT", "error", ex.toString());}

		}

		objVendors.obj = arrVendorsObj;
		objVendors.ids = arrVendorIds;

		nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap", JSON.stringify(objVendors));
		return objVendors;

	}else{
		nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap", null);
		return null;
	}


	}else{
		nlapiLogExecution("DEBUG", "func: getVendorLocCustomerMap", "no location id");
		return null;
	}
}

function getItemVendorLines(itemObj, arrVendors){



	nlapiLogExecution("DEBUG", "func: getItemVendorLines", "START");
	nlapiLogExecution("DEBUG", "func: getItemVendorLines: Item: ", itemObj.getId());

	var intItemVendorCount = itemObj.getLineItemCount('itemvendor');
	nlapiLogExecution("DEBUG", "func: getItemVendorLines: Vendor Count: ", intItemVendorCount);

	for(var m=1; m <= intItemVendorCount; m++){

		var intVendorId = itemObj.getLineItemValue('itemvendor','vendor',m);
		var intVendorName = itemObj.getLineItemText('itemvendor','vendor',m);
		var intVendorName = itemObj.getLineItemText('itemvendor','vendor',m);

		var objVendorObject = new Object();
		objVendorObject.vendorid = intVendorId;
		objVendorObject.vendorname = intVendorName;

		nlapiLogExecution("DEBUG", "func: getItemVendorLines", JSON.stringify(objVendorObject));
	}
}


function runSchedScript(recordId, lineNumber){

    var params = {
    		custscript_cuvp_record_id: recordId ,
    		custscript_cuvp_starting_line_number: lineNumber
	 };

	 var result = nlapiScheduleScript('customscript_erp_vendorintercoprice_ss', null, params);                  // 20 units
     nlapiLogExecution("DEBUG", "func: runSchedScript", result);
}



function getItemAverageCost(arrItemArray){

		nlapiLogExecution("DEBUG", "func: getItemAverageCost: length", arSavedSearchResultsItm.length);

		var strSavedSearchIDItm = null;
		var arSaveSearchFiltersItm = new Array();
		var arSavedSearchColumnsItm = new Array();
		var arSavedSearchResultsItm = null;

		var arrResultObjectArray = new Array();
		var arrIdObjectArray = new Array();

		var objItemResult = new Object();

		if(!isNullOrEmpty(items)){
			arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null , 'anyof', arrItemArray ));
		}

		arSaveSearchFiltersItm.push(new nlobjSearchFilter( 'currency', 'pricing',  'anyof', currency));
		arSaveSearchFiltersItm.push(new nlobjSearchFilter( 'pricelevel', 'pricing',  'is', pricelevel));

		arSavedSearchColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('name'));
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('currency','pricing'));
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('pricelevel','pricing'));
		arSavedSearchColumnsItm.push(new nlobjSearchColumn('unitprice','pricing'));

		arSavedSearchResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchColumnsItm);


		if(!isNullOrEmpty(arSavedSearchResultsItm)){
			nlapiLogExecution("DEBUG", "func: getItemAverageCost: length", arSavedSearchResultsItm.length);
		}
}



function getItemLocAveCost(items, currency, pricelevel, location){


	var strSavedSearchIDItm = null;
	var arSaveSearchFiltersItm = new Array();
	var arSavedSearchColumnsItm = new Array();
	var arSavedSearchResultsItm = null;

	var arrResultObjectArray = new Array();
	var arrIdObjectArray = new Array();

	var objItemResult = new Object();

	if(!isNullOrEmpty(items)){
		arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null , 'anyof', items ));
	}

	arSaveSearchFiltersItm.push(new nlobjSearchFilter( 'inventorylocation', null ,  'anyof', location));

	arSavedSearchColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('name'));
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('locationaveragecost'));
	arSavedSearchColumnsItm.push(new nlobjSearchColumn('inventorylocation'));

	arSavedSearchResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchColumnsItm);

	if(!isNullOrEmpty(arSavedSearchResultsItm)){

		//Build the result set object lookup and fill the Array Maps
		for (var j = 0 ; j < arSavedSearchResultsItm.length; j++){

			var obj = new Object();
			obj.id = arSavedSearchResultsItm[j].getValue('internalid');
			obj.name = arSavedSearchResultsItm[j].getValue('name');
			obj.locavecost = arSavedSearchResultsItm[j].getValue('locationaveragecost');
			obj.invlocation = arSavedSearchResultsItm[j].getValue('inventorylocation');



			arrResultObjectArray.push(obj);
			arrIdObjectArray.push(obj.id);
		}

		objItemResult.ids = arrIdObjectArray;
		objItemResult.obj = arrResultObjectArray;

		nlapiLogExecution("DEBUG", "func: getItemResultObj : arrResultObjectArray: length", arrResultObjectArray.length);

		return objItemResult;

	}else{

		nlapiLogExecution("DEBUG", "func: getItemResultObj : arrResultObjectArray: length", "null");
		return null;
	}


}



function getItemSublist(){

	var arrItems = new Array();
	var intGetPOlineCount = nlapiGetLineItemCount('item');

	for(var i = 1; i <= intGetPOlineCount; i++){

		var idItemId = nlapiGetLineItemValue('item','item', i);
		var strItemType = nlapiGetLineItemValue('item','itemtype', i);

		if(strItemType == 'InvtPart'){
			nlapiLogExecution("DEBUG", "getItemSublist", idItemId);
			arrItems.push(idItemId);
		}

	}

	nlapiLogExecution('DEBUG', "func: getItemSublist: Item Sublist ", arrItems.length);
	return arrItems;
}


function getItemSublistAS(itemReceipt){

	var arrItems = new Array();
	var intGetPOlineCount = itemReceipt.getLineItemCount('item');

	for(var i = 1; i <= intGetPOlineCount; i++){

		var idItemId =  itemReceipt.getLineItemValue('item','item', i);
		var strItemType =  itemReceipt.getLineItemValue('item','itemtype', i);

		if(strItemType == 'InvtPart'){
			//nlapiLogExecution("DEBUG", "getItemSublist", idItemId);
			arrItems.push(idItemId);
		}

	}

	nlapiLogExecution('DEBUG', "func: getItemSublistAS: Item Sublist ", arrItems.length);
	return arrItems;
}


function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}



function createUpdate_VendorInterCOPriceBS(type){
	//
//		if(false){
//		nlapiLogExecution("DEBUG", "func: createUpdate_VendorInterCOPriceBS", nlapiGetRecordId());
	//
	//
//		//Check the Item Receipt
//		var idPriceLevel = 1;
//		var idCurrency = nlapiGetFieldValue('currency');
//		var arrItemArray = getItemSublist();
	//
//		//Load Vendors to be updated/created
//		var idIRLocation = nlapiGetFieldValue('location');
//		var objVendorList = getVendorLocCustomerMap(idIRLocation); 									//Vendors
//		var objItemAvePriceLoc = getItemLocAveCost(arrItemArray, idCurrency, idPriceLevel, idIRLocation);			//Items LocationAverage Price
	//
	//
//		var arrItems = objItemAvePriceLoc.ids;
//		var arrItemsObj = objItemAvePriceLoc.obj;
	//
//		var arrVendors = objVendorList.ids;
//		var arrVendorsObj = objVendorList.obj;
	//
//
//		if(!isNullOrEmpty(objItemAvePriceLoc)){
//			//setItemRecord(objSearchResultItem, objVendorList);
//
//			var intIRItemCount = nlapiGetLineItemCount('item')
//			for(var o = 1 ; o <= intIRItemCount; o++){
//
//				var intItemId  = nlapiGetLineItemValue('item','item',o);
//
//				var ItemRecord = nlapiLoadRecord("inventoryitem", intItemId);
//				var itemVendorCount = ItemRecord.getLineItemCount("itemvendor");
//				var updateObject = new Array();
//
//				for(var q=0; q< arrVendors.length; q++){
//
//					var thisVendor = arrVendors[q];
//					var itemVendorCount = ItemRecord.getLineItemCount("itemvendor");
//					var found = false;
//					var objVendorMatch = new Object();
//					var vendorline = -1;
//
//					for(var r=1;r<=itemVendorCount;r++){
//
//						var idVendor = ItemRecord.getLineItemValue("itemvendor", "vendor" , r);
//
//						if(thisVendor == idVendor){
//							found = true; //nlapiLogExecution("AUDIT", "Finding Vendor Match: " + thisVendor, "found");
//							vendorline = r;
//							//nlapiGetCurrentLineItemIndex('itemvendor','index')
//						}
//					}
//
//					objVendorMatch.exist = found;
//					objVendorMatch.item = ItemRecord.getId();
//					objVendorMatch.itemname = ItemRecord.getFieldValue("itemid");
//					objVendorMatch.vendorid = thisVendor;
//					objVendorMatch.vendorname = arrVendorsObj[q].vendorname;
//					objVendorMatch.vendormarkup = arrVendorsObj[q].markup;
//					objVendorMatch.vendorline = vendorline;
//					objVendorMatch.vendorcurrency = arrVendorsObj[q].currency;
//
//					var itemIndex = arrItems.indexOf(intItemId);
//					if(itemIndex >= 0){
//						objVendorMatch.locavecost =  arrItemsObj[itemIndex].locavecost;
//					}
//
//					if(objVendorMatch.exist){
//						ItemRecord.removeLineItem('itemvendor', objVendorMatch.vendorline, true);
//						ItemRecord.selectNewLineItem('itemvendor');
//
//						var currAveCost = converCurrency( objVendorMatch.locavecost, objVendorMatch.vendorcurrency);
//
//						ItemRecord.setCurrentLineItemValue("itemvendor", "purchaseprice", currAveCost);
//						ItemRecord.setCurrentLineItemValue("itemvendor", "vendor", objVendorMatch.vendorid );
//						ItemRecord.commitLineItem("itemvendor");
//					}else{
//						ItemRecord.selectNewLineItem("itemvendor");
//						ItemRecord.setCurrentLineItemValue("itemvendor", "vendor", objVendorMatch.vendorid );
//
//						var currAveCost = converCurrency( objVendorMatch.locavecost, objVendorMatch.vendorcurrency);
//
//						ItemRecord.setCurrentLineItemValue("itemvendor", "purchaseprice", currAveCost);
//						ItemRecord.commitLineItem("itemvendor");
//					}
//
//					nlapiLogExecution("DEBUG", "Finding Vendor Match: item: " + objVendorMatch.item + " vendor: " + thisVendor, JSON.stringify(objVendorMatch));
//					updateObject.push(objVendorMatch);
//				}
//
//				ItemRecord.setFieldValue('custitem_erp_interco_json',JSON.stringify(updateObject));
//				ItemRecord.setFieldValue('custitem_erp_last_interco_ir',objVendorMatch.itemreceipt);
//				ItemRecord.setFieldValue('custitem_ec_intercompany_price_updated', nlapiDateToString(new Date(), "datetimetz"));
//
//				var submitid = nlapiSubmitRecord(ItemRecord);
//				nlapiLogExecution("DEBUG", "Finding Vendor Match: Submit", submitid);
//
//
////				for(var p=1; p<=itemVendorCount; p++){
	////
////					var idVendor = ItemRecord.getLineItemValue("itemvendor", "vendor" , p);
////
////					var itemIndex = arrItems.indexOf(intItemId);
////
////
////					if(vendorIndex >= 0){
//////						nlapiLogExecution("AUDIT", "TEST idVendor markup", arrVendorsObj[vendorIndex].markup);
////					}
////
////					if(itemIndex >= 0){
//////						nlapiLogExecution("AUDIT", "TEST itemIndex avg", arrItemsObj[itemIndex].salesprice);
////					}
////
////				}
//			}
//		}
//		}
	}
