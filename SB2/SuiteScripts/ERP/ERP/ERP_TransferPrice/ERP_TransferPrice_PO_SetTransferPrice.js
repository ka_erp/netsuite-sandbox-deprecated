/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       23 Sep 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 *
 */


function main_TransferPricePOSetTransferPrice_BS(type){
	
	if(type == 'delete'){return;}
	

	nlapiLogExecution("AUDIT","PURCHASE ORDER", nlapiGetRecordId());

	var ordStatus = nlapiGetFieldValue('orderstatus');
	
	nlapiLogExecution("AUDIT","PURCHASE ORDER", ordStatus);

	if(ordStatus == 'A' || ordStatus == 'B'){

		//Get Valid forms
		var testscriptconfig 	= 	SCRIPTCONFIG.getScriptConfigValue('Transfer Price: Enable FCA');
		var customForms 		= 	SCRIPTCONFIG.getScriptConfigValue('Transfer Price: Intercompany Forms');
		customForms 			= 	customForms.split(',');

		//Valid Price Method
		var pricingmethod 	= 	SCRIPTCONFIG.getScriptConfigValue('Transfer Price: Pricing Method'); //Acceptable //FCA
		
		nlapiLogExecution("AUDIT","PURCHASE ORDER: pricingmethod", pricingmethod);									
			
		var idCustomform = nlapiGetFieldValue('customform');				
		
		nlapiLogExecution("AUDIT","PURCHASE ORDER: idCustomform", idCustomform);	
		
		if(customForms.indexOf(idCustomform) > -1){	

				nlapiLogExecution("AUDIT","PURCHASE ORDER", nlapiGetRecordId());

				var bPOError = false;
				var idPriceLevel = 1;
				var idCurrency = nlapiGetFieldValue('currency');
				var priceMethod = nlapiGetFieldValue('custbody_erp_price_method'); //1 - Standard , 2 - Base Price

				// Decide pricing
				
				if(priceMethod == 1){
					
					nlapiLogExecution("AUDIT","PURCHASE ORDER", 1);
					setItemMemo();						
				
				}else if(priceMethod == 2){

					nlapiLogExecution("AUDIT","PURCHASE ORDER", nlapiGetContext().getExecutionContext()  + " " + 2.0);
					//if(nlapiGetContext().getExecutionContext() == 'userinterface'){
						
						nlapiLogExecution("AUDIT","PURCHASE ORDER: pricingmethod", pricingmethod + ' ' + 2.1);
																										
						if(pricingmethod ==  'PRICELIST'){
							nlapiLogExecution("AUDIT","PURCHASE ORDER", 2.2);
							setItemTransferPrice_PriceList(priceMethod);
						}
						else if(pricingmethod ==  'FCA'){
							nlapiLogExecution("AUDIT","PURCHASE ORDER", 2.3);
							setItemTransferPrice();
						}
					//}
					
				}else if(priceMethod == 3){
					
					nlapiLogExecution("AUDIT","PURCHASE ORDER", 3);
					if(pricingmethod ==  'PRICELIST'){
						setItemTransferPrice_PriceList(priceMethod);
					}
					
				}
		}
	}
}


function setItemTransferPrice_PriceList(price_method){

	nlapiLogExecution("AUDIT","setItemTransferPrice_PriceList", 'setItemTransferPrice_PriceList');

	try{

		//Config		
		var priceprecision 	= 	SCRIPTCONFIG.getScriptConfigValue('Transfer Price: Price Precision');
		priceprecision = parseInt(priceprecision);
		
		//Item and Item Price List
		var arrItemArray 			=		getPOItemArray(); 															//Get all the items
		var objItemPriceResults 	= 		getTransferPrice_ItemPriceList(arrItemArray); 								//store the price details into an object
		var intGetPOlineCount 		= 		nlapiGetLineItemCount('item');

		//Vendor		
		var idVendor 				= 		nlapiGetFieldValue('entity');
				
		//Markup
		var fMarkup					=		1;
		
		
		if(price_method ==  3){		//Check Markup Method // Regular	
			
			var strVendorRetailFormula 	= 		nlapiLookupField('vendor', idVendor, 'custentity_intercompany_markup_percent');
			
			if(strVendorRetailFormula.indexOf("%") > -1){
				strVendorRetailFormula = parseFloat(strVendorRetailFormula); 
				strVendorRetailFormula = strVendorRetailFormula / 100;
				strVendorRetailFormula = 1 + strVendorRetailFormula;
				fMarkup =	strVendorRetailFormula;  
			}		
		}else if (price_method == 2){			//Check Markup Method // Drop Ship from Factory
			
			var strVendorRetailFormula 	= 		nlapiLookupField('vendor', idVendor, 'custentity_intercompany_retail_percent');
			
			if(strVendorRetailFormula.indexOf("%") > -1){
				strVendorRetailFormula = parseFloat(strVendorRetailFormula); 
				strVendorRetailFormula = strVendorRetailFormula / 100;
				strVendorRetailFormula = 1 + strVendorRetailFormula;
				fMarkup =	strVendorRetailFormula;  
			}	else{
				strVendorRetailFormula = eval(strVendorRetailFormula); 			
				fMarkup =	strVendorRetailFormula;  
				
			}	
		}

		
		//Currency
		var toCurrency 			= 		nlapiGetFieldValue('currency');													//This is actually PO Currency
		var toCurrencyTxt 		= 		nlapiGetFieldText('currency');	
		
		fx.base = "USD";
		fx.rates = JSON.parse(getTransferPrice_CurrencyList2()); 
								
		nlapiLogExecution("AUDIT","setItemTransferPrice_PriceList: Rates", JSON.stringify(fx.rates));
							
		
			//Update Item Price		
			if(!isNullOrEmpty(objItemPriceResults)){
	
				var arrIdObjectArray 		= 	objItemPriceResults.ids;
				var arrResultObjectArray 	=   objItemPriceResults.objs;
				var arrCurrencyObjectArray 	=   objItemPriceResults.curr;
				                     
				nlapiLogExecution("AUDIT","objItemPriceResults", JSON.stringify(objItemPriceResults));
	
				//var currencyArray			= 	getTransferPrice_CurrencyList(arrCurrencyObjectArray); 
							
				for(var i = 1; i <= intGetPOlineCount; i++){
				
					
					var idItemId = nlapiGetLineItemValue('item','item', i);
					var itemIndex = arrIdObjectArray.indexOf(idItemId);
	
					if(itemIndex >= 0){
						
						var curr = arrResultObjectArray[itemIndex].currencyTxt;
						var currId = arrResultObjectArray[itemIndex].currency;
						var itemRate = arrResultObjectArray[itemIndex].baseprice;	//TODO add logic to set 0 if not found
						
						
						var newPrice = fx(itemRate).from(curr).to(toCurrencyTxt);
						newPrice = newPrice * strVendorRetailFormula;
						newPrice = newPrice.toFixed(priceprecision);
						
						nlapiLogExecution("AUDIT","objItemPriceResults:newPrice", newPrice);
						
						var rateforone = fx(1).from(curr).to(toCurrencyTxt); 
						var memo = "currency fx: "  + curr +  " > " + toCurrencyTxt +": "  + rateforone;
						
						nlapiSetLineItemValue('item','rate', i, newPrice);
						nlapiSetLineItemValue('item','custcol_erp_to_memo', i, memo);
						
						nlapiSetLineItemValue('item','custcol_erp_trnprc_base_price', i, itemRate);
						nlapiSetLineItemValue('item','custcol_erp_trnprc_markup_rule', i, strVendorRetailFormula);
						nlapiSetLineItemValue('item','custcol_erp_trnprc_fxrate', i, rateforone);
						nlapiSetLineItemValue('item','custcol_erp_trnprc_base_currency', i, currId);
						
						
						
					}else{
						
						
						
						var rateforone = fx(1).from('USD').to(toCurrencyTxt); 
						var memo = "currency fx: "  + 'USD' +  " > " + toCurrencyTxt +": "  + rateforone;
						memo = memo + " /n Price not found"; 

						nlapiSetLineItemValue('item','rate', i, 0);
						nlapiSetLineItemValue('item','custcol_erp_to_memo', i, memo);
						
						nlapiSetLineItemValue('item','custcol_erp_trnprc_base_price', i, 0);
						nlapiSetLineItemValue('item','custcol_erp_trnprc_markup_rule', i, strVendorRetailFormula);
						nlapiSetLineItemValue('item','custcol_erp_trnprc_fxrate', i, rateforone);
						nlapiSetLineItemValue('item','custcol_erp_trnprc_base_currency', i, '');
						
					}
					
					//convertPrice(currencyObject); 
				}
												
			}else{
				
				for(var i = 1; i <= intGetPOlineCount; i++){
					
					var rateforone = fx(1).from('USD').to(toCurrencyTxt); 
					var memo = "currency fx: "  + 'USD' +  " > " + toCurrencyTxt +": "  + rateforone;
					memo = memo + " /n Price not found"; 
					
					nlapiSetLineItemValue('item','rate', i, 0);
					nlapiSetLineItemValue('item','custcol_erp_to_memo', i, memo);
					
					nlapiSetLineItemValue('item','custcol_erp_trnprc_base_price', i, 0);
					nlapiSetLineItemValue('item','custcol_erp_trnprc_markup_rule', i, strVendorRetailFormula);
					nlapiSetLineItemValue('item','custcol_erp_trnprc_fxrate', i, rateforone);
					nlapiSetLineItemValue('item','custcol_erp_trnprc_base_currency', i, '');
				}
				
				
			}
		
		
		}
		catch(ex){

			nlapiLogExecution("ERROR","setItemTransferPrice_PriceList:ERROR",ex.toString());
		}

}


function setItemMemo(){

	var bError = false;
	var intGetPOlineCount = nlapiGetLineItemCount('item');

	var arrItemArray 			=		getPOItemArray(); 												//Get all the items
	var objItemPriceResults 	= 		getItemFCAResultObj(arrItemArray); 								//store the price details into an object


	if(!isNullOrEmpty(objItemPriceResults)){

		var arrIdObjectArray 		= 	objItemPriceResults.ids;
		var arrResultObjectArray 	=   objItemPriceResults.objs;


		for(var i = 1; i <= intGetPOlineCount; i++){

			var idItemId = nlapiGetLineItemValue('item','item', i);

			var itemIndex = arrIdObjectArray.indexOf(idItemId);

			if(itemIndex >= 0){

				var sourceRate 				=  	arrResultObjectArray[itemIndex].salesprice;
				var sourceCurrency 			=  	arrResultObjectArray[itemIndex].currencyTxt;

			}


			var intQty = nlapiGetLineItemValue('item','quantity', i);
			var taxCode = nlapiGetLineItemValue('item','taxcode', i);
			var bOverride = nlapiGetLineItemValue('item','custcol_erp_transferprice_validation', i);
			var fRetailPrice = nlapiGetLineItemValue('item','custcol_erp_retail_price',i);
			var fRetailValue  = nlapiGetLineItemValue('item','custcol_ka_po_retail_value',i);
			var idRetailCurrency = nlapiGetLineItemValue('item','custcol_erp_posp_retail_currency',i);

//			Retail Currency	 		2064	List/Record	Currency	 	 	Y
//			64	Retail Price	 	custcol_erp_retail_price	2602	Currency	 	 	 	 	 	 	 	 	 	 	Y
//			10	Retail Value	 	custcol_ka_po_retail_value


			nlapiRemoveLineItem('item', i);
			nlapiInsertLineItem('item', i);



			nlapiSetLineItemValue('item','item', i, idItemId);
			nlapiSetLineItemValue('item','quantity', i, intQty);
			nlapiSetLineItemValue('item','taxcode', i, taxCode);
			nlapiSetLineItemValue('item','custcol_erp_retail_price', i, fRetailPrice);
			nlapiSetLineItemValue('item','custcol_ka_po_retail_value', i, fRetailValue);
			nlapiSetLineItemValue('item','custcol_erp_posp_retail_currency', i, idRetailCurrency);
			nlapiSetLineItemValue('item','custcol_erp_transferprice_validation', i, bOverride);

			var memo = 'Calculation: \n Standard Pricing is used. \n Last FCA:' + sourceRate + ' ' + sourceCurrency + '\nSee Item - Vendor Price';

			nlapiSetLineItemValue('item','custcol_erp_to_memo', i, memo);

		}
	}

}

function setItemTransferPrice(){

	//function is using FCA calculation from the item record

	try{

		var arrItemArray 			=		getPOItemArray(); 												//Get all the items
		var objItemPriceResults 	= 		getItemFCAResultObj(arrItemArray); 								//store the price details into an object
		var intGetPOlineCount 		= 		nlapiGetLineItemCount('item');

		var idVendor 				= 		nlapiGetFieldValue('entity');
		var strVendorRetailFormula 	= 		nlapiLookupField('vendor', idVendor, 'custentity_intercompany_retail_percent');
		strVendorRetailFormula  	= 		eval(strVendorRetailFormula);


		var idCurrencyPO 			= 		nlapiGetFieldValue('currency');									//This is actually PO Currency
		var strCurrencyPOTxt 		= 		nlapiGetFieldText('currency');
		var objSourceCurrency		= 		new  Object();
		objSourceCurrency.id		=		idCurrencyPO;
		objSourceCurrency.name		= 		strCurrencyPOTxt;


		if(!isNullOrEmpty(objItemPriceResults)){

			var arrIdObjectArray 		= 	objItemPriceResults.ids;
			var arrResultObjectArray 	=   objItemPriceResults.objs;
			var arrCurrencyObjectArray 	=   objItemPriceResults.curr;

			var currencyObject		= 		createCurrencyExchange(objSourceCurrency, arrCurrencyObjectArray);


			nlapiLogExecution('DEBUG', 'currencyObject', JSON.stringify(currencyObject));

			var bError = false;
			var intGetPOlineCount = nlapiGetLineItemCount('item');

			for(var i = 1; i <= intGetPOlineCount; i++){

				var idItemId = nlapiGetLineItemValue('item','item', i);
				var itemIndex = arrIdObjectArray.indexOf(idItemId);

				if(itemIndex >= 0){

					var sourceRate 				=  	arrResultObjectArray[itemIndex].salesprice;
					var sourceCurrency 			=  	arrResultObjectArray[itemIndex].currency;


					nlapiLogExecution('AUDIT', 'PO Rate', sourceCurrency + ":" +  idCurrencyPO);



					var rateObj 				=	getRate(sourceRate, sourceCurrency, idCurrencyPO, currencyObject, strVendorRetailFormula);

					var rate = rateObj.rate;
					var memo = rateObj.memo;

					nlapiSetLineItemValue('item','rate', i, rate);
					nlapiSetLineItemValue('item','custcol_erp_to_memo', i, memo);

//					var rate = convertCurrency(fFCA, idFCACurrency, currencyObject);
//
//
//
//
//					var newrate = calculateRate(fFCA, idFCACurrency, idVendorCurrency, strVendorRetailFormula);
//					var memo = getRetailMemo(fFCA, '', newrate[0], '', strVendorRetailFormula, newrate[1]);



//
//


				}
			}
		}

	}catch(ex){

		nlapiLogExecution('ERROR', 'func:setItemTransferPrice', ex.toString());
	}
}
