/**
 *	File Name		:	ERP_3PL_IntegrationLogs_TransferOrder_UE.js
 *	Function		:	Displays a sublist of Integration logs 
 *						(this is a workaround because while Custom Sublist can be applied to SO and PO, it cannot be applied to TO)
 * 	Remarks			:	
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{
	var LOGS_SEARCH = SCRIPTCONFIG.getScriptConfigValue('WTKA Integration Logs: Saved Search');
	var LOGS_SUBTAB = SCRIPTCONFIG.getScriptConfigValue('WTKA Integration Logs: ERP Integration Subtab');
	
	var TEXT_MAXLEN = 150;
}


/*
*	name: beforeLoad_IntegrationLogs
*	descr: 	1) Display Integration Logs sublist
*
*	author: apple.villanueva@kitandace.com
*	@param: {type} type of operation on the record
*   @param: {form} the form the scirpt is deployed on 
*   @param: {request} the request sent to the script
*
*/
function beforeLoad_IntegrationLogs(type, form, request){

	if(nlapiGetContext().getExecutionContext() == 'userinterface' && (type == 'edit' || type == 'view')){
		
		//Sublist
		var LogList = form.addSubList('custpage_list_integrationlogs', 'list', 'WTKA Integration Logs', LOGS_SUBTAB);
		LogList.addField('custpage_edit', 'text', 'Edit');
		LogList.addField('custpage_date', 'text', 'Date Created');
		LogList.addField('custpage_editype', 'text', 'EDI Type');
		LogList.addField('custpage_edinumber', 'text', 'EDI Number');
		LogList.addField('custpage_callstatus', 'checkbox', 'Call Status');
		LogList.addField('custpage_author', 'text', 'Author');
		LogList.addField('custpage_nstran', 'select', 'NetSuite Transaction', 'transaction');
		LogList.addField('custpage_edistatus', 'text', 'EDI Status');
		LogList.addField('custpage_jsonpayload', 'textarea', 'JSON Payload');
		LogList.addField('custpage_response', 'textarea', 'Response');
				
		//Get data from saved search
		var arResults;
		if(LOGS_SEARCH){
			var search = nlapiLoadSearch('', LOGS_SEARCH);
			search.addFilters([new nlobjSearchFilter('custrecord_transaction_source', 'custrecord_parentid', 'anyof', nlapiGetRecordId())]);
			
			var resultSet = search.runSearch();

			if(resultSet){
				arResults = getAllSearchResults(resultSet);
			}
		}
		
		//Populate sublist
		if(arResults){			
			for(var i = 0; i < arResults.length; i++){	
				LogList.setLineItemValue('custpage_edit', i+1, 
					'<a href="' 
					+ nlapiResolveURL('RECORD', 'customrecord_integrationlogs_child', arResults[i].getId(), 'EDIT') 
					+ '" target="_blank">Edit</a>');
				LogList.setLineItemValue('custpage_date', i+1, arResults[i].getValue('created'));
				LogList.setLineItemValue('custpage_editype', i+1, arResults[i].getText('custrecord_editype', 'custrecord_parentid'));
				LogList.setLineItemValue('custpage_edinumber', i+1, arResults[i].getText('custrecord_edinumber', 'custrecord_parentid'));
				LogList.setLineItemValue('custpage_callstatus', i+1, arResults[i].getValue('custrecord_edicallstatus', 'custrecord_parentid'));
				LogList.setLineItemValue('custpage_author', i+1, arResults[i].getValue('custrecord_author'));
				LogList.setLineItemValue('custpage_nstran', i+1, arResults[i].getValue('custrecord_netsuitetransaction'));
				LogList.setLineItemValue('custpage_edistatus', i+1, arResults[i].getText('custrecord_edistatus'));
				LogList.setLineItemValue('custpage_jsonpayload', i+1, getDisplayText(arResults[i].getValue('custrecord_payload')));
				LogList.setLineItemValue('custpage_response', i+1, getDisplayText(arResults[i].getValue('custrecord_response')));
			}		
		}
	}
}

function getDisplayText(stText){
	if(stText.length > TEXT_MAXLEN){
		stText = stText.substring(0, TEXT_MAXLEN-1) + '(more...)';
	}
	return stText;
}
