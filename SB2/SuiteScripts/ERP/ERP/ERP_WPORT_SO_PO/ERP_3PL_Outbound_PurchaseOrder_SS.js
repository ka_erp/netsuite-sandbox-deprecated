/**
 *	File Name		:	ERP_3PL_Outbound_PurchaseOrder_SC.js
 *	Function		:	Mass scheduler for Send to 3PL functionality for Inbound Factory PO
 * 	Remarks			:	
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{
	var STATUS_SEND = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Send to 3PL');
	var IFPO_SS		= SCRIPTCONFIG.getScriptConfigValue('IFPO: Send to 3PL: Scheduler Saved Search');
}

/**
 * @param {String} type Context Types: scheduled, ondemand, userinterface, aborted, skipped
 * @returns {Void}
 */
function scheduled_IFPOSendTo3PL(type) {
	
	nlapiLogExecution('debug', 'scheduled_IFPOSendTo3PL', '::::::::START::::::::');
	
	//Fetch POs to send to 3PL
	var arPOs = fetchPOToSend();
	
	//Send POs to ESB
	sendPOToExport(arPOs);
	
	nlapiLogExecution('debug', 'scheduled_IFPOSendTo3PL', '::::::::END::::::::');
	
}

/**
 * Returns the POs to send to 3PL
 * @param
 * @returns arResults
 */
function fetchPOToSend(){
	
	var arResults;
	
	//Get source of POs from saved search
	if(IFPO_SS){
		var search = nlapiLoadSearch('', IFPO_SS);
		var resultSet = search.runSearch();

		if(resultSet){
			arResults = getAllSearchResults(resultSet);
		}
	}
		
	return arResults;
}

/**
 * Triggers the ESB to query the POs
 * @param arPOs
 * @returns
 */
function sendPOForExport(arPOs){
	
	if(arPOs){
		for(var i = 0; i < arPOs.length; i++){
			//Update Send To 3PL Status field
			//This will trigger ERP_3PL_Outbound_MassExport_UE
			nlapiSubmitField('purchaseorder', arPOs[i].getValue('internalid'), 'custbody_export_3pl_status', STATUS_SEND);
			
			checkGovernance();
		}
	}
}

/*******************
 Governance Check
********************/
function checkGovernance(){
	if (!validUsageRemaining()) {
		yieldScript();
	} else {
		nlapiLogExecution('DEBUG', 'scheduled_IFPOSendTo3PL', 'Remaining usage = ' + nlapiGetContext().getRemainingUsage());
	}
}

function validUsageRemaining() {                                                 
	return nlapiGetContext().getRemainingUsage() >= 500;
}

function yieldScript() {
	var state = nlapiYieldScript();
	var message = 'Reason: ' + state.reason + ' Info: ' + state.information + ' Size: ' + state.size;

	if (state.status == 'FAILURE'){

		nlapiLogExecution('Debug', 'Sched Script', 'Failed to yield script; exiting. ' + message);
		nlapiLogExecution('Error', 'Sched Script', 'Failed to yield script; exiting. ' + message);

		throw nlapiCreateError('SCRIPT_ERROR', 'Failed to yield script; exiting. ' + message);

	} else if (state.status == 'RESUME'){

		nlapiLogExecution('Debug', 'Resuming script because of ' + message);
		nlapiLogExecution('Audit', 'Resuming script because of ' + message);

	}
}
