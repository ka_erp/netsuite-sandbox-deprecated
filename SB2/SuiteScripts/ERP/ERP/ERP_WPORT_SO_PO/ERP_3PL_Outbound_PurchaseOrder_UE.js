/**
 *	File Name		:	ERP_3PL_Outbound_PurchaseOrder_UE.js
 *	Function		:	Send to 3PL
 * 	Remarks			:	
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{
	var SHIPTO_FILTER = SCRIPTCONFIG.getScriptConfigValue('IFPO: Send to 3PL: Ship To Filter');
}

/*
*	name: beforeLoad_showButton
*	descr: 	1) Determine whether to show the button 'Send to 3PL'
*
*	author: apple.villanueva@kitandace.com
*	@param: {type} type of operation on the record
*   @param: {form} the form the script is deployed on 
*   @param: {request} the request sent to the script
*
*/
function beforeLoad_showButton(type, form, request){

	var recType = nlapiGetRecordType();

	if(recType == 'purchaseorder' && nlapiGetContext().getExecutionContext() == 'userinterface' && type == 'view'){
		
		var UserRole = nlapiGetContext().getRole();
		var formStatus = nlapiGetFieldValue('statusRef');

		var buttonDisplayCheck = 'F';

		//1) Determine whether to show the button 'Send to 3PL'
		if(formStatus){ //TODO - specify the status filter
		
			if(UserRole != 3){ //TODO - will this condition apply? // 3 = Administrator
			
				//Check if Ship To can display the 'Ship to 3PL' button				
				if(canSendTo3pl()){
					buttonDisplayCheck = 'T';
				}
				
			} else{
				buttonDisplayCheck = 'T';
			}
		}
				
		if(buttonDisplayCheck == 'T'){ 
			form.addButton('custpage_sendto3pl_btn', "Send to 3PL", /*'callButtonSuitelet_sendTo3pl()'*/''); //TODO - put client event //function is in ERP_3PL_Button_CL.js
			form.setScript('customscript_erp_3pl_button_cl');
		}
	}
}

/**
 * Determines if Send to 3PL button will be displayed given the Ship To value
 */
function canSendTo3pl(){
	
	//Get eligible Ship To from Script Config
	var arSCShipTo = [];
	if(SHIPTO_FILTER){
		arSCShipTo = SHIPTO_FILTER.split(',');
	}
	
	var stShipTo = nlapiGetFieldValue('shipto');
	if(stShipTo && arSCShipTo.indexOf(stShipTo) > -1){
		return true;
	}
	
	return false;
}
