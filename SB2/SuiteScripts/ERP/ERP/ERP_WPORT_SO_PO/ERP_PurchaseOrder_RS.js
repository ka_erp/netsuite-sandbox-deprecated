/**
 *	File Name		:	ERP_PurchaseOrder_RS.js
 *	Function		:	
 * 	Remarks			:	
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

function getPurchaseOrderObject(dataIn)  // REST API Inbound call for generating TO Canonical data object model
{
	nlapiLogExecution('DEBUG', 'dataIn', JSON.stringify(dataIn));
	var type 			= 'purchaseorder';
	InboundOrderNumber  = dataIn.id;
	InboundOrderId 		= FetchOrderId(type, InboundOrderNumber);
	var status 			= Validate940(type, InboundOrderId); 
	if(status)
	{
		var record 	 	= nlapiLoadRecord(type, InboundOrderId);
		var CanonObject = generatePOCanonObject(record);
		nlapiLogExecution('DEBUG', 'Canonical Object', JSON.stringify(CanonObject)); 
		//var purchaseOrderLogs 	= LogCreation(2, 1, InboundOrderId, 0, dataIn, 1, CanonObject, 'T'); //2 - Outbound Call, 1 - Inbound call
		
		//For additional logging - START
		if(CanonObject.status == 'Exception'){
			//Exception occurred
			LogCreation(2, 1, InboundOrderId, 0, dataIn, 3, CanonObject, 'T'); //3-Exception
		} else {
			//Success Logging
			LogCreation(2, 1, InboundOrderId, 0, dataIn, 1, CanonObject, 'T'); //1-Success
		}
		//For additional logging - END
		
		return CanonObject;
	}
	else
	{
		var errorStatus = (ErrorObj.status == "Error") ? 2 : 3; // 2- Error, 3-Exception
		
		//For additional logging - START
		//Commented out so that invalid order will be logged also
		//if(invalid_orderid == 'F')
		//{
			var orderid 	= (InboundOrderId != null) ? InboundOrderId : 0;
			var ErrorLogs 	= LogCreation(2, 1, orderid, 0, dataIn, errorStatus, ErrorObj, 'F'); //2 - Outbound Call, 1 - Inbound call
			nlapiLogExecution('DEBUG', 'Error Object', JSON.stringify(ErrorObj));
		//}
		//For additional logging - END
		
		//Send Error Mail
		send3PLIntegrationErrorMail(dataIn, 'Outbound 940 call processing failure', InboundOrderNumber);
		
		return ErrorObj;
	}
}

function generatePOCanonObject(record) //Generation of Canonical data object model for PO
{
	//TODO
	try{
		
		var purchaseOrderSchema = new Object();
		
	} catch(po_error){
        nlapiLogExecution('DEBUG','error message',po_error.message);
		ErrorObj.editype 	 				= 940;
		ErrorObj.orderid 					= recordId;
		ErrorObj.messages[0] 				= new Object();
		ErrorObj.messages[0].messagetype 	= "Purchase Order Error";
		ErrorObj.messages[0].message 	 	= "Error Details: " + po_error.message;
		
		//For additional logging - START
		ErrorObj.status = 'Exception';
		//For additional logging - END
		
		purchaseOrderSchema 				= ErrorObj;
	}
	
	return purchaseOrderSchema;
}

