/**
 *	File Name		:	ERP_SalesOrder_RS.js
 *	Function		:	
 * 	Remarks			:	
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

function getSalesOrderObject(dataIn) // REST API Inbound call for generating SO Canonical data object model
{
	nlapiLogExecution('DEBUG', 'dataIn', JSON.stringify(dataIn));
	var type 			= 'salesorder';
	InboundOrderNumber 	= dataIn.id;
	InboundOrderId 		= FetchOrderId(type, InboundOrderNumber);
	var status 			= Validate940(type, InboundOrderId); 
	if(status)
	{
		var record 	 	= nlapiLoadRecord(type, InboundOrderId);
		var CanonObject = generateSOCanonObject(record);
		nlapiLogExecution('DEBUG', 'Object', JSON.stringify(CanonObject));
		//var salesorderLogs 	= LogCreation(2, 1, InboundOrderId, 0, dataIn, 1, CanonObject, 'T'); //2 - Outbound Call, 1 - Inbound call
		
		//For additional logging - START
		if(CanonObject.status == 'Exception'){
			//Exception occurred
			LogCreation(2, 1, InboundOrderId, 0, dataIn, 3, CanonObject, 'T'); //3-Exception
		} else {
			//Success Logging
			LogCreation(2, 1, InboundOrderId, 0, dataIn, 1, CanonObject, 'T'); //1-Success
		}
		//For additional logging - END
		
		return CanonObject;
	}
	else
	{
		var errorStatus = (ErrorObj.status == "Error") ? 2 : 3; // 2- Error, 3-Exception
		
		//For additional logging - START
		//Commented out so that invalid order will be logged also
		//if(invalid_orderid == 'F')
		//{
			var orderid 	= (InboundOrderId != null) ? InboundOrderId : 0;
			var ErrorLogs 	= LogCreation(2, 1, orderid, 0, dataIn, errorStatus, ErrorObj, 'F'); //2 - Outbound Call, 1 - Inbound call
			nlapiLogExecution('DEBUG', 'Error Object', JSON.stringify(ErrorObj));
		//}	
		//For additional logging - END
				
		//Send Error Mail
		send3PLIntegrationErrorMail(dataIn, 'Outbound 940 call processing failure', InboundOrderNumber);
		
		return ErrorObj;
	}
}

function generateSOCanonObject(record) //Generation of Canonical data object model for SO
{
	try
	{
		var recordId 	= record.getId();
		var customerId 	= record.getFieldValue('entity');

		var carriercode = (record.getFieldValue('shipcarrier') 	!= null) ? 	record.getFieldValue('shipcarrier') : record.getFieldValue('carrier');
                if (carriercode == 'nonups') { carriercode = 'UPS' };
		var carriername = record.getFieldText('shipmethod');
		carriername 	= (carriername.match(/FedEx/gi) != null) ? 'FedEx' : (carriername.match(/UPS/gi) != null) ? 'UPS' : 'FedEx'; //Default to FedEx if Ship method is not set

		var fromLocationData  	= (record.getFieldValue('location') != null) ? getLocationData(record.getFieldValue('location')) : "";
		var customerData 	  	= getCustomerData(customerId);
		var shipData 	      	= getShippingData(record);
		var billData          	= getBillingData(record);

		var interco				= record.getFieldValue('intercotransaction');
		var icto				= (interco != null && interco.length > 0) 	? true : false;
		var locale 			  	= (readValue(fromLocationData.country) != "") ? readValue(fromLocationData.country).toLowerCase() : "";
		var customerEmail	  	= record.getFieldValue('email');
		
		if(!icto)
		{
			var paymentid 			= "";
			var authUrl 			= "";
			var paymentmethod 		= "";
			for(var k=0; k < record.getLineItemCount('paymentevent'); k++)
			{
				paymentid = (paymentid == "") ? record.getLineItemValue('paymentevent', 'nseqnum', 	k+1) : paymentid + ', ' + record.getLineItemValue('paymentevent', 'nseqnum', 	k+1);
				authUrl = record.getLineItemValue('paymentevent', 'viewurl', 	k+1);
			}

			if(readValue(record.getFieldValue('paymentmethod')) != "")
			{
				paymentmethod = record.getFieldText('paymentmethod');
				if(Math.abs(record.getFieldValue('giftcertapplied')) > 0)
				{
					paymentmethod += '/Gift Card';
				}
			}
			else if(Math.abs(record.getFieldValue('giftcertapplied')) > 0)
			{
				paymentmethod = 'Gift Card';
			}
		}
		
		var ordStatus = record.getFieldValue('status');
		var orderStatus;
		switch(ordStatus)
		{
			case 'Pending Fulfillment':
				orderStatus = "Created";
				break;
			case 'Pending Billing':
				orderStatus = "Shipped";
				break;
			case 'Billed':
				orderStatus = "Completed";
				break;
			case 'Partially Fulfilled':
				orderStatus = "Released";
				break;
			default:
				orderStatus = "";
		}
		
		var salesOrderSchema 									= new Object();
		salesOrderSchema.order									= new Object();
		salesOrderSchema.order.orderHeader 						= new Object();
		salesOrderSchema.order.orderHeader.orderId 				= readValue(record.getFieldValue('tranid'));
		salesOrderSchema.order.orderHeader.parentOrderNumber 	= "";
		salesOrderSchema.order.orderHeader.childOrderNumber 	= "";
		salesOrderSchema.order.orderHeader.childSourceSystem 	= (icto) ? "Netsuite" 	: "Hybris";
		salesOrderSchema.order.orderHeader.orderTransactionDt 	= readValue(record.getFieldValue('trandate'));
		salesOrderSchema.order.orderHeader.orderStatus 			= (icto) ? orderStatus 	: "Completed";
		salesOrderSchema.order.orderHeader.orderClosedDt 		= "";
		salesOrderSchema.order.orderHeader.orderCreateTs 		= readValue(record.getFieldValue('createddate'));
		salesOrderSchema.order.orderHeader.orderLastModifiedTs 	= readValue(record.getFieldValue('lastmodifieddate'));
		salesOrderSchema.order.orderHeader.transactionType 		= "Transfer Order"; 
		salesOrderSchema.order.orderHeader.exchangeRate 		= parseFloat(record.getFieldValue('exchangerate'));
		salesOrderSchema.order.orderHeader.isGiftOrder 			= false;
		salesOrderSchema.order.orderHeader.giftMessage 			= "";
		salesOrderSchema.order.orderHeader.orderHeaderMemo 		= readValue(record.getFieldValue('memo'));
		salesOrderSchema.order.orderHeader.paymentStatus 		= "Pending";
		salesOrderSchema.order.orderHeader.isFulfilled 			= false;
		salesOrderSchema.order.orderHeader.totalQuantityOrdered = totalquantity;
		salesOrderSchema.order.orderHeader.totalAmountOrdered 	= parseFloat(record.getFieldValue('total'));
		
		salesOrderSchema.order.orderHeader.sellingLocation 				= new Object();
		salesOrderSchema.order.orderHeader.sellingLocation.locationCode = readValue(fromLocationData.externalid);
		salesOrderSchema.order.orderHeader.sellingLocation.locationName = readValue(fromLocationData.name);
		salesOrderSchema.order.orderHeader.sellingLocation.addressLine1 = readValue(fromLocationData.address1); 
		salesOrderSchema.order.orderHeader.sellingLocation.addressLine2 = readValue(fromLocationData.address2); 
		salesOrderSchema.order.orderHeader.sellingLocation.city 		= readValue(fromLocationData.city);
		salesOrderSchema.order.orderHeader.sellingLocation.province 	= readValue(fromLocationData.state);
		salesOrderSchema.order.orderHeader.sellingLocation.country 		= readValue(fromLocationData.country);
		salesOrderSchema.order.orderHeader.sellingLocation.mailingCode 	= readValue(fromLocationData.zip);
		salesOrderSchema.order.orderHeader.sellingLocation.phoneNumber 	= readValue(fromLocationData.addrphone); 
		salesOrderSchema.order.orderHeader.sellingLocation.latitude 	= 0.00000; 
		salesOrderSchema.order.orderHeader.sellingLocation.longitude 	= 0.00000; 
		salesOrderSchema.order.orderHeader.sellingLocation.openingDate 	= readValue(fromLocationData.custrecord_ka_store_opening_date);
		salesOrderSchema.order.orderHeader.sellingLocation.closedDate 	= "";
		salesOrderSchema.order.orderHeader.sellingLocation.locationType = readValue(fromLocationData.custrecord_ra_loctype);
		salesOrderSchema.order.orderHeader.sellingLocation.storeHours 	= "";
		salesOrderSchema.order.orderHeader.sellingLocation.locStatus 	= (fromLocationData.isinactive == 'F') ? 'Open' : 'Closed'; 
		salesOrderSchema.order.orderHeader.sellingLocation.DCLocCode 	= fromLocationData.externalid;

		salesOrderSchema.order.orderHeader.billingCustomer 					  	= new Object();
		salesOrderSchema.order.orderHeader.billingCustomer.customerId 		  	= customerId;
		salesOrderSchema.order.orderHeader.billingCustomer.customerEmail 	  	= readValue(customerEmail);
		salesOrderSchema.order.orderHeader.billingCustomer.honorific 		  	= readValue(customerData.salutation);
		salesOrderSchema.order.orderHeader.billingCustomer.firstName 		  	= readValue(customerData.firstname);
		salesOrderSchema.order.orderHeader.billingCustomer.lastName 			= readValue(customerData.lastname);
		salesOrderSchema.order.orderHeader.billingCustomer.customerPhoneNumber 	= readValue(customerData.phone);
		salesOrderSchema.order.orderHeader.billingCustomer.gender 				= "";
		salesOrderSchema.order.orderHeader.billingCustomer.birthDt 				= "";
		salesOrderSchema.order.orderHeader.billingCustomer.locale 				= locale;
		
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress 					 	= new Object();
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.billingAddressId 	 	= record.getFieldValue('billingaddress_key');
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.billingFirstName	 	= readValue(customerData.firstname);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.billingLastName 	 	= readValue(customerData.lastname);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.billingEmail 		 	= readValue(customerEmail);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.billingAltEmail 	 	= "";
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.billingAddressLine1 	= readValue(billData.billaddr1);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.billingAddressLine2 	= readValue(billData.billaddr2);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.poBox 				= "";
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.cityName				= readValue(billData.billcity);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.postalCode 		 	= readValue(billData.billzip);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.provinceName 		 	= readValue(billData.billstate);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.provinceAltName 	 	= "";
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.countryName 		 	= readValue(billData.billcountry);
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.countryAltName 	 	= "";
		salesOrderSchema.order.orderHeader.billingCustomer.billingAddress.inActiveDate 		 	= "";
		
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress 						= new Object();
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.shippingAddressId 	= record.getFieldValue('shippingaddress_key');
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.recipientFirstName 	= readValue(customerData.firstname);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.recipientLastName 	= readValue(customerData.lastname);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.recipientEmail 		= readValue(customerEmail);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.recipientAltEmail 	= "";
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.recipientPhoneNumber = readValue(shipData.shipphone);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.shippingAddressLine1 = readValue(shipData.shipaddr1);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.shippingAddressLine2 = readValue(shipData.shipaddr2);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.poBox 				= ""
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.cityName 			= readValue(shipData.shipcity);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.postalCode 			= readValue(shipData.shipzip);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.provinceName 		= readValue(shipData.shipstate);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.provinceAltName 		= "";
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.countryName 			= readValue(shipData.shipcountry);
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.countryAltName 		= "";
		salesOrderSchema.order.orderHeader.billingCustomer.shippingAddress.inActiveDate 		= "";
		
		if(!icto)
		{
			salesOrderSchema.order.orderHeader.paymentInfo 				= new Object();
			salesOrderSchema.order.orderHeader.paymentInfo.paymentId 	= (paymentid != "") 	? paymentid 	: null;
			salesOrderSchema.order.orderHeader.paymentInfo.tenderType 	= (paymentmethod != "") ? paymentmethod : "";
			salesOrderSchema.order.orderHeader.paymentInfo.currencyCode = readValue(record.getFieldValue('currencyname'));
			salesOrderSchema.order.orderHeader.paymentInfo.authUrl 		= (authUrl != "") 		? authUrl 		: null;
		}
		else
		{
			salesOrderSchema.order.orderHeader.paymentInfo 				= new Object();
			salesOrderSchema.order.orderHeader.paymentInfo.paymentId 	= null;
			salesOrderSchema.order.orderHeader.paymentInfo.tenderType 	= "Promise To Pay";
			salesOrderSchema.order.orderHeader.paymentInfo.currencyCode = readValue(record.getFieldValue('currencyname'));
			salesOrderSchema.order.orderHeader.paymentInfo.authUrl 		= null;
		}
		salesOrderSchema.order.orderDetail = new Array();
		var i=0;
		var totalItemsShipped = 0;
		for(var j=0; j<record.getLineItemCount('item'); j++)
		{
			var itemType = record.getLineItemValue('item', 'itemtype', i+1);
			if(itemType == 'InvtPart' || itemType == 'NonInvtPart')
			{
				var amount  	= parseFloat(record.getLineItemValue('item', 'amount', 	i+1));
				var dualTax 	= false;
				var TaxRate1 	= record.getLineItemValue('item', 'taxrate1', i+1);
				TaxRate1 		= (TaxRate1 != null && TaxRate1.indexOf('%') != -1) ? TaxRate1.substring(0, TaxRate1.indexOf('%'))/100 : 0.0;
				var TaxRate2 	= record.getLineItemValue('item', 'taxrate2', i+1);
				if(TaxRate2 != null)	dualTax = true;
				TaxRate2 		= (TaxRate2 != null && TaxRate2.indexOf('%') != -1) ? TaxRate2.substring(0, TaxRate2.indexOf('%'))/100 : 0.0;
				
				newOrderDetail 						= new Object();
				newOrderDetail.orderId 				= recordId;
				newOrderDetail.orderLineNumber 		= record.getLineItemValue('item', 'id', i+1);
				newOrderDetail.countItemOrdered 	= parseInt(record.getLineItemValue('item', 	'quantity', i+1));
				newOrderDetail.amtExtended 			= amount;
				newOrderDetail.amtExtendedCost 		= 0.00; 
				newOrderDetail.orderDetailMemo 		= "";
				newOrderDetail.isTaxable 			= (TaxRate1 > 0 || TaxRate2 > 0) ? true : false;
			
				newOrderDetail.item 				= new Object();
				newOrderDetail.item.SKUId 			= record.getLineItemValue('item', 'custcol_wtka_upccode', 	i+1);
				newOrderDetail.item.SKUCode 		= record.getLineItemValue('item', 'item_display', 			i+1);
				newOrderDetail.item.SKUName 		= record.getLineItemValue('item', 'description', 			i+1);
		
				newOrderDetail.amtItemPrice							= new Object();
				newOrderDetail.amtItemPrice.priceId 				= "";
				newOrderDetail.amtItemPrice.currencyCode 			= readValue(record.getFieldValue('currencyname'));
				newOrderDetail.amtItemPrice.amtCurrentPrice 		= parseFloat(record.getLineItemValue('item', 'rate', i+1));
				newOrderDetail.amtItemPrice.amtPromoPrice 			= parseFloat(record.getLineItemValue('item', 'rate', i+1));
				newOrderDetail.amtItemPrice.priceEffectiveStartDt 	= null;
				newOrderDetail.amtItemPrice.priceEffectiveEndDt 	= null;
				newOrderDetail.amtItemPrice.lastModifiedByUser 		= "";
				newOrderDetail.amtItemPrice.lastModifiedTimestamp 	= "";
				
				newOrderDetail.tax 					= new Array();
				newOrderDetail.tax[0] 				= new Object();
				newOrderDetail.tax[0].taxId 		= record.getLineItemValue('item', 'taxcode', 	 		 i+1);
				newOrderDetail.tax[0].taxName 		= record.getLineItemValue('item', 'taxcode_display', 	 i+1);
				newOrderDetail.tax[0].taxAltName 	= "";
				newOrderDetail.tax[0].taxRate 		= parseFloat(record.getLineItemValue('item', 'taxrate1', i+1));
				newOrderDetail.tax[0].taxCity 		= "";
				newOrderDetail.tax[0].taxCounty 	= "";
				newOrderDetail.tax[0].taxState 		= "";
				newOrderDetail.tax[0].taxCountry 	= "";
				newOrderDetail.tax[0].amtTaxApplied	= TaxRate1 * amount;
				
				if(dualTax)
				{
					newOrderDetail.tax[1] 				= new Object();
					newOrderDetail.tax[1].taxId 		= record.getLineItemValue('item', 'taxcode', 	 		 i+1);
					newOrderDetail.tax[1].taxName 		= record.getLineItemValue('item', 'taxcode_display', 	 i+1);
					newOrderDetail.tax[1].taxAltName 	= "";
					newOrderDetail.tax[1].taxRate 		= parseFloat(record.getLineItemValue('item', 'taxrate2', i+1));
					newOrderDetail.tax[1].taxCity 		= "";
					newOrderDetail.tax[1].taxCounty 	= "";
					newOrderDetail.tax[1].taxState 		= "";
					newOrderDetail.tax[1].taxCountry 	= "";
					newOrderDetail.tax[1].amtTaxApplied	= TaxRate2 * amount;
				}
				
				salesOrderSchema.order.orderDetail.push(newOrderDetail);
				totalItemsShipped += parseInt(record.getLineItemValue('item', 'quantityfulfilled', i+1));
			}
			i++;
		}
		
		salesOrderSchema.order.shipment			 										= new Array();
		salesOrderSchema.order.shipment[0]												= new Object();
		salesOrderSchema.order.shipment[0].shipmentHeader 								= new Object();
		salesOrderSchema.order.shipment[0].shipmentHeader.shipmentId 					= readValue(record.getFieldValue('tranid'));
		salesOrderSchema.order.shipment[0].shipmentHeader.shipmentCode 					= carriername;
		salesOrderSchema.order.shipment[0].shipmentHeader.shippingMethod 				= carriercode;
		salesOrderSchema.order.shipment[0].shipmentHeader.trackingNumber 				= "";
		//salesOrderSchema.order.shipment[0].shipmentHeader.RMA 							= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.shipmentStatus 				= "CL";
		salesOrderSchema.order.shipment[0].shipmentHeader.countTotalShipped 			= totalItemsShipped;
		salesOrderSchema.order.shipment[0].shipmentHeader.countTotalReceivedInShipment 	= parseInt(record.getLineItemValue('item', 'quantityreceived', i+1));
		if(!salesOrderSchema.order.shipment[0].shipmentHeader.countTotalReceivedInShipment)
		{
			salesOrderSchema.order.shipment[0].shipmentHeader.countTotalReceivedInShipment = 0;
		}
		salesOrderSchema.order.shipment[0].shipmentHeader.shipDt 						= readValue(record.getFieldValue('shipdate'));
		salesOrderSchema.order.shipment[0].shipmentHeader.shipReceivedDt 				= readValue(record.getFieldValue('custbody_ka_to_expected_recv_date'));  
		salesOrderSchema.order.shipment[0].shipmentHeader.expectedShipDate 				= readValue(record.getFieldValue('custbody_ka_to_expected_ship_date'));
		salesOrderSchema.order.shipment[0].shipmentHeader.handlingInstructions 			= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.carrierName 					= carriername;
		
		var expectedReceiveDate  = readValue(record.getFieldValue('custbody_ka_to_expected_recv_date'));
		salesOrderSchema.order.shipment[0].shipmentHeader.expectedReceiveDate 			= expectedReceiveDate ? moment.utc(moment(expectedReceiveDate)).format("YYYY-MM-DDTHH:MM:SSZ") : '';
		
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient 					= new Object();
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.customerId 			= customerId;
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.customerEmail 		= readValue(customerEmail);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.honorific 			= readValue(customerData.salutation);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.firstName 			= readValue(customerData.firstname);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.lastName 			= readValue(customerData.lastname);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.customerPhoneNumber = readValue(customerData.phone);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.gender 				= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.birthDt 			= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.locale 				= locale;
		
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress 						= new Object();
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.billingAddressId 	= record.getFieldValue('billingaddress_key');
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.billingFirstName	 	= readValue(customerData.firstname);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.billingLastName 	 	= readValue(customerData.lastname);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.billingEmail 		= readValue(customerEmail);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.billingAltEmail 		= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.billingAddressLine1 	= readValue(billData.billaddr1);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.billingAddressLine2	= readValue(billData.billaddr2);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.poBox 				= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.cityName				= readValue(billData.billcity);			
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.postalCode 		 	= readValue(billData.billzip);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.provinceName 		= readValue(billData.billstate);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.provinceAltName 	 	= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.countryName 		 	= readValue(billData.billcountry);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.countryAltName 	 	= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.billingAddress.inActiveDate 		= "";

		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress 						= new Object();
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.shippingAddressId 		= record.getFieldValue('shippingaddress_key');
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.recipientFirstName		= readValue(customerData.firstname);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.recipientLastName 		= readValue(customerData.lastname);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.recipientEmail 			= readValue(customerEmail);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.recipientAltEmail 	 	= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.recipientPhoneNumber 	= readValue(shipData.shipphone);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.shippingAddressLine1 	= readValue(shipData.shipaddr1);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.shippingAddressLine2 	= readValue(shipData.shipaddr2);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.poBox 				 	= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.cityName 		 		= readValue(shipData.shipcity);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.postalCode 		 		= readValue(shipData.shipzip);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.provinceName 	 		= readValue(shipData.shipstate);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.provinceAltName 		= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.countryName 		 	= readValue(shipData.shipcountry);
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.countryAltName 	 		= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.recipient.shippingAddress.inActiveDate 	 		= "";

		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation				= new Object();
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.locationCode 	= readValue(fromLocationData.externalid); 
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.locationName 	= readValue(fromLocationData.name);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.addressLine1 	= readValue(fromLocationData.address1);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.addressLine2 	= readValue(fromLocationData.address2);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.city 			= readValue(fromLocationData.city);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.province 		= readValue(fromLocationData.state);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.country 		= readValue(fromLocationData.country);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.mailingCode 	= readValue(fromLocationData.zip);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.phoneNumber 	= readValue(fromLocationData.addrphone);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.latitude 		= 0.00000; 
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.longitude 		= 0.00000; 
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.openingDate 	= readValue(fromLocationData.custrecord_ka_store_opening_date);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.closedDate 	= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.locationType 	= readValue(fromLocationData.custrecord_ra_loctype);
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.storeHours 	= "";
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.locStatus 		= (fromLocationData.isinactive == 'F') ? 'Open' : 'Closed';
		salesOrderSchema.order.shipment[0].shipmentHeader.fulfillingLocation.DCLocCode 		= fromLocationData.externalid;

		salesOrderSchema.order.shipment[0].shipmentDetail 	= new Array();		
		for(var i=0; i < record.getLineItemCount('item'); i++)
		{
			var itemType = record.getLineItemValue('item', 'itemtype', i+1);
			if(itemType == 'InvtPart' || itemType == 'NonInvtPart')
			{
				newDetail 					= new Object();
				newDetail.orderDetailId 	= readValue(record.getLineItemValue('item', 'id', 					i+1));
				newDetail.quantityOrdered 	= parseInt(record.getLineItemValue('item', 	'quantity',				i+1));
				newDetail.quantityShipped 	= parseInt(record.getLineItemValue('item', 	'quantityfulfilled', 	i+1));
				salesOrderSchema.order.shipment[0].shipmentDetail.push(newDetail);
			}
		}
	
	} catch(so_error){
        nlapiLogExecution('DEBUG','error message',so_error.message);
		ErrorObj.editype 	 				= 940;
		ErrorObj.orderid 					= recordId;
		ErrorObj.messages[0] 				= new Object();
		ErrorObj.messages[0].messagetype 	= "Sales Order Error";
		ErrorObj.messages[0].message 	 	= "Error Details: " + so_error.message;
		
		//For additional logging - START
		ErrorObj.status = 'Exception';
		//For additional logging - END
		
		salesOrderSchema 					= ErrorObj;
	}
	return salesOrderSchema;
}