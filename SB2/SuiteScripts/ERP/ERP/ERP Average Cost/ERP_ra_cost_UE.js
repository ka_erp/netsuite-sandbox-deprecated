/**
 * Module Description
 *  Generate an average cost from the closest warehouse when an item is returned to the store, and there is no cost associated with the item. If no cost can be found email inventory control
 * Version    Date            Author           Remarks
 * 1.00       11 Mar 2016     timothy.frazer
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType :Invoice, Credit memo
 *
 * @param {String} type Operation types: on create
 * @returns {Void}
 */

// at the moment assuming happy path. Will add Try and Catches later
function main_underscore(type){

}
Transaction = {
	//need a different name for the namespace
	avg_cost_BS : function (type){

		if(type == 'delete'){return;}
			//create item array and return any items that have no Est. Average Cost
			// if all the items have a value set, then run adjustment
		if(nlapiGetFieldValue('custbody_erp_avg_cost_adj') == 'F') {
			var itemToAdjust = Transaction.getItemArray_BS();

			if(!isNullOrEmpty(itemToAdjust)){
				var avgCost = Transaction.getAvgCost_BS(itemToAdjust);
				var idSubsidiary = nlapiGetFieldValue('subsidiary');
				var locationId = nlapiGetFieldValue('location');

				nlapiLogExecution('DEBUG','avg_cost_BS: avg cost',	JSON.stringify(avgCost));
				if(!isNullOrEmpty(avgCost)){
				var invAdj ={};
				invAdj.in = Transaction.createInvAdj_BS(avgCost,1,idSubsidiary,locationId);

				if(!isNullOrEmpty(invAdj.in)){
					nlapiSetFieldValue('custbody_erp_aca_in',invAdj.in);
					nlapiLogExecution('DEBUG','avg_cost_BS: Inv adj +1',	invAdj.in);

		  	invAdj.out= Transaction.createInvAdj_BS(avgCost,-1,idSubsidiary,locationId);

					nlapiSetFieldValue('custbody_erp_aca_out',invAdj.out);
					nlapiLogExecution('DEBUG','avg_cost_BS: Inv adj -1',	invAdj.out);

					nlapiSetFieldValue('custbody_erp_aca','T');
					nlapiSetFieldValue('custbody_erp_avg_cost_adj','T');
					}
					else {
						nlapiLogExecution('DEBUG','avg_cost_BS:createInvAdj_BS',' failure in average cost setting');
					}
					}
					else{
						nlapiSetFieldValue('custbody_erp_aca','F');
					//	console.log('email');
						nlapiLogExecution('DEBUG','avg_cost_BS',' no AVG cost found Error Send email');
					}
			}else{ //ivan
				
				nlapiSetFieldValue('custbody_erp_aca', 'T'); //ivan
			} //ivan
		}
	},
	//Get the item array of objects containg internal ID and line number
	getItemArray_BS : function() {
		var intGetlineCount = nlapiGetLineItemCount('item');
		var arrItems = new Array();
		for(var i = 1; i <= intGetlineCount; i++){

				var idItemId = nlapiGetLineItemValue('item','item', i);
				var lnItem = i;
				//var itemValue=nlapiGetLineItemValue('item','costestimaterate',i); //
				
				var itemValue=nlapiGetLineItemValue('item','custcol_erp_loc_average_cost',i); // 
				var strItemType = nlapiGetLineItemValue('item','itemtype', i);

			//only add items that are inventory to the average cost look up & have no  Est. Cost value
			if(strItemType == 'InvtPart' && itemValue <= 0) {
				arrItems.push({
					id:idItemId,
					line:lnItem,
					costEst:itemValue
				});

			}
	}
		nlapiLogExecution("DEBUG", "getItemArray_BS : getItemArray", JSON.stringify(arrItems));
		//return item array of items with no Est. Cost Value.
		return arrItems;
	},
	//find the average cost for the item Object
	getAvgCost_BS: function (arrZeroAvgItemIds) {
		var idSubsidiary = nlapiGetFieldValue('subsidiary');
 		var arrayItm = new Array();

		var searchFiltersMainLocation = [];
		searchFiltersMainLocation[0] =  new nlobjSearchFilter('subsidiary', null, 'is', idSubsidiary);
		searchFiltersMainLocation[1] =  new nlobjSearchFilter('custrecord_ka_main_warehouse', null, 'is', 'T');
		var searchColumnsMainLocation = [new nlobjSearchColumn('internalid')];
		var resultsMainLocation = nlapiSearchRecord('location', null, searchFiltersMainLocation, searchColumnsMainLocation);
		if (!isNullOrEmpty(resultsMainLocation)){

			var idLocation = resultsMainLocation[0].getValue('internalid');
			var arSavedSearchItemMainResultsItm = null;
		    var arSaveSearchFiltersItm = new Array();
		    var arSavedSearchItemMainColumnsItm = new Array();
		    var strSavedSearchIDItm = null;

		    arSaveSearchFiltersItm.push(new nlobjSearchFilter('inventorylocation', null , 'is', idLocation ));
		    //traverse the object for the item id
		    arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null , 'anyof', _.pluck(arrZeroAvgItemIds,"id")));

		    arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
		    arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('name'));
		    arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('inventorylocation'));
		    arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('locationaveragecost')); 
			arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('custitem_erp_vendorprice_last_fca')); //FCA
			arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('custitem_erp_vendorprice_last_currency')); // Currency
							
			
				try{
			  arSavedSearchItemMainResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchItemMainColumnsItm);
		    if (!isNullOrEmpty(arSavedSearchItemMainResultsItm)){
		        for(var i = 0 ;  i < arSavedSearchItemMainResultsItm.length; i++ ){
		         	var itmObj = {
		          		    	id : arSavedSearchItemMainResultsItm[i].getValue('internalid'),
		                    name : arSavedSearchItemMainResultsItm[i].getValue('name'),
		                    location : arSavedSearchItemMainResultsItm[i].getValue('inventorylocation'),
		                    locationname : arSavedSearchItemMainResultsItm[i].getText('inventorylocation'),
		                    locavecost : arSavedSearchItemMainResultsItm[i].getValue('locationaveragecost'),
							locfcacost : arSavedSearchItemMainResultsItm[i].getValue('custitem_erp_vendorprice_last_fca'),
							locfcalastcurr : arSavedSearchItemMainResultsItm[i].getValue('custitem_erp_vendorprice_last_currency')
							
		            };
								if (isNullOrEmpty(itmObj.locavecost) && isNullOrEmpty(itmObj.locfcacost) ) {
									//no average cost found
									return null;
								}
								arrayItm.push(itmObj);
		         //	console.log(arrayItm[i]);
		        }
		    }
		return arrayItm;
		}
		catch(e){
			nlapiLogExecution('DEBUG','getAvgCost_BS:',	'ERROR '+ e);
			return null;
		}
	}
},
	createInvAdj_BS: function(itemObj,adjQty,subsidiaryId,locationId,tranId){
				var adjObj =	nlapiCreateRecord('inventoryadjustment');
					//set inventory adjustment values
					adjObj.setFieldValue('subsidiary',subsidiaryId);
					adjObj.setFieldValue('custbody_erp_avg_cost_adj','T');
					//adjObj.setFieldValue('externalid',tranId); 															//IvAn Temp
					nlapiLogExecution('AUDIT','Invoice ID',tranId);
					//make Netsuite Script Parameter so that the cogs account can be changed at a later date
					adjObj.setFieldValue('account',132);
					adjObj.setFieldValue('adjlocation',locationId);
					// create line items and set them bad boys
					for(var i = 0; i < itemObj.length;i++){
						adjObj.selectNewLineItem('inventory');
					  adjObj.setCurrentLineItemValue('inventory','item',itemObj[i].id);
						adjObj.setCurrentLineItemValue('inventory','location',locationId);
						
						if(!isNullOrEmpty(itemObj[i].locavecost)){
							adjObj.setCurrentLineItemValue('inventory','unitcost',itemObj[i].locavecost);
						}
						else{
							
							var fcaRaw = itemObj[i].locfcacost;

							var subcurr = nlapiLookupField('subsidiary',subsidiaryId,'currency')
							var fcacurr = itemObj[i].locfcalastcurr;	
							var fxRate = nlapiExchangeRate(fcacurr,subcurr);
							
							var fcaFx = parseFloat(fcaRaw) * parseFloat(fxRate);
							
							adjObj.setCurrentLineItemValue('inventory','unitcost',fcaFx);	
							
							

						}
					
						adjObj.setCurrentLineItemValue('inventory','adjustqtyby',adjQty);
						adjObj.commitLineItem('inventory');
					}

					var invAdj =nlapiSubmitRecord(adjObj);
					if(!isNullOrEmpty(invAdj))	{
					var newTranId = 'ACA-'+nlapiLookupField('inventoryadjustment',invAdj,'tranid');
					nlapiSubmitField('inventoryadjustment',invAdj,'tranid',newTranId);
					return invAdj;
				}
				else {
					return null;
				}
		

		},

	recordInvAdjAction_as: function() {
		if(type == 'delete'){return;}
		// If an inventory adjustment was made then link back the inventory adjustment to the originating transaction
		if(nlapiGetFieldValue('custbody_erp_avg_cost_adj')=='T'){
			var transId = nlapiGetRecordId();
		nlapiLogExecution('DEBUG','recordInvAdjAction_as:set inv adjustment link',	transId);
		try{
		nlapiSubmitField('inventoryadjustment',nlapiGetFieldValue('custbody_erp_aca_in'),'custbody_erp_cogs_transaction',	transId);
//		nlapiSubmitField('inventoryadjustment',nlapiGetFieldValue('custbody_erp_aca_in'),'externalid',transId+'a');

		nlapiSubmitField('inventoryadjustment',nlapiGetFieldValue('custbody_erp_aca_out'),'custbody_erp_cogs_transaction',transId);
		//nlapiSubmitField('inventoryadjustment',nlapiGetFieldValue('custbody_erp_aca_out'),'externalid',transId+'b');
	}
catch(e){		nlapiLogExecution('DEBUG','avg_cost_BS:  Adjustment links unable to set',	JSON.stringify(transId));}
	}// else IF no average cost was found send an email
	else if ( nlapiGetFieldValue('custbody_erp_aca') == 'F'){
		Transaction.sendEmail_as();
	}
	//Nothing to do yeah!

	},
		//send email after the transaction has been made
	sendEmail_as: function(){
		var recId
				, tranId
				, recordType
				,	emailMerger
				,	mergeResult
				,	templateId
				,	emailBody
				,	emailSender
				,	emailSubject
				,	emailRecipient;
				recId = nlapiGetRecordId();
				recordType = nlapiGetRecordType();
			  tranId = nlapiGetFieldValue('tranid');

				nlapiLogExecution('DEBUG', 'sendEmail_as: Sending Email ', recId);
				try {

						emailSender = 1549;
						emailSubject = 'No Average Cost Found: '+recordType+' : '+tranId;
						emailRecipient = 298467;

						nlapiLogExecution('DEBUG', 'sendEmail_as: emailRecipient ', emailRecipient);

						if(!isNullOrEmpty(emailRecipient)){

							emailMerger = nlapiCreateEmailMerger(25);
							emailMerger.setTransaction(recId);
							mergeResult = emailMerger.merge();
							emailBody =  mergeResult.getBody();
							try{
								var sendEmail = nlapiSendEmail(emailSender, emailRecipient, emailSubject, emailBody, null, null,recId,null,true);
								nlapiLogExecution('DEBUG', 'sendEmail_as: Email ', sendEmail);
							}
							catch(e){
								nlapiLogExecution('ERROR', 'sendEmail_as:Error sending notification email', e);
							}
						}
					}
	catch(e) {
		nlapiLogExecution('ERROR', 'Unexpected Error', e);
		nlapiSendEmail(6, 'erpsupport@kitandace.com', 'Unexpected Error sending Average Cost notification  email', e);

			}
	}
}


function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}