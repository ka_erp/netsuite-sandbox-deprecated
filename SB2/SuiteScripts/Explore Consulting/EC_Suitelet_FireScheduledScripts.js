/**
 * Company            Explore Consulting
 * Copyright            2014 Explore Consulting, LLC
 * Type                NetSuite EC_Suitelet_FireScheduledScripts
 * Version            1.0.0.0
 * Description        Boilerplate startup code for a NetSuite RESTlet.
 **/

if ( nlapiGetContext().getEnvironment() == "PRODUCTION" ){
    var scriptStatusURL = "https://system.na1.netsuite.com/app/common/scripting/scriptstatus.nl?daterange=TODAY&primarykey=&sortcol=dcreated&sortdir=DESC&date=TODAY&scripttype=";
    //var scriptStatusURL = "https://system.na1.netsuite.com/app/common/scripting/scriptstatus.nl?";       // This URL is only used if we just want the general status page - and not a script specific one
}
else{
    //var scriptStatusURL = "https://system.sandbox.netsuite.com/app/common/scripting/scriptstatus.nl?daterange=TODAY&primarykey=&sortcol=dcreated&sortdir=DESC&date=TODAY&scripttype=";
    //var scriptStatusURL = "https://system.sandbox.netsuite.com/app/common/scripting/scriptstatus.nl?";       // This URL is only used if we just want the general status page - and not a script specific one

    var scriptStatusURL = "https://system.sandbox.netsuite.com/app/common/scripting/scriptstatus.nl?daterange=TODAY&primarykey=&sortcol=dcreated&sortdir=DESC&date=TODAY&scripttype=";
}


function onRequest(request,response)
{
    var params = request.getAllParameters();
    Log.d('Params','Script ID: ' + params['scriptid'] + ', Deploy ID: ' + params['deployid']);

    if ( params['scriptid'] )
        fireScheduledScript(params['scriptid'],params['deployid']);
    else
        renderSuitelet();

    function renderSuitelet()
    {
        Log.d('renderSuitelet','STARTING');
        var configRecords,configHtml,fullHtml;

        loadConfigRecords();
        buildHtml();
        buildFullHtml();

        Log.d('renderSuitelet','fullHtml:  ' + fullHtml);
        response.write(fullHtml);

        function loadConfigRecords()
        {
            Log.d('loadConfigRecords','STARTING');
            var configProps = [
                'custrecord_ec_fire_scheduled_title',
                'custrecord_ec_fire_scheduled_script_id',
                'custrecord_ec_fire_scheduled_deploy_id',
                'custrecord_ec_fire_scheduled_script_url',
                'custrecord_ec_fire_sched_script_int_id'
            ];
            var filter = new nlobjSearchFilter("isinactive", null, "is", "F");
            var columns = [new nlobjSearchColumn("internalid")];
            columns[0].setSort();
            var configRecResults = nlapiSearchRecord('customrecord_ec_fire_scheduled_config', null, filter, columns);

            configRecords = [];

            _.each(configRecResults,function(res)
            {
                configRecords.push(nsdal.loadObject('customrecord_ec_fire_scheduled_config',res.getId(),configProps));
            });
            Log.d('loadConfigRecords','configRecords.length:  ' + configRecords.length);
        }

        function buildHtml()
        {
            Log.d('buildHtml','STARTING');

            configHtml = '<table>';
            _.each(configRecords,function(configRec, index)
            {
                var lineNumber = index+1;
                Log.d('buildHtml','configRec.custrecord_ec_fire_scheduled_title:  ' + configRec.custrecord_ec_fire_scheduled_title);
                Log.d('buildHtml','configRec.custrecord_ec_fire_scheduled_script_id:  ' + configRec.custrecord_ec_fire_scheduled_script_id);
                Log.d('buildHtml','configRec.custrecord_ec_fire_sched_script_int_id:  ' + configRec.custrecord_ec_fire_sched_script_int_id);
                Log.d('buildHtml','configRec.custrecord_ec_fire_scheduled_deploy_id:  ' + configRec.custrecord_ec_fire_scheduled_deploy_id);
                Log.d('buildHtml','configRec.custrecord_ec_fire_scheduled_script_url:  ' + configRec.custrecord_ec_fire_scheduled_script_url);


                //scriptStatusURL = scriptStatusURL + configRec.custrecord_ec_fire_sched_script_int_id;
                //Log.d('buildHtml','scriptStatusURL:  ' + scriptStatusURL);

                configHtml += '\
                <tr>\
                    <td><button data-script-id="' + configRec.custrecord_ec_fire_scheduled_script_id + '" \
                                data-deploy-id="' + configRec.custrecord_ec_fire_scheduled_deploy_id + '" \
                                data-lineid="' + lineNumber + '" \
                                data-suitelet-url="' + configRec.custrecord_ec_fire_scheduled_script_url +
                    '">Fire!\
                    </button>\
                </td>\
                <td>' + configRec.custrecord_ec_fire_scheduled_title + '</td>\
                <td style="padding-left:10px;padding-right:10px;"><a target="_blank" href="' + scriptStatusURL + configRec.custrecord_ec_fire_sched_script_int_id + '">Check Status</a></td>\
                <td>Last Run:&nbsp;&nbsp;<span id="last_run_' + lineNumber + '"</span></td>\
                </tr>';
            });

            configHtml += '</table>';
            Log.d('buildHtml','configHtml:  ' + configHtml);
        }

        function buildFullHtml()
        {
            Log.d('buildHtml','STARTING');
            var globalProps = [
                'custrecord_ec_fire_scheduled_template',
                'custrecord_ec_fire_scheduled_js',
                'custrecord_ec_fire_scheduled_jquery'
            ];
            // This will be set up by the bundle install script
            var globalConfigRec = nsdal.loadObject('customrecord_ec_fire_scheduled_global',1,globalProps);
            var htmlFile = nlapiLoadFile(globalConfigRec.custrecord_ec_fire_scheduled_template);
            var jQueryFile = nlapiLoadFile(globalConfigRec.custrecord_ec_fire_scheduled_jquery).getURL();
            var jsFile = nlapiLoadFile(globalConfigRec.custrecord_ec_fire_scheduled_js).getURL();

            fullHtml = htmlFile.getValue()
                .replace('<SUBS_SCRIPT_LINES />',configHtml)
                .replace('<SUBS_JQUERY_FILE />',jQueryFile)
                .replace('<SUBS_JS_FILE />',jsFile);
        }
    }

    function fireScheduledScript(scriptId,deployId)
    {
        Log.d('fireScheduledScript','scriptId:  ' + scriptId + "  deployId:  " + deployId);
        nlapiScheduleScript(scriptId,deployId);
    }
}