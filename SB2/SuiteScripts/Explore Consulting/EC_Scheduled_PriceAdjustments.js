/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_Scheduled_PriceAdjustments.js
 * Version            1.0.0.0
 **/
if (typeof EC == "undefined" || !EC)
{
    var EC = {};
}

EC.Settings =
{
    PriceAdjustmentScheduleStatus:{
        Scheduled:1,
        InProgress:2,
        Completed:3,
        Error:4,
        NotScheduled:5,
        CompletedWithErrors:6
    },
    Currencies: [
        {ID: 1, Name: 'CAD'},
        {ID: 2, Name: 'USD'},
        {ID: 3, Name: 'GBP'},
        {ID: 4, Name: 'EUR'},
        {ID: 5, Name: 'HKD'},
        {ID: 6, Name: 'AUD'},
        {ID: 7, Name: 'JPY'}
    ],
    elapsedTimeThreshold: 3400
};

EC.itemLevelErrors = '';
EC.governance = null;
//var itemLevelErrors = '';
EC.logit = function(title, message)
{
    Log.d(title, message);
};

function onStart()
{
    EC.performPriceAdjustment();
}

EC.performPriceAdjustment = function()
{
    EC.logit('performPriceAdjustment', 'start');

    // Search the Price Adjustment Schedule custom record type (customrecord_markdown_price_level) for records where:
    //   - Start Date is less than the current datetime and Start Date Status = Scheduled
    //   - OR End Date is less than the current datetime and End Date Status = Scheduled

    // For each Price Adjustment Schedule record:
    //  - Validate that Online Price Level, Markdown Price Level and Online Retail Price Level fields are all populated
    //  - IF Start Date is less than the current datetime and Start Date Status = Scheduled
    //    - Set Start Date Status = In Progress
    //  - IF End Date is less than the current datetime and End Date Status = Scheduled
    //    - Set End Date Status = In Progress
    //  - Search items where Price Adjustment Schedule (custitem_price_adjustment_schedule) = the schedule we are processing
    //  - For each Item:
    //    - Load the item record
    //    - For each Currency:
    //      - Start Date Logic:
    //        - IF Start Date is less than the current datetime and Start Date Status = Scheduled
    //          - Validate the Online price does not equal the Markdown price
    //          - IF Update Online Retail Price checkbox is checked
    //            - Validate that the 0 Quantity price for the Online and Markdown price levels are populated
    //            - Update the Online Retail price with the value of the Online price
    //            - Update the Online price with the value of the Markdown price
    //          - IF Update Online Retail Price checkbox is NOT checked
    //            - Validate that the 0 Quantity price for the Online price level is populated
    //            - Update the Online price with the value of the Markdown price
    //          - IF an error occurs within the Start Date logic, set Start Date Status to Completed with Errors
    //      - End Date Logic:
    //        - IF End Date is less than the current datetime and End Date Status = Scheduled
    //          - Validate the Online price does not equal the OnlineRetail price
    //          - Validate that the 0 Quantity price for the Online Retail price level is populated
    //          - Update the Online price with the value of the Online Retail price
    //          - IF an error occurs within the End Date logic, set End Date Status to Completed with Errors
    //  - IF any error occurs outside of the Item loop
    //    - IF Start Date Status = In Progress, change it to Error
    //    - IF End Date Status = In Progress, change it to Error
    //    - Write error message to the Error Message field
    // - IF the Start Date Status is still In Progress, it means there were no errors.  Set status to Complete.
    // - IF the End Date Status is still In Progress, it means there were no errors.  Set status to Complete.

    EC.governance = new Governance();
    EC.enableLazySearch();
    var now = moment();
    var schedules = EC.createSearch('customrecord_markdown_price_level'
        , [
            [
                ['custrecord_run_date', 'before', nlapiDateToString(now.toDate(), 'datetime')]
                , 'and', ['custrecord_start_date_status', 'anyof', EC.Settings.PriceAdjustmentScheduleStatus.Scheduled]
            ]
            , 'or'
            ,[
                ['custrecord_sale_end_date', 'before', nlapiDateToString(now.toDate(), 'datetime')]
                , 'and', ['custrecord_end_date_status', 'anyof', EC.Settings.PriceAdjustmentScheduleStatus.Scheduled]
            ]
        ]
    ).toArray(); // 10 units

    _.each(schedules, function(schedule){
        EC.processPriceAdjustmentSchedule(schedule, now);
    });
};

EC.processPriceAdjustmentSchedule = function(schedule, now)
{
    EC.logit(logContext, 'Start');
    EC.itemLevelErrors = '';
    var performStartLogic = false;
    var performEndLogic = false;
    var scheduleInternalID = schedule.getId();
    var logContext = 'processPriceAdjustmentSchedule: scheduleInternalID: ' + scheduleInternalID + ': ';

    try
    {
        //throw new Error('test schedule level error');
        var scheduleRecord = nsdal.loadObject('customrecord_markdown_price_level', scheduleInternalID
            , ['name','custrecord_run_date','custrecord_start_date_status','custrecord_sale_end_date'
                ,'custrecord_end_date_status','custrecord_online_price_level','custrecord_to_price_level','custrecord_online_retail_price','custrecord_update_online_retail_price'
            ]); // 2 units

        if(scheduleRecord.custrecord_online_price_level && scheduleRecord.custrecord_to_price_level && scheduleRecord.custrecord_online_retail_price)
        {
            EC.logit(logContext, 'All 3 price levels are populated.');
            if(now.isAfter(scheduleRecord.custrecord_run_date) && scheduleRecord.custrecord_start_date_status == EC.Settings.PriceAdjustmentScheduleStatus.Scheduled)
            {
                EC.logit(logContext, 'Performing Start logic');
                EC.updatePriceAdjustmentSchedule(scheduleInternalID,EC.Settings.PriceAdjustmentScheduleStatus.InProgress, null, ''); // 2 units
                scheduleRecord.custrecord_start_date_status = EC.Settings.PriceAdjustmentScheduleStatus.InProgress;
                performStartLogic = true;
            }
            if(now.isAfter(scheduleRecord.custrecord_sale_end_date) && scheduleRecord.custrecord_end_date_status == EC.Settings.PriceAdjustmentScheduleStatus.Scheduled)
            {
                EC.logit(logContext, 'Performing End logic');
                EC.updatePriceAdjustmentSchedule(scheduleInternalID,null, EC.Settings.PriceAdjustmentScheduleStatus.InProgress, ''); // 2 units
                scheduleRecord.custrecord_end_date_status = EC.Settings.PriceAdjustmentScheduleStatus.InProgress;
                performEndLogic = true;
            }

            var items = EC.createSearch('item'
                ,[
                    ['custitem_price_adjustment_schedule', 'anyof', scheduleInternalID]
                    //, 'and', ['internalid', 'anyof', '5128']
                ]
                ,[['itemid']]
            ).toArray(); // 10 units
            EC.logit(logContext, 'Number of Items: ' + items.length);

            _.each(items, function(item){
                // Check governance and yield if necessary
                if(EC.governance.remainingUsage().units <= 100 || EC.governance.elapsedTime() > EC.Settings.elapsedTimeThreshold)
                {
                    EC.logit(logContext, 'Performing End logic');
                    Log.d(logContext, "Yielding.  Remaining Usage: " + EC.governance.remainingUsage().units + ', Elapsed Time: ' + EC.governance.elapsedTime());
                    nlapiYieldScript();
                    EC.governance = new Governance();
                }
                EC.processItem(item, scheduleRecord, performStartLogic, performEndLogic);
            });

            // All item have been processed.  Update the Price Adjustment Schedule record
            var startDateStatus = null;
            var endDateStatus = null;
            var errorMessage = EC.itemLevelErrors;

            if(performStartLogic)
            {
                startDateStatus = scheduleRecord.custrecord_start_date_status;
                if(scheduleRecord.custrecord_start_date_status == EC.Settings.PriceAdjustmentScheduleStatus.InProgress)
                {
                    startDateStatus = EC.Settings.PriceAdjustmentScheduleStatus.Completed;
                    errorMessage = '';
                }
            }
            if(performEndLogic)
            {
                endDateStatus = scheduleRecord.custrecord_end_date_status;
                if(scheduleRecord.custrecord_end_date_status == EC.Settings.PriceAdjustmentScheduleStatus.InProgress)
                {
                    endDateStatus = EC.Settings.PriceAdjustmentScheduleStatus.Completed;
                    errorMessage = '';
                }
            }
            EC.updatePriceAdjustmentSchedule(scheduleInternalID,startDateStatus, endDateStatus, errorMessage); // 2 units
        }
    }
    catch(e)
    {
        var startDateStatus = null;
        var endDateStatus = null;

        if(performStartLogic) startDateStatus = EC.Settings.PriceAdjustmentScheduleStatus.Error;
        if(performEndLogic) endDateStatus = EC.Settings.PriceAdjustmentScheduleStatus.Error;

        if(!performStartLogic && !performEndLogic)
        {
            // In this unlikely event, it can only mean there was an unexpected error loading the schedule record
            // We don't know if which logic will be performed, so flag them both as error.
            startDateStatus = EC.Settings.PriceAdjustmentScheduleStatus.Error;
            endDateStatus = EC.Settings.PriceAdjustmentScheduleStatus.Error;
        }
        EC.updatePriceAdjustmentSchedule(scheduleInternalID,startDateStatus, endDateStatus, e.message); // 2 units
    }
};

EC.processItem = function(item, scheduleRecord, performStartLogic, performEndLogic)
{
    var scheduleInternalID = scheduleRecord.getId();
    var itemInternalID = item.getId();
    var itemName = item.getValue('itemid');
    var logContext = 'processItem: scheduleInternalID: ' + scheduleInternalID + ', Item: ' + itemName + ': ';
    var saveItem = false;

    try{
        EC.logit(logContext, 'Start');
        var onlinePriceLevel = scheduleRecord.custrecord_online_price_level;
        var markdownPriceLevel = scheduleRecord.custrecord_to_price_level;
        var onlineRetailPriceLevel = scheduleRecord.custrecord_online_retail_price;

        //throw new Error('test item level error');
        var itemRecord = nsdal.loadObject('inventoryitem', itemInternalID,  ['internalid']); // 5 units

        _.each(EC.Settings.Currencies, function(currency){
            logContext = 'processItem: scheduleInternalID: ' + scheduleInternalID + ', Item: ' + itemName + ', ' + currency.Name + ': ';
            var lineItemType = 'price' + currency.ID;
            var onLinePriceLevelLineNumber = itemRecord.findLineItemValue(lineItemType, 'pricelevel', onlinePriceLevel);
            var markdownPriceLevelLineNumber = itemRecord.findLineItemValue(lineItemType, 'pricelevel', markdownPriceLevel);
            var onLineRetailPriceLevelLineNumber = itemRecord.findLineItemValue(lineItemType, 'pricelevel', onlineRetailPriceLevel);

            var onLinePrice = itemRecord.getLineItemMatrixValue(lineItemType, 'price', onLinePriceLevelLineNumber, 1);
            var markdownPrice = itemRecord.getLineItemMatrixValue(lineItemType, 'price', markdownPriceLevelLineNumber, 1);
            var onLineRetailPrice = itemRecord.getLineItemMatrixValue(lineItemType, 'price', onLineRetailPriceLevelLineNumber, 1);

            //if(onLinePrice && markdownPrice && onLineRetailPrice)
            //{
            //}
            EC.logit(logContext, 'All prices are populated');
            if(performStartLogic)
            {
                EC.logit(logContext, 'Beginning StartDate logic');
                EC.logit(logContext, 'onLinePrice <> markdownPrice');
                if(scheduleRecord.custrecord_update_online_retail_price)
                {
                    if(onLinePrice && markdownPrice && onLinePrice != markdownPrice)
                    {
                        // Copy the onLinePrice to the onLineRetailPrice and markdown price to the onLinePrice
                        EC.logit(logContext, 'Copy the onLinePrice to the onLineRetailPrice and markdown price to the onLinePrice');
                        itemRecord.selectLineItem(lineItemType, onLineRetailPriceLevelLineNumber);
                        itemRecord.setCurrentLineItemMatrixValue(lineItemType, 'price', 1, onLinePrice);
                        itemRecord.commitLineItem(lineItemType);

                        // Copy the markdownPrice to the onLinePrice
                        itemRecord.selectLineItem(lineItemType, onLinePriceLevelLineNumber);
                        itemRecord.setCurrentLineItemMatrixValue(lineItemType, 'price', 1, markdownPrice);
                        itemRecord.commitLineItem(lineItemType);
                        saveItem = true;
                    }
                }
                else
                {
                    if(markdownPrice && onLinePrice != markdownPrice)
                    {
                        EC.logit(logContext, 'Copy the markdown price to the onLinePrice');
                        // Copy the markdownPrice to the onLinePrice
                        itemRecord.selectLineItem(lineItemType, onLinePriceLevelLineNumber);
                        itemRecord.setCurrentLineItemMatrixValue(lineItemType, 'price', 1, markdownPrice);
                        itemRecord.commitLineItem(lineItemType);
                        saveItem = true;
                    }
                }
            }
            if(performEndLogic)
            {
                EC.logit(logContext, 'Beginning EndDate logic');
                // Validate that the onLinePrice <> onLineRetailPrice
                if(onLineRetailPrice && onLinePrice != onLineRetailPrice)
                {
                    EC.logit(logContext, 'Copy the onLineRetailPrice to the onLinePrice');

                    // Copy the onLineRetailPrice to the onLinePrice
                    itemRecord.selectLineItem(lineItemType, onLinePriceLevelLineNumber);
                    itemRecord.setCurrentLineItemMatrixValue(lineItemType, 'price', 1, onLineRetailPrice);
                    itemRecord.commitLineItem(lineItemType);
                    saveItem = true;
                }
            }
        });

        if(saveItem) itemRecord.save(false, true); // 10 units
    }
    catch(e1)
    {
        var startDateStatus = null;
        var endDateStatus = null;

        if(performStartLogic)
        {
            startDateStatus = EC.Settings.PriceAdjustmentScheduleStatus.CompletedWithErrors;
            scheduleRecord.custrecord_start_date_status = EC.Settings.PriceAdjustmentScheduleStatus.CompletedWithErrors;
        }
        if(performEndLogic)
        {
            endDateStatus = EC.Settings.PriceAdjustmentScheduleStatus.CompletedWithErrors;
            scheduleRecord.custrecord_end_date_status = EC.Settings.PriceAdjustmentScheduleStatus.CompletedWithErrors;
        }

        EC.itemLevelErrors += '\r' + itemName + ': ' + e1.message;
    }
};

EC.updatePriceAdjustmentSchedule = function(internalID, startDateStatus, endDateStatus, errorMessage)
{
    var fields = ['custrecord_price_adj_sch_error_msg'];
    var values = [errorMessage];

    if(startDateStatus)
    {
        fields.push('custrecord_start_date_status');
        values.push(startDateStatus);
    }
    if(endDateStatus)
    {
        fields.push('custrecord_end_date_status');
        values.push(endDateStatus);
    }

    nlapiSubmitField('customrecord_markdown_price_level', internalID, fields, values, false);
};