/**
 * Company            Explore Consulting
 * Copyright          2015 Explore Consulting, LLC
 * Type               NetSuite EC_UserEvent_Customer.js
 * Version            1.0.0.0
 * Description        This scripts primary functionality is to validate the customer record to identify whether or not
 *                      to update the native netsuite access. If validation fails, do not update the customer. If
 *                      validation passes, then it will grant the customer generic customer center role access and
 *                      generate them a password.
 *
 *                      Joel McConaughy.  Updated to set account status = 1 for all new POS customers but only give access to new POS customers from Canadian subsidiary.
 **/

// NetSuite property list for Customer records
var nsCustomerProps = [
    'custentity_ra_externalid'
    , 'custentity_acct_status'
    , 'giveaccess'
    , 'accessrole'
    , 'password'
    , 'password2'
    , 'subsidiary'
];

var candianSubsidiary = 3;

EC.onBeforeSubmit = function(type){
    try{
        if ( nlapiGetFieldValue('email') == "monicadhosseini@gmail.com" || nlapiGetFieldValue('email') == "stephanie.g.hunt@gmail.com" )
            Log.d("ELENA QC CHECK EMAIL FOUND", nlapiGetFieldValue('firstname') + " " + nlapiGetFieldValue('lastname'));
    }
    catch(e){
        Log.e('onBeforeSubmit', 'Unexpected Error');
    }
};

EC.onAfterSubmit = function(type){
    try{
        if(type != 'edit' && type != 'create')
            return;

        EC.updateCustomerAccess();
    }
    catch(e){
        Log.e('Unexpected Error');
    }
};

/**
 * Functioned defined to validate the customer record to identify whether or not the access needs to be generated for
 * the customer. If validation passes, it will grant the customer customer center access and generate a password for the
 * customer. It will also populate other sufficient information pertaining the customer granted access
 */
EC.updateCustomerAccess = function(){
    try{
        var customerRecord = nsdal.loadObject('customer', nlapiGetRecordId(), nsCustomerProps);
        Log.d('customerRecord', customerRecord);

        // Validate that the customer is in a valid state to update the access for the customer
        if(EC.validateCustomerAccess(customerRecord)){
            Log.d('Updating Customer Access');

            // Update the customer record fields for access
            if (customerRecord.subsidiary == candianSubsidiary) {
                customerRecord.giveaccess = true;
                customerRecord.accessrole = 14; // 14 = Customer Center
                customerRecord.password = 'POS' + nlapiGetRecordId();
                customerRecord.password2 = 'POS' + nlapiGetRecordId();
            }
            customerRecord.custentity_acct_status = 1; // 1 == POS-NP
            Log.d('Access Level Fields Updated');

            var id = customerRecord.save();
            Log.d('Customer Record Updated', id);
        }
        else{
            Log.d('Not Updating Customer Access. Validation Failed.');
        }
    }
    catch(e){
        getExceptionDetail(e);
    }
};

/**
 * Function defined to identify whether or not the customer record is in the correct state to update the access for
 * the customer. If all validation passes this function returns true. Otherwise if any validaiton fails, returns false.
 * @param customerRecord nsdal loaded customer record with predefined properties
 * @returns {boolean}
 */
EC.validateCustomerAccess = function(customerRecord){
    var isValid = true;

    if(!customerRecord.custentity_ra_externalid){
        isValid = false;
        Log.a('Invalid Customer. RA External ID is Empty. Do not Update Access for Customer.');
    }
    if(customerRecord.custentity_acct_status){
        isValid = false;
        Log.a('Invalid Customer. Account Status is already Defined. Do not Update Access for Customer.');
    }
    if(customerRecord.giveaccess){
        isValid = false;
        Log.a('Invalid Customer. Customer already has Access. Do not Update Access for Customer.');
    }

    return isValid;
};

/**
 * Takes a netsuite error object and parses out the details of the message into a friendly string in a response
 * @param {nlobjError|Object|Error} e
 * @returns {string}
 */
function getExceptionDetail(e) {
    var detail = e.toString() + " \n";
    if (e.stack) detail += e.stack;
    else if (_.isFunction(e.getStackTrace)) detail += e.getStackTrace().join();
    else detail += "[no stack trace]";
    return detail;
}

Log.AutoLogMethodEntryExit();