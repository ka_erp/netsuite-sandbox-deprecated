/**
 * Company            Explore Consulting
 * Copyright            2014 Explore Consulting, LLC
 * Type                NetSuite EC_Scheduled_MassDelete
 * Version            1.0.0.0
 **/

function onStart() {
    deleteRecords();
}

function deleteRecords()
{
    try
    {
        nlapiLogExecution('DEBUG','deleteRecords', 'STARTING');
        var results = nlapiSearchRecord('customrecord_inv_adj_stage', null, null, null);         // 530
        if ( results )
        {
            nlapiLogExecution('DEBUG','deleteRecords', 'results.length:  ' + results.length);
            if ( results.length == 1000 )
                var rerun = true;
            else
                var rerun = false;

            var context = nlapiGetContext();
            for ( var i=0; context.getRemainingUsage() > 50 && i<results.length; i++ )
            {
                var recID = results[i].getId();
                var recType = results[i].getRecordType();
                nlapiLogExecution('DEBUG','deleteRecords', 'Starting Processing for Record Type: ' + recType + '  ID:  ' + recID);

                try{
                    nlapiDeleteRecord(recType, recID);
                }
                catch(e)
                {
                    nlapiLogExecution('DEBUG','deleteRecords', 'Ending Processing for Record ID:  ' + recID);
                }
            }

            if ( context.getRemainingUsage() <= 50 || rerun == true)
            {
                nlapiLogExecution('DEBUG','Scheduling Script to continue updating');
                var result = nlapiScheduleScript('customscript_ec_sch_mass_delete', null, null);                  // 20 units
                nlapiLogExecution('DEBUG','Scheduling Script RESULT:  ' + result);
                if ( result != 'QUEUED' )
                {
                    nlapiLogExecution('DEBUG','scheduled script not queued',
                        'expected QUEUED but scheduled script api returned ' + result);
                    var emailBody5 = "Script: EC_Scheduled_MassDelete.js\nFunction: deleteRecords\nError: There was an issue re-scheduling the scheduled script in this script";
                    nlapiSendEmail(125247, "rhackey@exploreconsulting.com", "Error in EC_Scheduled_MassDelete.js", emailBody5);
                }
                else
                {
                    nlapiLogExecution('DEBUG','deleteRecords -- Re-Scheduling Script Status', "Result:  " + result);
                }
            }
        }
        else
        {
            nlapiLogExecution('DEBUG','deleteRecords', 'No Search Results Found');
        }
    }
    catch(e)
    {
        nlapiLogExecution('DEBUG','deleteRecords()', "An Unexpected Error Occurred: " + e);
    }
}
