/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_WorkflowAction_SendStoreReceiptEmail
 * Version            1.0.0.0
 **/

if ( nlapiGetContext().getEnvironment() == "PRODUCTION" ) {
    var folder = "301654";                                    // TODO:  Update all ids when released to Production
    var EmailTemplates = {
        Inv: 17,
        CM: 18,
        HTML_TEMPLATE: 499487
    };
}
else
{
    /** SANDBOX 3 VARIABLES */
    /*var folder = "301654";
    var EmailTemplates = {
        Inv: 17,
        CM: 18,
        HTML_TEMPLATE: 499487
    };*/

    /** SANDBOX 5 VARIABLES */
    var folder = "456918";
    var EmailTemplates = {
        Inv: 19,
        CM: 20,
        HTML_TEMPLATE: 663988
    };
}


function onStart(type) {

    nlapiLogExecution("DEBUG", "onStart - HELLO", "TYPE:  " + type);
    // Record Information
    var recid = nlapiGetRecordId();
    //var invoice = nlapiLoadRecord(nlapiGetRecordType(), recid);
    var invoice = nlapiGetNewRecord();
    nlapiLogExecution("DEBUG", "RECORD LOADED - Type:  " + nlapiGetRecordType(), "RECORD ID:  " + recid);

    EC.PopulateCustomFields(invoice);

    // Get Email Template Body
    // Set up email merger
    var templateID = (nlapiGetRecordType() == "invoice" || nlapiGetRecordType() == "INVOICE" ? EmailTemplates.Inv : EmailTemplates.CM);
    var titleText = (nlapiGetRecordType() == "invoice" || nlapiGetRecordType() == "INVOICE" ? "INVOICE" : "RETURN");
    var emailMerger = nlapiCreateEmailMerger(templateID);
    emailMerger.setTransaction(recid);
    var mergeResult = emailMerger.merge();
    // Get email subject
    var emailSubject = mergeResult.getSubject();
    // Get email body
    var templatebody = mergeResult.getBody();
    nlapiLogExecution("DEBUG", "TEMPLATE BODY ESTABLISHED", "");

    var file = nlapiLoadFile(EmailTemplates.HTML_TEMPLATE);
    nlapiLogExecution("DEBUG", "FILE LOADED", "");
    var fileContents = file.getValue();

    // build up BFO-compliant XML using well-formed HTML
    var xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE pdf PUBLIC \"-//big.faceless.org//report\" \"report-1.1.dtd\">\n";
    xml += "<pdf>\n";
    xml += fileContents;
    xml += '\n</pdf>';

    // Get Location Specific Data
    var fields = ['address1', 'address2', 'address3', 'city', 'state', 'zip', 'custrecord_ka_shop_email', /*'custrecord_loc_store_return_policy'*/'custrecord_loc_ret_pol_text'];
    var locInfo = nlapiLookupField("location", invoice.getFieldValue('location'), fields);

    //var returnPolicyText = nlapiLookupField("customrecord_return_policy", locInfo.custrecord_loc_store_return_policy, "custrecord_return_policy_text");

    var barcode = "";
    /*if (nlapiGetRecordType() == "creditmemo" || nlapiGetRecordType() == 'CREDITMEMO')
        barcode = invoice.getFieldValue('custbody_extid_barcode').replace("-RETURNS", "");
    else
        barcode = invoice.getFieldValue('custbody_extid_barcode');
    */
    if (nlapiGetRecordType() == "creditmemo" || nlapiGetRecordType() == 'CREDITMEMO')
        barcode = invoice.getFieldValue('custbody1').replace("-RETURNS", "");
    else
        barcode = invoice.getFieldValue('custbody1');

    // Get Customer Data
    var fields2 = ['firstname', 'lastname'];
    var custInfo = nlapiLookupField("customer", invoice.getFieldValue('entity'), fields2);

    var lineCount = invoice.getLineItemCount("item");
    var itemHTML = "";
    // Header Row
    itemHTML += '<table width="100%"><tr>';
    itemHTML += '<td align="left">ITEM</td>';
    itemHTML += '<td align="right">COLOUR</td>';
    itemHTML += '<td align="right">QTY</td>';
    itemHTML += '<td align="right">SIZE</td>';
    itemHTML += '<td align="right">PRICE</td>';
    itemHTML += '<td align="right">SUBTOTAL</td>';
    itemHTML += "</tr>";

    for ( var i=1; i<=lineCount; i++ ){

        // Special Handling for Discount Items
        var itemtype = invoice.getLineItemValue('item', 'itemtype', i);
        if (itemtype == "Discount"){
            itemHTML += "<tr>";
            itemHTML += '<td>' + invoice.getLineItemValue("item", "description", i) + '</td>';
            itemHTML += '<td></td>';
            itemHTML += '<td></td>';
            itemHTML += '<td></td>';
            itemHTML += '<td align="right">' + invoice.getLineItemValue("item", "rate", i) + '</td>';
            itemHTML += '<td align="right">' + invoice.getLineItemValue("item", "amount", i) + '</td>';
            itemHTML += "</tr>";
        }
        else
        {
            // Item Lookup for Style Description & UPC CODE                // TODO:  Possible governance issue
            var itemInfo = nlapiLookupField("item", invoice.getLineItemValue("item", "item", i),['upccode','custitem_ka_item_style_description']);

            itemHTML += "<tr>";
            itemHTML += '<td>' + itemInfo.custitem_ka_item_style_description + '<br />' + itemInfo.upccode + '</td>';
            itemHTML += '<td align="right">' + invoice.getLineItemText("item", "custcol_prewfxcolor", i) + '</td>';         //custcol_item_color
            itemHTML += '<td align="right">' + invoice.getLineItemValue("item", "quantity", i) + '</td>';
            itemHTML += '<td align="right">' + invoice.getLineItemText("item", "custcol_item_size", i) + '</td>';
            itemHTML += '<td align="right">' + invoice.getLineItemValue("item", "rate", i) + '</td>';
            itemHTML += '<td align="right">' + invoice.getLineItemValue("item", "amount", i) + '</td>';
            itemHTML += "</tr>";
        }
    }

    // Adding Tax Lines
    var tax2 = invoice.getFieldValue('tax2total');
    if ( tax2 ) {
        itemHTML += "<tr>";
        itemHTML += '<td></td>';
        itemHTML += '<td></td>';
        itemHTML += '<td></td>';
        itemHTML += '<td></td>';
        itemHTML += '<td align="right">GST</td>';
        itemHTML += '<td align="right">';
        if (nlapiGetRecordType() == "creditmemo" || nlapiGetRecordType() == "CREDITMEMO") {
            itemHTML += '&#45;';
        }
        itemHTML += invoice.getFieldValue("taxtotal") + '</td>';
        itemHTML += "</tr>";

        itemHTML += "<tr>";
        itemHTML += '<td></td>';
        itemHTML += '<td></td>';
        itemHTML += '<td></td>';
        itemHTML += '<td></td>';
        itemHTML += '<td align="right">PST</td>';
        itemHTML += '<td align="right">';
        if (nlapiGetRecordType() == "creditmemo" || nlapiGetRecordType() == "CREDITMEMO") {
            itemHTML += '&#45;';
        }
        itemHTML += invoice.getFieldValue("tax2total") + '</td>';
        itemHTML += "</tr>";
    }
    else
    {
        itemHTML += "<tr>";
        itemHTML += '<td></td>';
        itemHTML += '<td></td>';
        itemHTML += '<td></td>';
        itemHTML += '<td></td>';
        itemHTML += '<td align="right">Tax</td>';
        itemHTML += '<td align="right">';
        if (nlapiGetRecordType() == "creditmemo" || nlapiGetRecordType() == "CREDITMEMO") {
            itemHTML += '&#45;';
        }
        itemHTML += invoice.getFieldValue("taxtotal") + '</td>';
        itemHTML += "</tr>";
    }

    // Adding Gift Certificate Lines
    if ( nlapiGetRecordType() == "invoice" || nlapiGetRecordType() == "INVOICE" ) {
        var gc = invoice.getFieldValue('giftcertapplied');
        if (gc != 0) {
            itemHTML += "<tr>";
            itemHTML += '<td></td>';
            itemHTML += '<td></td>';
            itemHTML += '<td></td>';
            itemHTML += '<td></td>';
            itemHTML += '<td align="right">Gift Certificate</td>';
            itemHTML += '<td align="right">' + invoice.getFieldValue("giftcertapplied") + '</td>';
            itemHTML += "</tr>";
        }
    }

    // Adding Total Line
    itemHTML += "<tr>";
    itemHTML += '<td></td>';
    itemHTML += '<td></td>';
    itemHTML += '<td></td>';
    itemHTML += '<td></td>';
    itemHTML += '<td align="right"><b>Total</b></td>';
    itemHTML += '<td align="right"><b>';
    if (nlapiGetRecordType() == "creditmemo" || nlapiGetRecordType() == "CREDITMEMO") {
        itemHTML += '&#45;';
    }
    itemHTML += invoice.getFieldValue("total") + '</b></td>';
    itemHTML += "</tr>";

    // Add Payment Amount line
    if ( invoice.getFieldValue('memo') ) {
        if (nlapiGetRecordType() == "invoice" || nlapiGetRecordType() == "INVOICE") {
            itemHTML += "<tr>";
            itemHTML += '<td></td>';
            itemHTML += '<td></td>';
            itemHTML += '<td></td>';
            itemHTML += '<td></td>';
            itemHTML += '<td align="right">' + invoice.getFieldValue('memo') + '</td>';
            itemHTML += '<td align="right">' + invoice.getFieldValue("amountpaid") + '</td>';
            itemHTML += "</tr>";
        }
        else     // Credit Memo Handling
        {
            itemHTML += "<tr>";
            itemHTML += '<td></td>';
            itemHTML += '<td></td>';
            itemHTML += '<td></td>';
            itemHTML += '<td></td>';
            itemHTML += '<td align="right">' + invoice.getFieldValue('memo') + '</td>';
            itemHTML += '<td align="right">&#45;' + invoice.getFieldValue("applied") + '</td>';
            itemHTML += "</tr>";
        }
    }

    itemHTML += "</table>";

    // Special Formatting needed for the Return Policy Text Block
    nlapiLogExecution("DEBUG", "******Return Policy Text", escape(locInfo.custrecord_loc_ret_pol_text));
    var escText = escape(locInfo.custrecord_loc_ret_pol_text);
    var newReturnPolicyText = escText.replace(/%0D%0A%0D%0A/g, "<br/><br/>");
    nlapiLogExecution("DEBUG", "******Still escaped - NEW", newReturnPolicyText);
    var finalReturnPolicyText = unescape(newReturnPolicyText);
    nlapiLogExecution("DEBUG", "******Final Text", finalReturnPolicyText);

    templatebody = templatebody.replace("%TRANS_NUMBER%", invoice.getFieldValue("custbody_extid_barcode_2"));

    try
    {
        // Do XML String Replacements
        xml = xml
            .replace("{NL_ITEM_TABLE}", itemHTML)
            .replace("{NL_BARCODE}", invoice.getFieldValue('custbody_extid_barcode'))
            .replace("{NL_BARCODE2}", barcode)
            .replace("{NL_LOC_ADDRESS1}", locInfo.address1 + "<br />")
            .replace("{NL_LOC_CITY}", locInfo.city)
            .replace("{NL_LOC_STATE}", locInfo.state)
            .replace("{NL_LOC_ZIP}", locInfo.zip)
            .replace("{NL_KA_SHOP_EMAIL}", locInfo.custrecord_ka_shop_email)
            .replace("{NL_RETURN_POLICY}", finalReturnPolicyText)
            .replace("{NL_CUSTBODYTIMESTAMP}", invoice.getFieldValue('custbodytimestamp'))
            .replace("{NL_NUMBER_TITLE}", titleText)
            .replace("{NL_INV_NUMBER_CUT}", EC.GetInvoiceNumber(invoice.getFieldValue('custbody_extid_barcode')))
            .replace("{NL_CUST_FIRSTNAME}", custInfo.firstname)
            .replace("{NL_CUST_LASTNAME}", custInfo.lastname)
            .replace("{NL_CUST_EMAIL}", invoice.getFieldValue('email'));

        if (locInfo.address2)
           xml = xml.replace("{NL_LOC_ADDRESS2}", locInfo.address2 + "<br />");
        else
            xml = xml.replace("{NL_LOC_ADDRESS2}", "");

        if (locInfo.address3)
            xml = xml.replace("{NL_LOC_ADDRESS3}", locInfo.address3 + "<br />");
        else
            xml = xml.replace("{NL_LOC_ADDRESS3}", "");


        nlapiLogExecution("DEBUG", "XML STRING ASSEMBLED", xml);

        // run the BFO library to convert the xml document to a PDF
        var storeReceipt = nlapiXMLToPDF( xml );
        if (nlapiGetRecordType() == "invoice" || nlapiGetRecordType() == "INVOICE")
            storeReceipt.setName("INV_" + invoice.getFieldValue("tranid") + ".pdf");
        else
            storeReceipt.setName("CM_" + invoice.getFieldValue("tranid") + ".pdf");
        storeReceipt.setFolder(folder);
        nlapiLogExecution("DEBUG", "XML TO PDF CONVERSION", "");

        var records = new Object();
        records['transaction'] = recid;

        nlapiSendEmail(1549, invoice.getFieldValue("email"), emailSubject, templatebody, null, null, records, storeReceipt);
        nlapiLogExecution("DEBUG", "EMAIL SENT", invoice.getFieldValue("email"));
    }
    catch(e)
    {
        nlapiLogExecution('DEBUG', 'xmlToPDF', 'Unexpected Error: ' + e);
        nlapiLogExecution('DEBUG', 'xmlToPDF', 'This error is most likely caused by XMLtoPDF Conversion. Check Content of Data being Displayed. Must be CSS2 standard.');
        nlapiSendEmail('-5', 'kasupport@exploreconsulting.com,rhackey@exploreconsulting.com', "Error occurred in EC_WorkflowAction_SendStoreReceiptEmail.js", "Review " + nlapiGetRecordType() + " Transaction:  " + nlapiGetRecordId() + "\nError:  " + e);
    }
}

EC.PopulateCustomFields = function(rec){

    rec.setFieldValue('custbody_extid_barcode', rec.getFieldValue('custbody1'));
    nlapiLogExecution("DEBUG", "Record Type", nlapiGetRecordType());

    if ( nlapiGetRecordType() == "creditmemo" ) {
        // We are having an issue with the spliced External ID being printed as the Credit Number on
        // the scriptable email template.  So I am using another field to capture this so freemarker
        // doesn't have to deal with the splicing.
        var cutid = GetReturnNumber(rec.getFieldValue('custbody_extid_barcode'));
        rec.setFieldValue("custbody_extid_barcode_2", cutid);
    }
    else if ( nlapiGetRecordType() == "invoice")
    {
        var cutid = GetInvoiceNumber_WFA(rec.getFieldValue('custbody_extid_barcode'));
        rec.setFieldValue("custbody_extid_barcode_2", cutid);
    }
    else
    {
        var cutid = GetReturnNumber(rec.getFieldValue('custbody1'));
        rec.setFieldValue("custbody_extid_barcode_2", cutid);
    }
};

/**
 * Function that will return the appropriate Invoice Number to display on the Store Receipt Email
 * @param extID
 * @returns {*}
 * @constructor
 */
EC.GetInvoiceNumber = function(extID){
    if ( !extID || extID.length < 8 )
        return extID;

    // For Invoice we will take the External ID and only display the last eight digits
    if ( nlapiGetRecordType() == "invoice" || nlapiGetRecordType() == "INVOICE" )
        return extID.substr(extID.length - 8);
    else {      // Handling for Credit Memo Return Store Receipts
        // First return the "-return" string from the external id
        extID = extID.replace("-RETURNS", "");
        // Then return the last eight digits of the string.
        return extID.substr(extID.length - 8);
    }

};

/**
 * Function that will return the appropriate Return Number to display on the CM Store Receipt Email
 * @param extID
 * @returns {*}
 * @constructor
 */
function GetReturnNumber(extID){
    if ( !extID || extID.length < 8 )
        return extID;

    extID = extID.replace("-RETURNS", "");
    return extID.substr(extID.length - 8);
}

/**
 * Function that will return the appropriate Return Number to display on the CM Store Receipt Email
 * @param extID
 * @returns {*}
 * @constructor
 */
function GetInvoiceNumber_WFA(extID){
    if ( !extID || extID.length < 8 )
        return extID;

    return extID.substr(extID.length - 8);
}

