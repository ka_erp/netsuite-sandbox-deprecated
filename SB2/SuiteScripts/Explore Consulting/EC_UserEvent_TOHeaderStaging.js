/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_UserEvent_TOHeaderStaging
 * Version            1.0.0.0
 **/


function onBeforeLoad(type, form, request) {

}

function onBeforeSubmit(type) {

}

function onAfterSubmit(type) {

    if ( nlapiGetFieldValue('custrecord_tranfer_stage_status') != TO_HeaderStagingStatus.PendingApproval ) return;
    if ( type == 'create' || type == 'edit' || type == 'xedit' ) EC.checkChildRecordsForErrors();
    if ( type == 'edit' || type == 'xedit' ) EC.validateHeaderEntry();
}

EC.checkChildRecordsForErrors = function(){
    try
    {
        var rec = nsdal.loadObject(nlapiGetRecordType(), nlapiGetRecordId(), HeaderStagingRecordProperties);

        if ( !rec.custrecord_transfer_error_message )
            var errorMsg = "";
        else if ( rec.custrecord_transfer_error_message && rec.custrecord_transfer_error_message == "There are no TO Line Staging records found at all for this TO Header Record.  Please review." )
            var errorMsg = "";
        else
            var errorMsg = rec.custrecord_transfer_error_message;

        if ( !EC.AnyLineRecordsFound(rec.id) ){
            errorMsg += "There are no TO Line Staging records found at all for this TO Header Record.  Please review.  ";
            Log.d("AnyLineRecordsFound - No TO Line Staging records found.", msg);

            rec.custrecord_tranfer_stage_status = TO_HeaderStagingStatus.Error_ToBeReviewed;
            rec.custrecord_transfer_error_message = errorMsg;
            var id2 = rec.save(true, true);
            Log.d("checkChildRecordsForErrors", "Re-saved record:  " + id2);
        }
        else
        {
            // Since we know there are now line records connected, remove the error
            if ( rec.custrecord_transfer_error_message ) {
                var newmsg = rec.custrecord_transfer_error_message.replace("There are no TO Line Staging records found at all for this TO Header Record.  Please review.", "");
                rec.custrecord_transfer_error_message = newmsg;
                rec.custrecord_tranfer_stage_status = TO_HeaderStagingStatus.PendingApproval;
            }

            var msg = EC.AllRecordsComplete(rec.id);

            if ( msg ) {
                if (errorMsg)
                    errorMsg += "  " + msg;
                else
                    errorMsg = msg;
            }

            if ( errorMsg != "" )
            {
                rec.custrecord_tranfer_stage_status = TO_HeaderStagingStatus.Error_ToBeReviewed;
                rec.custrecord_transfer_error_message = errorMsg;
            }

            var id = rec.save(true, true);
            Log.d("checkChildRecordsForErrors", "Re-saved record:  " + id);
        }

    }
    catch(e){
        Log.d("checkChildRecordsForErrors", "Unexpected error while checking child records:  " + e);
    }
};

EC.validateHeaderEntry = function(){
    try
    {
        var rec = nsdal.loadObject(nlapiGetRecordType(), nlapiGetRecordId(), HeaderStagingRecordProperties);
        var errorMsg = rec.custrecord_transfer_error_message || "";

        if ( !rec.custrecord_transfer_reference_number )
            errorMsg += "No Transfer Reference Number found.  ";

        if ( !rec.custrecord_transfer_stage_date )
            errorMsg += "No Transfer Stage Date found.  ";

        if ( !rec.custrecord_transfer_subsidiary )
            errorMsg += "Invalid Subsidiary id.  ";

        if ( !rec.custrecord_transfer_from_location )
            errorMsg += "Invalid From Location external id.  ";

        if ( !rec.custrecord_transfer_to_location )
            errorMsg += "Invalid To Location external id.  ";

        if ( !rec.custrecord_transf_stage_expe_ship_date )
            errorMsg += "No Expected Ship Date found.  ";

        if ( !rec.custrecord_expected_receipt_date )
            errorMsg += "No Expected Receipt Date found.  ";


        if ( errorMsg != "" )
        {
            rec.custrecord_tranfer_stage_status = TO_HeaderStagingStatus.Error_ToBeReviewed;
            rec.custrecord_transfer_error_message = errorMsg;
        }

        var id = rec.save(true, true);
        Log.d("validateHeaderEntry", "Re-saved record:  " + id);
    }
    catch(e){
        Log.d("validateHeaderEntry", "Unexpected error while validating entry:  " + e);
    }
};

EC.AllRecordsComplete = function(currentID){

    var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"),
        new nlobjSearchFilter("custrecord_transfer_stage_header", null, "anyof", currentID),
        new nlobjSearchFilter("custrecord_transfer_stage_line_status", null, "anyof", [TO_LineStagingStatus.ErrorReview])];
    var columns = [new nlobjSearchColumn("internalid"), new nlobjSearchColumn("custrecord_transfer_stage_line_msg")];
    var detailLeft = nlapiSearchRecord("customrecord_transfer_order_staging_line", null, filters, columns);

    if ( detailLeft ) {
        var msg = "There are still " + detailLeft.length + " TO line records to review.  Details:\n";
        for ( var d=0; d<detailLeft.length; d++ ){
            msg += detailLeft[d].getId() + " (" + detailLeft[d].getValue("custrecord_transfer_stage_line_msg") + ")   ";
        }

        Log.d("AllRecordsComplete - Details from Line records that aren't yet complete.", msg);
        return msg;
    }
    else return undefined;
};

EC.AnyLineRecordsFound = function(currentID){

    var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"),
        new nlobjSearchFilter("custrecord_transfer_stage_header", null, "anyof", currentID)];
    var columns = [new nlobjSearchColumn("internalid")];
    var detailLeft = nlapiSearchRecord("customrecord_transfer_order_staging_line", null, filters, columns);

    if ( detailLeft && detailLeft.length > 0 )
        return true;
    else
        return false;
};

Log.AutoLogMethodEntryExit(null, true, true, true);
