/**
 *	File Name		:	WTKA_Customer_Library.js
 *	Function		:	Library functions file for Customer record
 * 	Authors			:	Rini Thomas
 *	Company			:	Wipro Limited
 *	Release Dates	:	DD-MMM-2016 (v1.0)
 * 	Current Version	:	1.0
**/

/* Variables to be cleared on function call script file :
	var customerUpdateFail		= 0,	 subsidiary;
	var sendEmail 		 		= true,	 changed = false;
*/

function customerUpdate(dataIn, requestType) //requestType - order request
{
	try
	{
		var customer = 0, 		custNo, 	record;
		var existing = false;
		if(PrevalidationCustomerCheck(dataIn))
		{
			var locale  = dataIn.locale.split('/');
			var country = locale[locale.length-1].toUpperCase(); //Locale - kitandace.com/ca
			if(country == "UK")	country = "GB";
			var subsid = FetchSubsidiaryId('country', country);
			
			if(subsid == -1)
			{
				ErrorObj.messages[0] = new Object();
				ErrorObj.messages[0].status   		= "Error";
				ErrorObj.messages[0].messagetype    = "Subsidiary error";
				ErrorObj.messages[0].message 	    = "Invalid Query. Error: Subsidiary for Country: " + country + " cannot be found.";
				nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
				if(requestType == null)
				{
					sendCustomerErrorEmail(dataIn, ErrorObj);
					var errorLogs = LogEntityCreation(1, 4, customer, dataIn, 2, ErrorObj);
				}
				return ErrorObj;
			}
			
			//Lookup Existing Customers using Email
			var cols 		 = new Array();
			cols.push(new nlobjSearchColumn('entityid'));
			cols.push(new nlobjSearchColumn('subsidiary'));
			var filters 	 = new Array();
			filters.push(new nlobjSearchFilter('email',  	  null, 'is', 	dataIn.customerEmail));
			filters.push(new nlobjSearchFilter('subsidiary',  null, 'is', 	subsid.code));
			var searchResult = nlapiSearchRecord('customer',  null, filters, cols);
			if(searchResult != null)
			{
				customer 	= searchResult[0].getId();
				custNo	 	= searchResult[0].getValue('entityid');
				subsidiary	= searchResult[0].getValue('subsidiary');
				existing 	= true;
			}
			
			try
			{
				if(existing)
				{
					record = nlapiLoadRecord('customer', customer); //Update
					var recStat = record.getFieldValue('entitystatus');
					changed = (recStat != 13);
				}
				else
				{
					record = nlapiCreateRecord('customer'); //Create
					record.setFieldValue('subsidiary', subsid.code);
					record.setFieldValue('email', 		dataIn.customerEmail);
					changed = true;
				}
				
				/* Set Names */
				var firstName = " ";
				var lastName  = " ";
				if(checkEmptyObject(dataIn.firstName, "FirstName"))
				{
					if(existing)
					{
						var recFirstName = record.getFieldValue('firstname');
						changed = (recFirstName != dataIn.firstName);
					}
					firstName = dataIn.firstName;
				}
				if(checkEmptyObject(dataIn.lastName, "LastName"))
				{
					if(existing)
					{
						var recLastName = record.getFieldValue('lastname');
						changed = (recLastName != dataIn.lastName);
					}
					lastName = dataIn.lastName;
				}
				record.setFieldValue('firstname', 	firstName);
				record.setFieldValue('lastName', 	lastName);
				
				/* Set Other Values */
				record.setFieldValue('entitystatus', 				13); //Customer - Closed Won
				record.setFieldText('globalsubscriptionstatus', 	'Soft Opt-In');
				record.setFieldValue('custentity_newsletteroptin', 	'T');		
				
				/* Set Addresses */
				//Check if address should be set
				var setBillingAddressFlag = false, 	 setShippingAddressFlag = false;
				if(emptyObject(dataIn.billingAddress,									"BillingAddress"))
				{
					if(checkEmptyObject(dataIn.billingAddress.billingFirstName,			"BillingFirstName"))			setBillingAddressFlag  = true;
					if(checkEmptyObject(dataIn.billingAddress.billingLastName,			"BillingLastName"))				setBillingAddressFlag  = true;
					if(checkEmptyObject(dataIn.billingAddress.billingAddressLine1,		"BillingAddressLine1"))			setBillingAddressFlag  = true;
					if(checkEmptyObject(dataIn.billingAddress.cityName,					"BillingCity"))			 		setBillingAddressFlag  = true;
					if(checkEmptyObject(dataIn.billingAddress.provinceName,				"BillingProvinceName"))  		setBillingAddressFlag  = true;
					if(checkEmptyObject(dataIn.billingAddress.postalCode,				"BillingPostalCode")) 	 		setBillingAddressFlag  = true;
					if(checkEmptyObject(dataIn.billingAddress.countryName,				"BillingCountry")) 		 		setBillingAddressFlag  = true;
				}
				
				if(emptyObject(dataIn.shippingAddress,									"ShippingAddress"))
				{
					if(checkEmptyObject(dataIn.shippingAddress.recipientFirstName,		"ShippingFirstName")) 	 		setShippingAddressFlag  = true;
					if(checkEmptyObject(dataIn.shippingAddress.recipientLastName,		"ShippingLastName"))	 		setShippingAddressFlag  = true;
					if(checkEmptyObject(dataIn.shippingAddress.shippingAddressLine1,	"ShippingAddressLine1"))		setShippingAddressFlag  = true;
					if(checkEmptyObject(dataIn.shippingAddress.cityName,				"ShippingCity"))		 		setShippingAddressFlag  = true;
					if(checkEmptyObject(dataIn.shippingAddress.provinceName,			"ShippingProvinceName")) 		setShippingAddressFlag  = true;
					if(checkEmptyObject(dataIn.shippingAddress.postalCode,				"ShippingPostalCode"))	 		setShippingAddressFlag  = true;
					if(checkEmptyObject(dataIn.shippingAddress.countryName,				"ShippingCountry"))		 		setShippingAddressFlag  = true;
				}
				
				var sameAddressList = false;
				if(setBillingAddressFlag && setShippingAddressFlag && compareAddresses(dataIn))
				{
					sameAddressList = true;
				}
				
				if(setBillingAddressFlag)
				{
					var subrecord;
					var billingIndex = findNewAddress(record, dataIn, 'billingAddress');
					// nlapiLogExecution('debug', 'billingIndex', billingIndex);
					if(billingIndex > 0) // Do Nothing - Match
					{
						record.selectLineItem('addressbook', billingIndex);
						if(record.getCurrentLineItemValue('addressbook', 'defaultbilling') 		!= 'T')	changed = true;
						record.setCurrentLineItemValue('addressbook', 'defaultbilling',  'T');
						if(sameAddressList)
						{
							if(record.getCurrentLineItemValue('addressbook', 'defaultshipping') != 'T')	changed = true;
							record.setCurrentLineItemValue('addressbook', 'defaultshipping',  'T');
						}
						record.setCurrentLineItemValue('addressbook', 'isresidential',   'F'); 
						subrecord = record.editCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
					}
					else
					{
						changed = true;
						record.selectNewLineItem('addressbook');
						record.setCurrentLineItemValue('addressbook', 'defaultbilling',  'T');
						if(sameAddressList)	record.setCurrentLineItemValue('addressbook', 'defaultshipping',  'T');
						record.setCurrentLineItemValue('addressbook', 'isresidential',   'F'); 
						subrecord = record.createCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
					
						var firstName = " ";
						var lastName  = " ";
						if(checkEmptyObject(dataIn.billingAddress.billingFirstName,		"BillingFirstName"))	firstName 	 = dataIn.billingAddress.billingFirstName;
						if(checkEmptyObject(dataIn.billingAddress.billingLastName,	"BillingLastName"))			lastName = dataIn.billingAddress.billingLastName;
						subrecord.setFieldValue('addressee', firstName + ' ' + lastName);
						
						if(checkEmptyObject(dataIn.customerPhoneNumber,					"CustomerPhoneNumber"))		subrecord.setFieldValue('addrphone', 		dataIn.customerPhoneNumber);
						if(checkEmptyObject(dataIn.billingAddress.billingAddressLine1,	"BillingAddressLine1"))		subrecord.setFieldValue('addr1', 			dataIn.billingAddress.billingAddressLine1);
						if(checkEmptyObject(dataIn.billingAddress.billingAddressLine2,	"BillingAddressLine2"))		subrecord.setFieldValue('addr2', 			dataIn.billingAddress.billingAddressLine2);
						if(checkEmptyObject(dataIn.billingAddress.cityName,				"BillingCity"))				subrecord.setFieldValue('city', 			dataIn.billingAddress.cityName);
						if(checkEmptyObject(dataIn.billingAddress.provinceName,			"BillingProvinceName"))		subrecord.setFieldValue('state', 			dataIn.billingAddress.provinceName);
						if(checkEmptyObject(dataIn.billingAddress.countryName,			"BillingCountry"))			subrecord.setFieldValue('country', 			dataIn.billingAddress.countryName);
						if(checkEmptyObject(dataIn.billingAddress.postalCode,			"BillingPostalCode"))		subrecord.setFieldValue('zip', 				dataIn.billingAddress.postalCode);
					}
					subrecord.commit();
					record.commitLineItem('addressbook');
				}
				if(setShippingAddressFlag && !sameAddressList)
				{
					var subrecord;
					var shippingIndex = findNewAddress(record, dataIn, 'shippingAddress');
					// nlapiLogExecution('debug', 'shippingIndex', shippingIndex);
					if(shippingIndex > 0) // Do Nothing - Match
					{
						record.selectLineItem('addressbook', shippingIndex);
						if(record.getCurrentLineItemValue('addressbook', 'defaultshipping') != 'T')	changed = true;
						record.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T');
						record.setCurrentLineItemValue('addressbook', 'isresidential',   'F'); 
						subrecord = record.editCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
					}
					else
					{
						changed = true;
						record.selectNewLineItem('addressbook');
						record.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T');
						record.setCurrentLineItemValue('addressbook', 'isresidential',   'F'); 
						subrecord = record.createCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
						
						var firstName = " ";
						var lastName  = " ";
						if(checkEmptyObject(dataIn.shippingAddress.recipientFirstName,		"ShippingFirstName"))	firstName 	 = dataIn.shippingAddress.recipientFirstName;
						if(checkEmptyObject(dataIn.shippingAddress.recipientLastName, 		"ShippingLastName"))	lastName = dataIn.shippingAddress.recipientLastName;
						subrecord.setFieldValue('addressee', firstName + ' ' + lastName);
												
						if(checkEmptyObject(dataIn.customerPhoneNumber,						"CustomerPhoneNumber"))		subrecord.setFieldValue('addrphone', 		dataIn.shippingAddress.recipientPhoneNumber);
						if(checkEmptyObject(dataIn.shippingAddress.shippingAddressLine1,	"ShippingAddressLine1"))	subrecord.setFieldValue('addr1', 			dataIn.shippingAddress.shippingAddressLine1);
						if(checkEmptyObject(dataIn.shippingAddress.shippingAddressLine2,	"ShippingAddressLine2"))	subrecord.setFieldValue('addr2', 			dataIn.shippingAddress.shippingAddressLine2);
						if(checkEmptyObject(dataIn.shippingAddress.cityName,				"ShippingCity"))			subrecord.setFieldValue('city', 			dataIn.shippingAddress.cityName);
						if(checkEmptyObject(dataIn.shippingAddress.provinceName,			"ShippingProvinceName"))	subrecord.setFieldValue('state', 			dataIn.shippingAddress.provinceName);
						if(checkEmptyObject(dataIn.shippingAddress.postalCode,				"ShippingPostalCode"))		subrecord.setFieldValue('zip', 				dataIn.shippingAddress.postalCode);
						if(checkEmptyObject(dataIn.shippingAddress.countryName,				"ShippingCountry"))			subrecord.setFieldValue('country', 			dataIn.shippingAddress.countryName);
					}	
					subrecord.commit();
					record.commitLineItem('addressbook');
				}
				
				if(requestType != null)
				{
					/* Set tax code */
					/* var state = dataIn.shippingAddress.provinceName.split('-');
					var province = state[state.length-1].toUpperCase();
					taxCode  	 = FetchTaxCode(country, province);
					
					if(existing)
					{
						var recTax = record.getFieldValue('taxitem');
						changed = (recTax != taxCode);
					}
					// record.setFieldValue('taxable', 'F');
					record.setFieldValue('taxitem', taxCode); */
					
					/* Set Multi-currency */
					var cols 		 = new Array();
					cols.push(new nlobjSearchColumn('name'));
					var filters 	 = new Array();
					var searchResult = nlapiSearchRecord('currency', null, filters, cols);
					var currencyList = new Array();
					
					if(searchResult != null && searchResult.length != record.getLineItemCount('currency'))
					{
						for(var i=0; searchResult != null && i<searchResult.length; i++)
						{
							var flag = false;
							var listCurrency = searchResult[i].getValue('name');
							for(var j=0; j<record.getLineItemCount('currency'); j++)
							{
								var recCurrency  = record.getLineItemText('currency', 'currency', j+1);
								if(recCurrency == listCurrency)
								{
									flag = true;
									break;
								}
							}
							if(!flag)	currencyList.push(searchResult[i].getId());
						}
						if(currencyList.length > 0)
						{
							for(var i= 0; i<currencyList.length; i++)
							{
								record.selectNewLineItem('currency');
								record.setCurrentLineItemValue('currency', 'currency', currencyList[i]);
								record.commitLineItem('currency');
							}
							changed = true;
						}
					}
				}
								
				if(changed)	customer = nlapiSubmitRecord(record, true, true); //Submit only for changes
				
				if(!existing)
				{
					var customerRecord 	= nlapiLoadRecord('customer', customer);
					custNo	   	   		= customerRecord.getFieldValue('entityid');
					subsidiary	  		= customerRecord.getFieldValue('subsidiary');
				}
				
				var SuccessObj 						= new Object();
				SuccessObj.status 					= "Success";
				SuccessObj.messages 				= new Array();
				SuccessObj.messages[0] 				= new Object();
				SuccessObj.messages[0].recordId 	= customer;
				SuccessObj.messages[0].customerId 	= custNo;
				SuccessObj.messages[0].message 		= "Customer successfully ";
				SuccessObj.messages[0].message 		+= (existing) ? "updated." : "created.";
				nlapiLogExecution('debug', 'SuccessObj', JSON.stringify(SuccessObj));
				if(requestType == null)	var customerLogs = LogEntityCreation(1, 4, customer, dataIn, 1, SuccessObj);
				return SuccessObj;
			}
			catch(err)
			{
				if(err.code == 'RCRD_HAS_BEEN_CHANGED') // Retry customer update
				{
					nlapiLogExecution('debug', 'Multiple Customer Update: ' + dataIn.order.orderHeader.orderId, 'Retrying customer master update');
					if(customerUpdateFail < 3) //Maximum re-try 2 times
					{
						customerUpdateFail++;
						return (createCustomer(dataIn), requestType);
					}
					else
					{
						ErrorObj.status   				 = "Exception";
						ErrorObj.messages[0] 			 = new Object();
						ErrorObj.messages[0].message 	 = "Customer could not be created or updated";
						ErrorObj.messages[0].message 	+= " Details as below."; 
						ErrorObj.messages[0].details 	 = err;
						nlapiLogExecution('debug', 'ErrorObj: ', JSON.stringify(ErrorObj));
						if(requestType == null)
						{
							sendCustomerErrorEmail(dataIn, ErrorObj);
							var errorLogs = LogEntityCreation(1, 4, customer, dataIn, 2, ErrorObj);			
						}
						return ErrorObj;
					}
				}
				else
				{
					ErrorObj.status 	 = 'Exception'; 
					ErrorObj.messages[0] = new Object();
					ErrorObj.messages[0].messagetype 	= (existing) ? "Customer Update Failure" : "Customer Creation Failure";
					ErrorObj.messages[0].message 		= (existing) ? "Customer " + custNo + " could not be updated" : "Customer could not be created.";
					ErrorObj.messages[0].message 		+= " Details as below."; 
					ErrorObj.messages[0].details 		= err.getCode() + ' : ' + err.getDetails(); //err;
					nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
					if(requestType == null)
					{
						sendCustomerErrorEmail(dataIn, ErrorObj);
						var errorLogs = LogEntityCreation(1, 4, customer, dataIn, 2, ErrorObj);			
					}
					return ErrorObj;
				}
			}
		}
		else
		{
			nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
			if(requestType == null)
			{
				sendCustomerErrorEmail(dataIn, ErrorObj);
				var errorLogs = LogEntityCreation(1, 4, customer, dataIn, 2, ErrorObj);	
			}
			return ErrorObj;
		}
	}
	catch(any_err)
	{
		ErrorObj.status 	 = 'Exception'; 
		ErrorObj.messages[0] = new Object();
		ErrorObj.messages[0].messagetype 	= "Customer Update Failure";
		ErrorObj.messages[0].message 		= "Customer could not be created or updated";
		ErrorObj.messages[0].message 		+= " Details as below."; 
		ErrorObj.messages[0].details 		= any_err;
		nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
		if(requestType == null)
		{
			sendCustomerErrorEmail(dataIn, ErrorObj);
			// var errorLogs = LogEntityCreation(1, 4, customer, dataIn, 2, ErrorObj);	
		}
		return ErrorObj;
	}
}

function PrevalidationCustomerCheck(dataIn)
{
	//Pre-Validation checks on DataIn
	
	//1. Check if dataIn is empty
	if(!emptyInbound(dataIn))												return false;

	//2. Check if dataIn has header level objects values set
	if(!checkEmptyObject(dataIn.customerEmail, 		"CustomerEmail"))		return false;
	if(!checkEmptyObject(dataIn.locale, 			"Locale"))				return false;
	
	return true;
}

function compareAddresses(dataIn)
{
	if(checkEmptyObject(dataIn.shippingAddress.recipientPhoneNumber,		"RecipientPhoneNumber") && checkEmptyObject(dataIn.customerPhoneNumber,					"CustomerPhoneNumber"))	
	{
		if(dataIn.shippingAddress.recipientPhoneNumber != dataIn.customerPhoneNumber)						return false;
	}
	if(checkEmptyObject(dataIn.shippingAddress.shippingAddressLine1,		"ShippingAddressLine1") && checkEmptyObject(dataIn.billingAddress.billingAddressLine1,	"BillingAddressLine1"))
	{
		if(dataIn.shippingAddress.shippingAddressLine1 != dataIn.billingAddress.billingAddressLine1)		return false;
	}
	if(checkEmptyObject(dataIn.shippingAddress.shippingAddressLine2,		"ShippingAddressLine2") && checkEmptyObject(dataIn.billingAddress.billingAddressLine2,	"BillingAddressLine2"))
	{
		if(dataIn.shippingAddress.shippingAddressLine2 != dataIn.billingAddress.billingAddressLine2)		return false;
	}
	if(checkEmptyObject(dataIn.shippingAddress.cityName,					"ShippingCity") 		&& checkEmptyObject(dataIn.billingAddress.cityName,				"BillingCity"))
	{
		if(dataIn.shippingAddress.cityName 				!= dataIn.billingAddress.cityName)					return false;
	}
	
	if(checkEmptyObject(dataIn.shippingAddress.provinceName,				"ShippingProvinceName")	&& checkEmptyObject(dataIn.billingAddress.provinceName,			"BillingProvinceName"))
	{
		if(dataIn.shippingAddress.provinceName != dataIn.billingAddress.provinceName)						return false;
	}
	if(checkEmptyObject(dataIn.shippingAddress.countryName,				"BillingCountry")			&& checkEmptyObject(dataIn.billingAddress.countryName,			"BillingCountry"))
	{
		if(dataIn.shippingAddress.countryName 			!= dataIn.billingAddress.countryName)				return false;
	}
	if(checkEmptyObject(dataIn.shippingAddress.postalCode,				"BillingPostalCode")		&& checkEmptyObject(dataIn.billingAddress.postalCode,			"BillingPostalCode"))
	{
		if(dataIn.shippingAddress.postalCode 			!= dataIn.billingAddress.postalCode)				return false;
	}
	// nlapiLogExecution('debug', 'Same Billing & Shipping Address');
	return true;
}

function findNewAddress(record, dataIn, type)
{
	var index 			= 0; //First line (new)
	var existingAddress = record.getLineItemCount('addressbook');
	// nlapiLogExecution('debug', 'existingAddress', existingAddress);
	var countDiff 		= 0;
	var i				= 1;
	for(; i<=existingAddress; i++) 
	{
		var change = false;
		// record.removeLineItem('addressbook', i);
		record.selectLineItem('addressbook', i);
		var subrecord = record.editCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
		
		if(checkDuplicate(dataIn, type, subrecord))
		{
			// nlapiLogExecution('debug', 'change: ' + i, countDiff); 
			countDiff++;
		}
		else
		{
			// nlapiLogExecution('debug', 'No change');
			index = i;
			break;
		}
	}
	return index;
}

function checkDuplicate(dataIn, type, subrecord)
{
	if(type == 'billingAddress')
	{
		if(checkEmptyObject(dataIn.customerPhoneNumber,						"CustomerPhoneNumber"))	
		{
			if(dataIn.customerPhoneNumber 					!= subrecord.getFieldValue('addrphone'))		return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.billingAddressLine1,		"BillingAddressLine1"))
		{
			if(dataIn.billingAddress.billingAddressLine1 	!= subrecord.getFieldValue('addr1'))			return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.billingAddressLine2,		"BillingAddressLine2"))
		{
			if(dataIn.billingAddress.billingAddressLine2 	!= subrecord.getFieldValue('addr2'))			return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.cityName,					"BillingCity"))
		{
			if(dataIn.billingAddress.cityName 				!= subrecord.getFieldValue('city'))				return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.provinceName,				"BillingProvinceName"))
		{
			if(dataIn.billingAddress.provinceName 			!= subrecord.getFieldValue('state'))			return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.countryName,				"BillingCountry"))
		{
			if(dataIn.billingAddress.countryName 			!= subrecord.getFieldValue('country'))			return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.postalCode,				"BillingPostalCode"))
		{
			if(dataIn.billingAddress.postalCode 			!= subrecord.getFieldValue('zip'))				return true;
		}
	}
	else if(type == 'shippingAddress')
	{
		if(checkEmptyObject(dataIn.shippingAddress.recipientPhoneNumber,		"RecipientPhoneNumber"))	
		{
			if(dataIn.shippingAddress.recipientPhoneNumber 	!= subrecord.getFieldValue('addrphone'))		return true;
		}
		if(checkEmptyObject(dataIn.shippingAddress.shippingAddressLine1,		"ShippingAddressLine1"))
		{
			if(dataIn.shippingAddress.shippingAddressLine1 	!= subrecord.getFieldValue('addr1'))			return true;
		}
		if(checkEmptyObject(dataIn.shippingAddress.shippingAddressLine2,		"ShippingAddressLine2"))
		{
			if(dataIn.shippingAddress.shippingAddressLine2 	!= subrecord.getFieldValue('addr2'))			return true;
		}
		if(checkEmptyObject(dataIn.shippingAddress.cityName,					"ShippingCity"))
		{
			if(dataIn.shippingAddress.cityName 				!= subrecord.getFieldValue('city'))				return true;
		}
		
		if(checkEmptyObject(dataIn.shippingAddress.provinceName,				"ShippingProvinceName"))
		{
			if(dataIn.shippingAddress.provinceName 			!= subrecord.getFieldValue('state'))			return true;
		}
		if(checkEmptyObject(dataIn.shippingAddress.countryName,				"BillingCountry"))
		{
			if(dataIn.shippingAddress.countryName 			!= subrecord.getFieldValue('country'))			return true;
		}
		if(checkEmptyObject(dataIn.shippingAddress.postalCode,				"BillingPostalCode"))
		{
			if(dataIn.shippingAddress.postalCode 			!= subrecord.getFieldValue('zip'))				return true;
		}
	}
	return false;
}

function sendCustomerErrorEmail(request, message)
{
	var subject = 'Customer Opt-In failure';
	var body 	= 'Hello,<br><br>';
	body 		+= 'Customer creation and Opt-In request failed in NetSuite due to below error.<br>';
	body 		+= JSON.stringify(message);
	body 		+= '<br><br><b>Data received in NetSuite: </b><br><br>' + JSON.stringify(request) + '<br><br><br>';
	body 		+= '<br><br>Process needs to be resubmitted or handled manually through user interface after corrections.';
	body 		+= '<br><br><br><br><br><br><br>Thanks';
	body 		+= '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
	nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
	if(sendEmail)	nlapiSendEmail(243423, toList, subject, body, ccList);
}

function LogEntityCreation(ediType, ediNumber, sourceEntity, dataIn, ediStatus, ediResponse)
{
	var parentId = 0;
	var cols 	 = new Array();
	cols[0]  	 = new nlobjSearchColumn('custrecord_wtka_entity');
	cols[1]  	 = new nlobjSearchColumn('custrecord_wtka_entity_editype');
	cols[2]  	 = new nlobjSearchColumn('custrecord_wtka_entity_edinumber');
	
	var filters  = new Array();
	filters[0]   = new nlobjSearchFilter('custrecord_wtka_entity', 				null, 'anyof', sourceEntity);
	filters[1]   = new nlobjSearchFilter('custrecord_wtka_entity_editype', 		null, 'anyof', ediType);
	filters[2]   = new nlobjSearchFilter('custrecord_wtka_entity_edinumber', 	null, 'anyof', ediNumber);

	var searchRecord = nlapiSearchRecord('customrecord_wtka_entitylogs', null, filters, cols);
	if(searchRecord != null)
	{
		for(var i=0;  i < searchRecord.length ; i++)
		{
			parentId = searchRecord[i].getId()
		}
	}
	else
	{
		var logRecord = nlapiCreateRecord('customrecord_wtka_entitylogs');
		logRecord.setFieldValue('custrecord_wtka_entity_editype', 	ediType);
		logRecord.setFieldValue('custrecord_wtka_entity_edinumber', ediNumber);
		if(sourceEntity > 0) logRecord.setFieldValue('custrecord_wtka_entity',  sourceEntity);
		parentId = nlapiSubmitRecord(logRecord);
	}

	var logRecordChild = nlapiCreateRecord('customrecord_wtka_entitylogs_child');

	logRecordChild.setFieldValue('custrecord_wtka_entity_parent', 	parentId);
	logRecordChild.setFieldValue('custrecord_wtka_entity_request', 	JSON.stringify(dataIn));
	logRecordChild.setFieldValue('custrecord_wtka_entity_response', JSON.stringify(ediResponse));
	logRecordChild.setFieldValue('custrecord_wtka_entity_status', 	ediStatus);
	
	var childId = nlapiSubmitRecord(logRecordChild);
	
	var logArray = new Array();
	logArray[0]  = parentId;
	logArray[1]  = childId;
	return logArray;
}

function checkEmptyObject(objectVar, objectName) //additional check for unknown
{
	//Check if dataIn has header level objects values set
	if(!emptyObject(objectVar, objectName))
	{
		return false;
	}
	else if(objectVar.match(/unknown/gi) != null)
	{
		ErrorObj.messages[0] 			 = new Object();
		ErrorObj.messages[0].messagetype = "Bad Request";
		ErrorObj.messages[0].message 	 = objectName + " data is unknown";
		return false;
	}
	else	return true;
}