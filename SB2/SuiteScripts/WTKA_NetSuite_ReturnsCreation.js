/**
 *	File Name		:	WTKA_NetSuite_ReturnsCreation.js
 *	Function		:	Returns (Creation, Receipt, Refund)
 * 	Authors			:	Rini Thomas, Vinod Kumar
 *	Company			:	Wipro Limited
 *	Release Dates	:	5-May-2016 (v1.0)
 * 	Current Version	:	1.0
**/

{
	var ErrorObj 		 			 = new Object();
	ErrorObj.status		 			 = "Error";
	ErrorObj.messages 	 			 = new Array();
	ErrorObj.messages[0] 			 = new Object();
	ErrorObj.messages[0].messagetype = "";
	ErrorObj.messages[0].message	 = "";
	
	var finalObject 	 			 = new Object();
	finalObject.records 			 = new Array();

	var invalid_orderid 			 = 'F', 				recordId  		= 0;
	var OriginalOrderId			     = 0, 					ediStatus		= 2;
	var FinalReturnOrderArray 		 = new Array(), 		returnDetails 	= new Array();
	var dualTax 					 = false,   			rollbackFlag 	= false
	var itemreceiptflag 			 = false, 				sendEmail 		= true; //false;
	var subject, 											body;
	var ccList = null; //['']; //Enter CC Addresses as array
	var toList = ['3PL_eComm_Integration@kitandace.com'];
}

function postReturnOrderCreation(dataIn)
{
	nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
	
	try
	{
		if(PrevalidationCheck(dataIn))
		{
			nlapiLogExecution('DEBUG', 'FinalReturnOrderArray', JSON.stringify(FinalReturnOrderArray));
			var zeroShipped = 0;
			for(var i=0; !rollbackFlag && FinalReturnOrderArray != null && i < FinalReturnOrderArray.length; i++)
			{
				itemreceiptflag = false;
				//1. Transform salesorder to returnAuthorization
				var recordObj 			= new Object();
				recordObj.shipmentId 	= FinalReturnOrderArray[i].extNumber;
				recordObj.transactions 	= new Array();
				returnDetails[i]		= new Object();
				var receiptFlag 		= 0;
				nlapiLogExecution('debug', 'Processing Shipment', FinalReturnOrderArray[i].extNumber);
				var returnFlag = createReturns(dataIn, FinalReturnOrderArray[i].origOrderId, recordObj, i);
				
				if(!rollbackFlag && returnFlag != "ZERO_RETURNS")
				{
					returnDetails[i].returnId 		= returnFlag;
					returnDetails[i].itemReceiptId	= 0;
					returnDetails[i].cashRefundId	= 0;
					returnDetails[i].logId			= 0;
				}
				if(returnFlag == "ZERO_RETURNS")
				{
					zeroShipped++;
					if(FinalReturnOrderArray.length == zeroShipped)
					{
						finalObject.records.push(recordObj);
						var message = JSON.stringify(finalObject) + '<br>';
						sendReturnMail(message, dataIn);
						break;
					}
				}
				
				//2. Transform returnAuthorization to itemReceipt
				if(!rollbackFlag && returnFlag > 0)
				{
					receiptFlag	= createItemReceipt(dataIn, returnFlag, FinalReturnOrderArray[i].returnDate, recordObj, i);
					returnDetails[i].itemReceiptId	= receiptFlag;		
				}
				
				//3. Create CashRefund or CreditMemo
				if(!rollbackFlag && receiptFlag > 0)
				{
					returnDetails[i].cashRefundId = createCashRefund(dataIn, returnFlag, FinalReturnOrderArray[i].returnDate, i, recordObj);
				}
				
				finalObject.records.push(recordObj);
				returnDetails[i].logId  = LogCreation(1, 6, recordId, recordId, dataIn, ediStatus, finalObject, 'F'); //6 - Returns
			}
			nlapiLogExecution('DEBUG', 'finalObject', JSON.stringify(finalObject));
		}
		else
		{
			ediStatus 	  = (ErrorObj.status == "Error") ? 2 : 3; // 2- Error, 3-Exception
			finalObject   = ErrorObj;
			var errorLogs = LogCreation(1, 6, recordId, recordId, dataIn, ediStatus, finalObject, 'F'); 
			nlapiLogExecution('debug', 'Error', JSON.stringify(finalObject));
			var message = JSON.stringify(finalObject) + '<br>';
			sendReturnMail(message, dataIn);
		}
	}
	catch(other_errors)
	{
		rollbackFlag 		= true;
		finalObject.status  = "Exception";
		finalObject.message = "Unable to Process Orders. Error Details: ";
		finalObject.message = other_errors;
		var message = JSON.stringify(finalObject) + '<br>';
		sendReturnMail(message, dataIn);
	}

	/*try
	{
		if(rollbackFlag && returnDetails != null && returnDetails.length > 0)
		{
			var message = '';
			nlapiLogExecution('debug', 'Rolling back...');
			if(!catchError)	message = JSON.stringify(finalmessage) + '<br>';
			var rollbackstatus = rollbackProcess(returnDetails, 'returns'); // Rollback all processes
			message += (rollbackstatus) ? '<br>The process has been successfully rolled back and all the transactions listed above have been deleted.' : '<br>The process failed to rollback.'
			sendReturnMail(message, dataIn);
		}
	}
	catch(rollback_err)
	{
		nlapiLogExecution('debug', 'Rollback failed');
		var message = '<br><b>Error Details: </b><br>' + JSON.stringify(rollback_err);
		sendReturnMail(message, dataIn);
	}*/
	return finalObject;
}

function PrevalidationCheck(dataIn)
{
	try
	{
		//1. Check if dataIn is empty
		if(!emptyInbound(dataIn)) return false; 
	
		//1.1 Validate dates 
		if(!validateISODate(dataIn.returnHeader.returnTransactionDt, 'date'))	return false;
		
		//2. Check if dataIn has header level objects values set
		if(!emptyObject(dataIn.returnHeader,  					 "returnHeader")) 	 	 return false; 
		if(!emptyObject(dataIn.returnHeader.returnLocation, 	 "returnLocation")) 	 return false; 
		if(!emptyObject(dataIn.returnHeader.customer, 	 		 "customer")) 	 		 return false; 
		if(!emptyObject(dataIn.returnHeader.paymentInfo, 		 "paymentInfo")) 		 return false; 
		if(!emptyObject(dataIn.returnHeader.originalOrderId, 	 "originalOrderId")) 	 return false; 
		if(!emptyObject(dataIn.returnHeader.returnTransactionDt, "returnTransactionDt")) return false; 
		if(!emptyObject(dataIn.returnHeader.totalAmountReturned, "TotalAmountReturned")) return false; 

		var totalReturned = (emptyObject(dataIn.returnHeader.totalQuantityReturned, "TotalAmountReturned")) ? dataIn.returnHeader.totalQuantityReturned : 0;
		
		//3. Check if dataIn has Lines leve objects values set
		if(!emptyObject(dataIn.returnDetails, 		"returnDetails"))  		return false; 
		if(!containLines(dataIn.returnDetails, 		"Items present"))		return false;
		
		//4. Check if dataIn has line items
		
		var headerRMAID = dataIn.returnHeader.RMAID;
		for(var i in dataIn.returnDetails)
		{
			if(!containLines(dataIn.returnDetails[i].item, 								"Items present"))		 return false;
			if(!emptyObject(dataIn.returnDetails[i].item.SKUId,							"SKUId"))  			 	 return false;
			if(!emptyObject(dataIn.returnDetails[i].originalShipmentId,					"OriginalShipmentId"))   return false;
			if(!containLines(dataIn.returnDetails[i].amountItemPrice, 					"AmtItemPrice present")) return false;
			if(!emptyObject(dataIn.returnDetails[i].amountItemPrice.amtCurrentPrice,	"AmtItemPrice"))  		 return false;
			// if(!emptyObject(dataIn.returnDetails[i].tax,								"Tax"))					 return false;
			// if(!containLines(dataIn.returnDetails[i].tax, 								"Tax data present"))	 return false;
			
			//4.1 Check if returnHeader.RMAID matches returnDetails[i].RMAID
			var detailsRMAID = dataIn.returnDetails[i].RMAID;
			if(headerRMAID != detailsRMAID) 
			{
				ErrorObj.messages[0].messagetype = "Bad Request";
				ErrorObj.messages[0].message 	 = "RMAID in returnDetails object does not match RMAID in returnHeader object.";
				return false;
			}
		
			//5. Check is duplicates based on RMAID
			/*var InboundReturnNumber  = dataIn.returnHeader.RMAID + '_' + dataIn.returnDetails[i].originalShipmentId;
			var type 				 = 'returnauthorization';
			var InboundReturnId		 = FetchOrderId(type, InboundReturnNumber, 'ext', 'tranid', 'return');
			if(InboundReturnId != 0)
			{
				ErrorObj.messages[0].messagetype = "Duplicate Return Order";
				ErrorObj.messages[0].message 	 = "Return Authorization cannot be created as the Return Order " + InboundReturnNumber + " already exists.";
				return false;
			}*/
		}
		
		//6. Check if OriginalShipmentId in returnDetails is a valid order in NetSuite
		var zeroReturned = 0;
		var noOrder		 = 0;
		for(var i in dataIn.returnDetails)
		{
			//6.1 Check if originalShipmentId is Valid
			var record;
			var returnDate 	= '';
			var type 		= 'salesorder';
			var ext_order 	= dataIn.returnHeader.originalOrderId + '_' + dataIn.returnDetails[i].originalShipmentId;
			OriginalOrderId = FetchOrderId(type, ext_order, 'ext', null, 'nonIcto');
			if(OriginalOrderId <= 0)
			{
				noOrder++;
				nlapiLogExecution('debug', 'Order not found', ext_order);
				continue;
			}
			
			try
			{
				record = nlapiLoadRecord('salesorder', OriginalOrderId);
			}
			catch(err_record)
			{
				return (invalidOrder());
			}
			
			var recordStatus = record.getFieldValue('statusRef');
			if(recordStatus == 'pendingFulfillment' || recordStatus == 'pendingBilling' || recordStatus == 'cancelled')
			{
				ErrorObj.messages[0].messagetype = "Invalid Original Order Status";
				ErrorObj.messages[0].message 	 = 'Return Order cannot be created as the original order is in ' + recordStatus + ' status';
				return false;
			}
			
			//6.2 Check if returnTransactionDt >= Original Order date
			var orderDate = record.getFieldValue('trandate');
			orderDate	  = nlapiStringToDate(orderDate, 'date');
			var dateValue = dataIn.returnHeader.returnTransactionDt; 
			returnDate	= moment(dateValue).format('l LT');
			returnDate 	= new Date(returnDate);
			
			if(returnDate < orderDate)
			{
				ErrorObj.messages[0].messagetype = "Invalid Return Order Date";
				ErrorObj.messages[0].message 	 = 'Return Order cannot be lesser than original order date';
				return false;
				
			}
			
			returnDate 	= nlapiDateToString(returnDate, 'date');		
			
			//6.3 Check if the UPC number is part of the original order
			for(var j=0; j< record.getLineItemCount('item'); j++)
			{
				var UPCFlag = false;
				var LineUPCNumber = record.getLineItemValue('item', 'custcol_wtka_upccode', j+1);
				if(LineUPCNumber == dataIn.returnDetails[i].item.SKUId)
				{
					//6.3.1 Check if return quantity <= original order quantity
					var ItemReturned = dataIn.returnDetails[i].countItemReturned;
					var ItemOrdered = record.getLineItemValue('item', 'quantity', j+1);
					if(ItemReturned > ItemOrdered)
					{
						ErrorObj.messages[0].messagetype = "Invalid Return Order Quantity";
						ErrorObj.messages[0].message = 'Return Order quantity cannot be greater than Original order quantity';
						return false;
					}
					UPCFlag = true;
				}
				
				if(UPCFlag) 
				{
					//Create the final return object that will be used for final processing
					var flag = false;
					for(var k=0; FinalReturnOrderArray != null && k < FinalReturnOrderArray.length; k++)
					{
						if(FinalReturnOrderArray[k].origOrderId == OriginalOrderId)
						{
							var lineArray 		  	= new Object();
							lineArray 				= FinalReturnOrderArray[k];
							
							var itemDetails 	   = new Object;
							itemDetails.SKUId 	   = dataIn.returnDetails[i].item.SKUId;
							itemDetails.returnQty  = dataIn.returnDetails[i].countItemReturned;
							itemDetails.itemPrice  = dataIn.returnDetails[i].amountItemPrice.amtCurrentPrice;
							
							if(itemDetails.returnQty == 0) zeroReturned++;
							
							itemDetails.reasonCode = dataIn.returnDetails[i].returnReasonCode;
							
							itemDetails.itemTax 				  	= new Object();
							itemDetails.itemTax.taxRate1 			= 0;
							itemDetails.itemTax.taxRate2 			= 0;
							itemDetails.itemTax.taxAmountApplied1	= 0.00;
							itemDetails.itemTax.taxAmountApplied2 	= 0.00;
							
							try
							{
								var taxData 	= FetchTaxData(dataIn.returnDetails[i], 'return');
								dualTax 		= taxData.dualTax;
								var taxName 	= dataIn.returnDetails[i].tax[0].taxName;
								var subsidiary 	= nlapiLookupField('salesorder', OriginalOrderId, 'subsidiary');
								if(subsidiary == 3 && taxName.match(/pst/gi) != null) //switch tax1 and tax2
								{
									itemDetails.itemTax.taxRate1 			= taxData.taxRate2;
									itemDetails.itemTax.taxRate2 			= taxData.taxRate1;
									itemDetails.itemTax.taxAmountApplied1	= quantityParse(taxData.tax2Amount, 		'float'); //Set currently
									itemDetails.itemTax.taxAmountApplied2 	= quantityParse(taxData.tax1Amount, 		'float'); //Set currently
									/*if(totalReturned > 0) //Avoid divide by 0
									{
										var taxValue		 					= quantityParse(taxData.tax2Amount, 		'float');
										var partTaxValue	 					= (itemDetails.returnQty/totalReturned)*taxValue;	  		//apportion tax
										itemDetails.itemTax.taxAmountApplied1	= quantityParse(partTaxValue.toFixed(2), 	'float');
										
										var taxValue 		 					= quantityParse(taxData.tax1Amount, 		'float');
										var partTaxValue	 					= (itemDetails.returnQty/totalReturned)*taxValue;			//apportion tax
										itemDetails.itemTax.taxAmountApplied2 	= quantityParse(partTaxValue.toFixed(2), 	'float');
									}*/
								}
								else 
								{
									itemDetails.itemTax.taxRate1 			= taxData.taxRate1;
									itemDetails.itemTax.taxRate2 			= taxData.taxRate2;
									itemDetails.itemTax.taxAmountApplied1	= quantityParse(taxData.tax1Amount, 		'float'); //Set currently
									itemDetails.itemTax.taxAmountApplied2 	= quantityParse(taxData.tax2Amount, 		'float'); //Set currently
									/*if(totalReturned > 0) //Avoid divide by 0
									{
										var taxValue		 					= quantityParse(taxData.tax1Amount, 		'float');
										var partTaxValue	 					= (itemDetails.returnQty/totalReturned)*taxValue;	  		//apportion tax
										itemDetails.itemTax.taxAmountApplied1	= quantityParse(partTaxValue.toFixed(2), 	'float');
										
										var taxValue 		 					= quantityParse(taxData.tax2Amount, 		'float');
										var partTaxValue	 					= (itemDetails.returnQty/totalReturned)*taxValue;			//apportion tax
										itemDetails.itemTax.taxAmountApplied2 	= quantityParse(partTaxValue.toFixed(2), 	'float');
									}*/
								}
							}
							catch(ex)
							{
								// nlapiLogExecution('DEBUG', 'Tax Calc Error', ex);
							}
							
							lineArray.items.push(itemDetails);
							flag = true;
							break;
						}
					}
					
					if(!flag) //Doesnt exist, create new entry
					{
						// nlapiLogExecution('DEBUG', 'Return record not found', flag);
						var lineArray 			= new Object();
						lineArray.origOrderId 	= OriginalOrderId;
						lineArray.extNumber 	= dataIn.returnDetails[i].RMAID + '_' + dataIn.returnDetails[i].originalShipmentId;
						lineArray.ordNumber 	= String(dataIn.returnHeader.originalOrderId);
						lineArray.returnDate 	= returnDate;
						lineArray.reasonCode 	= dataIn.returnDetails[i].returnReasonCode;
						lineArray.memo 			= dataIn.returnDetails[i].returnDetailMemo;
						lineArray.email 		= dataIn.returnHeader.customer.customerEmail;
						lineArray.items 		= new Array();
						
						var itemDetails 	   = new Object;
						itemDetails.SKUId 	   = dataIn.returnDetails[i].item.SKUId;
						itemDetails.returnQty  = dataIn.returnDetails[i].countItemReturned;
						itemDetails.itemPrice  = dataIn.returnDetails[i].amountItemPrice.amtCurrentPrice;
						
						if(itemDetails.returnQty == 0) zeroReturned++;
						
						itemDetails.reasonCode = dataIn.returnDetails[i].returnReasonCode;
						
						itemDetails.itemTax 				  	= new Object();
						itemDetails.itemTax.taxRate1 			= 0;
						itemDetails.itemTax.taxRate2 			= 0;
						itemDetails.itemTax.taxAmountApplied1	= 0.00;
						itemDetails.itemTax.taxAmountApplied2 	= 0.00;
						
						try
						{
							var taxData 	= FetchTaxData(dataIn.returnDetails[i], 'return');
							dualTax 		= taxData.dualTax;
							var taxName 	= dataIn.returnDetails[i].tax[0].taxName;
							var subsidiary 	= nlapiLookupField('salesorder', OriginalOrderId, 'subsidiary');
							if(subsidiary == 3 && taxName.match(/pst/gi) != null) //switch tax1 and tax2
							{
								itemDetails.itemTax.taxRate1 			= taxData.taxRate2;
								itemDetails.itemTax.taxRate2 			= taxData.taxRate1;
								itemDetails.itemTax.taxAmountApplied1	= quantityParse(taxData.tax2Amount, 		'float'); //Set currently
								itemDetails.itemTax.taxAmountApplied2 	= quantityParse(taxData.tax1Amount, 		'float'); //Set currently
								/*if(totalReturned > 0) //Avoid divide by 0
								{
									var taxValue		 					= quantityParse(taxData.tax2Amount, 		'float');
									var partTaxValue	 					= (itemDetails.returnQty/totalReturned)*taxValue;	  		//apportion tax
									itemDetails.itemTax.taxAmountApplied1	= quantityParse(partTaxValue.toFixed(2), 	'float');
									
									var taxValue 		 					= quantityParse(taxData.tax1Amount, 		'float');
									var partTaxValue	 					= (itemDetails.returnQty/totalReturned)*taxValue;			//apportion tax
									itemDetails.itemTax.taxAmountApplied2 	= quantityParse(partTaxValue.toFixed(2), 	'float');
								}*/
							}
							else 
							{
								itemDetails.itemTax.taxRate1 			= taxData.taxRate1;
								itemDetails.itemTax.taxRate2 			= taxData.taxRate2;
								itemDetails.itemTax.taxAmountApplied1	= quantityParse(taxData.tax1Amount, 		'float'); //Set currently
								itemDetails.itemTax.taxAmountApplied2 	= quantityParse(taxData.tax2Amount, 		'float'); //Set currently
								/*if(totalReturned > 0) //Avoid divide by 0
								{
									var taxValue		 					= quantityParse(taxData.tax1Amount, 		'float');
									var partTaxValue	 					= (itemDetails.returnQty/totalReturned)*taxValue;	  		//apportion tax
									itemDetails.itemTax.taxAmountApplied1	= quantityParse(partTaxValue.toFixed(2), 	'float');
									
									var taxValue 		 					= quantityParse(taxData.tax2Amount, 		'float');
									var partTaxValue	 					= (itemDetails.returnQty/totalReturned)*taxValue;			//apportion tax
									itemDetails.itemTax.taxAmountApplied2 	= quantityParse(partTaxValue.toFixed(2), 	'float');
								}*/
							}
						}
						catch(ex)
						{
							// nlapiLogExecution('DEBUG', 'Tax Calc Error', ex);
						}
						lineArray.items.push(itemDetails);
						
						FinalReturnOrderArray.push(lineArray);
					}
					break;
				}
			}
			
			if(!UPCFlag)
			{
				ErrorObj.messages[0].messagetype = "Invalid SKUCode";
				ErrorObj.messages[0].message 	 = 'Return Order contains one or more invalid SKUCode which is not part of Original order';
				return false;
			}
		}
		if(zeroReturned == dataIn.returnDetails.length)
		{
			ErrorObj.messages[0].messagetype 	= "Empty Returns";
			ErrorObj.messages[0].message 	 	= "Request cannot be processed as the all shipments have zero items returned.";
			return false;
		}
		
		if(noOrder == dataIn.returnDetails.length)
		{
			ErrorObj.messages[0].messagetype = "Invalid Order";
			ErrorObj.messages[0].message 	 = "Return Authorization cannot be created as the order " + dataIn.returnHeader.originalOrderId + " does not exist.";
			return false;
		}
		return true;
	}
	catch(validate_err)
	{
		ErrorObj.status 				 = "Exception";
		ErrorObj.messages[0].messagetype = "Invalid Order";
		ErrorObj.messages[0].message 	 = "Return Authorization cannot be created due to the following error." + validate_err;
		return false;
	}
}

function returnReasonCode(reasonCode)
{
	var returnReason; 
	switch(reasonCode) 
	{
		case "Color":
			returnReason = 1; 
			break;
		case "Damaged":
			returnReason = 2; 
			break;
		case "Factory Defect":
			returnReason = 3;
			break;
		case "Fit Issues":
			returnReason = 4;
			break;
		case "Guest Cancellation":
			returnReason = 5;
			break;
		case "Guest Missed Order":
			returnReason = 6; 
			break;
		case "Missing from Box":
			returnReason = 7; 
			break;
		case "Miss-ship":
			returnReason = 8; 
			break;
		case "Samples":
			returnReason = 9; 
			break;
		case "Style Issues":
			returnReason = 10; 
			break;
		case "Wear/Wash Issues":
			returnReason = 11; 
			break;
		case "Wrong Size":
			returnReason = 12;
			break;
		case "Gift Exchanged":
			returnReason = 13; 
			break;
		default:
			returnReason = -1;
			break;
	}

	return returnReason;
}

function createReturns(dataIn, origOrderId, recordObj, index)
{
	recordId = 0;
	try
	{
		nlapiLogExecution('DEBUG', 'createReturns', index);
		var taxamt1 = 0.0, taxamt2 = 0.0;
		var recordType 	  = 'salesorder';
		var transformType = 'returnauthorization';
		var record 		  = nlapiTransformRecord(recordType, origOrderId, transformType);
		var returnReason  = returnReasonCode(FinalReturnOrderArray[index].reasonCode);
		if(returnReason != -1)	record.setFieldValue('custbody_reason_for_return', returnReason);
		else
		{
			var message  = '<br><b>Error Details: </b><br> Return Reason: ' + FinalReturnOrderArray[index].reasonCode + ' not found.';
			sendReturnMail(message, dataIn);
			rollbackFlag = true;
			var processObject 				= new Object();
			processObject.status 			= "Success";
			processObject.transactionType 	= "Return Authorization";
			processObject.message 			= "Return Reason " + FinalReturnOrderArray[index].reasonCode + ' not found.';
			nlapiLogExecution('DEBUG', 'Return Authorization', JSON.stringify(processObject));
			recordObj.transactions.push(processObject);
			return recordId;
		}
		record.setFieldValue('ccapproved', 'T');
		
		/* Set Returns Location */
		var subsid = record.getFieldText('subsidiary');
		var retLocation = 0, locationCode = '';
		if(subsid.match(/Kit and Ace Operating Canada/gi) != null)	locationCode = '1903';
		if(subsid.match(/Kit and Ace Operating US/gi) != null)		locationCode = '2903';
		
		if(locationCode != '')
		{
			retLocation = FetchInternalId('location', locationCode);
		}
		else
		{
			var message  = '<br><b>Error Details: </b><br> Location for subsidiary: ' + subsid + ' not found.';
			sendReturnMail(message, dataIn);
			rollbackFlag = true;
			var processObject 				= new Object();
			processObject.status 			= "Success";
			processObject.transactionType 	= "Return Authorization";
			processObject.message 			= 'Location for subsidiary: ' + subsid + ' not found.';
			nlapiLogExecution('DEBUG', 'Return Authorization', JSON.stringify(processObject));
			recordObj.transactions.push(processObject);
			return recordId;
		}
		
		if(retLocation > 0)	record.setFieldValue('location', retLocation);
		else
		{
			var message  = '<br><b>Error Details: </b><br> Location ' + locationCode + ' not found.';
			sendReturnMail(message, dataIn);
			rollbackFlag = true;
			var processObject 				= new Object();
			processObject.status 			= "Success";
			processObject.transactionType 	= "Return Authorization";
			processObject.message 			= 'Location ' + locationCode + ' not found.';
			nlapiLogExecution('DEBUG', 'Return Authorization', JSON.stringify(processObject));
			recordObj.transactions.push(processObject);
			return recordId;
		}
		record.setFieldValue('custbody_wtka_extsys_order_number', 	FinalReturnOrderArray[index].extNumber);
		record.setFieldValue('custbody_wtka_extsys_hybris_order', 	FinalReturnOrderArray[index].ordNumber);
//		record.setFieldValue('trandate', 							FinalReturnOrderArray[index].returnDate);                                               //IVAN DISABLE 20160516 3:40
		record.setFieldValue('memo', 								FinalReturnOrderArray[index].memo);		
		record.setFieldValue('email', 								FinalReturnOrderArray[index].email);		
		
		/* Set item quantities */
		var setLines = new Array();
		
		var zeroReturned = 0;
		for(var j=0; j < record.getLineItemCount('item'); j++)
		{
			record.selectLineItem('item', j+1);
			var itemCode = record.getLineItemValue('item', 'custcol_wtka_upccode', j+1);
			record.setCurrentLineItemValue('item', 'quantity', null);
			for(var k=0; k < FinalReturnOrderArray[index].items.length; k++)
			{
				var returnSKUId = FinalReturnOrderArray[index].items[k].SKUId;
				if(returnSKUId == itemCode)
				{
					var returnReason = returnReasonCode(FinalReturnOrderArray[index].items[k].reasonCode);
					if(returnReason != -1)	record.setCurrentLineItemValue('item', 'custcol_erp_temp_reasoncode', returnReason);
					else
					{
						var message  = '<br><b>Error Details: </b><br> Return Reason: ' + FinalReturnOrderArray[index].reasonCode + ' not found.';
						sendReturnMail(message, dataIn);
						rollbackFlag = true;
						var processObject 				= new Object();
						processObject.status 			= "Success";
						processObject.transactionType 	= "Return Authorization";
						processObject.message 			= 'Return Reason: ' + FinalReturnOrderArray[index].reasonCode + ' not found.';
						nlapiLogExecution('DEBUG', 'Return Authorization', JSON.stringify(processObject));
						recordObj.transactions.push(processObject);
						return recordId;
					}
					if(FinalReturnOrderArray[index].items[k].returnQty == 0)	zeroReturned++;
					record.setCurrentLineItemValue('item', 'isdropshipment', 	'F');
					record.setCurrentLineItemValue('item', 'quantity',  		FinalReturnOrderArray[index].items[k].returnQty);
					record.setCurrentLineItemValue('item', 'price', 			-1);
					record.setCurrentLineItemValue('item', 'rate', 				FinalReturnOrderArray[index].items[k].itemPrice);
					taxamt1 += FinalReturnOrderArray[index].items[k].itemTax.taxAmountApplied1;
					taxamt2 += FinalReturnOrderArray[index].items[k].itemTax.taxAmountApplied2;
					if(subsid.match(/Kit and Ace Operating US/gi) != null)
					{
						record.setCurrentLineItemValue('item', 'taxrate1', 		FinalReturnOrderArray[index].items[k].itemTax.taxRate1);
						record.setCurrentLineItemValue('item', 'taxrate2', 		FinalReturnOrderArray[index].items[k].itemTax.taxRate2);
					}
					record.setCurrentLineItemValue('item', 'custcol_wtka_taxrate1', 	FinalReturnOrderArray[index].items[k].itemTax.taxRate1);
					record.setCurrentLineItemValue('item', 'custcol_wtka_taxrate2', 	FinalReturnOrderArray[index].items[k].itemTax.taxRate2);
					setLines[j] = true;
					break;						
				}
			}
			record.commitLineItem('item');
		}

		if(zeroReturned == record.getLineItemCount('item'))
		{
			var processObject 				= new Object();
			processObject.status 			= "Success";
			processObject.transactionType 	= "Return Authorization";
			processObject.message 			= "Shipment has zero items returned";
			nlapiLogExecution('DEBUG', 'Return Authorization', JSON.stringify(processObject));
			recordObj.transactions.push(processObject);
			return "ZERO_RETURNS";
		}
		
		/* Remove other lines */
		for(var j=0; j<record.getLineItemCount('item'); j++)
		{
			if(!setLines[j])
			{
				record.selectLineItem('item', j+1);
				record.setCurrentLineItemValue('item', 'quantity', 0);
			}		
			record.commitLineItem('item');
		}
		
		/* Set total tax amount */
		record.setFieldValue('taxamountoverride',  taxamt1);
		record.setFieldValue('taxamount2override', taxamt2); //PST for Canadian Orders
		
		record.setFieldValue('custbody_wtka_taxamountoverride',  taxamt1);
		record.setFieldValue('custbody_wtka_taxamount2override', taxamt2); //PST for Canadian Orders
		
		recordId   = nlapiSubmitRecord(record, true); 
		
		var processObject 				= new Object();
		processObject.status 			= "Success";
		processObject.transactionType 	= "Return Authorization";
		processObject.transactionId 	= recordId;
		processObject.transactionNumber = nlapiLookupField(transformType, recordId, 'tranid');
		nlapiLogExecution('DEBUG', 'processObject-1', JSON.stringify(processObject));
		recordObj.transactions.push(processObject);
		ediStatus 			 			= 1;
	}
	catch(return_error)
	{
		ediStatus	 = 3; // Exception
		rollbackFlag = true;
		
		var  returnObject 		= new Object();
		returnObject.status  	= "Exception"; 
		returnObject.messagetype = "Return Authorization Creation Failure";
		returnObject.message 	= 'Return Authorization record could not be created. Details as below.'; 
		returnObject.details 	= return_error.getCode() + ' : ' + return_error.getDetails() + '<br>';
		recordObj.transactions.push(returnObject);
		
		var message = '<br><b>Error Details: </b><br>' + JSON.stringify(returnObject);
		sendReturnMail(message, dataIn);
	}
	return recordId;
}

function createItemReceipt(dataIn, rmaId, receiptDate, recordObj, index)
{
	var receiptId 	= 0;
	try
	{
		nlapiLogExecution('DEBUG', 'createItemReceipt', index);
		//Item Receipt 
		var recordType 		 = 'returnauthorization';
		var transformType 	 = 'itemreceipt';
		var receiptRecord 	 = nlapiTransformRecord(recordType, rmaId, transformType);
		// receiptRecord.setFieldValue('trandate', receiptDate); 
		
		for(var j=0; j < receiptRecord.getLineItemCount('item'); j++)
		{
			receiptRecord.selectLineItem('item', j+1);
			var itemCode = receiptRecord.getLineItemValue('item', 'custcol_wtka_upccode', j+1);
			
			for(var k=0; k < FinalReturnOrderArray[index].items.length; k++)
			{
				var returnSKUId = FinalReturnOrderArray[index].items[k].SKUId;
				if(returnSKUId == itemCode)
				{
					receiptRecord.setCurrentLineItemValue('item', 'quantity',  	FinalReturnOrderArray[index].items[k].returnQty);
					receiptRecord.setCurrentLineItemValue('item', 'receive', 	'T');
					break;						
				}
			}
			receiptRecord.commitLineItem('item');
		}
		
		receiptId  = nlapiSubmitRecord(receiptRecord, true); 
		
		var  processObject 				= new Object();
		processObject.status 			= "Success";
		processObject.transactionType 	= "Item Receipt";
		processObject.transactionId 	= receiptId;
		processObject.transactionNumber = nlapiLookupField(transformType, receiptId, 'tranid');
		nlapiLogExecution('DEBUG', 'Item Receipt', JSON.stringify(processObject));
		recordObj.transactions.push(processObject);
		itemreceiptflag 	 			= true;
		ediStatus 			 			= 1;
	}
	catch(err_fulfill)
	{
		nlapiLogExecution('DEBUG','Error Message', err_fulfill.message);
		ediStatus	 = 3; // Exception
		rollbackFlag = true;
		
		var itemReceiptObject 			= new Object();
		itemReceiptObject.status  		= "Error"; 
		itemReceiptObject.messagetype 	= "Item Receipt Failure";
		itemReceiptObject.message 		= 'Item Receipt record could not be created. Details as below.'; 
		itemReceiptObject.details 		= err_fulfill.getCode() + ' : ' + err_fulfill.getDetails();
		recordObj.transactions.push(itemReceiptObject);
		
		var message = '<br><b>Error Details: </b><br>' + JSON.stringify(itemReceiptObject);
		sendReturnMail(message, dataIn);
	}
	return receiptId;
}

function createCashRefund(dataIn, rmaId, returnDate, index, recordObj)
{
	var refundRecordId = 0;
	var taxamt1 = 0.0, taxamt2 = 0.0;
	try
	{
		nlapiLogExecution('DEBUG', 'createCashRefund', index);
		recordType 			= 'returnauthorization';
		transformType 		= 'cashrefund';
		var refundRecord 	= nlapiTransformRecord(recordType, rmaId, transformType);
		var subsid = refundRecord.getFieldText('subsidiary');
		// refundRecord.setFieldValue('trandate', 	returnDate);
		refundRecord.setFieldValue('account', 	115);
		
		refundRecord.setFieldValue('ccapproved', 'T');
		refundRecord.setFieldValue('chargeit', 	 'F');
		
		for(var j=0; j < refundRecord.getLineItemCount('item'); j++)
		{
			refundRecord.selectLineItem('item', j+1);
			var itemCode = refundRecord.getLineItemValue('item', 'custcol_wtka_upccode', j+1);
			for(var k=0; k < FinalReturnOrderArray[index].items.length; k++)
			{
				var returnSKUId = FinalReturnOrderArray[index].items[k].SKUId;
				if(returnSKUId == itemCode)
				{
					taxamt1 += FinalReturnOrderArray[index].items[k].itemTax.taxAmountApplied1;
					taxamt2 += FinalReturnOrderArray[index].items[k].itemTax.taxAmountApplied2;
					if(subsid.match(/Kit and Ace Operating US/gi) != null)
					{
						refundRecord.setCurrentLineItemValue('item', 'taxrate1', 	FinalReturnOrderArray[index].items[k].itemTax.taxRate1);
						refundRecord.setCurrentLineItemValue('item', 'taxrate2', 	FinalReturnOrderArray[index].items[k].itemTax.taxRate2);
						// if(dualTax)	refundRecord.setCurrentLineItemValue('item', 'taxrate2', 	FinalReturnOrderArray[index].items[k].itemTax.taxRate2);
					}
					break;						
				}
			}
			refundRecord.commitLineItem('item');
		}
		// Set total tax amount 
		refundRecord.setFieldValue('taxamountoverride',  taxamt1);
		refundRecord.setFieldValue('taxamount2override', taxamt2); //PST for Canadian Orders
		
		refundRecord.setFieldValue('custbody_wtka_taxamountoverride',  taxamt1);
		refundRecord.setFieldValue('custbody_wtka_taxamount2override', taxamt2); //PST for Canadian Orders
		
		refundRecordId   = nlapiSubmitRecord(refundRecord, true); 
		
		var  processObject 				= new Object();
		processObject.status 			= "Success";
		processObject.transactionType 	= "Cash Refund";
		processObject.transactionId 	= refundRecordId;
		processObject.transactionNumber = nlapiLookupField(transformType, refundRecordId, 'tranid');
		nlapiLogExecution('DEBUG', 'Cash Refund', JSON.stringify(processObject));
		recordObj.transactions.push(processObject);
		ediStatus = 1;
	}
	catch(err)
	{
		ediStatus		= 3; // Exception
		rollbackFlag 	= true;
		catchError 		= true;

		var cashRefund 			= new Object();
		cashRefund.status 		= 'Exception'; 
		cashRefund.messagetype 	= "Cash Refund Failure";
		cashRefund.message 		= 'Cash Refund could not be created. Details as below.'; 
		cashRefund.details 		= err.getCode() + ' : ' + err.getDetails();
		recordObj.transactions.push(cashRefund);
		
		var message = '<br><b>Error Details: </b><br> ' + JSON.stringify(cashRefund);
		sendReturnMail(message, dataIn);
	}
	return refundRecordId;
}

function rollbackReturns(returnDetails)
{
	var recordCount = 0;
	for(var i=0; returnDetails != null &&  i < returnDetails.length; i++)
	{
		try
		{
			var orderId 		= returnDetails[i].orderId;
			var receiptId 		= returnDetails[i].itemReceiptId;
			var cashRefundId 	= returnDetails[i].cashRefundId;
			var logId			= returnDetails[i].logId;
			
			if(logId != 0)			var logStatus 			= deleteLogsFromIds(logId); //Delete Logs
			if(cashRefundId > 0)	var cashRefund 			= nlapiDeleteRecord('cashrefund', 			cashRefundId, 		{deletionreason: 8}); //External System Rollback
			if(receiptId > 0)		var fulfillmentRecord 	= nlapiDeleteRecord('itemreceipt', 			receiptId, 			{deletionreason: 8}); //External System Rollback
			if(orderId > 0)			var orderRecord 		= nlapiDeleteRecord('returnauthorization', 	orderId, 			{deletionreason: 8}); //External System Rollback
			nlapiLogExecution('debug', 'Rollback record details', 'returnId: ' + orderId + ' | receiptId: ' + receiptId + ' | cashRefundId: ' + cashRefundId);
			recordCount++;
		}
		catch(err_rollback)
		{
			var newObj 				= new Object();
			newObj.status 			= 'Rollback Failure';
			newObj.message 			= 'The process failed to rollback.';
			newObj.details 			= err_rollback.message + ':' + err_rollback.getDetails();
			finalObject.transactions.push(newObj);
			nlapiLogExecution('debug', 'Rollback record details', 'orderId: ' + orderId + ' || Error: ' + err_rollback);
			body += '<br>The process failed to rollback.'
			break;
		}
	}
	if(returnDetails != null && recordCount > 0 && recordCount == returnDetails.length)
	{
		var newObj 				= new Object();
		newObj.status 			= 'Rollback Success';
		newObj.message 			= 'The process has been successfully rolled back and all the transactions listed above have been deleted.';
		finalObject.transactions.push(newObj);
		nlapiLogExecution('debug', 'Rollback back operation status', "Success");
		body += '<br>The process has been successfully rolled back and all the transactions listed above have been deleted.'
		return true;
	}
	else
	{
		nlapiLogExecution('debug', 'Rollback back operation status', "Failure");
		body += '<br>The process failed to rollback.'
		return false;
	}
}

//User Event After Submit function
function overrideTaxAmount(type)
{
	if(type != 'delete')
	{
		try
		{
			nlapiLogExecution('debug', nlapiGetRecordType(), nlapiGetRecordId());
			var record  = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());
			//Restrict override for Hybris Orders based on custbody_wtka_extsys_order_number field value
			var extOrder = record.getFieldValue('custbody_wtka_extsys_order_number');
			nlapiLogExecution('debug', 'extOrder', extOrder);
			if(extOrder != null && extOrder.length > 0)
			{
				// Set total tax amount 
				var taxamt1 = record.getFieldValue('custbody_wtka_taxamountoverride');
				var taxamt2 = record.getFieldValue('custbody_wtka_taxamount2override');
				
				record.setFieldValue('taxamountoverride',  taxamt1);
				record.setFieldValue('taxamount2override', taxamt2); //PST for Canadian Orders
				
				var recordId   = nlapiSubmitRecord(record, true); 
				nlapiLogExecution('debug', 'Taxes override successful', recordId);
			}
		}
		catch(override_err)
		{
			nlapiLogExecution('debug', 'Error in Overriding Taxes', override_err);
		}
	}
}

function sendReturnMail(message, request)
{
	subject  = 'Return processing failure';
	body 	 = 'Hello,<br><br>';
	body 	+= 'Process failed in NetSuite. <b>NetSuite Response: </b><br><br>';
	body	+= message;
	body 	+= '<br><br><b>Data received in NetSuite: </b><br><br>' + JSON.stringify(request) + '<br><br><br>';
	body 	+= '<br><br>Process needs to be resubmitted or handled manually through user interface after corrections.';
	body 	+= '<br><br><br><br><br><br><br>Thanks';
	body 	+= '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
	nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
	if(sendEmail)	nlapiSendEmail(243423, toList, subject, body, ccList);
}