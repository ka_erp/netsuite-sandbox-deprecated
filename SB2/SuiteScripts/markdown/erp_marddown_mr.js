/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 * @param {record} objRec
 * @param {search} objSearch
 * @param {error} objError
 * @param {email} objEmail
 * @param {runtime} objRuntime
 * 1) Schedule a search to look at custom records
 * 2) If the custom record matches [time frame] & active then run map reduce
 * 3)
 */
var PRICE_STATUS = {
  status:[1,2,3,4], // pending, in progress ,completed, error
  record:null,
  recordStat: null
};

define(['N/search',
        'N/record',
        'N/email',
        'N/runtime',
        'N/error',
        'N/file',
        ],
    function(search, record, email, runtime, error) {

		function getInputData()
	    {
			//Get all of the item records that need to be changed
      // the search contains the filters for the markdown

			log.debug('Get markdown data-  getInputData()');
			var searchObj =  search.load({
				id: 'customsearch_pa_items'
			});

      var itmsToAdj = [];
      var pagedData = searchObj.runPaged({ pageSize : 1000 });

              log.debug('input count: ',pagedData.count);
              pagedData.pageRanges.forEach(function (pageRange) {
                  var page = pagedData.fetch({ index : pageRange.index });
                  itmsToAdj = itmsToAdj.concat(page.data);
              });
      log.debug('items',JSON.stringify(itmsToAdj));
			log.debug('Search Results Input Data',JSON.stringify(searchObj));
			return itmsToAdj;
	    }

      // as this is a 1-1 ratio a reduce is not rquired for doing the price adjustments
        function map(context) {

        	log.debug('map parameters',context.value);
        	var searchResult = JSON.parse(context.value);
        	log.debug('Search Map Results: ',JSON.stringify(searchResult));

          var itm = {
            id: 0,
            priceLevel:0,
            currency:0,
            recLine:0,
            newRec: {
              price:0,
            },
            oldRec: {
              price:0,
            }
          };
// should this be set everytime I read a record line????

        record.submitFields({
            type:'customrecord_price_adjustment',
            id:searchResult.id,
            values: {
            custrecord_pa_status:'2' // in progress
            }
          });

          itm.id =searchResult.values.formulatext;

          var itemObj = record.load({ // 5 GOV
                        type: record.Type.INVENTORY_ITEM,
                        id: itm.id // internal ID of the item
                    });
        
          itm.newRec.price  = searchResult.values.formulacurrency;
          itm.currency = 'price'+searchResult.values.formulatext_2;
          itm.priceLevel =  searchResult.values.formulatext_1.toString();
          itm.adjId =  searchResult.values.formulanumeric; //price record id
          log.debug('Child adj ID ', JSON.stringify(itm.adjId));
      //    var priceAdjustmentRec = record.load({ type:'customrecord_price_adjustment', id: '100'});


                  //find the row that has the internal ID of the price level
                itm.recLine = itemObj.findSublistLineWithValue({
                    sublistId:itm.currency,
                    fieldId: 'pricelevel',
                    value: itm.priceLevel
                  });
                  log.debug('item rec line: ', JSON.stringify(itm.recLine ));



                itm.oldRec.price= itemObj.getMatrixSublistValue({
                    sublistId: itm.currency,
                    fieldId: 'price',
                    column:0, // always this as we don't do discounts on volume
                    line:itm.recLine
                   });

                //backup 3 GOV
                record.submitFields({
                       type:'customrecord_pa_child',
                       id:itm.adjId,
                       values: {
                       custrecord_pa_price_backup:itm.oldRec.price
                       }
                     });

                     log.debug('backup ', lg+itm.oldRec.price);

          itemObj.setMatrixSublistValue({
                          sublistId: itm.currency,
                          fieldId: 'price',
                          column: 0,
                          line: itm.recLine,
                          value: itm.newRec.price
                      });

          log.debug('new rec', JSON.stringify(itemObj));
          itemObj.save();

        	context.write(itm.id,1); //Key-Value pair ? - why?
        }


        function summarize(summary)
        {
            sendEmailLog();
        	  log.debug('summarizing...');
            var type = summary.toString();
            log.debug(type + ' Usage Consumed', summary.usage);
            log.debug(type + ' Number of Queues', summary.concurrency);
            log.debug(type + ' Number of Yields', summary.yields);
        }

        return {
            getInputData: getInputData,
            map: map,
            summarize: summarize
        };

        function getItemRecord (itemid,adjustmentPrice){}

        function sendEmailLog() {
            var senderId = -5;
            var recipientEmail = 'timothy.frazer@kitandace.com';
            var timeStamp = new Date().getUTCMilliseconds();
            var recipientId = 305596; // temp

            email.send({
                author: senderId,
                recipients: recipientId,
                subject: 'Test Sample Email Module',
                body: 'email body',

            });
        }



        function createAndSaveFile() {
            var fileObj = file.create({
                name: 'test.txt',
                fileType: file.Type.PLAINTEXT,
                contents: 'Hello World\nHello World'
            });
            fileObj.folder = -15;
            var id = fileObj.save();
            fileObj = file.load({
                id: id
            });
        }


    });
