/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope SameAccount
 */
define(['N/error', 'N/file', 'N/record', 'N/runtime', 'N/search'],
/**
 * @param {error} error
 * @param {file} file
 * @param {record} record
 * @param {runtime} runtime
 */
function(error, file, record, runtime, search) {
   
	const PUNCTUATION_REGEXP = /[\u2000-\u206F\u2E00-\u2E7F\\'!"#\$%&\(\)\*\+,\-\.\/:;<=>\?@\[\]\^_`\{\|\}~]/g;
    
	function getInputData() {
//        return "the quick brown fox \njumped over the lazy dog.".split('\n');
        
        
        var poSearch = search.load({
            id: 'customsearch_erp_mrtest_inventory' //Run Saved search: 'POs to Export to 3PL'
        });

		return poSearch;
        
        
        
    }
	
    function map(context) {
//        for (var i = 0; context.value && i < context.value.length; i++)
//            if (context.value[i] !== ' ' && !PUNCTUATION_REGEXP.test(context.value[i]))
//                context.write(context.value[i], 1);
    	
    	
    	var searchResult = JSON.parse(context.value); 			//log.debug('searchResult', searchResult );
     	var poId = searchResult.id;  							//log.debug('poId', poId );
        var poName = searchResult.values.itemid;				//log.debug('poName', poName );

        context.write(poId, poName);
    }
    
    function reduce(context) {
        context.write(context.key, context.value);
    }
    
    function summarize(summary) {
        var type = summary.toString();
        log.audit(type + ' Usage Consumed', summary.usage);
        log.audit(type + ' Number of Queues', summary.concurrency);
        log.audit(type + ' Number of Yields', summary.yields);
        var contents = '';
        
        summary.output.iterator().each(function(key, value) {
            contents += (key + ' ' + value + '\n');
            return true;
        });
        
        var fileObj = file.create({
            name: 'wordCountResult27.txt',
            fileType: file.Type.PLAINTEXT,
            contents: contents
        });
        fileObj.folder = -15;
        fileObj.save();
    	
    	
        log.audit(type + ' Saving', 'this');
    }

    return {
        getInputData: getInputData,
        map: map,
        reduce: reduce,
        summarize: summarize
    };
    
});
