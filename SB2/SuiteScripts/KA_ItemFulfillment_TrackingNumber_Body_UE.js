/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       18 Aug 2015     ivan.sioson
 * 1.10       6 Jan 2016      Tim.Frazer - Changed to Fedex
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function generateTrackingUserEventBeforeSubmit(type){
 	
	var intPackageCount = nlapiGetLineItemCount('package');
	var arrPackageDetails = new Array(); 
	
	for (var i = 1  ;  i <= intPackageCount; i++){
		nlapiSelectLineItem('package', i);
		var strTracking = nlapiGetCurrentLineItemValue('package', 'packagetrackingnumber');
			if(strTracking != ''){

        if(arrPackageDetails.indexOf(strTracking) >= 0){}else{				
					arrPackageDetails.push(strTracking); 	
					nlapiLogExecution('debug', 'array', arrPackageDetails.toString()); 
				}
			}               		
	}
  if (arrPackageDetails.length > 1){
  arrPackageDetails.join(",");
  }
	
	var strUrl = 'https://www.fedex.com/apps/fedextrack/?action=track&tracknumbers='; 
		
	nlapiSetFieldValue('custbody_ka_tracking_body_textarea', arrPackageDetails.toString());
	nlapiSetFieldValue('custbody_ka_tracking_body_field', '<a href="' + strUrl + arrPackageDetails + '">'+   "here"  +' </a>');  

}