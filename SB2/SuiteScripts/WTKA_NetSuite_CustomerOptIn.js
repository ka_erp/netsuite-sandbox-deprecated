/**
 *	File Name		:	WTKA_NetSuite_CustomerOptIn.js
 *	Function		:	Create Customers and set Opt-In
 * 	Authors			:	Rini Thomas
 *	Company			:	Wipro Limited
 *	Release Dates	:	29-June-2016 (v1.0)
 * 	Current Version	:	1.0
**/

{
	var ErrorObj 		 		= new Object();
	ErrorObj.status 	 		= "Error";
	ErrorObj.messages 	 		= new Array();
	ErrorObj.messages[0] 		= new Object();
	
	var customerUpdateFail		= 0,	 subsidiary;
	var sendEmail 		 		= true,	 changed = false;
	var ccList 			 		= null; //['']; //Enter CC Addresses as array
	var toList 			 		= ['3PL_eComm_Integration@kitandace.com'];
}

function createCustomer(dataIn)
{
	nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn));
	return customerUpdate(dataIn);
}

function userEventBeforeLoad(type, form, request)
{
	if((nlapiGetContext().getExecutionContext() == 'userinterface') && (type == 'view'))
	{
		var cols = new Array();
		cols[cols.length] = new nlobjSearchColumn('custrecord_wtka_entity_editype');
		cols[cols.length] = new nlobjSearchColumn('custrecord_wtka_entity_edinumber');
		cols[cols.length] = new nlobjSearchColumn('custrecord_wtka_entity');
		
		var filters = new Array();
		filters[filters.length] = new nlobjSearchFilter('custrecord_wtka_entity', null, 'anyof', nlapiGetRecordId());
		
		var searchResult = nlapiSearchRecord('customrecord_wtka_entitylogs', null, filters, cols);
		if(searchResult !=  null)	displayEntityLogs(form, searchResult);
	}
}

function displayEntityLogs(form, searchResult)
{
	var edinumber = searchResult[0].getText('custrecord_wtka_entity_edinumber');
	
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wtka_entity_parent', null, 'anyof', searchResult[0].getId())); //Parent Id
	
	var cols = new Array();
	cols.push(new nlobjSearchColumn('custrecord_wtka_entity_parent'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_entity_request'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_entity_response'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_entity_status'));
	cols.push(new nlobjSearchColumn('created'));
	
	var searchResultChild = nlapiSearchRecord('customrecord_wtka_entitylogs_child', null, filters, cols);
	if(searchResultChild != null)
	{
		form.addTab('custpage_wtkalogs', 'Integration Logs');
		var LogList = form.addSubList('custpage_wtkaloglist', 'list', searchResult[0].getText('custrecord_wtka_entity_edinumber'), 'custpage_wtkalogs');
		LogList.addField('custpage_wtkacreated', 	'date', 		'Date Created');
		LogList.addField('custpage_wtkatime', 		'timeofday', 	'Time');
		LogList.addField('custpage_wtkaedinumber', 	'text', 		'EDI Number');
		LogList.addField('custpage_wtkaedistatus', 	'text', 		'EDI Status');
		LogList.addField('custpage_wtkarequest', 	'textarea', 	'Request');
		LogList.addField('custpage_wtkaresponse', 	'textarea', 	'Response');
			
		for(var j=0; searchResultChild != null && j<searchResultChild.length; j++)
		{
			var createdDate = new Date (searchResultChild[j].getValue('created'));
		
			LogList.setLineItemValue('custpage_wtkacreated',	j+1, nlapiDateToString(createdDate, 'date')); 
			LogList.setLineItemValue('custpage_wtkatime',	    j+1, nlapiDateToString(createdDate, 'timeofday')); 
			LogList.setLineItemValue('custpage_wtkaedinumber', 	j+1, searchResult[0].getText('custrecord_wtka_entity_edinumber'));
			LogList.setLineItemValue('custpage_wtkaedistatus',  j+1, searchResultChild[j].getText('custrecord_wtka_entity_status'));
			LogList.setLineItemValue('custpage_wtkarequest', 	j+1, searchResultChild[j].getValue('custrecord_wtka_entity_request'));
			LogList.setLineItemValue('custpage_wtkaresponse', 	j+1, searchResultChild[j].getValue('custrecord_wtka_entity_response'));
		}

		if(searchResult.length > 1)
		{
			var filters1 = new Array();
			filters1.push(new nlobjSearchFilter('custrecord_wtka_entity_parent', null, 'anyof', searchResult[1].getId())); //Parent Id
			
			var searchResultChild1 = nlapiSearchRecord('customrecord_wtka_entitylogs_child', null, filters1, cols);
			if(searchResultChild1 != null)
			{
				edinumber 	 = searchResult[1].getText('custrecord_wtka_entity_edinumber');
				var LogList1 = form.addSubList('custpage_wtkaloglist1', 'staticlist', edinumber, 'custpage_wtkalogs');
				LogList1.addField('custpage_wtkacreated1', 		'date', 		'Date Created');
				LogList1.addField('custpage_wtkatime1', 		'timeofday', 	'Time');
				LogList1.addField('custpage_wtkaedinumber1', 	'text', 		'EDI Number');
				LogList1.addField('custpage_wtkaedistatus1', 	'text', 		'EDI Status');
				LogList1.addField('custpage_wtkarequest1', 		'textarea', 	'Request');
				LogList1.addField('custpage_wtkaresponse1', 	'textarea', 	'Response');
				
				for(var j=0; j < searchResultChild1.length; j++)
				{
					var createdDate = new Date (searchResultChild1[j].getValue('created'));
					LogList1.setLineItemValue('custpage_wtkacreated1',		j+1, nlapiDateToString(createdDate, 'date')); 
					LogList1.setLineItemValue('custpage_wtkatime1',	    	j+1, nlapiDateToString(createdDate, 'timeofday')); 
					LogList1.setLineItemValue('custpage_wtkaedinumber1', 	j+1, edinumber);
					LogList1.setLineItemValue('custpage_wtkaedistatus1',  	j+1, searchResultChild1[j].getText('custrecord_wtka_entity_status'));
					LogList1.setLineItemValue('custpage_wtkarequest1', 		j+1, searchResultChild1[j].getValue('custrecord_wtka_entity_request'));
					LogList1.setLineItemValue('custpage_wtkaresponse1', 	j+1, searchResultChild1[j].getValue('custrecord_wtka_entity_response'));
				}
			}
		}
	}
}
