/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Dec 2014     joel             Check for duplicate email.
 * 2.00       30 Sept 2015     anekascott             Check for duplicate email- not duplicate if guest email only
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	nlapiLogExecution('debug', 'SS_DuplicateEmailCheck', 'start');
	
	var result = { email: '', dup: false, acctStatus: '', subsidiary: '' };
	
	try {
		var params = request.getAllParameters();
		for (param in params) {
			nlapiLogExecution('debug', param, params[param]);
		}
		// duplicate check	
var filters = [];
			filters.push(new nlobjSearchFilter('email', null, 'is', params['email']));
		
		var columns = [];
		columns.push(new nlobjSearchColumn('email'));
		columns.push(new nlobjSearchColumn('custentity_acct_status'));
		columns.push(new nlobjSearchColumn('subsidiary'));
                columns.push(new nlobjSearchColumn('internalid'));
 columns.push(new nlobjSearchColumn('entityid'));
		columns.push(new nlobjSearchColumn('firstname'));
                columns.push(new nlobjSearchColumn('lastname'));
                var searchResults = nlapiSearchRecord('customer', null, filters, columns) || [];
		nlapiLogExecution('debug', 'searchResults.length', searchResults.length);
               
              var found = false;
               for ( var i = 0; searchResults != null && i < searchResults.length; i++ )
	        {
		        var res = searchResults[i];
		        var listID = (res.getValue('entityid'));
                        var fname = (res.getValue('firstname'));
                        var lname = (res.getValue('lastname'));
                        nlapiLogExecution('DEBUG', ("customer id " + listID+" Name: "+ fname));
                       if(fname=="Guest" && lname=="Shopper")
                       {
                              nlapiLogExecution('DEBUG', 'guest email found');
                              continue;
                        }

                       found = true;
                       break;
		      
		 }


	
	
		result.email = params['email'];
		
		if (found) {
			result.dup = true;
			var firstResult = searchResults[0];
			result.acctStatus = firstResult.getValue('custentity_acct_status') || '';
			result.subsidiary = firstResult.getValue('subsidiary') || '';
		}
	}
	catch (e) {
		nlapiLogExecution('error', 'SS_DuplicateEmailCheck: exception', e);
	}
	
	nlapiLogExecution('debug', 'JSON.stringify(result)', JSON.stringify(result));
	response.write(JSON.stringify(result));
}
