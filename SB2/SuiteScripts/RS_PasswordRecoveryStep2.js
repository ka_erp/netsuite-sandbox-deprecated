/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 Dec 2014     joel             Step 2 of password recovery.  This runs as an authenticated service.
 *
 */

var retJson = { 
	email: '', 
	status: false, 
	msgs: [] 
};

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function post(dataIn) {
	try {
		nlapiLogExecution('DEBUG', 'onPost', 'ENTERING');
		retJson.msgs.push('RS_PasswordRecoveryStep2: ' + 'post');
		retJson.msgs.push('dataIn: ' + dataIn);
		retJson.msgs.push('stringify(dataIn): ' + JSON.stringify(dataIn));

		var email = dataIn['email'];
		retJson.msgs.push('email: ' + JSON.stringify(email));
		
		// var template = nlapiLoadRecord('emailtemplate', 4);
		// retJson.msgs.push('emailtemplate: ' + JSON.stringify(template));

		var mergedRecord = nlapiMergeRecord(4, 'customer', 35067);	// 4 - Password Recovery Template, 35067 - LEAD jmcconaughy@exploreconsulting.com
		nlapiLogExecution('DEBUG', 'onPost', 'Record Merged');

		var body = mergedRecord.getValue();
		nlapiLogExecution('DEBUG', 'onPost', 'Get Body');
		nlapiLogExecution('DEBUG', 'onPost', JSON.stringify(body));
		retJson.msgs.push('body: ' + JSON.stringify(body));

		var records = new Object();
		records['entity'] = '11619';

		//nlapiSendEmail(11, ['jiajing28@gmail.com', 'jwang@exploreconsulting.com'], 'K&A - Forget Password', body, null, null, records);
		//nlapiSendEmail('test4@test.com', ['jiajing28@gmail.com', 'jwang@exploreconsulting.com'], 'K&A - Forget Password', body, null, null, records);
		nlapiSendEmail(11, ['jiajing28@gmail.com', 'jwang@exploreconsulting.com'], 'K&A - Forget Password', body, null, null, records);

		nlapiLogExecution('DEBUG', 'onPost', 'Post Send Email');
	}
	catch (e) {
		retJson.msgs.push('exception: ' + e);

		nlapiLogExecution('DEBUG', 'onPost error', e);
	}
	
	return JSON.stringify(retJson);
}