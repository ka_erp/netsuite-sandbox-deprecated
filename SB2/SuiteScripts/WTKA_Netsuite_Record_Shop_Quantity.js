

/*Distribution list to send out email based on ship country */
/*var SHIPMENT_EMAIL_DISTRIBUTION_LIST_CANADA= 'MyShipmentsCanada@kitandace.com';
var SHIPMENT_EMAIL_DISTRIBUTION_LIST_US= 'MyShipmentsUSA@kitandace.com';
*/

/*This method is capturing line item user changes for quantity recieved*/
function markLineChanged(type, name)
{
	try{		
			if(name=='custcol_wtka_quantity_recieved')
			{
				nlapiSetCurrentLineItemValue('item', 'custcol_wtka_netsuite_has_changed','T', true,true);					
			}
		}
	catch(err)
	{
		nlapiLogExecution('DEBUG',' Error in Item Reciept Save -verifyEnteredQuantityRecieved  ',err);		
	}		
}
/* Disable and defaul quantity recieved*/
function disableQuantityRecievedOnLineItem(type,name)
{
	try
	{	
		//If Inter company PO is associated then disable quantity recieved.
		//var hasPurchaseOrderAssociated= nlapiGetFieldValue('custbody_ka_intercompany_po');		
		
		var createdFrom= nlapiGetFieldText('createdfrom');			
		var hasTransferOrder=createdFrom.startsWith('Transfer Order');		
		
		nlapiLogExecution('DEBUG','Before Load Event','hasTransferOrder = '+hasTransferOrder +'Created From='+createdFrom);		
		var numlineItems = nlapiGetLineItemCount('item');
		
		for (var itemIndex=1; itemIndex<= numlineItems; itemIndex++)
				{
					//set default false value to this field.
					nlapiSetLineItemValue('item','custcol_wtka_netsuite_has_changed',itemIndex,'F');	
					
					var fulFilledQuantity= nlapiGetLineItemValue  ('item', 'quantity',itemIndex);
					var quantityRecieved= nlapiGetLineItemValue  ('item', 'custcol_wtka_quantity_recieved',itemIndex);
					
					//alert('Item=' +nlapiGetLineItemValue  ('item', 'item',itemIndex) + ' fulFilledQuantity= '+fulFilledQuantity + ' quantityRecieved ='+quantityRecieved );
					//No quantity set on quantity recieved then copy quantity field in quantity recieved.
					if (quantityRecieved<=0 || quantityRecieved=='')
					{						
							nlapiSetLineItemValue('item', 'custcol_wtka_quantity_recieved',itemIndex,fulFilledQuantity);	
					}
					//if transfer order then do not disable
					if(hasTransferOrder) {continue;}
					nlapiSetLineItemDisabled('item', 'custcol_wtka_quantity_recieved', true,itemIndex);						
				}				
		nlapiLogExecution('DEBUG','Before Load Event end','' );		
	}
	catch(err)
	{
		nlapiLogExecution('DEBUG',' Error  ',err);
		
	}	
}

function verifyEnteredQuantityRecieved(type)
{
	try{
		
		var context=nlapiGetContext().getExecutionContext();
		var contextType=type;
			
		
		nlapiLogExecution('DEBUG','verifyEnteredQuantityRecieved- Started','Context='+context +' Type='+contextType );		
		
		if ((context == 'userinterface') && ((contextType == 'edit')|| (contextType == 'create')  ))
		 {	
				var createdFrom= nlapiGetFieldValue('createdfrom')
				if (createdFrom<0) {return;}
			
				var headerInfo	 = new Object();
				headerInfo.date= nlapiGetFieldValue('createddate');
								
				var Orderfields = ['tranid', 'shipcountry', 'recordType'];
				var transferOrderInfo = nlapiLookupField('transferorder', createdFrom, Orderfields);
				if (transferOrderInfo==null) {return; /* no transfer Order associated*/ }
								
				headerInfo.transferOrderNumber= transferOrderInfo.tranid;
				headerInfo.shipcountry= transferOrderInfo.shipcountry;
				
				var locationFields = ['transferlocation' ,'location'];
				var transferOrderLocationInfo = nlapiLookupField('transferorder', createdFrom, locationFields,true);
				headerInfo.fromLocation=transferOrderLocationInfo.location; 
				headerInfo.toLocation= transferOrderLocationInfo.transferlocation; 
							
				//Remove locations hierarchy
				headerInfo.fromLocation = headerInfo.fromLocation.substring(headerInfo.fromLocation.lastIndexOf(":") + 1);
				headerInfo.toLocation = headerInfo.toLocation.substring(headerInfo.toLocation.lastIndexOf(":") + 1);
			
				var fields = ['email', 'altname' ]
				var employeeInfo = nlapiLookupField('employee', nlapiGetFieldValue('nluser'), fields);
				headerInfo.createBy=employeeInfo.altname;
				headerInfo.userEmail=employeeInfo.email;
				headerInfo.currentUserInternalID=nlapiGetFieldValue('nluser');
							
				nlapiLogExecution('DEBUG','Header values','date='+headerInfo.date+' , fromLocation='+headerInfo.fromLocation+', toLocation='+headerInfo.toLocation+' , transferOrderNumber='+headerInfo.transferOrderNumber+ ',createBy='+headerInfo.createBy+'  UserEmail='+headerInfo.userEmail+' Ship Country='+headerInfo.shipcountry+' current user internal id=' +headerInfo.currentUserInternalID   );	
				var numlineItems = nlapiGetLineItemCount('item');
				var arrayLineItems =  new Array();
				var iCounter=0
				for (var itemIndex=1; itemIndex<= numlineItems; itemIndex++)
					{									
						
						var quantity = nlapiGetLineItemValue('item','quantity',itemIndex);
						var quantityRecieved = nlapiGetLineItemValue('item','custcol_wtka_quantity_recieved',itemIndex);
						nlapiLogExecution('DEBUG','values ','quantity= '+quantity+' quantityRecieved='+quantityRecieved+'' );	
						var hasLineChangedbyUser = nlapiGetLineItemValue('item','custcol_wtka_netsuite_has_changed',itemIndex);
							nlapiLogExecution('DEBUG','hasLineChangedbyUser Checkbox value' , hasLineChangedbyUser);	
						
						if(quantity!=quantityRecieved && hasLineChangedbyUser=='T')
						{
							var lineItemsDetail	 = new Object();
							lineItemsDetail.upcCode= nlapiGetLineItemValue('item','custcol_ka_upc_code_pick_slip',itemIndex);
							lineItemsDetail.ItemDescription= nlapiGetLineItemValue('item','itemdescription',itemIndex);
							lineItemsDetail.quantityTransferred= nlapiGetLineItemValue('item','quantity',itemIndex);
							lineItemsDetail.quantityRecieved= quantityRecieved;							
							arrayLineItems[iCounter]=lineItemsDetail;
							iCounter++;
							
						}
						//nlapiSetLineItemValue('item','custcol_wtka_netsuite_has_changed',itemIndex,'F');				
						
					}	
					//nlapiCommitLineItem('item');
					
					nlapiLogExecution('DEBUG','Line Item Array from Email length' , arrayLineItems.length );	
					//Check If "arrayLineItems" has any item for sending email. 
					  if (arrayLineItems.length<=0) {/* No need any  action*/ return;}	
					//Send email.
					sendEmailToUser(arrayLineItems,headerInfo)
		 }
		
	}
	catch(err)
	{
		nlapiLogExecution('DEBUG',' Error in Item Reciept Save -verifyEnteredQuantityRecieved  ',err);		
	}		
}

function sendEmailToUser(arrayLineItems,headerInfo)
{
	try
	{
		
		var subject = 'Transfer Discrepancy';
		var body = '<br>';
		body += 'Date: '+headerInfo.date + '<br>'; 
		body +=	'Transfer Order Number: '+ headerInfo.transferOrderNumber+ '<br>';
		body += 'Ship From: '+ headerInfo.fromLocation+ '<br>';
		body += 'Ship To: '+ headerInfo.toLocation+ '<br><br>';
		
		
		for (i=0; i<arrayLineItems.length; i++)
		{
			var lineItemsDetail= arrayLineItems[i];
			body += 'UPC: ' +lineItemsDetail.upcCode+	', Item Description: ' + lineItemsDetail.ItemDescription+	', Quantity Transferred: ' + lineItemsDetail.quantityTransferred+	', Quantity Received: ' +lineItemsDetail.quantityRecieved +'<br>' ;
		}		
		
		var emailDistributionList= '';	

		emailDistributionList=getEmailDistributionList(headerInfo.shipcountry);
		
		/*switch (headerInfo.shipcountry.toUpperCase())
		{
			case 'US' : 
				emailDistributionList=SHIPMENT_EMAIL_DISTRIBUTION_LIST_US;
				break;
			case 'CA' : 
				emailDistributionList=SHIPMENT_EMAIL_DISTRIBUTION_LIST_CANADA;
				break;
		}*/
		
		if(emailDistributionList=='')
		{
			nlapiLogExecution('ERROR','ERROR- Distrubition List','No Email distribution is configured');	
			return;			
		}
		
		
		var emailTo = headerInfo.userEmail;
		
		if (emailDistributionList)
		{
			emailTo   = emailTo+ ','+ emailDistributionList;
		}
		nlapiLogExecution('DEBUG', 'Email Notifications', 'Subject: ' + subject + ' Body: ' + body+ 'Email To '+  emailTo +' Current User Internal ID '+headerInfo.currentUserInternalID);
		nlapiSendEmail(/*798313*/headerInfo.currentUserInternalID,emailTo , subject, body);
	}
	catch(err)
	{
		nlapiLogExecution('DEBUG',' sendEmailToUser   ',err);		
	}		
}

function getEmailDistributionList(countryName)
{
	try{
		var columns = new Array();
		columns[0] = new nlobjSearchColumn( 'name' );
		columns[1] = new nlobjSearchColumn( 'custrecord_wtka_email_dist_email' );
				
			var filters = new Array();
				filters[0] = new nlobjSearchFilter( 'name', null, 'is', countryName.toUpperCase());			
				var searchresults = nlapiSearchRecord('customrecord_wtka_email_dist_country', null, filters,columns);		
				//Check if no currency available which has inclusive TAX.
				if (searchresults!=null && searchresults.length==1)
				{	var searchresult = searchresults[ 0 ];				
					var emailDistributionList= searchresult.getValue( 'custrecord_wtka_email_dist_email' );		
					nlapiLogExecution('DEBUG', 'emailDistributionList',emailDistributionList);		
					return  emailDistributionList	;				
				}
				else{
					return '';
				}				
	}
	catch(err)
	{
		nlapiLogExecution('DEBUG',' getEmailDistributionList',err);		
	}	
	
}


