/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       31 Dec 2014     joel
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	try {

		var params = request.getAllParameters()
		email = params['email'];

		nlapiLogExecution('DEBUG', 'email', email);

		var filters = [
            new nlobjSearchFilter('email', null, 'is', email)
        ];

		var columns = new Array();
		columns.push(new nlobjSearchColumn('email'));

		var searchResults = nlapiSearchRecord('customer', null, filters, columns) || [];

		nlapiLogExecution('DEBUG', 'searchResults', JSON.stringify(searchResults));
		
		if (searchResults.length) {
			custId = searchResults[0].getId();
			nlapiLogExecution('debug', 'custId', custId);
			cust = nlapiLoadRecord('customer', custId);
			nlapiLogExecution('debug', 'cust', cust);
			cust.setFieldValue('custentity_acct_status', 3);
			nlapiSubmitRecord(cust);
		}
		
	}
	catch (e) {
		nlapiLogExecution('error', 'exception', e);
	}
}