var KA_CASH_CRASH_RETURN_FORM_INTERNALID = '189';
var KA_CASH_CRASH_NEW_SALE_FORM_INTERNALID = '188';		
var TAX_INCLUSIVE_CURRENCY_LIST= 'AUD,EUR,GBP';
/*

Item prices for UK and Australia are maintained in Netsuite including Taxes.when credit memos and invoice are creating, duplicate tax is getting calculates. 
This method is use to strip out tax while creating Invoice and credit memos. 
*/
function onBeforeSubmit_stripoutTaxFromPrice()
{
	try
	{
		
		//Check if form='K&A Cash Crash Return' or 'K&A Cash Crash New Sale'
		
		var transactionFormInternalID=	nlapiGetFieldValue('customform');	
		nlapiLogExecution('DEBUG', 'Transaction Form ID ',transactionFormInternalID);
		if (transactionFormInternalID==KA_CASH_CRASH_RETURN_FORM_INTERNALID ||  transactionFormInternalID==KA_CASH_CRASH_NEW_SALE_FORM_INTERNALID)
		{			
	
			//1. Check transaction currency - if this does not come in list of tax inclusive currency then EXIT 
			var transactionCurrency= nlapiGetFieldValue('currencyname');
			var filters = new Array();
			filters[0] = new nlobjSearchFilter( 'name', null, 'is', transactionCurrency );			
			var searchresults = nlapiSearchRecord('customrecord_wtka_tax_inclusive_curr', null, filters,null);		
				
			nlapiLogExecution('DEBUG', 'Transaction Currency ',transactionCurrency);	
			//Check if no currency available which has inclusive TAX.
			if (searchresults!=null && searchresults.length==1)
			{	
			nlapiLogExecution('DEBUG', 'Search result',searchresults.length);		
			
			//2. Strip out rate with tax for specific currencies. 		
				var numlineItems = nlapiGetLineItemCount('item');
				
				for (var itemIndex=1; itemIndex<= numlineItems; itemIndex++)
				{
					nlapiSelectLineItem('item', itemIndex); 				
					var itemRate = nlapiGetLineItemValue('item','rate',itemIndex);
					var taxAmount = nlapiGetLineItemValue('item','taxrate1',itemIndex);
					taxAmount=taxAmount.replace('%','');
					nlapiLogExecution('DEBUG', 'ItemRate='+ itemRate+' TaxAmount='+taxAmount+' ');			
					var itemRateExcludingTax=  eval(itemRate * 100)/eval(eval(100)+eval(taxAmount)); 			
					nlapiLogExecution('DEBUG', 'Calculated Item rate (Excluded Tax)', itemRateExcludingTax.toFixed(2) );
					nlapiSetCurrentLineItemValue('item', 'amount', itemRateExcludingTax.toFixed(2), true,true);
					nlapiCommitLineItem('item'); 
				}
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('DEBUG','Error in Tax Stripout Script',err);
		
	}
	
}


/*
Item prices for UK and Australia are maintained in Netsuite including Taxes.when credit memos and invoice are creating, duplicate tax is getting calculates. 
This method is use to strip out tax while creating Invoice and credit memos. 
*/
function stripOutTaxFromPrice(type)
{
		
		var transactionFormInternalID=	nlapiGetFieldValue('customform');	
		nlapiLogExecution('DEBUG', 'Transaction Form ID ',transactionFormInternalID);				
		
		//if user doesn't select item and click on add button  then script should not trigger.
		if (nlapiGetCurrentLineItemValue('item','item')==''){return true;}
		
		//Check if form='K&A Cash Crash Return' or 'K&A Cash Crash New Sale'
		if (transactionFormInternalID==KA_CASH_CRASH_RETURN_FORM_INTERNALID ||  transactionFormInternalID==KA_CASH_CRASH_NEW_SALE_FORM_INTERNALID)
		{
			try
			{
			
				//1. Check transaction currency - if this does not come in list of tax inclusive currency then EXIT 
				var transactionCurrency= nlapiGetFieldValue('currencyname');
				
				var filters = new Array();
				filters[0] = new nlobjSearchFilter( 'name', null, 'is', transactionCurrency.toUpperCase());			
				var searchresults = nlapiSearchRecord('customrecord_wtka_tax_inclusive_curr', null, filters,null);		
				//Check if no currency available which has inclusive TAX.
				if (searchresults!=null && searchresults.length==1)
				{
						nlapiLogExecution('DEBUG', 'Search result',searchresults.length);							
						var itemRate = nlapiGetCurrentLineItemValue('item','rate');						
						var taxAmount = nlapiGetCurrentLineItemValue('item','taxrate1');
						taxAmount=taxAmount.replace('%','');
						nlapiLogExecution('DEBUG', 'ItemRate='+ itemRate+' TaxAmount='+taxAmount+' ');			
						var itemRateExcludingTax=  eval(itemRate * 100)/eval(eval(100)+eval(taxAmount)); 			
						nlapiLogExecution('DEBUG', 'Calculated Item rate (Excluded Tax)', itemRateExcludingTax.toFixed(2) );
						nlapiSetCurrentLineItemValue('item', 'amount', itemRateExcludingTax.toFixed(2), true,true);				
					
				}
			}catch(err)
			{
				alert('Error while stripping out tax, You must contact to administrator' +err.message);
				return false;
				
			}			
		}	
		return true;
}
