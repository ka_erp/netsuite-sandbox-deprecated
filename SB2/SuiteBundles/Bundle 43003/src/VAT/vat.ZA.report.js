/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var VAT;
if (!VAT) VAT = {};
if (!VAT.ZA) VAT.ZA = {};

VAT.ZA.ONLINE_TEMPLATE_ENG = "VAT_HTML_ZA_ENG";
VAT.ZA.PRINTING_TEMPLATE_ENG = "VAT_PDF_ZA_ENG";

VAT.ZA.Data = function(startPeriodId, endPeriodId, subId, isConsolidated, salecacheid, purchasecacheid) {
    var _CountryCode = "ZA";
    var taxmap = {
    	box015: {id: "box015", label: "Box 15", taxcodelist:[{taxcode: "S", available: "PURCHASE"}]},
    	box014: {id: "box014", label: "Box 14", taxcodelist:[{taxcode: "SC", available: "PURCHASE"}]},
    	box014A: {id: "box014A", label: "Box 14A", taxcodelist:[{taxcode: "I", available: "PURCHASE"}]}
    };
    this.TaxMap = taxmap;
    var _Defs = {
		S: function(taxcode) {
            return taxcode.CountryCode == _CountryCode && taxcode.Rate > 0 && !taxcode.IsCapitalGoods && !taxcode.IsForExport && !taxcode.IsImport && !taxcode.IsExempt;
        },
		SC: function(taxcode) {
            return taxcode.CountryCode == _CountryCode && taxcode.IsCapitalGoods && !taxcode.IsForExport && !taxcode.IsImport && !taxcode.IsExempt;
        },
        Z: function(taxcode) {
            return taxcode.CountryCode == _CountryCode && taxcode.Rate == 0.00 && !taxcode.IsCapitalGoods && !taxcode.IsForExport && !taxcode.IsImport && !taxcode.IsExempt;
        },
		O: function(taxcode) {
            return taxcode.CountryCode == _CountryCode && !taxcode.IsCapitalGoods && taxcode.IsForExport && !taxcode.IsImport && !taxcode.IsExempt;
        },
		I: function(taxcode) {
            return taxcode.CountryCode == _CountryCode && !taxcode.IsCapitalGoods && !taxcode.IsForExport && taxcode.IsImport && !taxcode.IsExempt;
        },
		E: function(taxcode) {
            return taxcode.CountryCode == _CountryCode && !taxcode.IsCapitalGoods && !taxcode.IsForExport && !taxcode.IsImport && taxcode.IsExempt;
        }
	};
    this.TaxDefinition = _Defs;
    var _DR = new VAT.DataReader(new VAT.TaxcodeDefinitions(_CountryCode, _Defs), startPeriodId, endPeriodId, subId, isConsolidated, salecacheid, purchasecacheid);

	this.GetHeaderData = GetHeaderData;
	function GetHeaderData(languageCode) {
		var headerData = new VAT.DataHeader(startPeriodId, endPeriodId, subId, isConsolidated, languageCode, null, _CountryCode);
		return headerData;
	}
	
    this.GetData = function() {
        var sales = _DR.GetSalesSummary();
        var purchases = _DR.GetPurchaseSummary();
        var salesadj = _DR.GetSalesAdjustmentSummary(taxmap);
        var purchaseadj = _DR.GetPurchaseAdjustmentSummary(taxmap);
        var obj = {};
        var emptyboxes = ["box009", "box012", "box015A", "box016", "box017", "box018", "box005", "box006", "box007", "box008", "box010", "boxlesscredit", "boxaddpenalty", "boxinterest"];
        for(var iempty = 0; iempty < emptyboxes.length; iempty++) { obj[emptyboxes[iempty]] = 0;}
		
		var fractionMultiplier = 0.12280;
		
		obj.box011 = obj.box010 * fractionMultiplier;
		obj.box001 = sales.Of("S").GrossAmount;
		obj.box004 = obj.box001 * fractionMultiplier;
		obj.box001A = sales.Of("SC").GrossAmount;
		obj.box004A = obj.box001A * fractionMultiplier;
		obj.box002 = sales.Of("Z").NetAmount;
		obj.box002A = sales.Of("O").NetAmount;
		obj.box003 = sales.Of("E").NetAmount;
		
		obj.box015 = purchases.Of("S").TaxAmount + purchaseadj.Of("box015", "S").TaxAmount;
		obj.box014 = purchases.Of("SC").TaxAmount + purchaseadj.Of("box014", "SC").TaxAmount;
		obj.box014A = purchases.Of("I").TaxAmount + purchaseadj.Of("box014A", "I").TaxAmount;
		
		obj.box013 = obj.box004 + obj.box004A + obj.box009 + obj.box011 + obj.box012;
		obj.box019 = obj.box014 + obj.box014A + obj.box015 + obj.box015A + obj.box016 + obj.box017 + obj.box018;
		obj.box020 = obj.box013 - obj.box019;
		obj.boxtotalpenalty = obj.boxaddpenalty + obj.boxinterest;
		obj.boxtotalvatamtpay = obj.box020 + obj.boxtotalpenalty - obj.boxlesscredit;
		
        return obj;
    };
	
    this.GetDrilldownData = function(boxNumber) {
        var data = [];

        switch (boxNumber) {
	        case 'box001': case 'box004': data = _DR.GetSalesDetails(["S"]); break;
			case 'box001A': case 'box004A': data = _DR.GetSalesDetails(["SC"]); break;
			case 'box002': data = _DR.GetSalesDetails(["Z"]); break;
			case 'box002A': data = _DR.GetSalesDetails(["O"]); break;
			case 'box003': data = _DR.GetSalesDetails(["E"]); break;
			case 'box015': data = _DR.GetPurchaseDetails(["S"], "box015"); break;
			case 'box014': data = _DR.GetPurchaseDetails(["SC"], "box014"); break;
			case 'box014A': data = _DR.GetPurchaseDetails(["I"], "box014A"); break;
        }
       
        return data;
    };
};

VAT.ZA.GetReportData = function(subId, fromPeriodId, toPeriodId, className, isConsolidated) {
    var ds = {};
	var dataSource = new VAT.ZA.Data(fromPeriodId, toPeriodId, subId, isConsolidated);
	var data = dataSource.GetData();
	var headerData = dataSource.GetHeaderData();
	
	ds.ToPeriodId = toPeriodId;
	ds.FromPeriodId = fromPeriodId;
    ds.SubId = subId;
    ds.ReportIndex = className;
	
	for(var i in data) {
		ds[i] = nlapiFormatCurrency(data[i]);
	}
	
	for(var j in headerData) {
		ds[j] = headerData[j];
	}
	
    return ds;
};

VAT.ZA.GetPrintData = function(subId, fromPeriodId, toPeriodId, userInfo, params, isConsolidated) {
    var ds = VAT.ZA.GetReportData(subId, fromPeriodId, toPeriodId, params.ReportIndex, isConsolidated);
	for (var k in params) {
		if (params[k]) {
			ds[k] = params[k];
		} else {
			ds[k] = '&nbsp;';
		}
	}
	ds.library = _App.GetLibraryFile(VAT.LIB.FORMAT).getValue();
    return ds;
};

VAT.ZA.GetDrilldownData = function(boxNum, startPeriodId, endPeriodId, subId, isConsolidated, languagecode, salecacheid, purchasecacheid) {
	var ds = [];
	var dataSource = new VAT.ZA.Data(startPeriodId, endPeriodId, subId, isConsolidated, salecacheid, purchasecacheid);
	var rptData = dataSource.GetDrilldownData(boxNum);
	
	ds.ReportData = rptData;
	return ds;
};

if (!VAT.ZA.ENG) VAT.ZA.ENG = {};
VAT.ZA.ENG.Report = function() {
    this.ClassName = "VAT.ZA.ENG.Report";
    this.CountryCode = "ZA";
    this.LanguageCode = "ENG";
    this.Name = "South Africa (English)";
	this.Actions = VAT.ZA.ReportActions;
	this.ReportName = "VAT 201";
	this.HelpURL = "/app/help/helpcenter.nl?topic=DOC_SouthAfricaTaxTopics";
	this.HelpLabel = "Click here for South Africa VAT Help Topics";
	this.isAdjustable = true;
	
    this.GetAdjustmentMetaData = function (fromPeriodId, toPeriodId, subId, isConsolidated) {
    	var ds = new VAT.ZA.Data(fromPeriodId, toPeriodId, subId, isConsolidated);
    	
    	return {
    		TaxDefinition: ds.TaxDefinition,
    		TaxMap: ds.TaxMap
    	};
    };

    this.GetReportTemplate = function(fromPeriodId, toPeriodId) {
    	var template = getTaxTemplate(VAT.ZA.ONLINE_TEMPLATE_ENG);
        return _App.RenderTemplate(template.short, {imgurl : template.imgurl});
    };
    
    this.GetPrintTemplate = function(fromPeriodId, toPeriodId) {
    	var template = getTaxTemplate(VAT.ZA.PRINTING_TEMPLATE_ENG);
        return _App.RenderTemplate(template.short, {
        	imgurl : nlapiEscapeXML(template.imgurl),
        	koodak : nlapiEscapeXML(template.koodak)});
    };

    this.GetData = function(fromPeriodId, toPeriodId, subId, isConsolidated) {
        return VAT.ZA.GetReportData(subId, fromPeriodId, toPeriodId, this.ClassName, isConsolidated);
    };

	this.GetPrintData = function(fromPeriodId, toPeriodId, subId, isConsolidated, userInfo, params) {
        return VAT.ZA.GetPrintData(subId, fromPeriodId, toPeriodId, userInfo, params, isConsolidated);
    };
	
	this.GetDrilldownData = function(boxNum, startPeriodId, endPeriodId, subId, isConsolidated, salecacheid, purchasecacheid) {
		return VAT.ZA.GetDrilldownData(boxNum, startPeriodId, endPeriodId, subId, isConsolidated, this.LanguageCode, salecacheid, purchasecacheid);
	};
};
