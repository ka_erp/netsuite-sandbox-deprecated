/**
 * © 2015 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or
 * otherwise make available this code.
 *
 * @NScriptName FAM Date Utility
 * @NScriptId _fam_util_date
 * @NApiVersion 2.0
*/

define([], function (){
    return {
        countMonths : function countMonths(start, end) {
            var additional = 0;            
            
            // To prevent negative value
            if (end.getTime() < start.getTime()){
                return 0;
            }
            if (end.getDate() > start.getDate() - 1) {
                additional = 1; 
            }
            return 12 * (end.getFullYear() - start.getFullYear()) + 
                    end.getMonth() - start.getMonth() + additional;
        }
    };
});