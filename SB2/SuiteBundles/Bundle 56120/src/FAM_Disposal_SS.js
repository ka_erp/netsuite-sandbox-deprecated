/**
 * © 2015 NetSuite Inc.
 * User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
**/

var FAM;
if (!FAM) { FAM = {}; }

/**
 * Starter for Background Processing function for Asset Disposal
 *
 * Parameters:
 *     BGP {FAM.BGProcess} - Process Instance Record for this background process
 * Returns:
 *     true {boolean} - processing should be requeued
**/
function famAssetDisposal(BGP) {
    var assetDisposal = new FAM.Disposal_SS(BGP);

    try {
        assetDisposal.run();
        return true;
    }
    catch (e) {
        assetDisposal.logObj.printLog();
        throw e;
    }
}

/**
 * Constructor class
 *
 * Parameters:
 *     procInsRec {Object} - Process Instance Record
 * Returns:
 *     void
**/
FAM.Disposal_SS = function (procInsRec) {
    this.procInsRec = procInsRec;

    this.logObj = new printLogObj('debug');

    this.perfTimer = new FAM.Timer();

    this.assetCache = new FAM.FieldCache('customrecord_ncfar_asset');
    this.accCache = new FAM.FieldCache('account');
    this.currCache = new FAM.FieldCache('currency');
    this.currSymbol = FAM.SystemSetup.getSetting('nonDecimalCurrencySymbols');
    
    
    this.totalRecords = +this.procInsRec.getFieldValue('rec_total') || 0;
    this.recsProcessed = +this.procInsRec.getFieldValue('rec_count') || 0;
    this.recsFailed = +this.procInsRec.getFieldValue('rec_failed') || 0;

    this.recList = { Accounting : 1, TaxMethod : 2 };
    this.rollbackData = { invoice : [], journal : [], history : [], asset : {}, tax : {} };

    if (FAM.SystemSetup.getSetting('allowCustomTransaction') === 'T') {
        this.journalEntryRec = 'customtransaction_fam_disp_jrn';
        this.jrnReversDateId = 'custbody_fam_jrn_reversal_date';
    }
    else {
        this.journalEntryRec = 'journalentry';
        this.jrnReversDateId = 'reversaldate';
    }
    
    this.perfTimer.start();
};

FAM.Disposal_SS.prototype.execLimit = 500;
FAM.Disposal_SS.prototype.timeLimit = 30 * 60 * 1000; // 30 minutes

/**
 * Main function for this class
 *
 * Parameters:
 *     none
 * Returns:
 *     void
**/
FAM.Disposal_SS.prototype.run = function () {
    this.logObj.startMethod('FAM.Disposal_SS.run');

    var blnToRequeue, status = +this.procInsRec.getFieldValue('status');

    if (this.procInsRec.getFieldValue('rollback_data')) {
        this.rollbackData = JSON.parse(this.procInsRec.getFieldValue('rollback_data'));
    }

    if (status === FAM.BGProcessStatus.InProgress) {
        try {
            blnToRequeue = this.resumeIterateTaxMethod();
            if (!blnToRequeue) {
                blnToRequeue = this.iterateAssets();
            }
        }
        catch (e) {
            status = FAM.BGProcessStatus.Reverting;
            this.logObj.printLog();
            this.procInsRec.catchError(e, status);
        }
    }
    if (status === FAM.BGProcessStatus.Reverting) {
        blnToRequeue = this.revertChanges();
    }

    this.updateProcIns(blnToRequeue, status);
    this.logObj.endMethod();
};

/**
 * Resumes Tax Method Disposal from Requeued script
 *
 * Parameters:
 *     none
 * Returns:
 *     {boolean} - True if needs more requeueing else false
**/
FAM.Disposal_SS.prototype.resumeIterateTaxMethod = function () {
    this.logObj.startMethod('FAM.Disposal_SS.resumeIterateTaxMethod');
    var blnToRequeue  = false,
        lastAssetId   = +this.procInsRec.getScriptParam('lowerLimit') || 0,
        lastTaxMethId = +this.procInsRec.getScriptParam('upperLimit') || 0;

    if(lastAssetId && lastTaxMethId) {
        var runResult = this.iterateTaxMethod(lastAssetId, lastTaxMethId);

        blnToRequeue  = runResult.blnToRequeue;
        lastTaxMethId = runResult.lastTaxMethId;

        if (blnToRequeue) {
            this.procInsRec.setScriptParams({
                lowerLimit : lastAssetId,
                upperLimit : lastTaxMethId
            });
        }
    }
    this.logObj.endMethod();
    return blnToRequeue;
};

/**
 * Dispose Asset Indicated via statevalue info
 *                 asset - Asset Id to dispose
 *                 date  - Disposal Date
 *                 type  - 1 = Sale, 2 = Write-Off
 *                 qty   - Disposal Quantity
 *                 item  - Non Inventory Item ID
 *                 cust  - Customer ID
 *                 amt   - Disposal Amount
 *                 tax   - Tax Code ID
 *                 loc   - Location Id
 *                 JrnPermit - User Permissions (0=None, 1=View, 2=Create, 3=Edit, 4=Full)
 * Parameters: None
 * Returns:
 *     {boolean} - true if needs requeueing else false
**/
FAM.Disposal_SS.prototype.iterateAssets = function () {
    this.logObj.startMethod('FAM.Disposal_SS.iterateAssets');

    var i, blnToRequeue = false, runResult = {},
        lastAssetId   = +this.procInsRec.getScriptParam('lowerLimit') || 0,
        lastTaxMethId = 0,
        disposalObj   = this.procInsRec.stateValues,
        assetResult   = this.searchAssets(disposalObj.asset, lastAssetId);
        this.totalRecords = assetResult.length;
        
    for(i = 0; i < assetResult.length && !blnToRequeue; i++) {
        lastTaxMethId = 0;
        this.disposeRecord(assetResult.getResult(0), this.recList.Accounting);
        lastAssetId = assetResult.getValue(i, 'internalid');
        this.logObj.logExecution('Processed Asset Id: ' + assetResult.getValue(i, 'internalid') +
            ' Time Elapsed: ' + this.perfTimer.getReadableElapsedTime());

        runResult = this.iterateTaxMethod(assetResult.getValue(i, 'internalid'), lastTaxMethId);
        blnToRequeue  = runResult.blnToRequeue;
        lastTaxMethId = runResult.lastTaxMethId;
        if (!blnToRequeue) {
            this.recsProcessed++;
        }
    }
    if (blnToRequeue) {
        this.logObj.logExecution('Execution Limit | Remaining Usage: ' +
            FAM.Context.getRemainingUsage() + ' | Time Elapsed: ' +
            this.perfTimer.getReadableElapsedTime());
        this.procInsRec.setScriptParams(
            { lowerLimit : lastAssetId,
              upperLimit : lastTaxMethId });
    }

    this.logObj.endMethod();
    return blnToRequeue;
};

/**
 * Dispose All Tax Method for a given asset
 *
 * Parameters:
 *     parentAsset {int} - Asset Id for Tax Methods to Process
 *     lastTaxMethId {int} - last Tax Method Id for Requeueing continuation
 * Returns:
 *     {Object} error: error message, if any
 *              blnToRequeue: true will requeue otherwise false
 *              lastTaxMethId: last tax method id processed
**/
FAM.Disposal_SS.prototype.iterateTaxMethod = function (parentAsset, lastTaxMethId) {
    this.logObj.startMethod('FAM.Disposal_SS.iterateTaxMethod');
    var j, retObj = { blnToRequeue : false},
        taxResult = this.searchAssetTaxMethods(parentAsset, lastTaxMethId);

    for(j = 0; j < taxResult.length; j++) {
        if (this.hasExceededLimit()) {
            retObj.blnToRequeue = true;
            break;
        }

        this.logObj.clearMsg();

        this.disposeRecord(taxResult.getResult(j), this.recList.TaxMethod);

        retObj.lastTaxMethId = +taxResult.getValue(j, 'internalid') || 0;
        this.logObj.logExecution('Processed Tax Id: ' + taxResult.getValue(j, 'internalid') +
            ' Time Elapsed: ' + this.perfTimer.getReadableElapsedTime());
    }

    this.logObj.endMethod();
    return retObj;
};

/**
 * Dispose an accounting or tax method record
 *
 * Parameters:
 *     famResult {FAM.SearchResult} - Result Set containing Accounting or Tax Method Record
 *     recType {int} - Specific if processing for Accounting or Tax Method Record
 *
 * Returns:
 *     void
**/
FAM.Disposal_SS.prototype.disposeRecord = function (famResult, recType) {
    this.logObj.startMethod('FAM.Disposal_SS.disposeRecord');

    var journalId = null, invoiceId = null,
        disposalObj = this.procInsRec.stateValues,
        recObj = this.getInformation(famResult, recType);

    this.adjustQuantity(recObj);

    if (disposalObj.type == FAM.DisposalType['Sale']) {
        if (recType === this.recList.Accounting) {
            invoiceId = this.createInvoice(recObj);
            
            this.logObj.logExecution('created invoice: ' + invoiceId);
            if (!invoiceId) {
                throw FAM.resourceManager.GetString('custpage_invoicefail', 'assetdisposal');
            }
        }
    }

    if (recObj.bookingId || recType === this.recList.Accounting) {
        journalId = this.writeJournal(recObj);
        this.logObj.logExecution('created journal: ' + journalId);
    }

    this.writeHistory(recObj, journalId);

    this.updateRecord(famResult.getId(), recType, invoiceId, recObj);
    this.createRecordRollbackData(famResult, recType);

    this.logObj.endMethod();
};

/**
 *  Adjust asset disposal quantity and adjustment for partial depreciation
 *
 * Parameters:
 *     recObj {object} - Asset or Tax Method Record Object
 * Returns:
 *     void
 */
FAM.Disposal_SS.prototype.adjustQuantity = function (recObj) {
    this.logObj.startMethod('FAM.Disposal_SS.adjustQuantity');

    var disposeQty = +this.procInsRec.stateValues.qty;

    if(disposeQty <= 0 || disposeQty > recObj.quantity) {
        throw FAM.resourceManager.GetString('custpage_quantityerror', 'assetdisposal', null,
            [0, recObj.quantity]);
    }
    else if (disposeQty < recObj.quantity) {
        //For Partial Disposal
        var newRatio   = 1-(disposeQty / recObj.quantity), diff;
        diff = FAM.Util_Shared.Math.roundByCurrency((recObj.currCost * newRatio),
                                recObj.currSym, this.currSymbol);
        recObj.currCostAdj = FAM.Util_Shared.Math.roundByCurrency((recObj.currCost - diff),
                            recObj.currSym, this.currSymbol);
        recObj.currCost = diff;

        diff = FAM.Util_Shared.Math.roundByCurrency((recObj.cumDepr * newRatio),
                                recObj.currSym, this.currSymbol);
        recObj.cumDeprAdj = FAM.Util_Shared.Math.roundByCurrency((recObj.cumDepr - diff),
                                recObj.currSym, this.currSymbol);
        recObj.cumDepr = diff;

        recObj.currNBVAdj = FAM.Util_Shared.Math.roundByCurrency((recObj.currCostAdj - recObj.cumDeprAdj),
                                recObj.currSym, this.currSymbol);
        recObj.currNBV = FAM.Util_Shared.Math.roundByCurrency((recObj.currCost - recObj.cumDepr),
                                recObj.currSym, this.currSymbol);
        recObj.origCost = FAM.Util_Shared.Math.roundByCurrency((recObj.origCost * newRatio),
                                recObj.currSym, this.currSymbol);
        recObj.resValue = FAM.Util_Shared.Math.roundByCurrency((recObj.resValue * newRatio),
                                recObj.currSym, this.currSymbol);
        recObj.priorNbv = FAM.Util_Shared.Math.roundByCurrency((recObj.priorNbv * newRatio),
                                recObj.currSym, this.currSymbol);
        recObj.lastDeprAmount = FAM.Util_Shared.Math.roundByCurrency((recObj.lastDeprAmount * newRatio),
                                recObj.currSym, this.currSymbol);
    }
    else {
        //Manually assign values for Full Depreciation
        recObj.currCostAdj = FAM.Util_Shared.Math.roundByCurrency(recObj.currCost, recObj.currSym, this.currSymbol);
        recObj.cumDeprAdj = FAM.Util_Shared.Math.roundByCurrency(recObj.cumDepr, recObj.currSym, this.currSymbol);
        recObj.currNBVAdj = FAM.Util_Shared.Math.roundByCurrency(recObj.currNBV, recObj.currSym, this.currSymbol);
        recObj.currNBV = 0;
        recObj.priorNbv = 0;
        recObj.cumDepr = 0;
        recObj.lastDeprAmount = 0;
    }

    recObj.quantity -= disposeQty;
    recObj.qtyDisposed += disposeQty;

    this.logObj.endMethod();
};

/**
 * Search Asset and returns its data
 *
 * Parameters:
 *     assetId {int} - Search this asset
 *     lastAssetId {int} - find assets greater than this parameter
 * Returns:
 *     {object} - Tax Method List
**/
FAM.Disposal_SS.prototype.searchAssets = function (assetId, lastAssetId) {
    this.logObj.startMethod('FAM.Disposal_SS.searchAssets');

    var fSearch = new FAM.Search(new FAM.Asset_Record());

    if(assetId) {
        fSearch.addFilter('internalidnumber', null, 'equalto', assetId);
    }
    if(lastAssetId) {
        fSearch.addFilter('internalidnumber', null, 'greaterthan', lastAssetId);
    }

    fSearch.addFilter('isinactive', null, 'is', 'F');

    fSearch.addColumn('internalid', null, null, 'SORT_ASC');
    fSearch.addColumn('subsidiary');
    fSearch.addColumn('asset_type');
    fSearch.addColumn('initial_cost');
    fSearch.addColumn('current_cost');
    fSearch.addColumn('book_value');
    fSearch.addColumn('rv');
    fSearch.addColumn('lifetime');
    fSearch.addColumn('quantity');
    fSearch.addColumn('currency');
    fSearch.addColumn('classfld');
    fSearch.addColumn('department');
    fSearch.addColumn('location');
    fSearch.addColumn('depr_start_date');
    fSearch.addColumn('writeoff_account');
    fSearch.addColumn('disposal_account');
    fSearch.addColumn('asset_account');
    fSearch.addColumn('depr_account');
    fSearch.addColumn('name');
    fSearch.addColumn('altname');
    fSearch.addColumn('cummulative_depr');
    fSearch.addColumn('prior_nbv');
    fSearch.addColumn('lastDeprAmount');
    fSearch.addColumn('quantity_disposed');
    fSearch.addColumn('currency');
    fSearch.addColumn('disposal_item');
    fSearch.addColumn('customer');
    fSearch.addColumn('sales_amount');
    fSearch.addColumn('sales_invoice');
    fSearch.addColumn('disposal_type');
    fSearch.addColumn('disposal_date');
    fSearch.addColumn('status');
    fSearch.addColumn('depr_active');

    fSearch.run();

    this.logObj.endMethod();
    return fSearch;
};

/**
 * Search All Tax method for an Asset
 *
 * Parameters:
 *     assetId {int} - Tax Method of this Asset Id
 *     lastTaxMethId {int} - Find tax method Id's greater that this parameter
 * Returns:
 *     {object} - Tax Method List
**/
FAM.Disposal_SS.prototype.searchAssetTaxMethods = function (assetId, lastTaxMethId) {
    this.logObj.startMethod('FAM.Disposal_SS.searchAssetTaxMethods');

    var fSearch = new FAM.Search(new FAM.AltDeprMethod_Record());

    fSearch.addFilter('parent_asset', null, 'is', assetId);
    fSearch.addFilter('isinactive', null, 'is', 'F');
    if(lastTaxMethId) {
        fSearch.addFilter('internalidnumber', null, 'greaterthan', lastTaxMethId);
    }

    fSearch.addColumn('internalid', null, null, 'SORT_ASC');
    fSearch.addColumn('subsidiary');
    fSearch.addColumn('asset_type');
    fSearch.addColumn('parent_asset');
    fSearch.addColumn('alternate_method');
    fSearch.addColumn('depr_method');
    fSearch.addColumn('booking_id');
    fSearch.addColumn('original_cost');
    fSearch.addColumn('current_cost');
    fSearch.addColumn('book_value');
    fSearch.addColumn('residual_value');
    fSearch.addColumn('asset_life');
    fSearch.addColumn('depr_start_date');
    fSearch.addColumn('write_off_account');
    fSearch.addColumn('disposal_account');
    fSearch.addColumn('asset_account');
    fSearch.addColumn('depr_account');
    fSearch.addColumn('cumulative_depr');
    fSearch.addColumn('prior_year_nbv');
    fSearch.addColumn('last_depr_amount');
    fSearch.addColumn('currency');
    fSearch.addColumn('status');
    fSearch.addColumn('depr_active');

    fSearch.run();

    this.logObj.endMethod();
    return fSearch;
};

/**
 * Search Invoice Line Items for reversal and returns its data
 *
 * Parameters:
 *     txnId {int} - Search this asset
 * Returns:
 *     {object} - Tax Method List
**/
FAM.Disposal_SS.prototype.searchInvForReversalLines = function (invId) {
    this.logObj.startMethod("FAM.Disposal_SS.searchInvForReversalLines");

    var fSearch = new FAM.Search(new FAM.Transaction());
    
    fSearch.addFilter("internalid", null, "is", invId);    
    fSearch.addColumn("debitamount");
    fSearch.addColumn("creditamount");
    fSearch.addColumn("account");
    fSearch.addColumn("mainline");    
    fSearch.addColumn("entity");
    fSearch.addColumn("trandate");
    fSearch.addColumn("postingperiod");
    
    if (FAM.Context.blnOneWorld) {
    	fSearch.addColumn("subsidiary");	
    };
    
    if (FAM.Context.blnMultiCurrency) {
    	fSearch.addColumn("exchangerate");
        fSearch.addColumn("currency");
    }
    
    fSearch.run();

    this.logObj.endMethod();
    return fSearch;
};

/**
 * Acquires values needed for disposal
 *
 * Parameters:
 *     searchObj {FAM.SearchResult} - Asset or Tax Method search result
 *     recType {int} - Specifiec if Accounting or Tax Method
 * Returns:
 *     {object} - record object to be disposed
**/
FAM.Disposal_SS.prototype.getInformation = function (searchObj, recType) {
    this.logObj.startMethod('FAM.Disposal_SS.getInformation');

    var ret = { currCost     : +searchObj.getValue('current_cost'),
                currNBV      : +searchObj.getValue('book_value'),
                deprStart    : searchObj.getValue('depr_start_date'),
                subsidiary   : searchObj.getValue('subsidiary'),
                assetType    : searchObj.getValue('asset_type'),
                assetAcc     : searchObj.getValue('asset_account'),
                assetAccName : searchObj.getText('asset_account'),
                deprAcc      : searchObj.getValue('depr_account'),
                deprAccName  : searchObj.getText('depr_account'),
                dispAcc      : searchObj.getValue('disposal_account'),
                dispAccName  : searchObj.getText('disposal_account')};

    if (recType === this.recList.Accounting) {
        ret.assetId = searchObj.getValue('internalid');
        ret.origCost = +searchObj.getValue('initial_cost');
        ret.resValue = searchObj.getValue('rv');
        ret.lifetime = searchObj.getValue('lifetime');
        ret.quantity = +searchObj.getValue('quantity');
        ret.classfld = searchObj.getValue('classfld');
        ret.department = searchObj.getValue('department');
        ret.location = searchObj.getValue('location');
        ret.writeOffAcc = searchObj.getValue('writeoff_account');
        ret.writeOffAccName = searchObj.getText('writeoff_account');
        ret.assetName = searchObj.getValue('name') + ' ' + searchObj.getValue('altname');
        ret.cumDepr = +searchObj.getValue('cummulative_depr')  || 0;
        ret.priorNbv = +searchObj.getValue('prior_nbv')  || 0;
        ret.lastDeprAmount = +searchObj.getValue('lastDeprAmount') || 0;
        ret.qtyDisposed = +searchObj.getValue('quantity_disposed') || 0;
        if (FAM.Context.blnMultiBook) {
            ret.bookingId = FAM.Util_Shared.getPrimaryBookId();
        }
        ret.currId = searchObj.getValue('currency') || this.assetCache.getApplicableCurrency(ret.subsidiary, ret.bookingId) || 1;
        // set values to cache object for tax methods
        this.assetCache.values[ret.assetId + '-custrecord_assetclass'] = ret.classfld;
        this.assetCache.values[ret.assetId + '-custrecord_assetdepartment'] = ret.department;
        this.assetCache.values[ret.assetId + '-custrecord_assetlocation'] = ret.location;
        this.assetCache.values[ret.assetId + '-currid'] = ret.currId;
        this.assetCache.values[ret.assetId + '-name'] = searchObj.getValue('name');
        this.assetCache.values[ret.assetId + '-altname'] = searchObj.getValue('altname');
        // set original quantity to stateValues
        this.procInsRec.stateValues.origQty = ret.quantity;
    }
    else if (recType == this.recList.TaxMethod) {
        ret.taxMetId = searchObj.getValue('internalid');
        ret.assetId = searchObj.getValue('parent_asset');
        ret.altMethod = searchObj.getValue('alternate_method');
        ret.deprMethod = searchObj.getValue('depr_method');
        ret.deprMethodName = searchObj.getText('depr_method');
        ret.bookingId = searchObj.getValue('booking_id');
        ret.origCost = +searchObj.getValue('original_cost');
        ret.resValue = searchObj.getValue('residual_value');
        ret.lifetime = searchObj.getValue('asset_life');
        ret.writeOffAcc = searchObj.getValue('write_off_account');
        ret.writeOffAccName = searchObj.getText('write_off_account');
        ret.cumDepr = +searchObj.getValue('cumulative_depr') || 0;
        ret.priorNbv = +searchObj.getValue('prior_year_nbv') || 0;
        ret.lastDeprAmount = +searchObj.getValue('last_depr_amount') || 0;
        ret.quantity = this.procInsRec.stateValues.origQty;
        ret.classfld = this.assetCache.fieldValue(ret.assetId, 'custrecord_assetclass');
        ret.department = this.assetCache.fieldValue(ret.assetId, 'custrecord_assetdepartment');
        ret.location = this.assetCache.fieldValue(ret.assetId, 'custrecord_assetlocation');
        ret.assetName = this.assetCache.fieldValue(ret.assetId, 'name') + ' ' +
            this.assetCache.fieldValue(ret.assetId, 'altname');
        ret.currId = FAM.Context.blnMultiCurrency ? searchObj.getValue('currency') : 1;
        if (!ret.currId) {
            //Reattempt to fetch currency for pre-MB tax method records
            if (FAM.Context.blnMultiBook) {
                ret.currId = this.assetCache.getApplicableCurrency(
                        ret.subsidiary, ret.bookingId);
            } else if(FAM.Context.blnOneWorld) {
                ret.currId = this.assetCache.fieldValue(ret.assetId, 'currid');
            }
            else {
                ret.currId = 1; //For SI
            }
        }
    }
    if(FAM.Context.blnMultiCurrency) {
        ret.currSym = this.currCache.fieldValue(ret.currId, 'symbol');
    }

    this.logObj.endMethod();
    return ret;
};

/**
 * Create Invoice for Asset
 *
 * Parameters:
 *     recObj {object} - Asset or Tax Method Record Object
 * Returns:
 *     {int} - Internal Id of Invoice Record
**/
FAM.Disposal_SS.prototype.createInvoice = function (recObj) {
    this.logObj.startMethod('FAM.Disposal_SS.createInvoice');

    var ret, invoiceObj = new FAM.Invoice(),
        disposalObj = this.procInsRec.stateValues,
        initValues = { 'recordmode': 'dynamic' },
        invoiceValues = {
            'entity'        : disposalObj.cust,
            'currency'      : recObj.currId,
            'exchangerate'  : '1',
            'trandate'      : nlapiDateToString(new Date(disposalObj.date))
        },
        lineItemValues = {
            'customer'      : disposalObj.cust,
            'item'          : disposalObj.item,
            'quantity'      : disposalObj.qty,
            'amount'        : FAM.Util_Shared.Math.roundByCurrency(disposalObj.amt,recObj.currSym, this.currSymbol)
        };

    if (FAM.SystemSetup.getSetting('disposalForm')) {
        initValues.customform = FAM.SystemSetup.getSetting('disposalForm');
    }
    if (FAM.Context.blnOneWorld) {
        invoiceValues.subsidiary = recObj.subsidiary;
    }
    if (disposalObj.tax) {
        lineItemValues.taxcode = disposalObj.tax;
    }

    //Custom Form can set CDL fields as per line so assign both
    if (recObj.classfld) {
        invoiceValues['class'] = recObj.classfld;
        lineItemValues['class'] = recObj.classfld;
    }
    if (recObj.department) {
        invoiceValues.department = recObj.department;
        lineItemValues.department = recObj.department;
    }
    if (disposalObj.loc) {
        invoiceValues.location = disposalObj.loc;
        lineItemValues.location = disposalObj.loc;
    }

    invoiceObj.createRecord(invoiceValues, initValues);
    invoiceObj.addLineItem('item', lineItemValues);

    ret = invoiceObj.submitRecord();
    this.rollbackData.invoice.push(ret);

    this.logObj.endMethod();
    return ret;
};

/**
 * Writes the transaction amounts to journal entries
 *
 * Parameters:
 *     recObj {object} - record object to be disposed
 * Returns:
 *     {number} - internal id of journal entry saved
**/
FAM.Disposal_SS.prototype.writeJournal = function (recObj) {
    this.logObj.startMethod('FAM.Disposal_SS.writeJournal');

    var ret, dispAcc, dispAccName, transferObj, accounts = [], debits = [], credits = [], memoId,
        journalMemo, memoValues = [], noValues = false, inactives = [],
        journalObj = new FAM.JournalWriting(), disposalObj = this.procInsRec.stateValues;

    if (disposalObj.type == FAM.DisposalType['Sale']) {
        dispAcc = recObj.dispAcc;
        dispAccName = recObj.dispAccName;
        memoId = 'custpage_salememo';
    }
    else if (disposalObj.type == FAM.DisposalType['Write Off']) {
        dispAcc = recObj.writeOffAcc;
        dispAccName = recObj.writeOffAccName;
        memoId = 'custpage_writeoffmemo';
    }

    journalMemo = FAM.resourceManager.GetString(memoId, 'assetdisposal', null, [recObj.assetName]);
    if (+recObj.cumDeprAdj) {
        if (!recObj.deprAcc) {
            noValues = true;
        }
        else if (this.accCache.fieldValue(recObj.deprAcc, 'isinactive') === 'T') {
            inactives.push(recObj.deprAccName);
        }
        else {
            accounts.push(recObj.deprAcc);
            debits.push(+recObj.cumDeprAdj);
            credits.push(0);
            memoValues.push(journalMemo);
        }
    }
    if (+recObj.currNBVAdj) {
        if (!dispAcc) {
            noValues = true;
        }
        else if (this.accCache.fieldValue(dispAcc, 'isinactive') === 'T') {
            inactives.push(dispAccName);
        }
        else {
            accounts.push(dispAcc);
            debits.push(+recObj.currNBVAdj);
            credits.push(0);
            memoValues.push(journalMemo);
        }
    }
    if (+recObj.currCostAdj) {
        if (!recObj.assetAcc) {
            noValues = true;
        }
        else if (this.accCache.fieldValue(recObj.assetAcc, 'isinactive') === 'T') {
            inactives.push(recObj.assetAccName);
        }
        else {
            accounts.push(recObj.assetAcc);
            debits.push(0);
            credits.push(+recObj.currCostAdj);
            memoValues.push(journalMemo);
        }
    }

    if (noValues) {
        throw FAM.resourceManager.GetString('custpage_nodispacc', 'assetdisposal', null,
            [recObj.deprMethodName || recObj.assetName]);
    }
    if (inactives.length > 0) {
        throw FAM.resourceManager.GetString('custpage_inactiveacc', 'assetdisposal', null,
            [inactives.join(', ')]);
    }
    if (accounts.length === 0) {
        return null;
    }

    if (!journalObj.periodInfo) {
        journalObj.periodInfo = FAM.getAccountingPeriodInfo();

        if (!journalObj.periodInfo) {
            throw 'Unable to retrieve accounting period information';
        }
    }

    journalDate = journalObj.getOpenPeriod(new Date(disposalObj.date));
    var jeObj = {
    	"trnDate" : journalDate,
    	"currId" : recObj.currId,
    	"accts" : accounts,
    	"debitAmts" : debits,
    	"creditAmts" : credits,
    	"entities" : [], 
    	"ref" : memoValues,
    	"subId" : recObj.subsidiary,
    	"classId" : recObj.classfld,
    	"deptId" : recObj.department,
    	"locId" : disposalObj.loc,
    	"bookId" : recObj.bookingId,
    	"permit" : this.procInsRec.stateValues.JrnPermit,
    	"type" : this.journalEntryRec
    };

    ret = FAM_Util.createJournalEntry(jeObj);
    this.rollbackData.journal.push(ret);

    this.logObj.endMethod();
    return ret;
};

/**
 * Writes depreciation history record
 *
 * Parameters:
 *     recObj {object} - record object
 *     journalId {int} - journal id to be assigned to dhr
 * Returns:
 *     {int} - Created History Id
**/
FAM.Disposal_SS.prototype.writeHistory = function (recObj, journalId) {
    this.logObj.startMethod('FAM.Disposal_SS.writeHistory');
    var histRec     = new FAM.DepreciationHistory_Record(),
        disposalObj = this.procInsRec.stateValues, historyId,
        fields      = {
            asset                : recObj.assetId,
            asset_type           : recObj.assetType,
            date                 : nlapiDateToString(new Date(disposalObj.date)),
            quantity             : disposalObj.qty,

        };
    if (journalId) { fields.posting_reference = journalId; }
    if (recObj.subsidiary) { fields.subsidiary = recObj.subsidiary; }
    if (recObj.bookingId) { fields.bookId = recObj.bookingId; }
    if (recObj.taxMetId) {
        fields.alternate_depreciation     = recObj.taxMetId;
        fields.alternate_method           = recObj.altMethod;
        fields.actual_depreciation_method = recObj.deprMethod;
    }

    //Create Sale/WriteOff type
    if (disposalObj.type == FAM.DisposalType['Sale']) {
        fields.transaction_type = FAM.TransactionType['Sale'];
    }
    else if (disposalObj.type == FAM.DisposalType['Write Off']) {
        fields.transaction_type = FAM.TransactionType['Write-Off'];
    }
    fields.net_book_value = recObj.currCost;
    fields.transaction_amount = recObj.currCostAdj;

    histRec.createRecord(fields);
    historyId = histRec.submitRecord(false);
    this.logObj.pushMsg('Created Sale History: ' + historyId);
    this.rollbackData.history.push(historyId);

    //Create Depreciation Type
    fields.transaction_type = FAM.TransactionType['Depreciation'];
    fields.net_book_value = recObj.currNBV;
    fields.transaction_amount = recObj.cumDeprAdj * -1;

    histRec.createRecord(fields);
    var historyId = histRec.submitRecord(false);
    this.logObj.pushMsg('Created Depreciation History: ' + historyId);
    this.rollbackData.history.push(historyId);

    this.logObj.endMethod();
};

/**
 * Updates Process Instance Record
 *
 * Parameters:
 *     bRequeue {boolean} - If Process will be requeued
 *     status {number} - current status of the background process
 * Returns:
 *     void
**/
FAM.Disposal_SS.prototype.updateProcIns = function (bRequeue, status){
    this.logObj.startMethod('FAM.Disposal_SS.updateProcIns');

    var procFields = { rollback_data : JSON.stringify(this.rollbackData) };

    if (bRequeue) {
        procFields.status = status;
        procFields.state = JSON.stringify(this.procInsRec.stateValues); //includes origQty
    }
    else if (status === FAM.BGProcessStatus.Reverting) {
        procFields.status = FAM.BGProcessStatus.Failed;
        this.recsFailed++;
    }
    else {
        procFields.message = 'Successfully Disposed Asset';
        procFields.status = FAM.BGProcessStatus.Completed;
    }
    
    procFields.rec_total = this.totalRecords;
    procFields.rec_count = this.recsProcessed;
    procFields.rec_failed = this.recsFailed;

    this.procInsRec.submitField(procFields);
    this.logObj.endMethod();
};

/**
 * Determines if the Execution Limit or Time Limit has exceeded
 *
 * Returns:
 *     true {boolean} - Execution Limit or Time Limit has exceeded
 *     false {boolean} - Execution Limit or Time Limit has not exceeded
**/
FAM.Disposal_SS.prototype.hasExceededLimit = function () {
    this.logObj.startMethod('FAM.Disposal_SS.hasExceededLimit');

    var ret = FAM.Context.getRemainingUsage() < this.execLimit ||
        this.perfTimer.getElapsedTime() > this.timeLimit;

    this.logObj.endMethod();
    return ret;
};

/**
 * Update Records after Disposal
 *
 * Parameters:
 *     id {integer}         record id of Asset/Tax Method
 *     recType {integer}    record type indicator (Asset or Tax method)
 *     invoiceId {integer   record if of Invoice created if available
**/
FAM.Disposal_SS.prototype.updateRecord = function (id, recType, invoiceId, recObj){
    this.logObj.startMethod('FAM.Disposal_SS.updateRecord');
    var fields = {}, disposalObj = this.procInsRec.stateValues,
        isDisposed = (recObj.quantity == 0), updateRec = null;

    //update record
    if(recType === this.recList.Accounting){
        updateRec = new FAM.Asset_Record();
        fields = {
            'status'            : isDisposed ? FAM.AssetStatus['Disposed'] : FAM.AssetStatus['Part Disposed'],
            'book_value'        : recObj.currNBV,
            'disposal_type'     : disposalObj.type,
            'disposal_date'     : nlapiDateToString(new Date(disposalObj.date)),
            'quantity'          : recObj.quantity,
            'quantity_disposed' : recObj.qtyDisposed,
            'rv'                : recObj.resValue,
            'cummulative_depr'  : recObj.cumDepr,
            'initial_cost'      : recObj.origCost,
            'current_cost'      : recObj.currCost,
            'prior_nbv'         : recObj.priorNbv,
            'lastDeprAmount'    : recObj.lastDeprAmount};

        if(disposalObj.type == FAM.DisposalType['Sale']){
            fields['disposal_item'] = disposalObj.item;
            fields['customer']      = disposalObj.cust;
            fields['sales_amount']  = FAM.Util_Shared.Math.roundByCurrency(disposalObj.amt,recObj.currSym, this.currSymbol);
            fields['sales_invoice'] = invoiceId || '';
        }
    }
    else{
        updateRec = new FAM.AltDeprMethod_Record();
        fields = {
            'status'           : isDisposed ? FAM.TaxMethodStatus['Disposed'] : FAM.TaxMethodStatus['Part Disposed'],
            'book_value'       : recObj.currNBV,
            'residual_value'   : recObj.resValue,
            'cumulative_depr'  : recObj.cumDepr,
            'original_cost'    : recObj.origCost,
            'current_cost'     : recObj.currCost,
            'prior_year_nbv'   : recObj.priorNbv,
            'last_depr_amount' : recObj.lastDeprAmount};
    }

    if (recObj.quantity === 0) {
            fields['depr_active'] = FAM.DeprActive.False;
    }
    updateRec.recordId = id;
    updateRec.submitField(fields, false);

    this.logObj.endMethod();
};

/**
 * Generate rollback data for asset/tax record
 *
 * Parameters:
 *     famResult {FAM.SearchResult} - Result Set containing Accounting or Tax Method Record
 *     recType {number} - Specific if processing for Accounting or Tax Method Record
 *
 * Returns:
 *     void
**/
FAM.Disposal_SS.prototype.createRecordRollbackData = function (famResult, recType) {
    this.logObj.startMethod('FAM.Disposal_SS.createRecordRollbackData');

    var rollbackData = {
        status : famResult.getValue('status'),
        book_value : famResult.getValue('book_value'),
        depr_active : famResult.getValue('depr_active'),
        current_cost : famResult.getValue('current_cost')
    };
    
    if (recType === this.recList.Accounting) {
        rollbackData.disposal_item = famResult.getValue('disposal_item');
        rollbackData.customer = famResult.getValue('customer');
        rollbackData.sales_amount = famResult.getValue('sales_amount');
        rollbackData.sales_invoice = famResult.getValue('sales_invoice');
        rollbackData.disposal_type = famResult.getValue('disposal_type');
        rollbackData.disposal_date = famResult.getValue('disposal_date');
        rollbackData.cummulative_depr = famResult.getValue('cummulative_depr');
        rollbackData.quantity = famResult.getValue('quantity');
        rollbackData.quantity_disposed = famResult.getValue('quantity_disposed');
        rollbackData.rv = famResult.getValue('rv');
        rollbackData.initial_cost = famResult.getValue('initial_cost');
        rollbackData.prior_nbv = famResult.getValue('prior_nbv');
        rollbackData.lastDeprAmount = famResult.getValue('lastDeprAmount');

        this.rollbackData.asset[famResult.getId()] = rollbackData;
    }
    else {
        rollbackData.cumulative_depr = famResult.getValue('cumulative_depr');
        rollbackData.residual_value = famResult.getValue('residual_value');
        rollbackData.original_cost = famResult.getValue('original_cost');
        rollbackData.prior_year_nbv = famResult.getValue('prior_year_nbv');
        rollbackData.last_depr_amount = famResult.getValue('last_depr_amount');

        this.rollbackData.tax[famResult.getId()] = rollbackData;
    }

    this.logObj.endMethod();
};

/**
 * Reverts changes made by disposal
 *
 * Parameters:
 *     none
 * Returns:
 *     {boolean} - True if need to requeue else false
**/
FAM.Disposal_SS.prototype.revertChanges = function () {
    this.logObj.startMethod('FAM.Disposal_SS.revertChanges');

    var i, ret = false, assetRec = new FAM.Asset_Record(), taxRec = new FAM.AltDeprMethod_Record();

    while (!ret && (i = this.rollbackData.invoice.shift())) {
    	this.reverseInvoice(i);
    	if (this.hasExceededLimit()) {
            ret = true;
        }
    }
    
    while (!ret && (i = this.rollbackData.journal.shift())) {
        this.reverseJournal(i);
        if (this.hasExceededLimit()) {
            ret = true;
        }
    }

    while (!ret && (i = this.rollbackData.history.shift())) {
        nlapiDeleteRecord('customrecord_ncfar_deprhistory', i);
        if (this.hasExceededLimit()) {
            ret = true;
        }
    }

    if (!ret && Object.keys(this.rollbackData.asset).length > 0) {
        for (i in this.rollbackData.asset) {
            assetRec.recordId = i;
            assetRec.submitField(this.rollbackData.asset[i]);
            delete this.rollbackData.asset[i];
            if (this.hasExceededLimit()) {
                ret = true;
                break;
            }
        }
    }

    if (!ret && Object.keys(this.rollbackData.tax).length > 0) {
        for (i in this.rollbackData.tax) {
            taxRec.recordId = i;
            taxRec.submitField(this.rollbackData.tax[i]);
            delete this.rollbackData.tax[i];
            if (this.hasExceededLimit()) {
                ret = true;
                break;
            }
        }
    }

    this.logObj.endMethod();
    return ret;
};

/**
 * Reverses the given journal entry
 *
 * Parameters:
 *     id {number} - internal id of the journal record to be reversed
 * Returns:
 *     void
**/
FAM.Disposal_SS.prototype.reverseJournal = function (id) {
    this.logObj.startMethod('FAM.Disposal_SS.reverseJournal');

    var journalRec = nlapiLoadRecord(this.journalEntryRec, id);

    journalRec.setFieldValue(this.jrnReversDateId, journalRec.getFieldValue('trandate'));

    nlapiSubmitRecord(journalRec, true, true);
    this.logObj.endMethod();
};


/**
 * Reverses the given invoice
 *
 * Parameters:
 *     id {number} - internal ID of the invoice record to be reversed
 * Returns:
 *     void
**/
FAM.Disposal_SS.prototype.reverseInvoice = function (invId) {
    this.logObj.startMethod('FAM.Disposal_SS.reverseInvoice');

    var invRes = this.searchInvForReversalLines(invId);
    
	// Create Line Items to reverse
	var debitAmts 	= [];
	var creditAmts	= [];
	var accts 		= [];
	var entities 	= [];
	var memoValues  = [];
	var subId;
	var trandate;
	var postingPeriodId;
	var exchangeRate;
	var currencyId;

    for (var i = 0; i<invRes.length; i++) {
		var invResRow = invRes.getResult(i);
		
		if (invResRow.getValue("mainline") == "*") {
			entities[i] = invResRow.getValue("entity");
			subId = invResRow.getValue("subsidiary") || null;
			trandate = invResRow.getValue("trandate");
			postingPeriodId = invResRow.getValue("postingperiod");
			exchangeRate = invResRow.getValue("exchangerate") || null;
			currencyId = invResRow.getValue("currency") || null;
		}
		creditAmts[i] = invResRow.getValue("debitamount"); // Reverse debit amount
		debitAmts[i] = invResRow.getValue("creditamount"); // Reverse credit amount
		if (!debitAmts[i] && !creditAmts[i]) { // Prevent adding a blank line because of tax
			nlapiLogExecution("DEBUG", "Ignoring blank line", "Possibly because of zero tax.");
			break;
		}
		
		accts[i] = invResRow.getValue("account");
		memoValues[i] = "";
    }
    
    // Create Journal Entry for invoice reversal
    var jeValues = {
		"trnDate" : trandate,
		"postingperiod" : postingPeriodId,
		"subId" : subId,
		"currency" : currencyId,
		"exchangerate" : exchangeRate,
		"debitAmts" : debitAmts,
    	"creditAmts" : creditAmts,
    	"accts" : accts,
    	"entities" : entities,
    	"ref" : memoValues,
    	"type" : this.journalEntryRec,
    	"permit" : FAM.Permissions.Full,
    	"alwaysApproved" : true // Always true when reversing invoices
    };

    var jeId = FAM_Util.createJournalEntry(jeValues);
    
    // Transform invoice to payment to reverse it
    var customerPayment = nlapiTransformRecord("invoice", invId, "customerpayment");
    var jeCreditsListLength = customerPayment.getLineItemCount("credit");
    for (var i = 1; i<=jeCreditsListLength; i++) {

    	if (customerPayment.getLineItemValue("credit", "internalid", i) == jeId) {
    		customerPayment.setLineItemValue("credit", "apply", i, "T");
    	}
    }

    nlapiSubmitRecord(customerPayment);
    
    this.logObj.endMethod();
};