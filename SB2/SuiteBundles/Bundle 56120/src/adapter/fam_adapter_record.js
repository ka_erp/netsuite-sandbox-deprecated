/**
 * © 2016 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

define(["N/record"],
    famAdapterRecord);

function famAdapterRecord(record) {
    var module = {
        Type   : record.Type,
        create : function(options) {
            return record.create(options);
        },
        load : function(options) {
            return record.load(options);
        },
        submitFields : function(options) {
            return record.submitFields(options);
        }
    };
    module['delete'] = function(options) {
        return record.delete(options);
    };
    return module;
}