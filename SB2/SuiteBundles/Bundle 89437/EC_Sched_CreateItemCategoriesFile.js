//noinspection JSUnusedGlobalSymbols
/**
 * Company           Explore Consulting
 * Copyright         2015 Explore Consulting, LLC
 * Description       Creates a JSON file of item categories used by the ecqs SCA QuickStart Categories Manager. This
 * is an optimization for speed and governance usage. Governance is not checked because we don't expect to ever
 * come close to the 10,000 units (this script is totalusage = 2N where N = number of categories)
 **/


/**
 * Converts string field names to columns
 * @param {Array.<string>} fieldNames
 * @returns {Array|*}
 */
function fieldNamesToColumns(fieldNames) {
    return _.map(fieldNames, function (f) {
        return new nlobjSearchColumn(f);
    });
}

/**
 * Evil hack used to reconstitute native netsuite objects such as nlobjRecord or nlobjSearchResult with many of their
 * fields
 * @param {Object | Array<>} input
 */
function stringifyParseHack(input) {
    return JSON.parse(JSON.stringify(input));
}


/**
 * Gets categories from custom record as a big JSON array
 * @returns {Array|*}
 */
EC.getResults = function () {

	var result = [];
	var omitList = [
		'customform',
		'isinactive',
		'lastmodified',
		'lastmodifiedby',
		'recordid',
		'recordtype'
	];

    var inactiveFilter = new nlobjSearchFilter('isinactive', null, 'is', 'F', null);
	var sampleRecord = nlapiSearchRecord('customrecord_ecqs_category', null, [inactiveFilter]);
    Log.d("sampleRecord", sampleRecord);
    
    if (!sampleRecord) {
		return result;
	}
    
    sampleRecord = nlapiLoadRecord('customrecord_ecqs_category', sampleRecord[0].getId());

    // HACK: JSON stringify on a nlobjRecord results in most (but not all) fields being rendered. So this is kludge
    // to render most of the fields from a loadRecord() call into a native javascript object.
    var prototypeSample = stringifyParseHack(sampleRecord);

    // all the field names on the record except ones NS will abort if we try to create columns from them
    var desiredFieldNames = _(prototypeSample)
        // explicitly omit NS properties that can't be nlobjColumn
        .omit([
            'recmachcustrecord_ecqs_category_parent', // note this means we can't pull
            'recmachcustrecord_ecqs_catitem_cat',
            'recmachcustrecord_ecqs_subcat_parent'
        ])
        .omit(omitList)
        .keys()
        .value();

    Log.d("desiredField names", desiredFieldNames);

    // this retrieves body fields but not the child items
    var categoryRecordsRaw = nlapiSearchRecord(
    	'customrecord_ecqs_category',
        null, 
        [inactiveFilter], 
        fieldNamesToColumns(desiredFieldNames));

    var categoryRecords = stringifyParseHack(categoryRecordsRaw);

    Log.d("initial categoryRecords after stringifyParseHack", categoryRecords);

    // now blend in the child items by loading it and applying the stringify hack
    result = _.map(categoryRecords, function (r) {
        var category = nlapiLoadRecord('customrecord_ecqs_category', r.id);
        
        // The extend order here is important because images in the columns object have URLs rather than
        // file names that are returned at the top level and we need the URLs.
        return _.extend({}, _.omit(stringifyParseHack(category), omitList), r.columns);
    });

    Log.d('after merging', result);
    return result;
};

EC.getFolderId = function (folderName) {
    var folder = nlapiSearchRecord('folder',null,[['name', 'is', folderName]]);
    if (!folder) {
        throw nlapiCreateError("INVALID FOLDER NAME", "Folder name:" + folderName + " not found");
    }
    else return folder[0].getId();
};


/**
 * Main entrypoint of script
 */
EC.onStart = function () {
    // this is the name of the file in the file cabinet... tried to choose something that will be unique
    var FILENAME = 'EC_ECQS_CategoryCache.json';
    var FOLDERNAME = 'Live Hosting Files';
    // the name of the folder needs to be constant, but the id can change - if bundles now have static ids perhaps
    // we can forgo this step
    var FOLDERID = EC.getFolderId(FOLDERNAME);

    var fileContent = EC.getResults();
    var file        = nlapiCreateFile(FILENAME, 'JSON', JSON.stringify(fileContent));
    file.setFolder(FOLDERID);
    var id          = nlapiSubmitFile(file);
    Log.d("created/updated file", "internal id " + id + " in folder id " + FOLDERID);
};

Log.includeCorrelationId = true;
Log.AutoLogMethodEntryExit({withProfiling: true});
Log.AutoLogGovernanceUsage();