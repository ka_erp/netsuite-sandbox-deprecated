/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       23 Dec 2014     joel
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	nlapiLogExecution('debug', 'userEventBeforeLoad', 'type = ' + type);
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	nlapiLogExecution('debug', 'userEventBeforeSubmit', 'type = ' + type);
	
	try {
		if (type == 'create') {
			var trigger = nlapiGetNewRecord();
			var email = trigger.getFieldValue('custrecord_email');
			nlapiLogExecution('debug', 'userEventBeforeSubmit', 'email = ' + email);
			
			// find corresponding customer
			var filters = [];
			filters.push(new nlobjSearchFilter('email', null, 'is', email));
			var columns = [];
			columns.push(new nlobjSearchColumn('email'));
			var searchResults = nlapiSearchRecord('customer', null, filters, columns) || [];

			if (searchResults.length > 0) {
				var custId = searchResults[0].getId();
				nlapiLogExecution('debug', 'userEventBeforeSubmit', 'custId = ' + custId);
				var cust = nlapiLoadRecord('customer', custId);
				nlapiLogExecution('debug', 'userEventBeforeSubmit', 'cust = ' + cust);
				cust.setFieldValue('custentity_acct_status', 3);
				nlapiSubmitRecord(cust);
			}
		}
	}
	catch (e) {
		nlapiLogExecution('error', 'exception', e);
	}
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	nlapiLogExecution('debug', 'userEventAfterSubmit', 'type = ' + type);
	
	//FIX: delete newly created record
	try {
		if (type == 'create') {
			var triggerId = nlapiGetRecordId();
			nlapiLogExecution('debug', 'userEventAfterSubmit', 'triggerId = ' + triggerId);
			nlapiDeleteRecord('customrecord_set_account_status', triggerId);
		}
	}
	catch (e) {
		nlapiLogExecution('error', 'exception', e);
	}
}
