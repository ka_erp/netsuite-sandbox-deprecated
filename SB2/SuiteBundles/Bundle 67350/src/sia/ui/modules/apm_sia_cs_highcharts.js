/**
 * © 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       23 Feb 2015     jyeh             Initial
 * 2.00       04 Sep 2015     rwong            Disabled focus on selection in grid
 *
 */

PSGP.APM.SIA.Highcharts = {

    timelineChart : null,

    renderTimeline : function (chartData) {
        if ((!chartData)||(chartData.length == 0)) {
            if (this.timelineChart) {
                this.timelineChart.destroy();
                this.timelineChart = null;
            }
            return;
        }

        var containerId = 'psgp-apm-sia-timeline-chart';

        var chartConfig = {
            chart: {
                renderTo: containerId,
                animation: false,
                type: 'columnrange',
                inverted: true,
                backgroundColor: 'rgba(255, 255, 255, 0.1)'
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'Categories (s)',
                id: 'Categories',
                categories: chartData.categories,
                crosshair: true,
                labels: {
                    formatter: function () {
                        var text = this.value,
                        formatted = text.length > 25 ? text.substring(0, 25) + '<b> ...  <b>' : text;
                        return '<div class="standard" style="width:150px; overflow:hidden" title="' + text + '">' + formatted + '</div>';
                    },
                    useHTML: true
                }
            },
            yAxis: {
                title: 'Time (s)',
                min: 0,
                max: chartData.totaltime,
                plotBands: [
                { //Visualizing initial request
                    from: 0 ,
                    to: chartData.redirectstart > 0 ? chartData.redirectstart : chartData.totaltime,
                    label: {
                        text: chartData.redirectstart > 0 ? '- Post -' : null,
                        style: {
                            color : '#666',
                            fontFamily : 'Arial',
                            fontSize : '13px',
                            fontWeight: 'bold'
                        }

                    }
                },
                { //Visualizing redirect request
                    from: chartData.redirectstart > 0 ? chartData.redirectstart : 0,
                    to: chartData.redirectstart > 0 ? chartData.totaltime : 0 ,
                    color: 'rgba(68, 170, 213, .1)',
                    label: {
                        text: chartData.redirectstart > 0 ? '- Get -' : null
                    }
                }],
                plotLines: [
                    {
                        id: 'start',
                        label: {
                            text: 'Request Start',
                            align: 'left',
                            style: {
                                color : '#666',
                                fontFamily : 'Arial',
                                fontSize : '11px',
                                fontWeight: 'normal'
                            }
                        },
                        color: 'black',
                        dashStyle: 'solid',
                        value: 0,
                        width: 0
                    },
                    {
                    id: 'redirectstart',
                    color: 'red',
                    dashStyle: 'solid',
                    value: chartData.redirectstart > 0 ? chartData.redirectstart : -200,
                    width: 3
                    }
                ]
            },
            exporting: {
                enabled: false,
                buttons: {
                    exportButton: {
                        enabled: false
                    },
                    printButton: {
                        enabled: false
                    }
                }
            },
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            },
            tooltip: {
                formatter: function () {
                    return this.x+ ' : ' + Number(this.point.high - this.point.low).toFixed(3) + '(s)';
                }
            },
            series: [
                {
                    name : 'Time (s)',
                    id: 'Time',
                    color: 'rgba(96, 119, 153, 0.8)',
                    pointPadding: 0.1,
                    groupPadding: 0,
                    pointWidth: 20,
                    point: {
                        events: {
                            mouseOver: function () {
                                var grid = Ext4.getCmp('psgp-apm-sia-grid-suitescriptdetail');
                                var targetRowIndex = grid.store.find('id', this.id);
                                grid.getSelectionModel().select(targetRowIndex, false, true);
                            },
                            mouseOut: function() {
                                var grid = Ext4.getCmp('psgp-apm-sia-grid-suitescriptdetail');
                                var targetRowIndex = grid.store.find('id', this.id);
                                grid.getSelectionModel().deselect(targetRowIndex, false, true);
                            }
                        }
                    },
                    //borderRadius: 10,
                    height: 0.75,
                    data: chartData.series
                }
            ],
            plotOptions: {
                columnrange: {
                    allowPointSelect: false,
                    animation: false,
                    borderWidth: 0,
                    slicedOffset: 20,
                    showInLegend: false,
                    dataLabels: {
                        enabled: false,
                        allowOverlap: false,
                        formatter: function () {
                            return this.y + 's';
                        },
                        style: {
                            color : '#666',
                            fontFamily : 'Arial',
                            fontSize : '11px',
                            fontWeight: 'normal'
                        },
                        overflow: 'justify',
                        crop: false
                    }
                }
            }
        };

        if (this.timelineChart) {
            this.timelineChart.destroy();
        }
        this.timelineChart = new Highcharts.Chart(chartConfig);
        this.resizeChart(this.timelineChart, containerId);
    },

    resizeAllCharts : function () {
        this.resizeChart(this.timelineChart, 'psgp-apm-sia-timeline-chart');
    },

    resizeChart : function (chart, containerId) {
        if (chart) {
            chart.setSize(Ext4.getCmp(containerId).getWidth() - 20, Ext4.getCmp(containerId).getHeight());
        }
    }
};