/**
 * © 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       29 Oct 2014     jmarimla         Initial
 * 2.00       04 Nov 2014     jmarimla         Added filter comboboxes
 * 3.00       07 Nov 2014     jmarimla         Added SuiteScript details grid
 *                                             Added search button functionality
 * 4.00       11 Nov 2014     jmarimla         Commented out references to deployment name combobox
 * 5.00       20 Nov 2014     rwong            Added SSD grid and window component definition
 * 6.00       21 Nov 2014     rwong            Updated suitescript detail grid columns and increase size of ssd window.
 * 7.00       28 Nov 2014     rwong            Added support for parameter passing.
 * 8.00       02 Dec 2014     jmarimla         Added performance chart
 * 9.00       29 Jan 2015     rwong            Updated call to perfchart
 * 10.00      02 Feb 2015     jmarimla         Removed instruction count from SSD window
 * ********************************************************************************
 * 1.00       20 Feb 2015     rwong            Ported SPM to APM
 * 2.00       07 Apr 2015     rwong            Added urlrequests, searches and records column
 * 3.00       19 May 2015     jmarimla         Removed unused code
 * 4.00       01 Jul 2015     jmarimla         Updated loading masks
 * 5.00       09 Jul 2015     jmarimla         Retrieve script name
 * 6.00       13 Aug 2015     jmarimla         Passed date parameters as string
 * 7.00       25 Aug 2015     jmarimla         Compid dropdown components; support for compid mode
 * 8.00       12 Oct 2015     jmarimla         Date range validation
 * 9.00       01 Dec 2015     jmarimla         Added csv export button
 * 10.00      21 Dec 2015     rwong            Adjust the spacing of the export button
 *
 */

Ext4.define('PSGP.APM.SSA.Component.BlueButton.Search', {
    extend: 'PSGP.APM.Component.BlueButton',
    text: 'Search',
    handler: function() {
        //check fields validity
        if (!Ext4.getCmp('psgp-apm-ssa-filters-date-startdate').isValid()) {
            alert('Please enter a valid start date.');
            return false;
        }
        if (!Ext4.getCmp('psgp-apm-ssa-filters-date-enddate').isValid()) {
            alert('Please enter a valid end date.');
            return false;
        }
        if (Ext4.getCmp('psgp-apm-ssa-filters-scripttype').getRawValue() == '') {
            alert('Please select a script type.');
            return false;
        }

        if ((COMPID_MODE == 'T') && (COMP_FIL)) {
            if (!Ext4.getCmp('psgp-apm-ssa-filters-scriptid').getValue()) {
                alert('Please enter a script id.');
                return false;
            }
        } else {
            if (Ext4.getCmp('psgp-apm-ssa-filters-scriptname').getRawValue() == '') {
                alert('Please select a script name.');
                return false;
            }
        }
//        if (Ext4.getCmp('psgp-apm-ssa-filters-deploymentname').getRawValue() == '') {
//            alert('Please select a deployment name.');
//            return false;
//        }

        var startdate = Ext4.getCmp('psgp-apm-ssa-filters-date-startdate').getValue();
        var starttime = Ext4.getCmp('psgp-apm-ssa-filters-time-starttime').getValue();
        var enddate = Ext4.getCmp('psgp-apm-ssa-filters-date-enddate').getValue();
        var endtime = Ext4.getCmp('psgp-apm-ssa-filters-time-endtime').getValue();
        var startDateObj = new Date(startdate.getFullYear(), startdate.getMonth(), startdate.getDate(), starttime.getHours(), starttime.getMinutes(), 0, 0);
        var endDateObj = new Date(enddate.getFullYear(), enddate.getMonth(), enddate.getDate(), endtime.getHours(), endtime.getMinutes(), 0, 0);

        if(startDateObj > endDateObj) {
            alert('Start date must be earlier than end date.');
            return false;
        }
        if(endDateObj.getTime() - startDateObj.getTime() > 1000*60*60*24*30) {
            alert('Date range should not exceed 30 days');
            return false;
        }

        var scriptType = Ext4.getCmp('psgp-apm-ssa-filters-scripttype').getValue();
        var scriptId;
        if ((COMPID_MODE == 'T') && (COMP_FIL)) {
            scriptId = Ext4.getCmp('psgp-apm-ssa-filters-scriptid').getValue();
        } else {
            scriptId = Ext4.getCmp('psgp-apm-ssa-filters-scriptname').getValue();
        }
        var scriptRec = PSGP.APM.SSA.dataStores.scriptNameComboBox.getById(scriptId);
        var scriptName = (scriptRec) ? scriptRec.get('name') : scriptId;
//      var deploymentId = Ext4.getCmp('psgp-apm-ssa-filters-deploymentname').getValue();

        PSGP.APM.SSA.dataStores.suiteScriptParams = {
                    startDate : Ext4.Date.format(startDateObj, 'Y-m-d') + 'T' + Ext4.Date.format(startDateObj, 'H:i:s')
                  , endDate : Ext4.Date.format(endDateObj, 'Y-m-d') + 'T' + Ext4.Date.format(endDateObj, 'H:i:s')
                  , scriptType : scriptType
                  , scriptId : scriptId
                  , scriptName : scriptName
                  , drilldown : 'F'
//                , deploymentId : deploymentId
        };
        PSGP.APM.SSA.dataStores.callSuiteScriptSummaryRESTlet();
        PSGP.APM.SSA.dataStores.callPerfChartRESTlet();

    }
});

Ext4.define('PSGP.APM.SSA.Component.ComboBox.ScriptType', {
    extend: 'PSGP.APM.Component.ComboBox',
    store: PSGP.APM.SSA.dataStores.scriptTypeComboBox,
    width: 208,
    allowBlank: true,
    listeners: {
        select: function (combo, records, eOpts) {
            var scriptType = combo.getValue();
            Ext4.getCmp('psgp-apm-ssa-filters-scriptname').setValue(0);
//            Ext4.getCmp('psgp-apm-ssa-filters-deploymentname').setValue(0);
            PSGP.APM.SSA.dataStores.scriptNameComboBox.load({
               params: {
                   scriptType: scriptType
               }
            });
//            PSGP.APM.SSA.dataStores.deploymentNameComboBox.loadData([], false);
        },
        change: function (combo, records, eOpts) {
            var scriptType = combo.getValue();
            Ext4.getCmp('psgp-apm-ssa-filters-scriptname').setValue(0);

            PSGP.APM.SSA.dataStores.scriptNameComboBox.load({
                params: {
                    scriptType: scriptType
                }
            });
        }
    }
});

Ext4.define('PSGP.APM.SSA.Component.ComboBox.ScriptName', {
    extend: 'PSGP.APM.Component.ComboBox',
    store: PSGP.APM.SSA.dataStores.scriptNameComboBox,
    width: 258,
    allowBlank: true,
    queryMode: 'local',
    matchFieldWidth: false,
    pickerAlign: 'tr-br'
});

Ext4.define('PSGP.APM.SSA.Component.Grid.SuitescriptDetails', {
    extend: 'PSGP.APM.Component.Grid',
    store: PSGP.APM.SSA.dataStores.suiteScriptSummaryData,
    disableSelection: true,
    listConfig: {
        loadMask: false
    },
    columns: {
        defaults : {
            hideable : false,
            draggable : false,
            menuDisabled : true,
            height: 28,
            flex: 1
        },
        items : [
                 {
                     text : 'Name',
                     dataIndex : 'name'
                 },
                 {
                     text : 'Value',
                     dataIndex : 'value'
                 }
        ]
    }
});

Ext4.define('PSGP.APM.SSA.Component.Grid.SSD', {
    extend: 'PSGP.APM.Component.Grid',
    store: PSGP.APM.SSA.dataStores.suiteScriptDetailData,
    viewConfig: {
        emptyText: 'No records to show.',
        loadMask: MASK_CONFIG
    },
    disableSelection: true,
    height: 550,
    width: 750,
    dockedItems: Ext4.create('PSGP.APM.Component.GridToolbar', {
        height: 36,
        items: [
            Ext4.create('PSGP.APM.Component.ExportCSVButton', {
                id: 'psgp-apm-ssa-btn-exportcsv-ssd',
                margin: '0 0 0 10',
                handler: function () {
                    var dataParams = PSGP.APM.SSA.dataStores.suiteScriptParams;
                    var requestParams = {};
                    if (dataParams.drilldown == 'T') {
                        requestParams = {
                                startDate : dataParams.drilldownStartDate
                              , endDate : dataParams.drilldownEndDate
                              , scriptType : dataParams.scriptType
                              , scriptId : dataParams.scriptId
                              , compfil : COMP_FIL
                        };
                    } else {
                        requestParams = {
                                startDate : dataParams.startDate
                              , endDate : dataParams.endDate
                              , scriptType : dataParams.scriptType
                              , scriptId : dataParams.scriptId
                              , compfil : COMP_FIL
                        };
                    }
                    var urlRequest = '/app/site/hosting/scriptlet.nl?script=customscript_apm_ssa_sl_ss_detail&deploy=customdeploy_apm_ssa_sl_ss_detail&testmode='+TEST_MODE
                                    + '&getcsv=T' + '&' + Ext4.urlEncode(requestParams);
                    //window.open(urlRequest);
                    window.location.href = urlRequest;
                }
            }),
            '->',
            Ext4.create('PSGP.APM.Component.ComboBox.PagingDropDown', {
                id: 'psgp-apm-ssa-combobox-suitescriptdetailpaging',
                store: PSGP.APM.SSA.dataStores.suiteScriptDetailPaging,
                hidden: true,
                listeners : {
                    select: function (combo, records, eOpts) {
                        var selectedPage = combo.getValue();
                        PSGP.APM.SSA.dataStores.suiteScriptDetailData.loadPage(selectedPage);
                    },
                    afterrender: function (combo) {
                        combo.setValue(1);
                        combo.el.on('mouseover', function () {
                            combo.expand();
                        }, combo);
                    },
                    expand: function (combo) {
                        combo.getPicker().el.monitorMouseLeave(500, combo.collapse, combo);
                    }
                }
            }),
            Ext4.create('PSGP.APM.Component.PagingToolbar', {
                id: 'psgp-apm-ssa-pagingtb-suitescriptdetail',
                store: PSGP.APM.SSA.dataStores.suiteScriptDetailData,
                hidden: true
            }),
            Ext4.create('PSGP.APM.Component.TotalPagesField', {
                id: 'psgp-apm-ssa-totalpages-suitescriptdetail'
            })
        ]
    }),

    columns: {
        defaults: {
            hideable: false,
            draggable: false,
            menuDisabled: true,
            height: 28,
            flex: 1
        },
        items: [
                {
                    text: 'Date & Time',
                    dataIndex: 'date'
                },
                {
                    text: 'Name',
                    dataIndex: 'entityname',
                    sortable: false
                },
                {
                    text: 'Email',
                    dataIndex: 'email',
                    //sortable : false
                },
                {
                    text: 'Role',
                    dataIndex: 'role',
                    //sortable : false
                },
                {
                    text: 'Record Id',
                    dataIndex: 'recordid'
                },
                {
                    text: 'Total Time',
                    dataIndex: 'totaltime'
                },
                {
                    text: 'Usage Count',
                    dataIndex: 'usagecount'
                },
                {
                    text: 'Url Requests',
                    dataIndex: 'urlrequests'
                },
                {
                    text: 'Search Calls',
                    dataIndex: 'searches'
                },
                {
                    text: 'Record Operations',
                    dataIndex: 'records'
                }
                ]
    }
});

Ext4.define('PSGP.APM.SSA.Component.Window.SSD', {
    extend: 'PSGP.APM.Component.Window',
    id: 'psgp-apm-ssa-window-ssd',
    title: 'SUITESCRIPT DETAILS',
    width: 1024,
    bodyPadding: 15,
    layout: 'fit',
    hidden: true,
    items: [
            Ext4.create('PSGP.APM.SSA.Component.Grid.SSD', {

            })
            ]
});

Ext4.define('Ext4.chart.theme.PerfChartTheme', {
    extend : 'Ext4.chart.theme.Base',
    constructor : function(config) {
        this.callParent([Ext.apply({
            colors : ['#83D97A', '#7AB0D9', '#F3EB5E', '#FAB65D', '#D95E5E'],
            axis: {
                fill: '#666666',
                'stroke-width': 1
            },
            axisLabelLeft: {
                font: '11px Arial',
                fill: '#666'
            },
            axisLabelRight: {
                font: '11px Arial',
                fill: '#666'
            },
            axisLabelBottom: {
                font: '11px Arial',
                fill: '#666'
            },
            axisTitleLeft: {
                font: 'bold 18px Arial',
                fill: '#666666'
            },
            axisTitleRight: {
                font: 'bold 18px Arial',
                fill: '#666666'
            },
            axisTitleBottom: {
                font: 'bold 18px Arial',
                fill: '#666666'
            },
        }, config)]);
    }
});

Ext4.define('PSGP.APM.SSA.Component.PerfChart', {
   extend: 'Ext4.chart.Chart',
   store: PSGP.APM.SSA.dataStores.perfChartData,
   theme: 'PerfChartTheme',
   shadow: false,
   legend: {
       position: 'bottom',
       labelFont: '14px Helvetica, sans-serif',
       itemSpacing: 20,
       boxStroke: '#E6E6E6'
   },
   axes: [
       {
           type: 'Numeric',
           position: 'right',
           fields: ['usagecountAve'],
           title: 'Usage Count',
           majorTickSteps: 5,
           grid: false,
           adjustMaximumByMajorUnit: true,
           minimum: 0
       },
       {
           type: 'Numeric',
           position: 'left',
           fields: ['totaltimeAve'],
           title: 'Total Time',
           majorTickSteps: 5,
           grid: true,
           adjustMaximumByMajorUnit: true,
           minimum: 0
       },
       {
           type: 'Time',
           position: 'bottom',
           fields: ['dateObj'],
           title: 'Timeline',
           dateFormat: 'Y-m-d H:i:s',
           step: [Ext4.Date.HOUR, 1/2],
           label : {
               rotate:{degrees:-270}
           }
       }
   ],
   series: [
       {
           type: 'line',
           axis: 'right',
           xField: 'dateObj',
           yField: 'usagecountAve',
           title: 'Usage Count',
           markerConfig: {
               type: 'cross',
               radius: 5,
           },
           style: {
               'stroke-width': 3
           }
       },
       {
           type: 'line',
           axis: 'left',
           xField: 'dateObj',
           yField: 'totaltimeAve',
           title: 'Total Time',
           markerConfig: {
               type: 'circle',
               radius: 5,
           },
           style: {
               'stroke-width': 3
           }
       },
       {
           type: 'line',
           axis: 'left',
           xField: 'dateObj',
           yField: 'totaltimeAveAll',
           title: 'Mean',
           showMarkers: false,
           style: {
               'stroke-width': 3,
               'stroke-dasharray': 6
           }
       },
       {
           type: 'line',
           axis: 'left',
           xField: 'dateObj',
           yField: 'totaltimeMedAll',
           title: 'Median',
           showMarkers: false,
           style: {
               'stroke-width': 3,
               'stroke-dasharray': 6
           }
       },
       {
           type: 'line',
           axis: 'left',
           xField: 'dateObj',
           yField: 'totaltime90PAll',
           title: '90th Percentile',
           showMarkers: false,
           style: {
               'stroke-width': 3,
               'stroke-dasharray': 6
           }
       }
   ]
});

Ext4.define('PSGP.APM.SSA.Component.BlueButton.CompIdQuickSelector.Done', {
    extend: 'PSGP.APM.Component.BlueButton',
    text: 'Done',
    handler: function() {
        var newCompFil = Ext4.getCmp('psgp-apm-ssa-quickselector-field-compid').getValue();
        COMP_FIL = newCompFil;
        Ext4.getCmp('psgp-apm-ssa-quicksel-compid').hide();

        //UI changes
        if (COMP_FIL) {
            Ext4.getCmp('psgp-apm-ssa-container-filters-scriptname').hide();
            Ext4.getCmp('psgp-apm-ssa-container-filters-scriptid').show();
            PSGP.APM.SSA.dataStores.suiteScriptSummaryData.filterBy(function (record, id) {
                if (id != 'errorCount') return true;
                else return false;
            }, this);
        } else {
            Ext4.getCmp('psgp-apm-ssa-container-filters-scriptname').show();
            Ext4.getCmp('psgp-apm-ssa-container-filters-scriptid').hide();
            PSGP.APM.SSA.dataStores.suiteScriptSummaryData.clearFilter();
        }
    }
});

Ext4.define('PSGP.APM.SSA.Component.GrayButton.CompIdQuickSelector.Cancel', {
    extend: 'PSGP.APM.Component.GrayButton',
    text: 'Cancel',
    handler: function() {
        Ext4.getCmp('psgp-apm-ssa-quicksel-compid').hide();
    }
});

Ext4.define('PSGP.APM.SSA.Component.CompIdQuickSelector', {
    extend: 'PSGP.APM.Component.QuickSelectorMenu',
    id: 'psgp-apm-ssa-quicksel-compid',
    hidden: true,
    listeners: {
        beforerender: function () {
            Ext4.getCmp('psgp-apm-ssa-quickselector-field-compid').setValue(COMP_FIL);
        },
        hide: function () {
            Ext4.getCmp('psgp-apm-ssa-quickselector-field-compid').setValue(COMP_FIL);
        }
    },
    items: [
        Ext4.create('PSGP.APM.Component.Display', {
            fieldLabel: 'Company ID',
            margin: '20 20 0 20'
        }),
        Ext4.create('PSGP.APM.Component.TextField', {
            id: 'psgp-apm-ssa-quickselector-field-compid',
            margin: '0 20 10 20'
        }),
        Ext4.create('PSGP.APM.SSA.Component.BlueButton.CompIdQuickSelector.Done', {
            id: 'psgp-apm-ssa-quickselector-btn-done',
            margin: '10 10 20 20'
        }),
        Ext4.create('PSGP.APM.SSA.Component.GrayButton.CompIdQuickSelector.Cancel', {
            id: 'psgp-apm-ssa-quickselector-btn-cancel',
            margin: '10 20 20 10'
        })
    ]
});
