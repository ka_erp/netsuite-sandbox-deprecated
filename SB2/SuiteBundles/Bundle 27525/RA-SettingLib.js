/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       26 V 2015     mkaluza
 *
 */

var fieldNames = new Array();
var fieldsConfiguration = {};
var fieldName = 'name';
var fieldValue = 'custrecord_ra_setting_value';
var fieldCheckbox = 'custrecord_ra_setting_checkbox';
var fieldInt = 'custrecord_ra_setting_int';
var fieldDecimal = 'custrecord_ra_setting_decimal';
var fieldDate = 'custrecord_ra_setting_date';

var fieldType = 'custrecord_ra_setting_type';
var fieldValueType = 'custrecord_ra_setting_value_type';
var settingValueType = 'settingValueType';

var fieldValueDefinition = {
	String: 'string1',
	Int: 'int1',
	Date: ['date', 'Date (format: mm/dd/yyyy hh:mm:ss)'],
	Decimal: 'decimal1',
	Checkbox: 'checkbox2'
};

var fieldsDefinition = [
	{'type': 'NS Replication settings',
		'fields': [
		   ['name', 'Name', 40], 
		   [settingValueType, 'Value', 4000], 
		   ['string2', 'Description'], 
		   ['checkbox1', 'Encrypted'], 
		   ['valuetype', 'Value type']
		]
	},
	{'type': 'RARS Replication XML Settings',
		'fields': [
		   ['name', 'Setting name', 128], 
		   [settingValueType, 'Setting value', 500], 
		   ['valuetype', 'Value type']
		]
	},
	{'type': 'NetSuite Inbound Trigger',
		'fields': [
		   ['name', 'Record type', 50], 
		   ['checkbox2', 'Is custom record'],
		   ['int1', 'Custom record type id', 10], 
		   ['string1', 'RAFS XSLT path'], 
		   ['string2', 'RAFS XSLT extra info'],
		   ['int2', 'Initialize order', 10],
		   ['string3', 'Search type', 200],
		   ['checkbox1', 'Search body only'],
		   ['string4', 'Action procedure', 50]
		]
	},
	{'type': 'NetSuite Outbound Trigger',
		'fields': [
		   ['name', 'Trigger name', 50], 
		   ['string1', 'Source', 128],
		   ['string2', 'xmlquery'], 
		   ['checkbox1', 'usexmlinfo'], 
		   ['string3', 'RAFS XSLT path'],
		   ['int1', 'RESTlet id', 10],
		   ['int2', 'Process order', 10]
		]
	},
	{'type': 'NetSuite Integration Configuration',
		'fields': [
		   ['name', 'Id'], 
		   ['string1', 'Passport email', 50],
		   ['string2', 'Passport account', 50], 
		   ['string3', 'SOAP endpoint', 100], 
		   ['string4', 'Gift cert url', 500]
		]
	},
	{'type': 'NetSuite Credit Card Mapping',
		'fields': [
		   ['name', 'Name', 6], 
		   ['int1', 'NetSuite id', 10],
		   ['int2', 'Location id', 10] 
		]
	},
];


function InitFields()
{
	fieldNames['name'] 		= fieldName;
	fieldNames['date'] 		= fieldDate;
	fieldNames['decimal1'] 	= fieldDecimal + '1';
	fieldNames['string1'] 	= fieldValue + '1';
	fieldNames['string2'] 	= fieldValue + '2';
	fieldNames['string3'] 	= fieldValue + '3';
	fieldNames['string4'] 	= fieldValue + '4';
	fieldNames['string5'] 	= fieldValue + '5';
	fieldNames['checkbox1'] = fieldCheckbox + '1';
	fieldNames['checkbox2'] = fieldCheckbox + '2';
	fieldNames['int1'] 		= fieldInt + '1';
	fieldNames['int2'] 		= fieldInt + '2';
	fieldNames['valuetype'] = fieldValueType;
	
	for(var i = 0; i < fieldsDefinition.length; i++) {
		fieldsConfiguration[fieldsDefinition[i].type] = fieldsDefinition[i];
	}
}


function ShowFields(fieldsToShow, clientScript)
{
	var fieldsId = [];
	for(var i in fieldsToShow) { 
		if (fieldsToShow[i] != null && fieldsToShow[i][0] != null) {
			fieldsId.push(fieldsToShow[i][0]);
		}
	}
	
	for (var fieldName in fieldNames) {
		// Field found to be shown
		var index = fieldsId.indexOf(fieldName);
		if (clientScript == true) {
			var jfield = jQuery('#' + fieldNames[fieldName] + '_fs').parent().parent();
			
			if (jfield != null && index >= 0) {
				jfield.show();	
			} 
			if (jfield != null && index < 0) {
				jfield.hide();
			}
			
			if (fieldsToShow[index] != null) {
				jQuery('#' + fieldNames[fieldName] + '_fs_lbl > a').text(fieldsToShow[index][1]);
			}
		} else {
			var field = nlapiGetField(fieldNames[fieldName]);
			if (field != null) {
				field.setDisplayType(index >= 0 ? 'normal' : 'hidden');
				if (fieldsToShow[index] != null && fieldsToShow[index][1] != null) {
					field.setLabel(fieldsToShow[index][1]);
				}
			}
		}
	}
}

function ValidateFields(settingType, settingValueType)
{
	var conf = fieldsConfiguration[settingType];
	if (conf != null)
	{
		var valueType = fieldValueDefinition[settingValueType] || 'string1';
		var fields = UpdateValueField(conf.fields, valueType);
		for (var i = 0; i < fields.length; i++) {
			var fieldName = fieldNames[fields[i][0]];
			if (fieldName != null)
			{
				var value = nlapiGetFieldValue(fieldName);
				var maxValueLength = fields[i][2];
				if (maxValueLength != null && value != null && value.length > maxValueLength)
				{
					alert('Value for '+ fields[i][1] +' is too long for this setting type! Max allowed limit is ' + maxValueLength + ' and entered value is ' + value.length + ' chars!');
					return false;
				}
			}
		}
	}
	
	if (settingType == 'NetSuite Integration Configuration')
	{
		var id = nlapiGetFieldValue('name');
		if (id != parseInt(id, 10)) {
			alert('This type requires ID to be numeric!');
			return false;
		}
	}
}

function CopyTypedValue(settingType, settingValueType)
{
	var valueTypes = ['Date', 'Int', 'Decimal', 'Checkbox'];
	
	if (valueTypes.indexOf(settingValueType) >= 0) {
		var value = null;
		switch (settingValueType)
		{
			case 'Date': 
				var date = nlapiGetFieldValue(fieldNames['date']);
				if (date != null) {
					value = nlapiDateToString(nlapiStringToDate(date), 'datetimetz');
				}
				break;
			case 'Int': value = nlapiGetFieldValue(fieldNames['int1']); break;
			case 'Decimal': value = nlapiGetFieldValue(fieldNames['decimal1']); break;
			case 'Checkbox': value = nlapiGetFieldValue(fieldNames['checkbox2']) == 'T' ? 'true' : 'false'; break;
		}
		if (settingType == 'NetSuite Credit Card Mapping') {
			value = nlapiGetFieldValue(fieldNames['int1']);
		}
		nlapiSetFieldValue(fieldNames['string1'], value, false);
	}
}


function UpdateFields(settingType, settingValueType, clientScript, clearValues)
{
	var conf = fieldsConfiguration[settingType];
	if (conf != null)
	{
		var valueType = fieldValueDefinition[settingValueType] || 'string1';
		var fields = UpdateValueField(conf.fields, valueType);
		ShowFields(fields, clientScript, clearValues);
	} else {
		ShowFields([], clientScript, clearValues);
	}
}

function UpdateValueField(fields, valueField)
{
	// Clone object
	var fields2 = JSON.parse(JSON.stringify(fields));
	for(var i = 0; i < fields2.length; i++) { 
		if (fields2[i][0] == settingValueType)
		{
			if (valueField != null && valueField.constructor === Array)
			{
				fields2[i][0] = valueField[0];
				fields2[i][1] = valueField[1];
			}
			else
			{
				fields2[i][0] = valueField;
			}
		}
	}
	return fields2;
}