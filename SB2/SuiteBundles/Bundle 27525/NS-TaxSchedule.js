/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       16 XII 2013     mkaluza
 *
 */

// @import RA-TaxLib.js

function userEventBeforeLoad(type, form, request){
}

function userEventAfterSubmit(type, form, request) 
{
	if (type == 'edit')
	{
		var customRecordId = GetTaxScheduleLinkId(nlapiGetRecordId());

		// Save RA-TaxSchedule if different name or description is used
		var record = nlapiLoadRecord(recordTaxSchedule, customRecordId);
		if (record.getFieldValue('name') != nlapiGetFieldValue('name') || record.getFieldValue(fieldDescription) != nlapiGetFieldValue('description')) {
			record.setFieldValue('name', nlapiGetFieldValue('name'));
			record.setFieldValue(fieldDescription, nlapiGetFieldValue('description'));
			nlapiSubmitRecord(record);
		}
	}
	else if (type == 'create') 
	{
		// If native tax schedule is created, create also RA-TaxSchedule
		var internalId = nlapiGetRecordId();
		SaveRATaxSchedule(nlapiGetFieldValue('name'), nlapiGetFieldValue('description'), internalId);
	}
	else if (type == 'delete')
	{
		var customRecordId = GetTaxScheduleLinkId(nlapiGetRecordId());
		if (customRecordId != null)
		{
			try {
				nlapiDeleteRecord(recordTaxSchedule, customRecordId);
			}
			catch (e) {
				nlapiLogExecution('DEBUG', 'Delete error', e);
			}
		}
	}
}

function SaveRATaxSchedule(name, description, linkId, receiptText)
{
	if (receiptText == null) {
		receiptText = name != null && name.length > 1 ? name.substring(0,2) : "";
	} 
	
	var record = nlapiCreateRecord(recordTaxSchedule);
	record.setFieldValue('name', name);
	record.setFieldValue(fieldDescription, description);
	record.setFieldValue(fieldScheduleLinkId, linkId);
	record.setFieldValue(fieldReceiptText, receiptText);
	nlapiSubmitRecord(record);
}

function GetZeroTaxGroup() {
	var zeroTaxGroups = GetRecords('taxgroup', ['internalid', 'itemid', 'rate'],  ['rate', 'lessthanorequalto', 0.1]);
	if (zeroTaxGroups != null && zeroTaxGroups.length > 0) {
		for (var i = 0; i < zeroTaxGroups; i++)
		{
			if (zeroTaxGroups[i].itemId == '-Not Taxable-') {
				return zeroTaxGroups[i].internalid;
			}
		}
		return zeroTaxGroups[0].internalid;	
	}
	return 0;
}

function GetTaxScheduleLinkId(id) {
	var filters = new Array();
	filters[0] = new nlobjSearchFilter(fieldScheduleLinkId, null, 'equalTo', id);
	var column = new nlobjSearchColumn('internalid',null,null);
	var searchResults = nlapiSearchRecord(recordTaxSchedule, null, filters, column);
	if (searchResults && searchResults.length == 1) {
		return searchResults[0].getValue('internalid');
	} 
	return -1;
}
