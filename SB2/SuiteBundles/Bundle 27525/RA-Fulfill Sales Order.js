function postItemFulfillment(datain) {
    // datain should be JSON from RAPOS
    // { 
	//     type: "salesorder",
    //     internalId: "#",
    //     location: "#",
    //     item: [{ internalId: "#", 
	//              type: "inventoryItem",
	//              qty: "#.##",
	//              location: "#" },
    //            { internalId: "#", 
	//              type: "inventoryItem",
	//              qty: "#.##", 
	//              location: "#" }
    //     ]
    // }
	
	if (datain == null)	{
		return error("There is not input data.");
	}
	
    // datain.type must be either transferorder or purchaseorder
    if (datain.type != "salesorder") {
    	return error("Invalid type specified in request.  Only salesorder allowed.");
    }

    if (datain.item == null || datain.item.length == 0){
    	return error("Invalid type specified in request.  Any item is defined.");    	
    }
       
    var context = nlapiGetContext();
    var isMultipleLocationFeatureEnabled = context.getFeature("MULTILOCINVT");
    var isPickPackShipFeatureEnabled = context.getFeature("PICKPACKSHIP");

    var items = {};
    for (var i = 0; i < datain.item.length; i++){
    	var internalId = datain.item[i].internalId;
    	
    	if (internalId == null){
    		return error("Item has to have Internal ID.");    		
    	}

		items[internalId] = datain.item[i];		
		items[internalId].location = datain.item[i].location || datain.location;
		
		if (items[internalId].location == null && isMultipleLocationFeatureEnabled) {
			return error("Item has to have location.");
		}		
    }
        
    var orderInternalId = datain.orderInternalId;
    if (orderInternalId == null) {
    	if (datain.orderExternalId == null){
    		return error("Internal or External ID of order have to be defined.");
    	}
        // Check if we have an external ID and get the internal ID through a search
        var filter = new nlobjSearchFilter("externalid", null, "is", datain.orderExternalId, null);
        var col = new nlobjSearchColumn("internalId");
        var results = nlapiSearchRecord(datain.type, null, filter, col);

        if (results != null && results.length > 0) {
            orderInternalId = results[0].getValue("internalId");
        }
    }

    var itemFulfillment = nlapiTransformRecord(datain.type, orderInternalId, "itemfulfillment");
    itemFulfillment.setFieldValue("externalid", datain.externalId);
    
    if (isPickPackShipFeatureEnabled){
        itemFulfillment.setFieldValue("shipstatus", "C");
    }

    // Set all quantities to 0 unless we find them below
    var itemCount = itemFulfillment.getLineItemCount("item");
    for (var j = 1; j <= itemCount; j++) {
    	var itemID = itemFulfillment.getLineItemValue("item", "item", j);
    	var item = items[itemID];
    	
    	if (item == null){
    		itemFulfillment.setLineItemValue("item", "quantity", j, 0);
    		continue;
    	}
    	
        itemFulfillment.setLineItemValue("item", "quantity", j, item.qty);
        itemFulfillment.setLineItemValue("item", "location", j, item.location);
    }

    // Let's submit the receipt
    var id = nlapiSubmitRecord(itemFulfillment);

    return itemFulfillment;
}

function error(message, code) {
	code = code || "401";
	return { code: code, message: message };
}