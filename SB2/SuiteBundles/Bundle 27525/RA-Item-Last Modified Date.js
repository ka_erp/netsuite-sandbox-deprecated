/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Dec 2013     bsomerville
 * 1.10       30 Jun 2014     nkomissarenko    Pricing and custom fields handling added, minor bugs fixed
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	if(type != 'create'){
		  var rec = nlapiGetNewRecord(); //contains only items that are modified
		  var old = nlapiGetOldRecord(); //all items
		  
		  nlapiLogExecution("DEBUG", "EDIT", rec.getId());
		  
		  // Temp variable to store the value from the old record
		  var oldValue = null;
		  var modifiedValue = null;
		  var modifiedFields = rec.getAllFields();//get fields names that are modified
		  
		  // Fields we care about
		  var fields =  ['usebins',
		                 'unitstype',
		                 'saleunit',
		                 'subsidiary',
		                 'includechildren',
		                 'department',
		                 'location',
		                 'costestimatetype',
		                 'displayname', 
		                 'salesdescription', 
		                 'description',
		                 'class', 
		                 'itemid', 
		                 'taxschedule', 
		                 'isinactive', 
		                 'pricinggroup', 
		                 'parent', 
		                 'upccode',                 
		                 'mpn', 
		                 'vendorname'
		                 ];
		  
		  //check pricing features
		  var multiCurrency = nlapiGetContext().getFeature('MULTICURRENCY');
		  var multiPrice = nlapiGetContext().getFeature('MULTPRICE');
		  var quantityPricing = nlapiGetContext().getFeature('QUANTITYPRICING');
		  
		  if (!multiCurrency && !multiPrice && !quantityPricing && checkPrices("rate", rec, old)) {
		  		return;
		  }
		  else if (multiCurrency) {
			  var currencies = nlapiSearchRecord('currency', null, null);
			  
			  for (var i = 0; i < currencies.length; i++) {
				  if(checkPrices("price" + currencies[i].getId(), rec, old)){
					  return;
				  }
			  }
		  }
		  else if (checkPrices("price", rec, old)){
			  return;
		  }

		  // Check each field
		  for (var x = 0; x < modifiedFields.length; x++) {
			  var fieldName = modifiedFields[x];

			  if(~fields.indexOf(fieldName) || ~fieldName.indexOf("custitem_")){
				  // Treating null as empty string
				  oldValue = old.getFieldValue( fieldName ) == null ? '' : old.getFieldValue( fieldName );
				  modifiedValue = rec.getFieldValue( fieldName ) == null ? '' : rec.getFieldValue( fieldName );;
				  
				  if(oldValue != modifiedValue ) {//even if it is field we are interested in, did the value change?
					  nlapiLogExecution("DEBUG", fields[x], oldValue + " => " + modifiedValue);
					  setModifiedDate();

					  return;//no need to continue
				  }
			  }
		  }
	} else {
		// Record created, update the last modified custom field
		  setModifiedDate();
	}  
}

function checkPrices(priceId, rec, old){
	var priceLevels = rec.getLineItemCount(priceId);
	var oldItemPrice = 0;
	var newItemPrice = 0;
	
	for (var i = 1; i <= priceLevels; i++) {
		newItemPrice = rec.getLineItemMatrixValue(priceId, 'price', i, 1); //taking care only of the base price from the qty schedule
		oldItemPrice = old.getLineItemMatrixValue(priceId, 'price', i, 1);
		
		if(newItemPrice != oldItemPrice){
			nlapiLogExecution("DEBUG", "price changed", oldItemPrice + " => " + newItemPrice);
			setModifiedDate();

			return true;
		}
	}

	return false;
}

function setModifiedDate() {
	var date_time_value = nlapiDateToString(new Date(), 'datetimetz');
	nlapiSetDateTimeValue('custitem_ra_lastmodified', date_time_value, 'America/Los_Angeles');
}