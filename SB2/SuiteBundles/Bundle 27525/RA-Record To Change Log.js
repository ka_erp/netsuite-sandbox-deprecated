function saveRecord(type, name)
{
   nlapiLogExecution('DEBUG', 'Save Record - Invoked ' + type);

   var stRecType = nlapiGetRecordType();
   var stRecId = nlapiGetRecordId();

   nlapiLogExecution('DEBUG', 'Save Record - Invoked', 'Record Type: ' + stRecType + '; Record ID: ' + stRecId);

   if (!stRecId || stRecId == "") return;

   var currentContext = nlapiGetContext();   
   if (currentContext.getExecutionContext() == 'webservices') return;

   if (stRecType == 'otherchargeitem' || stRecType == 'noninventoryitem'
      || stRecType == 'serviceitem' ) {
      var rec = nlapiLoadRecord(stRecType,stRecId);
      var subtype = rec.getFieldValue('subtype');
      stRecType = stRecType.substring(0, stRecType.length-4) + subtype + 'item';
   }

   if (type != 'delete') {  
      var recChange = nlapiCreateRecord('customrecordchangelog');
      recChange.setFieldValue('custrecordrecordtype',stRecType);
      recChange.setFieldValue('custrecordrecordid',stRecId);
      nlapiSubmitRecord(recChange);
   }
}