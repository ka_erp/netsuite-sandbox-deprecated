/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 Sep 2013     dotocka
 * 1.01       23 Mar 2015     dotocka
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function suitelet(request, response){
	if (request.getMethod() == 'GET') {
		var copy_from_form = nlapiCreateForm('Select workstation ...', true);
		copy_from_form.addSubmitButton('Copy');	
		copy_from_form.addButton('custpage_cancel', 'Cancel',  'window.close()');
		
		//get parameters
		var locationId = request.getParameter('lc');
		var workStationId = request.getParameter('ws');
		
		var filters = [];
		var columns = [];
		filters[filters.length] = new nlobjSearchFilter('custrecord_ra_ws_loc', null, 'anyof', parseInt(locationId));
		if(isNaN(parseInt(workStationId)) == false){
			filters[filters.length] = new nlobjSearchFilter('internalidnumber', null, 'notequalto', parseInt(workStationId));
		}
		columns[columns.length] = new nlobjSearchColumn('name');
		columns[columns.length] = new nlobjSearchColumn('internalid');
		
		var workstationResults = nlapiSearchRecord('customrecord_ra_workstation', null, filters, columns);
		
		var text = copy_from_form.addField('text', 'text', '');
		text.setDisplayType('inline');
		text.setDefaultValue('Please select workstation from which you want to copy the provider settings');
		text.setLayoutType('outsideabove');
		
		var copy_from = copy_from_form.addField('copy_from', 'select', 'Workstation to copy from');
		for ( var int = 0; workstationResults !== null && int < workstationResults.length; int++) {
			var result = workstationResults[int];
			copy_from.addSelectOption(result.getValue('internalid'), result.getValue('name'));
		}
		
		response.writePage(copy_from_form);
	}
	else {//copy
		submit(request, response);
	}
}

//Copy settings from chosen workstation
function submit(request, response){
	var copy_from = request.getParameter('copy_from');
	
	if(copy_from != null && parseInt(copy_from) != 0){
		var wsCopyFrom = nlapiLoadRecord('customrecord_ra_workstation', parseInt(copy_from));
		
		for (var property in providerProperties){
			if(property == 'custrecord_ra_provider_info_isemv'){
				providerProperties[property] = wsCopyFrom.getFieldValue(property) == 'T' ? true : false;
				continue;
			}
			providerProperties[property] = wsCopyFrom.getFieldValue(property);
		}
	}
	response.writeLine(createClientScript(providerProperties));
}

function createClientScript(providerProperties){
	var clientScript = '<script type="text/javascript">';
    clientScript += copyPaymentProviderProperties.toString() + '\n';
    clientScript += 'copyPaymentProviderProperties(' + JSON.stringify(providerProperties) + ');</script>';
	return clientScript;
}

function copyPaymentProviderProperties(providerProperties){
	for (var property in providerProperties){
		if(property == 'custrecord_ra_provider_info_isemv'){
			if(providerProperties[property] != window.opener.getCheckboxState(window.opener.NS.jQuery('#custrecord_ra_provider_info_isemv_fs').get(0))){
				window.opener.NS.jQuery('input[id^="custrecord_ra_provider_info_isemv"]').click();
			}
			continue;
		}
		if(property == 'custrecord_ra_cc_provider'){
			window.opener.getDropdown(window.opener.NS.jQuery('#custrecord_ra_cc_provider_fs').get(0)).setValue(providerProperties[property]);
			continue;
		}
		window.opener.NS.jQuery('input#'+ property).val(providerProperties[property]);
	}
	window.close();
}
