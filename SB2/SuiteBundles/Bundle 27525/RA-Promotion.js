function fieldChanged ( type, name ) {
   if ( name == "custrecord_ra_promo_promotype" ) {
	   setFieldsState();
   }
}

function clientSaveRecord(){
	var fv_dollar_off = +nlapiGetFieldValue("custrecord_ra_promo_dollaroff");
	var fv_new_price = +nlapiGetFieldValue("custrecord_ra_promo_newprice");
	var fv_buy = +nlapiGetFieldValue("custrecord_ra_promo_buy");
	var fv_get = +nlapiGetFieldValue("custrecord_ra_promo_get");
	var fv_name = nlapiGetFieldValue("name");
	var msg = "";
	
	if (fv_buy < 0){
		msg += "- 'Buy' field can't have negative value.\n";
	}
	if (fv_buy > 9999999){
		msg += "- 'Buy' can't be more than 9999999.\n";
	}
	if (fv_get < 0){
		msg += "- 'Get' field can't have negative value.\n";
	}
	if (fv_get > 9999999){
		msg += "- 'Get' can't be more than 9999999.\n";
	}
	if (fv_dollar_off < 0){
		msg += "- '$Off' can't have negative value.\n";
	}
	if (fv_dollar_off > 9999999){
		msg += "- '$Off' can't be more than 9999999.\n";
	}
	if (fv_new_price < 0){
		msg += "- 'New Price' can't have negative value.";
	}
	if (fv_new_price > 9999999){
		msg += "- 'New Price' can't be more than 9999999.";
	}
	if(fv_name.toString().length > 40){
		msg += "- 'Name' is too long, maximum length is 40 characters.";
	}
	
	if(msg.length > 0){
		alert("The record can't be saved due to the errors below:\n\n" + msg);
		return false;
	}

	return true;
}

function setFieldsState(){
	var promotype = nlapiGetFieldValue ( "custrecord_ra_promo_promotype" );
	
	if(promotype != 1){
		nlapiSetFieldValue("custrecord_ra_promo_percentoff", "", false, true);
	}
	if(promotype != 2){
		nlapiSetFieldValue("custrecord_ra_promo_dollaroff", "", false, true);
	}
	if(promotype != 3){
		nlapiSetFieldValue("custrecord_ra_promo_newprice", "", false, true);
	}

    nlapiDisableField ( "custrecord_ra_promo_percentoff", promotype != 1 );
    nlapiDisableField ( "custrecord_ra_promo_dollaroff", promotype != 2 );
    nlapiDisableField ( "custrecord_ra_promo_newprice", promotype != 3 );
}

function validatePromoItem(type){
	var currentId = nlapiGetCurrentLineItemValue(type, 'custrecord_ra_promoitem_item');
	var currentIndex = nlapiGetCurrentLineItemIndex(type);
	var itemCount = nlapiGetLineItemCount(type);
	
	for(var i = 1; i <= itemCount; i++)
	{
		var id = nlapiGetLineItemValue(type, 'custrecord_ra_promoitem_item', i);
		if (i != currentIndex && id == currentId)
		{
			alert('The item ' + nlapiGetCurrentLineItemValue(type, 'custrecord_ra_promoitem_displayname') + 
					' already exists in the list. Please choose a different one.');
			return false;
		}
	}
	return true;
}