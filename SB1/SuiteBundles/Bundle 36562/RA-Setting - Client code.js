/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 IX 2013     mkaluza
 *
 */

var selectField = new Array();
var settingType = new Array();

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type){
	nlapiLogExecution('DEBUG', 'Setting', 'Start');
	
	selectField['string'] 	= jQuery('#custrecord_ra_setting_value_fs').addClass('valueField string');
	selectField['date'] 	= jQuery('#custpage_ra_setting_value_date_fs').addClass('valueField date');
	selectField['checkbox'] = jQuery('#custpage_ra_setting_value_checkbox_fs').addClass('valueField checkbox');
	selectField['parent'] 	= selectField['string'].parent(); 
	
	// Put all field in a line
	selectField['string'].after(selectField['date']).after(selectField['checkbox']);
	
	// Hide all and show appropriate field
	selectField['parent'].children('.valueField').hide();
	
	var selectedValue = nlapiGetFieldText('custrecord_ra_setting_value_type');
	switch (selectedValue) {
	case 'Checkbox': 	
		var value = nlapiGetFieldValue('custrecord_ra_setting_value');
		nlapiSetFieldValue('custpage_ra_setting_value_checkbox', value == 'true');
		selectField['parent'].children('.valueField.checkbox').show(); 	
		break;
	case 'Date': 		
		var value = nlapiGetFieldValue('custrecord_ra_setting_value');
		nlapiSetFieldValue('custpage_ra_setting_value_date', value);
		selectField['parent'].children('.valueField.date').show();
		break;
	default:
		selectField['parent'].children('.valueField.string').show();
	}
	
	// Hide labels
	jQuery('#custpage_ra_setting_value_checkbox_fs_lbl').hide();
	jQuery('#custpage_ra_setting_value_date_fs_lbl').hide();
	
	fieldChanged('', 'custrecord_ra_setting_type');
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord(){
	nlapiLogExecution('DEBUG', 'Duplicates', 'Invoked');
		
	var name = nlapiGetFieldValue('name');
	var recordType = nlapiGetRecordType();
	var value = nlapiGetFieldValue('custrecord_ra_setting_value');
	var type = nlapiGetFieldValue('custrecord_ra_setting_type');
	var typeText = nlapiGetFieldText('custrecord_ra_setting_type');
	var internalId = nlapiGetRecordId();
	
	if (value == "") {
		if (confirm("Are you sure to leave the value empty?") == false) {
			return false;
		}
	}
	
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('name', null, 'is', name);
	filters[1] = new nlobjSearchFilter('custrecord_ra_setting_type', null, 'is', type);
	filters[2] = new nlobjSearchFilter('internalidnumber', null, 'notequalto', internalId);
	var column = new nlobjSearchColumn('internalid',null,null);
	var searchResults = nlapiSearchRecord(recordType, null, filters, column);
	
	if (searchResults){		
		nlapiLogExecution('DEBUG', 'Duplicates', 'Search result count: ' + searchResults.length);

		if (searchResults.length > 0){
			alert('Field is not unique. There already item with the same name.');
			nlapiLogExecution('DEBUG', 'Duplicates', "Field " + fieldItem + " is not unique. End with false.");
			return false;
		}
	}
	
	// We have to forbid entering two identical values for NS.InboundTriggers table
	if ( typeText == 'Custom Record TypeId for NS.InboundTriggers' && 
			CheckDuplicatesForInboundTriggers(value, internalId, recordType, type) == true) {
		alert('There is already a value ' + value + ' in InboudTriggers table. Please choose a different one.');
		return false;
	}
	
	// Copy to string field
	var selectedValue = nlapiGetFieldText('custrecord_ra_setting_value_type');
	nlapiLogExecution('DEBUG', 'Duplicates', 'Type: ' + type + 'Value type: ' + selectedValue);
	
	switch (selectedValue) {
	case 'Checkbox':	
		var value = nlapiGetFieldValue('custpage_ra_setting_value_checkbox');
		if (value == 'T') {
			nlapiSetFieldValue('custrecord_ra_setting_value', 'true');
		} else {
			nlapiSetFieldValue('custrecord_ra_setting_value', 'false');
		}
		break;
	case 'Date':
		var value = nlapiGetFieldValue('custpage_ra_setting_value_date');
		nlapiSetFieldValue('custrecord_ra_setting_value', value);
		break;
	}
	
	// Check name and value length
	var settingType = nlapiLoadRecord('customrecord_ra_setting_type', type);
	if (settingType) {
		var value = nlapiGetFieldValue('custrecord_ra_setting_value');
		var maxNameLength = settingType.getFieldValue('custrecord_max_length_name');
		var maxValueLength = settingType.getFieldValue('custrecord_max_length_value');

		if (value != null && value.length > maxValueLength) {
			alert('Value is too long for this setting type! Max allowed limit is ' + maxValueLength + ' and entered value is ' + value.length + ' chars!');
			return false;
		}
		if (name != null && name.length > maxNameLength) {
			alert('Name is too long for this setting type! Max allowed limit is ' + maxNameLength + ' and entered value is ' + name.length + ' chars!');
			return false;
		}
	}
	
	nlapiLogExecution('DEBUG', 'Saved', 'Saved value: ' + nlapiGetFieldValue('custrecord_ra_setting_value'));
    return true;
}

function fieldChanged(type, name){
	nlapiLogExecution('DEBUG', 'Field changed', 'Invoked');
	
	if (name == 'custrecord_ra_setting_type') {
		var selectedSettingType = nlapiGetFieldText('custrecord_ra_setting_type');
		nlapiLogExecution('DEBUG', 'Field changed', 'Selected setting type: ' + selectedSettingType);
		
		// set default fields
		jQuery('#custrecord_ra_setting_encrypted_fs').parent().parent().hide();
		jQuery('#custrecord_ra_setting_description_fs').parent().parent().show();
		jQuery('#custrecord_ra_setting_description_fs_lbl > a').text('Description');
		nlapiDisableField('custrecord_ra_setting_value_type', false);
		
		switch (selectedSettingType) {
		case 'NS Replication settings':
			jQuery('#custrecord_ra_setting_encrypted_fs').parent().parent().show();
			break;
		case 'RARS Replication XML Settings':
			jQuery('#custrecord_ra_setting_description_fs').parent().parent().hide();
			break;
		case 'RA Settings':
			break;
		case 'Custom Record TypeId for NS.InboundTriggers':
			nlapiSetFieldText('custrecord_ra_setting_value_type','Int');
			nlapiDisableField('custrecord_ra_setting_value_type', true);
			jQuery('#custrecord_ra_setting_description_fs_lbl > a').text('rafsxsltpath');
			break;
		case 'RA Flags':
			nlapiSetFieldText('custrecord_ra_setting_value_type','Checkbox');
			nlapiDisableField('custrecord_ra_setting_value_type', true);
			break;
		}
	} 
	
	if (name == 'custrecord_ra_setting_value_type' || name == 'custrecord_ra_setting_type') {
		var selectedValue = nlapiGetFieldText('custrecord_ra_setting_value_type');
		nlapiLogExecution('DEBUG', 'Field changed', 'Selected: ' + selectedValue);
		
		// hide all fields
		selectField['parent'].children('.valueField').hide();
		
		switch (selectedValue) {
		default:
		case 'String':
		case 'Int':	
		case 'Decimal':
			selectField['parent'].children('.valueField.string').show(); break;
		case 'Checkbox':
			selectField['parent'].children('.valueField.checkbox').show(); break;
		case 'Date':
			selectField['parent'].children('.valueField.date').show(); break;
		}
	}
	validateField(null,'custrecord_ra_setting_value');
}

function validateField(type, name) {
	nlapiLogExecution('DEBUG', 'Field validate', 'Invoked: ' + name);
	
	if (name == 'custrecord_ra_setting_value') {
		var selectedType = nlapiGetFieldText('custrecord_ra_setting_value_type');
		var value = nlapiGetFieldValue('custrecord_ra_setting_value');
		nlapiLogExecution('DEBUG', 'Field changed', 'Selected type: ' + selectedType + ' entered value: ' + value);
		
		if (value.length == 0) return true;
		
		switch (selectedType) {
		case 'Int':	
			if (isNaN(parseInt(value)) || !isFinite(value) ||  Math.floor(value) != value) {
				alert('Entered value is not in correct format!');
				return  false;
			}
			break;
		case 'Decimal':
			if (isNaN(parseFloat(value)) || !isFinite(value)) {
				alert('Entered value is not in correct format!');
				return  false;
			}
			break;
		}
	}
	
	return true;
}

function CheckDuplicatesForInboundTriggers(value, internalId, recordType, type) {
	var filters = new Array();
	filters[0] = new nlobjSearchFilter('custrecord_ra_setting_value', null, 'is', value);
	filters[1] = new nlobjSearchFilter('custrecord_ra_setting_type', null, 'is', type);
	filters[2] = new nlobjSearchFilter('internalidnumber', null, 'notequalto', internalId);
	var column = new nlobjSearchColumn('internalid',null,null);
	var searchResults = nlapiSearchRecord(recordType, null, filters, column);
	if (searchResults){		
		nlapiLogExecution('DEBUG', 'Duplicates', 'Duplicate inbounds found: ' + searchResults.length);
		return true;
	}
	return false;
}
