/**
 * Item Pricing and Additional Information
 * 
 * Version    Date            Author           Remarks
 * 2013.2.11  17 Oct 2013     bsomerville
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function postRESTlet(dataIn) {	
	var filters = new Array();
	var items = dataIn.ItemID.split(',');
	var itemsForPage = new Array();
	var ret = new Object();
	ret.Results = new Array();
	
	var count = 0;
	var pageCount = 0;
	var currenciesCount = nlapiGetContext().getFeature('MULTICURRENCY') ? getRecordCount("currency") : 1;
	var pageSize = Math.floor(1000 / getRecordCount("pricelevel") / currenciesCount - 1);
	
	// Split this up into bite-sized chunks to accommodate 1k limit on nlapiSearchRecord
	for (var item in items) {
		itemsForPage[count++] = items[item];
		
		if (count == items.length || item == items.length-1 || count >= pageSize) {
			filters[0] = new nlobjSearchFilter("internalid",null,"anyOf", itemsForPage);
			var results = new Object();
			results.Items = nlapiSearchRecord(null, "customsearch_ra_sync_itempricing", filters, null);
			ret.Results[pageCount++] = results;
			itemsForPage = new Array();
			count = 0;
		}
	}
	
	return ret;
}

// Get total number of price levels so we know how many iterations to run on each pricing matrix
function getPriceLevelCount() {
	var columns = new Array(); 
	
	columns[0] = new nlobjSearchColumn("internalid",null,null); 
	
	var results = nlapiSearchRecord("pricelevel",null,null,columns);
	
	return results.length;
}

function getRecordCount(recordType) {
	var columns = new Array();
	columns[0] = new nlobjSearchColumn("internalid", null, null);
	try {
		var results = nlapiSearchRecord(recordType,null,null,columns); 
		return results.length; 
	} catch (e) {
	// Record does not exist, so return 0 as not found 
		return 0;
	} 
}

function getItemPricing ( dataIn ) {
	var filters = new Array();
	var items = dataIn.itemId.split(',');
		
	filters[0] = new nlobjSearchFilter("internalid",null,"anyOf", items);
	results = nlapiSearchRecord(null, "customsearch_ra_sync_itempricing", filters, null);
	
	var ret = new Object();
	ret.Results = results;
	
	return ret;
}