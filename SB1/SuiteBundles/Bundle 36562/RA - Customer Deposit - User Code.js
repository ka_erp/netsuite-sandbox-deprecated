/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       02 Feb 2014     vkaluza
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord CustomerDeposite
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
	if ((type == 'create'|| type == 'edit') && nlapiGetContext().getExecutionContext() == 'webservices'){
		form.addField('custpage_ra_salesorder_externalid', 'text', 'Sales Order Extrnal ID');
	}	
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord CustomerDeposite
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){
	
	nlapiLogExecution('DEBUG', 'CustomerDeposit', 'Is invoked.');
	
	if ((type != 'create' && type != 'edit') || nlapiGetContext().getExecutionContext() != 'webservices'){
		return;
	}

	nlapiLogExecution('DEBUG', 'CustomerDeposit', 'Is invoked for create from web service.');

	var salesOrder = nlapiGetFieldValue('salesorder');
	
	nlapiLogExecution('DEBUG', 'salesOrder', (salesOrder) ? salesOrder : 'Is empty');	
		
	if (salesOrder){
		return;
	}

	var salesOrderExternalId = nlapiGetFieldValue('custpage_ra_salesorder_externalid');	

	nlapiLogExecution('DEBUG', 'salesOrderExternalId', (salesOrderExternalId) ? salesOrderExternalId : 'Is empty');	
	
	var results = nlapiSearchRecord('salesorder', null, ['externalid', 'is', salesOrderExternalId], new nlobjSearchColumn('internalid'));

	if (!results){
		throw nlapiCreateError('UNEXPECTED_ERROR', 'Deposit cannot be linked to sales order. Sales order cannot be found.', true);
	}		
	
	var internalId = results[0].getValue('internalid');
	
	nlapiLogExecution('DEBUG', 'salesOrderInternalid', internalId);
	
	if (!internalId){
		throw nlapiCreateError('UNEXPECTED_ERROR', 'Deposit cannot be linked to sales order.', true);
	}

	nlapiSetFieldValue('salesorder', internalId);
	
	nlapiLogExecution('DEBUG', 'salesOrder', nlapiGetFieldValue('salesorder'));
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord CustomerDeposite
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
}
