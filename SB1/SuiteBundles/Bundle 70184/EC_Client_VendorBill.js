/**
 * Company            Explore Consulting
 * Copyright          2013 Explore Consulting, LLC
 * Type               NetSuite EC_Client_VendorBill
 * Version            1.0.0.0
 * Description        Client script for Vendor Bills
 **/
function onInit(type) {

}

function vb_onSave() {
	
	var returnVal;
	
    // Check for duplicate invoice number
    returnVal = duplicateInvoiceNumberCheck();
    
    // If this is a valid invoice number, proceed with due date update
    if(returnVal == true) {
    	
    	updateDueDate(nlapiGetFieldValue('custbody_ap_invoice_date'));	
    }
    
    return returnVal;
}

function onValidateField(type, fld) {

    return true;
}

function onFieldChanged(type, fld) {

    // 9/15/2013:  Logic to set Due Date based on date when invoice is received from Vendor.
    nlapiLogExecution('DEBUG', 'onFieldChanged', 'fld:  ' + fld);
    nlapiLogExecution('DEBUG', 'onFieldChanged', 'Bill Date:  ' + nlapiGetFieldValue('custbody_ap_invoice_date'));
    
    if (fld == 'custbody_ap_invoice_date' || fld == 'terms' || fld == 'trandate') {
    	
    	updateDueDate(nlapiGetFieldValue('custbody_ap_invoice_date'));
    }
        
    return true;
}

function onPostSourcing(type, fld) {

}

function onLineSelect(type) {

}

function onValidateLine(type) {

    return true;
}

function onLineRecalc(type) {

}

function updateDueDate(invReceiveDate)
{
    var tranDate = nlapiGetFieldValue('trandate');
    nlapiLogExecution('DEBUG', 'updateDueDate', 'tranDate:  ' + tranDate);
    var termSet = nlapiGetFieldValue('terms');
    nlapiLogExecution('DEBUG', 'updateDueDate', 'termSet:  ' + termSet);
    if ( termSet != '' && tranDate != '' )
    {
        //var termDays = nlapiLookupField('term', termSet, 'daysuntilnetdue');
        var response = nlapiRequestURL('/app/site/hosting/scriptlet.nl?script=223&deploy=1' + '&term=' + termSet);
        var responseObj = JSON.parse(response.getBody());
        nlapiLogExecution('DEBUG', 'getEmpApprovedTimeOnTask', 'Response from Suitelet:  ' + responseObj.NumberOfDays);
        var termDays = responseObj.NumberOfDays;

        nlapiLogExecution('DEBUG', 'updateDueDate', 'termDays:  ' + termDays);
        if ( invReceiveDate == '' )
        {
            // Invoice Receive Date is not populated - so set Due Date 1 year out
            var dueDate = nlapiAddDays(nlapiStringToDate(tranDate), parseInt(365));
            dueDate = nlapiDateToString(dueDate);
            nlapiLogExecution('DEBUG', 'updateDueDate', 'dueDate:  ' + dueDate);
            nlapiSetFieldValue('duedate', dueDate);
        }
        else
        {
            // Invoice Receive Date was populated - so set the Due Date to be the Inv Recd Date + Term Days
            var dueDate = nlapiAddDays(nlapiStringToDate(invReceiveDate), parseInt(termDays));
            dueDate = nlapiDateToString(dueDate);
            nlapiLogExecution('DEBUG', 'updateDueDate', 'dueDate:  ' + dueDate);
            nlapiSetFieldValue('duedate', dueDate);
        }
    }
    else
    {
        nlapiLogExecution('DEBUG', 'updateDueDate', 'Either Tran Date or Term was empty so logic could not execution to set the Due Date');
    }

}
function duplicateInvoiceNumberCheck() {
	
	var returnVal = true
	,	bypassRoleArray
	,	role
	,	vendorId
	,	tranId
	,	filters = []
	,	billSearch
	,	currId;

	// Get excluded roles from script parameter
	bypassRoleArray = nlapiGetContext().getSetting('SCRIPT', 'custscript_ec_excluded_roles');
	bypassRoleArray = bypassRoleArray.split(',');	
	
	// Get user's role
	role = nlapiGetRole();
	
	// If user's role is in role array, they can bypass this operation; return
	if (bypassRoleArray.indexOf(role) !== -1) {
		
		return returnVal;
	}
	
	// Check for an existing vendor bill with same vendor and tran ID
	tranId = nlapiGetFieldValue('tranid');
	vendorId = nlapiGetFieldValue('entity');
	
	filters.push(new nlobjSearchFilter('tranid', null, 'is', tranId));
	filters.push(new nlobjSearchFilter('entity', null, 'is', vendorId));
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('type', null, 'is', 'VendBill'));
	
	currId = nlapiGetRecordId();
	if(currId) {
		
		filters.push(new nlobjSearchFilter('internalid', null, 'noneof', currId));
	}
	
	try {
		
		billSearch = nlapiSearchRecord('transaction', null, filters, null);
	} catch(e) {
		
		nlapiLogExecution('ERROR', 'Error searching vendor bill records', e);
		var body
		,	subject
		,	recipient
		,	author;
		
		subject = 'Vendor Bill Save Error';
		recipient = '';
		author = '11';
		body = 'An error occurred searching Vendor Bill records during the submit event on a Vendor Bill: ' + e;
//		nlapiSendEmail(author, recipient, subject, body);
	}
		
	// If duplicate exists, proceed
	if (billSearch && billSearch.length) {
		
		returnVal = false;
		alert('This Invoice Number already exists for this Vendor.  Please enter a unique number and save again.');
	}
	
	return returnVal;
  
}