/**
 * © 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       23 Mar 2015     jyeh             Initial
 * 2.00       24 Mar 2015     jyeh
 * 3.00       28 Mar 2015     jyeh
 * 4.00       06 Apr 2015     rwong            Changed link to add support for trigger types
 * 5.00       09 Apr 2015     jyeh
 * 6.00       01 Jul 2015     jmarimla         Updated loading masks
 * 7.00       06 Aug 2015     rwong            Added url css class to links
 * 8.00       11 Aug 2015     rwong            Added url css class to script deployment
 * 9.00       28 Aug 2015     rwong            Added support for customer debugging
 *
 */

Ext4.define('PSGP.APM.SIA.Component.Grid.SuiteScriptDetail', {
    extend: 'PSGP.APM.Component.Grid',
    store: PSGP.APM.SIA.dataStores.suiteScriptDetailData,
    viewConfig: {
        deferEmptyText: false,
        emptyText: 'No records to show.',
        loadMask: MASK_CONFIG
    },
    columns: {
        defaults : {
            hideable : false,
            draggable : false,
            menuDisabled : true,
            height: 28,
            flex: 1
        },
        items : [
            {
                text : 'Date & Time',
                dataIndex : 'date'
            },
            {
                text : 'Script Type / Workflow',
                dataIndex : 'scripttype'
            },
            {
                text : 'Name',
                dataIndex : 'script',
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    var url = record.get('scriptwfurl');
                    var scriptid = record.get('scriptid');
                    if ((COMPID_MODE == 'T') && (COMP_FIL)) {
                        if(url != '') {
                            return '<a href="' + url + '" target="_blank" class="apm-a">'
                            + scriptid + '</a>';
                        } else {
                            return scriptid;
                        }
                    } else {                    
                        return '<a href="' + url + '" target="_blank" class="apm-a">'
                        + value + '</a>';
                    }
                    return value;
                }
            },
            {
                text : 'Execution Context',
                dataIndex : 'triggertype'
            },
            {
                text : 'Deployment Id',
                dataIndex : 'deployment',
                renderer: function(value, metaData, record, rowIndex, colIndex, store) {
                    var url = record.get('deploymenturl');
                    if (value != null && url != '')
                    {
                        return '<a href="'+url+'" target="_blank" class="apm-a">'
                            + value +'</a>';
                    }
                    return value;
                }
            },
            {
                text : 'Total Time',
                dataIndex : 'totaltime'
            },
            {
                text : 'Usage',
                dataIndex : 'usagecount'
            },
            {
                text : 'Record Operations',
                dataIndex : 'records'
            },
            {
                text : 'Url Requests',
                dataIndex : 'urlrequests'
            },
            {
                text : 'Searches',
                dataIndex : 'searches'
            }
        ]
    },
    listeners : {
        itemmouseenter: function (view, record, item, index, e, opts)
        {
            var name = record.get('id');
            var chart = PSGP.APM.SIA.Highcharts.timelineChart;
            chart.get(name).setState('hover');
        },
        itemmouseleave: function (view, record, item, index, e, opts)
        {
            var name = record.get('id');
            var chart = PSGP.APM.SIA.Highcharts.timelineChart;
            chart.get(name).setState();
        }
    }
});

