/**
 * © 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       29 Oct 2014     jmarimla         Initial
 * 2.00       04 Nov 2014     jmarimla         Added filter panel
 * 3.00       07 Nov 2014     jmarimla         Added performance chart and suitescript detail panels
 * 4.00       11 Nov 2014     jmarimla         Commented out deployment name combobox
 * 5.00       20 Nov 2014     rwong            Added view all logs button
 * 6.00       28 Nov 2014     rwong            Implement support parameter setting of filters
 * 7.00       02 Dec 2014     jmarimla         Added components for performance chart portlet
 * 8.00       09 Jan 2015     rwong            Set psgp-apm-ssa-container-perfchart chart to be shown by default. Remove ExtJS Chart
 * 9.00       29 Jan 2015     rwong            Added height definition to psgp-apm-ssa-container-perfchart. Removed commented code.
 * 10.00      02 Feb 2015     jmarimla         Removed height in ssd summary portlet
 * ********************************************************************************
 * 1.00       23 Feb 2015     rwong            Ported SPM to APM.
 * 2.00       19 May 2015     jmarimla         Add component ids
 * 3.00       09 Jul 2015     jmarimla         Added summary fields in suitescript details
 * 4.00       06 Aug 2015     rwong            Added class url to links
 *                                             Renamed "View All Logs" to "View Logs"
 *                                             Filter Panel is collapsed on load
 * 5.00       25 Aug 2015     jmarimla         Create title toolbar; Added script id field for compid mode
 * 6.00       04 Sep 2015     rwong            Rename suitelet settings to Customer Debug Settings
 *
 */

Ext4.define('PSGP.APM.SSA.Component.MainPanel', {
    extend: 'PSGP.APM.Component.Container',
    id: 'psgp-apm-ssa-mainpanel',
    params: {
        sdatetime: '',
        edatetime: '',
        scripttype: '',
        scriptid: ''
    },
    minWidth: 1024,
    listeners: {
        beforerender: function () {
            var params = this.params;

            //Enable CompId Mode
            if (COMPID_MODE == 'T') {
                Ext4.getCmp('psgp-apm-ssa-btn-suiteletsettings').show();

                if (COMP_FIL) {
                    PSGP.APM.SSA.dataStores.suiteScriptSummaryData.filterBy(function (record, id) {
                        if (id != 'errorCount') return true;
                        else return false;
                    }, this);
                    Ext4.getCmp('psgp-apm-ssa-container-filters-scriptname').hide();
                    Ext4.getCmp('psgp-apm-ssa-container-filters-scriptid').show();
                }
            }

            if(params.fparam == true) {
                //set filter fields from parameters
                Ext4.getCmp('psgp-apm-ssa-filters-date-startdate').setValue(params.sdate);
                Ext4.getCmp('psgp-apm-ssa-filters-date-enddate').setValue(params.edate);
                Ext4.getCmp('psgp-apm-ssa-filters-time-starttime').setValue(params.stime);
                Ext4.getCmp('psgp-apm-ssa-filters-time-endtime').setValue(params.etime);
                Ext4.getCmp('psgp-apm-ssa-filters-scripttype').setValue(params.scripttype);
                if ((COMPID_MODE == 'T') && (COMP_FIL)) {
                    Ext4.getCmp('psgp-apm-ssa-filters-scriptid').setValue(params.scriptid);
                } else {
                    Ext4.getCmp('psgp-apm-ssa-filters-scriptname').setValue(params.scriptid);
                }
            } else {
                var today = new Date();
                var tomorrow = new Date(today.getTime() + 24*60*60*1000);
                Ext4.getCmp('psgp-apm-ssa-filters-date-startdate').setValue(today);
                Ext4.getCmp('psgp-apm-ssa-filters-date-enddate').setValue(tomorrow);
                Ext4.getCmp('psgp-apm-ssa-filters-time-starttime').setValue('12:00 AM');
                Ext4.getCmp('psgp-apm-ssa-filters-time-endtime').setValue('12:00 AM');
            }

        }
    },
    items: [
        Ext4.create('PSGP.APM.Component.PageToolbar', {
            items: [
                 Ext4.create('PSGP.APM.Component.PageTitle', {
                     value: 'SuiteScript Analysis'
                 }),
                 '->',
                 Ext4.create('PSGP.APM.Component.PageToolbarButton', {
                     id: 'psgp-apm-ssa-btn-suiteletsettings',
                     text: 'Customer Debug Settings',
                     margin: '0 50 0 0',
                     targetMenu: 'psgp-apm-ssa-quicksel-compid',
                     hidden: true
                 }),
            ]
        }),
        Ext4.create('PSGP.APM.SSA.Component.BlueButton.Search', {
            id: 'psgp-apm-ssa-btn-search',
            margin: '5 30 15 30'
        }),
        Ext4.create('PSGP.APM.Component.FiltersPanel', {
            id: 'psgp-apm-ssa-panel-filters',
            margin: '0 30 10 30',
            collapsed: true,
            items: [
                {
                    xtype: 'container',
                    width: 270,
                    border: false,
                    align: 'left',
                    items: [
                            Ext4.create('PSGP.APM.Component.Display', {
                                fieldLabel: 'Start Date/Time'
                            }),
                            {
                                xtype: 'container',
                                layout: 'hbox',
                                items: [
                                        Ext4.create('PSGP.APM.Component.Date', {
                                            id: 'psgp-apm-ssa-filters-date-startdate'
                                        }),
                                        Ext4.create('PSGP.APM.Component.DateSeparator'),
                                        Ext4.create('PSGP.APM.Component.Time', {
                                            id: 'psgp-apm-ssa-filters-time-starttime'
                                        })
                                        ]
                            }
                            ]
                },
                {
                    xtype: 'container',
                    width: 270,
                    border: false,
                    align: 'left',
                    items: [
                            Ext4.create('PSGP.APM.Component.Display', {
                                fieldLabel: 'End Date/Time'
                            }),
                            {
                                xtype: 'container',
                                layout: 'hbox',
                                items: [
                                        Ext4.create('PSGP.APM.Component.Date', {
                                            id: 'psgp-apm-ssa-filters-date-enddate'
                                        }),
                                        Ext4.create('PSGP.APM.Component.DateSeparator'),
                                        Ext4.create('PSGP.APM.Component.Time', {
                                            id: 'psgp-apm-ssa-filters-time-endtime'
                                        })
                                        ]
                            }
                            ]
                },
                {
                    xtype: 'container',
                    width: 240,
                    border: false,
                    align: 'left',
                    items: [
                            Ext4.create('PSGP.APM.Component.Display', {
                                fieldLabel: 'Script Type'
                            }),
                            Ext4.create('PSGP.APM.SSA.Component.ComboBox.ScriptType', {
                                id: 'psgp-apm-ssa-filters-scripttype'
                            })
                            ]
                },
                {
                    xtype: 'container',
                    id: 'psgp-apm-ssa-container-filters-scriptname',
                    width: 290,
                    border: false,
                    align: 'left',
                    items: [
                            Ext4.create('PSGP.APM.Component.Display', {
                                fieldLabel: 'Script Name'
                            }),
                            Ext4.create('PSGP.APM.SSA.Component.ComboBox.ScriptName', {
                                id: 'psgp-apm-ssa-filters-scriptname'
                            })
                            ]
                },
                {
                    xtype: 'container',
                    id: 'psgp-apm-ssa-container-filters-scriptid',
                    hidden: true,
                    width: 110,
                    border: false,
                    align: 'left',
                    items: [
                            Ext4.create('PSGP.APM.Component.Display', {
                                fieldLabel: 'Script ID'
                            }),
                            Ext4.create('PSGP.APM.Component.NumberField', {
                                id: 'psgp-apm-ssa-filters-scriptid'
                            })
                            ]
                }
            ]
        }),
        {
            xtype: 'container',
            border: false,
            layout: 'column',
            items: [
                Ext4.create('PSGP.APM.Component.EmptyPanel', {
                    margin: '0 5 10 30',
                    columnWidth: .75,
                    items: [
                        Ext4.create('PSGP.APM.Component.PortletPanel', {
                            id: 'psgp-apm-ssa-portlet-performancechart',
                            title: 'Performance Chart',
                            height: 500,
                            items: [
                                {
                                    xtype: 'container',
                                    id: 'psgp-apm-ssa-container-perfchart-nodata',
                                    layout: 'hbox',
                                    items: [
                                            Ext4.create('Ext4.panel.Panel', {
                                                id: 'psgp-apm-ssa-chart-nodata-icon',
                                                cls: 'apm-suitescriptdetail-chart-warning',
                                                height: 30,
                                                width: 32,
                                                border: false,
                                                margin: '10 0 0 10'
                                            }),
                                            Ext4.create('Ext4.form.Label', {
                                                id: 'psgp-apm-ssa-chart-nodata-text',
                                                flex: 1,
                                                border: false,
                                                margin: '10 0 0 10',
                                                text: 'No data available.'
                                            })
                                    ]
                                },
                                {
                                    xtype: 'container',
                                    border: false,
                                    layout: 'fit',
                                    height: 470,
                                    id: 'psgp-apm-ssa-container-perfchart',
                                }
                            ]
                        })
                    ]
                }),
                Ext4.create('PSGP.APM.Component.EmptyPanel', {
                    margin: '0 30 10 5',
                    columnWidth: .25,
                    items: [
                        Ext4.create('PSGP.APM.Component.PortletPanel', {
                            id: 'psgp-apm-ssa-portlet-suitescriptdetails',
                            title: 'Suitescript Details',
                            items: [
                                {
                                    xtype: 'container',
                                    id: 'psgp-apm-ssa-container-suitescriptdetails',
                                    width: '100%',
                                    layout: {
                                        type: 'vbox',
                                        align: 'center'
                                    },
                                    items: [
                                        {
                                            xtype: 'container',
                                            width: '100%',
                                            margin: '20 20 0 20',
                                            layout: {
                                                type: 'vbox',
                                                align: 'left'
                                            },
                                            items: [
                                                Ext4.create('PSGP.APM.Component.SummaryField', {
                                                    id: 'psgp-apm-ssa-display-summary-scriptname',
                                                    fieldLabel: 'Script Name',
                                                    width: '100%',
                                                    value: '-',
                                                    margin: '0 0 10 0'
                                                }),
                                                Ext4.create('PSGP.APM.Component.SummaryField', {
                                                    id: 'psgp-apm-ssa-display-summary-scripttype',
                                                    fieldLabel: 'Script Type',
                                                    width: '100%',
                                                    value: '-',
                                                    margin: '0 0 10 0'
                                                }),
                                                {
                                                    xtype: 'container',
                                                    width: '100%',
                                                    layout: {
                                                        type: 'hbox'
                                                    },
                                                    items: [
                                                        Ext4.create('PSGP.APM.Component.SummaryField', {
                                                            id: 'psgp-apm-ssa-display-summary-fromdate',
                                                            fieldLabel: 'From',
                                                            flex: 1,
                                                            value: '-'
                                                        }),
                                                        Ext4.create('PSGP.APM.Component.SummaryField', {
                                                            id: 'psgp-apm-ssa-display-summary-todate',
                                                            fieldLabel: 'To',
                                                            flex: 1,
                                                            value: '-'
                                                        })
                                                    ]
                                                }
                                            ]
                                        },
                                        Ext4.create('PSGP.APM.SSA.Component.Grid.SuitescriptDetails', {
                                            id: 'psgp-apm-ssa-grid-suitescriptdetails',
                                            margin: '20 20 20 20',
                                            width: '100%'
                                        }),
                                        Ext4.create('PSGP.APM.Component.GrayButton', {
                                            id: 'psgp-apm-ssa-btn-viewalllogs',
                                            text: 'View Logs',
                                            margin: '0, 20, 20, 20',
                                            height: 26,
                                            handler: function() {
                                                PSGP.APM.SSA.dataStores.suiteScriptDetailData.loadPage(1);
                                                Ext4.getCmp('psgp-apm-ssa-window-ssd').show();
                                            }
                                        })
                                    ]
                                }
                            ]
                        })
                    ]
                })
            ]
        }
    ]
});
