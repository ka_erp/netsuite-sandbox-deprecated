/**
 * � 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var FAM;
if (!FAM) { FAM = {}; }

FAM.AssetTransfer_CS = new function () {
	this.alertMessage = {};
	
    /**
     * pageInit event type of client scripts
    **/
    this.pageInit = function (type) {
        this.alertMessage = FAM.Util_CS.fetchMessageObj({
            EXTJS_MESSAGE_TITLE : 'extjs_message_title',
            EXTJS_PROCESSING : 'extjs_processing',
            EXTJS_READING_ASSET : 'extjs_reading_asset',
            EXTJS_CLEARING_DATA : 'extjs_clearing_data',
            EXTJS_LOADING_DATA : 'extjs_loading_data',
            EXTJS_REFRESHING_DATA : 'extjs_refreshing_data',
            CONFIRM_SUBSIDIARYBOOKID : 'client_assettransfer_subsidiarybookid',
            CONFIRM_SUBSIDIARYMETHOD : 'client_assettransfer_subsidiarymethod',
            ALERT_EMPTYSUBATFIELD : 'client_assettransfer_emptysubat',
            ALERT_NOCHANGE : 'client_assettransfer_nochange',
            ALERT_NOTRANSFERACCT : 'client_assettransfer_notransferacct',
            ALERT_BGPEXISTS : 'client_assettransfer_bgpexists',
            ALERT_SUBDEPT : 'client_subdept_notmatch',
            ALERT_SUBLOC : 'client_subloc_notmatch',
            ALERT_SUBCLASS : 'client_subclass_notmatch',
            ALERT_AT_ASSETACCT : 'client_assettransfer_atassetacct',
            ALERT_AT_DEPRACCT : 'client_assettransfer_atdepracct',
            ALERT_AT_CDL_MAND : 'client_assettransfer_cdl_mandatory',
            ALERT_BULKBGPEXISTS : 'client_assettransfer_bulkbgpexists',
            ALERT_WARNBGPEXISTS : 'client_assettransfer_warnbgpexists'
        });
        
        if (!nlapiGetFieldValue('custpage_assetid')) {
            this.disableFields(true);
        };
        
        // Check if an active Bulk Asset Transfer BGP instance exists
        if(FAM.Util_CS.hasExistingBulkTransfer()){
            alert(this.alertMessage.ALERT_WARNBGPEXISTS);
        }
    };

    /**
     * saveRecord event type of client scripts
    **/
    this.saveRecord = function () {
        var assetTypeAccounts, bookIds,
            assetId = nlapiGetFieldValue('custpage_assetid'),
            origType = nlapiGetFieldValue('custpage_assettype'),
            origDept = nlapiGetFieldValue('custpage_assetdepartment'),
            origClass = nlapiGetFieldValue('custpage_assetclass'),
            origLoc = nlapiGetFieldValue('custpage_assetlocation'),
            origSub = nlapiGetFieldValue('custpage_assetsubsidiary');
            newType = nlapiGetFieldValue('custpage_newassettype'),
            newDept = nlapiGetFieldValue('custpage_newdepartment'),
            newClass = nlapiGetFieldValue('custpage_newclass'),
            newLoc = nlapiGetFieldValue('custpage_newlocation'),
            newSub = nlapiGetFieldValue('custpage_newsubsidiary');
        
            
        //Asset Type must not be null
        if (!newType) {
            alert(this.alertMessage.ALERT_EMPTYSUBATFIELD);
            return false;
        }
        
        // Check if all values are the same as the original
        if (!this.changedField('custpage_newsubsidiary') &&
            !this.changedField('custpage_newassettype') &&
            !this.changedField('custpage_newclass') &&
            !this.changedField('custpage_newdepartment') &&
            !this.changedField('custpage_newlocation')) {
            
            alert(this.alertMessage.ALERT_NOCHANGE);
            return false;
        }
        
        // Check if Asset Transfer BGP instance for this asset already exists
        if (this.isAssetToBeTransferred(assetId)) {
            alert(this.alertMessage.ALERT_BGPEXISTS);
            return false;
        }
        
        // Check if an active Bulk Asset Transfer BGP instance exists
        if(FAM.Util_CS.hasExistingBulkTransfer()){
            alert(this.alertMessage.ALERT_BULKBGPEXISTS);
            return false;
        }
        
        // check Asset Type Asset Account and Depreciation Account
        if (origType !== newType) {
            assetTypeAccounts = nlapiLookupField('customrecord_ncfar_assettype', newType,
                ['custrecord_assettypeassetacc', 'custrecord_assettypedepracc']);
            
            if (!assetTypeAccounts.custrecord_assettypeassetacc) {
                alert(this.alertMessage.ALERT_AT_ASSETACCT);
                return false;
            }
            
            if (!assetTypeAccounts.custrecord_assettypedepracc) {
                alert(this.alertMessage.ALERT_AT_DEPRACCT);
                return false;
            }
        }
        
        if (FAM.Context.blnOneWorld) {
            //Subsidiary must not be null
            if (!newSub) {
                alert(this.alertMessage.ALERT_EMPTYSUBATFIELD);
                return false;
            }
            
            // Check C/D/L + Subsidiary Combination Validation
            if((!newDept && FAM.Context.getPreference('deptmandatory') === 'T')||
               (!newClass && FAM.Context.getPreference('classmandatory') === 'T')||
               (!newLoc && FAM.Context.getPreference('locmandatory') === 'T')){
                
                alert(this.alertMessage.ALERT_AT_CDL_MAND);
                return false;
            }
            
            if ((origSub !== newSub || newDept !== origDept) && newDept &&
                !FAM.compareCDLSubsidiary('department', newDept, newSub)) {
                
                alert(this.alertMessage.ALERT_SUBDEPT);
                return false;
            }
            
            if ((origSub !== newSub || newClass !== origClass) && newClass &&
                !FAM.compareCDLSubsidiary('classification', newClass, newSub)) {
                
                alert(this.alertMessage.ALERT_SUBCLASS);
                return false;
            }
            
            if ((origSub !== newSub || newLoc !== origLoc) && newLoc &&
                    !FAM.compareCDLSubsidiary('location', newLoc, newSub)) {
                    
                alert(this.alertMessage.ALERT_SUBLOC);
                return false;
            }
            
            if (origSub !== newSub) {
                // Check if Transfer Account exists
                if (!FAM.Util_Shared.getTransferAccounts(origSub, newSub)) {
                    alert(this.alertMessage.ALERT_NOTRANSFERACCT);
                    return false;
                }
                
                // Check Subsidiary + Accounting Books combination
                bookIds = FAM.Util_Shared.getTaxMethodBookIds(assetId);
                if (!FAM.Util_Shared.isValidSubsidiaryBooks(newSub, bookIds)) {
                    if (!confirm(this.alertMessage.CONFIRM_SUBSIDIARYBOOKID)) {
                        return false;
                    }
                }
                
                // Check Subsidiary + Alternate Methods combination
                if (!FAM.Util_Shared.compatibleSubAndMethods(assetId, newSub)) {
                    if (!confirm(this.alertMessage.CONFIRM_SUBSIDIARYMETHOD)) {
                        return false;
                    }
                }
            }
        }
        
    	return true;
    };

    /**
     * fieldChanged event type of client scripts
    **/
    this.fieldChanged = function (type, value) {
        var fld, disableSCDL, disableAT, dispField, progress, fldCount, pBreakdown, assetId, assetRec;
        
        if (value == 'custpage_assetid') {
            try {
                progress = 0;
                pBreakdown = { loading : 0.1, reading : 0.8, refresh : 0.1 };
                dispField = {
                    'custpage_assettype'        : 'custrecord_assettype',
                    'custpage_assetsubsidiary'  : 'custrecord_assetsubsidiary',
                    'custpage_assetclass'       : 'custrecord_assetclass',
                    'custpage_assetdepartment'  : 'custrecord_assetdepartment',
                    'custpage_assetlocation'    : 'custrecord_assetlocation',
                    'custpage_assetdescription' : 'custrecord_assetdescr',
                    'custpage_assetserialno'    : 'custrecord_assetserialno',
                    'custpage_assetalternateno' : 'custrecord_assetalternateno',
                    'custpage_assetorigcost'    : 'custrecord_assetcost',
                    'custpage_assetcurrcost'    : 'custrecord_assetcurrentcost',
                    'custpage_assetlifetime'    : 'custrecord_assetlifetime',
                    'custpage_assetlastdepr'    : 'custrecord_assetcurrentage',
                    'custpage_assetstatus'      : 'custrecord_assetstatus',
                    'custpage_assetcurrency'    : 'custrecord_assetcurrency',
                    'custpage_newassettype'     : 'custrecord_assettype',
                    'custpage_newsubsidiary'    : 'custrecord_assetsubsidiary',
                    'custpage_newclass'         : 'custrecord_assetclass',
                    'custpage_newdepartment'    : 'custrecord_assetdepartment',
                    'custpage_newlocation'      : 'custrecord_assetlocation'
                };
                
                fldCount = Object.keys(dispField).length;
                
                Ext.MessageBox.minWidth = 150;
                Ext.MessageBox.show({
                    title : this.alertMessage.EXTJS_MESSAGE_TITLE,
                    msg : this.alertMessage.EXTJS_PROCESSING,
                    progress : true
                });
                
                Ext.MessageBox.updateProgress(progress, this.alertMessage.EXTJS_CLEARING_DATA);
                
                for (fld in dispField) {
                    nlapiSetFieldValue(fld, '', false);
                }
                
                progress += pBreakdown.loading;
                Ext.MessageBox.updateProgress(progress, this.alertMessage.EXTJS_LOADING_DATA);

                assetId = nlapiGetFieldValue('custpage_assetid');
                if (assetId) {
                    assetRec = nlapiLoadRecord('customrecord_ncfar_asset', assetId);
                    
                    for (fld in dispField) {
                        progress += (pBreakdown.reading / fldCount);
                        Ext.MessageBox.updateProgress(progress, this.alertMessage.EXTJS_READING_ASSET);
                        nlapiSetFieldValue(fld, assetRec.getFieldValue(dispField[fld]), false);
                    }
                    
                    this.disableFields(false);
                }
                else {
                    this.disableFields(true);
                }
                
                Ext.MessageBox.updateProgress(progress + pBreakdown.refresh,
                    this.alertMessage.EXTJS_REFRESHING_DATA);
                Ext.MessageBox.hide();
            }
            catch (e) {
                this.disableFields(true);
                nlapiLogExecution('ERROR', 'Field Change', e.toString());
                Ext.MessageBox.hide();
                throw e;
            }
        }
        else if (value == 'custpage_newsubsidiary' ||
                value == 'custpage_newclass' ||
                value == 'custpage_newdepartment' ||
                value == 'custpage_newslocation') {
            disableAT = this.changedField('custpage_newsubsidiary') ||
                        this.changedField('custpage_newclass') ||
                        this.changedField('custpage_newdepartment') ||
                        this.changedField('custpage_newlocation');
            
            nlapiDisableField('custpage_newassettype', disableAT);
        }
        else if (value == 'custpage_newassettype') {
            disableSCDL = this.changedField('custpage_newassettype');
            this.disableSCDLFields(disableSCDL);
        }
    };

    this.disableFields = function (disable) {
        nlapiDisableField('custpage_newassettype', disable);
        nlapiDisableField('custpage_newsubsidiary', disable);
        nlapiDisableField('custpage_newclass', disable);
        nlapiDisableField('custpage_newdepartment', disable);
        nlapiDisableField('custpage_newlocation', disable);
    };

    this.disableSCDLFields = function (disable) {
        nlapiDisableField('custpage_newsubsidiary', disable);
        nlapiDisableField('custpage_newclass', disable);
        nlapiDisableField('custpage_newdepartment', disable);
        nlapiDisableField('custpage_newlocation', disable);
    };
    
    /**
     * If SCDL are all empty, enable the Asset Type dropdown field.
     * Otherwise, disable the Asset Type dropdown.
     *
     * Parameters:
     *     none
     * Returns:
     *     null
    **/
    this.toggleAssetTypeField = function () {
    	var newSubsidiary = nlapiGetFieldValue('custpage_newsubsidiary'),
            newClass = nlapiGetFieldValue('custpage_newclass'),
            newDepartment = nlapiGetFieldValue('custpage_newdepartment'),
            newLocation = nlapiGetFieldValue('custpage_newlocation'),
            disableFlag = newSubsidiary || newClass || newDepartment || newLocation ? true : false;
    		
        nlapiDisableField('custpage_newassettype', disableFlag);
    };
    
    /**
     * Checks if there is a BG - Process Instance record where Process Name: 'Asset Transfer',
     * Process Status: 'Queued' or 'In Progress' and the Asset Id (from Process State) is the Asset
     * Id on the form.
     *
     * Parameters:
     *     assetId {number} - internal id of the asset     
     * Returns:
     *     boolean
    **/
    this.isAssetToBeTransferred = function (assetId) {
    	if (!assetId) {
    		return false;
    	}

        var fil = [
            new nlobjSearchFilter('custrecord_far_proins_functionname', null, 'is',
                'famTransferAsset'),
            new nlobjSearchFilter('custrecord_far_proins_recordid', null, 'equalto', assetId),
            new nlobjSearchFilter('custrecord_far_proins_procstatus', null, 'anyof',
                [FAM.BGProcessStatus.InProgress, FAM.BGProcessStatus.Queued]),
            new nlobjSearchFilter('isinactive', null, 'is', 'F')
        ];

        if (nlapiSearchRecord('customrecord_bg_procinstance', null, fil)) {
            return true;
        }

    	return false;
    };
    
    this.changedField = function (fld) {
        var fieldMap = {'custpage_newassettype'     : 'custpage_assettype',
                        'custpage_newsubsidiary'    : 'custpage_assetsubsidiary',
                        'custpage_newclass'         : 'custpage_assetclass',
                        'custpage_newdepartment'    : 'custpage_assetdepartment',
                        'custpage_newlocation'      : 'custpage_assetlocation'};
        return nlapiGetFieldValue(fld) != nlapiGetFieldValue(fieldMap[fld]);
    };
};