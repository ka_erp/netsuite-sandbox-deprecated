/**
 * � 2014 NetSuite Inc.
 * User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
**/

var FAM;
if (!FAM) { FAM = {}; }

/**
 * Starter for Background Processing function for Asset Transfer
 *
 * Parameters:
 *     BGP {FAM.BGProcess} - Process Instance Record for this background process
 * Returns:
 *     true {boolean} - processing should be requeued
 *     false {boolean} - processing should not be requeued; essentially setting the deployment to
 *                       standby
**/

function famTransferAsset(BGP) {
    var assetTransfer = new FAM.AssetTransfer(BGP);

    try {
        assetTransfer.run();
        return true;
    }
    catch (e) {
        assetTransfer.logObj.printLog();
        throw e;
    }
}

FAM.AssetTransfer = function (procInsRec) {
    this.procInsRec = procInsRec;
    this._rollbackRecords = [];
    this.recCoreValue = [];
    this.oldVal = {};
    this.newVal = {};
    this.primaryBook = FAM.Util_Shared.getPrimaryBookId();
    this.intCurrSym = FAM.SystemSetup.getSetting('nonDecimalCurrencySymbols');
    this.trnDate = nlapiDateToString(new Date());
    this.totalRecords = +this.procInsRec.getFieldValue('rec_total') || 0;
    this.recsProcessed = +this.procInsRec.getFieldValue('rec_count') || 0;
    this.recsFailed = +this.procInsRec.getFieldValue('rec_failed') || 0;
    this.userId = this.procInsRec.getFieldValue('user');
    this.logObj = new printLogObj('debug');
    this.subCache = new FAM.FieldCache('subsidiary');
    this.atCache = new FAM.FieldCache('customrecord_ncfar_assettype');
    this.perfTimer = new FAM.Timer();
    this.objAssetTransfer = {};
    this.wError = this.procInsRec.getScriptParam('upperLimit')||false;
    this.perfTimer.start();
};

FAM.AssetTransfer.prototype.execLimit = 500;
FAM.AssetTransfer.prototype.timeLimit = 30 * 60 * 1000; // 30 minutes

FAM.AssetTransfer.prototype.run = function () {
    this.logObj.startMethod('FAM.AssetTransfer.run');
    var blnRequeue;
    
    if(this.procInsRec.getFieldValue('recordid')==-1){
        blnRequeue = this.bulkTransferAsset();
    }
    else{
        this.initObjTransfer();
        this.totalRecords = 1;
        blnRequeue = this.transferAsset();
        if(!blnRequeue){
            this.recsProcessed++;
        }
    }

    this.updProcIns(blnRequeue);
    this.logObj.endMethod();
};


FAM.AssetTransfer.prototype.bulkTransferAsset = function () {
    var lastAcctMethId = this.recsProcessed,
        stateVal = this.procInsRec.stateValues,
        arrAssetTransfer = JSON.parse(stateVal.transferVals),
        blnRequeue = false;

    this.totalRecords = arrAssetTransfer.length;

    for(var x = lastAcctMethId; x < arrAssetTransfer.length; x++){
        //re-initialize global variables for each asset in array
        this._rollbackRecords = [];
        this.recCoreValue = [];
        this.oldVal = {};
        this.newVal = {};
        this.trnDate = nlapiDateToString(new Date());
        this.initObjTransfer(arrAssetTransfer[x]);
        try{
            blnRequeue = this.transferAsset();
            if(blnRequeue){
                break;
            }
            this.recsProcessed++;
        }
        catch(e){
            //do nothing, just catch the error from single transfer and continue with the loop
        }
        
    }

    return blnRequeue;
    
};

FAM.AssetTransfer.prototype.initObjTransfer = function (objAsset) {
    this.objAssetTransfer = {};
    if(objAsset){
        this.objAssetTransfer.AssetId = objAsset.id;
        this.objAssetTransfer.AssetType = (objAsset.a||'')+'';
        this.objAssetTransfer.Subsidiary = (objAsset.s||'')+'';
        this.objAssetTransfer.Class = (objAsset.c||'')+'';
        this.objAssetTransfer.Department = (objAsset.d||'')+'';
        this.objAssetTransfer.Location = (objAsset.l||'')+'';
    }else{
        this.objAssetTransfer.AssetId = this.procInsRec.getFieldValue('recordid');
        this.objAssetTransfer.AssetType = this.procInsRec.stateValues.AssetType||'';
        this.objAssetTransfer.Subsidiary = this.procInsRec.stateValues.Subsidiary||'';
        this.objAssetTransfer.Class = this.procInsRec.stateValues.Class||'';
        this.objAssetTransfer.Department = this.procInsRec.stateValues.Department||'';
        this.objAssetTransfer.Location = this.procInsRec.stateValues.Location||'';
    }
    
};

FAM.AssetTransfer.prototype.transferAsset = function () {
    this.logObj.startMethod('FAM.AssetTransfer.transferAsset');
    
    var oldAssetRec, updateRec, taxSearched,
        lastTaxMethId = +this.procInsRec.getScriptParam('lowerLimit') || 0,
        postingNeeded = false, 
        interCompanyPosting = false,
        assetRec = new FAM.Asset_Record(),
        srcAcct = '',
        desAcct = '',
        msg = '',
        blnToRequeue = false;

    assetRec.recordId = this.objAssetTransfer.AssetId;
    oldAssetRec = assetRec.lookupField([
        'custrecord_assetsubsidiary',
        'custrecord_assetsubsidiary.isinactive',
        'custrecord_assettype',
        'custrecord_assettype.isinactive',
        'custrecord_assetclass',
        'custrecord_assetclass.isinactive',
        'custrecord_assetdepartment',
        'custrecord_assetdepartment.isinactive',
        'custrecord_assetlocation',
        'custrecord_assetlocation.isinactive',
        'custrecord_assetcost',
        'custrecord_assetcurrentcost',
        'custrecord_assetdeprtodate',
        'custrecord_assetmainacc',
        'custrecord_assetdepracc',
        'custrecord_ncfar_quantity',
        'custrecord_assetresidualvalue',
        'custrecord_assetbookvalue',
        'custrecord_assetlastdepramt',
        'custrecord_assetpriornbv'
    ]);
    if (!oldAssetRec) {
        throw nlapiCreateError('FAM_TRANSFER_INVALID_ASSET', FAM.resourceManager.GetString(
            'custpage_transfer_asset_undefined', null, null, [this.objAssetTransfer.AssetId]));
    }

    this.oldVal.asset_type = oldAssetRec.custrecord_assettype;
    this.oldVal.subsidiary = oldAssetRec.custrecord_assetsubsidiary;
    this.oldVal.classfld = oldAssetRec['custrecord_assetclass.isinactive'] === 'T' ? null :
        oldAssetRec.custrecord_assetclass;
    this.oldVal.department = oldAssetRec['custrecord_assetdepartment.isinactive'] === 'T' ? null :
        oldAssetRec.custrecord_assetdepartment;
    this.oldVal.location = oldAssetRec['custrecord_assetlocation.isinactive'] === 'T' ? null :
        oldAssetRec.custrecord_assetlocation;
    
    //collate changed fields
    if (this.objAssetTransfer.AssetType && (lastTaxMethId > 0 ||
        this.objAssetTransfer.AssetType != this.oldVal.asset_type)) {
        
        this.newVal.asset_type = this.objAssetTransfer.AssetType;
    }
    if (this.objAssetTransfer.Subsidiary && (lastTaxMethId > 0 ||
        this.objAssetTransfer.Subsidiary != this.oldVal.subsidiary)) {
        
        this.newVal.subsidiary = this.objAssetTransfer.Subsidiary;
    }
    if (this.objAssetTransfer.Class &&  (lastTaxMethId > 0 ||
        this.objAssetTransfer.Class != this.oldVal.classfld)) {
        
        this.newVal.classfld = this.objAssetTransfer.Class;
    }
    if (this.objAssetTransfer.Department && (lastTaxMethId > 0 ||
        this.objAssetTransfer.Department != this.oldVal.department)) {
        
        this.newVal.department = this.objAssetTransfer.Department;
    }
    if (this.objAssetTransfer.Location && (lastTaxMethId > 0 ||
        this.objAssetTransfer.Location != this.oldVal.location)) {
        
        this.newVal.location = this.objAssetTransfer.Location;
    }
    
    if (this.newVal.subsidiary) {
        postingNeeded = true;
        interCompanyPosting = true;
        
        var xferAcctSearch = new FAM.Search(new FAM.TransferAccount());
        
        xferAcctSearch.addFilter('origin_subsidiary', null, 'is',  this.oldVal.subsidiary);
        xferAcctSearch.addFilter('destination_subsidiary', null, 'is', this.newVal.subsidiary);
        
        xferAcctSearch.addColumn('origin_account');
        xferAcctSearch.addColumn('destination_account');
        
        xferAcctSearch.run();
        
        if(xferAcctSearch.results && xferAcctSearch.results.length == 1){
            srcAcct = xferAcctSearch.getValue(0, 'origin_account'); 
            desAcct = xferAcctSearch.getValue(0, 'destination_account');
        }
        else {
            throw nlapiCreateError('NCFAR_TRANSFERSETUP', FAM.resourceManager.GetString(
                'custpage_transfer_account_missing', null, null,
                [this.oldVal.subsidiary, this.newVal.subsidiary]));
        }
    }
    // For CDL transfer, do not write DHR/JE if either old or new value is blank
    else if ((this.oldVal.classfld && this.newVal.classfld && this.newVal.classfld != 'unset' &&
            FAM.SystemSetup.getSetting('isPostClass') === 'T') ||
        (this.oldVal.department && this.newVal.department && this.newVal.department != 'unset' &&
            FAM.SystemSetup.getSetting('isPostDepartment') === 'T') || 
        (this.oldVal.location && this.newVal.location && this.newVal.location != 'unset' &&
            FAM.SystemSetup.getSetting('isPostLocation') === 'T') ||
        this.objAssetTransfer.AssetType) {
        
        postingNeeded = true;
    }
    
    if (postingNeeded) {
        do {
            if (lastTaxMethId === 0) {
                //fetch Asset Record Data to recCoreValue array
                this.fetchAssetRecordData(oldAssetRec);
            }
            if (this.totalRecords > 0) {
                //add tax method data to recCoreValue array
                taxSearched = this.fetchTaxMethodData(oldAssetRec.custrecord_ncfar_quantity,
                    lastTaxMethId);
            }
            
            this.logObj.pushMsg('Process all records for transfer');
            for (var i = 0; i < this.recCoreValue.length; i++) {
                if (this.hasExceededLimit()) {
                    blnToRequeue = true;
                    break;
                }
                
                var bSkipTaxMethod = false, newBookVal, recValue = {};
                this.logObj.pushMsg('Processing Asset Id: ' + this.objAssetTransfer.AssetId);
                if (this.recCoreValue[i].altDep) {
                    this.logObj.pushMsg('Tax Method Id: ' + this.recCoreValue[i].altDep);
                }
                try {
                    if (!interCompanyPosting) { //CDL Transfer

                        // For CDL transfer, write DHR/JE only when book id is present.
                        // Asset type for now, maybe others also in the future, 
                        // will use this part, we'll check if it's a CDL transfer(this part is definite).
                        if ((this.newVal.department || this.newVal.classfld || this.newVal.location) 
                                && !this.recCoreValue[i].bookId) {
                           this.logObj.logExecution('Not writing DHR/JE for CDL transfer w/out book id.');
                           continue; 
                        }

                        if (((FAM.Context.getPreference('deptsperline') === 'F' && this.newVal.department) ||
                                (FAM.Context.getPreference('classesperline') === 'F' && this.newVal.classfld) ||
                                (FAM.Context.getPreference('locsperline') === 'F' && this.newVal.location))) { //single entry CDL
                            
                            //out
                            recValue.costType = 1; //costing schema 1
                            recValue.assetId    = this.objAssetTransfer.AssetId;
                            recValue.bookId                = this.recCoreValue[i].bookId;
                        
                            recValue.accts                 = [this.recCoreValue[i].oMainAcct,
                                                              this.recCoreValue[i].oDeprAcct,
                                                              this.recCoreValue[i].oMainAcct];
                            recValue.subsidiary            = this.recCoreValue[i].subsidiary;
                            recValue.asset_type            = this.oldVal.asset_type;
                            recValue.classfld              = this.oldVal.classfld;
                            recValue.department            = this.oldVal.department;
                            recValue.location              = this.oldVal.location;
                            recValue.currCost              = this.recCoreValue[i].currCost;
                            recValue.deprToDate            = this.recCoreValue[i].deprCost;
                            recValue.quantity              = this.recCoreValue[i].qty;
                            recValue.acqTransaction_amount = this.recCoreValue[i].currCost * -1;
                            recValue.acqNet_book_value     = 0;
                            recValue.depTransaction_amount = -this.recCoreValue[i].deprCost; 
                            recValue.depNet_book_value     = 0;
                            if(this.recCoreValue[i].altDep)
                                recValue.altDep = this.recCoreValue[i].altDep;
                            recValue.refs                  = ['Asset Transfer Out (FAM)', 'Asset Transfer Out (FAM)', 'Asset Transfer Out (FAM)']; 
                            recValue.failurePoint          = 'CDL/Asset Type Transfer Out';

                            //in
                            if(this.recCoreValue[i].nMainAcct && this.recCoreValue[i].nDeprAcct){
                                recValue.costType = 2; //costing schema 1
                                recValue.accts                 = [this.recCoreValue[i].nMainAcct,
                                                                  this.recCoreValue[i].nDeprAcct,
                                                                  this.recCoreValue[i].nMainAcct];
                                recValue.asset_type            = this.oldVal.asset_type;
                                recValue.classfld              = this.newVal.classfld;
                                recValue.department            = this.newVal.department;
                                recValue.location              = this.newVal.location;
                                recValue.currCost              = this.recCoreValue[i].currCost;
                                recValue.acqNet_book_value     = this.recCoreValue[i].currCost;
                                recValue.depTransaction_amount = this.recCoreValue[i].deprCost; 
                                recValue.depNet_book_value     = this.recCoreValue[i].currCost - this.recCoreValue[i].deprCost;
                                recValue.refs                  = ['Asset Transfer In (FAM)', 'Asset Transfer In (FAM)', 'Asset Transfer In (FAM)']; 
                                recValue.failurePoint          = 'CDL/Asset Type Transfer In';
                                
                                this.writeJournalandHistory(recValue);
                            }
                        }
                        else { //per line CDL transfer/Asset Type transfer
                            //create DHR/JE only if there are valid accounts and asset type is valid
                            if(this.recCoreValue[i].nMainAcct && this.recCoreValue[i].nDeprAcct) {
                                var isOldTypeActive;
                                if (this.newVal.asset_type) {
                                    isOldTypeActive = oldAssetRec['custrecord_assettype.isinactive'] == 'T';
                                }
                                //out
                                recValue.costType = 3; //costing schema 3
                                recValue.assetId    = this.objAssetTransfer.AssetId;
                                recValue.accts                 = [this.recCoreValue[i].oMainAcct,
                                                                  this.recCoreValue[i].oDeprAcct,
                                                                  this.recCoreValue[i].nMainAcct,
                                                                  this.recCoreValue[i].nDeprAcct];
                                recValue.subsidiary            = this.recCoreValue[i].subsidiary;
                                recValue.asset_type            = this.oldVal.asset_type;
                                recValue.classfld = !this.newVal.classfld ? 
                                        this.oldVal.classfld + '' : [this.oldVal.classfld + '', this.oldVal.classfld + '', this.newVal.classfld + '', this.newVal.classfld + ''];
                                recValue.department = !this.newVal.department ? 
                                        this.oldVal.department + '' : [this.oldVal.department + '', this.oldVal.department + '', this.newVal.department + '', this.newVal.department + ''];
                                recValue.location = !this.newVal.location ? 
                                        this.oldVal.location + '' : [this.oldVal.location + '', this.oldVal.location + '', this.newVal.location + '', this.newVal.location + ''];
                                recValue.currCost              = this.recCoreValue[i].currCost;
                                recValue.deprToDate            = this.recCoreValue[i].deprCost;
                                recValue.quantity              = this.recCoreValue[i].qty;
                                recValue.acqTransaction_amount = this.recCoreValue[i].currCost * -1; 
                                recValue.acqNet_book_value     = 0;
                                recValue.depTransaction_amount = -this.recCoreValue[i].deprCost; 
                                recValue.depNet_book_value     = 0;
                                if(this.recCoreValue[i].altDep)
                                    recValue.altDep = this.recCoreValue[i].altDep;
                                recValue.bookId = this.recCoreValue[i].bookId;
                                recValue.refs                  = ['Asset Transfer Out (FAM)', 'Asset Transfer Out (FAM)', 'Asset Transfer In (FAM)', 'Asset Transfer In (FAM)']; 
                                recValue.failurePoint          = 'CDL Transfer Out';
                                if(!isOldTypeActive) {
                                    recValue.journalId = this.writeJournalandHistory(recValue); //reuse journal id from cdl transfer out
                                    
                                    //in (should not create journal)
                                    recValue.asset_type = this.newVal.asset_type || this.oldVal.asset_type;
                                    recValue.acqTransaction_amount = this.recCoreValue[i].currCost; 
                                    recValue.acqNet_book_value     = this.recCoreValue[i].currCost;
                                    recValue.depTransaction_amount = this.recCoreValue[i].deprCost; 
                                    recValue.depNet_book_value     = this.recCoreValue[i].currCost - this.recCoreValue[i].deprCost;
                                    recValue.failurePoint          = 'CDL Transfer In';
                                    
                                    this.writeJournalandHistory(recValue);
                                }else{
                                    msg = 'Inactive Old Asset Type. Skipping creation of DHR/Journal Entries.';
                                    if(recValue.altDep){
                                        msg += ' (Tax Method ID: ' + recValue.altDep + ' )';
                                    }
                                    this.procInsRec.writeToProcessLog(msg, 'Error', 'Asset Id: ' + this.objAssetTransfer.AssetId);
                                }
                            }
                        }
                    } 
                    else { //subsidiary transfer
                        if(i && this.recCoreValue[i].bookId && 
                            !FAM.Util_Shared.isValidSubsidiaryBookId(this.newVal.subsidiary, this.recCoreValue[i].bookId)){
                             bSkipTaxMethod = true;
                             throw nlapiCreateError('FAM_INVALID_SUB_BOOK_ID', FAM.resourceManager.GetString('client_assettransfer_invalid_sub_bookid'), true);
                         }
                         if(i && !FAM.Util_Shared.isValidSubsidiaryAltMethod(this.newVal.subsidiary, this.recCoreValue[i].altMeth)){
                             bSkipTaxMethod = true;
                             throw nlapiCreateError('FAM_INVALID_SUB_ALT_METH', FAM.resourceManager.GetString('client_assettransfer_invalid_sub_altmeth'), true);
                         }
                         recValue.costType              = 1; //costing schema 1
                         recValue.assetId               = this.objAssetTransfer.AssetId;
                         recValue.bookId                = this.recCoreValue[i].bookId;
                         recValue.inactiveoldsub        = oldAssetRec['custrecord_assetsubsidiary.isinactive']=='T';
                         //out
                         recValue.accts                 = [this.recCoreValue[i].oMainAcct,
                                                           this.recCoreValue[i].oDeprAcct,
                                                           srcAcct];
                         recValue.subsidiary            = this.recCoreValue[i].subsidiary;
                         recValue.asset_type            = this.oldVal.asset_type;
                         recValue.classfld              = this.oldVal.classfld;
                         recValue.department            = this.oldVal.department;
                         recValue.location              = this.oldVal.location;
                         recValue.currCost              = this.recCoreValue[i].currCost;
                         recValue.deprToDate            = this.recCoreValue[i].deprCost;
                         recValue.quantity              = this.recCoreValue[i].qty;
                         recValue.acqTransaction_amount = this.recCoreValue[i].currCost * -1; 
                         recValue.acqNet_book_value     = 0;
                         recValue.depTransaction_amount = -this.recCoreValue[i].deprCost; 
                         recValue.depNet_book_value     = 0;
                         if(this.recCoreValue[i].altDep)
                             recValue.altDep = this.recCoreValue[i].altDep;
                         recValue.refs = ['Asset Transfer Out (FAM)', 'Asset Transfer Out (FAM)', 'Asset Transfer Out (FAM)']; 
                         recValue.failurePoint = 'Subsidiary Transfer Out';
                         
                         this.writeJournalandHistory(recValue);
                         
                         //in
                         recValue.costType              = 4; //costing schema 4 (reverse debit/credit
                         recValue.accts                 = [this.recCoreValue[i].nMainAcct,
                                                           this.recCoreValue[i].nDeprAcct,
                                                           desAcct];
                         recValue.subsidiary = this.newVal.subsidiary;
                         recValue.asset_type = this.newVal.asset_type || this.oldVal.asset_type;
                         recValue.classfld = this.newVal.classfld || this.oldVal.classfld;
                         recValue.department = this.newVal.department || this.oldVal.department;
                         recValue.location = this.newVal.location || this.oldVal.location;

                         newBookVal = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].bookVal * this.recCoreValue[i].exRate, this.recCoreValue[i].currency, this.intCurrSym);
                         
                         recValue.currCost              = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].currCost * this.recCoreValue[i].exRate, this.recCoreValue[i].currency, this.intCurrSym);
                         recValue.deprToDate            = recValue.currCost - newBookVal;
                         recValue.acqTransaction_amount = recValue.currCost; 
                         recValue.acqNet_book_value     = recValue.currCost;
                         recValue.depTransaction_amount = recValue.deprToDate; 
                         recValue.depNet_book_value     = recValue.currCost - recValue.deprToDate;
                         recValue.refs                  = ['Asset Transfer In (FAM)', 'Asset Transfer In (FAM)', 'Asset Transfer In (FAM)']; 
                         recValue.failurePoint          = 'Subsidiary Transfer In';
                         this.writeJournalandHistory(recValue);
                    }
                    //update Records here
                    if (!this.recCoreValue[i].altDep) {//update assetrecord
                        this.logObj.logExecution('Updating Asset Record Id: ' + assetRec.recordId);
                        if(this.newVal.asset_type){//emulate UI sourcing
                            this.newVal.asset_account      = this.recCoreValue[i].nMainAcct;
                            this.newVal.depr_account       = this.recCoreValue[i].nDeprAcct;
                            this.newVal.depr_charge_account= this.recCoreValue[i].chargeAcc;
                            this.newVal.writeoff_account   = this.recCoreValue[i].writeOffAcc;
                            this.newVal.writedown_account  = this.recCoreValue[i].writeDownAcc;
                            this.newVal.disposal_account   = this.recCoreValue[i].dispAcc;
                            
                            assetRec.loadRecord(assetRec.recordId, {recordmode: 'dynamic'});
                            for(var e in this.newVal){
                                assetRec.setFieldValue(e, this.newVal[e]);
                            }
                            assetRec.submitRecord();
                        }
                        else{
                            updateRec = JSON.parse(JSON.stringify(this.newVal));
                            if(this.newVal.classfld && this.newVal.classfld == 'unset')
                                updateRec.classfld = '';
                            if(this.newVal.department && this.newVal.department == 'unset')
                                updateRec.department = '';
                            if(this.newVal.location && this.newVal.location == 'unset')
                                updateRec.location = '';                            
                            
                            currData = this.subCache.subCurrData(recValue.subsidiary, recValue.bookId);
                            
                            updateRec.initial_cost     = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].origCost * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);                            
                            updateRec.current_cost     = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].currCost * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.rv               = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].rv * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.book_value       = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].bookVal * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.lastDeprAmount   = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].ldAmnt * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.prior_nbv        = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].priorNBV * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.cummulative_depr = FAM.Util_Shared.Math.roundByCurrency(updateRec.current_cost - updateRec.book_value, currData.currSym, this.intCurrSym);
                            updateRec.fixed_rate       = this.recCoreValue[i].exRate;
                            assetRec.submitField(updateRec);
                        }
                    } 
                    else if (this.newVal.subsidiary || this.newVal.asset_type) { //update tax methods only if subsidiary or asset type was changed
                        this.logObj.logExecution('Updating Tax Method Id: ' + this.recCoreValue[i].altDep);
                        var altDepr = new FAM.AltDeprMethod_Record();
                        altDepr.recordId = this.recCoreValue[i].altDep;
                        updateRec = JSON.parse(JSON.stringify(this.newVal));
                        if(this.newVal.asset_type){
                            updateRec.asset_account      = this.recCoreValue[i].nMainAcct;
                            updateRec.depr_account       = this.recCoreValue[i].nDeprAcct;
                            updateRec.charge_account     = this.recCoreValue[i].chargeAcc;
                            updateRec.write_off_account  = this.recCoreValue[i].writeOffAcc;
                            updateRec.write_down_account = this.recCoreValue[i].writeDownAcc;
                            updateRec.disposal_account   = this.recCoreValue[i].dispAcc;
                        }
                        else{
                            if(this.newVal.classfld && this.newVal.classfld == 'unset')
                                updateRec.classfld = '';
                            if(this.newVal.department && this.newVal.department == 'unset')
                                updateRec.department = '';
                            if(this.newVal.location && this.newVal.location == 'unset')
                                updateRec.location = '';
                                                        
                            currData = this.subCache.subCurrData(recValue.subsidiary, recValue.bookId);
                            
                            updateRec.original_cost      = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].origCost * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.current_cost       = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].currCost * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.residual_value     = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].rv * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.book_value         = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].bookVal * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.last_depr_amount   = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].ldAmnt * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.prior_year_nbv     = FAM.Util_Shared.Math.roundByCurrency(this.recCoreValue[i].priorNBV * this.recCoreValue[i].exRate, currData.currSym, this.intCurrSym);
                            updateRec.cumulative_depr    = FAM.Util_Shared.Math.roundByCurrency(updateRec.current_cost - updateRec.book_value, currData.currSym, this.intCurrSym);
                            updateRec.fixed_rate         = this.recCoreValue[i].exRate;
                            updateRec.currency           = this.recCoreValue[i].currency; //update currency
                        }
                        altDepr.submitField(updateRec);
                        
                        if(this.newVal.asset_type && 
                           this.recCoreValue[i].bookId &&
                           !this.recCoreValue[i].nMainAcct && 
                           !this.recCoreValue[i].nDeprAcct){//throw error for missing accounts in asset transfers with book id
                            bSkipTaxMethod = true;
                            throw nlapiCreateError('FAM_TAX_METHOD_NO_ACCT_ASSET_TYPE', 
                                                   FAM.resourceManager.GetString('client_assettransfer_no_tax_meth_acct'), true);
                        }
                    }
                }
                catch(e) {
                    msg = '', procmsg = e.toString(), bExit = false;
                    
                    //rollback existing rollback records
                    var rbIdx = this._rollbackRecords.length;
                    while( rbIdx > 0 )
                    {
                        --rbIdx;
                        this.logObj.logExecution('Deleting: ' + this._rollbackRecords[rbIdx].record + '(ID: ' + this._rollbackRecords[rbIdx].id + ')');
                        nlapiDeleteRecord(this._rollbackRecords[rbIdx].record, this._rollbackRecords[rbIdx].id);
                    }
                    
                    if(this.recCoreValue[i].altDep){
                        msg += '\nError Processing Tax Method Id: ' + this.recCoreValue[i].altDep;
                    }else{
                        msg += '\nError Processing Asset Id: ' + this.objAssetTransfer.AssetId;
                        bExit = true;
                    }
                    
                    msg += ' | Error Msg: ' + procmsg;
                    if(bSkipTaxMethod){ //set taxmethod depreciation active field to false
                        var altDepr = new FAM.AltDeprMethod_Record();
                        altDepr.recordId = this.recCoreValue[i].altDep;
                        altDepr.submitField({depr_active: FAM.DeprActive.False});
                    }
                    
                    this.logObj.logExecution(msg, 'ERROR');
                    this.procInsRec.writeToProcessLog(msg, 'Error', 'Asset Id: ' + this.objAssetTransfer.AssetId);
                    this.wError = true;
                    if(bExit){
                        this.recsFailed++;
                        this.recsProcessed++;
                        throw e;
                    }
                }
                
                lastTaxMethId = +this.recCoreValue[i].altDep || 0;
                //clear rollback record for every successful update
                this._rollbackRecords = [];
            }
            this.recCoreValue = [];
        } while (!blnToRequeue && taxSearched === 1000);
    } 
    else {//update asset CDL if blank
        this.logObj.logExecution('Updating Asset Record Id: ' + assetRec.recordId);
        updateRec = JSON.parse(JSON.stringify(this.newVal));
        if(this.newVal.classfld && this.newVal.classfld == 'unset') { updateRec.classfld = ''; }
        if(this.newVal.location && this.newVal.location == 'unset') { updateRec.location = ''; }
        if(this.newVal.department && this.newVal.department == 'unset') {
            updateRec.department = '';
        }
        if (Object.keys(this.newVal).length > 0) {
            assetRec.submitField(updateRec);
        }
    }
    
    if (blnToRequeue) {
        this.logObj.logExecution('Execution Limit | Remaining Usage: ' +
            FAM.Context.getRemainingUsage() + ' | Time Elapsed: ' +
            this.perfTimer.getReadableElapsedTime());
        this.procInsRec.setScriptParams({ lowerLimit : lastTaxMethId });
    }
    this.logObj.endMethod();
    return blnToRequeue;
};

FAM.AssetTransfer.prototype.writeJournalandHistory = function(recValue){
    this.logObj.startMethod('FAM.AssetTransfer.writeJournalandHistory');
    var msg,
    zeroVal = (recValue.currCost == 0 && recValue.deprToDate == 0),
    debits = [],
    credits = [],
    deprHistVal = {},
    journalId = null,
    currData = this.subCache.subCurrData(recValue.subsidiary, recValue.bookId);
    
    
    if (!zeroVal){ //don't create history/journal if both current cost and depreciation to date is zero
        if((FAM.Context.blnOneWorld && 
            recValue.subsidiary && 
            this.subCache.fieldValue(recValue.subsidiary, 'isinactive') == 'T') ||
            this.atCache.fieldValue(recValue.asset_type, 'isinactive') == 'T'){ //do not create DHR or Journals for inactive subsidiaries/at
            msg = 'Inactive Old Subsidiary/Asset Type. Skipping creation of DHR/Journal Entries. ';
            if(recValue.altDep){
                msg += ' (Tax Method ID: ' + recValue.altDep + ' )';
            }
            this.procInsRec.writeToProcessLog(msg, 'Error', 'Asset Id: ' + this.objAssetTransfer.AssetId);
            this.wError = true;
            return;
        }
        
        if(recValue.bookId && !recValue.inactiveoldsub){
            if(recValue.costType == 1){
                credits[0] = FAM.Util_Shared.Math.roundByCurrency(recValue.currCost, currData.currSym, this.intCurrSym);
                credits[1] = 0;
                credits[2] = 0;
                debits[0]  = 0;
                debits[1]  = FAM.Util_Shared.Math.roundByCurrency(recValue.deprToDate, currData.currSym, this.intCurrSym);
                debits[2]  = FAM.Util_Shared.Math.roundByCurrency(credits[0] - debits[1], currData.currSym, this.intCurrSym);
            }
            else if(recValue.costType == 2){
                credits[0] = 0;
                credits[1] = FAM.Util_Shared.Math.roundByCurrency(recValue.deprToDate, currData.currSym, this.intCurrSym);
                debits[0]  = FAM.Util_Shared.Math.roundByCurrency(recValue.currCost, currData.currSym, this.intCurrSym);
                credits[2] = FAM.Util_Shared.Math.roundByCurrency(debits[0] - credits[1], currData.currSym, this.intCurrSym);;
                debits[1]  = 0;
                debits[2]  = 0;
            }
            else if(recValue.costType == 3){
                credits[0] = FAM.Util_Shared.Math.roundByCurrency(recValue.currCost, currData.currSym, this.intCurrSym);
                credits[1] = 0;
                credits[2] = 0;
                credits[3] = FAM.Util_Shared.Math.roundByCurrency(recValue.deprToDate, currData.currSym, this.intCurrSym);
                debits[0]  = 0;
                debits[1]  = FAM.Util_Shared.Math.roundByCurrency(recValue.deprToDate, currData.currSym, this.intCurrSym);
                debits[2]  = FAM.Util_Shared.Math.roundByCurrency(recValue.currCost, currData.currSym, this.intCurrSym);
                debits[3]  = 0;
                
            }
            else if(recValue.costType == 4){
                debits[0] = FAM.Util_Shared.Math.roundByCurrency(recValue.currCost, currData.currSym, this.intCurrSym);
                debits[1] = 0;
                debits[2] = 0;
                credits[0]  = 0;
                credits[1]  = FAM.Util_Shared.Math.roundByCurrency(recValue.deprToDate, currData.currSym, this.intCurrSym);
                credits[2]  = FAM.Util_Shared.Math.roundByCurrency(debits[0] - credits[1], currData.currSym, this.intCurrSym);
            }
            
            if (recValue.journalId) {
                journalId = recValue.journalId;
            }
            else if (recValue.accts.indexOf('') == -1 || (recValue.accts[1] == '' &&
                recValue.deprToDate == 0)) {

                this.logObj.pushMsg(recValue.failurePoint + ' - Create Journal');
                
                var transType = FAM.SystemSetup.getSetting('allowCustomTransaction') === 'T' ?
                    'customtransaction_fam_transfer_jrn' : 'journalentry';
                
                journalId = FAM_Util.createJournalEntry({
                    type : transType,
                    trnDate : this.trnDate,
                    currId : currData.currId, 
                    accts : recValue.accts,
                    debitAmts : debits,
                    creditAmts : credits,
                    ref : recValue.refs,
                    subId : recValue.subsidiary,
                    classId : recValue.classfld,
                    deptId : recValue.department,
                    locId : recValue.location,
                    bookId : recValue.bookId,
                    entities : [],
                    permit : +this.procInsRec.stateValues.JrnPermit
                });

                this._rollbackRecords.push({record: transType, id: journalId});
            }
            else {
                msg = 'Skipping creation of Journals because of missing account(s)';
                this.wError = true;
                if (recValue.altDep) {
                    msg += ' (Tax Method ID: ' + recValue.altDep + ' )';
                }
                
                this.logObj.logExecution(msg, 'ERROR');
                this.procInsRec.writeToProcessLog(msg, 'Error', 'Asset Id: ' +
                    this.objAssetTransfer.AssetId);
            }
        }
    }
  //do not create history for zero value and cdl transfer
    if (!zeroVal || this.newVal.subsidiary || this.newVal.asset_type) {
        this.logObj.pushMsg(recValue.failurePoint + ' - Create Depreciation History (Acquisition/Disposal)');
        deprHistVal.subsidiary = recValue.subsidiary;
        deprHistVal.asset_type = recValue.asset_type;
        deprHistVal.transaction_type = FAM.TransactionType.Acquisition;
        deprHistVal.asset = recValue.assetId;
        deprHistVal.date = this.trnDate;
        deprHistVal.transaction_amount = FAM.Util_Shared.Math.roundByCurrency(recValue.acqTransaction_amount, currData.currSym, this.intCurrSym);
        deprHistVal.net_book_value = FAM.Util_Shared.Math.roundByCurrency(recValue.acqNet_book_value, currData.currSym, this.intCurrSym);
        deprHistVal.quantity = recValue.quantity;
        if(journalId)
            deprHistVal.posting_reference = journalId;
        if(recValue.altDep)
            deprHistVal.alternate_depreciation = recValue.altDep;
        if(FAM.Context.blnMultiBook && recValue.bookId)
            deprHistVal.bookId = recValue.bookId;
        
        var deprHistId = this.createDepreciationHistory(deprHistVal);
        this._rollbackRecords.push({record: 'customrecord_ncfar_deprhistory', id: deprHistId});
        
        this.logObj.pushMsg(recValue.failurePoint + ' - Create Depreciation History (Depreciation)');
        deprHistVal.transaction_type = FAM.TransactionType.Depreciation;
        deprHistVal.transaction_amount = FAM.Util_Shared.Math.roundByCurrency(recValue.depTransaction_amount, currData.currSym, this.intCurrSym);
        deprHistVal.net_book_value = FAM.Util_Shared.Math.roundByCurrency(recValue.depNet_book_value, currData.currSym, this.intCurrSym);
        
        deprHistId = this.createDepreciationHistory(deprHistVal);
        this._rollbackRecords.push({record: 'customrecord_ncfar_deprhistory', id: deprHistId});
    }
    
    this.logObj.endMethod();
    return journalId;
};

FAM.AssetTransfer.prototype.createDepreciationHistory = function (fldVal){
    this.logObj.startMethod('FAM.AssetTransfer.createDepreciationHistory');
    var deprHistRec = new FAM.DepreciationHistory_Record();
    deprHistRec.createRecord(fldVal);
    var dhrId = deprHistRec.submitRecord();
    this.logObj.endMethod();
    return dhrId;
};

FAM.AssetTransfer.prototype.getAllTaxMethods = function (assetId, lastTaxMethId, blnCount) {
    this.logObj.startMethod('FAM.AssetTransfer.getAllTaxMethods');
    
    var fSearch = new FAM.Search(new FAM.AltDeprMethod_Record());
    
    fSearch.addFilter('parent_asset', null, 'is', assetId);
    fSearch.addFilter('isinactive', null, 'is', 'F');
    fSearch.addFilter('internalidnumber', null, 'greaterthan', lastTaxMethId);
    
    if (blnCount) {
        fSearch.addColumn('internalid', null, 'count');
    }
    else {
        fSearch.addColumn('internalid', null, null, 'SORT_ASC');
        fSearch.addColumn('subsidiary');
        fSearch.addColumn('alternate_method');
        fSearch.addColumn('original_cost');
        fSearch.addColumn('current_cost');
        fSearch.addColumn('residual_value');
        fSearch.addColumn('cumulative_depr');
        fSearch.addColumn('book_value');
        fSearch.addColumn('prior_year_nbv');
        fSearch.addColumn('asset_account');
        fSearch.addColumn('depr_account');
        fSearch.addColumn('booking_id');
        fSearch.addColumn('last_depr_amount');
    }
    
    fSearch.run();
    
    this.logObj.endMethod();
    return fSearch;
};

FAM.AssetTransfer.prototype.updProcIns = function (bRequeue){
    this.logObj.startMethod('FAM.AssetTransfer.updProcIns');
    
    var totalPercent, procFields = {};
    
    if (this.totalRecords === 0) {
        procFields.message = 'No records found.';
    }
    else {
        totalPercent = FAM_Util.round((this.recsProcessed/this.totalRecords) *
            100);
        FAM.Context.setPercentComplete(totalPercent);
        if(this.totalRecords > 1){
            procFields.message = 'Bulk Transfer Status: '; 
        }
        else {
            procFields.message = 'Transfer Asset Id: ' + this.objAssetTransfer.AssetId;
            for (var e in this.oldVal) {
                if (this.newVal[e]) {
                    procFields.message += ' | ' +  e + ': \'' + this.oldVal[e] + '\' to \'' +
                        this.newVal[e] + '\'';
                }
            }
            procFields.message += ' | ';
        }
        procFields.message += this.recsProcessed + ' of ' + this.totalRecords + ' Done, ' + totalPercent + '% Complete';
        
    }
    if(!bRequeue){
        if(this.recsFailed > 0 || this.wError){
            procFields.status = FAM.BGProcessStatus.CompletedError;
        }
        else{
            procFields.status = FAM.BGProcessStatus.Completed;
        }
    }

    if(this.wError){
        this.procInsRec.setScriptParams({ upperLimit : 1 });
    }
    
    procFields.rec_total = this.totalRecords;
    procFields.rec_count = this.recsProcessed;
    procFields.rec_failed = this.recsFailed;
    
    if (procFields.message && procFields.message.length > 300) {
        nlapiLogExecution('DEBUG', 'catchError', 'Error Detail contained more than maximum ' +
            'number (300) characters, truncating message..');
        procFields.message = procFields.message.substring(0, 300);
    }

    this.procInsRec.submitField(procFields);
    
    this.logObj.endMethod();
};

FAM.AssetTransfer.prototype.fetchTaxMethodAccounts = function (assetType){
    this.logObj.startMethod('FAM.AssetTransfer.fetchTaxMethodAccounts');
    var arrDefAltVal = [];
    var defAltDepSearch = new FAM.Search(new FAM.DefaultValuesBook_Record());
    defAltDepSearch.addFilter('assetType', null, 'is',  assetType);
    defAltDepSearch.addFilter('isinactive', null, 'is', 'F');
    defAltDepSearch.addFilter('assetAcc', null, 'noneof','@NONE@');
    defAltDepSearch.addFilter('deprAcc', null, 'noneof','@NONE@');
    defAltDepSearch.addColumn('bookId');
    defAltDepSearch.addColumn('altMethod');
    defAltDepSearch.addColumn('assetAcc');
    defAltDepSearch.addColumn('deprAcc');
    defAltDepSearch.addColumn('chargeAcc');
    defAltDepSearch.addColumn('writeOffAcc');
    defAltDepSearch.addColumn('writeDownAcc');
    defAltDepSearch.addColumn('dispAcc');
    defAltDepSearch.run();
    
    if(defAltDepSearch.results){
        for(var i = 0; i < defAltDepSearch.results.length; i++){
            arrDefAltVal[i] = {bookId: defAltDepSearch.getValue(i, 'bookId'),
                               altMethod: defAltDepSearch.getValue(i, 'altMethod'),
                               assetAcc: defAltDepSearch.getValue(i, 'assetAcc'),
                               deprAcc: defAltDepSearch.getValue(i, 'deprAcc'),
                               chargeAcc: defAltDepSearch.getValue(i, 'chargeAcc'),
                               writeOffAcc: defAltDepSearch.getValue(i, 'writeOffAcc'),
                               writeDownAcc: defAltDepSearch.getValue(i, 'writeDownAcc'),
                               dispAcc: defAltDepSearch.getValue(i, 'dispAcc')
                              };
        }
    }
    this.logObj.endMethod();
    return arrDefAltVal;
};

FAM.AssetTransfer.prototype.fetchTaxMethodData = function (qty, lastTaxMethId){
    this.logObj.startMethod('FAM.AssetTransfer.fetchTaxMethodData');
    var taxMethods = this.getAllTaxMethods(this.objAssetTransfer.AssetId, lastTaxMethId);
    if(taxMethods.results){
        this.logObj.logExecution('Found ' + (taxMethods.results.length || 0) + ' Tax Methods for processing');
    }
    else {
        return 0;
    }
    var arrDefAltVal = [];
    if(this.newVal.asset_type){
        arrDefAltVal = this.fetchTaxMethodAccounts(this.newVal.asset_type);
    }
    for(var i = 0, j = 0; taxMethods.results && i < (taxMethods.results.length); i++){
        this.recCoreValue.push({});
        j = this.recCoreValue.length - 1;
        if(FAM.Context.blnMultiBook)
            this.recCoreValue[j].bookId   = taxMethods.getValue(i, 'booking_id');
        this.recCoreValue[j].subsidiary   = taxMethods.getValue(i, 'subsidiary');
        var oldCurrData = this.subCache.subCurrData(this.recCoreValue[j].subsidiary, this.recCoreValue[j].bookId);
        var newCurrData = oldCurrData;
        if(this.newVal.subsidiary){
            newCurrData = this.subCache.subCurrData(this.newVal.subsidiary, this.recCoreValue[j].bookId);
        } 
        this.recCoreValue[j].exRate   = FAM.Util_Shared.getExchangeRate(oldCurrData.currId, newCurrData.currId, this.trnDate);
        
        this.recCoreValue[j].oMainAcct = taxMethods.getValue(i, 'asset_account');
        this.recCoreValue[j].oDeprAcct = taxMethods.getValue(i, 'depr_account');
        if(this.newVal.asset_type){
            //fetch new accts
            this.recCoreValue[j].nMainAcct = null;
            this.recCoreValue[j].nDeprAcct = null;
            this.recCoreValue[j].chargeAcc = null;
            this.recCoreValue[j].writeOffAcc = null;
            this.recCoreValue[j].writeDownAcc = null;
            this.recCoreValue[j].dispAcc = null;
            for(var ii = 0; ii < arrDefAltVal.length; ii++){
                if(arrDefAltVal[ii].bookId == taxMethods.getValue(i, 'booking_id') && 
                   arrDefAltVal[ii].altMethod == taxMethods.getValue(i, 'alternate_method')){

                    this.recCoreValue[j].nMainAcct    = arrDefAltVal[ii].assetAcc;
                    this.recCoreValue[j].nDeprAcct    = arrDefAltVal[ii].deprAcc;
                    this.recCoreValue[j].chargeAcc    = arrDefAltVal[ii].chargeAcc;
                    this.recCoreValue[j].writeOffAcc  = arrDefAltVal[ii].writeOffAcc;
                    this.recCoreValue[j].writeDownAcc = arrDefAltVal[ii].writeDownAcc;
                    this.recCoreValue[j].dispAcc      = arrDefAltVal[ii].dispAcc;
                    break;
                }
            }
        }
        else {
            this.recCoreValue[j].nMainAcct = this.recCoreValue[j].oMainAcct;
            this.recCoreValue[j].nDeprAcct = this.recCoreValue[j].oDeprAcct;
        }
        this.recCoreValue[j].currCost = taxMethods.getValue(i, 'current_cost');
        this.recCoreValue[j].deprCost = taxMethods.getValue(i, 'cumulative_depr');
        this.recCoreValue[j].qty      = qty;
        this.recCoreValue[j].altMeth  = taxMethods.getValue(i, 'alternate_method');
        this.recCoreValue[j].altDep   = taxMethods.getId(i);
      //values for conversion
        this.recCoreValue[j].origCost = taxMethods.getValue(i, 'original_cost');
        this.recCoreValue[j].rv       = taxMethods.getValue(i, 'residual_value');
        this.recCoreValue[j].bookVal  = taxMethods.getValue(i, 'book_value');
        this.recCoreValue[j].ldAmnt   = taxMethods.getValue(i, 'last_depr_amount')||0;
        this.recCoreValue[j].priorNBV = taxMethods.getValue(i, 'prior_year_nbv');
        this.recCoreValue[j].currency = newCurrData.currId;
    }
    this.logObj.endMethod();
    return taxMethods.results.length;
};

FAM.AssetTransfer.prototype.fetchAssetRecordData = function (oldAssetRec){
    this.logObj.startMethod('FAM.AssetTransfer.fetchAssetRecordData');
    this.recCoreValue[0] = {};
    this.recCoreValue[0].bookId   = this.primaryBook||1;
    this.recCoreValue[0].subsidiary   = this.oldVal.subsidiary;
    var oldCurrData = this.subCache.subCurrData(this.recCoreValue[0].subsidiary, this.recCoreValue[0].bookId), newCurrData = oldCurrData;
    if(this.newVal.subsidiary){
        newCurrData = this.subCache.subCurrData(this.newVal.subsidiary, this.recCoreValue[0].bookId);
    } 
    this.recCoreValue[0].exRate   = FAM.Util_Shared.getExchangeRate(oldCurrData.currId, newCurrData.currId, this.trnDate);
    
    this.recCoreValue[0].oMainAcct = oldAssetRec.custrecord_assetmainacc;
    this.recCoreValue[0].oDeprAcct = oldAssetRec.custrecord_assetdepracc;
    if(this.newVal.asset_type){
        //fetch new asset accts
        this.recCoreValue[0].assetType = this.newVal.asset_type;
        var atAccounts = nlapiLookupField('customrecord_ncfar_assettype',this.newVal.asset_type,
                            ['custrecord_assettypeassetacc','custrecord_assettypedepracc',
                             'custrecord_assettypedeprchargeacc','custrecord_assettypewriteoffacc',
                             'custrecord_assettypewritedownacc','custrecord_assettypedisposalacc']);
        this.recCoreValue[0].nMainAcct    = atAccounts.custrecord_assettypeassetacc;
        this.recCoreValue[0].nDeprAcct    = atAccounts.custrecord_assettypedepracc;
        this.recCoreValue[0].chargeAcc    = atAccounts.custrecord_assettypedeprchargeacc;
        this.recCoreValue[0].writeOffAcc  = atAccounts.custrecord_assettypewriteoffacc;
        this.recCoreValue[0].writeDownAcc = atAccounts.custrecord_assettypewritedownacc;
        this.recCoreValue[0].dispAcc      = atAccounts.custrecord_assettypedisposalacc;
    }
    else {
        this.recCoreValue[0].nMainAcct = this.recCoreValue[0].oMainAcct;
        this.recCoreValue[0].nDeprAcct = this.recCoreValue[0].oDeprAcct;
    }
    this.recCoreValue[0].currCost = oldAssetRec.custrecord_assetcurrentcost;
    this.recCoreValue[0].deprCost = oldAssetRec.custrecord_assetdeprtodate;
    this.recCoreValue[0].qty      = oldAssetRec.custrecord_ncfar_quantity;
    //values for conversion
    this.recCoreValue[0].origCost = oldAssetRec.custrecord_assetcost;
    this.recCoreValue[0].rv       = oldAssetRec.custrecord_assetresidualvalue;
    this.recCoreValue[0].bookVal  = oldAssetRec.custrecord_assetbookvalue;
    this.recCoreValue[0].ldAmnt   = oldAssetRec.custrecord_assetlastdepramt||0;
    this.recCoreValue[0].priorNBV = oldAssetRec.custrecord_assetpriornbv;
    this.recCoreValue[0].currency = newCurrData.currId;

    this.logObj.endMethod();
};

/**
 * Determines if the Execution Limit or Time Limit has exceeded
 *
 * Returns:
 *     true {boolean} - Execution Limit or Time Limit has exceeded
 *     false {boolean} - Execution Limit or Time Limit has not exceeded
**/
FAM.AssetTransfer.prototype.hasExceededLimit = function () {
    this.logObj.startMethod('FAM.AssetTransfer.hasExceededLimit');

    var ret = FAM.Context.getRemainingUsage() < this.execLimit ||
        this.perfTimer.getElapsedTime() > this.timeLimit;

    this.logObj.endMethod();
    return ret;
};