/**
 * � 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var FAM;
if (!FAM) { FAM = {}; }

// implemented as an object as member functions should have no dependency of each other
FAM.Util_CS = {
    /**
     * Checks the value of convention against depreciation period
     *
     * Parameters:
     *     deprMethodId
     *     convention
     * Returns:
     *     void
    **/
    checkConvention : function (deprMethodId, convention) {
        var deprPeriod;

        if (!deprMethodId) { // no values loaded yet
            return true;
        }

        convention = +convention || FAM.Conventions.None;
        deprPeriod = +nlapiLookupField('customrecord_ncfar_deprmethod', deprMethodId,
            'custrecord_deprmethoddeprperiod');

        if ((deprPeriod === FAM.DeprPeriod.Annually &&
                convention === FAM.Conventions['Mid-Month']) ||
            (deprPeriod === FAM.DeprPeriod.Monthly &&
                (convention === FAM.Conventions['Half-Year'] ||
                convention === FAM.Conventions['Mid-Quarter']))) {

            return false;
        }

        return true;
    },

    /**
     * Accepts a hashmap of message ids to fetch messages from resource list
     *
     * Parameters:
     *     msgIds {string{}} - message ids to be retrieved
     *     screenName {string} - category identifier for the strings
     * Returns:
     *     {string{}} - object containing the messages identified by message id
    **/
    fetchMessageObj : function (msgIds, screenName) {
        var id, suParams, response, messages, DELIMITER = '$$', ret = {}, messageId = [];

        for (id in msgIds) {
            messageId.push(msgIds[id]);
        }

        suParams = {
            'custpage_messageid' : messageId.join(DELIMITER),
            'custpage_delimiter' : DELIMITER
        };

        if (screenName) {
            suParams.custpage_pageid = screenName;
        }

        //Request for String translation for messages
        response = nlapiRequestURL(
            nlapiResolveURL('SUITELET', 'customscript_fam_language_resource',
                'customdeploy_language_resource_deploy', false),
            suParams
        );

        //Store chain messages to array
        messages = response.getBody().split(DELIMITER);
        messages.reverse();

        for (id in msgIds) {
            ret[id] = messages.pop();
        }

        return ret;
    },

    /**
    * Replace first instance of (word) with the an html formatted link
    * Parameters
    *     {String} - msg: the display message; Requires a word enveloped in ()
    *     {String} - link: the html link
    * Return
    *     {String} - html formatted syntax for link message
    */
    insertLink : function (msg, link) {
        if(!msg || !link) return null;

        var linkLabel   = msg.slice((msg.indexOf('(') + 1),+msg.indexOf(')')),
            linkHtml    = '<a href=' + link + '>' + linkLabel + '</a>';
        return msg.replace('(','').replace(')','').replace(linkLabel, linkHtml);
    },

    /**
     * Checks for ACTIVE Bulk Transfer Process requests
     *
     * Parameters:
     * Returns:
     *     Boolean
    **/
    hasExistingBulkTransfer : function () {
        var fil =                   
            [[['custrecord_far_proins_functionname', 'is', 'famAssetTransferCSV'],
              'or',
              [['custrecord_far_proins_functionname', 'is', 'famTransferAsset'],
               'and',
               ['custrecord_far_proins_recordid', 'equalto', '-1']
              ]
             ],
             'and',
             ['custrecord_far_proins_procstatus', 'anyof', [FAM.BGProcessStatus.InProgress, FAM.BGProcessStatus.Queued]],
             'and',
             ['isinactive', 'is', 'F'] 
            ];

        if (nlapiSearchRecord('customrecord_bg_procinstance', null, fil)) {
            return true;
        }

        return false;
    }
};

Object.keys = Object.keys || (function () {
    var hasOwnProperty = Object.prototype.hasOwnProperty,
        hasDontEnumBug = !{toString:null}.propertyIsEnumerable("toString"),
        DontEnums = [
            'toString',
            'toLocaleString',
            'valueOf',
            'hasOwnProperty',
            'isPrototypeOf',
            'propertyIsEnumerable',
            'constructor'
        ],
        DontEnumsLength = DontEnums.length;

    return function (o) {
        if (typeof o != "object" && typeof o != "function" || o === null)
            throw new TypeError("Object.keys called on a non-object");

        var result = [];
        for (var name in o) {
            if (hasOwnProperty.call(o, name))
                result.push(name);
        }

        if (hasDontEnumBug) {
            for (var i = 0; i < DontEnumsLength; i++) {
                if (hasOwnProperty.call(o, DontEnums[i]))
                    result.push(DontEnums[i]);
            }
        }

        return result;
    };
})();

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement /*, fromIndex */ ) {
        'use strict';
        if (this == null) {
            throw new TypeError();
        }
        var n, k, t = Object(this),
            len = t.length >>> 0;

        if (len === 0) {
            return -1;
        }
        n = 0;
        if (arguments.length > 1) {
            n = Number(arguments[1]);
            if (n != n) { // shortcut for verifying if it's NaN
                n = 0;
            }
            else if (n != 0 && n != Infinity && n != -Infinity) {
                n = (n > 0 || -1) * Math.floor(Math.abs(n));
            }
        }
        if (n >= len) {
            return -1;
        }
        for (k = n >= 0 ? n : Math.max(len - Math.abs(n), 0); k < len; k++) {
            if (k in t && t[k] === searchElement) {
                return k;
            }
        }
        return -1;
    };
}

// Polyfill Function taken from https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind
if (!Function.prototype.bind) {
  Function.prototype.bind = function(oThis) {
    if (typeof this !== 'function') {
      // closest thing possible to the ECMAScript 5
      // internal IsCallable function
      throw new TypeError('Function.prototype.bind - what is trying to be bound is not callable');
    }

    var aArgs   = Array.prototype.slice.call(arguments, 1),
        fToBind = this,
        fNOP    = function() {},
        fBound  = function() {
          return fToBind.apply(this instanceof fNOP && oThis
                 ? this
                 : oThis,
                 aArgs.concat(Array.prototype.slice.call(arguments)));
        };

    fNOP.prototype = this.prototype;
    fBound.prototype = new fNOP();

    return fBound;
  };
}

/**
 * Wrapper function for reloading page for stubbing purposes
**/
function reloadPage() {
    window.location.reload();
}