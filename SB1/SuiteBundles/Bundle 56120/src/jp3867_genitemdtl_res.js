/**
 * � 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var FAM,
    LOCALIZATION_FILENAME = 'fam_resource',
    PREFIX_CUSTOM         = 'custom_';

if (!FAM) FAM = {};

/**
 * Copyright (c) 1998-2008 NetSuite, Inc.
 * 2955 Campus Drive, Suite 100, San Mateo, CA, USA 94403-2511
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * NetSuite, Inc. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with NetSuite.
 */

//================

var jp3867ns;
if (!jp3867ns) { jp3867ns = {}; }
if(!(jp3867ns.Resources)) jp3867ns.Resources={};

//==============================================================================
jp3867ns.Resources.ResourceManager = function ResourceManager( resfilename, userLanguage)
{
    var _UserLanguage = userLanguage == null? _GetUserLanguagePreference(): userLanguage;
    var _ResourceFile = resfilename;
    var _CustomResourceFile = PREFIX_CUSTOM + resfilename;
    var _FolderId = _GetFolderId( resfilename);  
    var _Resources = [];
    var _CustomResources = [];
     
    _LoadResource(_UserLanguage);
    
    
    //--------------------------------------------------------------------------
    function _LoadResource( userLanguage)
    {
        if(_Resources[userLanguage] == null)
        {
            var resource = _OpenResourceFile( userLanguage);	
            _Resources[userLanguage] = resource;
            
            var customresource = _OpenCustomResourceFile( userLanguage);
            _CustomResources[userLanguage] = customresource;
        }
    }
    
    /** Fetch the display value using the parameters */
    this.GetString = function GetString( fieldName, pageName, userLanguage, messageParam)
    {
    	var returnValue = null;
    	//null check
        if(userLanguage == null || userLanguage.length == 0)
        	userLanguage = _UserLanguage;
                
        if(_Resources[userLanguage] == null || _CustomResources[userLanguage] == null)
            _LoadResource( userLanguage);
        
        /** attempt to load from custom resource file */
        var resx = _CustomResources[userLanguage];
        if( resx != null && resx.data != null && resx.data.(@name == fieldName)){
        	if(pageName == null || pageName.length == 0){
        		//found entry with no page attribute
        		returnValue = resx.data.(@name == fieldName && @page.toString().length == 0).value.toString();
        	} else if(resx.data.(@page == pageName)) {
        		//found entry including matching the page
        		returnValue = resx.data.(@name == fieldName && @page == pageName).value.toString();
        	}
        }
        
        /** attempt to load from default resource file if no field item found */
        if(returnValue == null || returnValue.length == 0) {
	        resx = _Resources[userLanguage];
	        if(resx != null && resx.data != null && resx.data.(@name == fieldName)) {
	        	if(pageName == null || pageName.length == 0){
	        		//found entry with no page attribute
        			returnValue = resx.data.(@name == fieldName && @page.toString().length == 0).value.toString();
	        	} else if(resx.data.(@page == pageName)) {
	        		//found entry including matching the page
	        		returnValue = resx.data.(@name == fieldName && @page == pageName).value.toString();
	        	}
	        }
        } 
        
        //No data was found, set with a failsoft value
        if(returnValue == null || returnValue.length == 0) { 
        	returnValue = "(" + fieldName + ")";
        }
        
        if (messageParam!= null) {
        	returnValue = InjectMessageParameter(returnValue, messageParam);
        }
        return returnValue;
    }
    
    //--------------------------------------------------------------------------
    this.GetStream = function GetStream( fieldName, userLanguage)
    {
    	if(userLanguage == null || userLanguage.length == 0)
        	userLanguage = _UserLanguage;   
        
        if(_Resources[userLanguage] == null)
            _LoadResource( userLanguage);
            
        var resx = _Resources[userLanguage];
        
        if( resx == null || resx.data == null)
            return "(" + fieldName + ")";
        
        if( resx.data.(@name == fieldName && @type == "System.Drawing.Bitmap, System.Drawing"))
            return resx.data.(@name == fieldName).value;
            
        return "(" + fieldName + ")";
    }
    
    /**
     * Replace message with blank value from parameter.
     * Blank value is difined by (number) - pattern, i.e. (0) will be replace with
     * first element in param, (1) from 2nd and so forth
     * 
     *  strVar -> The message
     *  param -> param values to be replaced on the message
     */
    function InjectMessageParameter(strVar, param){
    	var returnValue = strVar;

    	// search pattern to find string to replace
    	var paramPattern = /[(]\d[)]/g;


    	//Return index of number inserted on string, also contains other non-numeric values
    	var paramList = returnValue.match(paramPattern);
    	
    	//Replace the original string
    	for(i in paramList){
    		if (!isNaN(i) && param[i] != null){
    			returnValue = returnValue.replace(paramList[i],param[i]);
    		}
    	}
    	return returnValue;
    }

    //--------------------------------------------------------------------------
    function _GetUserLanguagePreference()
    {
        return nlapiGetContext().getPreference('LANGUAGE');
    }
    
    
    //--------------------------------------------------------------------------
    function _GetFileId( fileName)
    {
        var result = nlapiSearchRecord( "file", null,
            [new nlobjSearchFilter("name", null, 'is', fileName)],
            [new nlobjSearchColumn("internalid")]);
                
        return result == null? null: result[0].getValue("internalid");
    }
    
    

    //--------------------------------------------------------------------------
    function _GetFolderId( fileName)
    {
        var fileId = _GetFileId( fileName);
        if( fileId == null)
            return null;
        
        var file = nlapiLoadFile( fileId);
        if( file == null)
            return null;
            
        return file.getFolder();
    }
        
    
    
    //--------------------------------------------------------------------------
    function _OpenCustomResourceFile( userLanguage )
    {
        
        var resxFileName = _CustomResourceFile + "." + userLanguage + ".resx.xml";
        var fileId = _GetFileId( resxFileName);
        

        if (fileId == null) { return null; }
            
        var file = nlapiLoadFile( fileId);
        if (file == null) { return null; }

        var xml = new XML();
        xml = eval(jp3867ns.Library.trim(file.getValue().replace(/<\?xml(.|[\n])*?\?>/i, '')));
        return xml;
    }
    
    function _OpenResourceFile( userLanguage )
    {
        
        var defaultResx = _ResourceFile + ".en_US.resx.xml";
        var resxFileName = _ResourceFile + "." + userLanguage + ".resx.xml";
        var fileId = _GetFileId( resxFileName);
        
        if (fileId == null) { fileId = _GetFileId(defaultResx); }

        if (fileId == null) { return null; }
            
        var file = nlapiLoadFile( fileId);
        if (file == null) { return null; }

        var xml = new XML();
        xml = eval(jp3867ns.Library.trim(file.getValue().replace(/<\?xml(.|[\n])*?\?>/i, '')));
        return xml;
    }
}

if (!LOCALIZATION_FILENAME) {
	nlapiLogExecution('ERROR','FAM Resource Library','Filename for resource xml undefined');
}
else if (!FAM.resourceManager) {
	FAM.resourceManager = new jp3867ns.Resources.ResourceManager(LOCALIZATION_FILENAME,
        nlapiGetContext().getPreference('LANGUAGE'));
}
