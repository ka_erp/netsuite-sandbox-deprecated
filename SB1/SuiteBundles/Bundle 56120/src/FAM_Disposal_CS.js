/**
 * � 2014 NetSuite Inc.
 * User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
**/

var FAM;
if (!FAM) { FAM = {}; }

FAM.Disposal_CS = new function () {
    this.alertMessage = {};
    this.acctParam = [];
    this.resultCache = {};
    this.currSymbol = {};
    this.isMultiLocInvt;
    this.isPrefLocMandatory;
    
    /**
     * pageInit event type of client scripts
     *
     * @param {String} type - the mode in which the record is being accessed.
    **/
    this.pageInit = function (type) {
    	this.isMultiLocInvt = FAM.Context.blnMultiLocInvt;
        this.isPrefLocMandatory = FAM.Context.blnLocMandatory;
    	
        this.alertMessage = FAM.Util_CS.fetchMessageObj({
            ALERT_PLEASEENTER     : 'client_enter_value',
            ALERT_ALREADYDISPOSED : 'client_disposal_alreadydisposed',
            ALERT_SUBSIDIARYMATCH : 'client_subsidiary_match',
            ALERT_BGPEXISTS       : 'client_disposal_bgpexists',
            ALERT_ACCTNOTEXIST    : 'client_disposal_acctnotexist',
            ALERT_EARLIERTHANLDD  : 'client_disposal_dateearlierthanldd',
            ALERT_SALEAMTPOSITIVE : 'client_disposal_saleamountgreater',
            ALERT_DISPQTYGREATER  : 'client_disposal_cannotbegreater',
            ALERT_ZEROVAL         : 'client_greater_than_zerovalue'});
        
        if (nlapiGetFieldValue('custpage_assetid')) {
            if (+nlapiGetFieldValue('custpage_assetstatus') === FAM.AssetStatus.Disposed){
                this.disableFields(true, true);
                alert(this.alertMessage.ALERT_ALREADYDISPOSED);
            }else{
                nlapiSetFieldValue('custpage_assetquantdisposed', nlapiGetFieldValue('custpage_assetquantity'));
            }
        }else{
            this.disableFields(true, true);
        }
        
        this.currSymbol = FAM.SystemSetup.getSetting('nonDecimalCurrencySymbols');
        
    };

    /**
     * field changed event of client scripts
     *
     * @param {String} type - Sublist internal id
     * @param {String} name - Field internal id
     * @param {Number} linenum - Optional line item number, starts from 1
    **/
    this.fieldChanged = function (type, name, linenum) {
        if (name == 'custpage_assetid') {
            // DOM Hacks! any workarounds?
            setWindowChanged(window, false);
            main_form.submit();
        }
        else if (name == 'custpage_disposaltype') {
            var disposalType = nlapiGetFieldValue('custpage_disposaltype');
            if( disposalType == FAM.DisposalType['Sale']) {
                this.disableFields(false);
            } else if ( disposalType == FAM.DisposalType['Write Off'] ) {
                this.disableFields(true);
            } else { //If blank
                this.disableFields(false);
            }
        } 
        else if (name == 'custpage_salesamount' && nlapiGetFieldValue('custpage_salesamount')) {
            nlapiSetFieldValue('custpage_salesamount', 
                               FAM.Util_Shared.Math.roundByCurrency(nlapiGetFieldValue('custpage_salesamount'),
                                                                    nlapiGetFieldValue('custpage_currsymbol'),
                                                                    this.currSymbol),
                               false);
        }
    };

    /**
     * save Record event type of client scripts
    **/
    this.saveRecord = function() {
        var returnVal = true;
        var assetSubsidiary = nlapiGetFieldValue('custpage_subsidiary');
        var selLocation = nlapiGetFieldValue('custpage_filterslocation');
        var disposalType = nlapiGetFieldValue('custpage_disposaltype');
        var errParam = [];
        
        if (disposalType == FAM.DisposalType['Sale']) {
            if( !nlapiGetFieldValue('custpage_disposalitem')) {
                errParam.push(nlapiGetField('custpage_disposalitem').getLabel());
            }
            if( !nlapiGetFieldValue('custpage_customer')) {
                errParam.push(nlapiGetField('custpage_customer').getLabel());
            }
            if ( !nlapiGetFieldValue('custpage_salesamount')) {
                errParam.push(nlapiGetField('custpage_salesamount').getLabel());
            }
            // Check if location is mandatory when it has no value and depending on the disposal type
            if (!selLocation && this.isLocationMandatory(disposalType)) {
            	errParam.push(nlapiGetField('custpage_filterslocation').getLabel());
            }    
            if(errParam.length) {
                alert(FAM.Util_Shared.String.injectMessageParameter(this.alertMessage.ALERT_PLEASEENTER,
                        [errParam.join(', ')]));
                returnVal = false;
                return returnVal;
            }
            // check if customer is in same subsidiary as the asset
            if (FAM.Context.blnOneWorld && assetSubsidiary){
                var custSubsidiary = nlapiLookupField('customer', nlapiGetFieldValue('custpage_customer'), 'subsidiary');
                if (custSubsidiary != assetSubsidiary) {
                    errParam.push(nlapiGetField('custpage_customer').getLabel());
                }
            }
        }
        else {
        	// Check if location is mandatory when it has no value and depending on the disposal type
            if (!selLocation && this.isLocationMandatory(disposalType)) {
            	errParam.push(nlapiGetField('custpage_filterslocation').getLabel());
            }    
            if(errParam.length) {
                alert(FAM.Util_Shared.String.injectMessageParameter(this.alertMessage.ALERT_PLEASEENTER,
                        [errParam.join(', ')]));
                returnVal = false;
                return returnVal;
            }	
        }
        
        // check if location is in same subsidiary as the asset
        if (FAM.Context.blnOneWorld && assetSubsidiary && selLocation) {
            if (!FAM.compareCDLSubsidiary('location', selLocation, assetSubsidiary)){
                errParam.push(nlapiGetField('custpage_filterslocation').getLabel());
            }
        }        
        if (errParam.length) {
            alert(FAM.Util_Shared.String.injectMessageParameter(this.alertMessage.ALERT_SUBSIDIARYMATCH,
                [errParam.join(' and ')]));
            returnVal = false;
            return returnVal;
        }
        
        // Check if Asset Transfer BGP instance for this asset already exists
        if (this.isAssetToBeDisposed(nlapiGetFieldValue('custpage_assetid'))) {
            alert(this.alertMessage.ALERT_BGPEXISTS);
            returnVal = false;
        }

        return returnVal;
    };
    
    /**
     * validateField event type of client scripts
    **/
    this.validateField = function (type, name, linenum) {
        var returnVal = true, retVal = {}, changedVal = nlapiGetFieldValue(name);
        
        if (name=='custpage_assetid') {
            // Check if Asset Transfer BGP instance for this asset already exists
            if (this.isAssetToBeDisposed(nlapiGetFieldValue('custpage_assetid'))) {
                alert(this.alertMessage.ALERT_BGPEXISTS);
                returnVal = false;
            }
            // Check if journal accounts exist in asset record
            if (!this.hasValidAccounts(changedVal)) {
                if(this.acctParam.length && !confirm(FAM.Util_Shared.String.injectMessageParameter(
                    this.alertMessage.ALERT_ACCTNOTEXIST, ['\n'+this.acctParam.join('\n')]))) {
                        this.acctParam = [];
                        returnVal = false;
                }
            }
            
        }
        else if (name == 'custpage_disposaldate' && !!changedVal) {
            var recLDD =  nlapiStringToDate(nlapiGetFieldValue('custpage_rec_ldd')).getTime(), 
                dispDate = nlapiStringToDate(changedVal).getTime();
            retVal = this.validateDisposalDate(dispDate, recLDD);
        }
        else if (name == 'custpage_salesamount' && !!changedVal){
            retVal = this.validateSalesAmount(+changedVal);
        }
        else if (name == 'custpage_assetquantdisposed' && !!changedVal){
            var assetQty = nlapiGetFieldValue('custpage_assetquantity');
            retVal = this.validateDisposalQuantity(+changedVal, +assetQty, nlapiGetField('custpage_assetquantdisposed').getLabel());
        }
        if(retVal.msg){
            alert(retVal.msg);
            returnVal = false;
        }
        
        return returnVal;
        
    };
    
    this.validateDisposalDate = function(dd, ldd){
        var retVal = {};
        if(dd < ldd){
            retVal.msg = FAM.Util_Shared.String.injectMessageParameter(this.alertMessage.ALERT_EARLIERTHANLDD,
                                [nlapiDateToString(new Date(dd)), nlapiDateToString(new Date(ldd))]);
        }
        return retVal;
    };
    
    this.validateSalesAmount = function(saleAmt){
        var retVal = {};
        if(saleAmt <= 0){
            retVal.msg = this.alertMessage.ALERT_SALEAMTPOSITIVE;
        }
        
        return retVal;
    };
    
    this.validateDisposalQuantity = function(dispQty, assetQty, dispQtyLbl){
        var retVal = {};
        if(dispQty > assetQty){
            retVal.msg = this.alertMessage.ALERT_DISPQTYGREATER;
        }else if (dispQty <= 0){
            retVal.msg = FAM.Util_Shared.String.injectMessageParameter(this.alertMessage.ALERT_ZEROVAL, [dispQtyLbl]);
        }
        return retVal;
    };

    /**
     * Disable Screen field
     *
     * @param {boolean} disable - True: disables field and also clears field content, false: enables field
     * @returns {Void}
     */
    this.disableFields = function (disable, all) {
        nlapiDisableField('custpage_disposalitem', disable);
        nlapiDisableField('custpage_customer', disable);
        nlapiDisableField('custpage_salesamount', disable);
        nlapiDisableField('custpage_taxcode', disable);
        if(disable) {
            nlapiSetFieldValue('custpage_disposalitem', '');
            nlapiSetFieldValue('custpage_customer', '');
            nlapiSetFieldValue('custpage_salesamount', '');
            nlapiSetFieldValue('custpage_taxcode', '');
        }
        
        if(all){
            nlapiDisableField('custpage_disposaldate', disable);
            nlapiDisableField('custpage_disposaltype', disable);
            nlapiDisableField('custpage_assetquantdisposed', disable);
            nlapiDisableField('custpage_filterslocation', disable);
        }
    };
    
    /**
     * Checks if there is a BG - Process Instance record where Process Name: 'Asset Disposal',
     * Process Status: 'Queued' or 'In Progress' and the Asset Id (from Process State) is the Asset
     * Id on the form.
     *
     * Parameters:
     *     assetId {number} - internal id of the asset
     * Returns:
     *     boolean
    **/
    this.isAssetToBeDisposed = function (assetId) {
        if (!assetId) {
            return false;
        }

        var fil = [
            new nlobjSearchFilter('custrecord_far_proins_functionname', null, 'is', 'famAssetDisposal'),
            new nlobjSearchFilter('custrecord_far_proins_recordid', null, 'equalto', assetId),
            new nlobjSearchFilter('custrecord_far_proins_procstatus', null, 'anyof',
                [FAM.BGProcessStatus.InProgress, FAM.BGProcessStatus.Queued]),
            new nlobjSearchFilter('isinactive', null, 'is', 'F')
        ];

        if (nlapiSearchRecord('customrecord_bg_procinstance', null, fil)) {
            return true;
        }

        return false;
    };
    
    /**
     * Checks if Asset and Tax Methods in that Asset has the required journal accounts set.
     *
     * Parameters:
     *      assetId {number} - internal id of the asset
     * Returns:
     *      boolean
     **/
    this.hasValidAccounts = function (assetId) {
        if (!assetId) {
            return false;
        }
        
        // Check if asset id is in cache
        if(this.resultCache.hasOwnProperty(assetId)){
            this.acctParam = this.resultCache[assetId].errParam;
            return false;
        }
        
        // Check asset accounts
        var asset = nlapiLoadRecord('customrecord_ncfar_asset',assetId);
        var fields = ['custrecord_assetwriteoffacc.isinactive',
                      'custrecord_assetdisposalacc.isinactive',
                      'custrecord_assetmainacc.isinactive',
                      'custrecord_assetdepracc.isinactive'];
        var strIsInactive = '.isinactive';
        // check if account is inactive and has no value in asset
        var fldInactive = nlapiLookupField('customrecord_ncfar_asset', assetId, fields), val;
        for (var name in fldInactive) {
            val = name.substr(0, name.indexOf(strIsInactive)); //get string before '.isinactive'
            if (fldInactive[name] == 'T' || !asset.getFieldValue(val)) {
                this.acctParam.push(asset.getFieldValue('name') + ' ' + asset.getFieldValue('altname'));
                break;
            }
        }
        
        // Check tax accounts
        var fSearch = new FAM.Search(new FAM.AltDeprMethod_Record());
        fSearch.filters = 
            [   ['isinactive', 'is', 'F'], 'and', 
                ['custrecord_altdeprasset', 'is', assetId], 'and',
                [   ['custrecord_altdepr_accountingbook', 'noneof', '@NONE@'], 'and',
                    [   ['custrecord_altdepr_writeoffaccount', 'anyof', '@NONE@'], 'or',
                        ['custrecord_altdepr_disposalaccount', 'anyof', '@NONE@'], 'or',
                        ['custrecord_altdepr_assetaccount', 'anyof', '@NONE@'], 'or',
                        ['custrecord_altdepr_depraccount', 'anyof', '@NONE@']   
                    ], 'or',
                    [   ['custrecord_altdepr_writeoffaccount.isinactive', 'is', 'T'], 'or',
                        ['custrecord_altdepr_disposalaccount.isinactive', 'is', 'T'], 'or',
                        ['custrecord_altdepr_assetaccount.isinactive', 'is', 'T'], 'or',
                        ['custrecord_altdepr_depraccount.isinactive', 'is', 'T']
                    ]
                ]
            ];
        fSearch.addColumn('booking_id');
        fSearch.addColumn('alternate_method');
        fSearch.addColumn('depr_method');
        fSearch.run();

        var msgParam, bookId, altMethod, deprMethod;
        if (fSearch.results) {
            for(var i = 0; i < fSearch.results.length; i++){
                bookId = fSearch.getText(i, 'booking_id');
                altMethod = fSearch.getText(i, 'alternate_method');
                deprMethod = fSearch.getText(i, 'depr_method');
                
                msgParam = bookId + ' (' + altMethod + ': ' + deprMethod + ')';
                this.acctParam.push(msgParam);
            }
        }
        
        this.resultCache[assetId]={errParam: this.acctParam}; //cache the results
        if(this.acctParam.length) {
            return false;
        }
        
        return true;
    };

    /**
     * Sets the location field to be mandatory based on the following conditions: 
     * - Disposal Type == Sale | Location is required if either:
     * 		- MLI = T || Make Locations Mandatory = T
     * - Disposal Type == Write-off | Location is required if:
     * 		-Make Locations Mandatory = T
     * 
     * @returns boolean
     */
    this.isLocationMandatory = function(disposalType) {
    	var retVal = false;
    	
    	if (disposalType == FAM.DisposalType['Sale']
    		&& (this.isMultiLocInvt || this.isPrefLocMandatory)) {
    			retVal = true;
        }
    	else if (disposalType == FAM.DisposalType['Write Off'] 
    		&& this.isPrefLocMandatory) {
        		retVal = true;
        }
        
    	return retVal;
    };
    
};