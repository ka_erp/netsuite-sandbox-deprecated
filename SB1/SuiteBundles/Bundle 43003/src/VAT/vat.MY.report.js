/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var VAT = VAT || {};
VAT.MY = VAT.MY || {};
VAT.MY.ENG = VAT.MY.ENG || {};
VAT.MY.MAY = VAT.MY.MAY || {};

VAT.MY.RealizedGLSummary = function _RealizedGLSummary() { 
	return { 
		NetAmount: 0,
		grossAmount: 0,
		taxAmount: 0
	}; 
};

VAT.MY.RealizedGLSummaryDao = function _RealizedGLSummaryDao(params) {
	this.params = params;
};

VAT.MY.RealizedGLSummaryDao.prototype.getSummary = function _getSummary() {
	var realizedSummary = new VAT.MY.RealizedGLSummary();
	
	try {
		var filters = this.getPeriodFilters(this.params.periodIds, this.params.periodFrom, this.params.periodTo); 
		if (nlapiGetContext().getFeature('SUBSIDIARIES')) {
			filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', this.params.subId));
		}
		var sr = nlapiSearchRecord('transaction', 'customsearch_my_realizedgl_summary', filters);		
		if (sr && sr.length > 0) {
			realizedSummary.NetAmount = sr[0].getValue('rgamount', null, 'SUM') || 0;
		}
	} catch (ex) {
		logException(ex, 'VAT.MY.RealizedGLSummaryDao.getSummary');
	}
	
	return realizedSummary;
};

VAT.MY.RealizedGLSummaryDao.prototype.getPeriodFilters = function _getPeriodFilters(periodIds, periodFrom, periodTo) { //rgl taxperiod or date
	var filters = [];
	if (periodIds.length == 1) {
		filters.push(new nlobjSearchFilter('taxperiod', 'rgpostingtransaction', 'is', periodIds[0], null, 2, 1, true));
	} else {
		for (var iperiod = 0; iperiod < periodIds.length; iperiod++) {
			var leftparens = (iperiod == 0) ? 2 : 0;
			var rightparens = ((iperiod + 1) == periodIds.length) ? 1 : 0;
			filters.push(new nlobjSearchFilter('taxperiod', 'rgpostingtransaction', 'is', periodIds[iperiod], null, leftparens, rightparens, true));
			
		}
	}
	filters.push(new nlobjSearchFilter('trandate', 'rgpostingtransaction', 'within', periodFrom, periodTo, 1, 2));
	return filters;
};

VAT.MY.RealizedGL = function _RealizedGL() {
	return { //same order with drilldown details
		taxcode: 'ES43', 
		date: '',
		name: '',
		number: '',
		type: '',
		netAmount: 0,
		taxAmount: 0,
		notionalAmount: 0,
		typeId: '', 
		taxType: 'ES43',
		timeStamp: '',
		typeInternalId: '',
		tranDateNumber: '',
		tranNumber: '', 
		memo: '',
		quantity: '',
		customsRegNo: '',
		shipCountry: '',
		toArray: function() {
			var array = [];
			for (var obj in this) {
				if (!isFunction(this[obj])) {
					array.push(this[obj]);
				}
			}
			return array;
		}
	};
};

function isFunction(functionToCheck) {
	 var getType = {};
	 return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
};

VAT.MY.RealizedGLDao = function _RealizedGLDao(params) {
	this.tranTypes = this.loadTranTypes();
	this.params = params;
	this.dateCache = {};
	this.resultSet = {};

	try {
		var search = nlapiLoadSearch('transaction', 'customsearch_my_realizedgl');
		var filters = this.getPeriodFilters(this.params.periodIds, this.params.periodFrom, this.params.periodTo);

		if (nlapiGetContext().getFeature('SUBSIDIARIES')) {
			filters.push(new nlobjSearchFilter('subsidiary', null, 'anyof', this.params.subId));
		}
		search.addFilters(filters);
		this.resultSet = search.runSearch();
	} catch (ex) {
		logException(ex, 'VAT.MY.RealizedGLDao');
	}
};

VAT.MY.RealizedGLDao.prototype.getPeriodFilters = function _getPeriodFilters(periodIds, periodFrom, periodTo) { //rgl taxperiod or date
	var filters = [];
	if (periodIds.length == 1) {
		filters.push(new nlobjSearchFilter('taxperiod', 'rgpostingtransaction', 'is', periodIds[0], null, 2, 1, true));
	} else {
		for (var iperiod = 0; iperiod < periodIds.length; iperiod++) {
			var leftparens = (iperiod == 0) ? 2 : 0;
			var rightparens = ((iperiod + 1) == periodIds.length) ? 1 : 0;
			filters.push(new nlobjSearchFilter('taxperiod', 'rgpostingtransaction', 'is', periodIds[iperiod], null, leftparens, rightparens, true));
			
		}
	}
	filters.push(new nlobjSearchFilter('trandate', 'rgpostingtransaction', 'within', periodFrom, periodTo, 1, 2));
	return filters;
};

VAT.MY.RealizedGLDao.prototype.getList = function _getList(startIndex, endIndex) {
	var realizedGLDetails = [];
	try {
		var sr = this.resultSet.getResults(startIndex, endIndex);
		for (var isr = 0; (sr && isr < sr.length); isr++) {
			var realizedGLDetail = this.convertToRealizedGL(sr[isr]);
			realizedGLDetails.push(realizedGLDetail.toArray());
		}
	} catch (ex) {
		logException(ex, 'VAT.MY.RealizedGLDao.getList');
	}
	return realizedGLDetails;
};

VAT.MY.RealizedGLDao.prototype.convertToRealizedGL = function _convertToRealizedGL(row) {
	var realizedGL = new VAT.MY.RealizedGL();
	try {
		var date = row.getValue('trandate', 'rgpostingtransaction');
		if (!this.dateCache[date]) {
			this.dateCache[date] = {};
			this.dateCache[date].value = nlapiStringToDate(date);
			this.dateCache[date].valueOf = this.dateCache[date].value.valueOf(); 
			this.dateCache[date].text = this.dateCache[date].value.toString(this.params.dateFormat);
		}
		realizedGL.date = this.dateCache[date].text;
		realizedGL.tranDateNumber = this.dateCache[date].valueOf;
		realizedGL.name = row.getText('entity', 'rgpostingtransaction');
		realizedGL.number = row.getValue('transactionnumber', 'rgpostingtransaction');
		realizedGL.tranNumber = realizedGL.number;
		realizedGL.type = row.getText('type', 'rgpostingtransaction');
		realizedGL.typeId = this.tranTypes[realizedGL.type] ? this.tranTypes[realizedGL.type].id : '';
		realizedGL.typeInternalId = this.tranTypes[realizedGL.type] ? this.tranTypes[realizedGL.type].internalid : '';
		realizedGL.netAmount = row.getValue('rgamount');
		realizedGL.timeStamp = row.getValue('custbody_report_timestamp', 'rgpostingtransaction');
		realizedGL.memo = row.getValue('memo', 'rgpostingtransaction');
		realizedGL.quantity = row.getValue('quantity', 'rgpostingtransaction');
		realizedGL.customsRegNo = row.getValue('custbody_4110_customregnum', 'rgpostingtransaction');
		realizedGL.shipCountry = row.getValue('shipcountry', 'rgpostingtransaction');
	} catch (ex) {
		logException(ex, 'VAT.MY.RealizedGLDao.convertToRealizedGL');
	}
	return realizedGL;
};

VAT.MY.RealizedGLDao.prototype.loadTranTypes = function _loadTranTypes() {
	var tranTypes = {};
	try {
		var filters = [new nlobjSearchFilter("custrecord_map_type", null, "is", "TXN")];
		var columns = [
			new nlobjSearchColumn("name"),
			new nlobjSearchColumn("custrecord_internal_id"), 
			new nlobjSearchColumn("custrecord_transaction_name")
		];

		var rs = nlapiSearchRecord("customrecord_tax_report_map", null, filters, columns);

		for(var irow = 0; irow < (rs && rs.length); irow++) {
			tranTypes[rs[irow].getText('custrecord_transaction_name')] = {id: rs[irow].getValue('name'), internalid:rs[irow].getValue('custrecord_internal_id')};
		}
	} catch (ex) {
		logException(ex, 'VAT.MY.RealizedGLDao.loadTranTypes');
	}
	return tranTypes;
};

VAT.MY.RealizedGLReader = function _RealizedGLReader(realizedGlDao) {
	this.dao = realizedGlDao;
	this.ENTRIES_PER_PAGE = 1000;
	this.context = nlapiGetContext();
};

VAT.MY.RealizedGLReader.prototype.getDetails = function() {
	var index = 0;
	var details = [];
	var list = [];
	try {
		do {
			list = this.dao.getList(index, index + this.ENTRIES_PER_PAGE);
			if (!list) {
				return details;
			}

			details = details.concat(list);
			
			if (this.isThresholdReached()) {
				return details;
			}
			
			index += this.ENTRIES_PER_PAGE;
		} while (list.length >= this.ENTRIES_PER_PAGE);
	} catch (ex) {
		logException(ex, 'VAT.MY.RealizedGLReader.getDetails');
	}
	return details;
};

VAT.MY.RealizedGLReader.prototype.isThresholdReached = function() { 
	if (this.context.getRemainingUsage() > 100) {
		return false;
	}
	return true;
};

VAT.MY.BaseData = function(params) {
    this.CountryCode = 'MY';
    VAT.ReportData.call(this, params);
};
VAT.MY.BaseData.prototype = Object.create(VAT.ReportData.prototype);

VAT.MY.BaseData.prototype.ProcessReportData = function(data, headerData) {
    var ds = {};
    ds.ToPeriodId = this.periodTo;
    ds.FromPeriodId = this.periodFrom;
    ds.SubId = this.subId;
    ds.ReportIndex = this.className;
    
	for(var i in data) {
		if (data[i]) {
			ds[i] = i.indexOf('rate') > -1 ? data[i] : nlapiFormatCurrency(data[i]);
		} else {
			ds[i] = nlapiFormatCurrency(0);
		}
	}
	    
    for(var j in headerData) {
        ds[j] = headerData[j];
    }
    
    return ds;
};

VAT.MY.BaseData.prototype.ProcessPrintData = function(data) {
	for (var k in this.otherParams) {
		if (this.otherParams[k]) {
			data[k] = this.otherParams[k];
			if (k.indexOf('chk') > -1) {
				data[k] = data[k] == 'T' ? 'X' : '';
			}
		} else {
			data[k] = '';
		}
	}
	return data;
};

VAT.MY.Report = VAT.MY.Report || {};
VAT.MY.Report.Data = function(params) {
    var _CountryCode = 'MY'; 
    this.TaxMap = {
        	box5b: {id: 'box5b', label: '5b', taxcodelist: [{taxcode: 'SR', available: 'SALE'}, {taxcode: 'DS', available: 'SALE'}, {taxcode: 'AJS', available: 'SALE'}]},
        	box6b: {id: 'box6b', label: '6b', taxcodelist: [{taxcode: 'TX', available: 'PURCHASE'}, {taxcode: 'IM', available: 'PURCHASE'}, {taxcode: 'TX-E43', available: 'PURCHASE'}, 
        	                                                {taxcode: 'TX-CAP', available: 'PURCHASE'}, {taxcode: 'TX-RE', available: 'PURCHASE'}, {taxcode: 'AJP', available: 'PURCHASE'}]},
        	box17: {id: 'box17', label: '17', taxcodelist: [{taxcode: 'AJP', available: 'PURCHASE'}]},
        	box18: {id: 'box18', label: '18', taxcodelist: [{taxcode: 'AJS', available: 'SALE'}]}
    };
    this.TaxDefinition = {
    	TX: function(taxcode) { // 6a, 6b
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForPurchase && taxcode.Rate > 0 && taxcode.IsCategoryType('S0', true);
    	},
    	'TX-RE': function(taxcode) { // 6a, 6b
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForPurchase && taxcode.Rate > 0 && taxcode.IsCategoryType('S9');
    	},
    	'TX-CAP': function(taxcode) { // 6a, 6b, 16
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForPurchase && taxcode.Rate > 0 && taxcode.IsCategoryType('S2');
    	},    	
    	'TX-E43': function(taxcode) { // 6a, 6b
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForPurchase && taxcode.Rate > 0 && taxcode.IsCategoryType('S4') && taxcode.IsExempt;
    	}, 
    	IM: function(taxcode) { // 6a, 6b
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForPurchase && taxcode.Rate > 0 && taxcode.IsCategoryType('S0', true) && taxcode.IsImport;
    	},
    	AJP: function(taxcode) { // 6b, 17
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForPurchase && taxcode.Rate > 0 && taxcode.IsCategoryType('S5');
    	},
    	IS: function(taxcode) { // 14, 15
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForPurchase && taxcode.Rate == 0 && taxcode.IsCategoryType('S1') && taxcode.IsImport;
    	},
    	AJS: function(taxcode) { // 5b, 18
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForSales && taxcode.Rate > 0 && taxcode.IsCategoryType('S7');
    	},
    	SR: function(taxcode) { // 5a, 5b
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForSales && taxcode.Rate > 0 && taxcode.IsCategoryType('S0', true);
    	},
    	DS: function(taxcode) { // 5a, 5b
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForSales && taxcode.Rate > 0 && taxcode.IsCategoryType('S0', true);
    	},
    	RS: function(taxcode) { // 13
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForSales && taxcode.Rate == 0 && taxcode.IsCategoryType('S8');
    	},
    	ES43: function(taxcode) { // 12
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForSales && taxcode.Rate == 0 && taxcode.IsCategoryType('S4') && taxcode.IsExempt;
    	},
    	ES: function(taxcode) { // 12 
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForSales && taxcode.Rate == 0 && taxcode.IsCategoryType('S0', true) && taxcode.IsExempt;
    	},    	
    	ZRE: function(taxcode) { // 11
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForSales && taxcode.Rate == 0 && taxcode.IsCategoryType('S0', true) && taxcode.IsForExport;
    	},
    	ZRL: function(taxcode) { // 10
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForSales && taxcode.Rate == 0 && taxcode.IsCategoryType('S0', true) && !taxcode.IsForExport;
    	},
    	OS: function(taxcode) { // no box, part of TX-RE computation
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForSales && taxcode.Rate == 0 && taxcode.IsCategoryType('S10');
    	},
    	GS: function(taxcode) { // no box, part of TX-RE computation
    		return taxcode.CountryCode == _CountryCode && taxcode.IsForSales && taxcode.Rate == 0 && taxcode.IsCategoryType('S11');
    	}
    };
    VAT.MY.BaseData.call(this, params);
    this.periodFromDate = new SFC.System.TaxPeriod(this.periodFrom).GetStartDate();
    this.periodToDate = new SFC.System.TaxPeriod(this.periodTo).GetEndDate();
    this.periodIds = getCoveredPeriodIds(this.periodFromDate, this.periodToDate);    
};
VAT.MY.Report.Data.prototype = Object.create(VAT.MY.BaseData.prototype);

VAT.MY.Report.Data.prototype.GetData = function() {
    var _DR = this.DataReader;
    var sales = _DR.GetSalesSummary();
    var purchases = _DR.GetPurchaseSummary();
    var salesadj = _DR.GetSalesAdjustmentSummary(this.TaxMap);
    var purchaseadj = _DR.GetPurchaseAdjustmentSummary(this.TaxMap);
    var realizedGLSummaryDao = new VAT.MY.RealizedGLSummaryDao({periodIds: this.periodIds, periodFrom: this.periodFromDate, periodTo: this.periodToDate, subId: this.subId}).getSummary();
    var standardGSTrate = .06;
    var obj = {};
	var emptyboxes = ['box19b', 'box19c', 'box19d', 'box19e', 'box19others', 
					  'box19b_percent', 'box19c_percent', 'box19d_percent', 'box19e_percent', 'box19others_percent'];
	var reclaimableTXREResult = this.GetReclaimableTXRE(sales, purchases.Of('TX-RE').TaxAmount);
	
	for ( var iempty = 0; iempty < emptyboxes.length; iempty++) {
		obj[emptyboxes[iempty]] = 0;
	}
    obj.box5a = sales.Accrue(['SR', 'DS']).NetAmount; 
    obj.box5b = sales.Accrue(['SR', 'DS', 'AJS']).TaxAmount + salesadj.Accrue('box5b', ['SR', 'DS', 'AJS']).TaxAmount;
    obj.box6a = purchases.Accrue(['TX', 'IM', 'TX-E43', 'TX-RE', 'TX-CAP']).NetAmount;
    obj.box10 = sales.Of('ZRL').NetAmount;
    obj.box11 = sales.Of('ZRE').NetAmount;
    obj.box12 = sales.Accrue(['ES', 'ES43']).NetAmount + parseFloat(realizedGLSummaryDao.NetAmount);
    obj.box13 = sales.Of('RS').NetAmount;
    obj.box14 = purchases.Of('IS').NetAmount;
    obj.box16 = purchases.Of('TX-CAP').NetAmount;
    obj.box17 = purchases.Of('AJP').NetAmount;
    obj.box18 = sales.Of('AJS').NetAmount;
    obj.box6b = purchases.Accrue(['TX', 'IM', 'TX-E43', 'TX-CAP', 'AJP']).TaxAmount 
    			+ reclaimableTXREResult.reclaimabletxre
    			+ purchaseadj.Accrue('box6b', ['TX', 'IM', 'TX-E43', 'TX-CAP', 'TX-RE', 'AJP']).TaxAmount;
    obj.box15 = obj.box14 * standardGSTrate;
    
    if (obj.box5b > obj.box6b){
    	obj.box7 = obj.box5b - obj.box6b;
    	obj.box8 = 0;
	}else{
		obj.box7 = 0;
		obj.box8 = obj.box6b - obj.box5b;
	}
    
    obj.box19a = obj.box5b;
    obj.box19total = obj.box19a;
    obj.box19a_percent = obj.box19a ? 100 : 0;
    obj.irrpercent = obj.box6b ? reclaimableTXREResult.irrpercent : 0;
    return obj;
};

VAT.MY.Report.Data.prototype.GetReclaimableTXRE = function(salesSummary, txre_tax_amount) {
	var reclaimableResults = {
		reclaimabletxre: 0,
		irrpercent: 0
	};
	var ringgitThreshold = 5000;
	var percentageThreshold = .05;
	var totalSalesNetAmount = salesSummary.Accrue(['SR', 'ZRL', 'ZRE', 'DS', 'OS', 'RS', 'GS']).NetAmount;
	var netAmountES = salesSummary.Of('ES').NetAmount;
	var totalSalesNetAmountWithES = (totalSalesNetAmount + netAmountES) || 1; 
	
	if (netAmountES <= ringgitThreshold 
		&& netAmountES / totalSalesNetAmountWithES <= percentageThreshold) {
		reclaimableResults.irrpercent = 100;
		reclaimableResults.reclaimabletxre = txre_tax_amount;
	} else {
		reclaimableResults.irrpercent = (totalSalesNetAmount / totalSalesNetAmountWithES) * 100;
		reclaimableResults.reclaimabletxre = txre_tax_amount * (totalSalesNetAmount / totalSalesNetAmountWithES);
	}
	return reclaimableResults;
};


VAT.MY.Report.Data.prototype.GetHeaderData = function() {
	var dataHeader = this.DataHeader;
	dataHeader.StartDate = new Date(dataHeader.StartDate).toString(dataHeader.shortdate);
	dataHeader.EndDate = new Date(dataHeader.EndDate).toString(dataHeader.shortdate);
    return dataHeader;
};

VAT.MY.Report.Data.prototype.GetDrilldownData = function(boxNumber) {
    var _DR = this.DataReader;
    var ds = {};
    var data = [];
	var realizeGLDetails = [];
	
	if (!this.saleCacheId) {
		var realizedGLDao = new VAT.MY.RealizedGLDao({periodIds: this.periodIds, periodFrom: this.periodFromDate, periodTo: this.periodToDate, subId: this.subId});
		realizeGLDetails = new VAT.MY.RealizedGLReader(realizedGLDao).getDetails();
	} 

	switch (boxNumber) {
		case 'box5a': 
			data = _DR.GetSalesDetails(['SR', 'DS']);
			break;
		case 'box5b': 
			data = _DR.GetSalesDetails(['SR', 'DS', 'AJS'], 'box5b'); 
			break;
		case 'box6a':
			data = _DR.GetPurchaseDetails(['TX', 'IM', 'TX-E43', 'TX-RE', 'TX-CAP']);
			break;
		case 'box10':
			data = _DR.GetSalesDetails(['ZRL']);
			break;
		case 'box11':
			data = _DR.GetSalesDetails(['ZRE']);
			break;
		case 'box12':
			data = _DR.GetSalesDetails(['ES', 'ES43']).concat(realizeGLDetails);
			break;
		case 'box13':
			data = _DR.GetSalesDetails(['RS']);
			break;
		case 'box14':
			data = _DR.GetPurchaseDetails(['IS']);
			break;
		case 'box16':
			data = _DR.GetPurchaseDetails(['TX-CAP']);
			break;
		case 'box17':
			data = _DR.GetPurchaseDetails(['AJP'], 'box17');
			break;
		case 'box18':
			data = _DR.GetSalesDetails(['AJS'], 'box18');
			break;
		case 'box6b':
			data = _DR.GetPurchaseDetails(['TX', 'IM', 'TX-E43', 'TX-CAP', 'AJP', 'TX-RE'], 'box6b');
			break;
	}
    ds.ReportData = data;
    return ds;
};

VAT.MY.BaseReport = function() {
	this.isNewFramework = true;
    this.CountryCode = 'MY';
    this.Type = 'VAT';
    VAT.Report.call(this);
};
VAT.MY.BaseReport.prototype = Object.create(VAT.Report.prototype);

VAT.MY.ENG.Report = function() {
	this.LanguageCode = 'ENG';
	this.ReportType = 'Report';
	this.metadata = {
		template: {'pdf': 'VAT_PDF_MY_ENG', 'html': 'VAT_HTML_MY_ENG'}
	};
    this.ClassName = 'VAT.MY.ENG.Report';    
    this.Name = 'Malaysia (English)';
    this.ReportName = 'GST Tax Return';
    this.isAdjustable = true;
    VAT.MY.BaseReport.call(this);
};
VAT.MY.ENG.Report.prototype = Object.create(VAT.MY.BaseReport.prototype);

VAT.MY.MAY.Report = function() {
	this.LanguageCode = 'MAY';
	this.ReportType = 'Report';
    this.metadata = {
    	template: {'pdf': 'VAT_PDF_MY_MAY', 'html': 'VAT_HTML_MY_MAY'}
    };
    this.ClassName = 'VAT.MY.MAY.Report';
    this.Name = 'Malaysia (Bahasa Malaysia)';
    this.ReportName = 'GST Tax Return';
    this.isAdjustable = true;
    VAT.MY.BaseReport.call(this);
};
VAT.MY.MAY.Report.prototype = Object.create(VAT.MY.BaseReport.prototype);