/**
 * Copyright 2015 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */
var Tax = Tax || {};
Tax.Supplementary = Tax.Supplementary || {};
Tax.Supplementary.VCS = Tax.Supplementary.VCS || {};
Tax.Supplementary.VCS.SK = Tax.Supplementary.VCS.SK || {};
Tax.Supplementary.VCS.SK.DAO = Tax.Supplementary.VCS.SK.DAO || {};
Tax.Supplementary.VCS.SK.Adapter = Tax.Supplementary.VCS.SK.Adapter || {};
Tax.Supplementary.VCS.SK.Formatter = Tax.Supplementary.VCS.SK.Formatter || {};

/*****************
 * GENERIC STUFF *
 *****************/
var base = Tax.Supplementary.VCS.SK;
var dao = base.DAO;
var adapter = base.Adapter;
var formatter = base.Formatter;

base.context = nlapiGetContext();
base.isUnitsOfMeasureEnabled = base.context.getFeature('unitsofmeasure');

base.SKFileManager = function SKFileManager() {
    Tax.FileManager.call(this);
    this.Name = 'SKFileManager';
};

base.SKFileManager.prototype = Object.create(Tax.FileManager.prototype);

base.SKFileManager.prototype.process = function process(result, params) {
    var fileProperties = params.meta.file[params.format];
    fileProperties.format = params.format;
    fileProperties.filename = [params.filename, new Date().toString('ddMMyyyy_Hmmss')].join('_');
    fileProperties.folder = params.meta.folder;

    var fileId = this.createFile(fileProperties, result.rendered);
    var file = this.getFileById(fileId);
    result.fileUrl = file.getURL();
    result.filename = file.getName();
    return result;
};

/***********************
 * COMPANY INFORMATION *
 ***********************/
dao.CompanyInformationDAO = function CompanyInformationDAO() {
    Tax.DAO.BaseDAO.call(this);
    this.Name = 'VCS_SK_CompanyInformationDAO';
};

dao.CompanyInformationDAO.prototype = Object.create(Tax.DAO.BaseDAO.prototype);

dao.CompanyInformationDAO.prototype.getList = function getList(params) {
    if (!params) {
        throw nlapiCreateError('MISSING_REQUIRED_PARAMETER', 'A search parameter object is required.');
    }

    var company = null;

    if (params.subsidiary) {
        company = new Tax.DAO.SubsidiaryDAO().getList({
            id: params.subsidiary,
            bookId: params.bookid
        });
    } else {
        company = new Tax.DAO.CompanyInformationDAO().getList();
    }

    return company;
};

adapter.CompanyInformationAdapter = function CompanyInformationAdapter(errorHandler) {
    Tax.Adapter.BaseAdapter.call(this);
    this.errorHandler = errorHandler;
    this.Name = 'CompanyInformationAdapter';
    this.daos = ['VCS_SK_CompanyInformationDAO'];
};

adapter.CompanyInformationAdapter.prototype = Object.create(Tax.Adapter.BaseAdapter.prototype);

adapter.CompanyInformationAdapter.prototype.transform = function transform(params) {
    var rawData = this.rawdata[0] || {};
    var result = {};

    var startPeriod = new SFC.System.TaxPeriod(params.periodfrom);
    var startMonth = startPeriod.GetStartDate().getMonth() + 1;

    result[this.Name] = [{
        vatno: rawData.vrn ? rawData.vrn.replace(/[^A-Za-z0-9]/g, '').replace(/^SK/i, '') : '',
        periodmonth: startPeriod.GetType() == 'month' ? startMonth.toString().length == 2 ? startMonth.toString() : '0' + startMonth : '',
        periodquarter: startPeriod.GetType() != 'month' ? '' + (Math.ceil(startMonth / 3)) : '',
        periodyear: startPeriod.GetStartDate().getFullYear().toString() || '',
        companyname: rawData.legalName || rawData.nameNoHierarchy || '',
        addressstreet: rawData.address1 || '',
        addressnumber: rawData.address2 || '',
        addresspostcode: rawData.zip || '',
        addresscity: rawData.city || '',
        addressstate: rawData.countryCode || '',
        addresstelephone: rawData.telephone || '',
        addressemail: rawData.email || '',
        printmsg: this.getFooterText(true),
        excelmsg: this.getFooterText(false)
    }];

    return result;
};

adapter.CompanyInformationAdapter.prototype.getFooterText = function getFooterText(isPDF) {
    return [
        isPDF ? 'Printed by' : 'Exported by',
        base.context.getName(),
        '(' + base.context.getUser() + ')',
        'on',
        new Date().toString('MMMM d, yyyy')
    ].join(' ');
};

/******************************************
 * ISSUED INVOICES - A.1 and A.2 (Common) *
 ******************************************/

dao.CustomerInvoiceDAO = function CustomerInvoiceDAO() {
    Tax.DAO.ReportDAO.call(this);
    this.Name = 'VCS_SK_CustomerInvoiceDAO';
    this.reportName = 'VCS SK - Sales Summary by Tax Code and Txn';
};

dao.CustomerInvoiceDAO.prototype = Object.create(Tax.DAO.ReportDAO.prototype);

dao.CustomerInvoiceDAO.prototype.ListObject = function ListObject() {
    return {
        taxCode: '',
        entityTaxNumber: '',
        documentNumber: '',
        transactionNumber: '',
        date: '',
        netAmount: '',
        taxAmount: '',
        taxRate: '',
        commodityCode: '',
        typeOfGoods: '',
        quantity: '',
        unitOfMeasure: '',
        itemId: ''
    };
};

dao.CustomerInvoiceDAO.prototype.getColumnMetadata = function getColumnMetadata(pivotReport) {
    var column = this.ListObject();
    try {
        var pivotReportColumns = this.getColumns(pivotReport);
        column.taxCode = pivotReportColumns[0];
        column.entityTaxNumber = pivotReportColumns[1];
        column.documentNumber = pivotReportColumns[2];
        column.transactionNumber = pivotReportColumns[3];
        column.date = pivotReportColumns[4];
        column.netAmount = pivotReportColumns[5];
        column.taxAmount = pivotReportColumns[6];
        column.taxRate = pivotReportColumns[7];
        column.commodityCode = pivotReportColumns[8];
        column.typeOfGoods = pivotReportColumns[9];
        column.quantity = pivotReportColumns[10];
        column.unitOfMeasure = pivotReportColumns[11];
        column.itemId = pivotReportColumns[12];
    } catch (ex) {
        logException(ex, 'CustomerInvoiceDAO.getColumnMetadata');
        throw ex;
    }
    return column;
};

adapter.SectionAAdapter = function SectionAAdapter() {
    Tax.Adapter.BaseAdapter.call(this);
    this.Name = 'SectionAAdapter';
};

adapter.SectionAAdapter.prototype = Object.create(Tax.Adapter.BaseAdapter.prototype);

adapter.SectionAAdapter.prototype.transform = function transform(params) {
    try {
        var data = [];
        var line = null;

        for (var i = 0; i < this.rawdata.length; i++) {
            line = this.rawdata[i];
            line.index = i;

            if (this.taxCodeLookup.anyOf(line.taxCode, this.taxCodes)) {
                var transformed = this.getLineData(line);
                if (transformed) {
                    data.push(transformed);
                }
            }
        }

        nlapiLogExecution('DEBUG', this.Name || 'SectionAAdapter', JSON.stringify(data));

        var result = {};
        result[this.Name] = data;
        return result;
    } catch (ex) {
        logException(ex, 'SectionAAdapter.transform');
        throw ex;
    }
};

adapter.SectionAAdapter.prototype.getLineData = function getLineData(line) {
    return line;
};




/********************************
 * ITEMS - A.2 and C.1 (Common) *
 *******************************/

dao.ItemDAO = function ItemDAO() {
    Tax.DAO.RecordDAO.call(this);
    this.Name = 'ItemDAO';
    this.recordType = 'item';
};

dao.ItemDAO.prototype = Object.create(Tax.DAO.RecordDAO.prototype);

// TODO: Remove DAO processing in Model method/class
dao.ItemDAO.prototype.ListObject = function ListObject(row) {
    if (!row) {
        throw nlapiCreateError('INVALID_PARAMETER', 'Invalid ItemDAO params');
    }

    var listObject = {
        id: row.getValue('internalid'),
        unitsTypeId: row.getValue('unitstype')
    };
    return listObject;
};

dao.ItemDAO.prototype.prepareSearch = function prepareSearch(params) {
    try{
        if (!params || !params.itemIdList) {
            throw nlapiCreateError('INVALID_PARAMETER', 'Invalid ItemDAO params');
        }

        this.columns.push(new nlobjSearchColumn('internalid'));
        this.columns.push(new nlobjSearchColumn('unitstype'));

        if (params.itemIdList) {
            this.filters.push(new nlobjSearchFilter('internalid', null, 'anyof', params.itemIdList));
        }
    } catch (ex) {
        logException(ex, 'ItemDAO.prepareSearch');
        throw ex;
    }
};

dao.ItemDAO.prototype.process = function(result, params) {
    try{
        var combinedResult = {};
        combinedResult.savedReport = result.dao;

        if (base.isUnitsOfMeasureEnabled) {
            var itemIdList = [];

            for (var i = 0; i < result.dao.length; i++) {
                var line = result.dao[i];

                if (itemIdList.indexOf(line.itemId) == -1) {
                    itemIdList.push(line.itemId);
                }
            }

            combinedResult.items = itemIdList.length > 0 ? this.getList({ itemIdList: itemIdList }) : [];
        }

        var newResult = {
            dao: combinedResult
        };

        return newResult;
    } catch (ex) {
        logException(ex, 'ItemDAO.process');
        throw ex;
    }
};

dao.UnitsTypeDAO = function UnitsTypeDAO() {
    Tax.DAO.RecordDAO.call(this);
    this.Name = 'UnitsTypeDAO';
    this.recordType = 'unitstype';
};

dao.UnitsTypeDAO.prototype = Object.create(Tax.DAO.RecordDAO.prototype);

//TODO: Remove DAO processing in Model method/class
dao.UnitsTypeDAO.prototype.ListObject = function ListObject(row) {
    var listObject = {
        id: row.getValue('internalid'),
        name: row.getValue('name'),
        unitName: row.getValue('unitname'),
        isBaseUnit: row.getValue('baseunit'),
        abbreviation: row.getValue('abbreviation'),
        pluralAbbreviation: row.getValue('pluralabbreviation'),
        conversionRate: row.getValue('conversionrate')
    };
    return listObject;
};

dao.UnitsTypeDAO.prototype.prepareSearch = function prepareSearch(params) {
    if (!params || !params.unitsTypeIdList) {
        throw nlapiCreateError('INVALID_PARAMETER', 'Invalid UnitsTypeDAO params');
    }

    this.columns.push(new nlobjSearchColumn('internalid'));
    this.columns.push(new nlobjSearchColumn('name'));
    this.columns.push(new nlobjSearchColumn('unitname'));
    this.columns.push(new nlobjSearchColumn('baseunit'));
    this.columns.push(new nlobjSearchColumn('abbreviation'));
    this.columns.push(new nlobjSearchColumn('pluralabbreviation'));
    this.columns.push(new nlobjSearchColumn('conversionrate'));

    if (params.unitsTypeIdList) {
        this.filters.push(new nlobjSearchFilter('internalid', null, 'anyof', params.unitsTypeIdList));
    }
};

dao.UnitsTypeDAO.prototype.process = function(result, params) {
    var combinedResult = result.dao;

    if (base.isUnitsOfMeasureEnabled) {
        var unitsTypeIdList = [];

        for (var i = 0; i < result.dao.items.length; i++) {
            var item = result.dao.items[i];
            if (item.unitsTypeId && unitsTypeIdList.indexOf(item.unitsTypeId) == -1) {
                unitsTypeIdList.push(item.unitsTypeId);
            }
        }

        combinedResult.unitsTypes = unitsTypeIdList.length > 0 ? this.getList({ unitsTypeIdList: unitsTypeIdList }) : [];
    }

    var newResult = {
        dao: combinedResult
    };

    return newResult;
};

adapter.CONSTANTS = {
    ALLOWED_UOM: ['kg', 't', 'm', 'ks']
};

adapter.ItemSectionAdapter = function ItemSectionAdapter(errorHandler) {
    adapter.SectionAAdapter.call(this);
};

adapter.ItemSectionAdapter.prototype = Object.create(adapter.SectionAAdapter.prototype);

adapter.ItemSectionAdapter.prototype.getLineData = function getLineData(line) {
    // Override
};

adapter.ItemSectionAdapter.prototype.getUnitOfMeasure = function getUnitOfMeasure(uom) {
    if (uom && adapter.CONSTANTS.ALLOWED_UOM.indexOf(uom.toLowerCase()) > -1) {
        return uom;
    }

    return '';
};

adapter.ItemSectionAdapter.prototype.getCode = function getCode(param) {
    if(!param) {
        return '';
    }

    var matches = param.match(/([A-Z]*) - .*/);
    return matches[1];
};

adapter.ItemSectionAdapter.prototype.process = function process(result, params) {
    if(!result || !result.dao) {
        throw nlapiCreateError('MISSING_ARGUMENT', 'result.dao is required');
    }
    var dao = result.dao;

    this.rawdata = dao.savedReport;
    this.itemUnitsTypeMap = base.isUnitsOfMeasureEnabled ? this.createItemUnitsTypeMap(dao.items) : {};
    this.unitsTypeUnitMap = base.isUnitsOfMeasureEnabled ? this.createUnitsTypeUnitMap(dao.unitsTypes): {};

    return {
        dao: this.rawdata,
        adapter: this.transform(params)
    };
};

adapter.ItemSectionAdapter.prototype.createItemUnitsTypeMap = function createItemUnitsTypeMap(items) {
    if(!items) {
        throw nlapiCreateError('MISSING_ARGUMENT', 'items is required');
    }
    var itemUnitsTypeMap = {};

    for (var i = 0; i < items.length; i++) {
        var item = items[i];
        itemUnitsTypeMap[item.id] = item.unitsTypeId;
    }

    return itemUnitsTypeMap;
};

adapter.ItemSectionAdapter.prototype.createUnitsTypeUnitMap = function createUnitsTypeUnitMap(unitsTypes) {
    if(!unitsTypes) {
        throw nlapiCreateError('MISSING_ARGUMENT', 'unitsTypes is required');
    }
    try {
        var unitsTypeUnitMap = {};
    
        for (var i = 0; i < unitsTypes.length; i++) {
            var unit = unitsTypes[i];
    
            if (!unitsTypeUnitMap[unit.id]) {
                unitsTypeUnitMap[unit.id] = [];
            }
    
            unitsTypeUnitMap[unit.id].push({
                isBaseUnit: unit.isBaseUnit,
                abbreviation: unit.abbreviation,
                pluralAbbreviation: unit.pluralAbbreviation,
                conversionRate: unit.conversionRate
            });
        }
    
        return unitsTypeUnitMap;
    } catch (ex) {
        logException(ex, 'ItemSectionAdapter.createUnitsTypeUnitMap');
        throw ex;
    }    
};

adapter.ItemSectionAdapter.prototype.convertQuantity = function convertQuantity(itemId, quantity, unitOfMeasure) {
    try {
        var convertedQuantity = Math.abs(quantity);
    
        if (!unitOfMeasure || !quantity) {
            return convertedQuantity;
        }
    
        var unitTypeId = this.itemUnitsTypeMap[itemId];
    
        if (unitTypeId == undefined) {
            return convertedQuantity;
        }
    
        var unit = this.unitsTypeUnitMap[unitTypeId];
    
        if (unit == undefined) {
            return convertedQuantity;
        }
    
        for (var i = 0; i < unit.length; i++) {
            var uom = unit[i];
            if (uom.abbreviation == unitOfMeasure || uom.pluralAbbreviation == unitOfMeasure) {
                convertedQuantity = convertedQuantity / uom.conversionRate;
            }
        }
    
        return convertedQuantity;
    } catch (ex) {
        logException(ex, 'ItemSectionAdapter.convertQuantity');
        throw ex;
    }
};

/*************************
 * ISSUED INVOICES - A.1 *
 *************************/
adapter.SectionA1Adapter = function SectionA1Adapter(errorHandler) {
    adapter.SectionAAdapter.call(this);
    this.errorHandler = errorHandler;
    this.taxCodes = ['S', 'R'];
    this.taxCodeLookup = VAT.TaxCodeLookup.getInstance('SK', VAT.SK.TaxCodeDefinition());
    this.Name = 'SectionA1Adapter';
    this.daos = ['VCS_SK_CustomerInvoiceDAO'];
};

adapter.SectionA1Adapter.prototype = Object.create(adapter.SectionAAdapter.prototype);

adapter.SectionA1Adapter.prototype.getLineData = function getLineData(line) {
    if (!line) {
        throw nlapiCreateError('INVALID_PARAMETER', 'Invalid SectionA1 line data');
    }

    var data = {};
    data.line = line.index ? line.index++ : 0;
    data.entityTaxNumber = line.entityTaxNumber || '';
    data.transactionId = line.documentNumber || line.transactionNumber || '';
    data.transactionDate = line.date || '';
    data.netAmount = line.netAmount || '';
    data.taxAmount = line.taxAmount || '';
    data.taxRate = line.taxRate || '';
    data.correctionCode = '';
    return data;
};

/*************************
 * ISSUED INVOICES - A.2 *
 *************************/
adapter.SectionA2Adapter = function SectionA2Adapter(errorHandler) {
    adapter.ItemSectionAdapter.call(this);
    this.errorHandler = errorHandler;
    this.Name = 'SectionA2Adapter';
    this.taxCodes = ['RC'];
    this.taxCodeLookup = VAT.TaxCodeLookup.getInstance('SK', VAT.SK.TaxCodeDefinition());
};

adapter.SectionA2Adapter.prototype = Object.create(adapter.ItemSectionAdapter.prototype);

adapter.SectionA2Adapter.prototype.getLineData = function getLineData(line) {
    if (!line) {
        throw nlapiCreateError('INVALID_PARAMETER', 'Invalid SectionA2Adapter line data');
    }

    try {
        var data = {};
        data.line = line.index++;
        data.entityTaxNumber = line.entityTaxNumber || '';
        data.transactionId = line.documentNumber || line.transactionNumber || '';
        data.transactionDate = line.date || '';
        data.netAmount = line.netAmount || '';
        data.commodityCode = line.commodityCode || '';
        data.typeOfGoods = this.getCode(line.typeOfGoods || '');
        data.quantity = this.convertQuantity(line.itemId, line.quantity, line.unitOfMeasure);
        data.measureUnit = this.getUnitOfMeasure(line.unitOfMeasure || '');
        data.correctionCode = '';
        return data;
    } catch (ex) {
        logException(ex, 'SectionA2Adapter.getLineData');
        throw ex;
    }
};

/******************************
 * RECEIVED INVOICES (Common) *
 ******************************/
dao.SupplierInvoiceDAO = function SupplierInvoiceDAO() {
    Tax.DAO.ReportDAO.call(this);
    this.Name = 'VCS_SK_SupplierInvoiceDAO';
    this.reportName = 'VCS SK - VAT Purchases by Transaction';
};

dao.SupplierInvoiceDAO.prototype = Object.create(Tax.DAO.ReportDAO.prototype);

dao.SupplierInvoiceDAO.prototype.ListObject = function ListObject() {
    return {
        tranId: '',
        vatNo: '',
        transactionType: '',
        transactionNumber: '',
        documentNumber: '',
        transactionDate: '',
        taxCode: '',
        taxRate: '',
        notionalRate: '',
        netAmount: '',
        taxAmount: '',
        notionalAmount: ''
    };
};

dao.SupplierInvoiceDAO.prototype.getColumnMetadata = function getColumnMetadata(pivotReport) {
    var column = {};

    try {
        var pivotReportColumns = this.getColumns(pivotReport);
        column.tranId = pivotReportColumns[0];
        column.vatNo = pivotReportColumns[1];
        column.transactionType = pivotReportColumns[2];
        column.transactionNumber = pivotReportColumns[3];
        column.documentNumber = pivotReportColumns[4];
        column.transactionDate = pivotReportColumns[5];
        column.taxCode = pivotReportColumns[6];
        column.taxRate = pivotReportColumns[7];
        column.notionalRate = pivotReportColumns[8];
        column.netAmount = pivotReportColumns[9];
        column.taxAmount = pivotReportColumns[10],
        column.notionalAmount = pivotReportColumns[11];
    } catch (ex) {
        logException(ex, 'SupplierInvoiceDAO.getColumnMetadata');
        throw ex;
    }

    return column;
};

dao.SupplierJournalDAO = function SupplierJournalDAO() {
    dao.SupplierInvoiceDAO.call(this);
    this.Name = 'VCS_SK_SupplierJournalDAO';
    this.reportName = 'VCS SK - VAT Purchases by Transaction [JRN]';
};

dao.SupplierJournalDAO.prototype = Object.create(dao.SupplierInvoiceDAO.prototype);

/***************************
 * RECEIVED INVOICES - B.1 *
 ***************************/
adapter.SectionB1Adapter = function SectionB1Adapter(errorHandler) {
    Tax.Adapter.BaseAdapter.call(this);
    this.errorHandler = errorHandler;
    this.Name = 'SectionB1Adapter';
    this.daos = ['VCS_SK_SupplierInvoiceDAO', 'VCS_SK_SupplierJournalDAO'];
    this.taxCodes = ['RC'];
    this.taxCodeLookup = VAT.TaxCodeLookup.getInstance('SK', VAT.SK.TaxCodeDefinition());
};

adapter.SectionB1Adapter.prototype = Object.create(Tax.Adapter.BaseAdapter.prototype);

adapter.SectionB1Adapter.prototype.compile = function compile(data) {
    if (!data) {
        throw nlapiCreateError('INVALID_PARAMETER', 'SectionB1Adapter data is invalid');
    }

    var txnMap = {};
    var line = {};
    var taxAmount = 0;

    for (var i = 0; i < data.length; i++) {
        line = data[i];
        taxAmount = this.taxCodeLookup.typeOf(line.taxCode, 'RC') ? parseFloat(line.notionalAmount) : parseFloat(line.taxAmount);

        if (txnMap[line.tranId]) {
            txnMap[line.tranId].transactionTotal += parseFloat(line.netAmount) + taxAmount;
            txnMap[line.tranId].lines.push(line);
        } else {
            txnMap[line.tranId] = {};
            txnMap[line.tranId].transactionTotal = parseFloat(line.netAmount) + taxAmount;
            txnMap[line.tranId].lines = [];
            txnMap[line.tranId].lines.push(line);
        }
    }

    return txnMap;
};

adapter.SectionB1Adapter.prototype.filter = function filter(txnMap) {
    if (!txnMap) {
        throw nlapiCreateError('INVALID_PARAMETER', 'SectionB1Adapter txnMap is invalid');
    }

    var validTransactions = [];

    var transaction = {};
    for (var txnId in txnMap) {
        transaction = txnMap[txnId];

        if (transaction.transactionTotal >= 100) {
            validTransactions = validTransactions.concat(transaction.lines);
        }
    }

    return validTransactions;
};

adapter.SectionB1Adapter.prototype.transform = function transform(params) {
    var data = [];
    var keyToDataMap = {};
    var line = null;
    var lineKey = '';
    var taxRate = 0;

    try {
        for (var i = 0; i < this.rawdata.length; i++) {
            line = this.rawdata[i];
            taxRate = this.getTaxRate(line);
            lineKey = [line.tranId, taxRate].join('-');

            if (this.taxCodeLookup.anyOf(line.taxCode, this.taxCodes)) {
                if (keyToDataMap[lineKey] != undefined) {
                    data[keyToDataMap[lineKey]].netAmount += parseFloat(line.netAmount);
                    data[keyToDataMap[lineKey]].taxAmount += parseFloat(this.getTaxAmount(line));
                } else {
                    data.push({
                        tranId: line.tranId,
                        entityTaxNumber: line.vatNo,
                        transactionId: line.documentNumber || line.transactionNumber || '',
                        transactionDate: line.transactionDate,
                        netAmount: parseFloat(line.netAmount),
                        taxAmount: parseFloat(this.getTaxAmount(line)),
                        taxRate: taxRate,
                        correctionCode: ''
                    });

                    keyToDataMap[lineKey] = data.length - 1;
                }
            }
        }

        data.sort(function(a, b) {
            var diff = Date.parse(a.transactionDate) - Date.parse(b.transactionDate);
            if (diff != 0) {
                return diff;
            }
            var sortAKey = a.tranId;
            var sortBKey = b.tranId;
            if (a.transactionId && b.transactionId) {
                sortAKey = a.transactionId;
                sortBKey = b.transactionId;
            }
            return (sortAKey > sortBKey ) ? 1 : (sortAKey < sortBKey) ? -1 : 0;
        });
    } catch(e) {
        logException(e, 'SectionB1Adapter.transform');
        throw e;
    }

    var result = {};
    result[this.Name] = data;
    return result;
};

adapter.SectionB1Adapter.prototype.process = function process(result, params) {
    try {
        var invoiceData = Tax.Cache.MemoryCache.getInstance().load('VCS_SK_SupplierInvoiceDAO');
        if (invoiceData) {
            this.rawdata = this.rawdata.concat(invoiceData);
        }

        var journalData = Tax.Cache.MemoryCache.getInstance().load('VCS_SK_SupplierJournalDAO');
        if (journalData) {
            var journalTxnMap = this.compile(journalData);
            var filteredJournalData = this.filter(journalTxnMap);
            if (filteredJournalData) {
                this.rawdata = this.rawdata.concat(filteredJournalData);
            }
        }
    } catch (ex) {
        logException(ex, 'SectionB1Adapter.process');
        throw ex;
    }
    return {adapter: this.transform(params)};
};

adapter.SectionB1Adapter.prototype.getTaxAmount = function getTaxAmount(line) {
    return line.notionalAmount;
};

adapter.SectionB1Adapter.prototype.getTaxRate = function getTaxRate(line) {
    return line.notionalRate * 100;
};

/******************************************
 * RECEIVED INVOICES - B.2 *
 *****************************************/
adapter.SectionB2Adapter = function SectionB2Adapter(errorHandler) {
    adapter.SectionB1Adapter.call(this);
    this.Name = 'SectionB2Adapter';
    this.taxCodes = ['S', 'R'];
};

adapter.SectionB2Adapter.prototype = Object.create(adapter.SectionB1Adapter.prototype);

adapter.SectionB2Adapter.prototype.getTaxAmount = function getTaxAmount(line) {
    return line.taxAmount;
};

adapter.SectionB2Adapter.prototype.getTaxRate = function getTaxRate(line) {
    return line.taxRate;
};

/******************************************
 * RECEIVED INVOICES - B.3 *
 *****************************************/
dao.SupplierCashRegisterDAO = function SupplierCashRegisterDAO() {
    Tax.DAO.ReportDAO.call(this);
    this.Name = 'VCS_SK_SupplierCashRegisterDAO';
    this.reportName = 'VCS SK - VAT Cash Register Purchases';
};

dao.SupplierCashRegisterDAO.prototype = Object.create(Tax.DAO.ReportDAO.prototype);

dao.SupplierCashRegisterDAO.prototype.ListObject = function ListObject() {
    return {
        tranId: '',
        taxCode: '',
        netAmount: '',
        taxAmount: '',
        notionalAmount: '',
        cashRegister: ''
    };
};

dao.SupplierCashRegisterDAO.prototype.getColumnMetadata = function getColumnMetadata(pivotReport) {
    var column = {};

    try {
        var pivotReportColumns = this.getColumns(pivotReport);
        column.tranId = pivotReportColumns[0];
        column.taxCode = pivotReportColumns[1];
        column.netAmount = pivotReportColumns[2];
        column.taxAmount = pivotReportColumns[3],
        column.notionalAmount = pivotReportColumns[4];
        column.cashRegister = pivotReportColumns[5];
    } catch (ex) {
        logException(ex, 'SupplierCashRegisterDAO.getColumnMetadata');
        throw ex;
    }

    return column;
};

adapter.SectionB3Adapter = function SectionB3Adapter(errorHandler) {
    Tax.Adapter.BaseAdapter.call(this);
    this.Name = 'SectionB3Adapter';
    this.daos = ['VCS_SK_SupplierCashRegisterDAO'];
    this.taxCodes = ['S', 'R', 'RC'];
    this.taxCodeLookup = VAT.TaxCodeLookup.getInstance('SK', VAT.SK.TaxCodeDefinition());
};

adapter.SectionB3Adapter.prototype = Object.create(Tax.Adapter.BaseAdapter.prototype);

adapter.SectionB3Adapter.prototype.compile = function compile(data) {
    if (!data) {
        throw nlapiCreateError('INVALID_PARAMETER', 'SectionB3Adapter data is invalid');
    }

    var txnMap = {};
    var line = {};
    var lineNetAmount = 0;
    var lineTaxAmount = 0;

    try {
        for (var i = 0; i < data.length; i++) {
            line = data[i];

            lineNetAmount = parseFloat(line.netAmount);
            lineTaxAmount = (this.taxCodeLookup.typeOf(line.taxCode, 'RC') ? parseFloat(line.notionalAmount) : parseFloat(line.taxAmount)) || 0;

            if (txnMap[line.tranId]) {
                txnMap[line.tranId].transactionTotal += lineNetAmount + lineTaxAmount;
            } else {
                txnMap[line.tranId] = {};
                txnMap[line.tranId].transactionTotal = lineNetAmount + lineTaxAmount;
                txnMap[line.tranId].cashRegister = line.cashRegister;
                txnMap[line.tranId].lines = [];
            }
            txnMap[line.tranId].lines.push({
                taxCode: line.taxCode,
                netAmount: lineNetAmount,
                taxAmount: lineTaxAmount
            });
        }
    } catch (e) {
        logException(e, 'SectionB3Adapter.compile');
        throw e;
    }

    return txnMap;
};

adapter.SectionB3Adapter.prototype.summarize = function summarize(txnMap) {
    if (!txnMap) {
        throw nlapiCreateError('INVALID_PARAMETER', 'SectionB3Adapter txnMap is invalid');
    }

    var transaction = {};
    var line = {};
    var summary = {
    	netAmount: 0,
    	taxAmount: 0,
    	correctionCode: ''
    };

    try {
        for (var txnId in txnMap) {
            transaction = txnMap[txnId];

            if (transaction.transactionTotal < 100 || transaction.cashRegister === 'T') {
                for (var i = 0; i < transaction.lines.length; i++) {
                    line = transaction.lines[i];

                    if (this.taxCodeLookup.anyOf(line.taxCode, this.taxCodes)) {
                    	summary.netAmount += line.netAmount;
                        summary.taxAmount += line.taxAmount;
                    }
                }
            }
        }
    } catch (e) {
        logException(e, 'SectionB3Adapter.summarize');
        throw e;
    }

    return summary;
};

adapter.SectionB3Adapter.prototype.transform = function transform(params) {
    var result = {};
    var txnMap = this.compile(this.rawdata);
    var summary = this.summarize(txnMap);

    result[this.Name] = (summary.netAmount > 0 || summary.taxAmount) > 0 ? [summary] : [];
    return result;
};

/*****************************
 * CORRECTIVE INVOICES - C.1 *
 *****************************/
dao.CustomerCreditsDAO = function CustomerCreditsDAO() {
    Tax.DAO.ReportDAO.call(this);
    this.Name = 'VCS_SK_CustomerCreditsDAO';
    this.reportName = 'VCS SK - Customer Credits by Txn, Item, Tax Code';
};

dao.CustomerCreditsDAO.prototype = Object.create(Tax.DAO.ReportDAO.prototype);

dao.CustomerCreditsDAO.prototype.ListObject = function ListObject(id) {
    return {
        id: id,
        vatNo: '',
        type: '',
        transactionNumber: '',
        documentNumber: '',
        date: '',
        item: '',
        commodityCode: '',
        typeOfGood: '',
        quantity: '',
        unit: '',
        taxCode: '',
        taxRate: '',
        notionalRate: '',
        netAmount: '',
        taxAmount: '',
        notionalAmount: ''
    };
};

dao.CustomerCreditsDAO.prototype.getColumnMetadata = function getColumnMetadata(pivotReport) {
    var column = {};
    try {
        var pivotReportColumns = this.getColumns(pivotReport);
        column.id = pivotReportColumns[0];
        column.vatNo = pivotReportColumns[1];
        column.type = pivotReportColumns[2];
        column.transactionNumber = pivotReportColumns[3];
        column.documentNumber = pivotReportColumns[4];
        column.date = pivotReportColumns[5];
        column.itemId = pivotReportColumns[6];
        column.commodityCode = pivotReportColumns[7];
        column.typeOfGood = pivotReportColumns[8];
        column.quantity = pivotReportColumns[9];
        column.unit = pivotReportColumns[10];
        column.taxCode = pivotReportColumns[11];
        column.taxRate = pivotReportColumns[12];
        column.notionalRate = pivotReportColumns[13];
        column.netAmount = pivotReportColumns[14];
        column.taxAmount = pivotReportColumns[15];
        column.notionalAmount = pivotReportColumns[16];
    } catch (ex) {
        logException(ex, 'CustomerCreditsDAO.getColumnMetadata');
        throw ex;
    }
    return column;
};

dao.CustomerCreditApplicationsDAO = function CustomerCreditApplicationsDAO() {
    Tax.DAO.SearchDAO.call(this);
    this.Name = 'VCS_SK_CustomerCreditApplicationsDAO';
    this.searchId = 'customsearch_vcs_sk_credit_applications';
    this.searchType = 'transaction';
    this.filters = [];
};

dao.CustomerCreditApplicationsDAO.prototype = Object.create(Tax.DAO.SearchDAO.prototype);

dao.CustomerCreditApplicationsDAO.prototype.prepareSearch = function prepareSearch(params) {
    if (!this.searchId) {
        throw nlapiCreateError('MISSING_SEARCH_ID', 'Please provide the ID of the saved search.');
    }
    
    if (!this.searchType) {
        throw nlapiCreateError('MISSING_SEARCH_TYPE', 'Please provide the saved search type.');
    }

    if (params && params.internalId) {
        this.filters.push(new nlobjSearchFilter('internalid', null, 'anyof', params.internalId));
    }
};

dao.CustomerCreditApplicationsDAO.prototype.ListObject = function ListObject() {
    return {
        id: '',
        appliedToTransaction: '',
        appliedAmount: ''
    };
};

dao.CustomerCreditApplicationsDAO.prototype.rowToObject = function rowToObject(row) {
    var obj = new this.ListObject();
    obj.id = row.getValue('internalid');
    obj.appliedToTransaction = row.getValue('appliedtotransaction');
    obj.appliedAmount = row.getValue('appliedtolinkamount');
    
    return obj;
};

//TODO
dao.CustomerCreditApplicationsDAO.prototype.process = function(result, params) {
    try{
        var combinedResult = {};
        combinedResult.savedReport = result.dao;

        var internalId = [];

        for (var i = 0; result.dao && i < result.dao.length; i++) {
            var line = result.dao[i];

            if (internalId.indexOf(line.id) == -1) {
            	internalId.push(line.id);
            }
        }

        combinedResult.creditApplications = internalId.length > 0 ? this.getList({ internalId: internalId }) : [];

        var newResult = {
            dao: combinedResult
        };

        return newResult;
    } catch (ex) {
        logException(ex, 'CustomerCreditApplicationsDAO.process');
        throw ex;
    }
};

dao.CorrectedTransactionDetailsDAO = function CorrectedTransactionDetailsDAO() {
    dao.CustomerCreditApplicationsDAO.call(this);
    this.Name = 'VCS_SK_CorrectedTransactionDetailsDAO';
    this.searchId = 'customsearch_vcs_sk_corrected_txn_detail';
    this.filters = [];
    this.columns = [];
};

dao.CorrectedTransactionDetailsDAO.prototype = Object.create(dao.CustomerCreditApplicationsDAO.prototype);

dao.CorrectedTransactionDetailsDAO.prototype.prepareSearch = function prepareSearch(params) {
    if (!this.searchId) {
        throw nlapiCreateError('MISSING_SEARCH_ID', 'Please provide the ID of the saved search.');
    }
    
    if (!this.searchType) {
        throw nlapiCreateError('MISSING_SEARCH_TYPE', 'Please provide the saved search type.');
    }
    
    if (params && params.internalId) {
        this.filters.push(new nlobjSearchFilter('internalid', null, 'anyof', params.internalId));
    }
    
    if (base.isUnitsOfMeasureEnabled) {
        this.columns.push(new nlobjSearchColumn('unit', null, 'group'));
    }
};

dao.CorrectedTransactionDetailsDAO.prototype.ListObject = function ListObject() {
    return {
        id: '',
        item: '',
        quantity: '',
        unit: '',
        taxCode: '',
        totalAmount: '',
        netAmount: '',
        taxAmount: '',
        transactionNumber: '',
        documentNumber: ''
    };
};

dao.CorrectedTransactionDetailsDAO.prototype.rowToObject = function rowToObject(row) {
    var obj = new this.ListObject();
    obj.id = row.getValue('internalid', null, 'group');
    obj.item = row.getValue('item', null, 'group');
    obj.quantity = row.getValue('quantity', null, 'sum');
    obj.taxCode = row.getValue('taxcode', null, 'group');
    obj.totalAmount = row.getValue('total', null, 'max');
    obj.netAmount = row.getValue('netamount', null, 'sum');
    obj.taxAmount = row.getValue('taxamount', null, 'sum');
    obj.transactionNumber = row.getValue('transactionnumber', null, 'group');
    obj.documentNumber = row.getValue('tranid', null, 'group');
    
    if (base.isUnitsOfMeasureEnabled) {
        obj.unit = row.getValue('unit', null, 'group');
    }
    
    return obj;
};

// TODO
dao.CorrectedTransactionDetailsDAO.prototype.process = function(result, params) {
    var combinedResult = result.dao;

    var internalId = [];

    for (var i = 0; result.dao.creditApplications && i < result.dao.creditApplications.length; i++) {
        var creditApplication = result.dao.creditApplications[i];
        if (internalId.indexOf(creditApplication.appliedToTransaction) == -1) {
        	internalId.push(creditApplication.appliedToTransaction);
        }
    }
    combinedResult.correctedTransactionDetails = internalId.length > 0 ? this.getList({ internalId: internalId }) : [];

    var newResult = {
        dao: combinedResult
    };

    return newResult;
};

dao.SectionC1ItemDAO = function ItemDAO() {
    dao.ItemDAO.call(this);
    this.Name = 'SectionC1ItemDAO';
    this.recordType = 'item';
};

dao.SectionC1ItemDAO.prototype = Object.create(dao.ItemDAO.prototype);

dao.SectionC1ItemDAO.prototype.process = function(result, params) {
    try{
        var combinedResult = {};
        combinedResult.savedReport = result.dao;
        
        if (base.isUnitsOfMeasureEnabled) {
            var txnIdList = this.constructIdList(result);
            combinedResult.items = txnIdList.length > 0 ? this.getList({ itemIdList: txnIdList }) : [];
        }

        var newResult = {
            dao: combinedResult
        };

        return newResult;
    } catch (ex) {
        logException(ex, 'SectionC1ItemDAO.process');
        throw ex;
    }
};

dao.SectionC1ItemDAO.prototype.constructIdList = function(result) {
    if(!result || !result.dao || !result.dao.savedReport) {
        throw nlapiCreateError('MISSING_ARGUMENT', 'result.dao is required');
    }
    
    try {
        var creditMemoList = result.dao.savedReport;
        var invoiceList = result.dao.correctedTransactionDetails;
        
        var itemList = this.getItemsFromList(creditMemoList, 'itemId');
        itemList = itemList.concat(this.getItemsFromList(invoiceList, 'item'));
        itemList = this.getUniqueList(itemList);
        
        return itemList;
    } catch (ex) {
        logException(ex, 'SectionC1ItemDAO.constructIdList');
        throw ex;        
    } 
};

dao.SectionC1ItemDAO.prototype.getItemsFromList = function(txnList, itemProperty) {
    if(!txnList || !itemProperty) {
        throw nlapiCreateError('MISSING_ARGUMENT', 'Transaction list is required');
    }

    try {
        var items = [];
        for (var i = 0; i < txnList.length; i++) {
            var item = txnList[i][itemProperty];
            if (item) {
                items.push(item);
            }
        }
        return items;
    } catch (ex) {
        logException(ex, 'SectionC1ItemDAO.getItemsFromList');
        throw ex;        
    } 
};

dao.SectionC1ItemDAO.prototype.getUniqueList = function(idList) {
    if(!idList) {
        throw nlapiCreateError('MISSING_ARGUMENT', 'List is required');
    }

    try {
        var uniqueList = 
            idList.slice().sort(function (a,b) { return a > b; }).reduce(function(a,b) {
            if (a.slice(-1)[0] !== b) {
                a.push(b);
            }
            return a;
        }, []);
        return uniqueList;
    } catch (ex) {
        logException(ex, 'SectionC1ItemDAO.getUniqueList');
        throw ex;        
    } 
};

adapter.SectionC1Adapter = function SectionC1Adapter(errorHandler) {
    adapter.ItemSectionAdapter.call(this);
    this.errorHandler = errorHandler;
    this.Name = 'SectionC1Adapter';
    this.daos = ['VCS_SK_CustomerCreditsDAO'];
    this.taxCodes = ['S', 'R', 'RC'];
    this.taxCodeLookup = VAT.TaxCodeLookup.getInstance('SK', VAT.SK.TaxCodeDefinition());
};

adapter.SectionC1Adapter.prototype = Object.create(adapter.ItemSectionAdapter.prototype);

adapter.SectionC1Adapter.prototype.process = function process(result, params) {
    if(!result || !result.dao || !result.dao.savedReport) {
        throw nlapiCreateError('MISSING_ARGUMENT', 'result.dao is required');
    }
    var dao = result.dao;
    
    this.rawdata = dao.savedReport.savedReport;
    this.itemUnitsTypeMap = base.isUnitsOfMeasureEnabled ? this.createItemUnitsTypeMap(dao.items) : {};
    this.unitsTypeUnitMap = base.isUnitsOfMeasureEnabled ? this.createUnitsTypeUnitMap(dao.unitsTypes): {};
    this.creditApplicationsMap = this.createCreditApplicationsMap(dao.savedReport.creditApplications, dao.savedReport.correctedTransactionDetails);

    return {
        dao: this.rawdata,
        adapter: this.transform(params)
    };
};

adapter.SectionC1Adapter.prototype.createCreditApplicationsMap = function createCreditApplicationsMap(appliedList, transactionDetailsList) {
    if(!appliedList) {
        throw nlapiCreateError('MISSING_ARGUMENT', 'appliedList is required');
    }
    if(!transactionDetailsList) {
        throw nlapiCreateError('MISSING_ARGUMENT', 'transactionDetailsList is required');
    }

    try {
        var creditMap = {};
        var applied = {};
        var transactionDetails = {};
        var taxCodeName = '';
        var itemId = '';
        var key = '';

        for (var i = 0; i < appliedList.length; i++) {
            applied = appliedList[i];
            
            if (!creditMap[applied.id]) {
                creditMap[applied.id] = {};
                for (var j = 0; j < transactionDetailsList.length; j++) {
                    transactionDetails = transactionDetailsList[j];
                    if (transactionDetails.id == applied.appliedToTransaction) {
                        creditMap[applied.id].transactionNumber = transactionDetails.transactionNumber;
                        creditMap[applied.id].documentNumber = transactionDetails.documentNumber == '- None -' ? '' : transactionDetails.documentNumber;
                        taxCodeName = this.getTaxCodeNameById(transactionDetails.taxCode);
                        itemId = transactionDetails.item;
                        
                        creditMap[applied.id][[itemId, taxCodeName].join('-')] = {
                            netAmount: parseFloat(transactionDetails.netAmount) || 0,
                            taxAmount: Math.abs(transactionDetails.taxAmount),
                            transactionNumber: transactionDetails.transactionNumber,
                            documentNumber: transactionDetails.documentNumber == '- None -' ? '' : transactionDetails.documentNumber,
                            quantity: transactionDetails.quantity || 0,
                            itemId: itemId,
                            unit: transactionDetails.unit || ''
                        };
                    }
                }
            }
        }

        return creditMap;
    } catch (ex) {
        logException(ex, 'SectionC1Adapter.createCreditApplicationsMap');
        throw ex;
    }    
};

adapter.SectionC1Adapter.prototype.getTaxCodeNameById = function getTaxCodeNameById(taxCodeId) {
    var taxCodeMap = this.taxCodeLookup.taxCodesMap;
    
    for (var name in taxCodeMap) {
        if (taxCodeMap[name] == taxCodeId) {
            return name;
        }
    }
};

adapter.SectionC1Adapter.prototype.getLineData = function getLineData(line) {
    if (!line) {
        throw nlapiCreateError('INVALID_PARAMETER', 'Invalid SectionC1Adapter line data');
    }

    try {
        var invoice = this.getInvoiceLine(line);
        if (!invoice) {
            return null;
        }

        var unit = line.unit || invoice.unit || '';
        line.computedTaxAmount = line.taxAmount || line.notionalAmount || 0;
        line.computedQuantity = this.convertQuantity(line.itemId, line.quantity, unit) || 0;
        
        invoice.computedTaxAmount = line.taxRate ? invoice.taxAmount : (invoice.netAmount * line.notionalRate);
        invoice.computedQuantity = this.convertQuantity(invoice.itemId, invoice.quantity, unit) || 0;
        
        var data = {};
        data.line = line.index++;
        data.entityTaxNumber = line.vatNo || '';
        data.transactionId = line.documentNumber || line.transactionNumber || '';
        data.invoiceTransactionId = invoice.documentNumber || invoice.transactionNumber || '';
        data.netAmount = Math.abs(invoice.netAmount) - Math.abs(line.netAmount);
        data.taxAmount = Math.abs(invoice.computedTaxAmount) - Math.abs(line.computedTaxAmount);
        data.taxRate = line.taxRate || (line.notionalRate * 100) || '';
        data.commodityCode = line.commodityCode || '';
        data.typeOfGoods = this.getCode(line.typeOfGood || '');
        data.quantity = invoice.computedQuantity - line.computedQuantity;
        data.measureUnit = this.getUnitOfMeasure(unit);
        data.correctionCode = '';

        return data;
    } catch (ex) {
        logException(ex, 'SectionC1Adapter.getLineData');
        throw ex;
    }
};

adapter.SectionC1Adapter.prototype.getInvoiceLine = function getInvoiceLine(creditLine) {
    if (!creditLine || !creditLine.taxCode) {
        throw nlapiCreateError('INVALID_PARAMETER', 'Invalid SectionC1Adapter credit memo');
    }
    if (!this.creditApplicationsMap) {
        throw nlapiCreateError('INVALID_PARAMETER', 'Credit Map is invalid');
    }
    
    var key = [creditLine.itemId, creditLine.taxCode].join('-');
    
    try {
        var invoice = this.creditApplicationsMap[creditLine.id];
        return (invoice && invoice[key]) || null;
    } catch (ex) {
        logException(ex, 'SectionC1Adapter.getInvoiceLine');
        throw ex;
    }
};

/*****************************
 * CORRECTIVE INVOICES - C.2 *
 *****************************/
dao.VendorCreditDAO = function VendorCreditDAO() {
    Tax.DAO.ReportDAO.call(this);
    this.Name = 'VCS_SK_VendorCreditsDAO';
    this.reportName = 'VCS SK - Vendor Credits by Txn, Tax Code';
};

dao.VendorCreditDAO.prototype = Object.create(Tax.DAO.ReportDAO.prototype);

dao.VendorCreditDAO.prototype.ListObject = function ListObject(id) {
    return {
        id: id,
        vatNo: '',
        transactionNumber: '',
        documentNumber: '',
        date: '',
        taxCode: '',
        taxRate: '',
        notionalRate: '',
        netAmount: '',
        taxAmount: '',
        notionalAmount: ''
    };
};

dao.VendorCreditDAO.prototype.getColumnMetadata = function getColumnMetadata(pivotReport) {
    var column = {};
    try {
        var pivotReportColumns = this.getColumns(pivotReport);
        column.id = pivotReportColumns[0];
        column.vatNo = pivotReportColumns[1];
        column.transactionNumber = pivotReportColumns[2];
        column.documentNumber = pivotReportColumns[3];
        column.date = pivotReportColumns[4];
        column.taxCode = pivotReportColumns[5];
        column.taxRate = pivotReportColumns[6];
        column.notionalRate = pivotReportColumns[7];
        column.netAmount = pivotReportColumns[8];
        column.taxAmount = pivotReportColumns[9];
        column.notionalAmount = pivotReportColumns[10];
    } catch (ex) {
        logException(ex, 'VendorCreditDAO.getColumnMetadata');
        throw ex;
    }
    return column;
};

adapter.SectionC2Adapter = function SectionC2Adapter(errorHandler) {
    adapter.ItemSectionAdapter.call(this);
    this.errorHandler = errorHandler;
    this.Name = 'SectionC2Adapter';
    this.daos = [];
    this.taxCodes = ['S', 'R', 'RC'];
    this.taxCodeLookup = VAT.TaxCodeLookup.getInstance('SK', VAT.SK.TaxCodeDefinition());
};

adapter.SectionC2Adapter.prototype = Object.create(Tax.Adapter.BaseAdapter.prototype);

adapter.SectionC2Adapter.prototype.process = function process(result, params) {
    if(!result || !result.dao) {
        throw nlapiCreateError('MISSING_ARGUMENT', 'result.dao is required');
    }
    var dao = result.dao;

    this.rawdata = dao.savedReport;
    this.creditApplicationsMap = this.createCreditApplicationsMap(dao.creditApplications, dao.correctedTransactionDetails);

    return {
        dao: this.rawdata,
        adapter: this.transform(params)
    };
};

adapter.SectionC2Adapter.prototype.getTaxCodeNameById = function getTaxCodeNameById(taxCodeId) {
	var taxCodeMap = this.taxCodeLookup.taxCodesMap;

	for (var name in taxCodeMap) {
		if (taxCodeMap[name] == taxCodeId) {
			return name;
		}
	}
};

adapter.SectionC2Adapter.prototype.createCreditApplicationsMap = function createCreditApplicationsMap(appliedList, transactionDetailsList) {
    if(!appliedList) {
        throw nlapiCreateError('MISSING_ARGUMENT', 'SectionC2Adapter - appliedList is required');
    }
    if(!transactionDetailsList) {
        throw nlapiCreateError('MISSING_ARGUMENT', 'SectionC2Adapter - transactionDetailsList is required');
    }
    // Assumption in creating the map is there is only 1:1 association between bill credit and bill payment
    try {
        var creditMap = {};
        var applied = {};
        var transactionDetails = {};
        var taxCodeName = '';

        for (var i = 0; i < appliedList.length; i++) {
        	applied = appliedList[i];
    		creditMap[applied.id] = {};
        	for (var j = 0; j < transactionDetailsList.length; j++) {
        		transactionDetails = transactionDetailsList[j];
        		if (transactionDetails.id == applied.appliedToTransaction) {
    				taxCodeName = this.getTaxCodeNameById(transactionDetails.taxCode);
    				if (!creditMap[applied.id].transactionNumber && !creditMap[applied.id].documentNumber) {
        				creditMap[applied.id].transactionNumber = transactionDetails.transactionNumber;
        				creditMap[applied.id].documentNumber = transactionDetails.documentNumber == '- None -' ? '' : transactionDetails.documentNumber;
    				}
    				if (!creditMap[applied.id][taxCodeName]) {
        				creditMap[applied.id][taxCodeName] = {
        					netAmount: parseFloat(transactionDetails.netAmount) || 0,
        					taxAmount: Math.abs(transactionDetails.taxAmount)
        				};
    				} else {
    					creditMap[applied.id][taxCodeName].netAmount += parseFloat(transactionDetails.netAmount) || 0;
    					creditMap[applied.id][taxCodeName].taxAmount += Math.abs(transactionDetails.taxAmount);
    				}
        		}
        	}
        }

        return creditMap;
    } catch (ex) {
        logException(ex, 'SectionC2Adapter.createCreditApplicationsMap');
        throw ex;
    }    
};

adapter.SectionC2Adapter.prototype.getLineData = function getLineData(line) {
	if (!line) {
		throw nlapiCreateError('INVALID_PARAMETER', 'Invalid SectionC2Adapter line data');
	}

	try {
		var bill = this.getBillLine(line);
		if (!bill) {
		    return null;
		}
		var lineNetAmount = parseFloat(line.netAmount) || 0;
		var lineTaxAmount = Math.abs(line.taxAmount || line.notionalAmount) || 0;
		var data = {};

		data.entityTaxNumber = line.vatNo || '';
		data.transactionId = line.documentNumber || line.transactionNumber || '';
		data.billTransactionId =  bill.documentNumber || bill.transactionNumber || '';

		if (bill.netAmount != undefined && bill.taxAmount != undefined) {
			data.netAmount = bill.netAmount - lineNetAmount;
			data.taxAmount = (line.taxRate ? bill.taxAmount : (bill.netAmount * line.notionalRate).toFixed(4)) - lineTaxAmount;
			data.taxRate = line.taxRate || line.notionalRate * 100 || '';
		} else {
			data.netAmount = '';
			data.taxAmount = '';
			data.taxRate = '';
		}

		data.taxCodeType = this.taxCodeLookup.getTaxCodeType(line.taxCode);
		data.correctionCode = '';

		return data;
	} catch (ex) {
		logException(ex, 'SectionC2Adapter.getLineData');
		throw ex;
	}
};

adapter.SectionC2Adapter.prototype.transform = function transform(params) {
    try {
        var data = [];
        var line = null;
        var lineKey = '';
        var keyToDataMap = {};
        var transformed = {};

        for (var i = 0; i < this.rawdata.length; i++) {
            line = this.rawdata[i];

            if (this.taxCodeLookup.anyOf(line.taxCode, this.taxCodes)) {
                transformed = this.getLineData(line);
                if (transformed == null) {
                	continue;
                }

                lineKey = [transformed.entityTaxNumber, transformed.transactionId, transformed.billTransactionId, transformed.taxCodeType].join('-');
                if (keyToDataMap[lineKey] != undefined) {
                    data[keyToDataMap[lineKey]].netAmount += parseFloat(transformed.netAmount);
                    data[keyToDataMap[lineKey]].taxAmount += parseFloat(transformed.taxAmount);
                } else {
                    data.push(transformed);

                    keyToDataMap[lineKey] = data.length - 1;
                }
            }
        }

        var result = {};
        result[this.Name] = data;
        return result;
    } catch (ex) {
        logException(ex, 'SectionC2Adapter.transform');
        throw ex;
    }
};

adapter.SectionC2Adapter.prototype.getBillLine = function getBillLine(creditLine) {
    if (!creditLine || !creditLine.taxCode) {
        throw nlapiCreateError('INVALID_PARAMETER', 'Invalid SectionC2Adapter credit memo');
    }
    if (!this.creditApplicationsMap) {
        throw nlapiCreateError('INVALID_PARAMETER', 'Credit Map is invalid');
    }

    try {
        var bill = this.creditApplicationsMap[creditLine.id];
        var billData = {};

        if (bill) {
        	billData.transactionNumber = bill.transactionNumber;
        	billData.documentNumber = bill.documentNumber;

            if (bill && bill[creditLine.taxCode]) {
            	billData.netAmount = bill[creditLine.taxCode].netAmount; 
            	billData.taxAmount = bill[creditLine.taxCode].taxAmount; 
            }
            return billData;

        }
        return null;
    } catch (ex) {
        logException(ex, 'SectionC2Adapter.getBillLine');
        throw ex;
    }
};

/******************************************
 * OTHER VAT DOCUMENT *
 *****************************************/
dao.CustomerJournalSummaryDAO = function CustomerJournalSummaryDAO() {
    Tax.DAO.ReportDAO.call(this);
    this.Name = 'VCS_SK_CustomerJournalDAO';
    this.reportName = 'VCS SK - Sales Summary by Tax Code [JRN]';
};

dao.CustomerJournalSummaryDAO.prototype = Object.create(Tax.DAO.ReportDAO.prototype);

dao.CustomerJournalSummaryDAO.prototype.ListObject = function ListObject() {
    return {
        taxCode: '',
        netAmount: '',
        taxAmount: '',
        grossAmount: '',
        cashRegister: ''
    };
};

dao.CustomerJournalSummaryDAO.prototype.getColumnMetadata = function getColumnMetadata(pivotReport) {
    var column = {};
    try {
        var pivotReportColumns = this.getColumns(pivotReport);
        column.taxCode = pivotReportColumns[0];
        column.netAmount = pivotReportColumns[1];
        column.taxAmount = pivotReportColumns[2];
        column.grossAmount = pivotReportColumns[3];
        column.cashRegister = pivotReportColumns[4];
    } catch (ex) {
        logException(ex, 'CustomerJournalSummaryDAO.getColumnMetadata');
        throw ex;
    }
    return column;
};

adapter.SectionD1Adapter = function SectionD1Adapter(errorHandler) {
    Tax.Adapter.BaseAdapter.call(this);
    this.Name = 'SectionD1Adapter';
    this.daos = ['VCS_SK_CustomerJournalDAO'];
    this.taxCodes = ['S', 'R'];
    this.salesTaxCodes1 = ['S'];
    this.salesTaxCodes2 = ['R'];
    this.taxCodeLookup = VAT.TaxCodeLookup.getInstance('SK', VAT.SK.TaxCodeDefinition());
};

adapter.SectionD1Adapter.prototype = Object.create(Tax.Adapter.BaseAdapter.prototype);

adapter.SectionD1Adapter.prototype.compile = function compile(data) {
    if (!data) {
        throw nlapiCreateError('INVALID_PARAMETER', 'SectionD1Adapter data is invalid');
    }

    try {
        var result = {
            totalAmount: 0,
            'netAmountS-SK': 0,
            'taxAmountS-SK': 0,
            'netAmountR-SK': 0,
            'taxAmountR-SK': 0,
        };

        for (var i = 0; i < data.length; i++) {
            line = data[i];
            if (!this.taxCodeLookup.anyOf(line.taxCode, this.taxCodes) || line.cashRegister !== 'T') {
                continue;
            }
            var netAmount = parseFloat(line.netAmount) || 0;
            var taxAmount = parseFloat(line.taxAmount) || 0;
            result.totalAmount += parseFloat(line.grossAmount) || 0;

            if (this.taxCodeLookup.anyOf(line.taxCode, this.salesTaxCodes1)) {
                result['netAmountS-SK'] += netAmount;
                result['taxAmountS-SK'] += taxAmount;
            } else if (this.taxCodeLookup.anyOf(line.taxCode, this.salesTaxCodes2)) {
                result['netAmountR-SK'] += netAmount;
                result['taxAmountR-SK'] += taxAmount;
            }
        }
        return result;
    } catch (e) {
        logException(e, 'SectionD1Adapter.compile');
        throw e;
    }
};

adapter.SectionD1Adapter.prototype.transform = function transform(params) {
    var result = {};
    var compiledData = this.compile(this.rawdata);
    var hasData = false;
    
    for (var data in compiledData) {
    	if (compiledData[data]) {
    		hasData = true;
    		break;
    	}
    }

    result[this.Name] = hasData ? [compiledData] : [];
    return result;
};

dao.SalesAdjustmentDAO = function SalesAdjustmentDAO() {
    Tax.DAO.SaleAdjustmentDetailsDAO.call(this);
    this.Name = 'VCS_SK_SalesAdjustmentDAO';   
};

dao.SalesAdjustmentDAO.prototype = Object.create(Tax.DAO.SaleAdjustmentDetailsDAO.prototype);

dao.SalesAdjustmentDAO.prototype.ListObject = function(id) {
	return {
		id: id,
		taxCode: '',
		netAmount: 0,
		taxAmount: 0,
		grossAmount: 0,
	};
};

dao.SalesAdjustmentDAO.prototype.rowToObject = function rowToObject(row) {
	var id = row.getValue('internalid', null, 'group');
	var line = new this.ListObject(id);
	line.taxCode = row.getText("custcol_adjustment_tax_code", null, "group");
	line.taxAmount = row.getValue(this.amountColumn, this.amountJoinColumn, "sum");
	line.grossAmount = line.taxAmount;
	line.cashRegister = 'F';
	return line;
};

adapter.SectionD2Adapter = function SectionD2Adapter(errorHandler) {
    Tax.Adapter.BaseAdapter.call(this);
    this.Name = 'SectionD2Adapter';
    this.daos = ['VCS_SK_CustomerJournalDAO', 'VCS_SK_SalesAdjustmentDAO'];
    this.taxCodes = ['S', 'R'];
    this.salesTaxCodes1 = ['S'];
    this.salesTaxCodes2 = ['R'];
    this.taxCodeLookup = VAT.TaxCodeLookup.getInstance('SK', VAT.SK.TaxCodeDefinition());
};

adapter.SectionD2Adapter.prototype = Object.create(Tax.Adapter.BaseAdapter.prototype);

adapter.SectionD2Adapter.prototype.compile = function compile(data) {
    if (!data) {
        throw nlapiCreateError('INVALID_PARAMETER', 'SectionD2Adapter data is invalid');
    }

    try {
        var result = {};
        result['netAmountS-SK'] = 0;
        result['taxAmountS-SK'] = 0;
        result['netAmountR-SK'] = 0;
        result['taxAmountR-SK'] = 0;
        for (var i = 0; i < data.length; i++) {
            line = data[i];
            if (!this.taxCodeLookup.anyOf(line.taxCode, this.taxCodes) || line.cashRegister === 'T') {
                continue;
            }
            var netAmount = parseFloat(line.netAmount) || 0;
            var taxAmount = parseFloat(line.taxAmount) || 0;
            if (this.taxCodeLookup.anyOf(line.taxCode, this.salesTaxCodes1)) {
                result['netAmountS-SK'] += netAmount;
                result['taxAmountS-SK'] += taxAmount;
            } else if (this.taxCodeLookup.anyOf(line.taxCode, this.salesTaxCodes2)) {
                result['netAmountR-SK'] += netAmount;
                result['taxAmountR-SK'] += taxAmount;
            }
        }
        result.correctionCode = '';

        return result;
    } catch (e) {
        logException(e, 'SectionD2Adapter.compile');
        throw e;
    }
};

adapter.SectionD2Adapter.prototype.transform = function transform(params) {
    var result = {};
    var compiledData = this.compile(this.rawdata);
    var hasData = false;
    
    for (var data in compiledData) {
    	if (compiledData[data]) {
    		hasData = true;
    		break;
    	}
    }

    result[this.Name] = hasData ? [compiledData] : [];
    return result;
};

