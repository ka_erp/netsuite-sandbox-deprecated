/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var Tax = Tax || {};

Tax.TemplateRenderer = function TemplateRenderer() {
	Tax.Processor.call(this);
	this.Name = 'TemplateRenderer';
};
Tax.TemplateRenderer.prototype = Object.create(Tax.Processor.prototype);

Tax.TemplateRenderer.prototype.renderTemplate = function renderTemplate(templateName, data) {
	var template = getTaxTemplate(templateName).short;
	var renderedData = VAT.RenderHandlebarsTemplate(template, data);
	return renderedData;
};

Tax.TemplateRenderer.prototype.process = function process(result, params) {
	var templateName = params.meta.templates[params.format];
	var renderedData = this.renderTemplate(templateName, result);
	result.rendered = renderedData;
	return result;
};