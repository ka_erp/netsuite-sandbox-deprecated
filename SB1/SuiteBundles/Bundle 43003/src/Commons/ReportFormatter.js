/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var Tax = Tax || {};

Tax.ReportFormatter = function ReportFormatter(params) {
	var languageCode = null;
	this.formatter = new VAT.Report.FormatterSingleton.getInstance(params.subsidiary, params.countryCode, languageCode);
};

Tax.ReportFormatter.prototype.format = function format(value, type, format) {

	var formattedValue = value;
	var displayCurrency = false;

	switch(type) {
		case 'date':
			var dateFormat = format ? format : this.formatter.shortdate;
			formattedValue = this.formatter.formatDate(value, dateFormat, false);
			break;
		case 'numeric':
			formattedValue = this.formatter.formatCurrency(value, displayCurrency, format); //should displaying of currency be supported?
			break;
		case 'regexp':
			formattedValue = this.formatter.formatRegExpString(value, format);
			break;
		default: // ?
			nlapiLogExecution('DEBUG', 'Tax.ReportFormatter.prototype.format', 'No formatting defined for type');
			break;
	}

	return formattedValue;
};