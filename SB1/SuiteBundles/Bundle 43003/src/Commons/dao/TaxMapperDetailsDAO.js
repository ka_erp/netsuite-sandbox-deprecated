/**
 * Copyright 2015 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

var Tax = Tax || {};
Tax = Tax || {};
Tax.DAO = Tax.DAO || {};

//Parent Record
Tax.DAO.TaxReportMapperDAO = function _TaxReportMapperDAO() {
	Tax.DAO.RecordDAO.call(this);
	this.Name = 'TaxReportMapperDAO';
	this.recordType = 'customrecord_tax_report_map';
	this.fields = {
		name: 'name',
		countryName: 'custrecord_country_name',
		transactionName: 'custrecord_transaction_name',
		mapType: 'custrecord_map_type',
		isEUCountry: 'custrecord_is_eu_country',
		altCode: 'custrecord_alt_code',
		internalid: 'custrecord_internal_id',
		isTaxFilingConfig: 'custrecord_is_tax_filing_config',
		isNonDeductible: 'custrecord_is_nondeductible',
		executionContext: 'custrecord_execution_context',
		enableFeature: 'custrecord_enable_feature',
		isFilterByLocation: 'custrecord_is_filter_by_location',
		TaxClass: 'custrecord_Tax_class',
		location: 'custrecord_location',
		formatSupplementary: 'custrecord_format_supplementary',
		extSchema: 'custrecord_ext_schema'
	};
};
Tax.DAO.TaxReportMapperDAO.prototype = Object.create(Tax.DAO.RecordDAO.prototype);

Tax.DAO.TaxReportMapperDAO.prototype.prepareSearch = function prepareSearch(params) {
	this.columns = [
		new nlobjSearchColumn(this.fields.name),
		new nlobjSearchColumn(this.fields.countryName),
		new nlobjSearchColumn(this.fields.transactionName),
		new nlobjSearchColumn(this.fields.mapType),
		new nlobjSearchColumn(this.fields.isEUCountry),
		new nlobjSearchColumn(this.fields.altCode),
		new nlobjSearchColumn(this.fields.internalid),
		new nlobjSearchColumn(this.fields.isTaxFilingConfig),
		new nlobjSearchColumn(this.fields.isNonDeductible),
		new nlobjSearchColumn(this.fields.executionContext),
		new nlobjSearchColumn(this.fields.enableFeature),
		new nlobjSearchColumn(this.fields.isFilterByLocation),
		new nlobjSearchColumn(this.fields.TaxClass),
		new nlobjSearchColumn(this.fields.location),
		new nlobjSearchColumn(this.fields.formatSupplementary),
		new nlobjSearchColumn(this.fields.extSchema)
	];
	
	if (params.name) {
		this.filters.push(new nlobjSearchFilter(this.fields.name, null, 'is', params.name));
	}
	
	if (params.mapType) {
		this.filters.push(new nlobjSearchFilter(this.fields.mapType, null, 'is', params.mapType));
	}
	
	if (params.isEUCountry) {
		this.filters.push(new nlobjSearchFilter(this.fields.isEUCountry, null, 'is', params.isEUCountry));
	}
};

Tax.DAO.TaxReportMapperDAO.prototype.ListObject = function _listObject(id) {
	return {
		id: id,
		text: '',
		name: '',
		countryName: '',
		countryNameText: '',
		transactionName: '',
		mapType: '',
		isEUCountry: '',
		alternateCode:'',
		internalId: '',
		isTaxFiling: '',
		isNonDeductible: '',
		executionContext: '',
		enableFeature: '',
		isFilterByLocation: '',
		TaxClass: '',
		location: '',
		formatSupplementary: '',
		extensionSchema: ''
	};
};

Tax.DAO.TaxReportMapperDAO.prototype.rowToObject = function _rowToObject(row) {
	var details = new this.ListObject(row.getId());
	details.text = row.getValue(this.fields.name);
	details.name = row.getValue(this.fields.name);
	details.countryName = row.getValue(this.fields.countryName);
	details.countryNameText = row.getText(this.fields.countryName);
	details.transactionName = row.getValue(this.fields.transactionName);
	details.mapType = row.getValue(this.fields.mapType);
	details.isEUCountry = row.getValue(this.fields.isEUCountry);
	details.alternateCode = row.getValue(this.fields.altCode);
	details.internalId = row.getValue(this.fields.internalid);
	details.isTaxFiling = row.getValue(this.fields.isTaxFilingConfig);
	details.isNonDeductible = row.getValue(this.fields.isNonDeductible);
	details.executionContext = row.getValue(this.fields.executionContext);
	details.enableFeature = row.getValue(this.fields.enableFeature);
	details.isFilterByLocation = row.getValue(this.fields.isFilterByLocation);
	details.TaxClass = row.getValue(this.fields.TaxClass);
	details.location = row.getValue(this.fields.location);
	details.formatSupplementary = row.getValue(this.fields.formatSupplementary);
	details.extensionSchema = row.getValue(this.fields.extSchema);

	return details;
};

//Child Record
Tax.DAO.TaxReportMapperDetailsDAO = function _TaxReportMapperDetailsDAO() {
	Tax.DAO.RecordDAO.call(this);
	this.Name = 'TaxReportMapperDetailsDAO';
	this.recordType = 'customrecord_tax_report_map_details';
	this.fields = {
		name: 'name',
		type: 'custrecord_type',
		format: 'custrecord_format',
		language: 'custrecord_language',
		taxReportTemplate: 'custrecord_tax_report_template',
		taxReportMap: 'custrecord_tax_report_map',
		plugin: 'custrecord_plugin',
		detailInternalId: 'custrecord_detail_internalid',
		detailLabel: 'custrecord_detail_label',
		subType: 'custrecord_subType',
		validUntil: 'custrecord_valid_until',
		effectiveFrom: 'custrecord_effective_from',
		meta: 'custrecord_meta',
		schema: 'custrecord_schema'
	};
};

Tax.DAO.TaxReportMapperDetailsDAO.prototype = Object.create(Tax.DAO.RecordDAO.prototype);

Tax.DAO.TaxReportMapperDetailsDAO.prototype.prepareSearch = function prepareSearch(params) {
	this.columns = [
		new nlobjSearchColumn(this.fields.name).setSort(),
		new nlobjSearchColumn(this.fields.type).setSort(),
		new nlobjSearchColumn(this.fields.format),
		new nlobjSearchColumn(this.fields.language),
		new nlobjSearchColumn(this.fields.taxReportTemplate),
		new nlobjSearchColumn(this.fields.taxReportMap),
		new nlobjSearchColumn(this.fields.plugin),
		new nlobjSearchColumn(this.fields.detailInternalId),
		new nlobjSearchColumn(this.fields.detailLabel),
		new nlobjSearchColumn(this.fields.subType),
		new nlobjSearchColumn(this.fields.validUntil),
		new nlobjSearchColumn(this.fields.effectiveFrom),
		new nlobjSearchColumn(this.fields.meta),
		new nlobjSearchColumn(this.fields.schema),
	];
	
	if (params.name) {
		this.filters.push(new nlobjSearchFilter(this.fields.name, null, 'is', params.name));
	}

	if (params.type) {
		this.filters.push(new nlobjSearchFilter(this.fields.type, null, 'is', params.type));
	}

	if (params.taxReportMap) {
		this.filters.push(new nlobjSearchFilter(this.fields.taxReportMap, null, 'anyof', params.taxReportMap));
	}

	if (params.detailInternalId) {
		this.filters.push(new nlobjSearchFilter(this.fields.detailInternalId, null, 'is', params.detailInternalId));
	}
	
	if (params.countryCode) {
		this.filters.push(new nlobjSearchFilter(this.fields.name, 'custrecord_tax_report_map', 'is', params.countryCode));
	}
	
	if (params.languageCode) {
		this.filters.push(new nlobjSearchFilter(this.fields.language, null, 'is', params.languageCode));
	}
};

Tax.DAO.TaxReportMapperDetailsDAO.prototype.ListObject = function _listObject(id) {
	return {
		id: id,
		name: '',
		type: '',
		format: '',
		language: '',
		taxReportTemplate: '',
		taxReportMap: '',
		plugin: '',
		detailInternalId: '',
		label: '',
		subType: '',
		validUntil: '',
		effectiveFrom: '',
		meta: '',
		taxFormSchema: ''
	};
};

Tax.DAO.TaxReportMapperDetailsDAO.prototype.rowToObject = function _rowToObject(row) {
	var details = new this.ListObject(row.getId());
	details.name = row.getValue(this.fields.name);
	details.type = row.getValue(this.fields.type);
	details.format = row.getValue(this.fields.format);
	details.language = row.getValue(this.fields.language);
	details.taxReportTemplate = row.getValue(this.fields.taxReportTemplate);
	details.taxReportMap = row.getValue(this.fields.taxReportMap);
	details.plugin = row.getValue(this.fields.plugin);
	details.detailInternalId = row.getValue(this.fields.detailInternalId);
	details.label = row.getValue(this.fields.detailLabel);
	details.subType = row.getValue(this.fields.subType);
	details.validUntil = row.getValue(this.fields.validUntil);
	details.effectiveFrom = row.getValue(this.fields.effectiveFrom);
	details.meta = row.getValue(this.fields.meta);
	details.taxFormSchema = row.getValue(this.fields.schema);

	return details;
};
