/**
 * Copyright 2015 NetSuite Inc. User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */
var Tax = Tax || {};
Tax.DAO = Tax.DAO || {};

Tax.DAO.VATOnlineConfigDAO =  function VATOnlineConfigDAO() {
	Tax.DAO.RecordDAO.call(this);
	this.Name = 'VATOnlineConfigDAO';
	this.recordType = 'customrecord_tax_return_setup_item';
	this.columns = [];
	this.filters = [];
};
Tax.DAO.VATOnlineConfigDAO.prototype = Object.create(Tax.DAO.RecordDAO.prototype);

Tax.DAO.VATOnlineConfigDAO.prototype.prepareSearch = function prepareSearch(params) {
	this.columns = [
		new nlobjSearchColumn('custrecord_vat_cfg_country'),
		new nlobjSearchColumn('custrecord_vat_cfg_subsidiary'),
		new nlobjSearchColumn('custrecord_vat_cfg_name'),
		new nlobjSearchColumn('custrecord_vat_cfg_type'),
		new nlobjSearchColumn('custrecord_vat_cfg_value'),
		new nlobjSearchColumn('custrecord_vat_cfg_taborder'),
		new nlobjSearchColumn('custrecord_vat_cfg_label'),
		new nlobjSearchColumn('custrecord_vat_cfg_isvisible'),
		new nlobjSearchColumn('custrecord_vat_cfg_ismandatory'),
		new nlobjSearchColumn('custrecord_vat_cfg_helptext'),
		new nlobjSearchColumn('custrecord_vat_cfg_product'),
		new nlobjSearchColumn('custrecord_vat_cfg_nondeductible'),
		new nlobjSearchColumn('custrecord_vat_cfg_newtaxfiling')
	];

	if (params.countryCode) {
		this.filters.push(new nlobjSearchFilter('custrecord_vat_cfg_country', null, 'is', params.countryCode));
	}

	if (params.subsidiary) {
		this.filters.push(new nlobjSearchFilter('custrecord_vat_cfg_subsidiary', null, 'is', params.subsidiary));
	}
};

Tax.DAO.VATOnlineConfigDAO.prototype.ListObject = function listObject(row) {

	return {
		countrycode: row.getValue('custrecord_vat_cfg_country'),
		subsidiaryid: row.getValue('custrecord_vat_cfg_subsidiary'),
		name: row.getValue('custrecord_vat_cfg_name'),
		type: row.getValue('custrecord_vat_cfg_type'),
		value: row.getValue('custrecord_vat_cfg_value'),
		taborder: row.getValue('custrecord_vat_cfg_taborder'),
		label: row.getValue('custrecord_vat_cfg_label'),
		isvisible: row.getValue('custrecord_vat_cfg_isvisible'),
		ismandatory: row.getValue('custrecord_vat_cfg_ismandatory'),
		help: row.getValue('custrecord_vat_cfg_helptext'),
		product: row.getValue('custrecord_vat_cfg_product'),
		isnondeductible: row.getValue('custrecord_vat_cfg_nondeductible'),
		isnewtaxfiling: row.getValue('custrecord_vat_cfg_newtaxfiling'),
	};
};