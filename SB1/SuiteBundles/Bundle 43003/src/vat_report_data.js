/**
 * Copyright 2014 NetSuite Inc.  User may not copy, modify, distribute, or re-bundle or otherwise make available this code.
 */

VAT.ReportData = function(params) {
    // Report-specific
    this.languageCode = params.languageCode;
    this.periodFrom = params.periodFrom;
    this.periodTo = params.periodTo;
    this.subId = params.subId;
    this.isConsolidated = params.isConsolidated;
    this.saleCacheId = params.saleCacheId;
    this.purchaseCacheId = params.purchaseCacheId;
    this.className = params.className;
    this.bookId = params.bookId;
    params.nexus = this.CountryCode;
    params.taxCodeDefs = new VAT.TaxcodeDefinitions(this.CountryCode, this.TaxDefinition);

    this.params = params;
    this.otherParams = params.otherParams;
    this.userInfo = params.userInfo;

    this.DataReader = new VAT.DataReader(params);
    this.DataHeader = new VAT.DataHeader(params);
    // TODO: Consider dependency injection for DataReader and DataHeader
};

VAT.ReportData.prototype.GetReportData = function() {
    var data = this.GetData();
    var headerData = this.GetHeaderData(this.languageCode);
    return this.ProcessReportData(data, headerData);
};

VAT.ReportData.prototype.ProcessReportData = function(data, headerData) {
    return {};
};

VAT.ReportData.prototype.GetHeaderData = function() {
    return this.DataHeader;
};

VAT.ReportData.prototype.GetData = function() {
    return {};
};

VAT.ReportData.prototype.GetAdjustmentMetaData = function() {
    return {
        TaxDefinition: this.TaxDefinition,
        TaxMap: this.TaxMap
    };
};

VAT.ReportData.prototype.GetDrilldownData = function(boxNumber) {
    return {};
};

VAT.ReportData.prototype.GetPrintData = function() {
    var reportData = this.GetReportData(); 
    return this.ProcessPrintData(reportData);
};

VAT.ReportData.prototype.ProcessPrintData = function(reportData) {
    for (var k in this.otherParams) {
        if (this.otherParams[k]) {
            reportData[k] = this.otherParams[k];
        } else {
            reportData[k] = '&nbsp;';
        }
    }
    
    reportData.library = _App.GetLibraryFile(VAT.LIB.FORMAT).getValue();
    
    return reportData;
};

VAT.ReportData.prototype.GetExportData = function() {
    var reportData = this.GetReportData(); 
    return this.ProcessExportData(reportData);
};

VAT.ReportData.prototype.ProcessExportData = function(reportData) {
    return {};
};
