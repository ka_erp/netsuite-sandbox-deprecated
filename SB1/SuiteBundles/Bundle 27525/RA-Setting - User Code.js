/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       30 IX 2013     mkaluza
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
//	nlapiLogExecution('DEBUG', 'Before load', 'Add checkbox and date fields');
	if (type == 'create' || type == 'edit') {
		form.addField('custpage_ra_setting_value_checkbox', 'checkbox', 'Flag');
		form.addField('custpage_ra_setting_value_date', 'date', 'Date');
	}
}

function userEventAfterSubmit(type) {
	if (type == 'create' || type == 'edit') {
	    var externalId = nlapiGetFieldValue ( 'externalid' );
	    var name = nlapiGetFieldValue ( 'name' );
	    var type = nlapiGetFieldText ('custrecord_ra_setting_type');
	    var prefix = '';
	    nlapiLogExecution('DEBUG', 'After load', 'Type: ' + type + ' Name: ' + name + ' ExternalID: ' + externalId);
	    
	    switch (type) {
	    case 'NS Replication settings':							prefix = 'REP_SET_';	break;
	    case 'RARS Replication XML Settings':					prefix = 'REP_XML_';	break;
	    case 'Custom Record TypeId for NS.InboundTriggers':		prefix = 'REP_TRG_'; 	break;
	    case 'RA Settings':										prefix = 'POS_SET_';	break;
	    case 'RA Flags':										prefix = 'POS_FLG_';	break;
	    default:												prefix = 'POS_OTH_'; 		break;
	    }
	    
	    var externalIdName = prefix + name;
	    if (externalId == null || externalId == "" || externalIdName != externalId) {
	       var record = nlapiLoadRecord ( nlapiGetRecordType(), nlapiGetRecordId() );
	       record.setFieldValue ( 'externalid', externalIdName );
	       nlapiSubmitRecord ( record, false );
	    }
	}
}