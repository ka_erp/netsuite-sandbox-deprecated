/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 XI 2013     mkaluza
 *
 */

/**
 * @param {Number} toversion
 * @returns {Void}
 */
function beforeInstall(toversion) {
	nlapiLogExecution('DEBUG', 'Before Install', '');
	
}

/**
 * @param {Number} toversion
 * @returns {Void}
 */
function afterInstall(toversion) {
	nlapiLogExecution('DEBUG', 'After Install', '');
	updateDiscounts();
	//updateTaxSchedules();
}

/**
 * @param {Number} fromversion
 * @param {Number} toversion
 * @returns {Void}
 */
function beforeUpdate(fromversion, toversion) {
	nlapiLogExecution('DEBUG', 'Before Update', '');
}

/**
 * @param {Number} fromversion
 * @param {Number} toversion
 * @returns {Void}
 */
function afterUpdate(fromversion, toversion) {
	nlapiLogExecution('DEBUG', 'After Update', '');
	updateDiscounts();
	//updateTaxSchedules();
}

//---------------------------------------------------------------------------------------------------------------------------------------------------
//RA-Tax Schedule
//---------------------------------------------------------------------------------------------------------------------------------------------------

//function updateTaxSchedules() {
//	nlapiLogExecution('DEBUG', 'Tax Schedules', 'Update Tax Schedules');
	
	// Check if there are some discounts
//	var columns = new Array();
//	columns[0] = new nlobjSearchColumn('internalid',null,null);
//	columns[1] = new nlobjSearchColumn('name',null,null);
//	columns[2] = new nlobjSearchColumn('description',null,null);
//	var taxScheduleResults = nlapiSearchRecord('taxschedule', null, null, columns);
//	if (taxScheduleResults && taxScheduleResults.length > 0) {
//		for (var i = 0; i < taxScheduleResults.length; i++) {
//			nlapiLogExecution('DEBUG', 'Create Tax Schedule', 
//					'Id: ' + taxScheduleResults[i].getValue('internalid') + 
//					' name: ' + taxScheduleResults[i].getValue('name') + 
//					' description: ' + taxScheduleResults[i].getValue('description'));
			
//			var id = taxScheduleResults[i].getValue('internalid');
//			var name = taxScheduleResults[i].getValue('name');
//			var description = taxScheduleResults[i].getValue('description');
			
//			var taxScheduleLinkId = GetTaxScheduleLinkId(id);
//			var recTaxSchedule = taxScheduleLinkId == -1 ? nlapiCreateRecord('customrecord_ra_tax_schedule') : nlapiLoadRecord('customrecord_ra_tax_schedule', taxScheduleLinkId);
//			recTaxSchedule.setFieldValue('name', name);
//			recTaxSchedule.setFieldValue('custrecord_ra_taxschedule_description', description);
//			recTaxSchedule.setFieldValue('custrecord_link_taxschedule_id', id);
//			recTaxSchedule.setFieldValue('custrecord_ra_taxschedule_recepit_text', name.substring(0,2));
//			nlapiSubmitRecord(recTaxSchedule);
//		}
//	}
//}

//function GetTaxScheduleLinkId(id) {
//	var filters = new Array();
//	filters[0] = new nlobjSearchFilter('custrecord_link_taxschedule_id', null, 'equalTo', id);
//	var column = new nlobjSearchColumn('internalid',null,null);
//	var searchResults = nlapiSearchRecord('customrecord_ra_tax_schedule', null, filters, column);
	
//	if (searchResults && searchResults.length == 1) {
//		return searchResults[0].getValue('internalid');
//	} 
//	return -1;
//}








//---------------------------------------------------------------------------------------------------------------------------------------------------
// RA-Discount
//---------------------------------------------------------------------------------------------------------------------------------------------------

function updateDiscounts() {
	nlapiLogExecution('DEBUG', 'Discounts', 'Update discounts');
	var typePercentOff = 1;
	var typeAmountOff = 2;
	var typePriceOverride = 3;
	
	// Check if there are some discounts
	var column = new nlobjSearchColumn('internalid',null,null);
	var discountResults = nlapiSearchRecord('discountitem', null, null, column);
	if (discountResults && discountResults.length > 0) {
		for (var i = 0; i < discountResults.length; i++) {
			var discountId = discountResults[i].getValue('internalid');

			var nativeDiscountItem = nlapiLoadRecord('discountitem', discountId);
			var rate = nativeDiscountItem.getFieldValue('rate');
			if (rate == null) {
				rate = '0.0';
			}
			var reductionType = nativeDiscountItem.getFieldValue('custpage_ra_discount_reduction_type');
			if (reductionType == null) {
				reductionType = rate.charAt(rate.length-1) == '%' ? typePercentOff : typeAmountOff;
				nativeDiscountItem.setFieldValue('custpage_ra_discount_reduction_type', reductionType);
			}

			nativeDiscountItem.setFieldValue('rate', rate);
			nlapiLogExecution('DEBUG', 'Discounts', 'Discount: ' + discountId);
			nlapiSubmitRecord(nativeDiscountItem);
		}
	} else if (discountResults == null || discountResults.length == 0) {
		
		// Create initial discounts
		// RA-Discount types and record in RA-Change Log will be automatically created as written in 'NS-Discount - User Code.js file'
		CreateDefaultDiscount('Percent Off', '0.0%', typePercentOff, 'P0');
		CreateDefaultDiscount('Amount Off', '0.0', typeAmountOff, 'A0');
		CreateDefaultDiscount('Price Override', '0.0', typePriceOverride, 'N0');
	}
}

//Search for linked RA-Discount
function GetDiscountLinkedId(id) {
	if (id) {
		var filters = new Array();
		filters[0] = new nlobjSearchFilter('custrecord_ra_discount_link', null, 'equalTo', id);
		var column = new nlobjSearchColumn('internalid',null,null);
		var searchResults = nlapiSearchRecord('customrecord_ra_discount', null, filters, column);
		
		if (searchResults && searchResults.length == 1) {
			return searchResults[0].getValue('internalid');
		}
	}
	return -1;
}

//Save record to RA-Change Log
function AddItemToChangeLog(type,id) {
	var recChange = nlapiCreateRecord('customrecordchangelog');
	recChange.setFieldValue('custrecordrecordtype', type);
	recChange.setFieldValue('custrecordrecordid', id);
	nlapiSubmitRecord(recChange);
}

function CreateDefaultDiscount(name, rate, reduction_type, externalId) {
	var discountItem = nlapiCreateRecord('discountitem');			
	discountItem.setFieldValue('itemid', name);
	discountItem.setFieldValue('rate', rate);
	discountItem.setFieldValue('nonposting','T');
	discountItem.setFieldValue('custpage_ra_discount_reduction_type', reduction_type);
	discountItem.setFieldValue('includechildren', 'T');
	nlapiSubmitRecord(discountItem);
	
	var discountItemLinkId = GetDiscountLinkedId(discountItem.getId());
	nlapiLogExecution('DEBUG', 'Discounts', 'Default discount - id: ' + discountItem.getId() + ' link id:' + discountItemLinkId);
	if  (discountItemLinkId != -1) {
		var customDiscountItem = nlapiLoadRecord('customrecord_ra_discount', discountItemLinkId);
		customDiscountItem.setFieldValue('externalid', externalId);
		nlapiSubmitRecord(customDiscountItem);
	}
}