/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       25 XI 2013     mkaluza
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged(type, name, linenum){
	nlapiLogExecution('DEBUG', 'Field', 'Type: ' + type + ' name: ' + name);
	var typePercentOff = 1;
	var typeAmountOff = 2;
	var typePriceOverride = 3;
	
	if (name == 'rate') {
		var rate = nlapiGetFieldValue('rate');
		var reduction_type = nlapiGetFieldValue('custpage_ra_discount_reduction_type');
		if (reduction_type != typePriceOverride || rate != 0.0) {
			if (rate.charAt(rate.length-1) == '%') {
				nlapiSetFieldValue('custpage_ra_discount_reduction_type', typePercentOff);
			} else {
				nlapiSetFieldValue('custpage_ra_discount_reduction_type', typeAmountOff);
			}
		}
	}
	
	if (name == 'custpage_ra_discount_reduction_type') {
		var reduction_type = nlapiGetFieldValue('custpage_ra_discount_reduction_type');
		var rate = nlapiGetFieldValue('rate');
		nlapiLogExecution('DEBUG', 'Field', 'Type: ' + reduction_type);
		if (reduction_type == typePercentOff && rate.charAt(rate.length-1) != '%') {
			nlapiSetFieldValue('rate', rate + '%');
		}
		if (reduction_type == typeAmountOff && rate.charAt(rate.length-1) == '%') {
			nlapiSetFieldValue('rate', rate.substring(0, rate.length-1));
		}
		if (reduction_type == typePriceOverride) {
			nlapiSetFieldValue('rate', 0.0);	
		}
	}
}
