/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       21 X 2013     mkaluza
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	nlapiLogExecution('DEBUG', 'after submit', 'type: "' + type + '"');
	
	var name = nlapiGetFieldValue('name');
	var rate = nlapiGetFieldValue('custrecord_ra_discount_rate');
	var reductionType = nlapiGetFieldValue('custrecord_ra_discount_reduction_type');
	var displayName = nlapiGetFieldValue('custrecord_ra_discount_display_name');
	var description = nlapiGetFieldValue('custrecord_ra_discount_description');
	var id = nlapiGetNewRecord().getId();
	var recordType = nlapiGetRecordType();
	var discountLink = nlapiGetFieldValue('custrecord_ra_discount_link');

	if (type == 'create') {
		nlapiLogExecution('DEBUG', 'Create', 'Id: ' + id + ' RecordType: ' + recordType + ' Name: ' + name + ' rate: ' + rate + 
				' ReductionType: ' + reductionType + ' Display Name: ' + displayName + ' Description: ' + description + ' DiscountLink: ' + discountLink);
		
		var recDiscount = nlapiCreateRecord('discountitem');
		recDiscount.setFieldValue('itemid', name);
		recDiscount.setFieldValue('rate', rate);
		recDiscount.setFieldValue('displayname', displayName);
		recDiscount.setFieldValue('description', description);
		recDiscount.setFieldValue('nonposting','T');
		nlapiSubmitRecord(recDiscount);
		
		// Set discount internal Id as a link to RA-Discount		
		var newRecord = nlapiLoadRecord(recordType, id);
		newRecord.setFieldValue('custrecord_ra_discount_link', recDiscount.getId());
		nlapiSubmitRecord(newRecord);
	
	} else if (type == 'edit') {
		nlapiLogExecution('DEBUG', 'Edit', 'Discount: ' + discountLink + ' Name: ' + name + ' rate: ' + rate + ' Display Name: ' + displayName + ' Description: ' + description);
		
		var discountItem = nlapiLoadRecord('discountItem', discountLink);
		discountItem.setFieldValue('displayname', displayName);
		discountItem.setFieldValue('description', description);
		discountItem.setFieldValue('itemid', name);
						
		if (reductionType == 1) {
			discountItem.setFieldValue('rate', rate + '%');
		} else if (reductionType == 2) {
			discountItem.setFieldValue('rate', rate);
		} else if (reductionType == 3) {
			discountItem.setFieldValue('rate', 0.0);
		}
		
		nlapiSubmitRecord(discountItem);
	
	} else if (type == 'delete') {
		nlapiLogExecution('DEBUG', 'Delete', 'Discount: ' + discountLink + ' Name: ' + name);
		nlapiDeleteRecord('discountItem', discountLink);
	}	

}

function userEventBeforeLoad(type, form, request){
	
	
}
