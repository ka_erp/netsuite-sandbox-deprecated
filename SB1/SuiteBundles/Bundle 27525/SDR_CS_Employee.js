/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       12 VIII 2013     mkaluza
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type){
	// 2-01 simple Alert statement to start us off
	//alert('Hello World!');
	
	// 3-01 prepopulate phone, display Supervisor info
	nlapiSetFieldValue('phone', '11111111');
	
	var email = nlapiGetFieldValue('email');
//	nlapiSetFieldValue('email', 'myownemail' + email, false, false);
}
