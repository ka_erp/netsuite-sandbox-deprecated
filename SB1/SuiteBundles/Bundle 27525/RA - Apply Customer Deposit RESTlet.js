function postItemReceipt(datain) {
    // datain should be JSON from RAPOS
    // { 
    //   "salesOrderExternalId":"00000000000000000000000000000000",
    //   "invoiceExternalId":"00000000000000000000000000000000",
    //   "amount":"#.##"
    // }

    var filters = new Array();
    filters[0] = new nlobjSearchFilter("externalid", null, "is", datain.invoiceExternalId, null);

    var col = new Array();
    col[0] = new nlobjSearchColumn("internalId");

    var results = nlapiSearchRecord("invoice", null, filters, col);

    if (results == null || results.length == 0 ) return "error";

    var invoiceInternalId = results[0].getValue("internalId");

    filters = new Array();
    filters[0] = new nlobjSearchFilter("externalidstring", null, "startswith", datain.salesOrderExternalId, null);
    filters[1] = new nlobjSearchFilter("mainline", null, "is", "T", null);

    col = new Array();
    col[0] = new nlobjSearchColumn("internalId");

    results = nlapiSearchRecord("customerdeposit", null, filters, col);

    var customerPayment = nlapiTransformRecord("invoice", invoiceInternalId, "customerpayment");

    // Go through list of invoices and find our invoice, marking it for apply
    for (var j = 1; j <= customerPayment.getLineItemCount("apply"); j++) {
        if (invoiceInternalId == customerPayment.getLineItemValue("apply", "doc", j)) {
            customerPayment.selectLineItem("apply", j);
            customerPayment.setCurrentLineItemValue("apply", "apply", "T");
            customerPayment.commitLineItem("apply");
            break;
        }
    }

    // Go through list of deposits and find our deposit(s)
    // There may be more than one deposit depending on how many different payment methods they used when paying
    // for the deposit.
    for (var i = 0; results != null && i < results.length; i++) {
        for (var j = 1; j <= customerPayment.getLineItemCount("deposit"); j++) {
            if (results[i].getValue("internalId") == customerPayment.getLineItemValue("deposit", "doc", j)) {
                customerPayment.selectLineItem("deposit", j);
                customerPayment.setCurrentLineItemValue("deposit", "apply", "T");
                customerPayment.commitLineItem("deposit");
                break;
            }
        }
    }

    nlapiSubmitRecord(customerPayment);

    return "success";
}