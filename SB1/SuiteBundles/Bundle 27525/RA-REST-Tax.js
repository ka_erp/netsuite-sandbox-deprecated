/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       24 May 2013     bsomerville      Provides a simple lookup method for tax codes.
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function getNonTaxForCountry(country) {
		  
	  nlapiLogExecution('DEBUG', 'Lookup Non-Tax for Country', country);
	
	  var filters = new Array(); 
	  var columns = new Array(); 
	  filters[0] = new nlobjSearchFilter("formulanumeric",null,"equalto",0.00).setFormula("{rate}");
	  filters[1] = new nlobjSearchFilter("country",null,"is",country);			
	  columns[0] = new nlobjSearchColumn("internalid",null,null); 						  		 
				
	  var results = nlapiSearchRecord("taxgroup",null,filters,columns); 
			
	  if(results) {
		 nlapiLogExecution('DEBUG', '(b-submit) Found Replacement for ' + country, results[0].getValue(columns[0]));
		 return results[0].getValue(columns[0]);
	  }
	  
	  return -8;
}

function getNonTaxID(datain) {
	var country = datain.Country;
	var subsidiary = datain.Subsidiary;
    var location = datain.Location;
    var taxId = -8;
    
    if(!country || country == '') {
    	if(subsidiary) {
    	  var subsidiaryRecord = null;
    	  
    	  try {
    	  	subsidiaryRecord = nlapiLoadRecord("subsidiary", subsidiary, null);
    	  } catch(err) { /* Subsidiary not found */ }	
    	  
    	  if(subsidiaryRecord)
    		  country = subsidiaryRecord.getFieldValue("country");
    	  
    	} else if (location) {
  		  
    	  var locationRecord = null;
    	  
    	  try {
    	     locationRecord = nlapiLoadRecord("location", location, null);
    	  } catch (err) { /* Location not found */ }
    	  
		  if(locationRecord)
			  country = locationRecord.getFieldValue("country");		  
    	}
    }
    
    if(country) {
    	taxId = getNonTaxForCountry(country);
    }
    
    var object = new Object();
    object.Country = country;
    object.TaxId = taxId;
    
    return object;
}