function fieldChanged(type, name) {
    if (name == "custrecord_ra_moneymanageaction_type") {
        nlapiDisableField("custrecord_ra_moneymanageaction_reason",
            nlapiGetFieldText("custrecord_ra_moneymanageaction_type") != "Funds Receipt"
            && nlapiGetFieldText("custrecord_ra_moneymanageaction_type") != "Funds Disbursement");
    }

    if (name == "custrecord_ra_moneymanageaction_reason" && nlapiGetFieldValue("custrecord_ra_moneymanageaction_reason")) {
        // Pull the account from the reason
        var filters = new Array();
        filters[0] = new nlobjSearchFilter("internalid", null, "is", nlapiGetFieldValue("custrecord_ra_moneymanageaction_reason"), null);
        var cols = new Array();
        cols[0] = new nlobjSearchColumn("custrecord_ra_fundsrecdisbreason_account");

        var account = -1;

        var results = nlapiSearchRecord("customrecord_ra_fundsrecdisbreason", null, filters, cols);
        for (var i = 0; results != null && i < results.length; i++) {
            account = results[0].getValue("custrecord_ra_fundsrecdisbreason_account");
        }

        if (nlapiGetFieldText("custrecord_ra_moneymanageaction_type") == "Funds Receipt") {
            // Set the credit account
            nlapiSetFieldValue("custrecord_ra_moneymanageaction_credit", account, true, true);
        } else {
            // Set the debit account
            nlapiSetFieldValue("custrecord_ra_moneymanageaction_debit", account, true, true);
        }
    }
}