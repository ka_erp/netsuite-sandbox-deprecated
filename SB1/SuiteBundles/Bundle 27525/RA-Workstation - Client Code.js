/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       13 Sep 2013     dotocka
 *
 */

var PCC = 1;
var MPS = 2;
var Shift4 = 3;

var NotProvisioned = 1;
var ToBeProvisioned = 2;
var InProvisioning = 3;
var Provisioned = 4;

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord customrecord_ra_workstation
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
function clientPageInit(type){
	var value = nlapiGetFieldText("custrecord_ra_cc_provider");
	var id = nlapiGetFieldValue("custrecord_ra_cc_provider");
	
	nlapiLogExecution('DEBUG', 'Page init provider', 'name:' + value + " id:" + id);
	processPaymentProviderFields(id);
}

/**
 * Method that goes through the provider fields and shows/hides them based on selected payment provider value
 * @param {String} name id of the provider in provider list (PCCharge - 1, MPS - 2, Shift4 - 3...)
 */
function processPaymentProviderFields(id){
	nlapiDisableField( "custrecord_ra_provider_info_username", id != PCC );
	nlapiDisableField ( "custrecord_ra_provider_info_processor", id != PCC );
	nlapiDisableField ( "custrecord_ra_provider_info_merchant", id != PCC && id != MPS && id != Shift4 );
	nlapiDisableField ( "custrecord_ra_provider_info_folder", id != PCC );
	nlapiDisableField ( "custrecord_ra_provider_info_xservers", id != MPS );
	nlapiDisableField ( "custrecord_ra_provider_info_gservers", id != MPS );
	nlapiDisableField ( "custrecord_ra_provider_info_proxyserver", id != MPS );
	nlapiDisableField ( "custrecord_ra_provider_info_url", id != Shift4 );
	nlapiDisableField ( "custrecord_ra_provider_info_accesscode", id != Shift4 );
	nlapiDisableField ( "custrecord_ra_provider_info_serialnumber", id != Shift4 );
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord customrecord_ra_workstation
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged(type, name, linenum){
	if (name == "custrecord_ra_cc_provider") {
		var value = nlapiGetFieldText(name);
		var id = nlapiGetFieldValue("custrecord_ra_cc_provider");
		nlapiLogExecution('DEBUG', 'Field changed to provider', 'name:' + value + " id:" + id);
		processPaymentProviderFields(id);
	}
}

/**
 * Check if mandatory field value is not empty, if it is alert the user.
 * @param {String} textName Name of the field that is supposed to be in alert message
 * @param {String} fieldName The internal ID name of the field being checked
 * @returns {Boolean} false if the field is empty, true otherwise
 */
function checkMandatoryFieldValueAlert(textName, fieldName){
	var value = nlapiGetFieldValue(fieldName);
	if (!value || value.length == 0){
		alert(textName + " field in provider info is empty. Please fill it.");
		return false;
	}
	return true;
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord customrecord_ra_workstation
 *   
 * @returns {Boolean} True to continue save, false to abort save
 */
function clientSaveRecord(){
	var value = nlapiGetFieldText("custrecord_ra_cc_provider");
	var id = nlapiGetFieldValue("custrecord_ra_cc_provider");
	nlapiLogExecution('DEBUG', 'Save workstation - provider', 'name:' + value + " id:" + id);
	if (!value || value.length == 0){
		return true;
	}
	
	if (id == MPS || id == PCC || id == Shift4){
		if(!checkMandatoryFieldValueAlert("Merchant", "custrecord_ra_provider_info_merchant")){
			return false;
		}
	}
	if (id == MPS){
		if(!checkMandatoryFieldValueAlert("xServers", "custrecord_ra_provider_info_xservers") || 
				!checkMandatoryFieldValueAlert("gServers", "custrecord_ra_provider_info_gservers") || 
				!checkMandatoryFieldValueAlert("Proxy server", "custrecord_ra_provider_info_proxyserver")){
			return false;
		}
	}
	else if (id == PCC){
		if(!checkMandatoryFieldValueAlert("User name", "custrecord_ra_provider_info_username") || 
				!checkMandatoryFieldValueAlert("Processor", "custrecord_ra_provider_info_processor") || 
				!checkMandatoryFieldValueAlert("Folder", "custrecord_ra_provider_info_folder")){
			return false;
		}
	}
	else if (id == Shift4){
		if(!checkMandatoryFieldValueAlert("Url", "custrecord_ra_provider_info_url") || 
				!checkMandatoryFieldValueAlert("Access code", "custrecord_ra_provider_info_accesscode") || 
				!checkMandatoryFieldValueAlert("Serial number", "custrecord_ra_provider_info_serialnumber")){
			return false;
		}
	}
	
	return true;
}

function prepareParameters() {
	var workstationId = nlapiGetRecordId();
	var locationId = nlapiGetFieldValue('custrecord_ra_ws_loc');
	
	nlapiLogExecution('DEBUG', 'Provider copy From', 'ws: ' + workstationId + ' loc: ' + locationId);
	
	return '&ws=' + workstationId + '&lc=' + locationId;
}

function providerInfoCopyFrom() {
	var suitelet_url = nlapiResolveURL('SUITELET', 'customscript_ra_provider_info_copyfrom', 'customdeploy_ra_prv_info_copyfromdep');
	var url = suitelet_url + prepareParameters();
	var winopts = 'height=300,width=650,toolbar=no,menubar=no';
	window.open(url ,'pvdInfCopyFrom',winopts);
}

function providerInfoCopyTo() {
	var suitelet_url = nlapiResolveURL('SUITELET', 'customscript_ra_provider_info_copyto', 'customdeploy_ra_prv_info_copytodep');
	var url = suitelet_url + prepareParameters();
	var winopts = 'height=600,width=650,toolbar=no,menubar=no';
	window.open(url ,'pvdInfCopyTo',winopts);
}

function provisionStart() {
	var workstationId = nlapiGetRecordId();
	var workstation = nlapiLoadRecord('customrecord_ra_workstation', workstationId);
	
	var status = nlapiGetFieldValue('custrecord_ra_ws_provisioning_status');
	if(status == Provisioned && !confirm("Workstation was already provisioned, are you sure you want to provision again?")) {//check if the station is provisioned already
		return false;
	}
	else{
		workstation.setFieldValue('custrecord_ra_ws_provisioning_status', 1);
	}
	//set checkbox to start to generate URL upon workstation submit record
	workstation.setFieldValue('custrecord_ra_provisioning_set_chbx', 'T');
	
	nlapiSubmitRecord(workstation, false, true);
	
	var url = nlapiResolveURL('RECORD', 'customrecord_ra_workstation', workstationId);
	window.location = url;
}