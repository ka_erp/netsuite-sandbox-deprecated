function getInventory(datain) {
    var internalId = datain.internalId;

    nlapiLogExecution("debug", "Inventory Check", internalId);

    var filters = new Array();
    filters[0] = new nlobjSearchFilter("internalid", null, "is", internalId, null);

    var results = "";

    var searchResults = nlapiSearchRecord("item", "customsearch_ra_inventorycheck_search", filters, null);
    for (var i = 0; searchResults != null && i < searchResults.length; i++) {
        // locationquantityavailable
        // internalid

        results += searchResults[i].getValue("internalid","inventoryLocation") + ":" + searchResults[i].getValue("locationquantityavailable") + "|";
        /*
        var columns = searchResults[i].getAllColumns();
        for (var j = 0; j < columns.length; j++) {
            results += columns[j].getJoin() + ":" + columns[j].getName() + ":" + searchResults[i].getValue(columns[j].getName(), columns[j].getJoin()) + "|";
        }
        return results;
        */
    }

    return results;
}