/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       27 May 2013     bsomerville
 *
 */

/**
 * @param {Object} dataIn Parameter object
 * @returns {Object} Output object
 */
function itemInfo(dataIn) {
	
	// Action to perform (see below)
	var action = dataIn.Action;
	
	// The item ID, with or without the NS prefix (ITM, NIS, etc)
	var itemID = dataIn.Item;
	
	// The RAPOS Barcode, which maps to UPC Code in NS. Used to aid in lookups.
	var raposID = dataIn.RaposId;
	var itemName = dataIn.Name;
	var bin = dataIn.UseBin == 'true';
	var binqty = dataIn.BinQuantity;
	var binLoc = dataIn.Location;
	var oiName = dataIn.OpenItemName;
	var oiBarcode = dataIn.OpenItemBarcode;	
	var exists = false;
	var itemRecord = (itemID || raposID ) ? itemRecordFromID(itemID,raposID) : null;
		
	var resultText = 'No item found.';
	
	nlapiLogExecution("DEBUG", "Bin lookup (do)", bin);	
	
	switch(action) {
	// Exists - Returns true when the item exists and is active.
		case "Exists":
		  exists = (itemRecord != null);
		  resultText = exists ? "Item exists." : "Item does not exist.";
		  break;
    // Ensure Exists - Always returns a valid item ID, providing the Open Item ID if no valid item is found.
		case "Ensure Exists":
		  if(bin && itemRecord) {	
			  if (!binForItem (itemRecord, binLoc, binqty))	{	  			 
				  nlapiLogExecution("DEBUG", "Bin fail", "have < need");
				  itemRecord = null;
			  }
		  } 
			
		  if (!itemRecord || itemRecord.getFieldValue('isinactive') == 'T') {			  			  			 
			  nlapiLogExecution("DEBUG", "Did not find item", itemID);
			 			 			  
			  itemRecord = getOpenItem(bin, oiName, oiBarcode);
			  
			  if(!itemRecord) {
				  itemRecord = null; //getCustomerFromLocation(location);
				  resultText = "No item found, created, or extrapolated.";
			  } else {
				  resultText = "Item does not exist, but an Open Item was found.";
			  }
		  } else {
			  resultText = "Item exists.";
		  }
		  exists = true;
		  break;
   // Sets the item to be active (isinactive = 'F') if the item exists.
		case "Activate":
			resultText = setItemActive (itemRecord, true);
			break;
   // Set the item to be inactive (isinactive = 'T') if the item exists.
		case "Deactivate":
			resultText = setItemActive (itemRecord,false);
			break;
   // Returns the Open Item ID, creating one if necessary.
		case "Open Item":
			itemRecord = getOpenItem();
			result = "Open Item";
			break;
		default:
		  throw "Invalid action - " + action;
	}
	
	var result = new Object();			
	result.Item = itemRecord != null ? itemRecord.getId() : "NOT FOUND";
	result.RaposId = itemRecord != null ? itemRecord.getFieldValue('upccode') : '';	
	result.Exists = exists;	
	result.IsInactive = itemRecord != null ? itemRecord.getFieldValue('isinactive') : '';
	result.Name = itemRecord != null ? itemRecord.getFieldValue ("displayname") : '';		
	result.BinOnHand = itemRecord != null ? itemRecord.getFieldValue ("binonhandavail") : '0.0';
	result.BinNumber = itemRecord != null ? itemRecord.getFieldValue ("binnumber") : '0';
	result.Type = itemRecord != null ? 	itemRecord.getRecordType() : '';
	result.Subtype = itemRecord != null ? 	itemRecord.getFieldValue ("subtype") : '';
	result.Result = resultText;
	
	// Prefix will be added to the item ID - ITM for inventory items, for example.
	var prefix = '';
	
	switch(result.Type) {
		case "inventoryitem":
			prefix = 'ITM';
			break;
		case "assemblyitem":
			prefix = 'ASM';
			break;
		case "kititem":
			prefix = 'KIT';
			break;
		case "service":
			prefix = 'SRV';
			break;
		case "giftcertificateitem":
			prefix = '';
			break;
		case "otherchargeitem":
			prefix = 'OCS';
			break;
		case "noninventoryitem":
			if(result.Subtype == "Sale") 
				prefix = 'NIS';
			else if (result.Subtype = "Resale")
				prefix = 'NIR';
			break;		
	}
	
	result.Item = itemRecord != null ? prefix + itemRecord.getId() : "NOT FOUND";
	
	return result;
}

function binForItem ( item, location, qty) {
	var binCount = item.getLineItemCount("binnumber");
	var foundBin = false;
	
	nlapiLogExecution("DEBUG", "Bin Lookup for Location " + location, binCount);
		
	// Does this item use bins? If not, let's get out of here right now. 
	var useBin = item.getFieldValue("usebins");	
	
	if(!useBin || useBin == 'F') return true;
	
	if(binCount && binCount > 0) {
		
		for (var binnum = 1; binnum <= binCount; binnum++) {
		    
			//nlapiLogExecution("DEBUG", "Bin Location (Claimed)", item.getLineItemValue("binnumber", "location", binnum));
			
		    if( item.getLineItemValue("binnumber", "location", binnum) == location && item.getLineItemValue("binnumber","preferredbin", binnum) == 'T' ) {
		    	foundBin = true;
		    	var oh = item.getLineItemValue ("binnumber","onhandavail", binnum);	    
		    	
		    	if (!oh) oh = 0;
		    	
		    	nlapiLogExecution("DEBUG", "Bin Has", oh);
		    	nlapiLogExecution("DEBUG", "Bin Needs", qty);
		    	
		    	if(oh < qty)
		    		return false;
		    	
		    	break;
		    }
		    
		}
	}
	
	return foundBin;
}

function itemRecordFromID ( itemID, raposID ) {
		
	nlapiLogExecution("DEBUG", "Lookup Item", itemID);
	
	var lookupitemID = '';
	var prefix = '';
	for(var s=0; s < itemID.length; s++) {		
		if (!isNaN(parseInt(itemID[s]))) {
			lookupitemID += itemID[s];
		} else {
			prefix += itemID[s];
		}
	}
	
	nlapiLogExecution("DEBUG", "Lookup Item (Post)", lookupitemID);
	nlapiLogExecution("DEBUG", "Lookup Item (Prefix)", prefix);
	
	//var prefix = itemID.substring(0,3);
	var item = null;	
	
	// If a prefix was provided, select the most logical item type to perform a quick lookup.
	if(prefix) 
		switch(prefix) {
		case "ITM":
			try { item = nlapiLoadRecord("inventoryitem", lookupitemID, null); } catch (err) {}
			break;
		case "NIS":
		case "NIR":
			try { item = nlapiLoadRecord("noninventoryitem", lookupitemID, null); } catch (err) {}
			break;			
		case "ASM":
			try { item = nlapiLoadRecord("assemblyitem", lookupitemID, null); } catch (err) {}
			break;
		case "KIT":
			try { item = nlapiLoadRecord("kititem", lookupitemID, null); } catch (err) {}
			break;
		case "SRV":
			try { item = nlapiLoadRecord("serviceitem", lookupitemID, null); } catch (err) {}
			break;
		}
	
	// Try all item types, if no prefix was specified or if the prefix doesn't match the item type
	if(!item) try { item = nlapiLoadRecord("inventoryitem", lookupitemID, null); } catch (err) { }
	if(!item) try { item = nlapiLoadRecord("noninventoryitem", lookupitemID, null); } catch (err) {}
	if(!item) try { item = nlapiLoadRecord("assemblyitem", lookupitemID, null); } catch (err) {}
	if(!item) try { item = nlapiLoadRecord("serviceitem", lookupitemID, null); } catch (err) {}
	if(!item) try { item = nlapiLoadRecord("kititem", lookupitemID, null); } catch (err) {}
	if(!item) try { item = nlapiLoadRecord("giftcertificateitem", lookupitemID, null); } catch (err) {}
	if(!item) try { item = nlapiLoadRecord("otherchargeitem", lookupitemID, null); } catch (err) {}

	// Try again using the RAPOS ID, searching on UPC Code
	if(!item && raposID) {
		nlapiLogExecution("DEBUG", "Lookup Item UPC", raposID);
		
		var filters = new Array(); 
		var columns = new Array(); 
		filters[0] = new nlobjSearchFilter("upccode",null,"is",raposID);
		columns[0] = new nlobjSearchColumn("internalid",null,null); 

		var results = null;	
		
		if(!results) try { results = nlapiSearchRecord("inventoryitem",null,filters,columns); } catch (err) {}
		if(!results) try { results = nlapiSearchRecord("noninventoryitem",null,filters,columns); } catch (err) {}
		if(!results) try { results = nlapiSearchRecord("assemblyitem",null,filters,columns); } catch (err) {}
		if(!results) try { results = nlapiSearchRecord("serviceitem",null,filters,columns); } catch (err) {}
		if(!results) try { results = nlapiSearchRecord("kititem",null,filters,columns); } catch (err) {}
		if(!results) try { results = nlapiSearchRecord("giftcertificateitem",null,filters,columns); } catch (err) {}
		if(!results) try { results = nlapiSearchRecord("otherchargeitem",null,filters,columns); } catch (err) {}				
		
		if(results) {
			itemID = results[0].getValue(columns[0]);			
			if(!item) try { item = nlapiLoadRecord("inventoryitem", itemID, null); } catch (err) {nlapiLogExecution("DEBUG", "Failed Item", err);  }
			if(!item) try { item = nlapiLoadRecord("noninventoryitem", itemID, null); } catch (err) {}
			if(!item) try { item = nlapiLoadRecord("assemblyitem", itemID, null); } catch (err) {}
			if(!item) try { item = nlapiLoadRecord("serviceitem", itemID, null); } catch (err) {}
			if(!item) try { item = nlapiLoadRecord("kititem", itemID, null); } catch (err) {}
			if(!item) try { item = nlapiLoadRecord("giftcertificateitem", itemID, null); } catch (err) {}
			if(!item) try { item = nlapiLoadRecord("otherchargeitem", itemID, null); } catch (err) {}
		}
	}
	
		
	return item;
}

function setItemActive ( item, active) {
	
	if(item) {
		
		var currentActive = item.getFieldValue("isinactive") != (active ? "F" : "T");
		
		nlapiLogExecution("DEBUG", "Set Active - Current",  item.getFieldValue("isinactive"));
		
		// Change item active status if needed
		if(currentActive) {		
			item.setFieldValue("isinactive", active ? "F" : "T");
			nlapiSubmitRecord(item, false, true);
			result = "Set to " + (active ? "active" : "inactive") + ".";
						
		} else {
			result = "No change made.";
		}
	} else {
		result = "No item found.";
	}
	
	nlapiLogExecution("DEBUG", "Set Active", result);
	return result;
}

function getOpenItem (binItem, oiName, oiBarcode) {
	var item = null;
	
	nlapiLogExecution("DEBUG", "Get Open Item", oiBarcode);
	
	if (!oiBarcode || oiBarcode == '') {
		oiBarcode = binItem ? 'RA_OPEN_BIN_ITEM' : 'RA_OPEN_ITEM';
		oiName = binItem ? "Open Item (Bin)" : "Open Item";
	}
	
	nlapiLogExecution("DEBUG", "Get Open Item (2)", oiBarcode);
	
	// Look up the Open Item ID. 
	var filters = new Array(); 
	var columns = new Array(); 
	
	filters[0] = new nlobjSearchFilter("upccode",null,"is",oiBarcode);
	columns[0] = new nlobjSearchColumn("internalid",null,null); 
	
	var results = nlapiSearchRecord("noninventoryitem",null,filters,columns);
	
	if(results) {
		item = nlapiLoadRecord("noninventoryitem", results[0].getValue(columns[0]), null);	
	}
	
	// No item was found - create one.
	if(!item) {
		item = nlapiCreateRecord("noninventoryitem", null);
		item.setFieldValue("upccode", oiBarcode);
		item.setFieldValue("displayname", oiName);		
		item.setFieldValue("itemid", oiName);
		item.setFieldValue("taxschedule", 1);
		item.setFieldValue("includechildren", "T");
		
		nlapiSubmitRecord(item, true, false);			
	}
	
	return item;
}


