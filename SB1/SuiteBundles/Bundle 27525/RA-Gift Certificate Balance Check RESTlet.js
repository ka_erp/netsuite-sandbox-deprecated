function getGiftCertificateBalance ( datain ) {
   var giftCertCode = datain.giftCertCode;

   nlapiLogExecution ( "debug", "Gift Cert Balance Check", giftCertCode );

   var filters = new Array();
   filters[0] = new nlobjSearchFilter("giftcertcode", null, "is", giftCertCode, null);

   var columns = new Array();
   columns[0] = new nlobjSearchColumn("amountremaining", null);
   var searchResults = nlapiSearchRecord ( "giftcertificate", null, filters, columns );
   for ( var i = 0; searchResults != null && i < searchResults.length; i++ ) {
       return searchResults[i].getId() + ": " + searchResults[i].getValue("amountremaining");
   }
}