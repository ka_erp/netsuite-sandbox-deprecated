function userEventAfterSubmit(type, form, request){
	nlapiLogExecution('DEBUG', 'Action', type);
	nlapiLogExecution('DEBUG', 'Request', request);
	
	var promoID = nlapiGetFieldValue('custrecord_ra_promoitem_promo');
	
	if(promoID) {
		var promo = nlapiLoadRecord('customrecord_ra_promo', promoID, null);
		nlapiSubmitRecord(promo, true, true);
	}
}

function userEventAfterSubmitPromo(type, form, request){
	nlapiLogExecution('DEBUG', 'Action', type);
	nlapiLogExecution('DEBUG', 'Request', request);
	
	var promoID = nlapiGetRecordId();
	
	if(promoID) {
		var promo = nlapiLoadRecord('customrecord_ra_promo', promoID, null);
		nlapiSubmitRecord(promo, true, true);
	}
}