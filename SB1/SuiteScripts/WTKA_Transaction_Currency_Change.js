/**
 *	File Name		:	WTKA_transaction_currency_change.js
 *	Function		:	Update currency on PO 
 * 	Authors			:	Manish Prajapati
 *	Company			:	Wipro Limited
 *	Release Dates	:	<To be determined>
 * 	Current Version	:	1.0
**/
/*On Before submit - Function is getting called  and update transaction currency based on user selection */
function updateTransactionCurrency()
{
	try
	{
		var context=nlapiGetContext().getExecutionContext();
		var contextType=type;
      
        var recOldEmployee = nlapiGetOldRecord();
        //nlapiLogExecution('debug', 'this old thin', JSON.stringify(recOldEmployee));
      
       var exchRate = nlapiGetFieldValue('exchangerate');  exchRate = parseFloat(exchRate); 
       nlapiLogExecution('audit', 'exchange rate', exchRate);
	
		var exchRate = nlapiSetFieldValue('exchangerate',1.0);
	
		if ((context == 'userinterface') && ((contextType == 'edit')))
		{
			
			nlapiLogExecution('debug', 'how far', '1');
		
			var transactionCurrency= nlapiGetFieldValue('currency');
			var transactionCurrencyChangeValue= nlapiGetFieldValue('custbody_erp_currency_change');
			nlapiLogExecution('DEBUG', 'Transaction Currency','Transaction Currency= '+transactionCurrency +' transactionCurrencyChangeValue ='+transactionCurrencyChangeValue);		
			nlapiLogExecution('DEBUG', 'transactionCurrencyChangeValue' +transactionCurrencyChangeValue.length);		
			if (transactionCurrency!=transactionCurrencyChangeValue && transactionCurrencyChangeValue !='')
			{			
		
				nlapiLogExecution('debug', 'how far', '2');
				nlapiSetFieldValue('currency', transactionCurrencyChangeValue);
				
				//IVAN START
				
				var ordType = nlapiGetFieldValue('type'); //if(ordType) ordType = ordType.toLowerCase;

				
				nlapiLogExecution('debug', 'ordType', ordType);
				
				if(ordType == "purchord"){
					
					nlapiLogExecution('debug', 'how far', '3');
				
						
						var intItemCount = nlapiGetLineItemCount('item'); intItemCount  = parseInt(intItemCount); 
						nlapiLogExecution('audit', 'intItemCount', intItemCount);
						
						for(var i = 1 ; i <= intItemCount; i++){
							
							var itemtype = nlapiGetLineItemValue('item','itemtype',i); nlapiLogExecution('audit', 'itemtype', itemtype);
							
							if(itemtype == 'InvtPart'){
								var rate = nlapiGetLineItemValue('item', 'rate', i);  rate = parseFloat(rate); 
								
								//retainrate = rate * exchRate; 
								
								
								var oldRate = recOldEmployee.getLineItemValue('item', 'rate', i); 
								nlapiLogExecution('audit', 'rate[' + i + ']', rate);
								nlapiSetLineItemValue('item', 'rate', i, oldRate); 
									
								/*									
								var tempObject = new Object(); 
								tempObject.rate = rate;
								tempObject.retainrate = retainrate;
								tempObject.exchRate = exchRate;
														
								nlapiLogExecution('audit', 'obj[' + i + ']', JSON.stringify(tempObject));*/
								
							}
							
						}
							
				}						
				//IVAN END				
				
			}
			else
			{
				nlapiLogExecution('DEBUG', 'Transaction Currency is same or EMPTY');		
			}
		}
	}
	catch(ex)
	{
		nlapiLogExecution('DEBUG','WTKA_transaction_currency_change::updateTransactionCurrency',ex);
	}
}