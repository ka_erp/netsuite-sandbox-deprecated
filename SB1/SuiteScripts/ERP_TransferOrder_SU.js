/**
 *	File Name		:	ERP_TransferOrder_SU.js
 *	Function		:	
 * 	Authors			:	christopher.neal@kitandace.com
 *	Company			:	Kit & Ace
 *	Release Dates	:	
 * 	Current Version	:	
 *
 *	Description: 	Suitelet to synchronize Transfer Orders with ESB/3PL. 
 *					
**/
function syncTransferOrder(request, response){


	if (request.getMethod() == 'GET'){
		nlapiLogExecution('DEBUG', 'Entered Suitelet', 'GET');

		var routerLogId = request.getParameter('routerlogid');

		nlapiLogExecution('DEBUG', 'routerLogId', routerLogId);

		processTransferRecord(routerLogId);

		response.write('Processed ID');
	} else {
		nlapiLogExecution('DEBUG', 'here', 'post');

		response.write('test post');
	}
}

/*
*	name: processTransferRecord
*	descr: 
*
*	author: christopher.neal@kitandace.com
*	@param: {id} routerLogId - ID of the Custom Router Record to be processed
*	@return: tbd
*
*/
function processTransferRecord(routerLogId){

	//load the custom record
	var routerRecord = nlapiLoadRecord('customrecord_external_sync_obj', routerLogId);
	var routerRequest = routerRecord.getFieldValue('custrecord_extsync_request');

	//load the Transfer Order record
	var transferOrderId = routerRecord.getFieldValue('custrecord_erp_ir_transactionlink');
	var transferOrderRecord = nlapiLoadRecord('transferorder', transferOrderId);

	fulfillTransferOrder(transferOrderId, routerRequest);

	receiveItems(transferOrderId, routerRequest);


}

/*
*	name: fulfillTransferOrder
*	descr: 	Determine whether to fulfill the transfer order.
*			Fulfill the necessary line items.
*
*	author: christopher.neal@kitandace.com
*	@param: {id} transferOrderId - ID of the transfer order
*	@param:	{obj} routerRequest - the request that was send by the ESB, will contain the line items
*	@return: tbd
*
*/
function fulfillTransferOrder(transferOrderId, routerRequest){

	var itemFulfillmentRecord = nlapiTransformRecord('transferorder', transferOrderId, 'itemfulfillment');
	var totalItems = itemFulfillmentRecord.getLineItemCount('item');

	//iterate through the items and receive the quantities from the request.
	for (var curItem = 1; curItem <= totalItems; curItem++){

		var qtyToFulfill = itemFulfillmentRecord.getLineItemValue('item', 'quantity', curItem); //THIS LINE WONT BE LIKE THIS, IT WILL GET THE VALUE FROM THE REQUEST

		itemFulfillmentRecord.setLineItemValue('item', 'quantity', curItem, qtyToFulfill);
	}

	itemFulfillmentRecord.setFieldValue('shipstatus', 'C'); //Set to 'C' for shipped

	var itemFulfillmentRecord = nlapiSubmitRecord(itemFulfillmentRecord);

}

/*
*	name: receiveItems
*	descr: 	Determine whether to receive the items.
*			Fulfill the necessary line items.
*
*	author: christopher.neal@kitandace.com
*	@param: {id} transferOrderId - ID of the transfer order
*	@param:	{obj} routerRequest - the request that was send by the ESB, will contain the line items
*	@return: tbd
*
*/
function receiveItems(transferOrderId, routerRequest){

	var itemReceiptRecord = nlapiTransformRecord('transferorder', transferOrderId, 'itemreceipt');

	var totalItems = itemReceiptRecord.getLineItemCount('item');

	//iterate through the items and receive the quantities from the request.
	for (var curItem = 1; curItem <= totalItems; curItem++){

		var qtyToReceive = itemReceiptRecord.getLineItemValue('item', 'quantity', curItem); //THIS LINE WONT BE LIKE THIS, IT WILL GET THE VALUE FROM THE REQUEST

		itemReceiptRecord.setLineItemValue('item', 'quantity', curItem, qtyToReceive);
	}

	var itemReceiptId = nlapiSubmitRecord(itemReceiptRecord);
}

/*
https://system.sandbox.netsuite.com/app/accounting/transactions/trnfrord.nl?id=3180225&whence=
*/