/**
 *	File Name		:	WTKA_ValidateChange_Currency_CL.js
 *	Function		:	Validate currency on PO 
 * 	Authors			:	Manish Prajapati
 *	Company			:	Wipro Limited
 *	Release Dates	:	<To be determined>
 * 	Current Version	:	1.0
**/

function validateLineCurrencyChange(){
	
	var checkReturn = nlapiGetFieldValue('custbody_erp_reset_line_rate');
	if(checkReturn ==  'T'){
		return false;
	}
	else{
		return true;
	}
}



function validateCurrencyChange(type, name)
{
	var retVal= true;	
	if(name=='custbody_erp_currency_change')
			{
				nlapiSetFieldValue('custbody_wtka_has_currency_changed', 'F');
				var changeCurrencyName=nlapiGetFieldText('custbody_erp_currency_change');
				var vendorInternalID =nlapiGetFieldValue('entity');
				if (changeCurrencyName=='')	{return retVal;}	
				if(vendorInternalID<0 || vendorInternalID==''){ alert('You must select vendor first.');}
				
				console.log('lets debug this');
				
				//IVAN
				var r = confirm("Changing the currency resets any change from the rates since opening this record");
				if (r == true) {
					
					//TRY
					/*nlapiSetFieldValue('custbody_erp_reset_line_rate', 'T');
					var intItemCount = nlapiGetLineItemCount('item'); 
						
					for(var i = 1 ; i <= intItemCount; i++){
						//nlapiSetLineItemValue('item', 'rate', i, 1); 
						nlapiSelectLineItem('item',i);
						nlapiSetCurrentLineItemValue('item', 'rate', 1.00);
						
					}*/
						
					//TRY
					
					
					retVal = true;
				} else {
					retVal = false;
				}
				
				
				//IVAN
				
				
				if (!isValidCurrency(vendorInternalID,changeCurrencyName))
				{
					alert('Selected currency is not associated with vendor, please select valid currency.');
					nlapiSetFieldValue('custbody_wtka_has_currency_changed', 'T');
					retVal= false;
				}
				
				
												
				
				
				
			}
		return retVal;			
}


function validateCurrencyChangeonSave()
{
	var hasInvalidCurrecyChange =nlapiGetFieldValue('custbody_wtka_has_currency_changed');
	if(hasInvalidCurrecyChange=='T')
	{
		alert('Selected currency is not associated with vendor, please select valid currency.');
		return false;
	}
	return true;
}



function isValidCurrency(vendorInternalID, changeCurrencyAsText)
{
	try{
		var retVal= false;
		var columns = new Array();
		columns[0] = new nlobjSearchColumn('currency', 'vendorcurrencybalance', null)
		var filters = new Array();
		filters[0] = new nlobjSearchFilter( 'internalid', null, 'is', vendorInternalID);			
		var searchresults = nlapiSearchRecord('vendor', null, filters,columns);
		if (searchresults!=null)
		{
			nlapiLogExecution('DEBUG', 'Search Result',JSON.stringify( searchresults));
			nlapiLogExecution('DEBUG', 'Search Result Length',searchresults.length);

		for (var resultItemIndex=0; resultItemIndex<searchresults.length; resultItemIndex++)
			{
						
					var searchresult = searchresults[resultItemIndex];																
					var vendorCurrency = searchresult.getValue(columns[0])
					nlapiLogExecution('DEBUG', 'Search Result ...vendorCurrency',vendorCurrency);
					if (vendorCurrency==changeCurrencyAsText)
					{
						retVal=true;
						break;
					}
						
			}		
			
		}	
		return retVal;
			
	}
	catch(err)
	{
		nlapiLogExecution('DEBUG',' isValidCurrency',err);		
		return false;
	}	
	
}
