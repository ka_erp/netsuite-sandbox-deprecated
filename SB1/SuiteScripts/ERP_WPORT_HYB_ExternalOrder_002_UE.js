/**
 *	File Name		:	ERP_WPORT_HYB_ExternalOrder_002_UE.js
 *	Function		:	Order Process - Step 2 (Decides which restlet to call for further processing)
 * 	Remarks			:	Relevant scripts isolated from WTKA_NetSuite_ICTO_Process.js
 *	Prepared by		:	A.Villanueva
 *	Release Dates	:	31-May-2016
 * 	Current Version	:	1.0
**/

function AfterSubmitICTO(type)
{

	// var exec = nlapiGetExecution();
	// var context = exec.getContext();
	//if type == xedit
	nlapiLogExecution('debug', 'type', type);

	if(type == 'create')
	{


		var tranId = nlapiGetFieldValue('custrecord_wtka_orderid');
		try
		{
			checkInventoryTransfer(nlapiGetRecordId());
		}
		catch(ue_err)
		{
			nlapiLogExecution('debug', 'checkInventoryTransfer Error', ue_err);
			var subject = 'Order processing failure';
			var body = 'Hello,<br><br>';
			body += 'Order request failed in NetSuite due to below error.<br>';
			body += '<br><b>Error Details: </b><br>' + JSON.stringify(ue_err);
			body += '<br><br><b>Data received in NetSuite: </b><br><br>' + JSON.stringify(dataIn) + '<br><br><br>';
			body += '<br><br>Process needs to be resubmitted or handled manually through user interface after corrections.';
			body += '<br><br><br><br><br><br><br>Thanks';
			body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
			nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
			if(sendEmail)	nlapiSendEmail(emailAuthor, toList, subject, body, ccList);			

			createTransactionLog(null, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, SYNC_RECORD_TYPE.TRANSFER, SYNC_STATUS.ERROR, JSON.stringify(ue_err), 2); //Library call to ERP_Logging_LIB.js
		}
	}
	else if(type == 'edit' || type =='xedit')
	{
		var rollback = nlapiGetFieldValue('custrecord_wtka_rollback');	
		nlapiLogExecution('debug', 'rollback', rollback);
		if(rollback == 'T')
		{
			/*try
			{
				// Trigger ROLLBACK RESTlet
				var orderRequest 			= new Object();
				orderRequest.id  			= nlapiGetRecordId();
				var processed_ids			= nlapiGetFieldValue('custrecord_wtka_processed_ids');
				orderRequest.orderDetails  	= (processed_ids != null || processed_ids != '') ? JSON.parse(processed_ids) : processed_ids;
				nlapiLogExecution('debug', 'Rollback - Request', JSON.stringify(orderRequest));
				var restletResponse 		= nlapiRequestURL(RollbkURL, JSON.stringify(orderRequest), headers);
				if(restletResponse.code == 200)
				{
					var respBody  = JSON.parse(restletResponse.body);
					var resp 	  = nlapiGetFieldValue('custrecord_wtka_final_response');
					if(resp != null)	finalResponse = JSON.parse(resp);
					finalResponse.records.push(respBody);
					respBody = JSON.stringify(finalResponse);
					
					setRecordFieldValues(orderRequest.id, null, finalResponse, 'F', 6);
				}
				nlapiLogExecution('debug', 'Rollback RESTLET Call', restletResponse.getBody());
			}
			catch(rollback_err)
			{
				nlapiLogExecution('debug', 'Rollback exception', rollback_err);
				var subject = 'Order processing failure';
				var body = 'Hello,<br><br>';
				body += 'Order request failed in NetSuite due to below error.<br>';
				body += '<br><b>Error Details: </b><br>' + JSON.stringify(rollback_err);
				body += '<br><br><b>Data received in NetSuite: </b><br><br>' + JSON.stringify(dataIn) + '<br><br><br>';
				body += '<br><br>Process needs to be resubmitted or handled manually through user interface after corrections.';
				body += '<br><br><br><br><br><br><br>Thanks';
				body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
				nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
				if(sendEmail)	nlapiSendEmail(emailAuthor, toList, subject, body, ccList);
			}*/
		}
		else
		{
			decideFlow(nlapiGetRecordId());
		}
	}
}

function checkInventoryTransfer(recID)
{
	//Invoke Inventory Transfers RESTLET for further processing 
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wtka_order_status', 	null, 'anyof', 1));
	filters.push(new nlobjSearchFilter('internalid', 					null, 'anyof', recID));

	var cols = new Array();
	cols.push(new nlobjSearchColumn('custrecord_wtka_orderid'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_inbound_request'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_request_counter'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_inventorytransfers'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_final_response'));
	
	var searchRecord = nlapiSearchRecord('customrecord_wtka_external_orders', null, filters, cols);
	if(searchRecord != null)
	{
		for(var i=0; i < searchRecord.length; i++)
		{
			var recordOrderId = searchRecord[i].getValue('custrecord_wtka_orderid');
			var recordFileId  = searchRecord[i].getValue('custrecord_wtka_inbound_request');
			var recordCounter = searchRecord[i].getValue('custrecord_wtka_request_counter');
			var recordId	  = searchRecord[i].getId();
			var recordResp	  = searchRecord[i].getValue('custrecord_wtka_final_response');
			var recordRequest = fetchRequestFromFile(recordFileId, recordCounter);
			var recordTrnfs	  = searchRecord[i].getValue('custrecord_wtka_inventorytransfers');
			if(recordRequest != 0)
			{
				nlapiLogExecution('DEBUG', 'Initiating Inventory Transfers RESTLET');
				var response = nlapiRequestURL(InvTrfURL, JSON.stringify(recordRequest), headers);
				nlapiLogExecution('DEBUG', 'RESPONSE', response.code);


				if(response.code == 200)
				{
					nlapiLogExecution('DEBUG', 'body', response.body);
					var fields 	  = new Array();
					var values    = new Array();
					var respBody  = JSON.parse(response.body); //respBody example: {"records":[{"status":"Success","transactionType":"Inventory Transfer","transactionId":"3429731","transactionNumber":"3082"}],"orderNumber":"00017097_chris13","status":"Success"}
					if(respBody.status == "Success")
					{
						var ordStatus 	= 2;
						var recResp 	= new Object();
						nlapiLogExecution('DEBUG', 'recordResp', recordResp);
						if(recordResp != null && recordResp != '')
						{
							recResp 		= JSON.parse(recordResp);
							nlapiLogExecution('DEBUG', 'recResp', JSON.stringify(recResp));
						}
						else
						{
							recResp.records = new Array();
						}
						recResp.status 	= 'Success';
						for(var r in respBody.records)		recResp.records.push(respBody.records[r]);
						
						var resp = JSON.stringify(recResp);
							
						//Fields to update on the WTKA_External_Order record					
						fields.push('custrecord_wtka_final_response');
						fields.push('custrecord_wtka_inventorytransfers');
						fields.push('custrecord_wtka_order_status');
						fields.push('custrecord_external_transactions_links');

						
						var transactionIds = nlapiGetFieldValues('custrecord_external_transactions_links'); //get the current list of linked transactions
						if (isEmpty(transactionIds)){
							transactionIds = []; //initiate the list, in case it is null
						}
						
						var invTrfVal    = JSON.parse(response.body);
						var invTrfValues = (recordTrnfs != null) ? recordTrnfs : "";
						for(var j in invTrfVal.records)
						{
							//put together array for field: TRANSACTIONS > TRANSACTION LINKS
							transactionIds.push(invTrfVal.records[j].transactionId); //add new transactions that occured last

							//put together string for field: INVENTORY TRANSFERS
							if(invTrfValues == ""){
								invTrfValues = invTrfVal.records[j].transactionId;
							} else {
								invTrfValues += ',' + invTrfVal.records[j].transactionId;
							} 	
						}

						nlapiLogExecution('DEBUG', 'transactionIds', JSON.stringify(transactionIds));

						//set values into the fields.
						values.push(resp);
						values.push(invTrfValues);
						values.push(ordStatus); //Inventory Transfer Processed
						values.push(transactionIds); 
						
						nlapiSubmitField('customrecord_wtka_external_orders', recordId, fields, values, false);	
						
						if(recordTrnfs == null || recordTrnfs == "" || (recordTrnfs.length != invTrfValues.length))
						{
							InvokeCloudhub('salesorder', 0, recordOrderId, 'Released', 'inventoryFlow');
						}
					}
					else
					{
						if(respBody.status == "Duplicate")
						{
							var ordStatus 	= 2; //Reset Status
							fields.push('custrecord_wtka_order_status');
							values.push(ordStatus); //Inventory Transfer Processed
							nlapiLogExecution('debug', 'Duplicate Inventory Transfer Request');
							nlapiSubmitField('customrecord_wtka_external_orders', recordId, fields, values, false);	
						}
						else
						{
							var ordStatus 	= 6; //Error
							var recResp 	= new Object();
							if (recordResp != null && recordResp != '')
							{
								recResp 		= JSON.parse(recordResp);
								recResp.status 	= respBody.status;
								recResp.records.push(respBody);
							}
							else
							{
								recResp = respBody;
							}
							var resp = JSON.stringify(recResp);
													
							fields.push('custrecord_wtka_final_response');
							fields.push('custrecord_wtka_order_status');
							
							values.push(resp);
							values.push(ordStatus); //Inventory Transfer Processed
							
							nlapiSubmitField('customrecord_wtka_external_orders', recordId, fields, values, false);	
						}
					}
				}
			}
		}
	}
	else
	{
		nlapiLogExecution('DEBUG', 'No records to process');
	}
}

function decideFlow(recID)
{
	var custRecord 	  = nlapiLoadRecord('customrecord_wtka_external_orders', recID);
	var resp 		  = custRecord.getFieldValue('custrecord_wtka_final_response');

	nlapiLogExecution('DEBUG', 'resp', resp);

	if(resp != null)	finalResponse = JSON.parse(resp);
	var completedFlag = custRecord.getFieldValue('custrecord_wtka_completed_request');
	
	try
	{
		var status = custRecord.getFieldValue('custrecord_wtka_order_status');
		nlapiLogExecution('debug', 'status', custRecord.getFieldText('custrecord_wtka_order_status'));
		if(status == 1)	checkInventoryTransfer(recID); // Inventory Reservation
		if(status == 3) // Completed Order
		{
			/* Trigger RESTlet */
			var orderRequest = new Object();
			orderRequest.id  = nlapiGetRecordId();
			
			var restletResponse = nlapiRequestURL(OrderURL, JSON.stringify(orderRequest), headers); 
			nlapiLogExecution('debug', 'Initial RESTLET Call', restletResponse.getBody());
			if(restletResponse.code == 200)
			{
				var fields 	  = new Array();
				var values    = new Array();
				var respBody  = JSON.parse(restletResponse.body);

				if(respBody.status != "Success")
				{
					setRecordFieldValues(recID, null, null, null, null, null, null, null, false); //Reset completed flag
				} else {

					nlapiLogExecution('DEBUG', 'Setting Order Logs', 'testing');
					nlapiLogExecution('DEBUG', 'recordId', nlapiGetRecordId());

					var orderDetails = nlapiGetFieldValue('custrecord_wtka_processed_ids');
					nlapiLogExecution('DEBUG', 'orderDetails', orderDetails);

					var custrecord_wtka_inventorytransfers = nlapiGetFieldValue('custrecord_wtka_inventorytransfers');
					nlapiLogExecution('DEBUG', 'custrecord_wtka_inventorytransfers', custrecord_wtka_inventorytransfers);

					// pluck_.pluck(list, propertyName) 
					// A convenient version of what is perhaps the most common use-case for map: extracting a list of property values.

					// var stooges = [{name: 'moe', age: 40}, {name: 'larry', age: 50}, {name: 'curly', age: 60}];
					// _.pluck(stooges, 'name');
					// => ["moe", "larry", "curly"]

					var a = _.pluck(resp, 'transactiontype');
					var b = _.pluck(resp, 'transactionid');

					nlapiLogExecution('DEBUG', 'JSON.stringify(a)', JSON.stringify(a));
					nlapiLogExecution('DEBUG', 'JSON.stringify(b)', JSON.stringify(b));


					// var orderDetails = JSON.parse(orderDetails);

					// var orderId = orderDetails[0].orderId;
					// var fulfillmentId = orderDetails[0].fulfillmentId;
					// var invoiceId = orderDetails[0].invoiceId;
					// nlapiLogExecution('DEBUG', 'orderId', orderId);
					// nlapiLogExecution('DEBUG', 'fulfillmentId', fulfillmentId);
					// nlapiLogExecution('DEBUG', 'invoiceId', invoiceId);

					// transactionRecordsArray = [];
					// transactionRecordsArray.push(orderId);
					// transactionRecordsArray.push(fulfillmentId);
					// transactionRecordsArray.push(invoiceId);

					// linkTransactionsToExternalWTKA(nlapiGetRecordId(), transactionRecordsArray);
				}
			}
		}
		if(status == 6) //Send Error Email
		{
			var InboundOrderNumber = custRecord.getFieldValue('custrecord_wtka_orderid');
			var subject = 'Order processing failure';
			var body 	= 'Hello,<br><br>';
			body += 'Order request failed in NetSuite due to below error.<br>';
			body += '<br><b>Error Details: </b><br>' + JSON.stringify(finalResponse);
			body += 'Process against <b>' + InboundOrderNumber + '</b> failed in NetSuite';
			body += '<br><br>Process needs to be resubmitted or handled manually through user interface after corrections.';
			body += '<br><br><br><br><br><br><br>Thanks';
			body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
			nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
			if(sendEmail)	nlapiSendEmail(emailAuthor, toList, subject, body, ccList);
		}
	}
	catch(err_create)
	{
		if(err_create.code == 'SSS_REQUEST_TIME_EXCEEDED') //re-trigger script
		{
			nlapiLogExecution('debug', 'Retriggering...');
			var orderRequest 	= new Object();
			orderRequest.id  	= nlapiGetRecordId();
			var restletResponse = nlapiRequestURL(OrderURL, JSON.stringify(orderRequest), headers);
			nlapiLogExecution('debug', 'Retriggered RESTLET', restletResponse.getBody());
		}
		else
		{
			var errObject 		  	= new Object();
			errObject.status 	 	= "Exception";
			errObject.message 	 	= "Order cannot be processed. Error:" + err_create;
			finalResponse.status 	= "Error";
			finalResponse.records.push(errObject);
			completedFlag = (completedFlag == 'T') ? false : null;
			setRecordFieldValues(recID, null, finalResponse, null, 6, null, null, null, completedFlag);
			nlapiLogExecution('debug', 'UE-catch', JSON.stringify(errObject));
		}	
	}
}