/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_Portlet_TransferOrderImport.js
 * Version            1.0.0.0
 * Description        Display Portlet for Transfer Order Import functionality.
 **/


function onStart(portlet, column)
{
    var suiteletURL = nlapiGetContext().getSetting("SCRIPT", "custscript_ec_to_suitelet_url");
    suiteletURL = suiteletURL + "&user=" + nlapiGetUser();
    portlet.setTitle('Transfer Order Import');
    portlet.setHtml('<html><div><iframe src="' + suiteletURL + '" scrolling="auto" width="100%" height="200px"></iframe></div></html>');
}


