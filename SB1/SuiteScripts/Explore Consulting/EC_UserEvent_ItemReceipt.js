/**
 * Company            Explore Consulting
 * Copyright            2014 Explore Consulting, LLC
 * Type                NetSuite EC_UserEvent_ItemReceipt
 * Version            1.0.0.0
 **/

if ( nlapiGetContext().getExecutionContext() == "PRODUCTION" )
	var SendErrorEmailsFrom = 125247;
else
	var SendErrorEmailsFrom = -5;


function onBeforeLoad(type, form, request) {

}

function onBeforeSubmit(type) {

}

function onAfterSubmit(type) {
	if ( (type == 'create' || type == 'edit') && nlapiGetContext().getExecutionContext() != 'scheduled' )
		EC.TriggerIntercompanyVendorPricingScript();
}


/**
 * Function to validate From Location field on the Transfer Order form.  This field must always be populated before any
 * lines are added to the order.  If populated, then a Suitelet is called to get the Transfer Pricing for the line.
 * @constructor
 */
EC.TriggerIntercompanyVendorPricingScript = function(){

	Log.d('Scheduling Script to update this Item Receipt');
	var params = {
		custscript_record_id: nlapiGetRecordId(),
		custscript_starting_line_number: 1
	};
	var result = nlapiScheduleScript('customscript_ec_sch_intercmpy_vndr_prcng', null, params);                  // 20 units
	Log.d('Scheduling Script RESULT:  ' + result);
	if ( result != 'QUEUED' )
	{
		Log.d('scheduled script not queued',
			'expected QUEUED but scheduled script api returned ' + result);
		var emailBody5 = "Script: EC_UserEvent_ItemReceipt.js\nFunction: TriggerIntercompanyVendorPricingScript\nError: There was an issue scheduling the scheduled script in this script";
		nlapiSendEmail(SendErrorEmailsFrom, "kasupport@exploreconsulting.com", "Error in EC_UserEvent_ItemReceipt.js", emailBody5);
	}
	else
	{
		Log.d('TriggerIntercompanyVendorPricingScript -- Re-Scheduling Script Status', "Result:  " + result);
	}
};


Log.AutoLogMethodEntryExit(null, true, true, true);
