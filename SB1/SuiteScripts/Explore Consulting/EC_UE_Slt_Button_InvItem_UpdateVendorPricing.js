/**
 * Company            Explore Consulting
 * Copyright          2015 Explore Consulting, LLC
 * Type               NetSuite EC_UE_Slt_Button_InvItem_UpdateVendorPricing.js
 * Version            1.0.0.0
 **/

// Function to add the button to the record form
// Deployed as a User Event, on Before Load
function addButton(type, form, request)
{
    try
    {
        // Only show if the Record is in View Mode			// Change as needed
        if ( type != 'view' && nlapiGetRecordType() != "inventoryitem" ) return;

        // Add button to form
        var id = nlapiGetRecordId();
        var rectype = nlapiGetRecordType();

        // The button script will be controlled via a suitelet.  See Suitelet code below.
        var url = nlapiResolveURL('SUITELET','customscript_ec_slt_vend_pricing_button','customdeploy_ec_slt_vend_pricing_button') + '&transactionRecord=' + id + '&transactionType=' + rectype;

        var roleId = nlapiGetContext().getRole();  

        if((roleId  == 1021) ||  (roleId  == 1065)  || (roleId  == 1048) || (roleId  == 1012) || (roleId  == 1050)){} else{
        form.addButton('custpage_update_vendor_pricing','Update Vendor Pricing', "document.location='"+url+"'");
        }
    }
    catch(e)
    {
        Log.d("addButton", "Unexpected Error:  " + e);
    }
}



/////////////////////////////   SUITELET FUNCTIONALITY    /////////////////////////////////////
/////////////////////////////    TRIGGERED BY BUTTON      /////////////////////////////////////
// Deploy the below as a Suitelet


if ( nlapiGetContext().getExecutionContext() == "PRODUCTION" )
    var SendErrorEmailsFrom = 125247;
else
    var SendErrorEmailsFrom = -5;

var recipient = 'kasupport@exploreconsulting.com';
var InterCompanyVendorSearchID = "97";      // Search:  InterCompany Vendor General View search (97)
var InterCompanyVendors = [];

/*
 *  Function triggered by custom Button on record form
 */
function buttonFunctionality(request, response)
{
    var itemID = request.getParameter( 'transactionRecord' );
    var itemType = request.getParameter( 'transactionType' );

    try
    {
        if ( !itemID || !itemType )
            throw "Parameters missing.";

        // Pre-load InterCompany Vendors data
        EC.GetInterCompanyVendorsCollection();

        EC.LoadAndProcessItem(itemID, itemType);

    }
    catch(e)
    {
        Log.d("buttonFunctionality ", "Unexpected Error:  " + e);
        nlapiSendEmail(SendErrorEmailsFrom, recipient, "Error in EC_UE_Slt_Button_InvItem_UpdateVendorPricing.js", "Processing Item:  " + itemID + "\nUnexpected Error:  " + e);
    }

    // After the button's functionality is complete, we will want to route the user back to the original record's form
    nlapiSetRedirectURL("RECORD", itemType, itemID, null, null);
}

EC.GetInterCompanyVendorsCollection = function(){

    InterCompanyVendors = nlapiSearchRecord("vendor", InterCompanyVendorSearchID);
    Log.d("InterCompany Vendors Collection", InterCompanyVendors);
};

EC.LoadAndProcessItem = function(itemid, itemtype){

    var itemRecord = nsdal.loadObject(itemtype, itemid, ["id", "type", "currency"])
        .withSublist("itemvendor", ["vendor", "purchaseprice"])
        .withSublist("locations", ["locationid", "averagecostmli"]);
    if ( itemRecord )
    {
        // If the Primary Currency of the Vendor is USD, then we will need to convert the Average Cost accordingly.
        var cost = EC.GetItemAverageCost(itemRecord);
        Log.d('Average Cost pulled from Item Record', cost);

        if ( cost ) {

            var lc = itemRecord.getLineItemCount("itemvendor");
            Log.d('Line Count of Current Item Vendors', lc);
            // Go through the Vendor Listing on this Item and remove any Intercompany Vendors already found there
            for ( var i=lc; i>0; i-- )
            {
                var lineNumber = parseInt(i);
                Log.d('Processing Line ' + lineNumber, 'Item Vendor:  ' + itemRecord.itemvendor[i-1].vendor);
                if (EC.IsVendorAnInterCompanyVendor(itemRecord.itemvendor[i-1].vendor)) itemRecord.removeLineItem("itemvendor", lineNumber);
            }
            Log.d('Line Count after clearing out InterCompany Vendors', itemRecord.getLineItemCount("itemvendor"));

            // Now add Vendor Pricing for all found InterCompany Vendors
            _.each(InterCompanyVendors, function (vendorDetails, index) {

                var line = parseInt(index) + 1;
                Log.d('Adding InterCompany Vendor ' + line, 'Vendor ID:  ' + vendorDetails.getId());
                var convertedCost = EC.ConvertToVendorCurrency(cost, vendorDetails.getText("currency"));
                Log.d('Converting average cost to Vendor Currency', 'convertedCost:  ' + convertedCost);

                // Get Vendor's Markup Percentage and add it to the currently calculated cost
                var markUpPercentage = vendorDetails.getValue("custentity_intercompany_markup_percent");
                Log.d('Vendor Markup Percentage', 'markUpPercentage:  ' + markUpPercentage);
                if (markUpPercentage) {
                    markUpPercentage = markUpPercentage.replace("%", "");
                    markUpPercentage = markUpPercentage/100;
                    var markedUpCost = (parseFloat(convertedCost) * parseFloat(markUpPercentage)) + parseFloat(convertedCost);
                    Log.d('Marked up Vendor Cost', 'markedUpCost:  ' + markedUpCost);

                    itemRecord.selectNewLineItem("itemvendor");
                    itemRecord.setCurrentLineItemValue("itemvendor", "vendor", vendorDetails.getId());
                    itemRecord.setCurrentLineItemValue("itemvendor", "purchaseprice", markedUpCost);
                    itemRecord.commitLineItem("itemvendor");
                }
                else
                {
                    Log.d('No Markup Percentage found on Vendor Record', 'Setting Vendor Price to ConvertedCost:  ' + convertedCost);
                    itemRecord.selectNewLineItem("itemvendor");
                    itemRecord.setCurrentLineItemValue("itemvendor", "vendor", vendorDetails.getId());
                    itemRecord.setCurrentLineItemValue("itemvendor", "purchaseprice", convertedCost);
                    itemRecord.commitLineItem("itemvendor");
                }
            });
            Log.d('Line Count after Adding Vendor Pricing', itemRecord.getLineItemCount("itemvendor"));

            itemRecord.setFieldValue('custitem_ec_intercompany_price_updated', nlapiDateToString(new Date(), "datetimetz"));
            var id = itemRecord.save(true, true);
            Log.d("ProcessItemVendors", "Item Record Re-saved:  " + id);
        }
    }
    else
    {
        Log.d("ProcessItemVendors", "Could not load Item Record");
    }
};

EC.ConvertToVendorCurrency = function(CAD_cost, vendorCurrency){

    // Pull the USD exchange rate as of today's date
    if (vendorCurrency != 'CAD') {
        var rate = nlapiExchangeRate('CAD', vendorCurrency, nlapiDateToString(new Date()));
        return parseFloat(CAD_cost) * rate;
    }
    else
        return parseFloat(CAD_cost);
};

EC.IsVendorAnInterCompanyVendor = function(vendor){

    if (!vendor) return false;
    var vendorFound = _.find(InterCompanyVendors, function(result){ return result.getId() == vendor; });
    if ( vendorFound ) {
        var representsSub = vendorFound.getValue("representingsubsidiary");
        if ( representsSub )
            return true;
        else
            return false;
    }
    else
        return false;
};

EC.GetItemAverageCost = function(itemRecord){

    var averageCost = 0.00;
    if ( itemRecord )
    {
        _.each(itemRecord.locations, function(locLine, index)
        {
            //Log.d('Processing Locations Line ' + index+1, 'Location:  ' + locLine.locationid);
            if ( locLine.locationid == 7 ){
                averageCost = locLine.averagecostmli;
                return;
            }
        });
    }
    return averageCost;
};

Log.AutoLogMethodEntryExit(null, true, true, true);
