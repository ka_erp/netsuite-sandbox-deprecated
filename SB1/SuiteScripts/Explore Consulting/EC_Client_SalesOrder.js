/**
 * Company            Explore Consulting
 * Copyright            2014 Explore Consulting, LLC
 * Type                NetSuite EC_Client_SalesOrder
 * Version            1.0.0.0
 **/
function onInit(type) {

}

function onSave() {

    return true;
}

function onValidateField(type, fld) {

    Log.d("onValidateField STARTING", "type:  " + type + "   fld:  " + fld);
    if ( fld == "shipaddress" )
        EC.CheckStateForAvatax_2();
    Log.d("onValidateField ENDING", "type:  " + type + "   fld:  " + fld);
    return true;
}

function onFieldChanged(type, fld) {

    if ( fld == "shipaddress" )
        Log.d("onFieldChanged STARTING", "type:  " + type + "   fld:  " + fld);
    //Log.d("onFieldChanged", "CONTEXT:  " + nlapiGetContext().getExecutionContext());

    if ( fld == "shipaddress" )
        EC.CheckStateForAvatax_2();
    Log.d("onFieldChanged ENDING", "type:  " + type + "   fld:  " + fld);

    return true;
}

function onPostSourcing(type, fld) {

    nlapiLogExecution("DEBUG", "FIELD CHECK", "subsidiary:  " + nlapiGetFieldValue('subsidiary') );
    if ( nlapiGetFieldValue('subsidiary') != 4) {
            nlapiLogExecution("DEBUG", "SUB IS NOT US - exiting", "subsidiary:  " + nlapiGetFieldValue('subsidiary') );
             return;
      }
    nlapiLogExecution("DEBUG", "FIELD CHECK", "shipaddress:  " + nlapiGetFieldValue('shipaddress') );
    nlapiLogExecution("DEBUG", "FIELD CHECK", "shipstate:  " + nlapiGetFieldValue('shipstate') + "  shipzip:  " + nlapiGetFieldValue('shipzip'));    
      
     if ( fld == "shipaddress" || fld == "shipmethod" ) {            // ADDED shipmethod 5/15/2015 to get around CA webstore tax issue
        Log.d("onPostSourcing STARTING", "Shipping Address Changed");
        EC.CheckStateForAvatax_2();
    }

    //Log.d("onPostSourcing", "CONTEXT:  " + nlapiGetContext().getExecutionContext());
    if ( type == "item" && fld == "item" ) {
        Log.d("onPostSourcing STARTING - ITEM", "type:  " + type + "   fld:  " + fld);
        EC.CheckStateForAvatax();
    }

    Log.d("onPostSourcing ENDING", "type:  " + type + "   fld:  " + fld);
}

function onLineSelect(type) {

}

function onValidateLine(type) {

    //if ( type == "item" ) EC.CheckStateForAvatax();
    return true;
}

function onLineRecalc(type) {

}

/**
 * Function that verify what type of tax is needed for this Sales Order
 */
EC.CheckStateForAvatax = function(){

    try
    {
        Log.d("CheckStateForAvatax  STARTING", "");
        // Only run this logic on Web Sales Orders
        // var source = nlapiGetFieldValue('source');
        //if ( source != "Web Services" ) return;

        var state = nlapiGetFieldValue('shipstate');
        if ( !state ) state = nlapiGetFieldValue('billstate');
        Log.d("CheckStateForAvatax", "state - using for lookup:  " + state);

        if ( state ) {
            // Call Suitelet to do lookup to Custom Record
            var response = nlapiRequestURL('https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=255&deploy=1&compid=3883338&h=ca18bb5a52ec028851ca' + '&state=' + state);
            var responseObj = JSON.parse(response.getBody());

            Log.d("CheckStateForAvatax", "Suitelet Response:  " + responseObj.statefound);
            if ( responseObj.statefound == true )     // State found in custom record list
            {
                Log.d("CheckStateForAvatax", "Taxable State");
                nlapiSetCurrentLineItemValue("item", "taxcode", 3193, false, true);      // 3193 = AVATAX tax code
                Log.d("Checking Tax Rate", nlapiGetCurrentLineItemValue("item", "taxcode"));
                //nlapiSetCurrentLineItemValue("item", "custcol_av_done", "T");

            }
            else if ( responseObj.statefound == false )      // State NOT found in list
            {
                Log.d("CheckStateForAvatax", "NON-Taxable State");
                nlapiSetCurrentLineItemValue("item", "taxcode", 3268, false, true);      // 3268 = KA Non-Taxable tax code
                Log.d("Checking Tax Rate", nlapiGetCurrentLineItemValue("item", "taxcode"));
                //nlapiSetCurrentLineItemValue("item", "custcol_av_done", "T");
            }
            else
            {
                Log.d("CheckStateForAvatax", "Response Unidentified");
            }
        }
        else
        {
            Log.d("CheckStateForAvatax", "No Ship State or Bill State Found");
        }
    }
    catch(e)
    {
        Log.d("CheckStateForAvatax", "Unexpected Error Occurred:  " + e);
    }
};

EC.CheckStateForAvatax_2 = function(){

    try
    {
        Log.d("CheckStateForAvatax_2  STARTING", "");
        var state = nlapiGetFieldValue('shipstate');
        if ( !state ) state = nlapiGetFieldValue('billstate');
        Log.d("CheckStateForAvatax_2", "state - using for lookup:  " + state);

        if ( state ) {

            // Call Suitelet to do lookup to Custom Record
            var response = nlapiRequestURL('https://forms.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=234&deploy=1&compid=3883338&h=4012529cfd360322e9db' + '&state=' + state);
            var responseObj = JSON.parse(response.getBody());
            Log.d("CheckStateForAvatax_2", "Suitelet Response:  " + responseObj.statefound);

            if ( responseObj.statefound == true )     // State found in custom record list
            {
                Log.d("CheckStateForAvatax_2", "Taxable State");
                Log.d("CheckStateForAvatax_2", "Line Count:  " + nlapiGetLineItemCount("item"));
                if ( nlapiGetLineItemCount("item") > 0 ) {
                    for (var i = 1; i <= nlapiGetLineItemCount("item"); i++) {
                        Log.d("CheckStateForAvatax_2", "Selecting Line " + i);
                        //nlapiSetLineItemValue("item", "taxcode", i, 3193);
                        nlapiSelectLineItem("item", i);
                        nlapiSetCurrentLineItemValue("item", "taxcode", 3193, false, true);
                        nlapiCommitLineItem("item");
                        Log.d("CheckStateForAvatax_2", "Updating Line " + i + " with code " + 3193);
                    }
                }
            }
            else if ( responseObj.statefound == false )      // State NOT found in list
            {
                Log.d("CheckStateForAvatax_2", "NON-Taxable State");
                Log.d("CheckStateForAvatax_2", "Line Count:  " + nlapiGetLineItemCount("item"));
                if ( nlapiGetLineItemCount("item") > 0 ) {
                    for (var i = 1; i <= nlapiGetLineItemCount("item"); i++) {
                        Log.d("CheckStateForAvatax_2", "Selecting Line " + i);
                        //nlapiSetLineItemValue("item", "taxcode", i, 3268);
                        nlapiSelectLineItem("item", i);
                        nlapiSetCurrentLineItemValue("item", "taxcode", 3268, false, true);
                        nlapiCommitLineItem("item");
                        Log.d("CheckStateForAvatax_2", "Updating Line " + i + " with code " + 3268);
                    }
                }
            }
            else
            {
                      Log.d("CheckStateForAvatax_2", "Suitelet Response Not Acceptable");
             }
        }
        else
        {
            Log.d("CheckStateForAvatax_2", "No Ship State or Bill State Found");
        }
    }
    catch(e)
    {
        Log.d("CheckStateForAvatax_2", "Unexpected Error Occurred:  " + e);
    }
    Log.d("CheckStateForAvatax_2  ENDING", "");
};
