/**
 * Company            Explore Consulting
 * Copyright            2014 Explore Consulting, LLC
 * Type                NetSuite EC_Portlet_FireScheduledScripts
 * Version            1.0.0.0
 * Description        Boilerplate startup code for a NetSuite RESTlet.
 **/

if ( nlapiGetContext().getEnvironment() == "PRODUCTION" )
    var suiteletURL = "https://forms.na1.netsuite.com/app/site/hosting/scriptlet.nl?script=244&deploy=1&compid=3883338&h=920d1b06dc40c4bf4448&user=" + nlapiGetUser();
else
    var suiteletURL = "https://forms.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=244&deploy=1&compid=3883338&h=920d1b06dc40c4bf4448&user=" + nlapiGetUser();

function onStart(portlet, column)
{
    portlet.setTitle('Adjustment Import');
    portlet.setHtml('<html><div><iframe src="' + suiteletURL + '" scrolling="auto" width="100%" height="200px"></iframe></div></html>');
}
