/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_Suitelet_InvAdj_Submit
 * Version            1.0.0.0
 **/

var InputData = {
    Date:undefined,
    Location:undefined,
    Department:undefined,
    Account:undefined,
    FileID:undefined,
    User:undefined,
    Memo:undefined
};

EC.ProcessRequest = function(request){

    InputData.Date = request.getParameter('date');
    Log.d("ProcessRequest", "PARAMETER - Date:  " + InputData.Date);
    InputData.Location = request.getParameter('location');
    Log.d("ProcessRequest", "PARAMETER - Location:  " + InputData.Location);
    InputData.Department = request.getParameter('department');
    Log.d("ProcessRequest", "PARAMETER - Department:  " + InputData.Department);
    InputData.Account = request.getParameter('account');
    Log.d("ProcessRequest", "PARAMETER - Account:  " + InputData.Account);
    InputData.User = request.getParameter('user');
    Log.d("ProcessRequest", "PARAMETER - User:  " + InputData.User);
};


    var HEADER_DETAIL_URL = "https://system.sandbox.netsuite.com/app/common/custom/custrecordentry.nl?rectype=207&id=";
    var FILES_FOLDER = 98038;

/**
 *
 * @param request
 * @param response
 * @constructor
 */
function InvAdjustmentUploader(request, response)
{
    // On GET display the form to the user
    if (request.getMethod() == 'GET')
    {
        //var form = nlapiCreateForm('Upload your file', false);
        var form = nlapiCreateForm('Inventory Adjustment Input', false);

        var group = form.addFieldGroup( 'groupone', 'Input Details');

        var dateField = form.addField('date', 'date', 'Date', null, 'groupone');
        dateField.setMandatory(true);
        var locField = form.addField('location','select','Location', null, 'groupone');
        CreateSelectField(locField, 'location');
        locField.setMandatory(true);
        var deptField = form.addField('department','select','Department', null, 'groupone');
        deptField.addSelectOption("","");
        CreateSelectField(deptField, 'department');
        deptField.setMandatory(false);
        var acctField = form.addField('account','select','Account', null, 'groupone');
        CreateSelectField(acctField, 'account');
        acctField.setMandatory(true);

        var fileField = form.addField('file', 'file', 'Adjustment File', null, null);
        fileField.setMandatory(true);

        var memoField = form.addField('memo', 'text', 'Adjustment Memo', null, null);
        memoField.setDisplaySize(57);

        // Set a custom User field so we can track who imported this file
        var loggedInUser = request.getParameter('user');
        Log.d("FORM GET - Show Form", "loggedInUser:  " + loggedInUser);
        var userField = form.addField('user', 'text', 'User', null, null);
        userField.setDefaultValue(loggedInUser);
        userField.setDisplayType("hidden");

        form.addSubmitButton("Submit");
        response.writePage(form);
    }
    else
    {
        try {
            Log.d("FORM SUBMITTED", "POST CHECK 1");
            //EC.ProcessRequest(request);
            InputData.Date = request.getParameter('date');
            Log.d("ProcessRequest", "PARAMETER - Date:  " + InputData.Date);
            InputData.Location = request.getParameter('location');
            Log.d("ProcessRequest", "PARAMETER - Location:  " + InputData.Location);
            InputData.Department = request.getParameter('department');
            Log.d("ProcessRequest", "PARAMETER - Department:  " + InputData.Department);
            InputData.Account = request.getParameter('account');
            Log.d("ProcessRequest", "PARAMETER - Account:  " + InputData.Account);
            InputData.User = request.getParameter('user');
            Log.d("ProcessRequest", "PARAMETER - User:  " + InputData.User);
            InputData.Memo = request.getParameter('memo');
            Log.d("ProcessRequest", "PARAMETER - Memo:  " + InputData.Memo);

            Log.d("FORM SUBMITTED", "POST CHECK 2");


            var file = request.getFile("file");
            Log.d("FORM SUBMITTED", "File Retrieved");

            // Validate the uploaded File
            if (!file) throw nlapiCreateError("NO_FILE_UPLOADED", "You must upload a file.");
            Log.d("FORM SUBMITTED", "File Name:  " + file.getName());
            if (file.getName()) {
                var fileNameParts = file.getName().split(".");
                var extension = fileNameParts[1];
                Log.d("FORM SUBMITTED", "extension:  " + extension);
                extension = extension.toLowerCase();
                if (extension != "csv") throw nlapiCreateError("INVALID_FILE_TYPE", "Only csv files are permitted for upload.");
            }

            // Save this file to the File Cabinet
            file.setName(file.getName());
            nlapiLogExecution('DEBUG', 'InvAdjustmentUploader', 'New File Name: ' + file.getName());
            file.setFolder(FILES_FOLDER);
            InputData.FileID = nlapiSubmitFile(file);
            nlapiLogExecution('DEBUG', 'InvAdjustmentUploader', 'New File ID: ' + InputData.FileID);

            // Create the Header Staging Record
            var parentRecord = EC.CreateParentStagingRecord();
            Log.d("InvAdjustmentUploader", "New Header Staging Record Created:  " + parentRecord);

            // Attach the file to this new Header Staging Record
            nlapiAttachRecord('file', InputData.FileID, 'customrecord_inv_adj_staging_header', parentRecord);
            Log.d("FORM SUBMITTED", "File Attached to Custom Record:  " + parentRecord);

            // Display upload response to User
            var html = '<br /><br />' +
                '<span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; ' +
                'color: #333333;">';

            var headerURL = HEADER_DETAIL_URL + parentRecord;
            html += 'Your upload is complete.&nbsp;&nbsp;';
            html += '<a href="' + headerURL + '" target="_blank">Click here</a> to view the newly created Header Staging Record.</span><br /><br />';
            html += 'You will not be able to approve this record until the Detail Staging records are created for it.  ';
            html += 'The scheduled script that creates the Detail Staging records will be triggered automatically now.  ';
            html += 'Once the Detail Staging records are created, the Header Staging record status will change to Pending Approval.  ';
            html += '<br /><br />';
        }
        catch(e)
        {
            // Display upload response to User
            var html = '<br /><br />' +
                '<span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; ' +
                'color: #333333;">';
            html += 'An unexpected error occurred during upload.  Error details are below and will be emailed to you.  Contact Support if you continue to have issues submitting files through this portlet.</span><br /><br />';
            html += 'Error Details:<br />';
            html += 'CODE:  ' + e.getCode() + '<br />';
            html += 'DETAILS:  ' + e.getDetails();
            html += '<br /><br />';

            var user = request.getParameter('user');
            if ( user ) {
                nlapiSendEmail(SendErrorEmailsFrom, user, "Kit & Ace -- EC_Suitelet_InvAdj_Submit", html);
                var addedHTML = html + "User:  " + user + '<br />';
                addedHTML += "Environment:  " + nlapiGetContext().getEnvironment();
                nlapiSendEmail(SendErrorEmailsFrom, recipient, "Kit & Ace -- EC_Suitelet_InvAdj_Submit", addedHTML);
            }
        }

        // Trigger the scheduled script to create Detail Staging Records
        Log.d('Scheduling Script to create detail staging records....');
        var result = nlapiScheduleScript('customscript_ec_sch_create_detail_recs', null, null);                  // 20 units
        Log.d('Scheduling Script RESULT:  ' + result);
        if (result != 'QUEUED') {
            Log.d('scheduled script not queued',
                'expected QUEUED but scheduled script api returned ' + result);
            //var emailBody5 = "Script: EC_Suitelet_InvAdj_Submit.js\nFunction: InvAdjustmentUploader\nError: There was an issue re-scheduling the scheduled script in this script";
            EC.SendEmailAlert("Error: There was an issue scheduling the script to create the detail staging records.  Header Record:  " + parentRecord, ScriptName);
        }
        else {
            Log.d('InvAdjustmentUploader -- Re-Scheduling Script Status', "Result:  " + result);
        }

        html += 'Please refresh this portlet to submit another file.';
        var fhform = nlapiCreateForm('Thank you', 'false');
        var field = fhform.addField('thankyou','inlinehtml', 'thankyou');
        field.setDefaultValue(html);
        response.writePage(fhform);
    }
}

EC.CreateParentStagingRecord = function(){
    var parentRec = nsdal.createObject("customrecord_inv_adj_staging_header", HeaderStagingRecordProperties);
    parentRec.custrecord_header_adjustment_date = InputData.Date;
    parentRec.custrecord_header_adjustment_location = InputData.Location;
    parentRec.custrecord_header_adjustment_account = InputData.Account;
    parentRec.custrecord_header_adjustment_dept = InputData.Department;
    parentRec.custrecord_header_importer = InputData.User;
    parentRec.custrecord_header_adjustment_memo = InputData.Memo;
    return parentRec.save(true, true);
};

Log.AutoLogMethodEntryExit(null, true, true, true);