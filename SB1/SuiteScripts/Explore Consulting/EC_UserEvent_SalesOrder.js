/**
 * Company            Explore Consulting
 * Copyright          2014 Explore Consulting, LLC
 * Type               NetSuite EC_UserEvent_SalesOrder.js
 * Version            1.0.0
 * Version 						1.0.1 - only run when the externalid Hybris order is empty
 **/

function so_onAfterSubmit(type) {

	if (!nlapiGetFieldValue('custbody_wtka_extsys_order_number')){
	// If this is a new record, proceed

	var userId = nlapiGetContext().getUser();

	if(userId == 298465){


		nlapiLogExecution('DEBUG', 'user: ', 'ivan');

		try {


			// If this record was not created from the UI context, return
			/*if(nlapiGetContext().getExecutionContext() != 'userinterface') {

				return;
			}*/

			var soId
			,	emailMerger
			,	mergeResult
			,	templateId
			,	emailBody
			,	emailSender
			,	emailSubject
			,	emailRecipient;

			if(nlapiGetContext().getEnvironment() == 'SANDBOX') {

				templateId =  '9';	 //'12';
			} else {

				templateId = '9';	// TODO: Change on migration
			}



			emailSender = 1549;
			soId = nlapiGetRecordId();
			emailSubject = 'Order Receipt – '+nlapiLookupField('salesorder',soId,'tranid');
			emailRecipient = nlapiGetFieldValue('entity');
			emailMerger = nlapiCreateEmailMerger(templateId);
			emailMerger.setTransaction(soId);
			mergeResult = emailMerger.merge();
			emailBody = mergeResult.getBody();

//			testBody = testBody.splice(200, 500);
//			nlapiLogExecution('DEBUG', 'Merge result', testBody);

			try {

				nlapiSendEmail(emailSender, emailRecipient, emailSubject, emailBody, null, null, {transaction: soId},null,true);
			} catch(e) {

				nlapiLogExecution('ERROR', 'Error sending Sales Order notification email', e);
			}

		} catch(e) {

			nlapiLogExecution('ERROR', 'Unexpected Error', e);
			nlapiSendEmail(6, 'client-login@exploreconsulting.com', 'Unexpected Error sending SO notification email', e);
		}
	}

	if(type == 'create') {

		try {


			// If this record was not created from the UI context, return
			/*if(nlapiGetContext().getExecutionContext() != 'userinterface') {

				return;
			}*/

			var soId
			,	emailMerger
			,	mergeResult
			,	templateId
			,	emailBody
			,	emailSender
			,	emailSubject
			,	emailRecipient;

			if(nlapiGetContext().getEnvironment() == 'SANDBOX') {

				templateId = '9'; //12
			} else {

				templateId = '9';	// TODO: Change on migration
			}



			emailSender = 1549;

			soId = nlapiGetRecordId();
			emailSubject = 'Order Receipt – '+nlapiLookupField('salesorder',soId,'tranid');

			emailRecipient = nlapiGetFieldValue('entity');
			emailMerger = nlapiCreateEmailMerger(templateId);
			emailMerger.setTransaction(soId);
			mergeResult = emailMerger.merge();
			emailBody = mergeResult.getBody();

//			testBody = testBody.splice(200, 500);
//			nlapiLogExecution('DEBUG', 'Merge result', testBody);

			try {

				nlapiSendEmail(emailSender, emailRecipient, emailSubject, emailBody, null, null, {transaction: soId},null,true);
				nlapiLogExecution('DEBUG', 'Error sending Sales Order notification email', emailRecipient+" "+nlapiGetFieldValue('email'));
			} catch(e) {

				nlapiLogExecution('ERROR', 'Error sending Sales Order notification email', e);
			}

		} catch(e) {

			nlapiLogExecution('ERROR', 'Unexpected Error', e);
			nlapiSendEmail(6, 'client-login@exploreconsulting.com', 'Unexpected Error sending SO notification email', e);
		}
	}
}
}
