/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_Suitelet_TransferOrderImport_Submit
 * Version            1.0.0.0
 **/

var InputData = {
    FileID:undefined,
    User:undefined
};

if ( nlapiGetContext().getEnvironment() == "PRODUCTION" ) {
    var HEADER_DETAIL_URL = "https://system.na1.netsuite.com/app/common/custom/custrecordentry.nl?rectype=215&id=";
    var FILES_FOLDER = 113305;
} else {
    var HEADER_DETAIL_URL = "https://system.sandbox.netsuite.com/app/common/custom/custrecordentry.nl?rectype=215&id="; //168&id=";
    var FILES_FOLDER = 113305; //57676;
}

/**
 *
 * @param request
 * @param response
 * @constructor
 */
function TransferOrderImporter(request, response)
{
    // On GET display the form to the user
    if (request.getMethod() == 'GET')
    {
        var form = nlapiCreateForm('Transfer Order Input', false);

        var fileField = form.addField('file', 'file', 'Import File', null, null);
        fileField.setMandatory(true);

        // Set a custom User field so we can track who imported this file
        var loggedInUser = request.getParameter('user');
        Log.d("FORM GET - Show Form", "loggedInUser:  " + loggedInUser);
        var userField = form.addField('user', 'text', 'User', null, null);
        userField.setDefaultValue(loggedInUser);
        userField.setDisplayType("hidden");

        form.addSubmitButton("Process Transfer Orders");
        response.writePage(form);
    }
    else
    {
        try {
            Log.d("TransferOrderImporter - FORM SUBMITTED", "POST CHECK 1");
            InputData.User = request.getParameter('user');
            Log.d("TransferOrderImporter", "PARAMETER - User:  " + InputData.User);

            var file = request.getFile("file");
            Log.d("TransferOrderImporter - FORM SUBMITTED", "File Retrieved");

            // Validate the uploaded File
            if (!file) throw nlapiCreateError("NO_FILE_UPLOADED", "You must upload a file.");
            Log.d("TransferOrderImporter - FORM SUBMITTED", "File Name:  " + file.getName());
            if (file.getName()) {
                var fileNameParts = file.getName().split(".");
                var extension = fileNameParts[1];
                Log.d("TransferOrderImporter - FORM SUBMITTED", "extension:  " + extension);
                extension = extension.toLowerCase();
                if (extension != "csv") throw nlapiCreateError("INVALID_FILE_TYPE", "Only csv files are permitted for upload.");
            }

            // Save this file to the File Cabinet
            file.setName(file.getName());
            Log.d('TransferOrderImporter', 'New File Name: ' + file.getName());
            file.setFolder(FILES_FOLDER);
            InputData.FileID = nlapiSubmitFile(file);
            Log.d('TransferOrderImporter', 'New File ID: ' + InputData.FileID);

            // To skip a step in the process, we will parse out the first line of the file.
            var fileObj = nlapiLoadFile(InputData.FileID);
            var fileContents = fileObj.getValue();
            // Parse the file's data
            var row = CSV.parse(fileContents);                  // CSV.js library file required

            // Create the Header Staging Record
            var parentRecord = EC.CreateParentStagingRecord(row[1]);       // row[0] would be the header row, so need to take second row
            Log.d("TransferOrderImporter", "New Header Staging Record Created:  " + parentRecord);

            // Attach the file to this new Header Staging Record
            nlapiAttachRecord('file', InputData.FileID, 'customrecord_tran_order_staging_header', parentRecord);
            Log.d("TransferOrderImporter", "File Attached to Custom Record:  " + parentRecord);

            // Display upload response to User
            var html = '<br /><br />' +
                '<span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; ' +
                'color: #333333;">';

            var headerURL = HEADER_DETAIL_URL + parentRecord;
            html += 'Your upload is complete.&nbsp;&nbsp;';
            html += '<a href="' + headerURL + '" target="_blank">Click here</a> to view the newly created Transfer Order Staging Record.</span><br /><br />';
            html += 'You will not be able to approve this record until the Line Staging records are created for it.  ';
            html += 'The scheduled script that creates the TO Line Staging records will be triggered automatically now.  ';
            html += 'Once the TO Line Staging records are created, the Transfer Order Staging record status will change to Pending Approval.  ';
            html += '<br /><br />';

            // Trigger the scheduled script to create Line Staging Records
            Log.d('Scheduling Script to create line staging records....');
            var params = {
                custscript_to_header_id: parentRecord,
                custscript_file_starting_line: 1
            };
            var result = nlapiScheduleScript('customscript_ec_sch_toimport_createlines', null, params);                  // 20 units
            Log.d('Scheduling Script RESULT:  ' + result);
            if (result != 'QUEUED') {
                Log.d('scheduled script not queued',
                    'expected QUEUED but scheduled script api returned ' + result);
                EC.SendEmailAlert("Error: There was an issue scheduling the script to create the detail staging records.  Header Record:  " + parentRecord, ScriptName);
            }
            else {
                Log.d('TransferOrderImporter -- Re-Scheduling Script Status', "Result:  " + result);
            }

            html += 'Please refresh this portlet to submit another file.';
            var fhform = nlapiCreateForm('Thank you', 'false');
            var field = fhform.addField('thankyou','inlinehtml', 'thankyou');
            field.setDefaultValue(html);
            response.writePage(fhform);
        }
        catch(e)
        {
            // Display upload response to User
            var html = '<br /><br />' +
                '<span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; ' +
                'color: #333333;">';
            html += 'An unexpected error occurred during upload.  Error details are below and will be emailed to you.  Contact Support if you continue to have issues submitting files through this portlet.</span><br /><br />';
            html += 'Error Details:<br />';
            html += 'CODE:  ' + e.getCode() + '<br />';
            html += 'DETAILS:  ' + e.getDetails();
            html += '<br /><br />';

            var user = request.getParameter('user');
            if ( user ) {
                nlapiSendEmail(SendErrorEmailsFrom, user, "Kit & Ace -- EC_Suitelet_TransferOrderImport_Submit", html);
                var addedHTML = html + "User:  " + user + '<br />';
                addedHTML += "Environment:  " + nlapiGetContext().getEnvironment();
                nlapiSendEmail(SendErrorEmailsFrom, recipient, "Kit & Ace -- EC_Suitelet_TransferOrderImport_Submit", addedHTML);
            }
        }
    }
}

EC.CreateParentStagingRecord = function(rowData){

    // Just going to pull out the relevant data to create the Header Staging Record
    Log.d("InitialSetOfHeaderRecord - rowData", rowData);
    Log.d("InitialSetOfHeaderRecord - column 1", "Transfer Reference Number:  " + rowData[0]);
    Log.d("InitialSetOfHeaderRecord - column 2", "Date:  " + rowData[1]);
    Log.d("InitialSetOfHeaderRecord - column 3", "Subsidiary:  " + rowData[2]);
    Log.d("InitialSetOfHeaderRecord - column 4", "From Location:  " + rowData[3]);
    Log.d("InitialSetOfHeaderRecord - column 5", "To Location:  " + rowData[4]);
    Log.d("InitialSetOfHeaderRecord - column 6", "Employee:  " + rowData[5]);
    Log.d("InitialSetOfHeaderRecord - column 7", "Department:  " + rowData[6]);
    Log.d("InitialSetOfHeaderRecord - column 8", "Expected Ship Date:  " + rowData[7]);
    Log.d("InitialSetOfHeaderRecord - column 9", "Expected Receipt Date:  " + rowData[8]);
    Log.d("InitialSetOfHeaderRecord - column 10", "UPC Code:  " + rowData[9]);
    Log.d("InitialSetOfHeaderRecord - column 11", "Transfer Quantity:  " + rowData[10]);

    if ( !rowData[0] ) {
        /*var refNumber = parseInt(rowData[0]);
        Log.d("InitialSetOfHeaderRecord", "refNumber:  " + refNumber);
        Log.d("InitialSetOfHeaderRecord", "type of number:  " + typeof refNumber);
        var refNumber2 = Math.round(refNumber);
        Log.d("InitialSetOfHeaderRecord", "After rounding:  " + refNumber2);
        var refNumber3 = refNumber2.toString();
        Log.d("InitialSetOfHeaderRecord", "Make it a string:  " + refNumber3);
        */
        var refNumber = rowData[0];
    }
    else
    {
        var refNumber = "TBP" + InputData.FileID;
    }

    var parentRec = nsdal.createObject("customrecord_tran_order_staging_header", HeaderStagingRecordProperties);
    if ( refNumber ) parentRec.name = refNumber;
    else parentRec.name = "TBP" + InputData.FileID;
    parentRec.custrecord_tranfer_stage_status = TO_HeaderStagingStatus.CreateLineRecords;
    if ( refNumber ) parentRec.custrecord_transfer_reference_number = refNumber;
    parentRec.custrecord_transfer_stage_date = rowData[1];
    parentRec.custrecord_transfer_employee = InputData.User;
    return parentRec.save(true, true);

};

Log.AutoLogMethodEntryExit(null, true, true, true);
