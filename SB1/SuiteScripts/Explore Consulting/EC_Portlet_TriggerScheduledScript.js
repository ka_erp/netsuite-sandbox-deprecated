/**
 * Company            Explore Consulting
 * Copyright            2014 Explore Consulting, LLC
 * Type                NetSuite EC_Portlet_FireScheduledScripts
 * Version            1.0.0.0
 * Description        Boilerplate startup code for a NetSuite RESTlet.
 **/

function onStart(portlet, column)
{
    portlet.setTitle('Scheduled Scripts Command Center');

    var suiteletURL = nlapiGetContext().getSetting('SCRIPT', 'custscript_ec_fire_sched_suitelet_url');

    portlet.setHtml('<html><div><iframe src="' + suiteletURL + '" scrolling="no" width="100%" height="80px"></iframe></div></html>');
}
