/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_Scheduled_TOImportPortlet_CreateDTOLineRecs
 * Version            1.0.0.0
 * Description          Script that will go through and create the TO Line Staging Records for Header Staging Records
 *                          that are awaiting Processing.
 *
 **/

var InputData = {
    FileID:undefined,           // Will keep track of the current file we are processing
    ParentRecID:undefined,      // Will keep track of the current TO Header Record
    User:undefined
};

var ScriptName = "EC_Scheduled_TOImportPortlet_CreateTOLineRecs";

function onStart() {
    EC.ProcessHeaderStagingRecords();
}


EC.ProcessHeaderStagingRecords = function(){

    // Pull the script parameters and validate them for processing
    var context = nlapiGetContext();
    var recordID = context.getSetting("SCRIPT", "custscript_to_header_id");
    var startingLine = context.getSetting("SCRIPT", "custscript_file_starting_line");
    Log.d('ProcessHeaderStagingRecords', 'Script Parameter Check - Record ID:  ' + recordID);
    Log.d('ProcessHeaderStagingRecords', 'Script Parameter Check - Starting Line To Process:  ' + startingLine);

    try{
        if ( !recordID )
            throw nlapiCreateError("INVALID_SCRIPT_PARAMETER", "No Record ID found in script parameter", false);

        if ( !startingLine || startingLine == 0 )
            startingLine = 1;

        // Get Parent Staging Records for Processing
        var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"), new nlobjSearchFilter("internalid", null, "is", recordID)];
        var columns = [new nlobjSearchColumn("internalid", "file")];
        var results = nlapiSearchRecord("customrecord_tran_order_staging_header", null, filters, columns);
        var timesUp = false;
        var currentID;
        var reprocess = false;

        if ( results ) {
            // Should only be one search result
            Log.d("ProcessHeaderStagingRecords", "Number of Search Results:  " + results.length);
            InputData.ParentRecID = recordID;
            InputData.User = nlapiGetContext().getUser();

            // Pre-load the Item collection before beginning processing
            // Can't preload any collections that we are going to lookup by External ID because
            //      of the limitations of search columns for the External ID data
            EC.PreloadCollections();

            for (var i=0; i < results.length && !timesUp && !reprocess; i++) {

                currentID = results[i].getId();
                Log.d("Processing TO Import Header Staging records - result #" + i, "Current Record ID:  " + currentID);
                reprocess = false;

                try{
                    // Process this Header TO Import Record
                    if ( !results[i].getValue("internalid", "file") ) throw nlapiCreateError("NO_FILE_FOUND", "No File found - Could not Process");
                    InputData.FileID = results[i].getValue("internalid", "file");

                    // Set the Header Record Status to Processing
                    nlapiSubmitField("customrecord_tran_order_staging_header", results[i].getId(), "custrecord_tranfer_stage_status", TO_HeaderStagingStatus.Processing);

                    // Need to Process through the File and create all of the TO Line staging records
                    reprocess = EC.ProcessFile(results[i].getId(), results[i].getValue("internalid", "file"));

                    // If governance limits didn't allow us to complete the processing of the file, we need to rescheduled and start at where we left off
                    if ( reprocess && reprocess.reprocess )
                    {
                        Log.d('Scheduling Script to continue updating for a specific file:  Record ID ' + reprocess.recID + '  Starting Line:  ' + reprocess.startLine);
                        //headerRec.custrecord_transfer_error_message = "Rescheduling script to start at line " + reprocess.startLine;
                        //var id = headerRec.save(true, true);
                        //Log.d("ProcessHeaderStagingRecords", "Header Record Re-saved:  " + id);

                        var params = {
                            custscript_to_header_rec_id: reprocess.recID,
                            custscript_starting_line_num: reprocess.startLine
                        };

                        var result = nlapiScheduleScript('customscript_ec_sch_toimport_partialfile', null, params);                  // 20 units
                        Log.d('Scheduling Script RESULT:  ' + result);
                        if (result != 'QUEUED') {
                            Log.d('scheduled script not queued',
                                'expected QUEUED but scheduled script api returned ' + result);
                            EC.SendEmailAlert("Issue Rescheduling Script for a Partially processed file.\nRecord ID:  " + reprocess.recID + "\nStarting Line:  " + reprocess.startLine, ScriptName);
                        }
                        else {
                            Log.d('ProcessHeaderStagingRecords -- Re-Scheduling Script Status', "Result:  " + result);
                        }
                    }
                    else if ( reprocess.recID )
                    {
                        // Set the Header Record Status to Processing
                        nlapiSubmitField("customrecord_tran_order_staging_header", reprocess.recID, "custrecord_tranfer_stage_status", TO_HeaderStagingStatus.PendingApproval);
                        Log.d("ProcessHeaderStagingRecords", "Header Record Re-saved:  " + reprocess.recID);
                    }
                }
                catch(e)
                {
                    Log.d("ProcessHeaderStagingRecords - " + InputData.ParentRecID, "Error was throwing during processing:  " + e);
                    var fields = ["custrecord_tranfer_stage_status", "custrecord_transfer_error_message"];
                    var values = [TO_HeaderStagingStatus.Error_ToBeReviewed, e];
                    nlapiSubmitField("customrecord_tran_order_staging_header", InputData.ParentRecID, fields, values);
                    EC.SendEmailAlert(e, ScriptName);
                }

                timesUp = EC.RunTimeScriptCheckers(Script_GovernanceThreshold);
            }
        }
        else
        {
            Log.d("ProcessHeaderStagingRecords", "No Header Staging Records are ready for Processing");
            EC.SendEmailAlert("Header Record not found - please review.  Header Record ID:  " + recordID, ScriptName);
        }

        // TODO:  Update function with deployment info
        /*if ( timesUp && !reprocess )
        {
            Log.d('Scheduling Script to continue updating');
            var params = {
                custscript_to_header_id: parentRecord,
                custscript_file_starting_line: 1
            };
            var result = nlapiScheduleScript('customscript_ec_sch_toimport_createlines', null, params);                  // 20 units
            Log.d('Scheduling Script RESULT:  ' + result);
            if (result != 'QUEUED') {
                Log.d('scheduled script not queued',
                    'expected QUEUED but scheduled script api returned ' + result);
                var emailBody5 = "Script: EC_Scheduled_InvAdjPortlet_CreateDetailRecs.js\nFunction: ProcessHeaderStagingRecords\nError: There was an issue re-scheduling the scheduled script in this script";
                EC.SendEmailAlert("Error: There was an issue re-scheduling the scheduled script in this script.", ScriptName);
            }
            else {
                Log.d('ProcessHeaderStagingRecords -- Re-Scheduling Script Status', "Result:  " + result);
            }
        } */
    }
    catch(e)
    {
        Log.d("ProcessHeaderStagingRecords", "Unexpected Error:  " + e);
        if ( recordID ) {
            var fields = ["custrecord_tranfer_stage_status", "custrecord_transfer_error_message"];
            var values = [TO_HeaderStagingStatus.Error_ToBeReviewed, e];
            nlapiSubmitField("customrecord_tran_order_staging_header", recordID, fields, values);
        }
        EC.SendEmailAlert(e, ScriptName);
    }
};

/**
 *
 * @param fileID
 * @param startingLine {integer} - (optional) this indicates the file in the file that we should start processing with.
 * @returns {*}
 * @constructor
 */
EC.ProcessFile = function(headerID, fileID, startingLine){

    if ( startingLine )
        var start = parseInt(startingLine);
    else
        var start = 1;

    var result = {reprocess:false, startLine:start, recID:headerID, headerRecord:undefined};
    try{

        // Open File and process it's contents
        var fileObj = nlapiLoadFile(fileID);
        var fileContents = fileObj.getValue();
        var stopProcessing = false;

        // Parse the file's data
        var rows = CSV.parse(fileContents);                  // CSV.js library file required
        //Log.d("ProcessFile - Parsed Data Object", rows);

        // If this is the first processing of this header record, we need to set the header record properties
        if ( !startingLine && start == 1 ){
            headerRec = nsdal.loadObject("customrecord_tran_order_staging_header", headerID, HeaderStagingRecordProperties);
            EC.InitialSetOfHeaderRecord(headerRec, rows[1]);
            headerRec.save(true, true);
        }

        // Open the TO Header Record for use throughout processing
        headerRec = nsdal.loadObject("customrecord_tran_order_staging_header", headerID, HeaderStagingRecordProperties);
        if ( headerRec && headerRec.custrecord_transfer_employee ) InputData.User = headerRec.custrecord_transfer_employee;

        // For each row of data - create a connected Staging Record custom record
        // Start with Line 1 of the file to skip the Header Row
        for ( var p=parseInt(start); p<rows.length && !stopProcessing; p++ ){
            // Need to ensure governance will allow us to continue with this file
            stopProcessing = EC.RunTimeScriptCheckers(File_GovernanceThreshold);
            if (!stopProcessing)
                headerRec = EC.CreateChildStagingRecord(headerRec, rows[p], p+1);
            else {
                //Reschedule and start with this line number
                return { reprocess:true, startLine:p, recID:headerRec.id };
            }
        }

        result.headerRecord = headerRec;
        result.recID = headerRec.id;
    }
    catch(e)
    {
        Log.d("ProcessFile", "Unexpected Error:  " + e);
        throw e;
    }
    return result;
};

EC.InitialSetOfHeaderRecord = function(headerRec, rowData){

    try{
        Log.d("InitialSetOfHeaderRecord - rowData", rowData);
        Log.d("InitialSetOfHeaderRecord - column 1", "Transfer Reference Number:  " + rowData[0]);
        Log.d("InitialSetOfHeaderRecord - column 2", "Date:  " + rowData[1]);
        Log.d("InitialSetOfHeaderRecord - column 3", "Subsidiary:  " + rowData[2]);
        Log.d("InitialSetOfHeaderRecord - column 4", "From Location:  " + rowData[3]);
        Log.d("InitialSetOfHeaderRecord - column 5", "To Location:  " + rowData[4]);
        Log.d("InitialSetOfHeaderRecord - column 6", "Employee:  " + rowData[5]);
        Log.d("InitialSetOfHeaderRecord - column 7", "Department:  " + rowData[6]);
        Log.d("InitialSetOfHeaderRecord - column 8", "Expected Ship Date:  " + rowData[7]);
        Log.d("InitialSetOfHeaderRecord - column 9", "Expected Receipt Date:  " + rowData[8]);
        Log.d("InitialSetOfHeaderRecord - column 10", "UPC Code:  " + rowData[9]);
        Log.d("InitialSetOfHeaderRecord - column 11", "Transfer Quantity:  " + rowData[10]);
        Log.d("InitialSetOfHeaderRecord - column 12", "Transfer Type:  " + rowData[11]);

        if ( rowData[0] ) {
            //headerRec.name = EC.FormatNumberToString(rowData[0]);
            headerRec.name = rowData[0];
            //headerRec.custrecord_transfer_reference_number = EC.FormatNumberToString(rowData[0]);
            headerRec.custrecord_transfer_reference_number = rowData[0];
        }
        headerRec.custrecord_transfer_stage_date = rowData[1];
        if ( rowData[2] )
            headerRec.custrecord_transfer_subsidiary = rowData[2];       // EC.FindSubsidiary(headerRec.custrecord_header_adjustment_location) ????
        if ( rowData[3] )
            headerRec.custrecord_transfer_from_location = EC.FindLocationByExtID(rowData[3].toString());
        if ( rowData[4] )
            headerRec.custrecord_transfer_to_location = EC.FindLocationByExtID(rowData[4].toString());
        if ( rowData[5] )
            headerRec.custrecord_transfer_employee = EC.FindEmployeeByExtID(rowData[5].toString());
        if ( rowData[6] )
            headerRec.custrecord_transfer_department = EC.FindDepartmentByExtID(rowData[6].toString());
        headerRec.custrecord_transf_stage_expe_ship_date = rowData[7];
        headerRec.custrecord_expected_receipt_date = rowData[8];
    }
    catch(e)
    {
        Log.d("InitialSetOfHeaderRecord", "Unexpected Error while updating header record with file data:  " + e);
        headerRec.custrecord_transfer_error_message = "Error updating Transfer Order Staging Record with data. [" + e + "].";
        EC.SendEmailAlert(e);
        throw e;
    }
};

EC.CreateChildStagingRecord = function(headerRec, rowData, rowLine){

    try{
        // Special formatting for Transfer Reference Number
        //var refNum = EC.FormatNumberToString(rowData[0]);
        var refNum = rowData[0];
        Log.d("New Incoming Row Data", rowData);

        // Check to see if the rowData matches the Header Record data
        Log.d("CreateChildStagingRecord - Comparing Old and New Ref Number", "OLD:  " + headerRec.custrecord_transfer_reference_number + "  NEW:  " + refNum);
        if ( refNum == headerRec.custrecord_transfer_reference_number ) {
            // Same Transfer Reference Number so proceed and connect this child record to the current Header Record
            Log.d("CreateChildStagingRecord", "Same Parent Record Used:  " + headerRec.id);
            EC.MapPortletInputToStagingRecord(headerRec, rowData, rowLine);
            return headerRec;    // Need to return this header record so we can keep building
                                 //     on it until we get through all its lines
        }
        else
        {
            // Since the Transfer Reference Number does not match - need to create new Header Record
            // Since we are done with the current TO Header Record, we need to close it out and create a new one
            headerRec.custrecord_tranfer_stage_status = TO_HeaderStagingStatus.PendingApproval;
            var id = headerRec.save(true, true);
            Log.d("CreateChildStagingRecord", "OLD Header Record Closed -- " + id);

            Log.d("CreateChildStagingRecord", "NEW Parent Record To Be Created:  " + refNum);
            headerRec = nsdal.createObject("customrecord_tran_order_staging_header", HeaderStagingRecordProperties);
            headerRec.name = refNum;
            headerRec.custrecord_tranfer_stage_status = TO_HeaderStagingStatus.Processing;
            headerRec.custrecord_transfer_reference_number = refNum;
            EC.InitialSetOfHeaderRecord(headerRec, rowData);
            InputData.ParentRecID = headerRec.save(true, true);

            // Attach the file to this new Header Staging Record
            nlapiAttachRecord('file', InputData.FileID, 'customrecord_tran_order_staging_header', InputData.ParentRecID);
            Log.d("CreateChildStagingRecord", "File Attached to Custom Record:  " + InputData.ParentRecID);
            headerRec = nsdal.loadObject("customrecord_tran_order_staging_header", InputData.ParentRecID, HeaderStagingRecordProperties);

            EC.MapPortletInputToStagingRecord(headerRec, rowData, rowLine);

            return headerRec;    // Need to return this header record so we can keep building
                                 //     on it until we get through all its lines
        }
    }
    catch(e)
    {
        Log.d("CreateChildStagingRecord", "Unexpected Error while creating parent staging record:  " + e);
        EC.SendEmailAlert(e);
    }
};

EC.MapPortletInputToStagingRecord = function(headerRec, rowData, rowLine){

    try{

        Log.d("MapPortletInputToStagingRecord - rowData", rowData);
        Log.d("MapPortletInputToStagingRecord - column 1", "Transfer Reference Number:  " + rowData[0]);
        Log.d("MapPortletInputToStagingRecord - column 10", "UPC Code:  " + rowData[9]);
        Log.d("MapPortletInputToStagingRecord - column 11", "Transfer Quantity:  " + rowData[10]);
        Log.d("MapPortletInputToStagingRecord - column 12", "Transfer Quantity:  " + rowData[11]);

        var stagingRecord = nsdal.createObject("customrecord_transfer_order_staging_line", LineStagingRecordProperties);
        stagingRecord.custrecord_transfer_stage_line_msg = "";
        stagingRecord.custrecord_transfer_stage_line_status = TO_LineStagingStatus.WaitingApproval;
        stagingRecord.custrecord_transfer_stage_header = headerRec.id;
        if ( rowData[11] ) stagingRecord.custrecord_transfer_type = EC.LookupTransferType(rowData[11]);

        // Need to do a lookup to find the Item ID using the UPC Code
        if ( rowData[9] ){
            //stagingRecord.custrecord_transfer_upc_code = EC.FormatNumberToString(rowData[9]);
            stagingRecord.custrecord_transfer_upc_code = rowData[9];
            var itemIntID = EC.FindItemIDByUPC(rowData[9]);
            if ( itemIntID ){
                stagingRecord.custrecord_transfer_stage_item = itemIntID;
            } else {
                stagingRecord.custrecord_transfer_stage_line_status = TO_LineStagingStatus.ErrorReview;
                stagingRecord.custrecord_transfer_stage_line_msg += "Invalid UPC Code.  ";
            }
        } else {
            stagingRecord.custrecord_transfer_stage_line_status = TO_LineStagingStatus.ErrorReview;
            stagingRecord.custrecord_transfer_stage_line_msg += "No UPC Code found on file line.  ";
        }

        stagingRecord.custrecord_transfer_quantity = rowData[10] || 0;
        if ( !rowData[10] || rowData[10] == 0 ){
            stagingRecord.custrecord_transfer_stage_line_status = TO_LineStagingStatus.ErrorReview;
            stagingRecord.custrecord_transfer_stage_line_msg += "Transfer Quantity not found or is zero.  Please review.  ";
        }

        stagingRecord.custrecord_file_line_number = rowLine;

        var newRecordID = stagingRecord.save(true, true);
        Log.d("MapPortletInputToStagingRecord", "New Staging Record Created:  " + newRecordID);
    }
    catch(e)
    {
        Log.d("MapPortletInputToStagingRecord", "Unexpected Error while creating staging record:  " + e);

        // If the creation of this record fails - let's still create a placeholder record to alert to the error that occurred
        var stagingRecord = nsdal.createObject("customrecord_transfer_order_staging_line", LineStagingRecordProperties);
        stagingRecord.custrecord_transfer_stage_line_status = TO_LineStagingStatus.ErrorReview;
        stagingRecord.custrecord_transfer_stage_header = headerRec.id;
        stagingRecord.custrecord_file_line_number = rowLine;
        stagingRecord.custrecord_transfer_stage_line_msg += e;
        var newRecordID = stagingRecord.save(true, true);
        Log.d("MapPortletInputToStagingRecord - ERROR CREATION", "New Staging Record Created:  " + newRecordID);

        EC.SendEmailAlert(e, ScriptName);
    }
};

EC.FindSubsidiary = function(location){

    var locRecord = nlapiLoadRecord("location", location);
    var sub = locRecord.getFieldValue("subsidiary");
    if ( sub ) return sub;
    else return "";
};




////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

function onStart2() {
    ScriptName += "  -- Partial File";
    EC.ProcessHeaderStagingRecords_PartialFile();
}

EC.ProcessHeaderStagingRecords_PartialFile = function(){

    try{

        // Pull the script parameters and validate them for processing
        var context = nlapiGetContext();
        var recordID = context.getSetting("SCRIPT", "custscript_to_header_rec_id");
        Log.d('ProcessHeaderStagingRecords_PartialFile', 'Script Parameter Check - Record ID:  ' + recordID);
        var startingLine = context.getSetting("SCRIPT", "custscript_starting_line_num");
        Log.d('ProcessHeaderStagingRecords_PartialFile', 'Script Parameter Check - Starting Line #:  ' + startingLine);

        if ( !recordID || !startingLine ) throw nlapiCreateError("INVALID_PARAMETER", "Either Record ID or Starting Line parameter is missing.\nRecord ID:  " + recordID + "\nStarting Line:  " + startingLine);

        // Get Parent Staging Records for Processing
        var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"), new nlobjSearchFilter("internalid", null, "is", recordID)];
        var columns = [new nlobjSearchColumn("internalid", "file")];
        var results = nlapiSearchRecord("customrecord_tran_order_staging_header", null, filters, columns);
        var timesUp = false;
        var currentID;
        var reprocess = false;

        if ( results ) {
            // Should only be one search result
            Log.d("ProcessHeaderStagingRecords_PartialFile", "Number of Search Results:  " + results.length);
            InputData.ParentRecID = recordID;

            // Pre-load the Location, Department, Employee, Subsidiary and Items collection before beginning processing
            EC.PreloadCollections();

            for (var i=0; i < results.length && !timesUp && !reprocess; i++) {

                currentID = results[i].getId();
                Log.d("Processing TO Import Header Staging records - result #" + i, "Current Record ID:  " + currentID);
                reprocess = false;

                try{
                    // Process this Header TO Import Record
                    if ( !results[i].getValue("internalid", "file") ) throw nlapiCreateError("NO_FILE_FOUND", "No File found - Could not Process");
                    InputData.FileID = results[i].getValue("internalid", "file");

                    // Set the Header Record Status to Processing
                    nlapiSubmitField("customrecord_tran_order_staging_header", results[i].getId(), "custrecord_tranfer_stage_status", TO_HeaderStagingStatus.Processing);

                    // Need to Process through the File and create all of the TO Line staging records
                    reprocess = EC.ProcessFile(results[i].getId(), results[i].getValue("internalid", "file"), startingLine);

                    // If governance limits didn't allow us to complete the processing of the file, we need to rescheduled and start at where we left off
                    if ( reprocess && reprocess.reprocess )
                    {
                        Log.d('Scheduling Script to continue updating for a specific file:  Record ID ' + currentID + '  Starting Line:  ' + reprocess.startLine);
                        headerRec.custrecord_transfer_error_message = "Rescheduling script to start at line " + reprocess.startLine;
                        var id = headerRec.save(true, true);
                        Log.d("ProcessHeaderStagingRecords", "Header Record Re-saved:  " + id);

                        var params = {
                            custscript_to_header_rec_id: reprocess.recID,
                            custscript_starting_line_num: reprocess.startLine
                        };

                        var result = nlapiScheduleScript('customscript_ec_sch_toimport_partialfile', null, params);                  // 20 units
                        Log.d('Scheduling Script RESULT:  ' + result);
                        if (result != 'QUEUED') {
                            Log.d('scheduled script not queued',
                                'expected QUEUED but scheduled script api returned ' + result);
                            EC.SendEmailAlert("Issue Rescheduling Script for a Partially processed file.\nRecord ID:  " + reprocess.recID + "\nStarting Line:  " + reprocess.startLine, ScriptName);
                        }
                        else {
                            Log.d('ProcessHeaderStagingRecords -- Re-Scheduling Script Status', "Result:  " + result);
                        }
                    }
                    else if ( reprocess && reprocess.headerRecord )
                    {
                        reprocess.headerRecord.custrecord_tranfer_stage_status = TO_HeaderStagingStatus.PendingApproval;
                        reprocess.headerRecord.custrecord_transfer_error_message = "";
                        var id = reprocess.headerRecord.save(true, true);
                        Log.d("ProcessHeaderStagingRecords", "Header Record Re-saved:  " + id);
                    }
                }
                catch(e)
                {
                    Log.d("ProcessHeaderStagingRecords - " + InputData.ParentRecID, "Error was throwing during processing:  " + e);
                    var fields = ["custrecord_tranfer_stage_status", "custrecord_transfer_error_message"];
                    var values = [TO_HeaderStagingStatus.Error_ToBeReviewed, e];
                    nlapiSubmitField("customrecord_tran_order_staging_header", currentID, fields, values);
                    EC.SendEmailAlert(e, ScriptName);
                }

                timesUp = EC.RunTimeScriptCheckers(Script_GovernanceThreshold);
            }
        }
        else
        {
            Log.d("ProcessHeaderStagingRecords", "No Header Staging Records are ready for Processing");
        }
    }
    catch(e)
    {
        Log.d("ProcessHeaderStagingRecords_PartialFile", "Unexpected Error:  " + e);
        EC.SendEmailAlert(e, ScriptName);
    }
};


////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////



Log.AutoLogMethodEntryExit(null, true, true, true);
