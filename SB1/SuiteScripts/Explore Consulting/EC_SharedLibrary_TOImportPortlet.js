/**
 * Created by Rachel10 on 5/16/2015.
 * File that will hold some global functions to help execute Transfer Order Import Portlet functionality.
 */

var Script_GovernanceThreshold = 200;
var File_GovernanceThreshold = 70;
var startTime = moment();
var maxEndTime = moment(startTime).add(50, "minutes");

var HeaderStagingRecordProperties = ["id", "name", "custrecord_tranfer_stage_status", "custrecord_transfer_reference_number",
    "custrecord_transfer_stage_date", "custrecord_transfer_subsidiary", "custrecord_transfer_from_location",
    "custrecord_transfer_to_location", "custrecord_transfer_employee", "custrecord_transfer_department",
    "custrecord_transfer_order", "custrecord_transfer_error_message", "custrecord_transf_stage_expe_ship_date",
    "custrecord_expected_receipt_date"];

var LineStagingRecordProperties = ["id", "custrecord_transfer_stage_header", "custrecord_transfer_stage_item",
    "custrecord_transfer_upc_code", "custrecord_transfer_quantity", "custrecord_transfer_stage_line_status",
    "custrecord_transfer_stage_line_msg", "custrecord_file_line_number", "custrecord_transfer_type"];

var TransferOrderProperties = ["id", "trandate", "location", "transferlocation", "department",
                                "employee", "subsidiary", "memo", "custbody_ka_order_type", "custbody_ka_to_expected_ship_date", "custbody_ka_to_expected_recv_date", "externalid", "orderstatus"];

var TransferOrderLineProperties = ["item", "quantity", "expectedreceiptdate", "expectedshipdate"];

var TO_HeaderStagingStatus = {
    CreateLineRecords: 2,       // To Be Processed
    Processing: 3,
    PendingApproval: 1,         // Awaiting Approval
    Error_ToBeReviewed: 4,
    Error_Reprocess: 5,
    Error_Cancel: 6,
    Completed: 7,
    Approved: 8,
    CreatingTransfers: 9
};

var TO_LineStagingStatus = {
    WaitingApproval: 6,
    ToBeProcessed: 1,
    InProcess: 7,
    ErrorReprocess: 5,
    ErrorReview: 3,
    ErrorCancel: 4,
    Completed: 2
};

var ItemCollection = undefined;
var TransferTypeCollection = undefined;


EC.randomNumber = function(){
    return Math.floor((Math.random() * 300) + 1);
};


EC.RunTimeScriptCheckers = function(govLimit){

    return nlapiGetContext().getRemainingUsage() < govLimit || moment().isAfter(maxEndTime);
};


var recipient = 'kasupport@exploreconsulting.com';

if ( nlapiGetContext().getEnvironment() == "PRODUCTION" )
    var SendErrorEmailsFrom = 125247;
else
    var SendErrorEmailsFrom = 65646;

EC.SendEmailAlert = function(errorDets, scriptName){
    nlapiSendEmail(SendErrorEmailsFrom, recipient, "Error in " + scriptName,
        "Environment:  " + nlapiGetContext().getEnvironment() + "\nError Details:  " + errorDets);
};

// Queries a record and returns a collection of the columns defined
function runSearch(recordType, columns, filters)
{
    var searchResults = nlapiCreateSearch(recordType, filters, columns);
    var returnSet = [];
    var results = searchResults.runSearch();
    if (results) {
        for(var t = 0; t <= 10000; t+=1000) {
            var resultSet = results.getResults(t, t+1000);
            if(!resultSet){break;}
            resultSet.forEach(function(element) { returnSet.push(element); });
        }
    }
    return returnSet;
}

EC.PreloadCollections = function(){

    //var filters = [new nlobjSearchFilter("isinactive", null, "is", "F")];
    //var columns = [new nlobjSearchColumn("internalid"), new nlobjSearchColumn("externalidstring")];
    //SubsidiaryCollection = runSearch("subsidiary", null, filters, columns);
    //LocationCollection = runSearch("location", null, filters, columns);
    //EmployeeCollection = runSearch("employee", null, filters, columns);
    //DepartmentCollection = runSearch("department", null, filters, columns);
    //var filters2 = [new nlobjSearchFilter("isinactive", null, "is", "F")];
    //var columns2 = [new nlobjSearchColumn("internalid"), new nlobjSearchColumn("upccode")];
    //ItemCollection = runSearch("inventoryitem", null, filters2, columns2);

    TransferTypeCollection = runSearch("customrecord_ka_order_type_list",
        [new nlobjSearchColumn("internalid"), new nlobjSearchColumn("name")],
        new nlobjSearchFilter("isinactive", null, "is", "F"));
    Log.d("TransferTypeCollection", TransferTypeCollection);
};

EC.FindLocationByExtID = function(locExtID){
    if ( !locExtID ) return "";

    var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"), new nlobjSearchFilter("externalidstring", null, "is", locExtID)];
    var columns = [new nlobjSearchColumn("internalid")];
    var locFound = runSearch("location", null, filters, columns);
    Log.d("CHECK LOCATION", locFound.length);

    if ( locFound && locFound.length > 0 )
        return locFound[0].getId();
    else
        return "";
};

EC.FindSubsidiaryByID = function(subID){
    if ( !subID ) return "";

    var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"), new nlobjSearchFilter("internalid", null, "is", subID)];
    var columns = [new nlobjSearchColumn("internalid")];
    var subFound = runSearch("subsidiary", null, filters, columns);
    Log.d("CHECK SUBSIDIARY", subFound.length);

    if ( subFound )
        return subFound[0].getId();
    else
        return "";
};

/**
 * DEPRECATED
 * @param subExtID
 * @returns {*}
 * @constructor
 */
EC.FindSubsidiaryByExtID = function(subExtID){
    if ( !subExtID ) return "";

    var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"), new nlobjSearchFilter("externalidstring", null, "is", subExtID)];
    var columns = [new nlobjSearchColumn("internalid")];
    var subFound = runSearch("subsidiary", null, filters, columns);
    Log.d("CHECK SUBSIDIARY", subFound.length);

    if ( subFound && subFound.length > 0 )
        return subFound[0].getId();
    else
        return "";
};

EC.FindEmployeeByExtID = function(empExtID){
    if ( !empExtID ) return "";

    var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"), new nlobjSearchFilter("externalidstring", null, "is", empExtID)];
    var columns = [new nlobjSearchColumn("internalid")];
    var empFound = runSearch("employee", null, filters, columns);
    Log.d("CHECK EMPLOYEE", empFound.length);

    if ( empFound && empFound.length > 0 )
        return empFound[0].getId();
    else
        return "";
};

EC.FindDepartmentByExtID = function(deptExtID){
    if ( !deptExtID ) return "";

    var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"), new nlobjSearchFilter("externalidstring", null, "is", deptExtID)];
    var columns = [new nlobjSearchColumn("internalid")];
    var deptFound = runSearch("department", null, filters, columns);
    Log.d("CHECK DEPARTMENT", deptFound.length);

    if ( deptFound && deptFound.length > 0 )
        return deptFound[0].getId();
    else
        return "";
};

EC.FindItemIDByUPC = function(upc){

    var itemid = undefined;
    try{
        if ( !upc ) return undefined;
        else upc = parseFloat(upc).toFixed(0);

        var results = nlapiSearchRecord("item", null, new nlobjSearchFilter("upccode", null, "is", upc));
        if ( results ) {
            itemid = results[0].getId();
            Log.d("FindItemIDByUPC", "ITEM ID FOUND:  " + itemid);
        }
        else Log.d("FindItemIDByUPC", "No search results found");
    }
    catch(e){
        Log.d("FindItemIDByUPC", "Unexpected Error while searching for UPC Code:  " + e);
    }
    return itemid;
};

EC.LookupTransferType = function(typename){
    try{
        var ttype = _.find(TransferTypeCollection, function(tt) { return tt.getValue("name") == typename; });
        if ( ttype )
            return ttype.getValue("internalid");
        else
            return undefined;
    }
    catch(e){
        Log.d("LookupTransferType", "Error Occurred during Lookup of Transfer Type:  " + e);
    }
};


EC.FormatNumberToString = function(num){
    var formattedNumber = num;
    if ( num ) {
        var refNumber = parseInt(num);
        Log.d("FormatNumberToString", "refNumber:  " + refNumber);
        var refNumber2 = Math.round(refNumber);
        Log.d("FormatNumberToString", "After rounding:  " + refNumber2);
        var refNumber3 = refNumber2.toString();
        Log.d("FormatNumberToString", "Make it a string:  " + refNumber3);
        formattedNumber = refNumber3;
    }
    return formattedNumber;
};
