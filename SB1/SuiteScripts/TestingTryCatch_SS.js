/**
 *	File Name		:	TestingTryCatch_SS.js
 *	Function		:	
 *	Prepared by		:	christopher.neal@kitandace.com, 
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{
	var postingMessage = 'not within the date range of your accounting period';
}


function TestingTryCatch(type){

	var recId;
	var oldDate = nlapiDateToString(new Date("February 12, 2016"), 'date');
	var subsidiary = 4;
	var testRec = nlapiCreateRecord('cashsale');
	testRec.setFieldValue('entity', 554828); 
	testRec.setFieldValue('location', 4);
	testRec.setFieldValue('trandate', oldDate);

	testRec.selectNewLineItem('item');
	testRec.setCurrentLineItemValue('item', 'item', 15450);
	testRec.setCurrentLineItemValue('item', 'quantity', 1);
	testRec.commitLineItem('item');

	try{
		nlapiLogExecution('DEBUG', 'TEST', '1) Trying to submit with oldDate: ' + oldDate); //prints: 1) Trying to submit with oldDate: 2/12/2016
		recId = nlapiSubmitRecord(testRec);
		nlapiLogExecution('DEBUG', 'TEST', '2) recId: ' + recId);
	} catch (err) {

		nlapiLogExecution('DEBUG', 'TEST', '3) Error caught'); //prints: 3) Error caught
		nlapiLogExecution('DEBUG', 'TEST', err.toString()); //prints: Code: USER_ERROR Details: The transaction date you specified is not within the date range of your accounting period.
		nlapiLogExecution('DEBUG', 'TEST', err.toString().indexOf(postingMessage)); //prints: -1

		if (err.toString().indexOf(postingMessage) > 0){
			var newDate = nlapiDateToString(new Date(), 'date');
			nlapiLogExecution('DEBUG', 'TEST', '4) Trying to submit with newDate: ' + newDate); //prints: 4) Trying to submit with newDate: 7/11/2016
			testRec.setFieldValue('trandate', newDate);
			recId = nlapiSubmitRecord(testRec);
			nlapiLogExecution('DEBUG', 'TEST', '5) recId: ' + recId); //prints: 5) recId: 3466524
		}
	}
	nlapiLogExecution('DEBUG', 'TEST', '6) Done submitting Cash Sale'); //prints: 6) Done submitting Cash Sale

}