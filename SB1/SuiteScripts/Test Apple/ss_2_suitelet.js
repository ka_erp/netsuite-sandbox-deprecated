/**
 *@NApiVersion 2.x
 *@NScriptType Suitelet
 */
define(['N/ui/serverWidget', 'N/render', 'N/file', 'N/log', 'N/record', 'N/runtime'],
    function(serverWidget, render, file, log, record, runtime) {
        function onRequest(context) {
            if(context.request.method == 'GET'){
				
				log.debug("Debug", context.request.parameters);
				log.debug("Debug", runtime.accountId);
				
				if(context.request.parameters.act == 'print'){
					context.response.writeFile(print(context.request.parameters.id), false);
				}
				
				else if(context.request.parameters.act == 'ui'){
					var form = serverWidget.createForm({
						title : 'Test Suitelet 2.0 Form'
					});
					
					var txtfield = form.addField({
						id : 'custpage_txt',
						type : serverWidget.FieldType.TEXT,
						label : 'Text'
					});

					var sublist = form.addSublist({
						id : 'custpage_list',
						type : serverWidget.SublistType.LIST,
						label : 'List of Records'
					});
					
					sublist.addField({
						id : 'custpage_list_checkbox',
						type : serverWidget.FieldType.CHECKBOX,
						label : 'Select'
					});
					
					sublist.addField({
						id : 'custpage_list_id',
						type : serverWidget.FieldType.TEXT,
						label : 'Internal ID'
					});
					sublist.addField({
						id : 'custpage_list_name',
						type : serverWidget.FieldType.TEXT,
						label : 'Name'
					});
					sublist.addField({
						id : 'custpage_list_value',
						type : serverWidget.FieldType.TEXT,
						label : 'Value'
					});
					
					form.addButton({
						id : 'custpage_search',
						label : 'Search',
						functionName : 'searchRecords'
					});
					
					form.addSubmitButton({
						label : 'Print'
					});
					
					sublist.setSublistValue({
						id : 'custpage_list_checkbox',
						line : 0,
						value : 'T'
					});
					sublist.setSublistValue({
						id : 'custpage_list_checkbox',
						line : 1,
						value : 'F'
					});
					
					form.clientScriptFileId = 1549574;
					
					context.response.writePage(form);
				}
				
			} else {
				context.response.writeFile(print(1), false);
			}
        }
		
		function print(recordId){		
			var xmlTemplateFile = file.load({
				id: 1549576
			});
			var renderer = render.create();
			renderer.templateContent = xmlTemplateFile.getContents();
			renderer.addRecord('record', record.load({
				type: 'customrecord366',
				id: parseInt(recordId)
			}));
			var pdffile = renderer.renderAsPdf();
			return pdffile;
		}
				
        return {
            onRequest: onRequest
			
			,print: print
        };
    });