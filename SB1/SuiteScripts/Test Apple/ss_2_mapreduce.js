/**
 *@NApiVersion 2.x
 *@NScriptType MapReduceScript
 */
define(['N/search', 'N/log', 'N/record'],
    function(search, log, record) {
        function getInputData() {			
			return search.create({
				type: 'customrecord366',
				filters: [['isinactive', search.Operator.IS, 'F']],
				columns: ['internalid', 'name', 'custrecord13'],
				title: 'test search'
			});
        }
		
		function map(context) {
            var searchResult = JSON.parse(context.value);
			var recId = searchResult.id;
			var recName = searchResult.values.name;
			var recValue = searchResult.values.custrecord13;
			var obj = {name: recName, value: recValue};
			
			context.write(recId, JSON.stringify(obj));
			
			//log.debug("Debug", 'map | ' + JSON.stringify(searchResult.values));
        }
		
		function reduce(context) {			
			log.debug("Debug", 'reduce | ' + context.key + ' : ' + JSON.stringify(context.values));
        }
		
		function summarize(summary) {

			var rec = record.create({
						type: 'customrecord366',
					});
					
			rec.setValue({
				fieldId : 'name',
				value: summary.usage
			});
			
			rec.setValue({
				fieldId: 'custrecord13',
				value: summary.seconds
			});
			
			rec.save();
        }
				
        return {
            getInputData: getInputData,
			map: map,
			reduce: reduce,
			summarize: summarize
        };
    });