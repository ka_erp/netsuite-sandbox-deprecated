//Script Configuration Utility
/**
*@NApiVersion 2.x
*@NModuleSc​o​p​e Public
*/
define(['N/log'],
        
function (log) {
    return function(objScriptConfig) {
        this.getScriptConfigValue = function(stName){
			var stValue = null;	
			if(stName){
				if(objScriptConfig[stName.toUpperCase()]){						
					stValue = objScriptConfig[stName.toUpperCase()].value;
				}
			}
			//log.debug('getScriptConfigValue', stName + ' | ' + stValue);				
			return stValue;
        };
    };   
});