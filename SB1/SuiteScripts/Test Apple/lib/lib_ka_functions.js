//Kit And Ace Utility Functions
/**
*@NApiVersion 2.0
*@NModuleSc​o​p​e Public
*/
define([], 

function () {
	return {			
		isEmpty : function(val){
			return (val === undefined || val == null || val.length <= 0);
		},
		
		getAllSearchResults : function(resultSet){
			var completeResults = [];
			
			var resultIndex = 0; 
			var resultStep = 1000; // Number of records returned in one step (maximum is 1000)
			var results; // temporary variable used to store the result set
			
			do{
				// fetch one result range
				results = resultSet.getRange({
					start: resultIndex,
					end: resultIndex + resultStep
				});
				
				 // increase pointer
				 resultIndex = resultIndex + resultStep;
				 
				 //store to complete results
				 completeResults = completeResults.concat(results);
				
			} while (results.length > 0)
				
			return completeResults;				
		}
	};
});



