/**
 *@NApiVersion 2.x
 *@NScriptType MapReduceScript
 */
define(['N/search',
        'N/log',
        'N/record',
		'./lib/lib_script_config',
		'./lib/lib_ka_functions'
        ],
    po_mass_outbound);

function po_mass_outbound (search, log, record, lib_script_config, lib_ka_functions) {
  
    var GLOBAL_AR = [];
		
	function getInputData() {	
	
		var scobj = lib_script_config.createInstance();
		var STATUS_SEND = scobj.getScriptConfigValue('Export to 3PL Status: Send to 3PL');
		var IFPO_SS = 'customsearch_po_outbound_ss';//scobj.getScriptConfigValue('IFPO: Send to 3PL: Scheduler Saved Search');
      
      GLOBAL_AR.push(1);
       GLOBAL_AR.push(2);
      
      //var myFilter = search.createFilter({
      //         name: 'custbody_last_modified_date',
      //         operator: search.Operator.ONORBEFORE,
      //         values: '10/26/2016'
      //   });
	
		return search.load({
			id: IFPO_SS,
          filters: [
            ["custbody_last_modified_date","onorbefore","TO_DATE('10/26/2016', 'MM/DD/YYYY')"]
           ]
		});
	}
	
	function map(context) {
		var searchResult = JSON.parse(context.value);
		var poId = searchResult.id;
		var poName = searchResult.values.tranid;
		
		context.write(poId, poName);
		
		log.debug('Outbound PO', 'map | ' + poId + ' | ' + poName);
      
      GLOBAL_AR.push(poId);
      
      lib_ka_functions.pushGlobal(poId);
	}
	
	function reduce(context) {			
		/*	
		//Update Send To 3PL Status field
		//This will trigger ERP_3PL_Outbound_MassExport_UE
		record.submitFields({
			type: record.Type.PURCHASE_ORDER,
			id: context.key,
			values: {
				custbody_export_3pl_status: STATUS_SEND
			},
			options: {
				enableSourcing: false,
				ignoreMandatoryFields : true
			}
		});*/
		
		log.debug('Outbound PO', 'reduce | ' + context.key + ' : ' + JSON.stringify(context.values));
	}
	
	function summarize(summary) {
		log.debug('Outbound PO', 'summarize | POs sent to 3PL');
      log.debug('GLOBAL_AR', JSON.stringify(GLOBAL_AR));
      //log.debug('getGlobal', JSON.stringify(lib_ka_functions.getGlobal())); -> does not work
      
      log.debug('summary', JSON.stringify(summary));
      log.debug('mapSummary', JSON.stringify(summary.mapSummary));
      
      var mapKeys = [];
	summary.mapSummary.keys.iterator().each(function (key){
		mapKeys.push(key);
		return true;
	});
      
      log.debug('mapKeys', JSON.stringify(mapKeys));

      
		//Send Mail ??
	}
			
	return {
		getInputData: getInputData,
		map: map,
		reduce: reduce,
		summarize: summarize
	};
}