function scheduled_adjustInventory(){

	var arFilters = [new nlobjSearchFilter('formulanumeric', null, 'equalto', 1).setFormula(
		"case when ({upccode} in "
		+ "('20003','20005','20007','20009','20011','20013','20015','20041','20042','20043',"
		+ "'20044','20045','20046','20047','20048','20049','20050','20051','20052','20053',"
		+ "'20054','20055','20056','20057','20058','20059','20060','20088','20089','20090',"
		+ "'20091','20092','20093','20094','20095','20096','20097','20098','20099','20100',"
		+ "'20101','20102','20121','20123','20159','20161','20163','20165','20167','20168',"
		+ "'20169','20170','20171','20172','20178','20179','20180','20181','20182','20183',"
		+ "'20184','20185','20186','20187','20193','20194','20195','20196','20197','20284',"
		+ "'20285','20286','20287','20288','20289','20290','20291','20292','20293','20312',"
		+ "'20315','23072','23074','23076','23078','23080','37164','37165','37166','37167',"
		+ "'37168','39555','39556','39557','39558','83091','83092','83093','83094','83095')) then 1 " 
		+ "else 0 end")];
			
	var arColumns = [new nlobjSearchColumn('internalid'),
					 new nlobjSearchColumn('upccode')]; 
	
	var search = nlapiCreateSearch('inventoryitem', arFilters, arColumns);
	var resultSet = search.runSearch();	
	var arResults = null;
	if(resultSet){
		arResults = getAllSearchResults(resultSet);
	}
	 
	if(arResults){
		//Create Inventory Adjustment - Tigers 2921
		createInvAdjust(arResults, 4, 198);
		
		//Create Inventory Adjustment - K&A 2901
		createInvAdjust(arResults, 4, 50);
		
		//Create Inventory Adjustment - K&A 1901
		createInvAdjust(arResults, 3, 7);
	}
}

function createInvAdjust(arResults, stSubs, stLoc){
	
	var rec = nlapiCreateRecord('inventoryadjustment', {customform: 116});
	rec.setFieldValue('account', 132);	
	rec.setFieldValue('subsidiary', stSubs);
	
	for(var i = 0; i < arResults.length; i++){
		rec.selectNewLineItem('inventory');
		rec.setCurrentLineItemValue('inventory', 'item', arResults[i].getValue('internalid'));
		rec.setCurrentLineItemValue('inventory', 'location', stLoc);
		rec.setCurrentLineItemValue('inventory', 'adjustqtyby', 5000);
		rec.commitLineItem('inventory');
	}
	
	var id = nlapiSubmitRecord(rec, false, true);
	
	nlapiLogExecution('debug', 'createInvAdjust', stLoc + ' | ' + id);
}