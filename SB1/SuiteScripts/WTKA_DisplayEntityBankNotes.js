/**
 *	File Name		:	WTKA_DisplayEntityBankNotes.js
 *	Function		:	Display Entity Bank Details system notes in Vendor record
 * 	Authors			:	Rini Thomas
 *	Company			:	Wipro Limited
 *	Release Dates	:	DD-MMM-2016 (v1.0)
 * 	Current Version	:	1.0
**/

function displayEntityBankNotes(type, form, request) // Before Load function on Vendor record
{
	// displayEntityBankNotes_Classified(type, form, request); // classified based on record type
	// displayEntityBankNotes_Consolidated(type, form, request); // Sorted based on record type & id
	displayEntityBankNotes_Chronological(type, form, request); // Sorted based on time
	
	/**** Replace function call with chosen approach definition here ****/
}

function displayEntityBankNotes_Classified(type, form, request) // Before Load function on Vendor record
{
	try
	{
		form.addTab('custpage_wtka_notes', 'Change Log');
		var vendorList = form.addSubList('custpage_wtka_notes_vendor', 'list', 'Vendor Details', 'custpage_wtka_notes');
		var entityList = form.addSubList('custpage_wtka_notes_entity', 'list', 'Bank Payment Details', 'custpage_wtka_notes');
		var notes  = searchNote('vendor', nlapiGetRecordId());
		if(notes != null && notes.length > 0)
		{
			vendorList.addField('custpage_wtka_notes_date', 		'datetimetz',	'DATE');
			// vendorList.addField('custpage_wtka_notes_date', 		'date',			'DATE');
			// vendorList.addField('custpage_wtka_notes_time', 		'timeofday',	'TIME');
			vendorList.addField('custpage_wtka_notes_name', 		'text', 		'SET BY');
			vendorList.addField('custpage_wtka_notes_context', 		'text', 		'CONTEXT');
			vendorList.addField('custpage_wtka_notes_type', 		'text', 		'TYPE');
			vendorList.addField('custpage_wtka_notes_field', 		'text', 		'FIELD');
			vendorList.addField('custpage_wtka_notes_oldvalue', 	'text', 		'OLD VALUE');
			vendorList.addField('custpage_wtka_notes_newvalue', 	'text', 		'NEW VALUE');
			
			for(var n=0; n<notes.length; n++)
			{
				var createdDate = new Date(notes[n].getValue('date', 'systemnotes'));

				vendorList.setLineItemValue('custpage_wtka_notes_date', 				n+1, nlapiDateToString(createdDate, 'datetimetz'));
				// vendorList.setLineItemValue('custpage_wtka_notes_date', 				n+1, nlapiDateToString(createdDate, 'date'));
				// vendorList.setLineItemValue('custpage_wtka_notes_time', 				n+1, nlapiDateToString(createdDate, 'timeofday'));
				vendorList.setLineItemValue('custpage_wtka_notes_name', 				n+1, notes[n].getText('name', 		'systemnotes'));
				vendorList.setLineItemValue('custpage_wtka_notes_context', 			n+1, notes[n].getValue('context', 	'systemnotes'));
				vendorList.setLineItemValue('custpage_wtka_notes_type', 				n+1, notes[n].getValue('type', 		'systemnotes'));
				vendorList.setLineItemValue('custpage_wtka_notes_field',  				n+1, notes[n].getText('field', 		'systemnotes'));
				vendorList.setLineItemValue('custpage_wtka_notes_oldvalue', 			n+1, notes[n].getValue('oldvalue', 	'systemnotes'));				
				vendorList.setLineItemValue('custpage_wtka_notes_newvalue', 			n+1, notes[n].getValue('newvalue', 	'systemnotes'));				
			}
		}
		
		var notes  = searchNote('customrecord_2663_entity_bank_details', nlapiGetRecordId());
		if(notes != null && notes.length > 0)
		{
			// nlapiLogExecution('debug', 'notes', notes.length);
			
			entityList.addField('custpage_wtka_notes_entity_bank_id', 		'text', 		'RECORD ID'); 
			entityList.addField('custpage_wtka_notes_entity_file_format', 	'text', 		'PAYMENT FILE FORMAT');
			entityList.addField('custpage_wtka_notes_date', 				'datetimetz',	'DATE');
			// entityList.addField('custpage_wtka_notes_date', 				'date',			'DATE');
			// entityList.addField('custpage_wtka_notes_time', 				'timeofday',	'TIME');
			entityList.addField('custpage_wtka_notes_name', 				'text', 		'SET BY');
			entityList.addField('custpage_wtka_notes_context', 				'text', 		'CONTEXT');
			entityList.addField('custpage_wtka_notes_type', 				'text', 		'TYPE');
			entityList.addField('custpage_wtka_notes_field', 				'text', 		'FIELD');
			entityList.addField('custpage_wtka_notes_oldvalue', 			'text', 		'OLD VALUE');
			entityList.addField('custpage_wtka_notes_newvalue', 			'text', 		'NEW VALUE');
			
			for(var n=0; n<notes.length; n++)
			{
				var createdDate = new Date(notes[n].getValue('date', 'systemnotes'));

				entityList.setLineItemValue('custpage_wtka_notes_entity_bank_id',		n+1, notes[n].getValue('record', 	'systemnotes'));
				entityList.setLineItemValue('custpage_wtka_notes_entity_file_format',	n+1, notes[n].getText('custrecord_2663_entity_file_format'));
				entityList.setLineItemValue('custpage_wtka_notes_date', 				n+1, nlapiDateToString(createdDate, 'datetimetz'));
				// entityList.setLineItemValue('custpage_wtka_notes_date', 				n+1, nlapiDateToString(createdDate, 'date'));
				// entityList.setLineItemValue('custpage_wtka_notes_time', 				n+1, nlapiDateToString(createdDate, 'timeofday'));
				entityList.setLineItemValue('custpage_wtka_notes_name', 				n+1, notes[n].getText('name', 		'systemnotes'));
				entityList.setLineItemValue('custpage_wtka_notes_context', 				n+1, notes[n].getValue('context', 	'systemnotes'));
				entityList.setLineItemValue('custpage_wtka_notes_type', 				n+1, notes[n].getValue('type', 		'systemnotes'));
				entityList.setLineItemValue('custpage_wtka_notes_field',  				n+1, notes[n].getText('field', 		'systemnotes'));
				entityList.setLineItemValue('custpage_wtka_notes_oldvalue', 			n+1, notes[n].getValue('oldvalue', 	'systemnotes'));				
				entityList.setLineItemValue('custpage_wtka_notes_newvalue', 			n+1, notes[n].getValue('newvalue', 	'systemnotes'));				
			}
		}
	}
	catch(errors)
	{
		nlapiLogExecution('debug', 'errors', errors);
	}
}

function displayEntityBankNotes_Consolidated(type, form, request) // Before Load function on Vendor record
{
	try
	{
		var count = 0;
		var tab = form.addTab('custpage_wtka_notes', 'Change Log');
		var logList = form.addSubList('custpage_wtka_notes_list', 'list', 'System Notes', 'custpage_wtka_notes');
		
		logList.addField('custpage_wtka_notes_rectype', 			'text',			'RECORD TYPE');
		logList.addField('custpage_wtka_notes_entity_bank_id', 		'text', 		'RECORD'); 
		logList.addField('custpage_wtka_notes_date', 				'datetimetz',	'DATE');
		// logList.addField('custpage_wtka_notes_date', 				'date',			'DATE');
		// logList.addField('custpage_wtka_notes_time', 				'timeofday',	'TIME');
		logList.addField('custpage_wtka_notes_name', 				'text', 		'SET BY');
		logList.addField('custpage_wtka_notes_context', 			'text', 		'CONTEXT');
		logList.addField('custpage_wtka_notes_type', 				'text', 		'TYPE');
		logList.addField('custpage_wtka_notes_field', 				'text', 		'FIELD');
		logList.addField('custpage_wtka_notes_oldvalue', 			'text', 		'OLD VALUE');
		logList.addField('custpage_wtka_notes_newvalue', 			'text', 		'NEW VALUE');
		
		var vendorNotes  = searchNote('vendor', nlapiGetRecordId());
		for(var n=0; vendorNotes != null &&  n<vendorNotes.length; n++)
		{
			var createdDate = new Date(vendorNotes[n].getValue('date', 'systemnotes'));

			logList.setLineItemValue('custpage_wtka_notes_rectype',				n+1, 'Vendor');
			logList.setLineItemValue('custpage_wtka_notes_entity_bank_id',		n+1, vendorNotes[n].getValue('record', 	'systemnotes'));
			logList.setLineItemValue('custpage_wtka_notes_date', 				n+1, nlapiDateToString(createdDate, 'datetimetz'));
			// logList.setLineItemValue('custpage_wtka_notes_date', 				n+1, nlapiDateToString(createdDate, 'date'));
			// logList.setLineItemValue('custpage_wtka_notes_time', 				n+1, nlapiDateToString(createdDate, 'timeofday'));
			logList.setLineItemValue('custpage_wtka_notes_name', 				n+1, vendorNotes[n].getText('name', 		'systemnotes'));
			logList.setLineItemValue('custpage_wtka_notes_context', 			n+1, vendorNotes[n].getValue('context', 	'systemnotes'));
			logList.setLineItemValue('custpage_wtka_notes_type', 				n+1, vendorNotes[n].getValue('type', 		'systemnotes'));
			logList.setLineItemValue('custpage_wtka_notes_field',  				n+1, vendorNotes[n].getText('field', 		'systemnotes'));
			logList.setLineItemValue('custpage_wtka_notes_oldvalue', 			n+1, vendorNotes[n].getValue('oldvalue', 	'systemnotes'));				
			logList.setLineItemValue('custpage_wtka_notes_newvalue', 			n+1, vendorNotes[n].getValue('newvalue', 	'systemnotes'));
			count++;
		}
		
		var entityNotes  = searchNote('customrecord_2663_entity_bank_details', nlapiGetRecordId());
		for(var n=count; entityNotes != null &&  n<(count + entityNotes.length); n++)
		{
			var createdDate = new Date(entityNotes[n].getValue('date', 'systemnotes'));

			logList.setLineItemValue('custpage_wtka_notes_rectype',				n+1, 'Entity Bank Details');
			logList.setLineItemValue('custpage_wtka_notes_entity_bank_id',		n+1, entityNotes[n].getValue('record', 	'systemnotes'));
			logList.setLineItemValue('custpage_wtka_notes_date', 				n+1, nlapiDateToString(createdDate, 'datetimetz'));
			// logList.setLineItemValue('custpage_wtka_notes_date', 				n+1, nlapiDateToString(createdDate, 'date'));
			// logList.setLineItemValue('custpage_wtka_notes_time', 				n+1, nlapiDateToString(createdDate, 'timeofday'));
			logList.setLineItemValue('custpage_wtka_notes_name', 				n+1, entityNotes[n].getText('name', 		'systemnotes'));
			logList.setLineItemValue('custpage_wtka_notes_context', 			n+1, entityNotes[n].getValue('context', 	'systemnotes'));
			logList.setLineItemValue('custpage_wtka_notes_type', 				n+1, entityNotes[n].getValue('type', 		'systemnotes'));
			logList.setLineItemValue('custpage_wtka_notes_field',  				n+1, entityNotes[n].getText('field', 		'systemnotes'));
			logList.setLineItemValue('custpage_wtka_notes_oldvalue', 			n+1, entityNotes[n].getValue('oldvalue', 	'systemnotes'));				
			logList.setLineItemValue('custpage_wtka_notes_newvalue', 			n+1, entityNotes[n].getValue('newvalue', 	'systemnotes'));				
		}
	}
	catch(errors)
	{
		nlapiLogExecution('debug', 'errors', errors);
	}
}

function displayEntityBankNotes_Chronological(type, form, request) // Before Load function on Vendor record
{
	try
	{
		var tab = form.addTab('custpage_wtka_notes', 'Change Log');
		
		var recordId 	 = nlapiGetRecordId();
		var logObjects 	 = new Array();
		var vendorNotes  = searchNote('vendor', recordId);
		for(var v=0; vendorNotes != null && v<vendorNotes.length; v++)
		{
			var newObject 	 	= new Object();
			newObject.rectype 	= "Vendor";
			newObject.bank_id 	= vendorNotes[v].getValue('record', 	'systemnotes');
			newObject.date 	  	= vendorNotes[v].getValue('date', 		'systemnotes');
			newObject.name 	  	= vendorNotes[v].getText('name', 		'systemnotes');
			newObject.context 	= vendorNotes[v].getValue('context', 	'systemnotes');
			newObject.type 	  	= vendorNotes[v].getValue('type', 		'systemnotes');
			newObject.field   	= vendorNotes[v].getText('field', 		'systemnotes');
			newObject.oldvalue	= vendorNotes[v].getValue('oldvalue', 	'systemnotes');		
			newObject.newvalue 	= vendorNotes[v].getValue('newvalue', 	'systemnotes');
			logObjects.push(newObject);
		}
		var entityNotes  = searchNote('customrecord_2663_entity_bank_details', recordId);	
		for(var e=0; entityNotes != null && e<entityNotes.length; e++)
		{
			var newObject 	 	= new Object();
			newObject.rectype 	= "Entity Bank Details";
			newObject.bank_id 	= entityNotes[e].getValue('record', 	'systemnotes');
			newObject.date 	  	= entityNotes[e].getValue('date', 		'systemnotes');
			newObject.name 	  	= entityNotes[e].getText('name', 		'systemnotes');
			newObject.context 	= entityNotes[e].getValue('context', 	'systemnotes');
			newObject.type 	  	= entityNotes[e].getValue('type', 		'systemnotes');
			newObject.field   	= entityNotes[e].getText('field', 		'systemnotes');
			newObject.oldvalue	= entityNotes[e].getValue('oldvalue', 	'systemnotes');		
			newObject.newvalue 	= entityNotes[e].getValue('newvalue', 	'systemnotes');
			logObjects.push(newObject);
		}
		
		if(logObjects!= null && logObjects.length > 0)
		{
			logObjects.sort(function(a,b) { return (new Date(a.date) - new Date(b.date)) } );
		
			var logList = form.addSubList('custpage_wtka_notes_list', 'list', 'System Notes', 'custpage_wtka_notes');
			
			logList.addField('custpage_wtka_notes_rectype', 			'text',			'RECORD TYPE');
			logList.addField('custpage_wtka_notes_entity_bank_id', 		'text', 		'RECORD'); 
			logList.addField('custpage_wtka_notes_date', 				'datetimetz',	'DATE');
			logList.addField('custpage_wtka_notes_name', 				'text', 		'SET BY');
			logList.addField('custpage_wtka_notes_context', 			'text', 		'CONTEXT');
			logList.addField('custpage_wtka_notes_type', 				'text', 		'TYPE');
			logList.addField('custpage_wtka_notes_field', 				'text', 		'FIELD');
			logList.addField('custpage_wtka_notes_oldvalue', 			'text', 		'OLD VALUE');
			logList.addField('custpage_wtka_notes_newvalue', 			'text', 		'NEW VALUE');
			
			for(var l=0; logObjects!= null && l<logObjects.length; l++)
			{
				var createdDate = new Date(logObjects[l].date);
				createdDate     = nlapiDateToString(createdDate, 'datetimetz');
				logList.setLineItemValue('custpage_wtka_notes_rectype',				l+1, logObjects[l].rectype);
				logList.setLineItemValue('custpage_wtka_notes_entity_bank_id',		l+1, logObjects[l].bank_id);
				logList.setLineItemValue('custpage_wtka_notes_date', 				l+1, createdDate);
				logList.setLineItemValue('custpage_wtka_notes_name', 				l+1, logObjects[l].name);
				logList.setLineItemValue('custpage_wtka_notes_context', 			l+1, logObjects[l].context);
				logList.setLineItemValue('custpage_wtka_notes_type', 				l+1, logObjects[l].type);
				logList.setLineItemValue('custpage_wtka_notes_field',  				l+1, logObjects[l].field);
				logList.setLineItemValue('custpage_wtka_notes_oldvalue', 			l+1, logObjects[l].oldvalue);	
				logList.setLineItemValue('custpage_wtka_notes_newvalue', 			l+1, logObjects[l].newvalue);
			}
		}
	}
	catch(errors)
	{
		nlapiLogExecution('debug', 'errors', errors);
	}
}

function searchNote(type, parentVendor)
{
	var filters = new Array();
	filters.push(new nlobjSearchFilter('context', 'systemnotes', 'noneof', '@NONE@'));
	if(type != 'customrecord_2663_entity_bank_details')	filters.push(new nlobjSearchFilter('internalid', null, 'anyof', parentVendor));
	var columns = new Array();
	if(type == 'customrecord_2663_entity_bank_details')
	{
		columns.push(new nlobjSearchColumn('custrecord_2663_parent_vendor'));
		columns.push(new nlobjSearchColumn('custrecord_2663_entity_file_format'));
	}
	columns.push(new nlobjSearchColumn('context',   'systemnotes'));
	// columns.push(new nlobjSearchColumn('date', 		'systemnotes').setSort(true));
	columns.push(new nlobjSearchColumn('date', 		'systemnotes').setSort());
	columns.push(new nlobjSearchColumn('field', 	'systemnotes'));
	columns.push(new nlobjSearchColumn('newvalue', 	'systemnotes'));
	columns.push(new nlobjSearchColumn('oldvalue', 	'systemnotes'));
	columns.push(new nlobjSearchColumn('record', 	'systemnotes'));
	columns.push(new nlobjSearchColumn('name', 		'systemnotes'));
	columns.push(new nlobjSearchColumn('type', 		'systemnotes'));
	
	var systemNotesSearch = nlapiSearchRecord(type, null, filters, columns);
	var responseArray 	  = new Array();
	if(type == 'customrecord_2663_entity_bank_details')
	{
		for(var i=0; systemNotesSearch != null && i<systemNotesSearch.length; i++)
		{
			if(systemNotesSearch[i].getValue('custrecord_2663_parent_vendor') == parentVendor)
			{
				responseArray.push(systemNotesSearch[i]);
			}
		}
	}
	else
	{
		responseArray = systemNotesSearch;
	}
	return responseArray;
}
