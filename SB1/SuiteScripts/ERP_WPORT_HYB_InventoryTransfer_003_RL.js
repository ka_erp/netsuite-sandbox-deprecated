/**
 *	File Name		:	ERP_WPORT_HYB_InventoryTransfer_003_RL.js
 *	Function		:	Order Process - Step 3 (Inventory Reservation)
 * 	Remarks			:	Relevant scripts isolated from WTKA_NetSuite_ICTO_Process.js
 *	Prepared by		:	A.Villanueva
 *	Release Dates	:	01-Jun-2016
 * 	Current Version	:	1.0
**/

function createInventoryTransfers(dataIn)
{
	nlapiLogExecution('DEBUG', 'Inside Create Inventory Transfer', JSON.stringify(dataIn));
	try
	{
		//1. Pre-Validations Checks
		if(preValidationCheckIT(dataIn))
		{
			finalMessage.orderNumber = dataIn.order.orderHeader.orderId; //get the external Order Number
			nlapiLogExecution('DEBUG', 'FinalInvTrfArray', JSON.stringify(FinalInvTrfArray));
			for(var i=0; i < FinalInvTrfArray.length; i++)
			{
				finalMessage.status = "Success";
				try
				{
					if(FinalInvTrfArray[i].invTrf == 0) // Process only new Reservation requests
					{
						var invTrf  = nlapiCreateRecord('inventorytransfer');
						invTrf.setFieldValue('subsidiary', 							FinalInvTrfArray[i].subsidiary);
						invTrf.setFieldValue('trandate', 							FinalInvTrfArray[i].orderDate);
						invTrf.setFieldValue('memo', 								(FinalInvTrfArray[i].memo != null) ? FinalInvTrfArray[i].memo : '');
						invTrf.setFieldValue('custbody_wtka_extsys_order_number', 	FinalInvTrfArray[i].extNumber);
						invTrf.setFieldValue('custbody_wtka_extsys_hybris_order', 	FinalInvTrfArray[i].ordNumber);
						invTrf.setFieldValue('location', 							FinalInvTrfArray[i].fromLocation);
						invTrf.setFieldValue('transferlocation', 					FinalInvTrfArray[i].toLocation);
						
						/* Set item quantities */
						for(var j=0; j < FinalInvTrfArray[i].items.length; j++)
						{
							invTrf.selectNewLineItem('inventory');
							invTrf.setCurrentLineItemValue('inventory', 'item',  		FinalInvTrfArray[i].items[j].SKUId);
							invTrf.setCurrentLineItemValue('inventory', 'adjustqtyby',  FinalInvTrfArray[i].items[j].orderQty);
							invTrf.commitLineItem('inventory');
						}
						var invTrfRecord = nlapiSubmitRecord(invTrf, true);
						
						var processObject 				= new Object();
						processObject.status 			= "Success";
						processObject.transactionType 	= "Inventory Transfer";
						processObject.transactionId 	= invTrfRecord;
						processObject.transactionNumber = nlapiLookupField('inventorytransfer', invTrfRecord, 'tranid');
						finalMessage.records.push(processObject);
						ediStatus 			 			= 1;
						var logId  = LogCreation(1, 7, invTrfRecord, invTrfRecord, dataIn, ediStatus, finalMessage, 'F'); //7 - Inventory Transfer

						linkTransactionToExternalWTKA(finalMessage.orderNumber, invTrfRecord);
						//createTransactionLog(dataIn, extTransactionId, direction, fromSystem, toSystem, recordType, status, logMessage); //todo
					}
					else
					{
						ErrorObj.status 					= "Duplicate";
						ErrorObj.messages[0]			 	= new Object();
						ErrorObj.messages[0].messagetype 	= "Duplicate request";
						ErrorObj.messages[0].message 	 	= "Request cannot be processed as the Inventory Transfer against Order " + orderShipID[i] + " already exists.";
						
						finalMessage.status = ErrorObj.status;
						finalMessage.records.push(ErrorObj.messages);
					}
				}
				catch(err1)
				{
					ErrorObj.status  	 = "Exception";
					ErrorObj.messages[0] = "Unable to validate Inventory Transfer Request : " + err1;
					finalMessage.status  = ErrorObj.status;
					finalMessage.records.push(ErrorObj);
					nlapiLogExecution('DEBUG', 'createInventoryTransfers Error', JSON.stringify(finalMessage));
					return finalMessage;
				}
			}
			nlapiLogExecution('DEBUG', 'processObject', JSON.stringify(finalMessage));
			return finalMessage;
		}
		else
		{
			nlapiLogExecution('DEBUG', 'Pre-validation failure-ErrorObj', JSON.stringify(ErrorObj));
			finalMessage.status  	 = ErrorObj.status;
			finalMessage.records.push(ErrorObj);
			var subject = 'Inventory Transfer failure';
			var body 	= 'Hello,<br><br>';
			body += 'Order request failed in NetSuite due to below error.<br>';
			body += '<br><b>Error Details: </b><br>' + JSON.stringify(ErrorObj);
			body += '<br><br><b>Data received in NetSuite: </b><br><br>' + JSON.stringify(dataIn) + '<br><br><br>';
			body += '<br><br>Process needs to be resubmitted or handled manually through user interface after corrections.';
			body += '<br><br><br><br><br><br><br>Thanks';
			body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
			nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
			if(sendEmail)	nlapiSendEmail(emailAuthor, toList, subject, body, ccList);
			return finalMessage;
		}
	}
	catch(err)
	{
		nlapiLogExecution('DEBUG', 'In createInventoryTransfers try/catch', err);
		ErrorObj.messages.push(err);
		nlapiLogExecution('DEBUG', 'Pre-validation failure-ErrorObj', JSON.stringify(ErrorObj));
		finalMessage.status  	 = ErrorObj.status;
		finalMessage.records.push(ErrorObj);
		var subject = 'Inventory Transfer failure';
		var body = 'Hello,<br><br>';
		body += 'Order request failed in NetSuite due to below error.<br>';
		body += '<br><b>Error Details: </b><br>' + JSON.stringify(ErrorObj);
		body += '<br><br><b>Data received in NetSuite: </b><br><br>' + JSON.stringify(dataIn) + '<br><br><br>';
		body += '<br><br>Process needs to be resubmitted or handled manually through user interface after corrections.';
		body += '<br><br><br><br><br><br><br>Thanks';
		body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
		nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
		if(sendEmail)	nlapiSendEmail(emailAuthor, toList, subject, body, ccList);
		return finalMessage;
	}
}

function preValidationCheckIT(dataIn)
{
	//Pre-Validation checks on DataIn

	if(dataIn.order.orderHeader.orderStatus == 'Created' || dataIn.order.orderHeader.orderStatus == 'Shipped' || dataIn.order.orderHeader.orderStatus == 'Completed')
	{
		ErrorObj.messages[0] = new Object();
		ErrorObj.messages[0].messagetype = "Invalid Order Status";
		ErrorObj.messages[0].message 	 = "Order cannot be created as the order "  + dataIn.order.orderHeader.orderId + " is in " + dataIn.order.orderHeader.orderStatus + " status and the Inventory Transfer is not complete.";
		return false;
	}	
	
	//Check for duplicates by reading through orderId and shipmentId combination
	var totalCount = 0;
	for(var i in dataIn.order.shipment)
	{
		var ordShipId 	=	dataIn.order.orderHeader.orderId + '_' + dataIn.order.shipment[i].shipmentHeader.shipmentId;
		nlapiLogExecution('DEBUG', 'ordShipId', ordShipId);
		var type 		= 'inventorytransfer';
		var InvTrfId	= FetchOrderId(type, ordShipId, 'ext');
		if(InvTrfId > 0)
		{
			totalCount++;
		}
		else
		{
			nlapiLogExecution('DEBUG', 'Inventory Transfer', 'Inventory Transfer to be processed for ' + ordShipId);
			var FinalUPCCodes = new Array();
			for(var ii in dataIn.order.orderDetail)
			{
				FinalUPCCodes[ii] = dataIn.order.orderDetail[ii].item.SKUId;
			}
			
			/* Validate dates */
			if(!validateISODate(dataIn.order.orderHeader.orderTransactionDt))	return false;
			
			var sellingLocation 	= dataIn.order.orderHeader.sellingLocation.locationCode;
			var fulfillingLocation  = dataIn.order.shipment[i].shipmentHeader.fulfillingLocation.country;
			var locale = (sellingLocation == fulfillingLocation) ? sellingLocation : fulfillingLocation;
			location = (locale == 'CA') ? CA_EcommDC : US_EcommDC;
			
			var subsid  = FetchSubsidiaryId('country', locale);
			if(subsid == -1)
			{
				ErrorObj.messages[0] 				= new Object();
				ErrorObj.messages[0].messagetype    = "Subsidiary error";
				ErrorObj.messages[0].message 	    = "Invalid Query. Error: Subsidiary for Country: " + locale + " cannot be found.";
				nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
				return false;
			}

			/* Lookup Items */
			FinalItemIDs = FetchIdFromName('item', FinalUPCCodes);
			nlapiLogExecution('DEBUG', 'FinalItemIDs', JSON.stringify(FinalItemIDs));
			if(FinalItemIDs[0] == 'Error')	return false

			for(var j in dataIn.order.shipment[i].shipmentDetail)  
			{
				var flag 	  = false;
				var orderDate = '';
				for(var k=0; k < FinalInvTrfArray.length; k++)
				{
					if(FinalInvTrfArray[k].extNumber == ordShipId)
					{
						var lineArray 		  	= new Object();
						lineArray 				= FinalInvTrfArray[k];
						
						var itemDetails 	  	= new Object;
						var lineItemId 			= dataIn.order.shipment[i].shipmentDetail[j].orderDetailId;

						var lineSKUId;
						for(var kk in dataIn.order.orderDetail)
						{
							var flag = false;
							if(dataIn.order.orderDetail[kk].orderLineNumber === lineItemId)
							{
								var lineUPCCode = dataIn.order.orderDetail[kk].item.SKUId; //UPC Codes 
								for(var ii in FinalItemIDs)
								{
									if(FinalItemIDs[ii].UPCCode == lineUPCCode)
									{
										lineSKUId = FinalItemIDs[ii].Id;
										flag 	  = true;
										break;
									}
								}
							}
							if(flag) break;
						} 			
						itemDetails.SKUId 	  	= lineSKUId;
						itemDetails.orderQty 	= dataIn.order.shipment[i].shipmentDetail[j].quantityOrdered;
						lineArray.items.push(itemDetails);
						flag 					= true;
						break;
					}
				}
				
				if(!flag)
				{
					//Object doesn't exist
					var lineArray 			= new Object();
					var dateValue 			= dataIn.order.orderHeader.orderTransactionDt;
					orderDate 				= moment(dateValue).format('l LT'); 	
					orderDate 				= new Date(orderDate);
					orderDate 				= nlapiDateToString(orderDate, 'date');						
					
					lineArray.extNumber 	= dataIn.order.orderHeader.orderId + '_' + dataIn.order.shipment[i].shipmentHeader.shipmentId;
					lineArray.ordNumber 	= String(dataIn.order.orderHeader.orderId);
					lineArray.invTrf		= InvTrfId;
					lineArray.orderDate 	= orderDate; 
					lineArray.memo 			= dataIn.order.orderHeader.orderHeaderMemo;
					lineArray.subsidiary	= subsid.code;
					lineArray.fromLocation	= location;
					lineArray.toLocation	= (locale == 'CA') ? CA_Virtual : US_Virtual;
					lineArray.items 		= new Array();
					var itemDetails 	  	= new Object;
					var lineItemId 			= dataIn.order.shipment[i].shipmentDetail[j].orderDetailId;

					var lineSKUId;
					for(var k in dataIn.order.orderDetail)
					{
						var flag = false;
						if(dataIn.order.orderDetail[k].orderLineNumber === lineItemId)
						{
							var lineUPCCode = dataIn.order.orderDetail[k].item.SKUId; //UPC Codes 
							for(var ii in FinalItemIDs)
							{
								if(FinalItemIDs[ii].UPCCode == lineUPCCode)
								{
									lineSKUId = FinalItemIDs[ii].Id;
									flag 	  = true;
									break;
								}
							}
						}
						if(flag) break;
					} 			
					itemDetails.SKUId 	  	= lineSKUId;
					itemDetails.orderQty 	= dataIn.order.shipment[i].shipmentDetail[j].quantityOrdered;
					lineArray.items.push(itemDetails);
					FinalInvTrfArray.push(lineArray);
				}
			}
		}
		
		if(totalCount == dataIn.order.shipment.length)
		{
			ErrorObj.status 					= "Duplicate";
			ErrorObj.messages[0]			 	= new Object();
			ErrorObj.messages[0].messagetype 	= "Duplicate request";
			ErrorObj.messages[0].message 	 	= "Request cannot be processed as the Inventory Transfer against Order " + orderShipID[i] + " already exists.";
			nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
			return false;
		}
	}
	return true;
}