/**
 *	File Name		:	WTKA_NetSuite_ICTO_Process.js
 *	Function		:	Inventory Reservation, Sales Orders, Drop Shipped Sales Orders(Creation, Fulfillment and Billing)
 * 	Authors			:	Rini Thomas, Vinod Kumar
 *	Company			:	Wipro Limited
 *	Release Dates	:	5-May-2016 (v1.0)
 * 	Current Version	:	1.0
**/

{
	var ErrorObj 		 		= new Object();
	ErrorObj.status				= "Error";
	ErrorObj.messages 	 		= new Array();
	
	var finalResponse			= new Object();
	finalResponse.status		= '';
	finalResponse.records		= new Array();
	
	var finalMessage 		 	= new Object();
	finalMessage.records	 	= new Array();
	
	var OrderObject 			= new Object(),	orderDet			= new Object(),	orderDetails		= new Array(); 
	var FinalInvTrfArray		= new Array();	orderShipID 		= new Array(), 	nsOrderNumber 		= new Array();
	var	RecordUPCCodes 			= new Array(),	FinalShipArray 		= new Array(), 	FinalShipQty 		= new Array();
	var orderDetails	   		= new Array(),	FinalItemIDs		= new Array(),	FinalShipOrderQty   = new Array();
	
	var headers 				= new Array();
	headers['Content-Type']  	= 'application/json';
	headers['Accept']  		 	= 'application/json';
	headers['Authorization'] 	= getESBCredential();
	
	var dataIn, 			createLogFlag, 			TranDate, 	   					record,									finalTransformType;
	var subsidiary, 		shipmethod,    			location, 						extSysPayment,							itemfulfillmentflag;
	var InvTrfURL, 			OrderURL, 				RollbkURL, 						CA_US_Vendor, 							US_CA_Vendor;
	var CA_Virtual, 		US_Virtual, 			US_EcommDC,						CA_EcommDC,								Ext_Payment;
	var Fedex_CA, 			Fedex_US, 				UPS_CA, 						UPS_US,									Taxcode_US;
	var edistatus = 2, 	 	recordId   = 0, 		governanceMinUsage  = 200, 		folderName = 'External System Orders',	customer  = 0;
	var dualTax   = false,	creditCard = false,		rollbackFlag 		= false,	createFulfillmentFlag 	= false, 		createInvoiceFlag   = false;
	var sendEmail			= true, /*false,*/		changed = false,				customerUpdateFail	= 0;
	var ccList = null; //['']; //Enter CC Addresses as array
	var toList = ['3PL_eComm_Integration@kitandace.com'];
	getEnvironmentValues();
}

function postOrderRequest(dataIn) // Entry point for Order RESTLet
{
	nlapiLogExecution('DEBUG', 'dataIn', JSON.stringify(dataIn));
	try
	{	
		if(PrevalidationCheck(dataIn))
		{
			if(createLog(dataIn))
			{
				var finalResp 	  = new Object();
				finalResp.status  = "Success";
				finalResp.message = "Requested accepted and will be processed shortly.";
				nlapiLogExecution('DEBUG', 'finalResponse', JSON.stringify(finalResp));
				finalMessage.records.push(finalResp);
			}
			else
			{
				finalMessage.records.push(ErrorObj);
				nlapiLogExecution('DEBUG', 'orderRequest-else', JSON.stringify(finalMessage));
				var message = '<br><b>Error Details: </b><br>' + JSON.stringify(ErrorObj);
				sendOrderMail('Order', message, dataIn);
			}
			
		}
		else
		{
			edistatus 	  = (ErrorObj.status == "Error") ? 2 : 3; // 2- Error, 3-Exception
			finalMessage.status = ErrorObj.status;
			finalMessage.records.push(ErrorObj);
			nlapiLogExecution('DEBUG', 'Error', JSON.stringify(finalMessage));
			var message = '<br><b>Error Details: </b><br>' + JSON.stringify(ErrorObj);
			sendOrderMail('Order', message, dataIn);
		}
	}
	catch(err)
	{
		var finalResp 	  = new Object();
		finalResp.status  = "Exception";
		finalResp.message = "Unable to Process Orders. Error Details: ";
		finalResp.message = err.getCode() + ':' + err.getDetails();
		finalMessage.records.push(finalResp);
		nlapiLogExecution('DEBUG', 'finalMessage', JSON.stringify(finalMessage));
		var message = '<br><b>Error Details: </b><br>Code: ' + err.getCode() + ' Details: ' + err.getDetails();
		sendOrderMail('Order', message, dataIn);
	}
	return finalMessage;
}

function PrevalidationCheck(dataIn)
{
	edistatus 		= 2;
	ErrorObj.status = "Error";
	try
	{
		//Pre-Validation checks on DataIn
	
		//1. Check if dataIn is empty
		if(!emptyInbound(dataIn))	return false;

		//2. Check if dataIn has header level objects values set
		if(!emptyObject(dataIn.order, 				"Order"))					return false;
		if(!emptyObject(dataIn.order.orderHeader, 	"orderHeader")) 			return false;
		if(!emptyObject(dataIn.order.orderDetail, 	"orderDetail"))				return false;
		if(!emptyObject(dataIn.order.shipment, 		"Shipment"))				return false;
		
		//3.Check if objects have lines
		if(!containLines(dataIn.order.orderDetail, 	"orderDetail present")) 	return false;
		if(!containLines(dataIn.order.shipment, 	"shipment present")) 		return false;
		
		//4.Check if fields have values
		if(!emptyObject(dataIn.order.orderHeader.orderId, 	  											"OrderId"))					return false;
		if(!emptyObject(dataIn.order.orderHeader.orderStatus, 	  										"OrderStatus"))				return false;
		if(!emptyObject(dataIn.order.orderHeader.orderTransactionDt, 	 								"OrderTransactionDt"))		return false;
		
		if(!checkEmptyObject(dataIn.order.orderHeader.billingCustomer, 										"BillingCustomer")) 		return false;
		if(!checkEmptyObject(dataIn.order.orderHeader.billingCustomer.locale, 	 							"Locale")) 					return false;
		if(!checkEmptyObject(dataIn.order.orderHeader.billingCustomer.customerEmail, 	 					"CustomerEmail")) 			return false;
		
		if(!checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress,						"BillingAddress"))	 		return false;
		// if(!checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.billingFirstName,		"BillingFirstName"))		return false;
		// if(!checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.billingLastName,		"BillingLastName"))			return false;
		if(!checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.countryName,			"BillingCountry"))			return false;
		
		var locale  = dataIn.order.orderHeader.billingCustomer.locale.split('/');
		locale 		= locale[locale.length-1].toUpperCase();
				
		if(!checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress,						"ShippingAddress"))	 		return false;
		// if(!checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.recipientFirstName,	"ShippingFirstName"))		return false;
		// if(!checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.recipientLastName,		"ShippingLastName"))		return false;
		if(!checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.countryName,			"ShippingCountry"))			return false;
			
		if(!emptyObject(dataIn.order.orderHeader.paymentInfo, 											"PaymentInfo")) 			return false;
		if(!emptyObject(dataIn.order.orderHeader.paymentInfo.tenderType, 								"TenderType"))	 			return false;
		
		if(!emptyObject(dataIn.order.orderHeader.sellingLocation, 										"SellingLocation")) 		return false;
		if(!emptyObject(dataIn.order.orderHeader.sellingLocation.locationCode, 	 						"SellingLocationCode"))		return false;
		
		//- Check if orderStatus is Created/Released/Shipped/Complete
		if(dataIn.order.orderHeader.orderStatus == 'Created' || dataIn.order.orderHeader.orderStatus == 'Shipped')
		{
			ErrorObj.messages[0] 			 = new Object();
			ErrorObj.messages[0].messagetype = "Invalid Order Status";
			ErrorObj.messages[0].message 	 = "Order cannot be created as the order "  + dataIn.order.orderHeader.orderId + " is in " + dataIn.order.orderHeader.orderStatus + " status.";
			return false;
		}
		
		var FinalUPCCodes = new Array();
		for(var i in dataIn.order.orderDetail)
		{
			if(!emptyObject(dataIn.order.orderDetail[i].orderLineNumber,				"OrderLineNumber"))  	return false;
			if(!emptyObject(dataIn.order.orderDetail[i].countItemOrdered,				"CountItemOrdered"))  	return false;
			if(!emptyObject(dataIn.order.orderDetail[i].item,							"Item"))  				return false;
			if(!emptyObject(dataIn.order.orderDetail[i].item.SKUId,						"SKUId"))  				return false;
			if(!emptyObject(dataIn.order.orderDetail[i].amtItemPrice,					"AmtItemPrice"))  		return false;
			if(!emptyObject(dataIn.order.orderDetail[i].amtItemPrice.amtCurrentPrice,	"AmtCurrentPrice"))		return false;
		}
		
		for(var i in dataIn.order.shipment)
		{
			if(!emptyObject(dataIn.order.shipment[i].shipmentHeader,					"ShipmentHeader"))  			return false;
			if(!emptyObject(dataIn.order.shipment[i].shipmentHeader.shipmentId,			"ShipmentId"))  				return false;
			if(!emptyObject(dataIn.order.shipment[i].shipmentHeader.shipmentCode, 		"ShipmentCode"))	 			return false;
			if(!emptyObject(dataIn.order.shipment[i].shipmentHeader.shippingMethod, 	"ShippingMethod"))	 			return false;
			if(!emptyObject(dataIn.order.shipment[i].shipmentDetail, 					"ShipmentDetail"))	 			return false;
			if(!containLines(dataIn.order.shipment[i].shipmentDetail, 					"shipmentDetails present")) 	return false;
			
			for(var j in dataIn.order.shipment[i].shipmentDetail)
			{
				if(!emptyObject(dataIn.order.shipment[i].shipmentDetail[j].orderDetailId, 		"OrderDetailId"))	 	return false;
				if(!emptyObject(dataIn.order.shipment[i].shipmentDetail[j].quantityOrdered, 	"QuantityOrdered"))	 	return false;
				if(!emptyObject(dataIn.order.shipment[i].shipmentDetail[j].quantityShipped, 	"QuantityShipped"))	 	return false;
			}
		}
		/* Validate dates */
		if(!validateISODate(dataIn.order.orderHeader.orderTransactionDt))	return false;
		
		return true;
	}
	catch(validate_err)
	{
		ErrorObj.status					 = "Exception";
		ErrorObj.messages[0] 			 = new Object();
		ErrorObj.messages[0].messagetype = "Validation Failed";
		ErrorObj.messages[0].message 	 = "Error: " + validate_err;
		var message = '<br><b>Error Details: </b><br>Code: ' + JSON.stringify(ErrorObj);
		sendOrderMail('Order', message, dataIn);
		return false;
	}
}

function orderPrevalidationCheck(dataIn)
{
	edistatus 		= 2;
	ErrorObj.status = "Error";
	try
	{
		if(dataIn.order.orderHeader.orderStatus != 'Completed')
		{
			ErrorObj.messages[0]			 = new Object();
			ErrorObj.messages[0].messagetype = "Order not Completed";
			ErrorObj.messages[0].message 	 = "Order cannot be created as the order "  + dataIn.order.orderHeader.orderId + " is not in Completed status but is in " + dataIn.order.orderHeader.orderStatus + " status.";
			return false;
		}
				
		var zeroShipped = 0;
		for(var i in dataIn.order.shipment)
		{
			/* Validate dates */
			if(!emptyObject(dataIn.order.shipment[i].shipmentHeader.shipDt, 	 				"ShipDt"))	 			return false;
			else if(!validateISODate(dataIn.order.shipment[i].shipmentHeader.shipDt, 			'shipDt'))				return false;
			
			if(dataIn.order.shipment[i].shipmentHeader.shipReceivedDt != null && dataIn.order.shipment[i].shipmentHeader.shipReceivedDt != '')
			{
				if(!validateISODate(dataIn.order.shipment[i].shipmentHeader.shipReceivedDt, 'shipReceivedDt'))	return false;
			}
			if(dataIn.order.shipment[i].shipmentHeader.expectedShipDate != null && dataIn.order.shipment[i].shipmentHeader.expectedShipDate != '')
			{
				if(!validateISODate(dataIn.order.shipment[i].shipmentHeader.expectedShipDate, 'expectedShipDate'))	return false;
			}
			
			/* Check for empty Shipments */ //Create Orders and no Item Fulfillments
			var shippedCount = 0;
			for(var j in dataIn.order.shipment[i].shipmentDetail)
			{
				if(dataIn.order.shipment[i].shipmentDetail[j].quantityShipped > 0)	shippedCount++;
			}
			
			if(shippedCount == 0)	zeroShipped++;
						
			/* Check for PO Vendor */
			var locale  		= dataIn.order.orderHeader.billingCustomer.locale.split('/');
			var sellingLocation = locale[locale.length-1].toUpperCase();
			if(sellingLocation != 'CA')	sellingLocation = 'US';
			var poVendor = 0;
			switch(sellingLocation)
			{
				case 'CA':
					switch(dataIn.order.shipment[i].shipmentHeader.fulfillingLocation.country.toUpperCase())
					{
						case 'US':
							poVendor	 =  CA_US_Vendor; //SW US Inc. - Inventory - Canada (Inventory)
							break;
						case 'CA': //eComm
							poVendor	 =  1;
							break;
					}
					break;
				case 'US':
					switch(dataIn.order.shipment[i].shipmentHeader.fulfillingLocation.country.toUpperCase())
					{
						case 'CA':
							poVendor	 =  US_CA_Vendor; //Kit and Ace Designs Inc. - USA (Inventory)
							break;
						case 'US': //eComm
							poVendor	 =  1;
							break;
					}
					break;
			}
			if(poVendor == 0)
			{
				ErrorObj.messages[0] 		  		= new Object();
				ErrorObj.messages[0].messagetype 	= "PO Vendor Error";
				ErrorObj.messages[0].message 	 	= "Request cannot be processed as the corresponding PO Vendor could not be found";
				return false;
			}
			
			createLogFlag = true; //Reset flag
			
			orderShipID[i] = dataIn.order.orderHeader.orderId + '_' + dataIn.order.shipment[i].shipmentHeader.shipmentId;
		
			var orderID = FetchOrderId('salesorder', orderShipID[i], 'ext', 'tranid', 'nonIcto');
			if(orderID != 0)
			{
				ErrorObj.messages[0] 		  		= new Object();
				ErrorObj.messages[0].messagetype 	= "Duplicate request";
				ErrorObj.messages[0].message 	 	= "Request cannot be processed as the order " + orderShipID[i] + " already exists " + orderID.tranid + ".";
				return false;
			}
		}
		
		if(zeroShipped == dataIn.order.shipment.length)
		{
			ErrorObj.messages[0] 		  		= new Object();
			ErrorObj.messages[0].messagetype 	= "Empty Shipment";
			ErrorObj.messages[0].message 	 	= "Request cannot be processed as the all shipments have zero items shipped.";
			return false;
		}
			
		/* Lookup Customer */
		var locale  = dataIn.order.orderHeader.billingCustomer.locale.split('/');
		var country = locale[locale.length-1].toUpperCase();
		
		/* Lookup Tax Code */
		var country_val = country;
		if(country_val != 'CA')	country_val = 'US';
		var state  	 = dataIn.order.orderHeader.billingCustomer.shippingAddress.provinceName.split('-');
		var province = state[state.length-1].toUpperCase();
		taxCode  	 = FetchTaxCode(country_val, province);
		if(taxCode < 0)
		{
			ErrorObj.messages[0] 		  		= new Object();
			ErrorObj.messages[0].messagetype 	= "Invalid Tax Code";
			ErrorObj.messages[0].message 	 	= "Request cannot be processed as the tax code / group for  " + province + " is not found.";
			return 0;
		}
		
		customer = lookupCustomer(dataIn);
		if(customer == 0)	return false;
		location = (country == 'CA') ? CA_Virtual : US_Virtual;
		
		/* Lookup Items */
		var FinalUPCCodes = new Array();
		for(var i in dataIn.order.orderDetail)
		{
			FinalUPCCodes[i] = dataIn.order.orderDetail[i].item.SKUId;
		}
		FinalItemIDs = FetchIdFromName('item', FinalUPCCodes);
		if(FinalItemIDs[0] == 'Error')	return false
		
		/* Lookup payment method */
		extSysPayment = LookupPayment(dataIn.order.orderHeader.paymentInfo.tenderType, dataIn.order.orderHeader.paymentInfo.currencyCode);
		if(extSysPayment == Ext_Payment)
		{
			ErrorObj.messages[0] 			 = new Object();
			ErrorObj.messages[0].messagetype = "Payment error";
			ErrorObj.messages[0].message 	 = "Invalid query. Error: Payment Method for " + dataIn.order.orderHeader.paymentInfo.tenderType + " (" + dataIn.order.orderHeader.paymentInfo.currencyCode + ")  cannot be found.";
			return false;
		}
		return true;
	}
	catch(validate_err)
	{
		ErrorObj.status					 = "Exception";
		ErrorObj.messages[0] 			 = new Object();
		ErrorObj.messages[0].messagetype = "Validation Failed";
		ErrorObj.messages[0].message 	 = "Error: " + validate_err;
		var message = '<br><b>Error Details: </b><br>Code: ' + JSON.stringify(ErrorObj);
		sendOrderMail('Order', message, dataIn);
		return false;
	}
}

function createLog(dataIn)
{
	try
	{
		var orderStatus = 0, fulfillFlag = false, errorFlag = false;
		var ExtOrderId  = dataIn.order.orderHeader.orderId;
		nlapiLogExecution('DEBUG', 'ExtOrderId', ExtOrderId);
		
		var cols = new Array();
		cols.push(new nlobjSearchColumn('custrecord_wtka_orderid'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_inbound_request'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_request_counter'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_order_status'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_completed_request'));
		
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_wtka_orderid', null, 'haskeywords', ExtOrderId));
		
		var extRecord 	 = 0, requestFileId = 0, requestCounter = 0, fileId = 0, completedStatus;
		var searchRecord = nlapiSearchRecord('customrecord_wtka_external_orders', null, filters, cols);
		if(searchRecord != null)
		{
			var found = false;
			for(var s=0; !found && s<searchRecord.length; s++)
			{
				var orderValue = searchRecord[s].getValue('custrecord_wtka_orderid');
				if(orderValue == ExtOrderId)
				{
					requestFileId   = searchRecord[s].getValue('custrecord_wtka_inbound_request');
					requestCounter  = searchRecord[s].getValue('custrecord_wtka_request_counter');
					orderStatus 	= searchRecord[s].getValue('custrecord_wtka_order_status');
					extRecord 		= nlapiLoadRecord('customrecord_wtka_external_orders', searchRecord[s].getId());
					completedStatus	= searchRecord[s].getValue('custrecord_wtka_completed_request');
					found			= true;
				}
			}
			if(!found)
			{
				extRecord   = nlapiCreateRecord('customrecord_wtka_external_orders');
				orderStatus = 1; //Request Accepted
			}
		}
		else
		{
			extRecord   = nlapiCreateRecord('customrecord_wtka_external_orders');
			orderStatus = 1; //Request Accepted
		}
		
		if(requestFileId == 0)
		{
			var filters 	= new Array();
			filters.push(new nlobjSearchFilter('name', 		null, 'is',		folderName));

			var fileCabinet = nlapiSearchRecord('folder', 	null, filters, 	null);
			var folderId   	= (fileCabinet != null) ? fileCabinet[0].getId() : 0;

			var requestArray 	 = new Object();
			requestArray.records = new Array();
			
			requestArray.records.push(dataIn);
			
			var newFile = nlapiCreateFile(ExtOrderId, 'PLAINTEXT', JSON.stringify(requestArray));
			newFile.setFolder(folderId);
			fileId 		= nlapiSubmitFile(newFile);
		}
		else
		{
			var extFile 	= nlapiLoadFile(folderName + '/' + ExtOrderId);
			var fileContent = JSON.parse(extFile.getValue());
			
			if(dataIn.order.orderHeader.orderStatus == 'Completed')
			{
				if(completedStatus == 'F') //Check for duplicate Order Completed requests
				{
					if(orderStatus == 2) //Inventory Transfer Complete
					{
						if(orderPrevalidationCheck(dataIn))
						{
							extRecord.setFieldValue('custrecord_wtka_customerid', customer);
							orderStatus = 3;
							extRecord.setFieldValue('custrecord_wtka_completed_request', 'T');
						}
						else
						{
							ErrorObj.messagetype 	= "Invalid Order Request";
							nlapiLogExecution('debug', 'Error', JSON.stringify(ErrorObj));
							errorFlag = true;
						}
					}
					else
					{
						ErrorObj.messages[0] 			 	= new Object();
						ErrorObj.messages[0].messagetype 	= "Invalid Request Status";
						ErrorObj.messages[0].message 		= "Request cannot be processed as the current status is " + searchRecord[0].getText('custrecord_wtka_order_status');
						errorFlag = true;
					}
				}
				else
				{
					nlapiLogExecution('DEBUG', 'Duplicate Request');
					ErrorObj.messages[0] 			 	= new Object();
					ErrorObj.messages[0].messagetype 	= "Duplicate request";
					ErrorObj.messages[0].message 		= "Completed Order request already present for order " + dataIn.order.orderHeader.orderId;
					errorFlag = true;
				}
			}
			else
			{
				if(completedStatus == 'T' || orderStatus > 2) // Order Completed
				{
					nlapiLogExecution('DEBUG', 'Invalid Order Request');
					ErrorObj.messages[0] 			 	= new Object();
					ErrorObj.messages[0].messagetype 	= "Invalid Order Request";
					ErrorObj.messages[0].message 		= "Order " + dataIn.order.orderHeader.orderId + " has already been processed.";
					errorFlag = true;
				}
				else	orderStatus = 1;
			}
			fileContent.records.push(dataIn);
			
			var updatedFile = nlapiCreateFile(extFile.getName(), 'PLAINTEXT', JSON.stringify(fileContent));
			updatedFile.setFolder(extFile.getFolder());
			fileId 			= nlapiSubmitFile(updatedFile);
			requestCounter++;
		}
		
		if(!errorFlag)
		{
			extRecord.setFieldValue('custrecord_wtka_order_status', 	orderStatus);
			extRecord.setFieldValue('custrecord_wtka_orderid', 			ExtOrderId);
		}
		
		extRecord.setFieldValue('custrecord_wtka_inbound_request', 	fileId);
		extRecord.setFieldValue('custrecord_wtka_request_counter', 	requestCounter);
		
		var recId = nlapiSubmitRecord(extRecord);
		if(errorFlag)	return false;
		return true;
	}
	catch(log_err)
	{
		ErrorObj.status						= "Exception";
		ErrorObj.messages[0]				= new Object();
		ErrorObj.messages[0].messagetype 	= "Log Creation Error"
		ErrorObj.messages[0].message 		= "Error in log creation. " + log_err
		nlapiLogExecution('DEBUG', 'Error in Log Creation', log_err);
		var subject = 'Order processing failure';
		var message = '<br><b>Error Details: </b><br>Code: ' + JSON.stringify(ErrorObj);
		sendOrderMail('Order', message, dataIn);
		return false;
	}
}

function AfterSubmitICTO(type)
{
	if(type == 'create')
	{
		try
		{
			checkInventoryTransfer(nlapiGetRecordId());
		}
		catch(ue_err)
		{
			nlapiLogExecution('debug', 'checkInventoryTransfer Error', ue_err);
			var message = '<br><b>Error Details: </b><br>' + JSON.stringify(ue_err);
			sendOrderMail('Inventory', message, dataIn);
		}
	}
	else if(type == 'edit' || type =='xedit')
	{
		var rollback 	= nlapiGetFieldValue('custrecord_wtka_rollback');	
		if(rollback == 'T')
		{
			/*try
			{
				// Trigger ROLLBACK RESTlet
				var orderRequest 			= new Object();
				orderRequest.id  			= nlapiGetRecordId();
				var processed_ids			= nlapiGetFieldValue('custrecord_wtka_processed_ids');
				orderRequest.orderDetails  	= (processed_ids != null || processed_ids != '') ? JSON.parse(processed_ids) : processed_ids;
				nlapiLogExecution('debug', 'Rollback - Request', JSON.stringify(orderRequest));
				var restletResponse 		= nlapiRequestURL(RollbkURL, JSON.stringify(orderRequest), headers);
				if(restletResponse.code == 200)
				{
					var respBody  = JSON.parse(restletResponse.body);
					var resp 	  = nlapiGetFieldValue('custrecord_wtka_final_response');
					if(resp != null)	finalResponse = JSON.parse(resp);
					finalResponse.records.push(respBody);
					respBody = JSON.stringify(finalResponse);
					
					setRecordFieldValues(orderRequest.id, null, finalResponse, 'F', 6);
				}
				nlapiLogExecution('debug', 'Rollback RESTLET Call', restletResponse.getBody());
			}
			catch(rollback_err)
			{
				nlapiLogExecution('debug', 'Rollback exception', rollback_err);
				var message = '<br><b>Error Details: </b><br>' + JSON.stringify(rollback_err);
				sendOrderMail('Order', message, dataIn);
			}*/
		}
		else
		{
			decideFlow(nlapiGetRecordId());
		}
	}
}

function decideFlow(recID)
{
	var custRecord 	  = nlapiLoadRecord('customrecord_wtka_external_orders', recID);
	var resp 		  = custRecord.getFieldValue('custrecord_wtka_final_response');
	if(resp != null)	finalResponse = JSON.parse(resp);
	var completedFlag = custRecord.getFieldValue('custrecord_wtka_completed_request');
	
	try
	{
		var status = custRecord.getFieldValue('custrecord_wtka_order_status');
		nlapiLogExecution('debug', 'status', custRecord.getFieldText('custrecord_wtka_order_status'));
		if(status == 1)	checkInventoryTransfer(recID); // Inventory Reservation
		if(status == 3) // Completed Order
		{
			/* Trigger RESTlet */
			var orderRequest = new Object();
			orderRequest.id  = nlapiGetRecordId();
			
			var restletResponse = nlapiRequestURL(OrderURL, JSON.stringify(orderRequest), headers);
			nlapiLogExecution('debug', 'Initial RESTLET Call', restletResponse.getBody());
			if(restletResponse.code == 200)
			{
				var fields 	  = new Array();
				var values    = new Array();
				var respBody  = JSON.parse(restletResponse.body);
				if(respBody.status != "Success")
				{
					setRecordFieldValues(recID, null, null, null, null, null, null, null, false); //Reset completed flag
				}
			}
			else
			{
				var message = '<br><b>Error Details: </b><br>' + JSON.stringify(restletResponse.body);
				sendOrderMail('Order', message, dataIn);
			}
		}
		if(status == 6) //Send Error Email
		{
			var message = '<br><b>Error Details: </b><br>' + JSON.stringify(finalResponse);
			sendOrderMail('Order', message, dataIn);
		}
	}
	catch(err_create)
	{
		if(err_create.code == 'SSS_REQUEST_TIME_EXCEEDED') //re-trigger script
		{
			nlapiLogExecution('debug', 'Retriggering...');
			var orderRequest 	= new Object();
			orderRequest.id  	= nlapiGetRecordId();
			var restletResponse = nlapiRequestURL(OrderURL, JSON.stringify(orderRequest), headers);
			nlapiLogExecution('debug', 'Retriggered RESTLET', restletResponse.getBody());
		}
		else
		{
			var errObject 		  	= new Object();
			errObject.status 	 	= "Exception";
			errObject.message 	 	= "Order cannot be processed. Error:" + err_create;
			finalResponse.status 	= "Error";
			finalResponse.records.push(errObject);
			completedFlag = (completedFlag == 'T') ? false : null;
			setRecordFieldValues(recID, null, finalResponse, null, 6, null, null, null, completedFlag);
			nlapiLogExecution('debug', 'UE-catch', JSON.stringify(errObject));
		}	
	}
}

function checkInventoryTransfer(recID)
{
	//Invoke Inventory Transfers RESTLET for further processing 
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wtka_order_status', 	null, 'anyof', 1));
	filters.push(new nlobjSearchFilter('internalid', 					null, 'anyof', recID));

	var cols = new Array();
	cols.push(new nlobjSearchColumn('custrecord_wtka_orderid'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_inbound_request'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_request_counter'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_inventorytransfers'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_final_response'));
	
	var searchRecord = nlapiSearchRecord('customrecord_wtka_external_orders', null, filters, cols);
	if(searchRecord != null)
	{
		for(var i=0; i < searchRecord.length; i++)
		{
			var recordOrderId = searchRecord[i].getValue('custrecord_wtka_orderid');
			var recordFileId  = searchRecord[i].getValue('custrecord_wtka_inbound_request');
			var recordCounter = searchRecord[i].getValue('custrecord_wtka_request_counter');
			var recordId	  = searchRecord[i].getId();
			var recordResp	  = searchRecord[i].getValue('custrecord_wtka_final_response');
			var recordRequest = fetchRequestFromFile(recordFileId, recordCounter);
			var recordTrnfs	  = searchRecord[i].getValue('custrecord_wtka_inventorytransfers');
			if(recordRequest != 0)
			{
				nlapiLogExecution('DEBUG', 'Initiating Inventory Transfers RESTLET');
				var response = nlapiRequestURL(InvTrfURL, JSON.stringify(recordRequest), headers);
				nlapiLogExecution('DEBUG', 'RESPONSE', response.code);
				if(response.code == 200)
				{
					nlapiLogExecution('DEBUG', 'body', response.body);
					var fields 	  = new Array();
					var values    = new Array();
					var respBody  = JSON.parse(response.body);
					if(respBody.status == "Success")
					{
						var ordStatus 	= 2;
						var recResp 	= new Object();
						nlapiLogExecution('DEBUG', 'recordResp', recordResp);
						if(recordResp != null && recordResp != '')
						{
							recResp 		= JSON.parse(recordResp);
							nlapiLogExecution('DEBUG', 'recResp', JSON.stringify(recResp));
						}
						else
						{
							recResp.records = new Array();
						}
						recResp.status 	= 'Success';
						for(var r in respBody.records)		recResp.records.push(respBody.records[r]);
						
						var resp = JSON.stringify(recResp);
												
						fields.push('custrecord_wtka_final_response');
						fields.push('custrecord_wtka_inventorytransfers');
						fields.push('custrecord_wtka_order_status');
						
						var invTrfVal    = JSON.parse(response.body);
						var invTrfValues = (recordTrnfs != null) ? recordTrnfs : "";
						for(var j in invTrfVal.records)
						{
							if(invTrfValues == "")  	invTrfValues = invTrfVal.records[j].transactionId;
							else 						invTrfValues += ',' + invTrfVal.records[j].transactionId;
						}
						values.push(resp);
						values.push(invTrfValues);
						values.push(ordStatus); //Inventory Transfer Processed
						
						nlapiSubmitField('customrecord_wtka_external_orders', recordId, fields, values, false);	
						
						if(recordTrnfs == null || recordTrnfs == "" || (recordTrnfs.length != invTrfValues.length))
						{
							var ediStat = InvokeCloudhub('salesorder', 0, recordOrderId, 'Released', 'inventoryFlow');
							if(ediStat != 'T')
							{
								/* Log no response from ESB */
								nlapiLogExecution('debug', 'ediStat', ediStat);
								var fields 	  = new Array();
								var values    = new Array();
								var ordStatus 	= 6; //Error
								if (recResp != null && recResp != '')
								{
									var respObj 	= new Object();
									respObj 		= ediStat;
									respObj.status 	= "Error";
									recResp.status 	= "Error";
									recResp.records.push(respObj);
								}
								var resp = JSON.stringify(recResp);
														
								fields.push('custrecord_wtka_final_response');
								fields.push('custrecord_wtka_order_status');
								
								values.push(resp);
								values.push(ordStatus); //Inventory Transfer Processed
								
								nlapiSubmitField('customrecord_wtka_external_orders', recordId, fields, values, false);	
							}
						}
					}
					else
					{
						if(respBody.status == "Duplicate")
						{
							var ordStatus 	= 2; //Reset Status
							fields.push('custrecord_wtka_order_status');
							values.push(ordStatus); //Inventory Transfer Processed
							nlapiLogExecution('debug', 'Duplicate Inventory Transfer Request');
							nlapiSubmitField('customrecord_wtka_external_orders', recordId, fields, values, false);	
						}
						else
						{
							var ordStatus 	= 6; //Error
							var recResp 	= new Object();
							if (recordResp != null && recordResp != '')
							{
								recResp 		= JSON.parse(recordResp);
								recResp.status 	= respBody.status;
								recResp.records.push(respBody);
							}
							else
							{
								recResp = respBody;
							}
							var resp = JSON.stringify(recResp);
													
							fields.push('custrecord_wtka_final_response');
							fields.push('custrecord_wtka_order_status');
							
							values.push(resp);
							values.push(ordStatus); //Inventory Transfer Processed
							
							nlapiSubmitField('customrecord_wtka_external_orders', recordId, fields, values, false);	
						}
					}
				}
				else
				{
					var message = '<br><b>Error Details: </b><br>' + JSON.stringify(response.body);
					sendOrderMail('Inventory', message, dataIn);
					
					/* Log no response from ESB */
					var fields 	  = new Array();
					var values    = new Array();
					var respBody  = JSON.parse(response.body);
					var ordStatus 	= 6; //Error
					var recResp 	= new Object();
					if (recordResp != null && recordResp != '')
					{
						recResp 		= JSON.parse(recordResp);
						recResp.status 	= respBody.status;
						recResp.records.push(respBody);
					}
					else
					{
						recResp = respBody;
					}
					var resp = JSON.stringify(recResp);
											
					fields.push('custrecord_wtka_final_response');
					fields.push('custrecord_wtka_order_status');
					
					values.push(resp);
					values.push(ordStatus); //Inventory Transfer Processed
					
					nlapiSubmitField('customrecord_wtka_external_orders', recordId, fields, values, false);	
				}
			}
		}
	}
	else
	{
		nlapiLogExecution('DEBUG', 'No records to process');
	}
}

function createInventoryTransfers(dataIn)
{
	nlapiLogExecution('DEBUG', 'Inside Create Inventory Transfer', JSON.stringify(dataIn));
	try
	{
		//1. Pre-Validations Checks
		if(preValidationCheckIT(dataIn))
		{
			finalMessage.orderNumber = dataIn.order.orderHeader.orderId;
			nlapiLogExecution('DEBUG', 'FinalInvTrfArray', JSON.stringify(FinalInvTrfArray));
			for(var i=0; i < FinalInvTrfArray.length; i++)
			{
				finalMessage.status = "Success";
				try
				{
					if(FinalInvTrfArray[i].invTrf == 0) // Process only new Reservation requests
					{
						var invTrf  = nlapiCreateRecord('inventorytransfer');
						invTrf.setFieldValue('subsidiary', 							FinalInvTrfArray[i].subsidiary);
						invTrf.setFieldValue('trandate', 							FinalInvTrfArray[i].orderDate);
						invTrf.setFieldValue('memo', 								(FinalInvTrfArray[i].memo != null) ? FinalInvTrfArray[i].memo : '');
						invTrf.setFieldValue('custbody_wtka_extsys_order_number', 	FinalInvTrfArray[i].extNumber);
						invTrf.setFieldValue('custbody_wtka_extsys_hybris_order', 	FinalInvTrfArray[i].ordNumber);
						invTrf.setFieldValue('location', 							FinalInvTrfArray[i].fromLocation);
						invTrf.setFieldValue('transferlocation', 					FinalInvTrfArray[i].toLocation);
						
						/* Set item quantities */
						for(var j=0; j < FinalInvTrfArray[i].items.length; j++)
						{
							invTrf.selectNewLineItem('inventory');
							invTrf.setCurrentLineItemValue('inventory', 'item',  		FinalInvTrfArray[i].items[j].SKUId);
							invTrf.setCurrentLineItemValue('inventory', 'adjustqtyby',  FinalInvTrfArray[i].items[j].orderQty);
							invTrf.commitLineItem('inventory');
						}
						var invTrfRecord = nlapiSubmitRecord(invTrf, true);
						
						var processObject 				= new Object();
						processObject.status 			= "Success";
						processObject.transactionType 	= "Inventory Transfer";
						processObject.transactionId 	= invTrfRecord;
						processObject.transactionNumber = nlapiLookupField('inventorytransfer', invTrfRecord, 'tranid');
						finalMessage.records.push(processObject);
						edistatus 			 			= 1;
						var logId  = LogCreation(1, 7, invTrfRecord, invTrfRecord, dataIn, edistatus, finalMessage, 'F'); //7 - Inventory Transfer
					}
					else
					{
						ErrorObj.status 					= "Duplicate";
						ErrorObj.messages[0]			 	= new Object();
						ErrorObj.messages[0].messagetype 	= "Duplicate request";
						ErrorObj.messages[0].message 	 	= "Request cannot be processed as the Inventory Transfer against Order " + finalMessage.orderNumber + "already exists.";
						
						finalMessage.status = ErrorObj.status;
						finalMessage.records.push(ErrorObj.messages);
					}
				}
				catch(err1)
				{
					ErrorObj.status  	 = "Exception";
					ErrorObj.messages[0] = "Unable to validate Inventory Transfer Request : " + err1;
					finalMessage.status  = ErrorObj.status;
					finalMessage.records.push(ErrorObj);
					nlapiLogExecution('DEBUG', 'createInventoryTransfers Error', JSON.stringify(finalMessage));
					return finalMessage;
				}
			}
			nlapiLogExecution('DEBUG', 'processObject', JSON.stringify(finalMessage));
			return finalMessage;
		}
		else
		{
			nlapiLogExecution('DEBUG', 'Pre-validation failure-ErrorObj', JSON.stringify(ErrorObj));
			finalMessage.status  	 = ErrorObj.status;
			finalMessage.records.push(ErrorObj);
			var message = '<br><b>Error Details: </b><br>' + JSON.stringify(ErrorObj);
			sendOrderMail('Inventory', message, dataIn);
			return finalMessage;
		}
	}
	catch(err)
	{
		nlapiLogExecution('DEBUG', 'In createInventoryTransfers try/catch', err);
		ErrorObj.messages.push(err);
		nlapiLogExecution('DEBUG', 'Pre-validation failure-ErrorObj', JSON.stringify(ErrorObj));
		finalMessage.status  	 = ErrorObj.status;
		finalMessage.records.push(ErrorObj);
		var message = '<br><b>Error Details: </b><br>' + JSON.stringify(ErrorObj);
		sendOrderMail('Inventory', message, dataIn);
		return finalMessage;
	}
}

function preValidationCheckIT(dataIn)
{
	//Pre-Validation checks on DataIn

	if(dataIn.order.orderHeader.orderStatus == 'Created' || dataIn.order.orderHeader.orderStatus == 'Shipped' || dataIn.order.orderHeader.orderStatus == 'Completed')
	{
		ErrorObj.messages[0] = new Object();
		ErrorObj.messages[0].messagetype = "Invalid Order Status";
		ErrorObj.messages[0].message 	 = "Order cannot be created as the order "  + dataIn.order.orderHeader.orderId + " is in " + dataIn.order.orderHeader.orderStatus + " status and the Inventory Transfer is not complete.";
		return false;
	}	
	
	//Check for duplicates by reading through orderId and shipmentId combination
	var totalCount = 0;
	for(var i in dataIn.order.shipment)
	{
		var ordShipId 	=	dataIn.order.orderHeader.orderId + '_' + dataIn.order.shipment[i].shipmentHeader.shipmentId;
		nlapiLogExecution('DEBUG', 'ordShipId', ordShipId);
		var type 		= 'inventorytransfer';
		var InvTrfId	= FetchOrderId(type, ordShipId, 'ext');
		if(InvTrfId > 0)
		{
			totalCount++;
		}
		else
		{
			nlapiLogExecution('DEBUG', 'Inventory Transfer', 'Inventory Transfer to be processed for ' + ordShipId);
			var FinalUPCCodes = new Array();
			for(var ii in dataIn.order.orderDetail)
			{
				FinalUPCCodes[ii] = dataIn.order.orderDetail[ii].item.SKUId;
			}
			
			/* Validate dates */
			if(!validateISODate(dataIn.order.orderHeader.orderTransactionDt))	return false;
			
			var sellingLocation 	= dataIn.order.orderHeader.sellingLocation.locationCode;
			var fulfillingLocation  = dataIn.order.shipment[i].shipmentHeader.fulfillingLocation.country;
			var locale = (sellingLocation == fulfillingLocation) ? sellingLocation : fulfillingLocation;
			location = (locale == 'CA') ? CA_EcommDC : US_EcommDC;
			
			var subsid  = FetchSubsidiaryId('country', locale);
			if(subsid == -1)
			{
				ErrorObj.messages[0] 				= new Object();
				ErrorObj.messages[0].messagetype    = "Subsidiary error";
				ErrorObj.messages[0].message 	    = "Invalid Query. Error: Subsidiary for Country: " + locale + " cannot be found.";
				nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
				return false;
			}

			/* Lookup Items */
			FinalItemIDs = FetchIdFromName('item', FinalUPCCodes);
			nlapiLogExecution('DEBUG', 'FinalItemIDs', JSON.stringify(FinalItemIDs));
			if(FinalItemIDs[0] == 'Error')	return false

			for(var j in dataIn.order.shipment[i].shipmentDetail)  
			{
				var flag 	  = false;
				var orderDate = '';
				for(var k=0; k < FinalInvTrfArray.length; k++)
				{
					if(FinalInvTrfArray[k].extNumber == ordShipId)
					{
						var lineArray 		  	= new Object();
						lineArray 				= FinalInvTrfArray[k];
						
						var itemDetails 	  	= new Object;
						var lineItemId 			= dataIn.order.shipment[i].shipmentDetail[j].orderDetailId;

						var lineSKUId;
						for(var kk in dataIn.order.orderDetail)
						{
							var flag = false;
							if(dataIn.order.orderDetail[kk].orderLineNumber === lineItemId)
							{
								var lineUPCCode = dataIn.order.orderDetail[kk].item.SKUId; //UPC Codes 
								for(var ii in FinalItemIDs)
								{
									if(FinalItemIDs[ii].UPCCode == lineUPCCode)
									{
										lineSKUId = FinalItemIDs[ii].Id;
										flag 	  = true;
										break;
									}
								}
							}
							if(flag) break;
						} 			
						itemDetails.SKUId 	  	= lineSKUId;
						itemDetails.orderQty 	= dataIn.order.shipment[i].shipmentDetail[j].quantityOrdered;
						lineArray.items.push(itemDetails);
						flag 					= true;
						break;
					}
				}
				
				if(!flag)
				{
					//Object doesn't exist
					var lineArray 			= new Object();
					var dateValue 			= dataIn.order.orderHeader.orderTransactionDt;
					orderDate 				= moment(dateValue).format('l LT'); 	
					orderDate 				= new Date(orderDate);
					orderDate 				= nlapiDateToString(orderDate, 'date');						
					
					lineArray.extNumber 	= dataIn.order.orderHeader.orderId + '_' + dataIn.order.shipment[i].shipmentHeader.shipmentId;
					lineArray.ordNumber 	= String(dataIn.order.orderHeader.orderId);
					lineArray.invTrf		= InvTrfId;
					lineArray.orderDate 	= orderDate; 
					lineArray.memo 			= dataIn.order.orderHeader.orderHeaderMemo;
					lineArray.subsidiary	= subsid.code;
					lineArray.fromLocation	= location;
					lineArray.toLocation	= (locale == 'CA') ? CA_Virtual : US_Virtual;
					lineArray.items 		= new Array();
					var itemDetails 	  	= new Object;
					var lineItemId 			= dataIn.order.shipment[i].shipmentDetail[j].orderDetailId;

					var lineSKUId;
					for(var k in dataIn.order.orderDetail)
					{
						var flag = false;
						if(dataIn.order.orderDetail[k].orderLineNumber === lineItemId)
						{
							var lineUPCCode = dataIn.order.orderDetail[k].item.SKUId; //UPC Codes 
							for(var ii in FinalItemIDs)
							{
								if(FinalItemIDs[ii].UPCCode == lineUPCCode)
								{
									lineSKUId = FinalItemIDs[ii].Id;
									flag 	  = true;
									break;
								}
							}
						}
						if(flag) break;
					} 			
					itemDetails.SKUId 	  	= lineSKUId;
					itemDetails.orderQty 	= dataIn.order.shipment[i].shipmentDetail[j].quantityOrdered;
					lineArray.items.push(itemDetails);
					FinalInvTrfArray.push(lineArray);
				}
			}
		}
		
		if(totalCount == dataIn.order.shipment.length)
		{
			ErrorObj.status 					= "Duplicate";
			ErrorObj.messages[0]			 	= new Object();
			ErrorObj.messages[0].messagetype 	= "Duplicate request";
			ErrorObj.messages[0].message 	 	= "Request cannot be processed as the Inventory Transfer against Order " + ordShipId + " already exists.";
			nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
			return false;
		}
	}
	return true;
}

function processOrders(orderRequest)
{
	nlapiLogExecution('debug', 'processOrders', JSON.stringify(orderRequest));
	var respObject  = new Object();
	try
	{
		var custRecord  	= nlapiLoadRecord('customrecord_wtka_external_orders', orderRequest.id);
		var resp 			= custRecord.getFieldValue('custrecord_wtka_final_response');
		if(resp != null)	finalResponse = JSON.parse(resp);
		var recordCounter 	= parseInt(custRecord.getFieldValue('custrecord_wtka_request_counter'));
		var recordFileId 	= parseInt(custRecord.getFieldValue('custrecord_wtka_inbound_request'));
		dataIn 		  		= fetchRequestFromFile(recordFileId, recordCounter);
		var lastProcessed 	= custRecord.getFieldValue('custrecord_wtka_order_lastprocessed');
		
		var icsoFlag;
		var shipmentArray = new Array();
		var resp 		  = custRecord.getFieldValue('custrecord_wtka_orderid_shipment');
		if(resp != null)	shipmentArray = JSON.parse(resp);
		var i 			  = parseInt(lastProcessed);
		if(i < 0)
		{
			respObject.status = "Success"; //Already completed
			return respObject;
		}
		var retrigger 	  = false;
		customer 		  = custRecord.getFieldValue('custrecord_wtka_customerid');
		
		lookupData(dataIn);
		
		for(; !rollbackFlag && orderShipID != null && i<orderShipID.length; i++)
		{
			//Reset variables
			createFulfillmentFlag = false;
			createInvoiceFlag	  = false;
			
			nlapiLogExecution('debug', 'PROCESSING SHIPMENT', i);
			
			finalMessage 		 		= new Object();
			finalMessage.orderNumber 	= orderShipID[i];
			finalMessage.nsOrderNumber 	= 0;
			finalMessage.records	 	= new Array();
			orderDet					= new Object();
			
			var locale  			= dataIn.order.orderHeader.billingCustomer.locale.split('/');
			var sellingLocation 	= locale[locale.length-1].toUpperCase();
			if(sellingLocation != 'CA')	sellingLocation = 'US';
			var fulfillingLocation 	= dataIn.order.shipment[i].shipmentHeader.fulfillingLocation.country.toUpperCase();
			if(sellingLocation === fulfillingLocation)	icsoFlag = false;
			else										icsoFlag = true;

			var resp = custRecord.getFieldValue('custrecord_wtka_processed_ids');
			if(resp != null)	orderDetails = JSON.parse(resp);
				
			
			var orderFlag = 0;
			var orderID   = FetchOrderId('salesorder', orderShipID[i], 'ext', 'status', 'nonIcto');
			if(orderID != 0) // Existing Order
			{
				if(orderID.col == 'pendingFulfillment') //Check status
				{
					createFulfillmentFlag = true;
				}
				else if(orderID.col == 'pendingBilling') //Check status
				{
					createInvoiceFlag = true;
				}
				else
				{
					continue;
				}
				nsOrderNumber[i] = orderID.col;
				recordId 		 = orderID.id;
				orderFlag		 = recordId;
				record	 		 = nlapiLoadRecord('salesorder', recordId);
			}
			else
			{
				if(!icsoFlag) //ECOMM
				{
					orderFlag = createOrder(dataIn, orderShipID[i], i); // Create Order
					if(!rollbackFlag && orderFlag != "ZERO_SHIPPED")
					{
						orderDet.icso			= false;
						orderDet.orderId 		= orderFlag;
						orderDet.fulfillmentId	= 0;
						orderDet.invoiceId		= 0;
						orderDet.transformtype	= 0;
						orderDet.logId			= 0;
					}
				}
				else //ICTO
				{
					var orderFlag 				= createOrder(dataIn, orderShipID[i], i, orderRequest.id, recordCounter);
					if(!rollbackFlag && orderFlag != "ZERO_SHIPPED")
					{
						orderDet.icso			= true;
						orderDet.orderId 		= orderFlag.so;
						orderDet.poId			= orderFlag.po;
					}
				}
						
				finalMessage.nsOrderNumber = nsOrderNumber[i];
				
				if(createFulfillmentFlag)
				{
					orderDet.fulfillmentId	= createFulfillment(dataIn, orderShipID[i], i); // Fulfill Order
				}
				
				if(createInvoiceFlag)
				{
					orderDet.invoiceId 		= createInvoice(dataIn, orderShipID[i], i);  // Bill Order
					orderDet.transformtype	= finalTransformType;
				}
			}
						
			if(rollbackFlag)	finalResponse.status = "Error";
			if(finalMessage.records.length > 0)	finalResponse.records.push(finalMessage);
			orderDet.logId = LogCreation(1, 5, recordId, recordId, dataIn, edistatus, finalResponse, 'F'); 
			orderDetails.push(orderDet);

			shipmentArray.push(orderShipID[i]);
			
			if(nlapiGetContext().getRemainingUsage() < governanceMinUsage)
			{
				setRecordFieldValues(orderRequest.id, orderDetails, finalResponse, null, null, shipmentArray, i);
				nlapiLogExecution('debug', 'Usage Exceeded', nlapiGetContext().getRemainingUsage());
				retrigger = true;
				break;
			}
		}
		if(rollbackFlag)
		{
			//nlapiLogExecution('debug', dataIn.order.orderHeader.orderId, 'Initiating Rollback...');
			respObject.status 	 = "Error";
			finalResponse.status = "Error";
			setRecordFieldValues(orderRequest.id, orderDetails, finalResponse, 'T', 6);
		}
		else
		{
			if(!retrigger && i == dataIn.order.shipment.length) //Success - Complete processing complete
			{
				finalResponse.status = "Success";
				respObject.status 	 = "Success";
				setRecordFieldValues(orderRequest.id, orderDetails, finalResponse, null, 4, shipmentArray, -1, icsoFlag);
				nlapiLogExecution('debug', 'Final State of Sales Order Flow', JSON.stringify(finalResponse));
			}
		}
	}
	catch(other_errors)
	{
		rollbackFlag	  = true;
		var finalResp	  = new Object();
		finalResp.status  = "Exception";
		finalResp.message = "Unable to Process Orders. Error Details: ";
		finalResp.message = other_errors;
		try
		{
			var custRecord  	= nlapiLoadRecord('customrecord_wtka_external_orders', orderRequest.id);
			var resp 			= custRecord.getFieldValue('custrecord_wtka_final_response');
			if(resp != null)	finalResponse = JSON.parse(resp);
			finalResponse.status = "Error";
			finalResponse.records.push(finalResp);
			setRecordFieldValues(orderRequest.id, null, finalResponse, 'T', 6);
		}
		catch(logError){ /* Custom record not found */ }
		finalResponse.status = "Error";
		finalResponse.records.push(finalResp);
		nlapiLogExecution('debug', 'Unable to Process Orders', JSON.stringify(finalResponse));
		respObject.status = "Error";
	}
	return respObject;
}

function fetchRequestFromFile(recordFileId, recordCounter)
{
	var extFile 	= nlapiLoadFile(recordFileId);
	var fileContent = JSON.parse(extFile.getValue());
	
	var requests 	= fileContent.records;
	// nlapiLogExecution('DEBUG', 'requests length', requests.length);

	if(Array.isArray(requests) && recordCounter <= requests.length)
	{
		return requests[recordCounter];
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Unable to process request', 'Invalid File Contents');
		return 0;
	}
}

function lookupData(dataIn, orderCheck)
{
	var FinalUPCCodes = new Array();
	for(var i in dataIn.order.orderDetail)
	{
		FinalUPCCodes[i] = dataIn.order.orderDetail[i].item.SKUId;
	}
	/* Lookup Items */
	FinalItemIDs = FetchIdFromName('item', FinalUPCCodes);
	
	for(var i in dataIn.order.shipment)
	{
		createLogFlag = true; //Reset flag

		orderShipID[i] = dataIn.order.orderHeader.orderId + '_' + dataIn.order.shipment[i].shipmentHeader.shipmentId;

		var orderID = FetchOrderId('salesorder', orderShipID[i], 'ext', 'tranid', 'nonIcto');
		if(orderID != 0)
		{
			nsOrderNumber[i] = orderID.col;
			recordId 		 = orderID.id;
		}
	}
	/* Lookup Customer */
	if(customer == null || customer == '')	customer = lookupCustomer(dataIn);
	var locale  = dataIn.order.orderHeader.billingCustomer.locale.split('/');
	var country = locale[locale.length-1].toUpperCase();
	if(country != 'CA')	country = 'US';
	var subsid  = FetchSubsidiaryId('country', country);
	subsidiary 	= subsid.code;

	/* Lookup Tax Code */
	var state  	 = dataIn.order.orderHeader.billingCustomer.shippingAddress.provinceName.split('-');
	var province = state[state.length-1].toUpperCase();
	taxCode  	 = FetchTaxCode(country, province);
	
	location = (country == 'CA') ? CA_Virtual : US_Virtual;

	/* Lookup payment method */
	extSysPayment = LookupPayment(dataIn.order.orderHeader.paymentInfo.tenderType, dataIn.order.orderHeader.paymentInfo.currencyCode);
}

function LookupPayment(type, currency)
{
	var paymentMethod = 0, searchName = '';
	switch(type)
	{
		case "Cash":
		case "Cheque":
		case "CreditCard":
			searchName = '(External) ' + type + ' - ' + currency;
			if(type == "CreditCard")	creditCard = true;
			break;
		case "AccountsReceivable":
		case "GiftCard":
		case "Promise To Pay":
			searchName = 'External Payment - ' + currency;
			break;
	}
	if(searchName != '')
	{
		var cols 		 = new Array();
		var filters 	 = new Array();
		filters.push(new nlobjSearchFilter('name',  null, 'is', searchName));
		var searchResult = nlapiSearchRecord('paymentmethod', null, filters, cols);
		if(searchResult != null)	paymentMethod = searchResult[0].getId();
	}
	if(paymentMethod == 0)
	{
		nlapiLogExecution('debug', 'Unable to find Payment Method');
		var message = 'Payment Method Lookup failed.<br>';
		message += '<br><b>Error Details: </b><br> Unable to find Payment Method. Defaulting Payment Method to "External System Payment"';
		message += '<br><br><b>Data received in NetSuite: </b> <br><br>Payment - ' + type + ' currency - ' + currency + '<br><br><br>';
		sendOrderMail('Order', message, dataIn);
		paymentMethod = Ext_Payment;
	}
	return paymentMethod;
}

function lookupCustomer(dataIn)
{
	var customerId = 0;

	var customerResp = customerUpdate(dataIn.order.orderHeader.billingCustomer, 'order');
	if(customerResp.status != 'undefined' && customerResp.status == "Success")
	{
		customerId = customerResp.messages[0].recordId;
	}
	else
	{
		ErrorObj	= customerResp;
		var message = '<br><b>Error Details: </b><br>' + JSON.stringify(ErrorObj);
		sendOrderMail('Order', message, dataIn);
	}
	return customerId;
}

function getOrderObject(orderId, dataIn, i, orderFlow)
{
	var itemPrice  = new Array(),   itemTax 	= new Array();
	var taxAmount1 = 0.00, 			taxAmount2  = 0.00;
	var sotaxAmt1  = 0.00, 			sotaxAmt2   = 0.00;
	dualTax 	   = false;

	FinalShipArray 	 	= new Array();
	FinalShipOrderQty 	= new Array();
	FinalShipQty		= new Array();
	RecordUPCCodes		= new Array();

	var OrderObj 			= new Object();
	OrderObj.orderId 		= orderId;
	OrderObj.item 		 	= FinalItemIDs;
	if(orderFlow != null)
	{
		OrderObj.customer 		= customer;
		OrderObj.subsidiary		= subsidiary;
		OrderObj.location 		= location;
		OrderObj.memo 			= readValue(dataIn.order.orderHeader.orderHeaderMemo);
		OrderObj.paymentmethod 	= extSysPayment;
		OrderObj.creditCard 	= creditCard;
		OrderObj.taxCode 		= taxCode;

		/* Setting Shipping Method based on subsidiary */
		var carriername = dataIn.order.shipment[i].shipmentHeader.shippingMethod;
		var carriercode = (carriername.match(/FedEx/gi) != null) ? 'FedEx' : (carriername.match(/UPS/gi) != null) ? 'UPS' : null;
		switch(carriercode)
		{
			case 'FedEx':
				OrderObj.shipcarrier = 'nonups';
				switch(OrderObj.subsidiary)
				{
					case '3': //CA
						OrderObj.shipmethod = Fedex_CA;
						break;
					case '4': //US
						OrderObj.shipmethod = Fedex_US;
						break;
					default:
						OrderObj.shipmethod = 0;
				}
				break;

			case 'UPS':
				OrderObj.shipcarrier = 'ups';
				switch(OrderObj.subsidiary)
				{
					case '3': //CA
						OrderObj.shipmethod = UPS_CA;
						break;
					case '4': //US
						OrderObj.shipmethod = UPS_US;
						break;
					default:
						OrderObj.shipmethod = 0;
				}
				break;

			default:
				OrderObj.shipcarrier = 'nonups';
				OrderObj.shipmethod = 0;
		}
	}
	var LineDetails = dataIn.order.shipment[i];
	for(var j=0; LineDetails != null && j < LineDetails.shipmentDetail.length; j++)
	{
		FinalShipArray[j] 	 = LineDetails.shipmentDetail[j].orderDetailId;
		FinalShipOrderQty[j] = parseInt(LineDetails.shipmentDetail[j].quantityOrdered);
		FinalShipQty[j]		 = parseInt(LineDetails.shipmentDetail[j].quantityShipped);
	}

	for(var j=0; FinalShipArray != null && j < FinalShipArray.length; j++)
	{
		for(var k in dataIn.order.orderDetail)
		{
			if(dataIn.order.orderDetail[k].orderLineNumber === FinalShipArray[j])
			{
				RecordUPCCodes[j] 	= dataIn.order.orderDetail[k].item.SKUId; //UPC Codes
				itemPrice[j] 		= parseFloat(dataIn.order.orderDetail[k].amtItemPrice.amtCurrentPrice); //Set Price
				
				var totalOrderQty	= parseInt(dataIn.order.orderDetail[k].countItemOrdered);
				itemTax[j] 			= new Object();
				itemTax[j].tax1 	= 0.00;
				itemTax[j].tax2 	= 0.00;
				
				try
				{
					/* Set tax */
					var taxData 		= FetchTaxData(dataIn.order.orderDetail[k], 'order');
					dualTax 			= taxData.dualTax;
					
					var taxName = dataIn.order.orderDetail[k].tax[0].taxName;
					if(OrderObj.subsidiary == 3 && taxName.match(/pst/gi) != null)  //switch tax1 and tax2
					{
						itemTax[j].tax1 	= taxData.taxRate2;
						itemTax[j].tax2 	= taxData.taxRate1;
						if(totalOrderQty > 0) //Avoid divide by 0 error
						{
							var taxValue		 = quantityParse(taxData.tax2Amount, 		'float');
							// sotaxAmt1			+= taxValue; 										//Set currently
							var partTaxValue	 = (FinalShipOrderQty[j]/totalOrderQty)*taxValue;		//apportion tax - qtyOrdered
							sotaxAmt1			+= quantityParse(partTaxValue.toFixed(2), 	'float');
							var partTaxValue	 = (FinalShipQty[j]/totalOrderQty)*taxValue;			//apportion tax - qtyShipped
							taxAmount1			+= quantityParse(partTaxValue.toFixed(2), 	'float');
							
							var taxValue 		 = quantityParse(taxData.tax1Amount, 		'float');
							// sotaxAmt2			+= taxValue; 										//Set currently
							var partTaxValue	 = (FinalShipOrderQty[j]/totalOrderQty)*taxValue;		//apportion tax - qtyOrdered
							sotaxAmt2			+= quantityParse(partTaxValue.toFixed(2), 	'float');
							var partTaxValue	 = (FinalShipQty[j]/totalOrderQty)*taxValue;			//apportion tax - qtyShipped
							taxAmount2			+= quantityParse(partTaxValue.toFixed(2), 	'float');
						}
					}
					else
					{
						itemTax[j].tax1 	= taxData.taxRate1;
						itemTax[j].tax2 	= taxData.taxRate2;
						if(totalOrderQty > 0) //Avoid divide by 0 error
						{
							var taxValue		 = quantityParse(taxData.tax1Amount, 		'float');
							// sotaxAmt1			+= taxValue; 										//Set currently
							var partTaxValue	 = (FinalShipOrderQty[j]/totalOrderQty)*taxValue; 		//apportion tax - qtyOrdered
							sotaxAmt1			+= quantityParse(partTaxValue.toFixed(2), 	'float');
							var partTaxValue	 = (FinalShipQty[j]/totalOrderQty)*taxValue;	  		//apportion tax - qtyShipped
							taxAmount1			+= quantityParse(partTaxValue.toFixed(2), 	'float');
							
							var taxValue 		 = quantityParse(taxData.tax2Amount, 		'float');
							// sotaxAmt2			+= taxValue; 										//Set currently
							var partTaxValue	 = (FinalShipOrderQty[j]/totalOrderQty)*taxValue;		//apportion tax - qtyOrdered
							sotaxAmt2			+= quantityParse(partTaxValue.toFixed(2), 	'float');
							var partTaxValue	 = (FinalShipQty[j]/totalOrderQty)*taxValue;			//apportion tax - qtyShipped
							taxAmount2			+= quantityParse(partTaxValue.toFixed(2), 	'float');
						}
					}
				}
				catch(ex){}
			}
		}
	}
	OrderObject.orderItems	= RecordUPCCodes;
	OrderObj.currency 	 	= dataIn.order.orderHeader.paymentInfo.currencyCode;
	OrderObj.tax 		 	= itemTax;
	OrderObj.dualTax 		= dualTax;
	OrderObj.soTax1Amount	= sotaxAmt1;
	OrderObj.soTax2Amount	= sotaxAmt2;
	OrderObj.tax1Amount   	= taxAmount1;
	OrderObj.tax2Amount   	= taxAmount2;
	OrderObj.price 		 	= itemPrice;
	nlapiLogExecution('debug', 'Values', JSON.stringify(OrderObj));
	return OrderObj;
}

function createOrder(dataIn, orderId, i, custRecordId, requestIndex)
{
	var ictoFlow = false;
	var recId 	 = 0;
	if(custRecordId != null)
	{
		nlapiLogExecution('debug', 'Creating Intercompany Order: ' + i, orderId);
		ictoFlow = true;
	}
	else
	{
		nlapiLogExecution('debug', 'Creating Order: ' + i, orderId);
	}
	if(!ictoFlow)	recordId = 0;			
	try
	{
		OrderObject = getOrderObject(orderId, dataIn, i, 'orderFlow');
		
		if(ictoFlow)
		{
			OrderObject.poVendor = 0;
			/* Map vendors */
			var locale  		= dataIn.order.orderHeader.billingCustomer.locale.split('/');
			var sellingLocation = locale[locale.length-1].toUpperCase();
			var fulfillingLocation 	= dataIn.order.shipment[i].shipmentHeader.fulfillingLocation.country.toUpperCase();
			if(sellingLocation != 'CA')	sellingLocation = 'US';
			switch(sellingLocation)
			{
				case 'CA':
					switch(fulfillingLocation)
					{
						case 'US':
							OrderObject.poVendor =  CA_US_Vendor; //SW US Inc. - Inventory - Canada (Inventory)
							break;
					}
					break;
				case 'US':
					switch(fulfillingLocation)
					{
						case 'CA':
							OrderObject.poVendor =  US_CA_Vendor; //Kit and Ace Designs Inc. - USA (Inventory)
							break;
					}
					break;
			}
			nlapiLogExecution('debug', 'PO Vendor', OrderObject.poVendor);
		}
		
		var recordType 	= 'salesorder';
		record 			= nlapiCreateRecord(recordType);
		
		if(ictoFlow)
		{
			var requestObject 		= new Object();
			requestObject.id  		= custRecordId;
			requestObject.index 	= requestIndex;
			requestObject.shipment 	= i;
			record.setFieldValue('custbody_wtka_request_data',			JSON.stringify(requestObject)); //Request Reference
		}
		
		record.setFieldValue('entity', 									OrderObject.customer);
		record.setFieldValue('custbody_wtka_extsys_order_number',		orderId);
		record.setFieldValue('custbody_wtka_extsys_hybris_order',		String(dataIn.order.orderHeader.orderId));
		record.setFieldValue('externalid', 								orderId);
		record.setFieldText('currency',									OrderObject.currency);
		var orderTransactionDt  = moment(dataIn.order.orderHeader.orderTransactionDt).format('l LT');
		if(orderTransactionDt != "Invalid date")
		{
			orderTransactionDt 		= new Date(orderTransactionDt);
			orderTransactionDt 		= nlapiDateToString(orderTransactionDt, 'date');
			record.setFieldValue('trandate', 							orderTransactionDt);
		}
		record.setFieldValue('memo', 									OrderObject.memo);
		record.setFieldValue('location', 								OrderObject.location);
		var shipDt 				= moment(dataIn.order.shipment[i].shipmentHeader.shipDt).format('l LT');
		if(shipDt != "Invalid date")
		{
			shipDt 					= new Date(shipDt);
			shipDt 					= nlapiDateToString(shipDt, 'date');
			record.setFieldValue('shipdate', 							shipDt);
		}
		var shipReceivedDt 		= moment(dataIn.order.shipment[i].shipmentHeader.shipReceivedDt).format('l LT');
		if(shipReceivedDt != "Invalid date")
		{
			shipReceivedDt 			= new Date(shipReceivedDt);
			shipReceivedDt 			= nlapiDateToString(shipReceivedDt, 'date');
			record.setFieldValue('custbody_ka_to_expected_ship_date', 	shipReceivedDt);
		}
		var expectedShipDate 	= moment(dataIn.order.shipment[i].shipmentHeader.expectedShipDate).format('l LT');
		if(expectedShipDate != "Invalid date")
		{
			expectedShipDate 		= new Date(expectedShipDate);
			expectedShipDate 		= nlapiDateToString(expectedShipDate, 'date');
			record.setFieldValue('custbody_ka_to_expected_recv_date', 	expectedShipDate);
		}
		
		/* Set custom Payment */
		record.setFieldValue('paymentmethod', 							OrderObject.paymentmethod);
		record.setFieldValue('custbody_wtka_extsys_payment_method',		dataIn.order.orderHeader.paymentInfo.tenderType);
		record.setFieldValue('ccapproved', 								'T'); //Approved Card
		record.setFieldValue('getauth', 								'F'); //Approved Card
		
		//==========Clear CC details from Sales Order, Cash Sale and return auth=============
		record.setFieldValue('authcode', 								'');
		record.setFieldValue('ccnumber', 								'');
		record.setFieldValue('ccexpiredate', 							'');
		record.setFieldValue('ccname', 									'');
		record.setFieldValue('ccstreet', 								'');
		record.setFieldValue('cczipcode', 								'');	
				
		record.setFieldValue('exchangerate', 							dataIn.order.orderHeader.exchangeRate);
		
		record.setFieldValue('billaddresslist', '');
		record.setFieldValue('billaddressee', 	dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.billingFirstName + ' ' + dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.billingLastName);
		// record.setFieldValue('billphone', 		dataIn.order.orderHeader.billingCustomer.customerPhoneNumber);						//Modified to avoid Invalid Phone Errors
		record.setFieldValue('billaddr1', 		dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.billingAddressLine1);
		record.setFieldValue('billaddr2', 		dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.billingAddressLine2);
		record.setFieldValue('billcity', 		dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.cityName);
		record.setFieldValue('billstate', 		dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.provinceName);
		record.setFieldValue('billzip', 		dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.postalCode);
		record.setFieldValue('billcountry', 	dataIn.order.shipment[i].shipmentHeader.recipient.billingAddress.countryName);
		
		/* Set custom Shipping Address */
		record.setFieldValue('shipaddresslist', '');
		record.setFieldValue('shipaddressee', 	dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.recipientFirstName + ' ' + dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.recipientLastName);
		// record.setFieldValue('shipphone', 		dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.recipientPhoneNumber);	//Modified to avoid Invalid Phone Errors
		record.setFieldValue('shipaddr1', 		dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.shippingAddressLine1);
		record.setFieldValue('shipaddr2', 		dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.shippingAddressLine2);
		record.setFieldValue('shipcity', 		dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.cityName);
		record.setFieldValue('shipstate', 		dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.provinceName);
		record.setFieldValue('shipzip', 		dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.postalCode);
		record.setFieldValue('shipcountry', 	dataIn.order.shipment[i].shipmentHeader.recipient.shippingAddress.countryName);
		
		var zeroShipped = 0;
		for(var j=0; FinalShipArray != null && j < FinalShipArray.length; j++)
		{
			for(var f=0; OrderObject.item != null && f < OrderObject.item.length; f++)
			{
				if(RecordUPCCodes[j] == OrderObject.item[f].UPCCode)
				{
					if(FinalShipQty[j] == 0)	zeroShipped++;
					
					record.selectNewLineItem('item');
					record.setCurrentLineItemValue('item', 'location', 	OrderObject.location);
					record.setCurrentLineItemValue('item', 'item', 		OrderObject.item[f].Id);
					record.setCurrentLineItemValue('item', 'quantity', 	FinalShipOrderQty[j]);
					if(ictoFlow) //Drop Shipment
					{
						record.setCurrentLineItemValue('item', 'povendor', 	OrderObject.poVendor); 
					}
					else //Avoid Drop Shipment
					{
						record.setCurrentLineItemValue('item', 'createpo', 	null);
						record.setCurrentLineItemValue('item', 'povendor', 	null);
					}
					/* Set custom Price */
					record.setCurrentLineItemValue('item', 'price', 	-1);
					record.setCurrentLineItemValue('item', 'rate', 		OrderObject.price[j]);
						
					/* Set Taxes */
					record.setCurrentLineItemValue('item', 'taxcode', OrderObject.taxCode);
					
					if(!isNullOrEmpty(OrderObject.tax) && OrderObject.subsidiary == 4) //subsidiary - 4 US
					{
						record.setCurrentLineItemValue('item', 'taxrate1', 	OrderObject.tax[j].tax1);
						record.setCurrentLineItemValue('item', 'taxrate2', 	OrderObject.tax[j].tax2);
					}
					
					record.setCurrentLineItemValue('item', 'custcol_wtka_taxrate1', 	OrderObject.tax[j].tax1);
					record.setCurrentLineItemValue('item', 'custcol_wtka_taxrate2', 	OrderObject.tax[j].tax2);
					
					record.commitLineItem('item');
					break;
				}
			}
		}

		if(zeroShipped == RecordUPCCodes.length)
		{
			var  orderObject 				= new Object();
			orderObject.status 				= "Success";
			orderObject.transactiontype 	= "Sales Order";
			orderObject.transactionNumber 	= nsOrderNumber[i];
			orderObject.message 			= "Shipment has zero items shipped"
			finalMessage.records.push(orderObject);
			nlapiLogExecution('debug', 'ORDER RESPONSE ' + orderId, JSON.stringify(finalMessage));
			return "ZERO_SHIPPED";
		}
		
		/* Set total tax amount */
		record.setFieldValue('taxamountoverride', OrderObject.soTax1Amount);
		record.setFieldValue('taxamount2override', OrderObject.soTax2Amount); //PST for Canadian Orders
		
		/* Commenting Setting the Shipcarrier as the system defaults it to Fedex/More */
		//record.setFieldText('shipcarrier', 							OrderObject.shipcarrier);
		if(OrderObject.shipmethod > 0)	record.setFieldValue('shipmethod', OrderObject.shipmethod);
		
		recordId = nlapiSubmitRecord(record);
		
		record 			 = nlapiLoadRecord(recordType, recordId);
		nsOrderNumber[i] = record.getFieldValue('tranid');
		
		var  ordObject 					= new Object();
		ordObject.status 				= "Success";
		ordObject.transactiontype 		= "Sales Order";
		ordObject.transactionid 		= recordId;
		ordObject.transactionNumber 	= nsOrderNumber[i];
		finalMessage.records.push(ordObject);

		if(ictoFlow)
		{
			var poId		 = record.getLineItemValue('item', 'createdpo', 1);
			if(poId == null || poId <= 0)
			{
				edistatus	 			= 3;
				rollbackFlag 			= true;
				var  orderObject 		= new Object();
				orderObject.status  	= "Exception"; 
				orderObject.messagetype = "Sales Order Creation Failure";
				orderObject.message 	= 'Sales Order record could not be created. Details as below.'; 
				orderObject.details 	= 'Intercompany Purchase Order has not been created. Check if items have been marked as drop ship';
				finalMessage.records.push(orderObject);
				nlapiLogExecution('debug', 'createICSO - Error' , 'PO not created');
				return recId;
			}
			
			var poRecord	 = nlapiLoadRecord('purchaseorder', poId);
			var poNumber	 = poRecord.getFieldValue('tranid');
			poNumber		 = "E" + poNumber;
			poRecord.setFieldValue('tranid', 		poNumber);
			poRecord.setFieldText('approvalstatus', 'Approved');
			poId = nlapiSubmitRecord(poRecord, false, true);
			
			var  orderObject 				= new Object();
			orderObject.status 				= "Success";
			orderObject.transactiontype 	= "Purchase Order";
			orderObject.transactionid 		= poId;
			orderObject.transactionNumber	= poNumber;
			finalMessage.records.push(orderObject);
			
			recId	 = new Object();
			recId.so = recordId;
			recId.po = poId;
		}
		createFulfillmentFlag = true;
		edistatus 	 		  = 1;
	}
	catch(err_order)
	{
		edistatus	 = 3; // Exception
		rollbackFlag = true;
		
		var  orderObject 		= new Object();
		orderObject.status  	= "Exception"; 
		orderObject.messagetype = "Sales Order Creation Failure";
		orderObject.message 	= 'Sales Order record could not be created. Details as below.'; 
		orderObject.details 	= err_order.getCode() + ' : ' + err_order.getDetails();
		finalMessage.records.push(orderObject);
		nlapiLogExecution('debug', orderId + ' createOrder - Error' , err_order);
	}
	nlapiLogExecution('debug', 'ORDER RESPONSE ' + orderId, JSON.stringify(finalMessage));
	if(ictoFlow)	return recId;
	else			return recordId;
}

function createFulfillment(dataIn, orderId, i, ictoFlow)
{
	nlapiLogExecution('debug', 'Fulfilling Order: ' + i, orderId);
	if(OrderObject.orderId == undefined)
	{
		if(ictoFlow != null)	OrderObject = getOrderObject(orderId, dataIn, i);
		else					OrderObject = getOrderObject(orderId, dataIn, i, true);
	}

	var fulfillmentId 	= 0;
	TranDate 			= '';
	try
	{
		//Transform Order
		TranDate  = moment(dataIn.order.shipment[i].shipmentHeader.shipDt).format('l LT');
		{
			TranDate  	= moment(dataIn.order.shipment[i].shipmentHeader.shipDt).format('l LT');
			TranDate 	= new Date(TranDate);
			TranDate 	= nlapiDateToString(TranDate, 'date');		
		}
		var ReturnReqRefNumber 	 = '';
		var TrackingNumber 		 = dataIn.order.shipment[i].shipmentHeader.trackingNumber;

		var itemFulfillment 	 = nlapiTransformRecord('salesorder', recordId, 'itemfulfillment');
		var itemFulfillItemCount = itemFulfillment.getLineItemCount('item');
		var inventoryItemFulfillmentCount = 0;
		
		if(itemFulfillItemCount == 0)
		{
			var fulfillmentObject 			= new Object();
			fulfillmentObject.status  		= "Error";
			fulfillmentObject.messagetype 	= "Item Fulfillment Failure";
			fulfillmentObject.message 		= 'Order cannot be processed as items are backordered and there are no items available to fulfill';
			edistatus						= 2;
			rollbackFlag 					= true;
			finalMessage.records.push(fulfillmentObject);
			return -1;
		}
		for(var f=0; f < itemFulfillment.getLineItemCount('item'); f++)
		{
			itemFulfillment.selectLineItem('item', f+1);
			var itemCode = itemFulfillment.getCurrentLineItemValue('item', 'custcol_wtka_upccode');
			for(var j=0; FinalShipArray != null && j < FinalShipArray.length; j++)
			{
				if(RecordUPCCodes[j] == itemCode)
				{
					itemFulfillment.setCurrentLineItemValue('item', 'quantity', 	FinalShipQty[j]);
					itemFulfillment.setCurrentLineItemValue('item', 'fulfill',  	'T');
					break;
				}
			}
			itemFulfillment.commitLineItem('item');
		}
		itemFulfillment.setLineItemValue('package', 'packageweight', 		 1, parseInt('1'));
		itemFulfillment.setLineItemValue('package', 'packagedescr', 		 1, ReturnReqRefNumber);
		itemFulfillment.setLineItemValue('package', 'packagetrackingnumber', 1, TrackingNumber);
		itemFulfillment.setFieldValue('trandate', 	TranDate);
		itemFulfillment.setFieldText('shipstatus', 	'Shipped', 				true);

		fulfillmentId 	= nlapiSubmitRecord(itemFulfillment, true);

		var fulfillmentObject 				= new Object();
		fulfillmentObject.status 			= "Success";
		fulfillmentObject.transactiontype 	= "Item Fulfillment";
		fulfillmentObject.transactionid 	= fulfillmentId;
		fulfillmentObject.transactionNumber = nlapiLookupField('itemfulfillment', fulfillmentId, 'tranid');
		fulfillmentObject.transactiondate 	= TranDate;
		finalMessage.records.push(fulfillmentObject);

		itemfulfillmentflag	 = true;
		createInvoiceFlag 	 = true;
		edistatus 			 = 1;
	}
	catch(err_fulfill)
	{
		edistatus	 = 3; // Exception
		rollbackFlag = true;

		var fulfillmentObject 			= new Object();
		fulfillmentObject.status  		= "Error";
		fulfillmentObject.messagetype 	= "Item Fulfillment Failure";
		fulfillmentObject.message 		= 'Item Fulfillment record could not be created. Details as below.';
		fulfillmentObject.details 		= err_fulfill.getCode() + ' : ' + err_fulfill.getDetails();
		finalMessage.records.push(fulfillmentObject);
		nlapiLogExecution('debug', orderId + ' createFulfillment - Error' , err_fulfill);
	}
	nlapiLogExecution('debug', 'FULFILLMENT RESPONSE ' + orderId, JSON.stringify(finalMessage));
	return fulfillmentId;
}

function createInvoice(dataIn, orderId, i, ictoFlow)
{
	nlapiLogExecution('debug', 'Billing Order: ' + i, orderId);
	var finalRecordId  = 0;
	finalTransformType = 0;

	if(OrderObject.orderId == undefined)
	{
		if(ictoFlow != null)	OrderObject = getOrderObject(orderId, dataIn, i);
		else					OrderObject = getOrderObject(orderId, dataIn, i, true);
		nlapiLogExecution('debug', 'Values', JSON.stringify(OrderObject));
	}

	try
	{
		if(TranDate == '') 		
		{
			TranDate  	= moment(dataIn.order.shipment[i].shipmentHeader.shipDt).format('l LT');
			TranDate 	= new Date(TranDate);
			TranDate 	= nlapiDateToString(TranDate, 'date');		
		}
		record = nlapiLoadRecord('salesorder', recordId);
		var recterms 	   = record.getFieldValue('terms');
		var recpaymeth 	   = record.getFieldValue('paymentmethod');
		finalTransformType = ((recterms != null && recterms.length > 0) || (recpaymeth != null && recpaymeth.length > 0)) ? 'cashsale' : 'invoice';
		var finalRecord    = nlapiTransformRecord('salesorder', recordId, finalTransformType);

		for(var r=0; r < finalRecord.getLineItemCount('item'); r++)
		{
			finalRecord.selectLineItem('item', r+1);
			var ItemCode = finalRecord.getLineItemValue('item', 'custcol_wtka_upccode', r+1);

			for(var f=0; RecordUPCCodes != null && f <RecordUPCCodes.length; f++)
			{
				if(RecordUPCCodes[f] == ItemCode)
				{
					finalRecord.setCurrentLineItemValue('item', 'price', 	-1);
					finalRecord.setCurrentLineItemValue('item', 'rate', 	OrderObject.price[f]);
					
					var subsidiary = finalRecord.getFieldText('subsidiary');
					var us = subsidiary.match(/Kit and Ace Operating US/i);
					if(!isNullOrEmpty(OrderObject.tax) && us != null)
					{
						finalRecord.setCurrentLineItemValue('item', 'taxrate1', 	OrderObject.tax[f].tax1);
						finalRecord.setCurrentLineItemValue('item', 'taxrate2', 	OrderObject.tax[f].tax2);
					}
					
					finalRecord.commitLineItem('item');
					break;
				}
			}
		}

		finalRecord.setFieldValue('trandate', TranDate);

		/* Set total tax amount */
		finalRecord.setFieldValue('taxamountoverride', OrderObject.tax1Amount);
		finalRecord.setFieldValue('taxamount2override', OrderObject.tax2Amount); //PST for Canadian Orders
				
		finalRecord.setFieldValue('ccapproved', 	'T');
		finalRecord.setFieldValue('chargeit', 		'F');
		
		//==========Clear CC details from Cash sale=============
		finalRecord.setFieldValue('ccnumber', 		'');
		finalRecord.setFieldValue('ccexpiredate', 	'');
		finalRecord.setFieldValue('ccname', 		'');
		finalRecord.setFieldValue('ccstreet', 		'');
		finalRecord.setFieldValue('cczipcode', 		'');	
		
		
		finalRecord.setFieldValue('custbody_erp_inv_cs_created', 'T');
		finalRecordId  = nlapiSubmitRecord(finalRecord, true);

		var invoiceObject 				= new Object();
		invoiceObject.status 			= "Success";
		invoiceObject.transactiontype 	= (finalTransformType == 'cashsale') ? "Cash Sale" : "Invoice";
		invoiceObject.transactionid 	= finalRecordId;
		invoiceObject.transactionNumber = nlapiLookupField(finalTransformType, finalRecordId, 'tranid');
		invoiceObject.transactiondate 	= TranDate;
		finalMessage.records.push(invoiceObject);

		edistatus = 1;
		
		try
		{
			//Close Partially Fulfilled Orders
			record = nlapiLoadRecord('salesorder', recordId);
			var soStatus = record.getFieldValue('status');
			if(soStatus == 'Partially Fulfilled')
			{
				nlapiLogExecution('debug', 'Closing Partially Fulfilled Order: ' + i, orderId);
				for(var r = 1; r<=record.getLineItemCount('item'); r++)
				{
				   /* Only set items those are not completely fulfilled*/	
					// var lineQtyFulfilled = record.getLineItemValue('item', 'quantityfulfilled', r);	
					// var lineQtyOrdered	= record.getLineItemValue('item', 'quantity', 			r);	
					// if(lineQtyFulfilled < lineQtyOrdered)
					{
						record.setLineItemValue('item', 'isclosed', r, 'T');
					}
				}
				var recId = nlapiSubmitRecord(record, false, true);
			}
		}
		catch(close_err)
		{
			edistatus	 = 3; // Exception
			// rollbackFlag = true; //Rollback if failed?

			var soObject 			= new Object();
			soObject.status 		= 'Exception';
			soObject.messagetype 	= "Partial Sales Order Failure";
			soObject.message 		= 'Sales order with Partially Fulfilled status could not be closed. Details as below.';
			soObject.details 		= close_err.getCode() + ' : ' + close_err.getDetails();
			finalMessage.records.push(soObject);
			nlapiLogExecution('debug', 'closePartialOrder - Error' , close_err);
		}
	}
	catch(err_transform)
	{
		edistatus	 = 3; // Exception
		rollbackFlag = true;

		var invoiceObject 			= new Object();
		invoiceObject.status 		= 'Exception';
		invoiceObject.messagetype 	= "Cash Sale/Invoice Failure";
		invoiceObject.message 		= 'Cash Sale/Invoice could not be created. Details as below.';
		invoiceObject.details 		= err_transform.getCode() + ' : ' + err_transform.getDetails();
		finalMessage.records.push(invoiceObject);
		nlapiLogExecution('debug', orderId + ' createInvoice - Error' , err_transform);
	}
	nlapiLogExecution('debug', 'INVOICE RESPONSE ' + orderId, JSON.stringify(finalMessage));
	return finalRecordId;
}

function rollbackRestlet(requestObject)
{
	var recID    = requestObject.id;
	var orderDet = requestObject.orderDetails;
	var newObj 	 = new Object();
	try
	{
		if(orderDet != null && orderDet.length > 0)
		{
			nlapiLogExecution('debug', recID, 'Rolling back...');
			var rollbackstatus  = rollbackOrderProcess(orderDet); // Rollback all processes
			
			if(rollbackstatus)
			{
				newObj.status	= 'Rollback Success';
				newObj.message	= 'The process has been successfully rolled back and all the transactions listed above have been deleted.';
			}
			else
			{
				newObj.status 	= 'Rollback Failure';
				newObj.message 	= 'The process failed to rollback.';
			}
		}
	}
	catch(rollback_err)
	{
		nlapiLogExecution('debug', recID + ': rollbackRestlet failed', rollback_err);
		newObj.status 	= 'Rollback Failure';
		newObj.message 	= 'The process failed to rollback.';
		newObj.details 	= rollback_err;
		finalResponse.records.push(newObj); //Request Object
	}
	return newObj;
}

function rollbackOrderProcess(objectDetails)
{
	var recordCount = 0;
	nlapiLogExecution('debug', 'objectDetails', JSON.stringify(objectDetails));
	for(var i=0; objectDetails != null &&  i < objectDetails.length; i++)
	{
		try
		{
			var icsoFlag = objectDetails[i].icso;
			var orderId 		= objectDetails[i].orderId;
			var fulfillmentId 	= objectDetails[i].fulfillmentId;
			var invoiceId 		= objectDetails[i].invoiceId;
			var transformtype	= objectDetails[i].transformtype;
			var logId			= objectDetails[i].logId;
			
			if(icsoFlag)
			{
				var poId 		= objectDetails[i].poId;
				if(poId > 0)	var fulfillmentRecord 	= nlapiDeleteRecord('purchaseorder', 	poId, 	 {deletionreason: 8}); //External System Rollback
				nlapiLogExecution('debug', 'Rollback record details', 'orderId: ' + orderId + ' | poId: ' + poId);
			}
			
			// var logStatus 		= deleteLogs(orderId); //Delete Logs
			if(logId != 0)			var logStatus 			= deleteLogsFromIds(logId); //Delete Logs
			if(invoiceId > 0)		var invoiceRecord 		= nlapiDeleteRecord(transformtype, 		invoiceId, 		{deletionreason: 8}); //External System Rollback
			if(fulfillmentId > 0)	var fulfillmentRecord 	= nlapiDeleteRecord('itemfulfillment', 	fulfillmentId, 	{deletionreason: 8}); //External System Rollback
			if(orderId > 0)			var orderRecord 		= nlapiDeleteRecord('salesorder', 		orderId, 		{deletionreason: 8}); //External System Rollback
			nlapiLogExecution('debug', 'Rollback record details', 'orderId: ' + orderId + ' | fulfillmentId: ' + fulfillmentId + ' | invoiceId: ' + invoiceId);
		}
		catch(err_rollback)
		{
			nlapiLogExecution('debug', 'Rollback Failure', 'orderId: ' + orderId + ' || Error: ' + err_rollback);
			return false;
		}
	}
	nlapiLogExecution('debug', 'Rollback Success');
	return true;
}


function sendOrderMail(type, message, request)
{
	var subject = (type == 'Order') ? 'Order Processing failure' : 'Inventory Transfer failure';
	var body 	= 'Hello,<br><br>';
	body 		+= 'Order request failed in NetSuite due to below error.<br>';
	body 		+= message;
	if(request != null && request != '')	
	{
		body 	+= '<br><br><b>Data received in NetSuite: </b><br><br>' + JSON.stringify(request) + '<br><br><br>';
	}
	body 		+= '<br><br>Process needs to be resubmitted or handled manually through user interface after corrections.';
	body 		+= '<br><br><br><br><br><br><br>Thanks';
	body 		+= '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
	nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
	if(sendEmail)	nlapiSendEmail(243423, toList, subject, body, ccList);
}