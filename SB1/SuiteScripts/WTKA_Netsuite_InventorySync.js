/**
 *	File Name		:	WTKA_Netsuite_InventorySync.js
 *	Function		:	GET and POST methods for Inventory synchronization with External System 
 * 	Authors			:	Rini Thomas, Vinod Kumar
 *	Company			:	Wipro Limited
 *	Release Dates	:	5-May-2016 (v1.0)
 * 	Current Version	:	1.0
**/

{
	var ErrorObj    		= new Object();
	ErrorObj.status    		= "Error";
	ErrorObj.messages    	= new Array();
	ErrorObj.messages[0]  	= new Object();
	var returnObject   		= new Object();
	returnObject.response  	= new Object();

	var FinalString,		processFromDate,				crRequest, 			crResourceId, 	crFileId, 			crFileCounter;
	var crLastProcessed, 	crStatus, 						crRequestId, 		crTimeOutCount, crRequestObj; 
	var lastProcessedId  	= 0,							fileCounter   		= 0,			script_entry 		= 0;
	var governanceMinUsage  = 200,							processLoopCount 	= 11, 			scriptMaxTimeout	= 3500; //3600 - is max
	var counterFlag   		= false, 						scriptLimit			= false,		sendEmail 			= true; //false;
	var processLocationType = new Array(),					processLocationCode = new Array();
	var toList 				= ['3PL_eComm_Integration@kitandace.com'];
	var	ccList 				= null; //['']; //Enter CC Addresses as array
	var folderName			= 'External System Synchronization';
	var subject 			= 'Inventory Synchronization failure';
	var body 				= 'Hello,<br><br>';
	body 					+= 'Inventory Synchronization failed in NetSuite due to below error.<br>';
	
	var processThis = ''; 
}	

function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}


function get_WTKA_NetSuiteInventoryData(dataIn)
{
	try
	{
		returnObject = getNetSuiteSyncData(dataIn, 1);
	}
	catch(get_err)
	{
		sendSyncMail(sendEmail, subject, body, get_err, dataIn);
		returnObject.response = get_err;
		nlapiLogExecution('debug', 'GET error', get_err);
	}
	return returnObject;
}

function ValidateRequest(dataIn)
{
	try
	{
		if(!emptyInbound(dataIn)) 															return false;
		if(!emptyObject(dataIn.resourceId,            			"ResourceId"))    			return false; 
		if(!emptyObject(dataIn.inventoryRequest,        		"InventoryRequest"))   		return false; 
		if(!emptyObject(dataIn.inventoryRequest.location,    	"Location"))     			return false;
		if(!containLines(dataIn.inventoryRequest.location,    	"location data present")) 	return false; 

		for(var i in dataIn.inventoryRequest.location)
		{
			if(!emptyObject(dataIn.inventoryRequest.location[i], "Location array"))  		return false;
		}
		
		//Validate Date
		if(dataIn.inventoryRequest.fromDate != null && dataIn.inventoryRequest.fromDate != '')
		{
			if(!validateISODate(dataIn.inventoryRequest.fromDate))	return false;
		}
		
		var resourceId  = dataIn.resourceId;
		var filters   	= new Array();
		filters.push(new nlobjSearchFilter('custrecord_wtka_extsys_sync_resourceid', null, 'is', resourceId));
		filters.push(new nlobjSearchFilter('custrecord_wtka_extsys_sync_type', 		 null, 'is', 1));
		
		var updateRecord   = nlapiSearchRecord('customrecord_wtka_extsys_sync', null, filters, null);
		if(updateRecord != null && updateRecord.length > 0)
		{
			ErrorObj.messages[0].messagetype = "Bad Request";
			ErrorObj.messages[0].message   = "ResourceId already exists. Please try again with a new ResourceId."
			return false;
		}
		return true;
	}
	catch(validate_Err)
	{
		sendSyncMail(sendEmail, subject, body, validate_Err, dataIn);
		nlapiLogExecution('debug', 'Validate error', validate_Err);
		return false;
	}
}

function post_WTKA_NetSuite_InventorySync(dataIn)
{
	try
	{
		nlapiLogExecution('DEBUG', 'Inbound Request', JSON.stringify(dataIn));
		var filters 	= new Array();
		filters.push(new nlobjSearchFilter('name', null, 'is',		folderName));
		var fileCabinet = nlapiSearchRecord('folder', 	null, filters, 	null);
		var folderId   	= (fileCabinet != null) ? fileCabinet[0].getId() : 0;
		if(ValidateRequest(dataIn)) //Queued
		{
			try
			{
				var newFile = nlapiCreateFile(dataIn.resourceId, 'PLAINTEXT', '');
				newFile.setFolder(folderId);
				var fileId = nlapiSubmitFile(newFile);
				
				var InventoryUpdate = nlapiCreateRecord('customrecord_wtka_extsys_sync');
              
                if(dataIn.runClone){
                InventoryUpdate.setFieldValue('custrecord_erp_inventory_sync_runclone', dataIn.runClone); //IDS to Run Clone
                }
              
				InventoryUpdate.setFieldValue('custrecord_wtka_extsys_sync_request',  	 	JSON.stringify(dataIn));
				InventoryUpdate.setFieldValue('custrecord_wtka_extsys_sync_resourceid',  	dataIn.resourceId);
				InventoryUpdate.setFieldValue('custrecord_wtka_extsys_sync_status',  	 	0); // 0 = not started / In Queue, // 1 = In Progress, // 2 = Completed, // 3 = Error 
				InventoryUpdate.setFieldValue('custrecord_wtka_extsys_sync_type',  		 	1); // Inventory Sync Type
				InventoryUpdate.setFieldValue('custrecord_wtka_extsys_sync_file',     	 	fileId); // File ID
				InventoryUpdate.setFieldValue('custrecord_wtka_extsys_sync_timeoutcount', 	0);
				var updateRecordId  = nlapiSubmitRecord(InventoryUpdate);
				
				/ Response "PENDING" /
				returnObject.response.status  	= "PENDING";
				returnObject.response.eta   	= "10 mins";
				nlapiLogExecution('DEBUG', 'responseObject', JSON.stringify(returnObject));
			}
			catch(err)
			{
				returnObject.response.message = "ERROR: " + err;
				sendSyncMail(sendEmail, subject, body, err, dataIn);
				nlapiLogExecution('debug', 'Search error', err);
			}
			return returnObject;
		}
		else
		{
			nlapiLogExecution('DEBUG', 'ErrorObj ', JSON.stringify(ErrorObj));
			sendSyncMail(sendEmail, subject, body, ErrorObj, dataIn);
			return ErrorObj;
		}
	}
	catch(post_err)
	{
		ErrorObj.status    		= "Exception";
		ErrorObj.messages[0]  	= post_err;
		sendSyncMail(sendEmail, subject, body, post_err, dataIn);
		nlapiLogExecution('debug', 'POST error', post_err);
		return ErrorObj;
	}
}

function FetchInventoryRecords()
{
	try
	{
		/*** Batch Statuses:
		0 = not started / In Queue
		1 = In Progress
		2 = Completed
		3 = Error ***/

		
		nlapiLogExecution('audit', '1 IDS FetchInventoryRecords : ENTRY', 'FetchInventoryRecords');
		
		
		script_entry = new Date(); //Script Start Time
		
		//1. Fetch data from custom record based on status 0 and 1
		var filters = new Array();
		filters.push(new nlobjSearchFilter('custrecord_wtka_extsys_sync_type', 	null,  'is', 		1)); //Inventory sync
		filters.push(new nlobjSearchFilter('custrecord_wtka_extsys_sync_status', null, 'lessthan', 	2));

		//Fetch records which are in Progress first and then by Internal id
		var cols = new Array();
		cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_request'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_resourceid'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_file'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_filecounter'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_lastprocess'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_timeoutcount'));
		cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_status').setSort(true)); //Fetch records which are in Progress first and then by Internal id
		cols.push(new nlobjSearchColumn('internalid').setSort());
		
		var searchRecords = nlapiSearchRecord('customrecord_wtka_extsys_sync', null, filters, cols);
		if(searchRecords != null)
		{
			
			//nlapiLogExecution('audit', 'IDS FetchInventoryRecords : searchRecords-Fetch', searchRecords.length);
			//nlapiLogExecution('audit', 'IDS FetchInventoryRecords : run loop 1', 'loop with gov stuff');
			for(var i = 0; getScriptTimeConsumed() < scriptMaxTimeout && nlapiGetContext().getRemainingUsage() > governanceMinUsage && searchRecords != null && i < searchRecords.length ; i++)
			{
				
				var tempObjForLog = new Object(); 
				tempObjForLog.getScriptTimeConsumed	= getScriptTimeConsumed();
				tempObjForLog.scriptMaxTimeout		= scriptMaxTimeout;
				tempObjForLog.getRemainingUsage		= nlapiGetContext().getRemainingUsage();
				tempObjForLog.governanceMinUsage	= governanceMinUsage ;
				tempObjForLog.searchRecordsnull		= searchRecords != null;
				tempObjForLog.searchRecordslength	= searchRecords.length;
				
				
				
				
				// Fetch record and split parameters to be processed
				crRequest 		= searchRecords[i].getValue('custrecord_wtka_extsys_sync_request');
				crResourceId 	= searchRecords[i].getValue('custrecord_wtka_extsys_sync_resourceid');
				crFileId 		= searchRecords[i].getValue('custrecord_wtka_extsys_sync_file');
				crFileCounter 	= searchRecords[i].getValue('custrecord_wtka_extsys_sync_filecounter');
				crLastProcessed = searchRecords[i].getValue('custrecord_wtka_extsys_sync_lastprocess');
				crStatus 		= searchRecords[i].getValue('custrecord_wtka_extsys_sync_status');
				crTimeOutCount	= searchRecords[i].getValue('custrecord_wtka_extsys_sync_timeoutcount');
				crRequestId 	= searchRecords[i].getId();
				crRequestObj 	= JSON.parse(crRequest);
				crTimeOutCount  = (crTimeOutCount != null && crTimeOutCount.length > 0) ? crTimeOutCount : 0;
				splitParameterValues(crRequest);
				fileCounter 	= crFileCounter;
				
				processThis = crResourceId;
				
				nlapiLogExecution('audit', processThis + ': IDS FetchInventoryRecords : For Loop ('+ i +') tempObjForLog ',  ' ' + JSON.stringify(tempObjForLog));
				nlapiLogExecution('audit', processThis + ': IDS FetchInventoryRecords : from WTKA: crStatus ' +  i , ' ' + crStatus);
				
				while(crStatus == 0 || crStatus == 1)
				{
					if(getScriptTimeConsumed() < scriptMaxTimeout && nlapiGetContext().getRemainingUsage() > governanceMinUsage)
					{
						// Update record status to 1 to indicate 'In Progress'
						if (crStatus == 0)
						{
							nlapiSubmitField('customrecord_wtka_extsys_sync', crRequestId, 'custrecord_wtka_extsys_sync_status', 1, false);
							nlapiLogExecution('audit', processThis + ': IDS FetchInventoryRecords : update WTKA: crStatus ' +  i , ' ' + crStatus);
							crStatus = 1; //Set the field to In progress
						}

						
						
						
						// Get Total count of records based on the request
						var totalLoops = 0;
						try
						{
							//nlapiLogExecution('debug', 'crLastProcessed', crLastProcessed);
																				
							var tempObjForLog2 = new Object(); 
							
							tempObjForLog2.processLocationCode = processLocationCode;
							tempObjForLog2.processLocationType = processLocationType;
							tempObjForLog2.processFromDate = processFromDate;
							tempObjForLog2.crLastProcessed = crLastProcessed;
							
							nlapiLogExecution('audit', processThis + ': IDS FetchInventoryRecords : INIT: performSearch run: full ' +  i , ' ' + JSON.stringify(tempObjForLog2));
							
							
							var searchResult = performSearch(processLocationCode, processLocationType, processFromDate, crLastProcessed, 'T');
							if(searchResult != null)
							{
								
								var tempObject3 = new Object();
								
								//Extract total count and divide by 1000, which provides 'n' number of loopings to be done.
								//nlapiLogExecution('DEBUG', 'total Results', searchResult.length);
								totalLoops = searchResult[0].getValue(FinalString);
								tempObject3.results = totalLoops;
								
								//nlapiLogExecution('DEBUG', 'totalCount-Records', totalLoops);
								totalLoops = parseInt(totalLoops/1000) + 2; 
								//nlapiLogExecution('DEBUG', 'totalLoops', totalLoops);
								tempObject3.totalLoops = totalLoops;
								
								nlapiLogExecution('audit', processThis + ': IDS FetchInventoryRecords : Loop Calculation' , ' ' + JSON.stringify(tempObject3));
								
								
								
								//3. PayLoad function to append to array and submit to file.
								BuildInventoryPayLoad(totalLoops);
								nlapiLogExecution('audit', processThis + ': IDS FetchInventoryRecords : BuildInventoryPayLoad run: singles: totalLoops ' +  i , ' ' + totalLoops);
								
								if(scriptLimit)
								{
									SwitchScheduleScript(1);
									break;
								}
							}
							else
							{
								nlapiLogExecution('DEBUG', 'LENGTH', '0');
								crStatus = 2; //Completed state
								updateCustomRecord();
								fileCounter 	= 0;
								lastProcessedId = 0;
							}
						}
						catch(err)
						{
							var errorRef = err;
							if(errorRef.code == 'SSS_SEARCH_TIMEOUT')
							{
								nlapiLogExecution('DEBUG', 'Error in Processing request', crRequestId);
								crTimeOutCount++;
								if(crTimeOutCount > 3)
								{
									nlapiLogExecution('DEBUG', 'Process Aborted', err);
									crStatus = 3;
									updateCustomRecord();
									fileCounter 	= 0;
									lastProcessedId = 0;
									sendSyncMail(sendEmail, subject, body, errorRef, crRequestObj);
									break;
								}
								else
								{
									crStatus = 1; //In-progress
									updateCustomRecord();
									nlapiLogExecution('DEBUG', 'Rescheduling script', 'Process timed out and rescheduling script');
									SwitchScheduleScript(1); // 1 - Inventory Sync
								}
							}
							else
							{
								nlapiLogExecution('debug', 'Error in Processing request', crRequestId);
								nlapiLogExecution('debug', 'Error Details', err);
								crStatus = 3;
								updateCustomRecord();
								fileCounter 	= 0;
								lastProcessedId = 0;
								sendSyncMail(sendEmail, subject, body, errorRef, crRequestObj);
								break;
							}
						}
					}
					else
					{
						//Update customrecord values based on  current state
						updateCustomRecord();
						SwitchScheduleScript(1); // 1 - Inventory Sync
					}
				}
				if(scriptLimit)
				{
					scriptLimit = false;
					break;
				}
			}
		}
		else
		{
			nlapiLogExecution('DEBUG', 'PROCESS', 'No records to process. All requests have been processed');
		}
	}
	catch(fetch_err)
	{
		sendSyncMail(sendEmail, subject, body, fetch_err, '');
		nlapiLogExecution('debug', 'Fetch error', fetch_err);
	}
}
	
function splitParameterValues(dataIn)
{
	try
	{
		//Split the request parameters into respective parameters for processing
		var crRequestObj 	 = JSON.parse(dataIn);
		processFromDate 	 = crRequestObj.inventoryRequest.fromDate;
		var processLocationCodeArray = new Array();
		
		for(var i in crRequestObj.inventoryRequest.location)
		{
			switch(crRequestObj.inventoryRequest.location[i].locationType)
			{
				case 'Retail Store':
					processLocationType[i] = 1;
					break;
				case 'Administration Center':
					processLocationType[i] = 2;
					break;
				case 'Distribution Center':
					processLocationType[i] = 3;
					break;
				case 'Headquarters':
					processLocationType[i] = 4;
					break;
				case 'Region':
					processLocationType[i] = 5;
					break;
				case 'Country':
					processLocationType[i] = 6;
					break;
			}
			var locationCode = crRequestObj.inventoryRequest.location[i].locationCode;
			//if(locationCode != "" && locationCode.length > 0) processLocationCodeArray[i] = locationCode; // crRequestObj.inventoryRequest.location[i].locationCode; //IDJS 20160316
			if(!isNullOrEmpty(locationCode))  processLocationCodeArray[i] = locationCode; //IDJS 20160316           
		}
		
		if(processLocationCodeArray.length > 0)
		{
			processLocationCode  = FetchIdFromName('location', processLocationCodeArray);
			if(processLocationCode[0] == "Error")	processLocationCode = null;
		}
		else
		{
			processLocationCode = null
		}
	}
	catch(split_Err)
	{
		sendSyncMail(sendEmail, subject, body, split_Err, crRequestObj);
		nlapiLogExecution('debug', 'Split Param error', split_Err);
	}
}

function performSearch(locationId, locationType, dateValue, lastProcessed, initCall)
{
	
	nlapiLogExecution('audit', processThis + ': IDS FetchInventoryRecords : performSearch ', ' Entering' );
	
	try
	{
		//Base filters
		var filters = new Array();
		//filters.push(['type',   'anyof',  'InvtPart']); 													// Inventory Item
		//filters.push("AND");
		filters.push(['matrix', 'is', 'F']); 																// Inventory without subitem and matrix child items
		filters.push("AND");																				
		filters.push(['locationquantityonhand', 'isnotempty', null]); 			
		filters.push("AND");
		filters.push(['inventorylocation.isinactive', 'is', 'F']);
		//filters.push("AND"); //IDJS 03162016

		//filters.push(['custitem_wfx_season', 'noneof', 3,31,32,24,5,30,6,7,25]);   //IDJS 03162016    
		//filters.push(new nlobjSearchFilter('custitem_wfx_season', null, 'noneof', 3,31,32,24,5,30,6,7,25));   //IDJS 03162016

		// filters.push(['custitem_wfx_season', 'anyof', 40,34,38,39,29]); 
		
		
		filters.push("AND");
		filters.push(['inventorylocation.makeinventoryavailable', 'is', 'T']);
		filters.push("AND");	
		filters.push(['inventorylocation.custrecord_sync_external', 'is', 'T']); 			 				//20160321 //New Filter flag

		
		//if(locationId == null || locationId.length <=0)			filters.push(['inventorylocation', 'anyof', '@ALL@']); // All Locations //IDJS 03162016
		if(isNullOrEmpty(locationId)) 
		{
			filters.push("AND");
			filters.push(['inventorylocation', 'anyof', '@ALL@']); // All Locations //IDJS 03162016
		}
		else
		{
			filters.push("AND");
			filters.push(generateMultiValueFilterExpression(locationId, 'inventorylocation'));
		}
		filters.push("AND");
		
		//if(locationType == null || locationType.length <=0)		filters.push(['inventorylocation.custrecord_ra_loctype', 'anyof', 1, 3, 4]); //3 - Distribution Center, 1-Retail Store, 4-Headquarters; //IDJS 03162016
		if(isNullOrEmpty(locationType))		
		{
			filters.push(['inventorylocation.custrecord_ra_loctype', 'anyof', 1, 3, 4]); //3 - Distribution Center, 1-Retail Store, 4-Headquarters; //IDJS 03162016
		}
		else 
		{
			filters.push(generateMultiValueFilterExpression(locationType, 'inventorylocation.custrecord_ra_loctype'));
		}
		
		if(dateValue != null && dateValue != '')
		{
			//if(dateValue.match(/^\d{4}\-\d{1,2}\-\d{1,2}T\d{1,2}:\d{2}:\d{2}Z/) != null) 				 dateValue = convertDate_UTC(dateValue);
			//if(dateValue.match(/^\d{4}\-\d{1,2}\-\d{1,2}T\d{1,2}:\d{2}:\d{2}[+-]\d{1,2}:\d{2}/) != null) dateValue = convertDate_extended(dateValue);
			
			nlapiLogExecution('debug', 'IDS date for the run: UTC' , dateValue); 
			
			dateValue = moment(dateValue).format('l LT');	

			filters.push("AND");
			filters.push(['lastquantityavailablechange', 'after', dateValue]); 
			
			nlapiLogExecution('debug', 'IDS date for the run: PDT' , dateValue); 
		}
		//else Initial Inventory Sync-no date filter
		
		if(lastProcessed > 0)
		{
			filters.push("AND");
			filters.push(['formulanumeric: ABS(CONCAT({internalid}, {inventorylocation.internalid}))', "greaterthan", lastProcessed, null]); 
		}
		
		var cols = new Array();
		if(initCall == 'F')
		{
			cols.push(new nlobjSearchColumn('custrecord_ra_loctype', 				'inventoryLocation'));
			cols.push(new nlobjSearchColumn('externalid', 							'inventoryLocation'));
			cols.push(new nlobjSearchColumn('created'));
			cols.push(new nlobjSearchColumn('modified'));
			cols.push(new nlobjSearchColumn('upccode'));
			cols.push(new nlobjSearchColumn('averagecost')); 							//IDS Added to include average cost
			cols.push(new nlobjSearchColumn('locationquantityonhand')); 
			cols.push(new nlobjSearchColumn('locationquantityonorder')); 
			cols.push(new nlobjSearchColumn('locationquantitybackordered')); 
			cols.push(new nlobjSearchColumn('locationquantityintransit')); 
			cols.push(new nlobjSearchColumn('locationquantityavailable')); 
			cols.push(new nlobjSearchColumn('locationquantitycommitted')); 
			cols.push(new nlobjSearchColumn('inventorylocation'));
			cols.push(new nlobjSearchColumn('baseprice'));
			FinalString = new nlobjSearchColumn('formulanumeric').setFormula('ABS(CONCAT({internalid}, {inventorylocation.internalid}))');
		}
		else
		{
			FinalString = new nlobjSearchColumn('formulanumeric', null, 'count').setFormula('ABS(CONCAT({internalid}, {inventorylocation.internalid}))');
		}
		cols.push(FinalString.setSort()); 

		
		
		//  
		var searchResult = nlapiSearchRecord('item', 'customsearch_inv_hybris_sync', filters, cols); //IDS Change Temp
		/*var arSavedSearchResultsSalesRange = nlapiLoadSearch('item','customsearch_inv_hybris_sync');

		arSavedSearchResultsSalesRange.addColumns(cols);
		arSavedSearchResultsSalesRange.addFilters(filters);

		var resultSet = arSavedSearchResultsSalesRange.runSearch();
		var searchResult = resultSet.getResults(0, 200000);
		*/
		
		
		
		return searchResult;
	}
	catch(performSearch_err)
	{
		sendSyncMail(sendEmail, subject, body, performSearch_err, crRequestObj);
		nlapiLogExecution('debug', 'Perform Search error', performSearch_err);
		return null;
	}
}

function BuildInventoryPayLoad(loopCounter)
{
	
	nlapiLogExecution('audit', processThis + ': IDS BuildInventoryPayLoad BuildInventoryPayLoad : enter ' , ' loopCounter: ' + loopCounter );
	
	try
	{
		var finalFileContents 	= '';
		var lastProcessed 		= 0;
		var processedLoop 		= 0;
		var itemObject			= new Array();

		for(var i=0; scriptLimit != true && i < loopCounter; i++)
		{
			
			
			nlapiLogExecution('audit', processThis + ': IDS BuildInventoryPayLoad BuildInventoryPayLoad : Loop ' +  i , '  scriptLimit'  + scriptLimit );
			
			nlapiLogExecution('DEBUG', 'loopCounter', parseInt(i+1));
			var finalResult = performSearch(processLocationCode, processLocationType, processFromDate, crLastProcessed, 'F');

			if(finalResult != null)
			{
				//nlapiLogExecution('DEBUG', 'Total Results - subresults', finalResult.length);
				for(var j=0; j < finalResult.length; j++)
				{
					var qtyOnHand			= quantityParse(finalResult[j].getValue('locationquantityonhand'), 		'int'); 
					var qtyBackOrder		= quantityParse(finalResult[j].getValue('locationquantitybackordered'), 'int'); 
					var qtyInTransit		= quantityParse(finalResult[j].getValue('locationquantityintransit'), 	'int'); 
					var qtyAvailableToSell	= quantityParse(finalResult[j].getValue('locationquantityavailable'), 	'int'); 
					var qtyCommitted		= quantityParse(finalResult[j].getValue('locationquantitycommitted'), 	'int'); 
					var amtAverageCost		= quantityParse(finalResult[j].getValue('averagecost'), 				'float');  // IDS 020116 Changed locationaveragecost to Average Cost
					var amtCurrentPrice		= quantityParse(finalResult[j].getValue('baseprice'), 					'float'); 
					var amtPromoPrice		= quantityParse(finalResult[j].getValue('baseprice'), 					'float'); 
					
					var locationType		= readValue(finalResult[j].getText('custrecord_ra_loctype', 'inventoryLocation'));
					switch(locationType)
					{
						case 'Retail Store':
							locationType = 'Retail Shops';
							break;
						case 'Distribution Center':
							locationType = 'Distribution Centres';
							break;
						case 'Headquarters':
							locationType = 'Head Office';
							break;
					}
					
					var newLine 									= new Object();
					newLine.inventory 								= new Object();
					newLine.inventory.inventoryId 					= finalResult[j].getId();
					newLine.inventory.qtyOnHand 					= qtyOnHand;
					newLine.inventory.qtyBackOrder 					= qtyBackOrder;
					newLine.inventory.qtyInTransit 					= qtyInTransit;
					newLine.inventory.qtyAvailableToSell			= qtyAvailableToSell;
					newLine.inventory.qtyCommitted 					= qtyCommitted;
					newLine.inventory.qtyPreOrder 					= null; //Set to null as field is for future-proofing
					newLine.inventory.amtAverageCost				= amtAverageCost;
					newLine.inventory.createdTimeStamp				= readValue(finalResult[j].getValue('created'));
					newLine.inventory.lastUpdatedTimeStamp			= readValue(finalResult[j].getValue('modified'));
					
					newLine.inventory.product 						= new Object();
					newLine.inventory.product.sku 					= new Object();
					newLine.inventory.product.sku.SKUId				= readValue(finalResult[j].getValue('upccode'));
					
					newLine.inventory.location 						= new Object();
					newLine.inventory.location.locationId			= readValue(finalResult[j].getValue('inventoryLocation'));
					newLine.inventory.location.locationType 		= locationType;
					newLine.inventory.location.locationCode 		= readValue(finalResult[j].getValue('externalid', 'inventoryLocation'));
					
					itemObject.push(newLine);
					
					lastProcessedId = finalResult[j].getValue(FinalString); 
					lastProcessed 	= lastProcessedId;
					crLastProcessed = lastProcessedId;
					if(finalResult.length < 1000) counterFlag = true;
					if(getScriptTimeConsumed() > scriptMaxTimeout || nlapiGetContext().getRemainingUsage() < governanceMinUsage)
					{
						//nlapiLogExecution('debug', 'BUILD - Time Consumed', getScriptTimeConsumed());
						counterFlag = true;
						scriptLimit = true;
						break;
					}
				}
				processedLoop++;
			}		
			else
			{
				nlapiLogExecution('DEBUG', 'No results', 'No contents');
				crStatus = 2;
				updateCustomRecord();
				fileCounter 	= 0;
				lastProcessedId = 0;
				//Reset globals before breaking
				crRequest = 0, crResourceId = '', crFileId = 0, crFileCounter = 0, crLastProcessed = 0, crRequestId = '', crTimeOutCount = 0;
				break;
			}
			
			
			nlapiLogExecution('audit', processThis + ': IDS BuildInventoryPayLoad BuildInventoryPayLoad : processedLoop == processLoopCount || counterFlag' , 
			
			' processedLoop:' + processedLoop +  
			' processLoopCount' + processLoopCount + 
			' counterFlag' + counterFlag 
			
			);
			
			if(processedLoop == processLoopCount || counterFlag)
			{
				if(itemObject.length > 0)
				{
					//nlapiLogExecution('DEBUG', 'Inside file creation loop', 'Inside file creation loop');
					//nlapiLogExecution('DEBUG', 'Governance Limit-Before File Creation', nlapiGetContext().getRemainingUsage());
					
					nlapiLogExecution('audit', processThis + ': IDS BuildInventoryPayLoad BuildInventoryPayLoad : write file',i); 
					
					var fileUpdate 	= nlapiLoadFile(folderName + '/' + crResourceId);
					var fileContent = fileUpdate.getValue();

					var newFileName = fileUpdate.getName();	
					if(fileCounter > 0) newFileName += '_' + parseInt(fileCounter);
					
					var updatedFile = nlapiCreateFile(newFileName, 'PLAINTEXT', JSON.stringify(itemObject));

					updatedFile.setFolder(fileUpdate.getFolder());
					var idupdate = nlapiSubmitFile(updatedFile);
					nlapiLogExecution('DEBUG', 'Governance Limit-After File Creation', nlapiGetContext().getRemainingUsage());
					fileCounter++;

					//Update CustomRecord
					updateCustomRecord();
					
					// Reset Values
					itemObject 		= new Array();
					processedLoop 	= 0;
					counterFlag 	= false;
				}
			}
			if(scriptLimit)	break;
		}
	}
	catch(build_err)
	{
		sendSyncMail(sendEmail, subject, body, build_err, crRequestObj);
		nlapiLogExecution('debug', 'Build error', build_err);
	}
}
function getScriptTimeConsumed()
{
	var currentTime = new Date();
	var timeDiff 	= (currentTime - script_entry)/1000; //in seconds
	return timeDiff;
}