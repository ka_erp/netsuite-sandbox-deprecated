//Suitelet
PO_Aggregator = {
    context: nlapiGetContext(),
    filterExpression: null,
    setFilterExpression: function () {
        //nlapiLogExecution('DEBUG', 'setFilterExpression', 'IN');
        if (this.isNotBlank(request.getParameter('custpage_vendor'))) {
            //nlapiLogExecution('DEBUG', 'setFilterExpression', 'I got a Vendor!');
            this.filterExpression = [['type', 'anyof', 'PurchOrd'], 'AND', ['mainline', 'is', 'T'], 'AND', ['status', 'anyof', 'PurchOrd:A', 'PurchOrd:B'], 'AND'];
            this.filterExpression.push(['entity', 'anyof', request.getParameter('custpage_vendor')]);

            if (this.isNotBlank(request.getParameter('custpage_subsidiary'))) {
                this.filterExpression.push('AND');
                this.filterExpression.push(['subsidiary', 'anyof', request.getParameter('custpage_subsidiary')]);
            }

            if (this.isNotBlank(request.getParameter('custpage_season'))) {
                this.filterExpression.push('AND');
                var seasonFilter = [];
                seasonFilter.push('custbody_ka_season');
                seasonFilter.push('anyof');
                //Clean the comma-delimited parameter field, convert it to an array, then convert all the array values to string
                var fieldValues = request.getParameter('custpage_season').replace(/[^\w\s]/gi, ',').split(',').map(String);
                for (var i = 0, seasonLength = fieldValues.length; i < seasonLength; i = i + 1) {
                    seasonFilter.push(fieldValues[i]);
                }
                this.filterExpression.push(seasonFilter);
            }

            if (this.isNotBlank(request.getParameter('wasexported')) && request.getParameter('wasexported').toString() !== 'E') {
                this.filterExpression.push('AND');
                this.filterExpression.push(['custbody_po_exported', 'is', request.getParameter('wasexported')]);
            }
        }
        else {
            //nlapiLogExecution('DEBUG', 'setFilterExpression', 'I got nuttin');
            this.filterExpression = null;
        }
        //nlapiLogExecution('DEBUG', 'setFilterExpression', 'OUT');
    },
    getSearchResults: function () {
        var search, searchResultSet, searchResultsArray;
        search = nlapiLoadSearch(null, 'customsearch_po_aggregator_search');
        search.setFilterExpression(this.filterExpression);
        searchResultSet = search.runSearch();
        searchResultsArray = searchResultSet.getResults(0, 1000);
        //nlapiLogExecution('DEBUG', 'searchResults', 'searchResultsArray: ' + searchResultsArray.length);
        return searchResultsArray;
    },
    isBlank: function (s) {
        return s === undefined || s === null || s === '' || s.length < 1;
    },
    isNotBlank: function (s) {
        return !this.isBlank(s);
    },
    isNumber: function (n) {
        return this.isNotBlank(n) && !isNaN(parseInt(n, 10)) && isFinite(n);
    },
    encodeHTML: function (html) {
        return html.replace(/<\?xml version="1\.0"\?>/g, '')
            .replace(/<!DOCTYPE pdf PUBLIC "-\/\/big\.faceless\.org\/\/report" "report-1\.1\.dtd">/g, '')
            .replace(/&c/g, '&amp;c')
            .replace(/&h/g, '&amp;h')
            .replace(/&nbsp;/g, '&#160;')
            .replace(/cellspacing=\"\d\"/g, '');

    },
    getPDFXML: function (purchaseOrderId) {
        //nlapiLogExecution('DEBUG', 'getPDFXML', 'purchaseOrderId: ' + purchaseOrderId.toString());
        var renderer = nlapiCreateTemplateRenderer();
        renderer.setTemplate(nlapiLoadFile(this.pdfTemplate).getValue());
        renderer.addRecord('record', nlapiLoadRecord('purchaseorder', purchaseOrderId));
        //nlapiLogExecution('DEBUG', 'getPDFXML', 'addRecord');
        var s_xml = renderer.renderToString();
        nlapiSubmitField('purchaseorder', purchaseOrderId, 'custbody_po_exported', 'T');
        //nlapiLogExecution('DEBUG', 'getPDFXML', 'After EncodeHTML: ' + this.encodeHTML(s_xml));
        return this.encodeHTML(s_xml);
    },
    round: function (value, decimals) {
        return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
    },
    form: null,
    subList: null,
    suitelet: function (request, response) {
        'use strict';
        var createPDF;

        if (this.isNotBlank(request.getParameter('custpage_createpdf_cbx')) && request.getParameter('custpage_createpdf_cbx').toString() === 'T') {
            var poIDs = '';
            for (var i = 1, poCount = parseInt(request.getLineItemCount('sublist'), 10); i <= poCount; i = i + 1) {
                if (request.getLineItemValue('sublist', 'choosen', i).toString() === 'T') {
                    poIDs += request.getLineItemValue('sublist', 'id', i) + ',';
                }
            }
            //Strip off last comma
            poIDs = poIDs.replace(/,\s*$/, '');
            //nlapiLogExecution('DEBUG', 'suitelet - PO string', poIDs);

            if (poIDs.length > 2) {
                //nlapiLogExecution('DEBUG', 'suitelet', 'Sending a request to the Scheduler!');
                var scheduleResponse = nlapiScheduleScript('customscript_po_aggreagator_ss', 'customdeploy_po_aggreagator_ss', {
                    'custscript_pdftemplate': this.context.getSetting('SCRIPT', 'custscript_po_aggregator_pdftemplate'),
                    'custscript_pdffolder': this.context.getSetting('SCRIPT', 'custscript_po_aggregator_pdffolder'),
                    'custscript_vendor': request.getParameter('custpage_vendor'),
                    'custscript_currentuser': nlapiGetUser(),
                    'custscript_pdfids': poIDs
                });
                //nlapiLogExecution('DEBUG', 'suitelet', 'Scheduler response is: ' + scheduleResponse);
                nlapiSetRedirectURL('SUITELET', 'customscript_po_aggreagator_suitelet', 'customdeploy_po_aggregator');
            }
        } else {
            this.form = nlapiCreateForm('PO Exporter');
            createPDF = this.form.addField('custpage_createpdf_cbx', 'checkbox', 'CreatePDF', null).setDisplayType('hidden');
            if (this.isNotBlank(request.getParameter('custpage_createpdf_cbx'))) {
                createPDF.setDefaultValue(request.getParameter('custpage_createpdf_cbx'));
            }
            var vendorField = this.form.addField('custpage_vendor', 'select', 'Vendor', 'vendor', null).setMandatory(true);
            if (this.isNotBlank(request.getParameter('custpage_vendor'))) {
                vendorField.setDefaultValue(request.getParameter('custpage_vendor'));
            }
            var sudsidiaryField = this.form.addField('custpage_subsidiary', 'select', 'Subsidiary', 'subsidiary', null);
            if (this.isNotBlank(request.getParameter('custpage_subsidiary'))) {
                sudsidiaryField.setDefaultValue(request.getParameter('custpage_subsidiary'));
            } else {
                //Default to the Canada Subsidiary
                sudsidiaryField.setDefaultValue(3);
            }

            this.form.addField('wasexportedlabel', 'label', 'Include Exported?').setLayoutType('startrow');
            this.form.addField('wasexported', 'radio', 'No', 'F').setLayoutType('midrow');
            this.form.addField('wasexported', 'radio', 'Yes', 'T').setLayoutType('midrow');
            this.form.addField('wasexported', 'radio', 'Either', 'E').setLayoutType('endrow');
            if (this.isNotBlank(request.getParameter('wasexported'))) {
                this.form.getField('wasexported', request.getParameter('wasexported')).setDefaultValue(request.getParameter('wasexported'));
            } else {
                this.form.getField('wasexported', 'F').setDefaultValue('F');
            }

            var seasonField = this.form.addField('custpage_season', 'multiselect', 'Seasons', 'customrecord_wfx_season', null);
            if (this.isNotBlank(request.getParameter('custpage_season'))) {
                var fieldValues = request.getParameter('custpage_season').replace(/[^\w\s]/gi, ',').split(',').map(String);
                seasonField.setDefaultValue(fieldValues);
            }

            this.form.addField('custpage_createpdf_message', 'inlinehtml', '', null).setDisplayType('inline').setDefaultValue('In order to alter the PDF structure: <ol><li>update an Advanced PDF\/HTML template to your liking<\/li><li>view the source code and copy it over into the text file found <a target=\"_blank\" href=\"\/app\/common\/media\/mediaitem.nl?id=' + this.context.getSetting('SCRIPT', 'custscript_po_aggregator_pdftemplate') + '\">here<\/a><\/li><\/ol>');

            this.subList = this.form.addSubList('sublist', 'list', 'Purchase Orders');
            this.subList.addField('choosen', 'checkbox', 'Include');
            this.subList.addField('id', 'integer', 'id').setDisplayType('hidden');
            this.subList.addField('trandate', 'date', 'Date');
            //
            this.subList.addField('duedate','date', 'Last modified date');
            this.subList.addField('tranid', 'text', 'PO#', 'transaction');
            //trying adding linkable Tranid
            //this.subList.addField('formulatext', 'text', 'PO#', null).setDisplayType('inline');
            /* '<a href="/app/accounting/transactions/purchord.nl?id=54196">' ||{number}||'</a>' */
            this.subList.addField('entity', 'select', 'Vendor', 'vendor').setDisplayType('inline');
            this.subList.addField('custbody_ka_season', 'select', 'Season', 'customrecord_wfx_season').setDisplayType('inline');
            this.subList.addField('memo', 'text', 'Memo');
            //Adding this via formula text this.subList.addField('statusref', 'select', 'Status', 'statusref').setDisplayType('inline')
            this.subList.addField('formulatext', 'text', 'Status', null).setDisplayType('inline');
            this.subList.addField('subsidiary', 'select', 'Subsidiary', 'subsidiary').setDisplayType('inline');
            this.subList.addField('custbody_po_exported', 'checkbox', 'Was Exported?').setDisplayType('inline');
            this.subList.addMarkAllButtons();
            this.form.addSubmitButton('Refresh Search');
            this.form.addButton('custpage_createpdf', 'Produce Purchase Order Export', 'PO_Aggregator.client.createPDF()');
            this.setFilterExpression();
            if (this.filterExpression) {
                //nlapiLogExecution('DEBUG', 'Ready to search!', 'Here we go!');
                this.subList.setLineItemValues(this.getSearchResults());
                if (parseInt(this.subList.getLineItemCount(), 10) > 0) {
                    this.subList.setLabel(this.subList.getLineItemCount() + ' Purchase Orders found!');
                } else {
                    this.subList.setLabel('0 Purchase Orders found.  Please refine criteria and Refresh Search');
                }
            } else {
                this.subList.setLabel('Set criteria and Refresh Search');
            }

            this.form.setScript('customscript_po_aggreagator_cs');
            response.writePage(this.form);
        }
    },
    client: {
        createPDF: function () {
            if (PO_Aggregator.isNotBlank(nlapiGetFieldValue('custpage_vendor'))) {
                alert('Creating Aggregated PDF!  You will be emailed the PDF when its ready');
                nlapiSetFieldValue('custpage_createpdf_cbx', 'T');
                NLDoMainFormButtonAction('submitter');
            } else {
                alert('Pick a Vendor!');
            }
        }
    },
    pdfTemplate: null,
    listOfPOs: null,
    currentUser: null,
    pdfFolder: null,
    aggregatePDFs: function (type) {
        this.pdfTemplate = this.context.getSetting('SCRIPT', 'custscript_pdftemplate');
        this.pdfFolder = parseInt(this.context.getSetting('SCRIPT', 'custscript_pdffolder'), 10);
        this.vendor = this.context.getSetting('SCRIPT', 'custscript_vendor');
        this.currentUser = this.context.getSetting('SCRIPT', 'custscript_currentuser');
        this.listOfPOs = this.context.getSetting('SCRIPT', 'custscript_pdfids').toString().split(',');
        var poXMLString = '';
        this.context.setPercentComplete(0.00);

        for (var i = 0, len = this.listOfPOs.length; i < len; i++) {
            poXMLString += this.getPDFXML(parseInt(this.listOfPOs[i], 10));
            //this loops are 30 Units per

            //We need around 100 Units to complete the operations
            if (parseInt(this.context.getRemainingUsage(), 10) < 100) {
                //nlapiLogExecution('DEBUG', 'aggregatePDFs', 'Attempting to Yield Script!');
                nlapiYieldScript();
            }
            this.context.setPercentComplete(this.round(((100 * i) / len), 2));
            this.context.getPercentComplete();
            //nlapiLogExecution('DEBUG', 'aggregatePDFs', 'units remaining: ' + this.context.getRemainingUsage() + ' after ' + i + ' completed.');
        }

        if (this.isNotBlank(poXMLString)) {
            poXMLString = '<pdfset>' + poXMLString + '</\pdfset>';
            //nlapiLogExecution('DEBUG', 'aggregatePDFs', 'pdfset: ' + poXMLString);
            //nlapiSendEmail(65656, 65656, 'Aggregated POs - pdfset', poXMLString, null, null, /*{'entity': this.vendor}*/null, null, false, false, null);
            var o_file = nlapiXMLToPDF(poXMLString); //10 Units!
            o_file.setFolder(this.pdfFolder);
            var fileid = nlapiSubmitFile(o_file); //20 Units

            //This call uses 10 Units
            nlapiSendEmail(this.currentUser, this.currentUser, 'Exported PO\'s', 'See attached exported PO\'s', null, null, /*{'entity': this.vendor}*/null, nlapiLoadFile(fileid), false, false, null);
        }
    }
};