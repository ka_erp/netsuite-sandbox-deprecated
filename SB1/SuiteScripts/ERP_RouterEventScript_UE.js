/**
 *	File Name		:	ERP_RouterEventScript_UE.js
 *	Function		:	
 * 	Authors			:	christopher.neal@kitandace.com
 *	Company			:	Kit & Ace
 *	Release Dates	:	
 * 	Current Version	:	
 *
 *	Description: 	
 *
**/

/*
*	name: afterSubmit_updateRecord
*	descr: 
*
*	author: christopher.neal@kitandace.com
*	@param: {type} type of operation on the record
*
*/
function afterSubmit_updateRecord(type){

	nlapiLogExecution('DEBUG', 'type', type);

	if (type == 'create'){
		makeInitialLog();
	}
}


function makeInitialLog(){

	var curRouterId = nlapiGetRecordId();
	var transactionNumver = nlapiGetFieldValue();

	var logRecord = nlapiCreateRecord('customrecord_integrations_log');	
	logRecord.nlapiSetFieldValue('custrecord_intlog_transaction_number', nlapiGetFieldValue(custrecord_router_transaction_number));
	logRecord.nlapiSetFieldValue('custrecord_intlog_in_out', nlapiGetFieldValue(custrecord_router_in_out));
	logRecord.nlapiSetFieldValue('custrecord_intlog_record_type', nlapiGetFieldValue(custrecord_router_record_type));
	logRecord.nlapiSetFieldValue('custrecord_intlog_index', 1);
	logRecord.nlapiSetFieldValue('custrecord_intlog_log_message', 'Created log');

	var startDate = nlapiDateToString(new Date, 'datetime');
	var endDate = nlapiDateToString(new Date, 'datetime');

	logRecord.nlapiSetFieldValue('custrecord_intlog_start', startDate);
	logRecord.nlapiSetFieldValue('custrecord_intlog_end', endDate);

	var logRecordId = nlapiSubmitRecord(logRecord);

	nlapiLogExecution('DEBUG', 'Created Log', 'Router: ' + curRouterId + ', Log: ' + logRecordId);
}







