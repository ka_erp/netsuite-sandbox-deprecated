/**
 *	File Name		:	ERP_ProductSync_SS.js
 *	Function		:	
 * 	Authors			:	christopher.neal@kitandace.com
 *	Company			:	Kit & Ace
 *	Release Dates	:	
 * 	Current Version	:	
 *
 *	Description: 	Scheduled script to synchronize products with ESB/3PL. Fetch all product records to
 *					be sent.
 *
**/

{
	var governanceMinUsage  = 100;
	var fileCounter   		= 0;
	var lastProcessedId  	= 0;
	var counterFlag   		= 'F';
	var scriptLimit			= false;
	var sendEmail 			= true;  //flag whether to send an email
	var crRequest, crResourceId, crFileId, crFileCounter, crLastProcessed, crStatus, crRequestId, processFromDate;
	var folderName = 'External Sync Dev';
	var recordLimit			= 950;
	var ccList = null; //['']; //Enter CC Addresses as array
	var toList = ['3PL_eComm_Integration@kitandace.com'];
}

var SYNC_TYPE = {};
SYNC_TYPE.PRODUCT = 1;
SYNC_TYPE.TRANSFER = 4;
SYNC_TYPE.ITEMRECEIPT = 5;

var WebFxSeasonWhitelist = [40,34,38,39,29];


/*
*	name: FetchProductRecords
*	descr:  
*
*	author: christopher.neal@kitandace.com (mostly taken from Wipro)
*
*/
function FetchProductRecords(){
	/*** Batch Statuses:
	0 = not started / In Queue
	1 = In Progress
	2 = Completed
	3 = Error ***/

	nlapiLogExecution('debug', 'ERP_ProductSync_SS.js', 'Initiating product sync');

	//Fetch data from custom record (customrecord_external_sync_obj) based on status 0 and 1
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_extsync_status_id', null, 'lessthan', 2));
	filters.push(new nlobjSearchFilter('custrecord_extsync_type', 	 null, 'is', SYNC_TYPE.PRODUCT)); //Product Sync

	//Fetch records which are in Progress first and then by Internal id
	var cols = new Array();
	cols.push(new nlobjSearchColumn('custrecord_extsync_request'));
	cols.push(new nlobjSearchColumn('custrecord_extsync_resourceid'));
	cols.push(new nlobjSearchColumn('custrecord_extsync_file'));
	cols.push(new nlobjSearchColumn('custrecord_extsync_filecounter'));
	cols.push(new nlobjSearchColumn('custrecord_extsync_lastprocess'));
	cols.push(new nlobjSearchColumn('custrecord_extsync_timeoutcount'));
	cols.push(new nlobjSearchColumn('custrecord_extsync_status_id').setSort(true)); //Fetch records which are in Progress first and then by Internal id
	cols.push(new nlobjSearchColumn('internalid').setSort());

	var searchRecords = nlapiSearchRecord('customrecord_external_sync_obj', null, filters, cols);

	if (searchRecords != null){

		nlapiLogExecution('debug', 'searchRecords-Fetch', searchRecords.length);
		for(var i = 0; nlapiGetContext().getRemainingUsage() > governanceMinUsage && searchRecords != null && i < searchRecords.length ; i++){
			
			// Fetch record and split parameters to be processed
			crRequest 		= searchRecords[i].getValue('custrecord_extsync_request');
			crResourceId 	= searchRecords[i].getValue('custrecord_extsync_resourceid');
			crFileId 		= searchRecords[i].getValue('custrecord_extsync_file');
			crFileCounter 	= searchRecords[i].getValue('custrecord_extsync_filecounter');
			crLastProcessed = searchRecords[i].getValue('custrecord_extsync_lastprocess');
			crStatus 		= searchRecords[i].getValue('custrecord_extsync_status_id');
			crTimeOutCount	= searchRecords[i].getValue('custrecord_extsync_timeoutcount');
			crRequestId 	= searchRecords[i].getId();

			crTimeOutCount  = (crTimeOutCount != null && crTimeOutCount.length > 0) ? crTimeOutCount : 0;
			var crRequestObj = JSON.parse(crRequest);
			processFromDate = crRequestObj.productRequest.fromDate;
			
			fileCounter = crFileCounter;
			
			while(crStatus == 0 || crStatus == 1){
				if(nlapiGetContext().getRemainingUsage() > governanceMinUsage){
					
					// Update record status to 1 to indicate 'In Progress'
					if (crStatus == 0){
						nlapiSubmitField('customrecord_external_sync_obj', crRequestId, 'custrecord_extsync_status_id', 1, false);
						crStatus = 1; //Set the field to In progress
					}

					// Get Total count of records based on the request
					var totalLoops = 0;
					try{
						nlapiLogExecution('debug', 'crLastProcessed', crLastProcessed);
						var searchResult = performSearch(processFromDate, crLastProcessed, 'T');
						if(searchResult != null){updateCustomRecord

							//Extract total count and divide by 1000, which provides 'n' number of loops to be done.
							nlapiLogExecution('DEBUG', 'total Results', searchResult.length);
							totalLoops = searchResult[0].getValue('internalid', null, 'count');
							nlapiLogExecution('DEBUG', 'totalLoops - ID', totalLoops); 
							totalLoops = parseInt(totalLoops/1000) + 2; 
							nlapiLogExecution('DEBUG', 'totalLoops', totalLoops);
							
							//3. PayLoad function to append to array and submit to file.
							BuildProductPayLoad(totalLoops);
							if(scriptLimit){
								initiateNetsuiteToTigersProductSync();
								break;
							}
						} else {
							nlapiLogExecution('DEBUG', 'LENGTH', '0');
							crStatus = 2; //Completed state
							updateCustomRecord();
							fileCounter 	= 0;
							lastProcessedId = 0;
						}
					} catch(err) {
						nlapiLogExecution('debug', 'Error in Processing request', crRequestId);
						nlapiLogExecution('debug', 'Error Details', err);
						crStatus = 3;
						updateCustomRecord();
						fileCounter 	= 0;
						lastProcessedId = 0;
						var subject = 'Product Synchronization failure';
						var body = 'Hello,<br><br>';
						body += 'Product Synchronization failed in NetSuite due to below error.<br>';
						body += '<br><b>Error Details: </b><br>' + JSON.stringify(err);
						body += '<br><br><br><br><br><br><br>Thanks';
						body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
						if(sendEmail){
							//nlapiSendEmail(243423, toList, subject, body, ccList); //don't do this for now
							nlapiLogExecution('debug', 'Send email', body);
						}	
						break;
					}
				}		
			}

			if(scriptLimit) {
				scriptLimit = false;
				break;
			}
		}
	} else {
		nlapiLogExecution('DEBUG', 'PROCESS', 'No records to process. All requests have been processed');
	}

	nlapiLogExecution('debug', 'ERP_ProductSync_SS.js', 'Done product sync');
}


/*
*	name: performSearch
*	descr: Get the Item records that need to be synced to the ESB/3PL
*
*	author: christopher.neal@kitandace.com (mostly taken from Wipro)
*	@param: {Date} dateValue (e.g. 2016-03-17T22:07:00Z)
*	@param: {id} lastProcessed - ID of the last processed record
*	@param: {char} initCall - 'T': first time the saved search is being called, 'F': otherwise
*	@return: 
*
*/
function performSearch(dateValue, lastProcessed, initCall){
	
	nlapiLogExecution("AUDIT", "func: PerformSearch", "date " + dateValue);
	
	//Base filters
	var filters = new Array();
	filters.push(new nlobjSearchFilter('type', 	 null, 'anyof',  'InvtPart')); 
	filters.push(new nlobjSearchFilter('parent', null, 'noneof', '@NONE@')); 
	filters.push(new nlobjSearchFilter('custitem_wfx_season', null, 'anyof', WebFxSeasonWhitelist));
	
	if(dateValue != null && dateValue != '')
	{
		newDateValue = moment(dateValue).format('l LT');
		filters.push(new nlobjSearchFilter('modified', null, 'after', newDateValue)); 
	}
	//else Initial Product Sync-no date filter
	
	if(lastProcessed > 0){
		filters.push(new nlobjSearchFilter('internalidnumber', 	null, 'greaterthan', lastProcessed)); 
	}
	
	var cols = new Array();
	if(initCall == 'F'){
		cols.push(new nlobjSearchColumn('internalid').setSort());
		cols.push(new nlobjSearchColumn('itemid'));
		cols.push(new nlobjSearchColumn('upccode'));
		cols.push(new nlobjSearchColumn('displayname'));
		cols.push(new nlobjSearchColumn('custitem_prewfxsize')); 
		cols.push(new nlobjSearchColumn('custitem_ka_item_style_description')); 
		cols.push(new nlobjSearchColumn('custitem_wfx_line')); 
		cols.push(new nlobjSearchColumn('custitem_wfx_type'));
		cols.push(new nlobjSearchColumn('custitem_wfx_gender'));  
		cols.push(new nlobjSearchColumn('custitem_wfx_uom'));
		cols.push(new nlobjSearchColumn('custitem_ka_item_master_style'));
		cols.push(new nlobjSearchColumn('custitem_wfx_season'));
		cols.push(new nlobjSearchColumn('custitem_wfx_division'));
		cols.push(new nlobjSearchColumn('custitem_color_version_code'));
		cols.push(new nlobjSearchColumn('custitem_wfx_colorname'));
		cols.push(new nlobjSearchColumn('custitem_wfx_color_version'));
		cols.push(new nlobjSearchColumn('custitem_wfx_color_code'));
		cols.push(new nlobjSearchColumn('custitem_wfx_color_reference'));
		cols.push(new nlobjSearchColumn('custitem_wfx_color_group_code'));
		cols.push(new nlobjSearchColumn('custitem_wfx_color_group'));
	} else {
		cols.push(new nlobjSearchColumn('internalid', null, 'count'));
	}

	var searchResult = nlapiSearchRecord('item', null, filters, cols);
	nlapiLogExecution("DEBUG", "performSearch: searchResult.length", searchResult.length);

	return searchResult;
}

/*
*	name: initiateProductSync
*	descr:  Start scheduled script to initiate product sync.
*
*	author: christopher.neal@kitandace.com (mostly taken from Wipro)
*/
function initiateNetsuiteToTigersProductSync(){
	var status = nlapiScheduleScript('customscript_erp_productsync_ss', 'customdeploy_erp_productsync_ss');
	nlapiLogExecution('debug', 'Script', status);
}


/*
*	name: BuildProductPayLoad
*	descr:  Iteratively create a file for the products to be synced.		
*
*	author: christopher.neal@kitandace.com (mostly taken from Wipro)
*/
function BuildProductPayLoad(loopCounter)
{
	var finalFileContents 	= '';
	var lastProcessed 		= 0;
	var recordCounter 		= 0;
	var productObject		= new Array();

	for(var i=0; scriptLimit != true && i < loopCounter; i++)
	{
		nlapiLogExecution('DEBUG', 'loopCounter', parseInt(i+1));
		
		var finalResult = performSearch(processFromDate, crLastProcessed, 'F');
	
		if(finalResult != null)
		{
			nlapiLogExecution('DEBUG', 'Total Results - subresults', finalResult.length);
			for(var j=0; j < finalResult.length && recordCounter < recordLimit; j++)
			{
				var productSync 					= new Object();
				productSync.SKUId 					= finalResult[j].getValue('upccode');
				productSync.SKUCode 				= finalResult[j].getValue('itemid').split(':')[1];
				productSync.SKUName 				= finalResult[j].getValue('displayname');
				productSync.sizeCode 				= finalResult[j].getText('custitem_prewfxsize');
				productSync.sizeDescription 		= "";
				productSync.styleDescription 		= finalResult[j].getValue('custitem_ka_item_style_description');
				productSync.styleStatus 			= "New"; //ENUM
				productSync.inheritanceDescription 	= "Child"; //ENUM
				productSync.styleColourVersionName 	= finalResult[j].getValue('custitem_wfx_color_version'); //ENUM
				productSync.fitAttributeDescription = "";
				productSync.technologyDescription 	= "";
				// productSync.fabricationDescription 	= "";
				productSync.influencerDescription 	= "";
				productSync.climateDescription		= "";
				productSync.productStory 			= "";
				productSync.productManager 			= ""; 
				productSync.productDesigner 		= "";
				productSync.productDeveloper 		= "";
				productSync.productPatternMaker 	= "";
				productSync.lineName 				= finalResult[j].getText('custitem_wfx_line'); //ENUM
				productSync.typeName 				= finalResult[j].getText('custitem_wfx_type'); //ENUM
				productSync.genderDescription 		=  finalResult[j].getText('custitem_wfx_gender'); 
				productSync.uom 					= finalResult[j].getText('custitem_wfx_uom'); //ENUM
				productSync.avgCost 				= 0; //quantityParse(finalResult[j].getValue('averagecost'));
				
				productSync.masterStyle 			= new Object();
				productSync.masterStyle.code 		= finalResult[j].getValue('custitem_ka_item_master_style'); 
				productSync.masterStyle.name 		= finalResult[j].getValue('custitem_ka_item_master_style');  
				
				productSync.masterStyle.season 		= new Object();
				productSync.masterStyle.season.code = finalResult[j].getValue('custitem_wfx_season');
				productSync.masterStyle.season.name = finalResult[j].getText('custitem_wfx_season');
				
				productSync.masterStyle.productBrand 	  = new Object();
				productSync.masterStyle.productBrand.code = "";
				productSync.masterStyle.productBrand.name = "";
				
				productSync.masterStyle.productDivision 	 = new Object();
				productSync.masterStyle.productDivision.code = finalResult[j].getValue('custitem_wfx_division'); 

				productSync.masterStyle.productDivision.name = finalResult[j].getValue('custitem_wfx_division');
				
				productSync.masterStyle.productDepartment 	   = new Object();
				productSync.masterStyle.productDepartment.code = ""; 
				productSync.masterStyle.productDepartment.name = ""; 
				
				productSync.masterStyle.productClass 	  = new Object();
				productSync.masterStyle.productClass.code = "";
				productSync.masterStyle.productClass.name = "";
				
				productSync.masterStyle.productSubClass 	 = new Object();
				productSync.masterStyle.productSubClass.code = ""; 
				productSync.masterStyle.productSubClass.name = ""; 
				
				productSync.season 		= new Object();
				productSync.season.code = finalResult[j].getValue('custitem_wfx_season');
				productSync.season.name = finalResult[j].getText('custitem_wfx_season');
				
				productSync.style 	   = new Object();
				productSync.style.code = "";  
				productSync.style.name = "";
				
				productSync.style.season 	  = new Object();
				productSync.style.season.code = "";
				productSync.style.season.name = "" ;
				
				productSync.style.styleColours 			 = new Array();
				productSync.style.styleColours[0] 		 = new Object();
				productSync.style.styleColours[0].code 	 = finalResult[j].getValue('custitem_color_version_code'); 
				productSync.style.styleColours[0].name 	 = finalResult[j].getValue('custitem_wfx_colorname');
				productSync.style.styleColours[0].version = finalResult[j].getValue('custitem_wfx_color_version');

				productSync.vendors = getVendorList(finalResult[j].getId());
				
				productSync.colourWay 				  = new Object(); 
				productSync.colourWay.code 			  = finalResult[j].getValue('custitem_wfx_color_code');
				productSync.colourWay.name 			  = finalResult[j].getValue('custitem_wfx_colorname');
				productSync.colourWay.version 		  = finalResult[j].getValue('custitem_wfx_color_version'); //Number
				productSync.colourWay.type 			  = "";  
				productSync.colourWay.colourReference = finalResult[j].getValue('custitem_wfx_color_reference'); //Number
				
				productSync.colourWay.colourGroup 	   = new Object(); 
				productSync.colourWay.colourGroup.code = finalResult[j].getValue('custitem_wfx_color_group_code');
				productSync.colourWay.colourGroup.name = finalResult[j].getValue('custitem_wfx_color_group');
				
				productSync.colourWay.colourWaySubGroup 	 = new Object();  
				productSync.colourWay.colourWaySubGroup.code = "";  
				productSync.colourWay.colourWaySubGroup.name = "";  

				productSync.price  = getPriceList(finalResult[j].getId());
				
				productObject.push(productSync);
				recordCounter++;
				
				lastProcessedId = finalResult[j].getValue('internalid'); 
				lastProcessed 	= lastProcessedId;
				crLastProcessed = lastProcessedId;
				
				if(finalResult.length < 1000){
					counterFlag = 'T';
				}

				if(nlapiGetContext().getRemainingUsage() < governanceMinUsage){
					counterFlag = 'T';
					scriptLimit = true;
					nlapiLogExecution('DEBUG', 'Breakpoint - Governance', recordCounter);
					break;
				}
			}
		}
		else{
			nlapiLogExecution('DEBUG', 'No results', 'No contents');
			crStatus 		= 2;
			updateCustomRecord();
			fileCounter 	= 0;
			lastProcessedId = 0;
			//Reset globals before breaking
			crRequest = 0, crResourceId = '', crFileId = 0, crFileCounter = 0, crLastProcessed = 0, crRequestId = '', crTimeOutCount = 0;
			break;
		}
		if(recordCounter >= recordLimit){
			nlapiLogExecution('DEBUG', 'Breakpoint - Record Limit', recordCounter);
		}
		
		nlapiLogExecution('DEBUG', 'Final - recordCounter', recordCounter);

		if(recordCounter == recordLimit || counterFlag == 'T'){
			if(productObject.length > 0){
				var resp = JSON.stringify(productObject);
				// nlapiLogExecution('DEBUG', 'resp length', resp.length);
				
				nlapiLogExecution('DEBUG', 'Inside file creation loop', 'Inside file creation loop');
				//nlapiLogExecution('DEBUG', 'Governance Limit-Before File Creation', nlapiGetContext().getRemainingUsage());
				var fileUpdate 	= nlapiLoadFile(folderName + '/' + crResourceId);
				var fileContent = fileUpdate.getValue();
				
				var newFileName = fileUpdate.getName();	
				if(fileCounter > 0) newFileName += '_' + parseInt(fileCounter);
				nlapiLogExecution('DEBUG', 'newFileName: ' + newFileName, 'fileCounter: ' + fileCounter);
				
				var updatedFile = nlapiCreateFile(newFileName, 'PLAINTEXT', JSON.stringify(productObject));
				updatedFile.setFolder(fileUpdate.getFolder());
				var idupdate = nlapiSubmitFile(updatedFile);
				nlapiLogExecution('DEBUG', 'Governance Limit-After File Creation', nlapiGetContext().getRemainingUsage());
				fileCounter++;

				//Update CustomRecord
				updateCustomRecord();
				
				// Reset Values
				productObject 	= new Array();
				recordCounter 	= 0;
				scriptLimit		= true;
				counterFlag 	= 'F';
				break;
			}
		}

		if(scriptLimit){
			break;
		}
	}
}

/*
*	name: UpdateCustomRecord
*	descr:  Update the custom record with after information has been put into a text file in the File Cabinet
*			
*
*	author: christopher.neal@kitandace.com
*/
function updateCustomRecord(){
	//this is what Wipro did, not sure if it works
	var fields = new Array();
	var values = new Array();

	fields.push('custrecord_extsync_filecounter');
	fields.push('custrecord_extsync_status_id');
	fields.push('custrecord_extsync_lastprocess');
	fields.push('custrecord_extsync_timeoutcount');

	values.push(fileCounter);
	values.push(crStatus);
	values.push(lastProcessedId);
	values.push(crTimeOutCount);

	crFileCounter = fileCounter;
	crLastProcessed = lastProcessedId;
	nlapiSubmitField('customrecord_external_sync_obj', crRequestId, fields, values, false);


	//Chris Neal did this, need to verify if it works
	// nlapiSubmitField(type, id, [field,field, field], [value, value, value], doSourcing)
	// nlapiSubmitField(type, id, field, value, doSourcing)

	// nlapiLogExecution('DEBUG', 'Updating Custom Record', crRequestId);
	
	// var curCustomRecord = nlapiLoadRecord('customrecord_external_sync_obj', crRequestId);
	// customrecord_external_sync_obj.nlapiSetFieldValue('custrecord_extsync_filecounter', fileCounter);
	// customrecord_external_sync_obj.nlapiSetFieldValue('custrecord_extsync_status_id', crStatus);
	// customrecord_external_sync_obj.nlapiSetFieldValue('custrecord_extsync_lastprocess', lastProcessedId);
	// customrecord_external_sync_obj.nlapiSetFieldValue('custrecord_extsync_timeoutcount', crTimeOutCount);
	// var crId = nlapiSubmitRecord(curCustomRecord);

	// nlapiLogExecution('DEBUG', 'Updated Custom Record', crId);
}

/*
*	name: getVendorList
*	descr:  Get the list of vendors for a given item ID
*
*	author: christopher.neal@kitandace.com (taken from Wipro)
*/
function getVendorList(itemId)
{
	var Vendors = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'anyof', itemId));

	var cols = new Array();
	cols.push(new nlobjSearchColumn('entityid', 'vendor'));
	var itemRecord = nlapiSearchRecord('item', null, filters, cols);
	if(itemRecord != null)
	{
		for(var v=0; v<itemRecord.length; v++)
		{
			var vendorList 				 = new Object();
			vendorList.code 			 = (itemRecord[v].getValue('entityid', 'vendor') != null) ? itemRecord[v].getValue('entityid', 'vendor') : "";
			vendorList.name 			 = (itemRecord[v].getValue('entityid', 'vendor') != null) ? itemRecord[v].getValue('entityid', 'vendor') : "";
			vendorList.altCode 			 = ""; 
			vendorList.factories 		 = new Array(); 
			vendorList.factories[0] 	 = new Object(); 
			vendorList.factories[0].code = "";
			vendorList.factories[0].name = ""; 
			Vendors.push(vendorList);
		}
	}
	else
	{
		var vendorList 				 = new Object();
		vendorList.code 			 = "";
		vendorList.name 			 = "";
		vendorList.altCode 			 = ""; 
		vendorList.factories 		 = new Array(); 
		vendorList.factories[0] 	 = new Object(); 
		vendorList.factories[0].code = "";
		vendorList.factories[0].name = ""; 
		Vendors.push(vendorList);
	}
	return Vendors;
}


/*
*	name: getPriceList
*	descr:  Get the price list for a given item ID
*			
*
*	author: christopher.neal@kitandace.com (taken from Wipro)
*/
function getPriceList(itemId)
{
	var Prices = new Array();
	var filters = new Array();
	filters.push(new nlobjSearchFilter('internalid', null, 'anyof', itemId));
	filters.push(new nlobjSearchFilter('pricelevel', 'pricing', 'is', [1, 5, 56]));	// 1- Base Price, 5- Online Price, 56-Sale Price
	
	var cols = new Array();
	cols.push(new nlobjSearchColumn('unitprice',  'pricing'));
	cols.push(new nlobjSearchColumn('currency',   'pricing').setSort());
	cols.push(new nlobjSearchColumn('pricelevel', 'pricing').setSort());
	var itemRecord = nlapiSearchRecord('item', null, filters, cols);
	
	if(itemRecord != null)
	{
		var records 	= false;
		var records1 	= false;
		for(var p=0; p<itemRecord.length; p++)
		{
			var priceArray = new Array();

			if(records)			records  = false;
			else if(records1)	records1 = false;
			else
			{
				for(var i=0; i<3 && (p+i)<itemRecord.length; i++)
				{
					if(i > 0 && itemRecord[p+i].getValue('currency', 'pricing') != priceArray[i-1].currency)	break;
					priceArray[i] = new Object();
					priceArray[i].currency 		=  itemRecord[p+i].getValue('currency', 'pricing');
					priceArray[i].price    		= (itemRecord[p+i].getValue('unitprice', 'pricing') != null) ? parseFloat(itemRecord[p+i].getValue('unitprice', 'pricing')) : 0.00;
					priceArray[i].priceLevel 	=  itemRecord[p+i].getValue('pricelevel', 'pricing');
				}
				
				if(priceArray.length > 1)	records1 = true;
				if(priceArray.length > 2)	records = true;
				
				var priceList = new Object();
				// priceList.priceId 			= "0";
				priceList.currencyCode 		= itemRecord[p].getText('currency', 'pricing');
				
				var prices = getPrices(priceArray, priceArray.length, false);
				
				priceList.amtCurrentPrice 	= prices.currentPrice;
				priceList.amtPromoPrice 	= prices.promoPrice;
				
				priceList.priceEffectiveStartDt = null;
				priceList.priceEffectiveEndDt 	= null;
				priceList.lastModifiedByUser 	= "";
				priceList.lastModifiedTimestamp = "";
				Prices.push(priceList);
			}
		}
	}
	else
	{
		var priceList = new Object();
		// priceList.priceId = "0";
	
		priceList.amtCurrentPrice = 0.00;
		priceList.currencyCode = "";
		priceList.amtPromoPrice = 0.00;
		priceList.priceEffectiveStartDt = null;
		priceList.priceEffectiveEndDt = null;
		priceList.lastModifiedByUser = "";
		priceList.lastModifiedTimestamp = "";
		Prices.push(priceList);
	}
	return Prices;
}

/*
*	name: getPrices
*	descr:  Get the price list for a given item ID
*			
*	author: christopher.neal@kitandace.com (taken from Wipro)
*/
function getPrices(priceArray, arrayLength, recall)
{
	if(!recall)	retObject = new Object();
	switch(arrayLength)
	{
		case 1:
			if(recall)
			{
				retObject.currentPrice = (priceArray[0].priceLevel == 5)   ? priceArray[0].price : null;
				recall = false;
			}
			else
			{
				retObject.currentPrice = (priceArray[0].priceLevel != 56)  ? priceArray[0].price : 0;
			}
			retObject.promoPrice 	= (priceArray[0].priceLevel == 56)   ? priceArray[0].price 		: null;
			break;
		case 2:
			getPrices(priceArray, 1, true);
			retObject.currentPrice 	= (retObject.currentPrice != null)   ? retObject.currentPrice   : (priceArray[1].priceLevel == 5)  ? priceArray[1].price : priceArray[0].price;
			retObject.promoPrice   	= (retObject.promoPrice != null) 	 ? retObject.promoPrice 	: (priceArray[1].priceLevel == 56) ? priceArray[1].price : null;
			break;
		case 3:
			getPrices(priceArray, 2, true);
			retObject.currentPrice 	= (retObject.currentPrice != null)   ? retObject.currentPrice   : (priceArray[2].priceLevel == 5)  ? priceArray[2].price : 0;
			retObject.promoPrice   	= priceArray[2].price;
			// retObject.promoPrice   	= (retObject.promoPrice != null) 	? retObject.promoPrice 	: (priceArray[2].priceLevel == 56) ? priceArray[2].price : null;
			break;
		default:
			retObject.currentPrice  = 0;
			retObject.promoPrice 	= null;
	}
	return retObject;
}