/**
 *	File Name		:	ERP_3PL_Transform_PO_to_IR_SL.js
 *	Function		:	Transforms Factory PO to IR, Creates Reversal Journal Entry
 * 	Remarks			:	
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{
	var STATUS_PROCESSED = SCRIPTCONFIG.getScriptConfigValue('IR Processed Status: Processed');
	var STATUS_FAILED = SCRIPTCONFIG.getScriptConfigValue('IR Processed Status: Failed');
	
	var REC_UPDATE_OBJ = {}; //Response
	
	var LogDirection = 1; //2 - Outbound Call, 1 - Inbound call
}


/**
 * SUITELET Main
 */
function suitelet_transformPOtoIR(request, response){
	
	var poId = request.getParameter('poid'); //PO Id
	var imId = request.getParameter('imid'); //Inbound Message record Id
	var fileId = request.getParameter('fileid'); //Inbound Message File Id
	var irId = request.getParameter('irid'); //Item Receipt Id
	
	nlapiLogExecution('DEBUG', 'poId', poId);
	nlapiLogExecution('DEBUG', 'imId', imId);
	nlapiLogExecution('DEBUG', 'irId', irId);
	nlapiLogExecution('DEBUG', 'fileId', fileId);

	var bSuccess;
	
	//If there is no IR yet
	if(!irId){		
		//Transform the PO to IR
		bSuccess = transformPOtoIR(poId, imId, fileId);
		
		//Create Reversal JE
		if(bSuccess){
			createReversalJE(poId, imId);
		}
		
	} else {

		//Logging - IR exists
		FactoryPOLib.log(LogDirection, poId, 0, 'Transforming PO to IR', FactoryPOLib.getLogStatusID('SUCCESS'), 'Item Receipt already exists: ' + irId, 'F', imId);
	
		//Create Reversal JE
		createReversalJE(poId, imId);
		bSuccess = true;
	}
		
	response.write(bSuccess);
}

/**
 * Transforms PO to IR
 */
function transformPOtoIR(poId, recId, fileId){
	//For logging
	var stProcessDesc = 'Transforming PO to IR';
	var stResultDesc;
	
	nlapiLogExecution('DEBUG', 'transformPOtoIR', stProcessDesc); 
	
	var itemReceiptId;

	try {
		
		//Get Staging record info
		var recStaging = nlapiLoadRecord('customrecord_erp_3pl_inbound_itemreceipt', recId);
		var stInboundMsg = recStaging.getFieldValue('custrecord_itemreceipt_inboundmessage');
		var stShipmentId = recStaging.getFieldValue('externalid');
		
		//Get received qty of items	
		var objQtyPerLine = FactoryPOLib.getInboundMessageReceivedItemsPerLine(FactoryPOLib.getInboundMessage(fileId, stInboundMsg), stShipmentId);

		//Transform PO to IR
		var trecord = nlapiTransformRecord('purchaseorder', poId, 'itemreceipt');
		
		//Receive only the qtys specified in Inbound Message
		for(var i = 1; i <= trecord.getLineItemCount('item'); i++){
			trecord.selectLineItem('item', i);
			
			//Set empty qty first
			trecord.setCurrentLineItemValue('item', 'quantity', null);
			trecord.setCurrentLineItemValue('item', 'itemreceive', 'F');
			
			//Get current line equivalent PO line number
			var stPOLineNo = trecord.getCurrentLineItemValue('item', 'line');
			
			//Set actual received qty
			if(objQtyPerLine[stPOLineNo]){
				trecord.setCurrentLineItemValue('item', 'quantity', objQtyPerLine[stPOLineNo]);
				trecord.setCurrentLineItemValue('item', 'itemreceive', 'T');
			}
			
			trecord.commitLineItem('item');
		}
		
		//Set Item Receipt externalid
		trecord.setFieldValue('externalid', stShipmentId + '_IR'); //appended '_IR' to prevent duplicate error as original PO may also have the same externalid

		itemReceiptId = nlapiSubmitRecord(trecord, true); //this will trigger UE script that creates ICPO (ERP - TransferPrice CreateInterCoPO UE)
		
		//Logging
		stResultDesc = 'Record transformed. Item Receipt ID: ' + itemReceiptId;		
		nlapiLogExecution('DEBUG', 'SUCCESS', stResultDesc);
		
	} catch (error) {
						
		//Logging
		stResultDesc = 'Record not transformed: ' + error.toString();
		FactoryPOLib.log(LogDirection, (poId ? poId : 0), (itemReceiptId ? itemReceiptId : 0), stProcessDesc, FactoryPOLib.getLogStatusID('ERROR'), stResultDesc, 'F', recId);	
		
		//Set Status field to Failed
		nlapiSubmitField('customrecord_erp_3pl_inbound_itemreceipt', recId, 'custrecord_itemreceipt_status', STATUS_FAILED);
		
		nlapiLogExecution('DEBUG', 'FAIL', error.toString());
		nlapiLogExecution('DEBUG', 'FAIL', stResultDesc);	

		return false;
	}
	
	//Update Inbound IR Message fields immediately here to avoid timing issues for succeeding scripts
	//Set IR, Status fields	
	REC_UPDATE_OBJ['custrecord_itemreceipt_transformed_ir'] = itemReceiptId;
	REC_UPDATE_OBJ['custrecord_itemreceipt_status'] = STATUS_PROCESSED;
	updateInboundIRMsg(poId, recId, {"IR": {id: itemReceiptId, tranid: nlapiLookupField('itemreceipt', itemReceiptId, 'tranid')}});
			
	//Logging
	FactoryPOLib.log(LogDirection, poId, itemReceiptId, stProcessDesc, FactoryPOLib.getLogStatusID('SUCCESS'), stResultDesc, 'F', recId);
			
	return true;
}

/**
 * Creates Reversal Journal Entry
 */
function createReversalJE(poId, recId){
	
	//Assume status to 'Processed'; status will be changed when RJE creation fails
	REC_UPDATE_OBJ['custrecord_itemreceipt_status'] = STATUS_PROCESSED;
	
	var stProcessDesc = 'Creating Reversal Journal Entry';
	
	var objProcTran = {};
	
	//Use PO info
	var recPO = nlapiLoadRecord('purchaseorder', poId);
	
	//Store JE to Processed Transactions	
	var stJEId = recPO.getFieldValue('custbody_3pl_journalentry');
	objProcTran['JE'] = {id: stJEId ? stJEId : '', tranid: stJEId ? nlapiLookupField('journalentry', stJEId, 'tranid') : ''};
	
	//Check if PO has JE to reverse
	if(!stJEId){ //No JE
				
		//Store empty RJE to Processed Transactions	
		objProcTran['RJE'] = {id: '', tranid: ''};
			
		//Logging
		FactoryPOLib.log(LogDirection, poId, 0, stProcessDesc, FactoryPOLib.getLogStatusID('SUCCESS'), 'PO has no Journal Entry to reverse', 'F', recId);
	
	} else { //Has JE
				
		//Check if PO already has a reversal JE
		var stReversalJEId = recPO.getFieldValue('custbody_3pl_journalentry_reversal');
		if(recPO.getFieldValue('custbody_3pl_journalentry_reversal')){ //Has RJE
		
			//Store RJE to Processed Transactions	
			//objProcTran['RJE'] = {id: stReversalJEId, tranid: nlapiLookupField('journalentry', stReversalJEId, 'tranid')};
		
			//Logging
			FactoryPOLib.log(LogDirection, poId, 0, stProcessDesc, FactoryPOLib.getLogStatusID('SUCCESS'), 'PO already has reversal Journal Entry: ' + stReversalJEId, 'F', recId);	
		
		} else { //No RJE
		
			//Additional Checking: identify if current staging record is for the 1st shipment
			//This is to prevent the creation of another Reversal JE in case two staging records are being processed at the same time (timing issue)
			if(isFirstShipment(poId, recId)){
			
				//Create reversal Journal Entry
				var objResult = FactoryPOLib.createJournalEntry(
									recPO.getFieldValue('tranid'), 
									recPO.getFieldValue('total'), 
									recPO.getFieldValue('subsidiary'), 
									recPO.getFieldValue('currency'),
									recPO.getFieldValue('exchangerate'), 
									recPO.getFieldText('custbody_3pl_journalentry'));
								
				if(objResult.success){	
				
					//Store RJE to Processed Transactions
					objProcTran['RJE'] = {id: objResult.journalid, tranid: nlapiLookupField('journalentry', objResult.journalid, 'tranid')};
					
					//Logging
					FactoryPOLib.log(LogDirection, poId, objResult.journalid, stProcessDesc, 
						FactoryPOLib.getLogStatusID('SUCCESS'), 'Reversal Journal Entry created: ' + objResult.journalid, 'F', recId);

					//Link RJE to PO
					nlapiSubmitField('purchaseorder', poId, 'custbody_3pl_journalentry_reversal', objResult.journalid);
					
				} else{
					//Logging
					FactoryPOLib.log(LogDirection, poId, 0, stProcessDesc, FactoryPOLib.getLogStatusID('ERROR'), objResult.error, 'F', recId);	
					
					//Set custom record to Failed status
					REC_UPDATE_OBJ['custrecord_itemreceipt_status'] = STATUS_FAILED;
				}
			}
		}
	}

	//Update Inbound IR Message fields
	updateInboundIRMsg(poId, recId, objProcTran);	
}

/**
 * Updates Inbound IR Message fields
 */
function updateInboundIRMsg(poId, recId, objProcTrans){
	
	//Surround with try-catch as this is susceptible to timing issues (e.g. record collision)
	try{
		var recIRMsg = nlapiLoadRecord('customrecord_erp_3pl_inbound_itemreceipt', recId);
		
		//Regular fields
		for(var field in REC_UPDATE_OBJ){
			recIRMsg.setFieldValue(field, REC_UPDATE_OBJ[field]);
		}
		
		//Store new Processed Transaction
		var procTran = recIRMsg.getFieldValue('custrecord_itemreceipt_processed_tran');
		for(var key in objProcTrans){
			procTran = FactoryPOLib.updateInboundMessageTranLinks(procTran, key, objProcTrans[key].id, objProcTrans[key].tranid);
		}	
		recIRMsg.setFieldValue('custrecord_itemreceipt_processed_tran', procTran);
		
		//Get IDs of processed transactions
		var arIds = FactoryPOLib.getTransactionLinkIds(procTran);
		
		//Update Transaction Link field
		recIRMsg.setFieldValues('custrecord_itemreceipt_linkedtransaction', arIds);	//multi-select
		
		//Submit
		nlapiSubmitRecord(recIRMsg, false, true);
		
	} catch(error){
		nlapiLogExecution('debug', 'updateInboundIRMsg error', error.toString());
				
		FactoryPOLib.log(LogDirection, poId ? poId : 0, 0, 'Updating staging record', FactoryPOLib.getLogStatusID('ERROR'), error.toString(), 'F', recId);
	}
	
	//Reset REC_UPDATE_OBJ
	REC_UPDATE_OBJ = {};
}

/**
 * Checks if staging record is the first shipment for the PO
 */
function isFirstShipment(poId, stagingId){
	
	var bIsFirstShipment = true;
	
	//Identify if there is another staging record for the PO
	var arIRMsg = FactoryPOLib.searchInboundIRMessage(poId);
	
	if(arIRMsg && arIRMsg.length > 0){
		
		for(var i = 0; i < arIRMsg.length; i++){
			if(stagingId > arIRMsg[i].getValue('internalid')){
				bIsFirstShipment = false;
				break;
			}
		}
	} 
	
	nlapiLogExecution('debug', 'isFirstShipment poId', poId);
	nlapiLogExecution('debug', 'isFirstShipment stagingId', stagingId);
	nlapiLogExecution('debug', 'isFirstShipment arIRMsg', JSON.stringify(arIRMsg));
	nlapiLogExecution('debug', 'isFirstShipment bIsFirstShipment', bIsFirstShipment);
	
	return bIsFirstShipment;
}