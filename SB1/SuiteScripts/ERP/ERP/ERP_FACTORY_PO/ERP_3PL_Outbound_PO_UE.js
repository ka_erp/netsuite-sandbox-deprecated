/**
 *	File Name		:	ERP_3PL_Outbound_PO_UE.js
 *	Function		:	Checks if the field 'Pricing Approval' is set to Approved
 *						If YES, then there is a call to Mulesoft/Cloudhub sending the record ID to integrate the 
 *						record with the 3PL system
 * 	Remarks			:	
 *	Prepared by		:	apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{
	var PRICING_APPROVAL_APPROVED = SCRIPTCONFIG.getScriptConfigValue('Factory PO: Pricing Approval: Approved');
	
	//EXPORT 3PL STATUS
	var STATUS_SEND = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Send');
	var STATUS_SENT = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Sent');
	var STATUS_ERROR = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Error');
	
	//FACTORY PO - filters
	var CHANNELS = SCRIPTCONFIG.getScriptConfigValue('Factory PO: Channels');
	var FROM_LOCATIONS = SCRIPTCONFIG.getScriptConfigValue('Factory PO: From Locations');
	var TO_LOCATIONS = SCRIPTCONFIG.getScriptConfigValue('Factory PO: To Locations');
	
	var sendEmail = true; //needed in WTKA_Library.js
}

/**
 * AFTER SUBMIT
 */
function afterSubmit_OutboundPO(type){
	
	var stContext = nlapiGetContext().getExecutionContext();
	var stPOId = nlapiGetRecordId();
		
	nlapiLogExecution('DEBUG', 'poid', stPOId); 
	nlapiLogExecution('DEBUG', 'context', stContext);
	nlapiLogExecution('DEBUG', 'type', type); 
		
	if(type == 'create' || type == 'edit' || type == 'xedit'){
		
		var stPricingApproval;
		var stExportStatus; 
		var stCurrentHOD;
		var stChannelText;
		var stLocation;
		var stShipTo;
		
		if(type == 'xedit'){
			var recPO = nlapiLoadRecord('purchaseorder', stPOId);
			stPricingApproval = recPO.getFieldValue('custbody_3pl_pricing_approval');
			stExportStatus = recPO.getFieldValue('custbody_export_3pl_status');
			stCurrentHOD = recPO.getFieldValue('custbody_ka_current_hod');
			stChannelText = recPO.getFieldText('custbody_ka_erp_channel_new');
			stLocation = recPO.getFieldValue('location');
			stShipTo = recPO.getFieldValue('shipto');
		} else {
			stPricingApproval = nlapiGetFieldValue('custbody_3pl_pricing_approval');
			stExportStatus = nlapiGetFieldValue('custbody_export_3pl_status');
			stCurrentHOD = nlapiGetFieldValue('custbody_ka_current_hod');
			stChannelText = nlapiGetFieldText('custbody_ka_erp_channel_new');
			stLocation = nlapiGetFieldValue('location');
			stShipTo = nlapiGetFieldValue('shipto');
		}
		
		nlapiLogExecution('DEBUG', 'Pricing Approval', stPricingApproval); 
		nlapiLogExecution('DEBUG', 'Export 3PL Status', stExportStatus);
		nlapiLogExecution('DEBUG', 'Current HOD', stCurrentHOD); 
		nlapiLogExecution('DEBUG', 'Channel', stChannelText); 
		nlapiLogExecution('DEBUG', 'Location', stLocation); 
		nlapiLogExecution('DEBUG', 'Ship To', stShipTo);

		//Trigger Export of PO
		if(stPricingApproval == PRICING_APPROVAL_APPROVED					//Pricing Approval = Approved
		   && !stExportStatus												//Epxport 3PL Status = blank
		   && stCurrentHOD													//Current HOD = not blank
		   && FactoryPOLib.isValidChannel(stChannelText, CHANNELS)			//Channel = valid (based on Script Config)
		   && FactoryPOLib.isValidLocation(stLocation, FROM_LOCATIONS)		//Location = valid (based on Script Config)
		   && FactoryPOLib.isValidShipToLocation(stShipTo, TO_LOCATIONS)){	//ShipTo Location = valid (based on Script Config)
			exportPO(stPOId);
		}
	}
}

/**
 * Trigger ESB to request PO object from NetSuite
 */
function exportPO(stPOId){
		
	nlapiLogExecution('DEBUG', 'exportPO', 'send to 3PL');
	
	var recPO = nlapiLoadRecord('purchaseorder', stPOId);
	var tranId = recPO.getFieldValue('tranid');	  		
	var status =  recPO.getFieldValue('status');
	var exportStatus;

	try {
		//Invoke Cloudhub to request PO object
		var recordValues = InvokeCloudhub(nlapiGetRecordType(), nlapiGetRecordId(), tranId, status); 	//call to send to Record ID to Mulesoft/Cloudehub to integrate the record.
																										//call to ERP_WTKA_Library.js

		nlapiLogExecution('DEBUG', 'recordValues', recordValues);

		if (recordValues == 'T'){
			nlapiLogExecution('DEBUG', 'SUCCESS', 'Message sent to Cloudhub successfully for transaction ' + tranId); 
			exportStatus = STATUS_SENT;
		} else {	            
			nlapiLogExecution('DEBUG', 'FAIL', 'Message to Cloudhub failed. Please check Integration logs. Transaction ' + tranId);
			exportStatus = STATUS_ERROR;
		}
	} catch (error) {
		exportStatus = STATUS_ERROR;
	}
	
	//Update EXPORT 3PL STATUS
	if(exportStatus){		
		recPO.setFieldValue('custbody_export_3pl_status', exportStatus);
		
		//Submit PO
		nlapiSubmitRecord(recPO, false, true);
	}
}