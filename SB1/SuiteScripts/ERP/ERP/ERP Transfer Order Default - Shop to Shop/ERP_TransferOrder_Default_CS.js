/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       10 Oct 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function transferOrderdefault_PageInit_CS(type, form, request) {


	try{

	//Shop to Shop - hide "From location"
	//Else - hide "To Location"
	
	
	var orderStatus = nlapiGetFieldValue('orderstatus');
	nlapiLogExecution("DEBUG", "orderStatus", orderStatus);
	
	if(orderStatus == '' || orderStatus == 'A' || orderStatus == 'B'){
	
	
		var formId = nlapiGetContext().getSetting('SCRIPT', 'custscript_erp_formid'); 	if(formId){var formIdArr = formId.split(",");}
		
		var custForm = nlapiGetFieldValue('customform'); custForm = "" + custForm; 
		
		//console.log(custForm);
		//console.log(formIdArr);
		//console.log(formIdArr.indexOf("" + custForm) );	
		
		if(formIdArr.indexOf(custForm) >= 0){
			
		
		jQuery('body').hide();
		
		//console.log('type: ' + type);
		//console.log('form: ' + form);
		
		//jQuery('#customtxt').hide();
			
		if (type == 'create') {
			
			//console.log('enter create');
			
			
			
			nlapiGetField("location").setDisplayType("hidden");
			nlapiGetField("transferlocation").setDisplayType("hidden");
			nlapiGetField('custbody_erp_location_destination').setDisplayType('hidden');
			nlapiGetField('custbody_erp_location_source').setDisplayType('hidden');
					
			
			var idUser = nlapiGetUser();
			
			nlapiSetFieldValue("subsidiary", nlapiGetSubsidiary(), false, false);
			nlapiSetFieldValue("employee", nlapiGetUser(), false, false);		
			nlapiSetFieldValue("custbody_erp_rolelist", 1012, false, false);
			
			
			var defaulRole = nlapiGetFieldValue("custbody_erp_rolelist");
			if(defaulRole == 1012){			
				
			}
			
	//		nlapiGetField('custbody_erp_location_destination').setDisplayType('hidden');
	//		nlapiGetField('custbody_erp_location_source').setDisplayType('hidden');


			setTimeout(function() {

				nlapiSetFieldValue("location", nlapiGetLocation(), false, false);
				nlapiSetFieldValue("custbody_erp_destination_location", nlapiGetLocation(), false, false);
				nlapiSetFieldValue("custbody_ka_order_type", 3, false, false);
				nlapiGetField('custbody_erp_location_destination').setDisplayType('normal');
				
				var idSubsidiary = nlapiGetSubsidiary();

			}, 1000);
			
			

		}
		else if (type != 'delete') {
			
			
			nlapiGetField("location").setDisplayType("hidden");
			nlapiGetField("transferlocation").setDisplayType("hidden");
			nlapiGetField('custbody_erp_location_destination').setDisplayType('hidden');
			nlapiGetField('custbody_erp_location_source').setDisplayType('hidden');
			
			var idOrderType = nlapiGetFieldValue('custbody_ka_order_type');
			
			//console.log('enter !delete' + idOrderType);
		
			if (idOrderType == 8) {			
				nlapiGetField('custbody_erp_location_source').setDisplayType('normal');	
			}
			
			if (idOrderType == 9) {			
				nlapiGetField('custbody_erp_location_destination').setDisplayType('normal');
			}
			
			
			
		}
		
		
		setTimeout(function() {

			jQuery('body').show();

		}, 200);
		
		
		}
	
	}

	}catch(ex){nlapiLogExecution("ERROR", "ERROR", ex.toString());}
}

function transferOrderdefault_FieldChanged(type, name) { 
	//change location
	//console.log(name);
	// 9	Damaged	
	//	3	Pullback
	//	8	Shop to Shop
	
	if (name == 'custbody_ka_order_type') {
		if (nlapiGetFieldValue('custbody_ka_order_type') == 9 || nlapiGetFieldValue('custbody_ka_order_type') == 3){	
			nlapiGetField('custbody_erp_location_destination').setDisplayType('normal');
			nlapiGetField('custbody_erp_location_source').setDisplayType('hidden');
			nlapiSetFieldValue("location", nlapiGetLocation(), false, false);	
		
	}
	else if (nlapiGetFieldValue('custbody_ka_order_type') == 8) {
			nlapiGetField('custbody_erp_location_destination').setDisplayType('hidden');
			nlapiGetField('custbody_erp_location_source').setDisplayType('normal');
	}
	}
	
	if(name =='custbody_erp_location_destination') {
	      		var fromLoc = nlapiGetFieldValue('custbody_erp_location_destination');
	      		var  loc = nlapiLookupField('customrecord_erp_shoptoshop_location', fromLoc, 'custrecord_erp_shopto_location');      nlapiSetFieldValue('transferlocation', loc);
	  }
	if (name =='custbody_erp_location_source') {
		if (nlapiGetFieldValue('custbody_ka_order_type') == 8){	
			//nlapiGetField('custbody_erp_location_source').setDisplayType('hidden');
			var fromLoc = nlapiGetFieldValue('custbody_erp_location_source');
			var  loc = nlapiLookupField('customrecord_erp_shoptoshop_location', fromLoc, 'custrecord_erp_shop_from');  
	
		    nlapiSetFieldValue('location', loc);
		 	nlapiSetFieldValue("transferlocation", nlapiGetLocation(), false, false);
		}
	  }
}

function updateTotalQuantity(type, name) {
	  //initialize variable for total quantity
	  var totalQuantity = 0;
	  
	  // count number of lines in 'item' sublist
	  var itemCount = nlapiGetLineItemCount('item');
	  //for each line in the 'item' sublist, add value in quantity column to the total quantity variable
	  for(var i=1; i<=itemCount; i++){
	   lineLevelQuantity = nlapiGetLineItemValue('item', 'quantity', i)
	   if(lineLevelQuantity != '' && lineLevelQuantity != null ) {
	    totalQuantity += parseInt(lineLevelQuantity);
	   }
	  }
	  
	  //  assuming custbody_totalqty is the custom body field for the total quantity, change its value based the value from the computation above
	  nlapiSetFieldValue('custbody_totalqty', totalQuantity, false);
	 }

function isNullOrEmpty(valueStr) {return (valueStr == null || valueStr == "" || valueStr == undefined);}