/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       20 Jan 2016     ivan.sioson
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function erp_sdc_SL(request, response){

	//var transID = request.getParameter( 'transactionRecord' );
	var transDate = request.getParameter( 'transactionDate' );
	var mode = request.getParameter( 'mode' );
	
	nlapiLogExecution('DEBUG', '>>>> erp_sdc_SL', 'mode '  +  mode); 
			
	try{
		
		if(!isNullOrEmpty(transDate)){
			
			nlapiLogExecution('DEBUG', '>>>> erp_sdc_SL', 'transDate '  +  transDate); 
			var responseObj = getPeriodDateRanges(transDate); 	    
			response.write( JSON.stringify(responseObj) );
			
		    
		}		
		
		
		if(!isNullOrEmpty(mode)){
			
			if(mode == 'salesinfo'){
				
				var locId = request.getParameter( 'locationId' );
				var date1 = request.getParameter( 'transactionDate1' );
				var date2 = request.getParameter( 'transactionDate2' );
				
				nlapiLogExecution('DEBUG', '>>>>>>>>>>>>>>>>>>>>>>salesinfo erp_sdc_SL ' + date1 + " - " + date2, ''); 
				nlapiLogExecution('DEBUG', '>> erp_sdc_SL a', new Date()); 
				var responseObj = getSalesRange(locId, date1, date2);
				nlapiLogExecution('DEBUG', '>> erp_sdc_SL b', new Date());  

				response.write( JSON.stringify(responseObj) );
				
			}
			
			
			if(mode == 'creditinfo'){
				
				var locId = request.getParameter( 'locationId' );
				var date1 = request.getParameter( 'transactionDate1' );
				var date2 = request.getParameter( 'transactionDate2' );
				
				nlapiLogExecution('DEBUG', '>>>>>>>>>>>>>>>>>>>>>>creditinfo erp_sdc_SL ' + date1 + " - " + date2, ''); 
				nlapiLogExecution('DEBUG', '>> erp_sdc_SL a', new Date()); 
				var responseObj = getCreditRange(locId, date1, date2);
				nlapiLogExecution('DEBUG', '>> erp_sdc_SL b', new Date());  

				response.write( JSON.stringify(responseObj) );
				
			}
			
		}
		
	}catch(ex){		
		nlapiLogExecution('DEBUG', '>>>> erp_sdc_SL: Error', ex);
	}	
}


function getSalesPOSDiscount(locId, date1, date2){
	
	// Order needs to be conforming to :
	// Type + Account + Location + Amount + Currency


	var salesArrayIds = new Array();
	var salesArrayObj = new Array();
	var salesObject = new Object();


	var strSavedSearchIDSalesRange = null;
	var arSaveSearchFiltersSalesRange = new Array();
	var arSavedSearchColumnsSalesRange = new Array();
	var arSavedSearchResultsSalesRange = null;



	arSaveSearchFiltersSalesRange.push(new nlobjSearchFilter( 'location', null, 'anyof', locId));
	arSaveSearchFiltersSalesRange.push(new nlobjSearchFilter('trandate', null, 'within', [date1,date2]));

	var arSavedSearchResultsSalesRange = nlapiLoadSearch('transaction', 'customsearch1342');	

	arSavedSearchResultsSalesRange.addColumns(arSavedSearchColumnsSalesRange);
	arSavedSearchResultsSalesRange.addFilters(arSaveSearchFiltersSalesRange);

	var resultSet = arSavedSearchResultsSalesRange.runSearch();
	
	if(!isNullOrEmpty(resultSet)){



		try{
			
			var results = resultSet.getResults(0,1000);
			//var objDateRange = new Object();

nlapiLogExecution('DEBUG', 'Result JSON POS Discount', JSON.stringify(results)); 


			if(results){

			var idCurrentItem = results[0].getValue('internalid');
			var columns = results[0].getAllColumns();
			var objResult = results[0];

			var objVendor = new Object();

			objVendor.type = objResult.getValue(columns[0]);
			objVendor.fd = objResult.getValue(columns[1]); //f&d
			objVendor.ed = objResult.getValue(columns[2]); //employee discount			
			objVendor.other = objResult.getValue(columns[3]);
			
			nlapiLogExecution('DEBUG', 'Result JSON POS Discount OBj', JSON.stringify(objVendor)); 

			return objVendor; 


						}
		}catch(ex){
			return null;
		}
	}else{
		return null;
	}
	
}

function getSalesRange(locId, date1, date2){
	
	
	// Order needs to be conforming to :
	// Type + Account + Location + Amount + Currency


	var salesArrayIds = new Array();
	var salesArrayObj = new Array();
	var salesObject = new Object();


	var strSavedSearchIDSalesRange = null;
	var arSaveSearchFiltersSalesRange = new Array();
	var arSavedSearchColumnsSalesRange = new Array();
	var arSavedSearchResultsSalesRange = null;



	arSaveSearchFiltersSalesRange.push(new nlobjSearchFilter( 'location', null, 'anyof', locId));
	arSaveSearchFiltersSalesRange.push(new nlobjSearchFilter('trandate', null, 'within', [date1,date2]));

	var arSavedSearchResultsSalesRange = nlapiLoadSearch('transaction', 'customsearch_erp_sales_infor_final');

	arSavedSearchResultsSalesRange.addColumns(arSavedSearchColumnsSalesRange);
	arSavedSearchResultsSalesRange.addFilters(arSaveSearchFiltersSalesRange);

	var resultSet = arSavedSearchResultsSalesRange.runSearch();

	if(!isNullOrEmpty(resultSet)){


		try{
			var results = resultSet.getResults(0,5);

			if(!isNullOrEmpty(results)){
				
				//nlapiLogExecution('DEBUG', 'Result JSON', JSON.stringify(results)); 

				for(var i = 0; i < results.length; i++){

					var idCurrentItem = results[i].getValue('internalid');
					var columns = results[i].getAllColumns();
					var objResult = results[i];

					var objVendor = new Object();

					objVendor.type = objResult.getValue(columns[0]);
					objVendor.accountid = objResult.getValue(columns[1]);
					objVendor.location = objResult.getValue(columns[2]);															
					objVendor.netsales_fx = objResult.getValue(columns[3]);						
					objVendor.netsales_fx = Number(Math.abs(objVendor.netsales_fx)).toFixed(0);
					
					objVendor.estCost = objResult.getValue(columns[4]);
					objVendor.currency = objResult.getValue(columns[5]);
									
					salesArrayIds.push(objVendor.type + objVendor.accountid);
					salesArrayObj.push(objVendor);

				}

				salesObject.ids = salesArrayIds;
				salesObject.obj = salesArrayObj;

			}

			return salesObject;

		}catch(ex){
			return null;
		}

	}else{
		return null;
	}
}


function getCreditRange(locId, date1, date2){
	
	
	// Order needs to be conforming to :
	// Type + Account + Location + Amount + Currency


	var salesArrayIds = new Array();
	var salesArrayObj = new Array();
	var salesObject = new Object();


	var strSavedSearchIDSalesRange = null;
	var arSaveSearchFiltersSalesRange = new Array();
	var arSavedSearchColumnsSalesRange = new Array();
	var arSavedSearchResultsSalesRange = null;



	arSaveSearchFiltersSalesRange.push(new nlobjSearchFilter( 'location', null, 'anyof', locId));
	arSaveSearchFiltersSalesRange.push(new nlobjSearchFilter('trandate', null, 'within', [date1,date2]));

	var arSavedSearchResultsSalesRange = nlapiLoadSearch('transaction', 'customsearch_erp_credit_infor_final');

	arSavedSearchResultsSalesRange.addColumns(arSavedSearchColumnsSalesRange);
	arSavedSearchResultsSalesRange.addFilters(arSaveSearchFiltersSalesRange);

	var resultSet = arSavedSearchResultsSalesRange.runSearch();

	if(!isNullOrEmpty(resultSet)){


		try{
			var results = resultSet.getResults(0,5);

			if(!isNullOrEmpty(results)){
				
				//nlapiLogExecution('DEBUG', 'Result JSON', JSON.stringify(results)); 

				for(var i = 0; i < results.length; i++){

					var idCurrentItem = results[i].getValue('internalid');
					var columns = results[i].getAllColumns();
					var objResult = results[i];

					var objVendor = new Object();

					objVendor.type = objResult.getValue(columns[0]);
					objVendor.accountid = objResult.getValue(columns[1]);
					objVendor.location = objResult.getValue(columns[2]);															
					objVendor.netsales_fx = objResult.getValue(columns[3]);						
					objVendor.netsales_fx = Number(Math.abs(objVendor.netsales_fx)).toFixed(0);
					
					objVendor.estCost = objResult.getValue(columns[4]);
					objVendor.currency = objResult.getValue(columns[5]);
									
					salesArrayIds.push(objVendor.type + objVendor.accountid);
					salesArrayObj.push(objVendor);

				}

				salesObject.ids = salesArrayIds;
				salesObject.obj = salesArrayObj;

			}

			return salesObject;

		}catch(ex){
			return null;
		}

	}else{
		return null;
	}
}




function getPeriodDateRanges(todayDate){

	nlapiLogExecution('DEBUG', 'erp_sdc_SL', 'getPeriodDateRanges '  +  todayDate); 
	
	try{


		var salesArrayIds = new Array();
		var salesArrayObj = new Array();
		var salesObject = new Object();

		var objDateRange = {
				"period": {},
				"quarter": {},
				"year":{},
			};

		var strSavedSearchIDDatePeriod = null;
		var arSaveSearchFiltersDatePeriod = new Array();
		var arSavedSearchColumnsDatePeriod = new Array();
		var arSavedSearchResultsDatePeriod = null;


		var arSavedSearchResultsDatePeriod = nlapiLoadSearch('AccountingPeriod', 'customsearch_erp_accounting_period_2');

		//filters[0] = new nlobjSearchFilter('formulatext', null, 'startswith', 'a');

		//arSaveSearchFiltersDatePeriod.push(new nlobjSearchFilter('formulatext', "case when (    to_date(" + todayDate + ", 'MM/DD/YYYY')      >= {startdate}) then 'true' else 'false' end)", 'is', 'true');
		//arSaveSearchFiltersDatePeriod.push(new nlobjSearchFilter('formulatext', "case when(			to_date(" + todayDate + ", 'MM/DD/YYYY')  		   <= {enddate})   then 'true' else 'false' end)", 'is', 'true');

		arSaveSearchFiltersDatePeriod[0] = new nlobjSearchFilter('formulatext', null, 'is', 'true');
		arSaveSearchFiltersDatePeriod[0].setFormula("case when (    to_date('" + todayDate + "', 'MM/DD/YYYY')      >= {startdate}) then 'true' else 'false' end");

		arSaveSearchFiltersDatePeriod[1] = new nlobjSearchFilter('formulatext', null, 'is', 'true');
		arSaveSearchFiltersDatePeriod[1].setFormula("case when(to_date('" + todayDate + "', 'MM/DD/YYYY')  <= {enddate}) then 'true' else 'false' end");

		arSavedSearchResultsDatePeriod.addColumns(arSavedSearchColumnsDatePeriod);
		arSavedSearchResultsDatePeriod.addFilters(arSaveSearchFiltersDatePeriod);

		var resultSet = arSavedSearchResultsDatePeriod.runSearch();



		if(!isNullOrEmpty(resultSet)){

			var results = resultSet.getResults(0,1000);
			//var objDateRange = new Object();




			if(results){

				for(var i = 0; i < results.length; i++){

					//var idCurrentItem = results[i].getValue('internalid');
					var columns = results[i].getAllColumns();
					var objResult = results[i];


					var year = objResult.getValue(columns[5]);
					var quarter = objResult.getValue(columns[6]);
					var start = objResult.getValue(columns[3]);
					var end = objResult.getValue(columns[4]);
					var thisLoop = '';


					if(year == 'F' && quarter == 'F'){
						thisLoop = 'period';
						objDateRange.period.start= objResult.getValue(columns[3]);
						objDateRange.period.end = objResult.getValue(columns[4]);
					}

					if(year == 'F' && quarter == 'T'){
						thisLoop = 'quarter';
						objDateRange.quarter.start = objResult.getValue(columns[3]);
						objDateRange.quarter.end = objResult.getValue(columns[4]);
					}


					if(year == 'T' && quarter == 'F'){
						thisLoop = 'year';
						objDateRange.year.start = objResult.getValue(columns[3]);
						objDateRange.year.end = objResult.getValue(columns[4]);
					}

//					console.log(thisLoop + ' year:' + year + ' quarter:' + quarter + ' start:' + start + ' end:' + end);


				}

//				console.log(JSON.stringify(objDateRange));
				nlapiLogExecution('DEBUG', 'DATE RANGE ', JSON.stringify(objDateRange));

		}

			return objDateRange;
	}
	}catch(ex){}

}

function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}