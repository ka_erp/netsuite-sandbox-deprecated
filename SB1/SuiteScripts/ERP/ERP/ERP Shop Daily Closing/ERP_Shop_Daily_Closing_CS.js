/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.02       11 Dec 2015     ivan.sioson
 *
 */

function erp_sdc_PageInit(type, name){

         var c = nlapiGetContext().getScriptId();


	console.log("erp_sdc_PageInit " + type + " c:" + c); 		
	checkIfComplete(); 
	
	
	var thisLocation = nlapiGetFieldValue('custrecord_erp_dclose_shop_name');	
	if(isNullOrEmpty(thisLocation)){
		 thisLocation = nlapiGetLocation(); 
	}
//	
//	nlapiSetFieldValue('custrecord_erp_dclose_shop_name', thisLocation); 
	
	//var idLocation = nlapiGetFieldValue('custrecord_erp_dclose_shop_name');
	
	
	var recLocation = nlapiLoadRecord('location', thisLocation);
	setMainShopClosing(recLocation);
	
	if(type == 'edit'){
								
		resetAllFields(); 
		recalcClosingNotes();
								
	}
	 	
}


function setMandatoryFields(mandatory){
	
		
	 nlapiSetFieldMandatory('custrecord_erp_dclose_hours_operation', mandatory); 
	 nlapiSetFieldMandatory('custrecord_erp_dclose_daily_goal', mandatory);
	 
	 
	 nlapiSetFieldMandatory('custrecord_erp_date_created'				, mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_complete'                    , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_closing_lead'         , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_daily_to_goal_curr'   , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_district_director'    , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_hours_operation'      , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_market_director'      , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_opening_lead'         , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_period_actual'        , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_qtr_actual'           , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_shop_director'        , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_shop_name'            , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_shop_name_short'      , mandatory);
	 nlapiSetFieldMandatory('custrecord_erp_dclose_shop_type'            , mandatory);
	 
	 
	 nlapiSetFieldMandatory('custrecord_erp_dclose_daily_actual'            , mandatory); 
	 nlapiSetFieldMandatory('custrecord_erp_dclose_weekly_goal'             , mandatory); 
	 nlapiSetFieldMandatory('custrecord_erp_dclose_period_goal'             , mandatory); 
	 nlapiSetFieldMandatory('custrecord_erp_dclose_qtr_goal'                , mandatory); 
	 nlapiSetFieldMandatory('custrecord_erp_dclose_actual_hours'            , mandatory); 
	 nlapiSetFieldMandatory('custrecord_erp_dclose_saleslabhour'            , mandatory); 
	 nlapiSetFieldMandatory('custrecord_erp_dclose_sptdpt'                  , mandatory); 
	 nlapiSetFieldMandatory('custrecord_erp_dclose_upt_units_pertrans'      , mandatory); 
	 nlapiSetFieldMandatory('custrecord_erp_dclose_netsuite_closing'        , mandatory); 
	 nlapiSetFieldMandatory('custrecord_erp_dclose_peak_times'              , mandatory); 
	 nlapiSetFieldMandatory('custrecord_erp_dclose_payment_terminal'        , mandatory); 
	 nlapiSetFieldMandatory('custrecord_erp_dclose_returns'                 , mandatory); 
	 //nlapiSetFieldMandatory('custrecord_erp_dclose_variance_reason'			, mandatory); 
}



function checkIfComplete(){
		
		console.log('checkIfComplete'); 
	
		 var complete = nlapiGetFieldValue('custrecord_erp_complete');
		 var setMandatory = null; 
		 
		 if(complete == 'T'){ 
			
			 setMandatory = true;
			 
		 }
		 if(complete == 'F'){
			 
			 setMandatory = false; 
		 }
		 
		 setMandatoryFields(setMandatory); 
		 						 
}





function erp_sdc_FieldChange(type, name, linenum){

	 
	 console.log("erp_sdc_CS_FieldChanged " + type);
	 
	 if(true){
		 
		 if(name == 'custrecord_erp_dclose_returns' || name == 'custrecord_erp_dclose_ecomm_returns'){
		 
		     var regular = nlapiGetFieldValue('custrecord_erp_dclose_ecomm_returns');
			 var total = nlapiGetFieldValue('custrecord_erp_dclose_returns');
			 
			 var exEcomm = total - regular;
			 
			 nlapiSetFieldValue('custrecord_erp_dclose_regreturn', exEcomm);
		 
		 }
		 
		 //Sales Comp Stuff
		 
		 if(name == 'custrecord_erp_dclose_daily_lysales' || name == 'custrecord_erp_dclose_daily_actual'){
			 
			 var sales = nlapiGetFieldValue('custrecord_erp_dclose_daily_actual');
			 var lysales = nlapiGetFieldValue('custrecord_erp_dclose_daily_lysales');
			 var comp = (lysales/sales) * 100;
			 comp = Math.round(comp);
			 
			 nlapiSetFieldValue('custrecord_erp_dclose_daily_salescomp', comp); 
			 
		 }
		 
		 if(name == 'custrecord_erp_dclose_weekly_lysales' || name == 'custrecord_erp_dclose_weekly_actual'){
		 
		 
		     var sales = nlapiGetFieldValue('custrecord_erp_dclose_weekly_actual');
			 var lysales = nlapiGetFieldValue('custrecord_erp_dclose_weekly_lysales');
			 var comp = (lysales/sales) * 100;
			 comp = Math.round(comp);
			 
			 nlapiSetFieldValue('custrecord_erp_dclose_weekly_salescomp', comp); 
		 
		 }
		 
		 if(name == 'custrecord_erp_dclose_period_lysales' || name == 'custrecord_erp_dclose_period_actual'){
		 
		     var sales = nlapiGetFieldValue('custrecord_erp_dclose_period_actual');
			 var lysales = nlapiGetFieldValue('custrecord_erp_dclose_period_lysales');
			 var comp = (lysales/sales) * 100;
			 comp = Math.round(comp);
			 
			 nlapiSetFieldValue('custrecord_erp_dclose_period_salescomp', comp);
		 
		 }
		 
		 if(name == 'custrecord_erp_dclose_qtr_lysales' || name == 'custrecord_erp_dclose_qtr_actual'){
		 
		     var sales = nlapiGetFieldValue('custrecord_erp_dclose_qtr_actual');
			 var lysales = nlapiGetFieldValue('custrecord_erp_dclose_qtr_lysales');
			 var comp = (lysales/sales) * 100;
			 comp = Math.round(comp);
			 
			 nlapiSetFieldValue('custrecord_erp_dclose_qtr_salescomp', comp);
		 
		 }
 
		 
		 
		 
		 
		 if(name == 'custrecord_erp_complete'){ 
			 checkIfComplete(); 
		 }
		 
		 if(name == 'custrecord_erp_dclose_shop_name'){

			 resetAllFields();
			 recalcClosingNotes();

		 }

		 

		 if(name == 'custrecord_erp_date_created'){

			 resetAllFields();			 
			 recalcClosingNotes()

		 }



			if(name == 'custrecord_erp_dclose_daily_goal' || name == 'custrecord_erp_dclose_daily_actual'){

//	 			var dailyGoal = nlapiGetFieldValue('custrecord_erp_dclose_daily_goal');
//	 			var  dailyActual = nlapiGetFieldValue('custrecord_erp_dclose_daily_actual');
//
//	 			var diff = dailyGoal - dailyActual;
//	 			var perc = (dailyActual / dailyGoal) * 100;
//
//	 			nlapiSetFieldValue('custrecord_erp_dclose_daily_to_goal_curr', diff );
//	 			nlapiSetFieldValue('custrecord_erp_dclose_daily_perc_to_goal', perc );
				
				recalcActualVSGoals('daily'); 

	 		}
			
			if(name == 'custrecord_erp_dclose_weekly_goal' || name == 'custrecord_erp_dclose_weekly_actual'){

//	 			var weekGoal = nlapiGetFieldValue('custrecord_erp_dclose_weekly_goal');
//	 			var  weekActual = nlapiGetFieldValue('custrecord_erp_dclose_weekly_actual');
//
//	 			var weekdiff = weekGoal - weekActual;
//	 			var weekperc = (weekActual / weekGoal) * 100;
//
//	 			nlapiSetFieldValue('custrecord_erp_dclose_weekly_to_goal', weekdiff );
//	 			nlapiSetFieldValue('custrecord_erp_dclose_weekly_perc_to_goa', weekperc );

				recalcActualVSGoals('weekly');
				
	 		}
			
			if(name == 'custrecord_erp_dclose_period_goal' || name == 'custrecord_erp_dclose_period_actual'){

//	 			var periodGoal = nlapiGetFieldValue('custrecord_erp_dclose_period_goal');
//	 			var  periodActual = nlapiGetFieldValue('custrecord_erp_dclose_period_actual');
//
//	 			var perioddiff = periodGoal - periodActual;
//	 			var periodperc = (periodActual/periodGoal ) * 100;
//
//	 			nlapiSetFieldValue('custrecord_erp_dclose_period_to_goal', perioddiff );
//	 			nlapiSetFieldValue('custrecord_erp_dclose_period_perc_to_goa', periodperc );
				
				recalcActualVSGoals('period');

	 		}
			
			if(name == 'custrecord_erp_dclose_qtr_goal' || name == 'custrecord_erp_dclose_qtr_actual'){

//	 			var qtrGoal = nlapiGetFieldValue('custrecord_erp_dclose_qtr_goal');
//	 			var qtrActual = nlapiGetFieldValue('custrecord_erp_dclose_qtr_actual');
//
//	 			var qtrdiff = qtrGoal - qtrActual;
//	 			var qtrperc = (qtrActual / qtrGoal ) * 100;
//
//				console.log(qtrGoal);
//				console.log(qtrActual);
//				console.log(qtrdiff);
//				console.log(qtrperc);
//				
//	 			nlapiSetFieldValue('custrecord_erp_dclose_qtr_to_goal', qtrdiff );
//	 			nlapiSetFieldValue('custrecord_erp_dclose_qtr_perc_to_goal', qtrperc );
				
				recalcActualVSGoals('qtr');

	 		}
			
			if(name == 'custrecord_erp_dclose_netsuite_closing' || name == 'custrecord_erp_dclose_payment_terminal' ){
				
				var nsClosing = nlapiGetFieldValue('custrecord_erp_dclose_netsuite_closing');
	 			var terminal = nlapiGetFieldValue('custrecord_erp_dclose_payment_terminal');

	 			var varianceNSTerminal =  terminal - nsClosing;
				nlapiSetFieldValue('custrecord_erp_dclose_variance', varianceNSTerminal );
			}


	 		if(name == 'custrecord_erp_dclose_actual_hours'){

	 				var  dailyActual = nlapiGetFieldValue('custrecord_erp_dclose_daily_actual');
	 				var  dailyActualHours = nlapiGetFieldValue('custrecord_erp_dclose_actual_hours');

	 				//if(dailyActualHours == ''){
	 					var salesLabHour = dailyActual / dailyActualHours;

	 					nlapiSetFieldValue('custrecord_erp_dclose_saleslabhour', salesLabHour);
	 				//}
	 		}
		 
		 
		 
	 }else{
		 
		 return true;
	 }
	 
	


 }












//function getPeriodDateRanges(todayDate){
//
////	console.log('getPeriodDateRanges: ' + todayDate );
//
//	var salesArrayIds = new Array();
//	var salesArrayObj = new Array();
//
//	var salesObject = new Object();
//
//	var objDateRange = {
//			"period": {},
//			"quarter": {},
//			"year":{},
//		};
//
//	var strSavedSearchIDDatePeriod = null;
//	var arSaveSearchFiltersDatePeriod = new Array();
//	var arSavedSearchColumnsDatePeriod = new Array();
//	var arSavedSearchResultsDatePeriod = null;
//
//
//	var arSavedSearchResultsDatePeriod = nlapiLoadSearch('AccountingPeriod', 'customsearch_erp_accounting_period_2');
//
//	//filters[0] = new nlobjSearchFilter('formulatext', null, 'startswith', 'a');
//
//	//arSaveSearchFiltersDatePeriod.push(new nlobjSearchFilter('formulatext', "case when (    to_date(" + todayDate + ", 'MM/DD/YYYY')      >= {startdate}) then 'true' else 'false' end)", 'is', 'true');
//	//arSaveSearchFiltersDatePeriod.push(new nlobjSearchFilter('formulatext', "case when(			to_date(" + todayDate + ", 'MM/DD/YYYY')  		   <= {enddate})   then 'true' else 'false' end)", 'is', 'true');
//
//	arSaveSearchFiltersDatePeriod[0] = new nlobjSearchFilter('formulatext', null, 'is', 'true');
//	arSaveSearchFiltersDatePeriod[0].setFormula("case when (    to_date('" + todayDate + "', 'MM/DD/YYYY')      >= {startdate}) then 'true' else 'false' end");
//
//	arSaveSearchFiltersDatePeriod[1] = new nlobjSearchFilter('formulatext', null, 'is', 'true');
//	arSaveSearchFiltersDatePeriod[1].setFormula("case when(to_date('" + todayDate + "', 'MM/DD/YYYY')  <= {enddate}) then 'true' else 'false' end");
//
//	arSavedSearchResultsDatePeriod.addColumns(arSavedSearchColumnsDatePeriod);
//	arSavedSearchResultsDatePeriod.addFilters(arSaveSearchFiltersDatePeriod);
//
//	var resultSet = arSavedSearchResultsDatePeriod.runSearch();
//
//	if(!isNullOrEmpty(resultSet)){
//
//		var results = resultSet.getResults(0,1000);
//		//var objDateRange = new Object();
//
//
//
//		if(results){
//
//			for(var i = 0; i < results.length; i++){
//
//				//var idCurrentItem = results[i].getValue('internalid');
//				var columns = results[i].getAllColumns();
//				var objResult = results[i];
//
//
//				var year = objResult.getValue(columns[5]);
//				var quarter = objResult.getValue(columns[6]);
//				var start = objResult.getValue(columns[3]);
//				var end = objResult.getValue(columns[4]);
//				var thisLoop = '';
//
//
//				if(year == 'F' && quarter == 'F'){
//					thisLoop = 'period';
//					objDateRange.period.start= objResult.getValue(columns[3]);
//					objDateRange.period.end = objResult.getValue(columns[4]);
//				}
//
//				if(year == 'F' && quarter == 'T'){
//					thisLoop = 'quarter';
//					objDateRange.quarter.start = objResult.getValue(columns[3]);
//					objDateRange.quarter.end = objResult.getValue(columns[4]);
//				}
//
//
//				if(year == 'T' && quarter == 'F'){
//					thisLoop = 'year';
//					objDateRange.year.start = objResult.getValue(columns[3]);
//					objDateRange.year.end = objResult.getValue(columns[4]);
//				}
//
////				console.log(thisLoop + ' year:' + year + ' quarter:' + quarter + ' start:' + start + ' end:' + end);
//
//
//			}
//
////			console.log(JSON.stringify(objDateRange));
//			nlapiLogExecution('DEBUG', 'DATE RANGE0', JSON.stringify(objDateRange));
//
//	}
//
//		return objDateRange;
//}
//	}



