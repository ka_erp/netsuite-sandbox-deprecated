/*
 *	File Name		:	ERP_Logging_LIB.js
 *	Function		:	
 * 	Remarks			:	
 *	Prepared by		:	Christopher Neal
 *	Release Dates	:	
 * 	Current Version	:	
**/

/*var IN_OUT = {};
IN_OUT.IN = 1;
IN_OUT.OUT = 2;

var SYNC_SYSTEM = {};
SYNC_SYSTEM.NETSUITE = 1;
SYNC_SYSTEM.WFX = 2;
SYNC_SYSTEM.ESB = 3;

var SYNC_STATUS = {};
SYNC_STATUS.CREATED = 1;
SYNC_STATUS.PENDING = 2;
SYNC_STATUS.ERROR = 3;

var SYNC_RECORD_TYPE = {};
SYNC_RECORD_TYPE.PURCHASE = 1;*/

/*
*	name: createTransactionLog
*	descr:  
*
*	author: 
*	@param: {} 
*	
*/
function createTransactionLog(dataIn, message, direction, fromSystem, toSystem){


	nlapiLogExecution('debug', 'creating transaction log', message);

	var logRecord = nlapiCreateRecord('customrecord_integrations_log');	
	//logRecord.nlapiSetFieldValue('custrecord_intlog_transaction_number', nlapiGetFieldValue(custrecord_router_transaction_number));
	logRecord.nlapiSetFieldValue('custrecord_intlog_in_out', direction);
	//logRecord.nlapiSetFieldValue('custrecord_intlog_record_type', nlapiGetFieldValue(custrecord_router_record_type));
	//logRecord.nlapiSetFieldValue('custrecord_intlog_index', 1);
	logRecord.nlapiSetFieldValue('custrecord_intlog_log_message', message);

	var startDate = nlapiDateToString(new Date, 'datetime');
	var endDate = nlapiDateToString(new Date, 'datetime');

	logRecord.nlapiSetFieldValue('custrecord_intlog_start', startDate);
	logRecord.nlapiSetFieldValue('custrecord_intlog_end', endDate);

	var logRecordId = nlapiSubmitRecord(logRecord);
	nlapiLogExecution('DEBUG', 'Created Log', 'Log: ' + logRecordId);



}



/*
from: WTKA_Library.js

function emptyObject(objectVar, objectName)
{
	//Check if dataIn has header level objects values set
	if(objectVar == null || String(objectVar) == '')
	{
		if(objectName == "Order" || objectName == "Orderheader" || objectName == "Orderid")	invalid_orderid = 'T';
		ErrorObj.messages[0] 			 = new Object();
		ErrorObj.messages[0].messagetype = "Bad Request";
		ErrorObj.messages[0].message 	 = objectName + " data is empty";
		return false;
	}
	return true;
}
*/