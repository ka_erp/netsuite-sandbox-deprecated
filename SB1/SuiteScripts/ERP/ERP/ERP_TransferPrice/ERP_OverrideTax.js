function setOverrideTaxAmout(){

  var taxAmountOverride = nlapiGetField('custbody_erp_override_tax');
  taxAmountOverride = parseFloat(taxAmountOverride);
  nlapiSetFieldValue('taxamountoverride', taxAmountOverride);
  nlapiSetFieldValue('taxamountoverride1', taxAmountOverride);
}

function setOverrideTaxAmoutAS(){


  var abc = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());

  var taxAmountOverride = abc.getFieldValue('custbody_erp_override_tax');
  taxAmountOverride = parseFloat(taxAmountOverride);

  abc.setFieldValue('taxamountoverride1', taxAmountOverride);
  abc.setFieldValue('taxamountoverride', taxAmountOverride);
  nlapiSubmitRecord(abc, true);

}
