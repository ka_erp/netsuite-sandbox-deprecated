/*
 *	File Name		:	ERP_Logging_LIB.js
 *	Function		:	
 * 	Remarks			:	
 *	Prepared by		:	Christopher Neal
 *	Release Dates	:	
 * 	Current Version	:	
**/
{
	var SYNC_SYSTEM = {};
	SYNC_SYSTEM.NETSUITE = 1;
	SYNC_SYSTEM.WFX = 2;
	SYNC_SYSTEM.ESB = 3;

	var DIRECTION = {};
	DIRECTION.IN = 1;
	DIRECTION.OUT = 2;

	var SYNC_STATUS = {};
	SYNC_STATUS.CREATED = 1;
	SYNC_STATUS.PENDING = 2;
	SYNC_STATUS.ERROR = 3;
}

/*
*	name: createTransactionLog
*	descr:  
*
*	author: chrisotpher.neal@kitandace.com
*	@param: {obj} dataIn 				- JSON object passed into Netsuite
*	@param: {string} extTransactionId 	- Transaction name passed into Netsuite
*	@param: {int} direction  			- Reference of the direction of the flow. (List: IN/OUT)
*	@param: {int} fromSystem        	- Reference of the systemn inititating the message (List: Router Integration Systems)
*	@param: {int} toSystem          	- Reference of the system receiving the message (List: Router Integration Systems)   
*	@param: {int} recordType        	- Reference of the record being type being synced (List: Transaction Types) 
*	@param: {int} status            	- Reference of the status of the record sync(List: Integration Status)
*	@param: {string} logMessage     	- text for the log message
*	@param: {id} nsTranId 				- Netsuite Internal id of a transaction
*	@param: {int} index 				- index value of the log
*	@return: {int} logRecordId 			- Netsuite internal id of the log record
*	
*/
function createTransactionLog(dataIn, extTransactionId, direction, fromSystem, toSystem, recordType, status, logMessage, nsTranId, index){

	//Ensure the data passed in is valid (if it is empty/null/undefined set to '')
	var dataIn_val 				= isEmpty(dataIn) ? {} : dataIn;
	var extTransactionId_val 	= isEmpty(extTransactionId) ? '' : extTransactionId;
	var direction_val 			= isEmpty(direction) ? '' : direction;
	var fromSystem_val 			= isEmpty(fromSystem) ? '' : fromSystem;
	var toSystem_val 			= isEmpty(toSystem) ? '' : toSystem;
	var recordType_val 			= isEmpty(recordType) ? '' : recordType;
	var status_val 				= isEmpty(status) ? '' : status;
	var logMessage_val 			= isEmpty(logMessage) ? '' : logMessage;
	var nsTranId_val 			= isEmpty(nsTranId) ? '' : nsTranId;
	var index_val 				= isEmpty(index) ? '' : index;

	if (false){		
		nlapiLogExecution('debug', 'dataIn', JSON.stringify(dataIn_val));
		nlapiLogExecution('debug', 'transactionId', extTransactionId_val);
		nlapiLogExecution('debug', 'direction', direction_val);
		nlapiLogExecution('debug', 'fromSystem', fromSystem_val);
		nlapiLogExecution('debug', 'toSystem', toSystem_val);
		nlapiLogExecution('debug', 'recordType', recordType_val);
		nlapiLogExecution('debug', 'status', status_val);
		nlapiLogExecution('debug', 'logMessage', logMessage_val);
		nlapiLogExecution('debug', 'nsTranId_val', nsTranId_val);
		nlapiLogExecution('debug', 'index_val', index_val);
	}

	//Create the log record	
	var logRecord = nlapiCreateRecord('customrecord_integrations_log');

	logRecord.setFieldValue('custrecord_intlog_transaction_number', extTransactionId_val);
	logRecord.setFieldValue('custrecord_intlog_in_out', direction_val);
	logRecord.setFieldValue('custrecord_intlog_log_from', fromSystem_val);
	logRecord.setFieldValue('custrecord_intlog_log_to', toSystem_val);
	logRecord.setFieldValue('custrecord_intlog_record_type', recordType_val);
	logRecord.setFieldValue('custrecord_intlog_index', index_val);
	logRecord.setFieldValue('custrecord_intlog_transaction_id', nsTranId_val);

	var startDate = getCurrentDateAsString();
	var endDate = getCurrentDateAsString();

	logRecord.setFieldValue('custrecord_intlog_start', startDate);
	logRecord.setFieldValue('custrecord_intlog_end', endDate);
	
	logRecord.setFieldValue('custrecord_intlog_status', status_val);
	logRecord.setFieldValue('custrecord_intlog_log_message', logMessage_val);

	
	var WTKA_External_Order_id = getWTKAExternalOrderId(extTransactionId);//Get the parent WTKA_Exteranl_Order ID (if it exists)			
	if (WTKA_External_Order_id != -1){
		logRecord.setFieldValue('custrecord_wtka_external_orders_link', WTKA_External_Order_id);
	}
	
	var logRecordId = nlapiSubmitRecord(logRecord);

	nlapiLogExecution('DEBUG', 'Created Intgration Log', 'Log: ' + logRecordId);

	return logRecordId;
}

/*
*	name: linkTransactionsToExternalWTKA
*	descr:  Pass in the Internal ID of the WTKA_External_Order record. The function will perform
*			a saved search to find the Netsuite Transaction Records attached to the Integration Logs associated with the WTKA_External_Order record.
*
*	author: chrisotpher.neal@kitandace.com
*	@param: {id} wtkaRecordId - Internal ID of the WTKA_External_Order record
*	
*/
function linkTransactionsToExternalWTKA(wtkaRecordId){

	nlapiLogExecution('debug', 'Linking Transactions on the WTKA_External_Order...', 'wtkaRecordId: ' + wtkaRecordId);

	//get all the transactions related to the WTKA_External_Order record
	var cols = new Array();
	cols.push(new nlobjSearchColumn('custrecord_intlog_transaction_id'));
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wtka_external_orders_link', null, 'is', wtkaRecordId));
	var intLogs = nlapiSearchRecord('customrecord_integrations_log', null, filters, cols);	
	
	var allTransactionIds = [];
	if (intLogs != null){
		for (var i = 0; i < intLogs.length; i++){
			if (intLogs[i].getValue('custrecord_intlog_transaction_id') != ''){
				allTransactionIds.push(intLogs[i].getValue('custrecord_intlog_transaction_id'));
			}
		}		
	}

	//Check if there are no transaction IDs on the record
	if (isEmpty(allTransactionIds) == false){

		nlapiLogExecution('debug', 'allTransactionIds', JSON.stringify(allTransactionIds)); 

		var WTKA_External_Order = nlapiLoadRecord('customrecord_wtka_external_orders', wtkaRecordId); 

		if (allTransactionIds.length == 1){
			WTKA_External_Order.setFieldValue('custrecord_external_transactions_links', allTransactionIds); //use 'setFieldValue' when there is only 1 transaction to set
		} else {
			WTKA_External_Order.setFieldValues('custrecord_external_transactions_links', allTransactionIds); //use 'setFieldValues' when there is 2 or more transactions to set
		}		

		wtkaRecordId = nlapiSubmitRecord(WTKA_External_Order); //save the changes to the record

		nlapiLogExecution('debug', 'Linking transaction to WTKA_External_Order Succeeded', 'wtkaRecordId: ' + wtkaRecordId + ', allTransactionIds: ' + JSON.stringify(allTransactionIds)); 
	} else {
		nlapiLogExecution('debug', 'No transactions to link to WTKA_External_Order ', 'wtkaRecordId: ' + wtkaRecordId + ', allTransactionIds: ' + JSON.stringify(allTransactionIds)); 
	}

}

/*
*	name: getCurrentDateAsString
*	descr:  get the current date and format it to the valid format to be set to a date field
*			
*
*	author: chrisotpher.neal@kitandace.com
*	@return: {string} curDateString - current date as a string
*	
*/
function getCurrentDateAsString(){

	//var curDateString = nlapiDateToString(new Date, 'datetime');	
	var curDateString = nlapiDateToString(new Date, 'datetimetz');
	
	//var n = curDateString.indexOf('m'); //find the 'm' in 'am' or 'pm'
	//curDateString = curDateString.substr(0, n-2) + ':00' + curDateString.substr(n-2); //add ':00' to the time
	curDateString = curDateString.replace('pm', 'PM'); //replace the 'pm' with 'PM' (if it exists)
	curDateString = curDateString.replace('am', 'AM'); //replace the 'am' with 'AM' (if it exists)

	nlapiLogExecution('debug', 'DATE', curDateString); 

	return curDateString;
}


/*
*	name: getWTKAExternalOrderId
*	descr:  Get the Netsuite ID of the WTKA_External_Order using the External Order ID
*
*	author: chrisotpher.neal@kitandace.com
*	@param: {string} extTransactionId 		- Transaction name passed into Netsuite
*	@return: {id} WTKA_External_Order_id	- the ID of the WTKA_External_Order if it exists, -1 if it does not exist
*	
*/
function getWTKAExternalOrderId(extTransactionId){

	var WTKA_External_Order_id = -1;
		
	//Get WTKA_External_Order 
	var cols = new Array();

	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wtka_orderid', null, 'is', extTransactionId));
	WTKA_External_Order = nlapiSearchRecord('customrecord_wtka_external_orders', null, filters, cols);	

	if (WTKA_External_Order != null){
		WTKA_External_Order_id = WTKA_External_Order[0].id; //Note: there will only be 1 WTKA_External_Order for each external ID
	}	

	return WTKA_External_Order_id;
}

/*
*	name: isEmpty
*	descr:  determine whethere a variable has a value set
*
*	@param: {obj} val - any type of input variable
*	@return: {boolean} true: if val is set, false: if val is not set
*	
*/
function isEmpty(val){
    return (val === undefined || val == null || val.length <= 0) ? true : false;
}