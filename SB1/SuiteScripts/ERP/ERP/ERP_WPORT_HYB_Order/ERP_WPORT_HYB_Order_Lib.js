/**
 *	File Name		:	ERP_WPORT_HYB_Order_Lib.js
 *	Function		:	Order Process - Common functions
 * 	Remarks			:	Relevant scripts isolated from WTKA_NetSuite_ICTO_Process.js
 *	Prepared by		:	A.Villanueva
 *	Release Dates	:	31-May-2016
 * 	Current Version	:	1.0
**/

{
	var ErrorObj 		 		= new Object();
	ErrorObj.status				= "Error";
	ErrorObj.messages 	 		= new Array();
	
	var finalResponse			= new Object();
	finalResponse.status		= '';
	finalResponse.records		= new Array();
	
	var finalMessage 		 	= new Object();
	finalMessage.records	 	= new Array();
	
	var OrderObject 			= new Object(),	orderDet			= new Object(),	orderDetails		= new Array(); 
	var FinalInvTrfArray		= new Array();	orderShipID 		= new Array(), 	nsOrderNumber 		= new Array();
	var	RecordUPCCodes 			= new Array(),	FinalShipArray 		= new Array(), 	FinalShipQty 		= new Array();
	var orderDetails	   		= new Array(),	FinalItemIDs		= new Array(),	FinalShipOrderQty   = new Array();
	
	var headers 				= new Array();
	headers['Content-Type']  	= 'application/json';
	headers['Accept']  		 	= 'application/json';
	headers['Authorization'] 	= 'NLAuth nlauth_account=' + nlapiGetContext().getCompany() + ',nlauth_email=esbuser@kitandace.com,nlauth_signature=mules0ftpw786!,nlauth_role=1047';
	
	var dataIn, 			createLogFlag, 			TranDate, 	   					record,									finalTransformType;
	var subsidiary, 		shipmethod,    			location, 						extSysPayment,							itemfulfillmentflag;
	var InvTrfURL, 			OrderURL, 				RollbkURL, 						CA_US_Vendor, 							US_CA_Vendor;
	var CA_Virtual, 		US_Virtual, 			US_EcommDC,						CA_EcommDC,								Ext_Payment;
	var Fedex_CA, 			Fedex_US, 				UPS_CA, 						UPS_US,									Taxcode_US;
	var ediStatus = 2, 	 	recordId   = 0, 		governanceMinUsage  = 200, 		folderName = 'External System Orders',	customer  = 0;
	var dualTax   = false,	creditCard = false,		rollbackFlag 		= false,	createFulfillmentFlag 	= false, 		createInvoiceFlag   = false;
	var updatedCustomer = false,					sendEmail			= true, /*false,*/	taxCode,								customerUpdateFail	= 0;
	var ccList = null; //['']; //Enter CC Addresses as array
	var toList = ['3PL_eComm_Integration@kitandace.com'];
	
	//Email notification author ID
	var emailAuthor;
	
	//Set Environment values
	wport_getEnvironmentValues();
}

function wport_getEnvironmentValues()
{
	var stAuth = SCRIPTCONFIG.getScriptConfigValue('Hybris: End Point: Authorization');
	if(stAuth){	
		var objAuth = JSON.parse(stAuth);
		headers['Authorization'] = 'NLAuth nlauth_account=' + nlapiGetContext().getCompany() 
			+ ',nlauth_email=' + objAuth.email
			+ ',nlauth_signature=' + objAuth.pwd
			+ ',nlauth_role=' + objAuth.role;
	}
	
	var stCCList = SCRIPTCONFIG.getScriptConfigValue('Hybris: Email Notification: CC List');
	if(stCCList) ccList = JSON.parse(stCCList);
	
	var stToList = SCRIPTCONFIG.getScriptConfigValue('Hybris: Email Notification: To List');
	if(stToList) toList = JSON.parse(stToList);
	
	emailAuthor = SCRIPTCONFIG.getScriptConfigValue('Hybris: Email Notification: Author');
	
	folderName = SCRIPTCONFIG.getScriptConfigValue('External System Orders Folder Name');
	
	CA_US_Vendor = SCRIPTCONFIG.getScriptConfigValue('CA_US_Vendor'); //SW US Inc. - Inventory - Canada (Inventory)
	US_CA_Vendor = SCRIPTCONFIG.getScriptConfigValue('US_CA_Vendor'); //Kit and Ace Designs Inc. - USA (Inventory)
	
	CA_EcommDC = SCRIPTCONFIG.getScriptConfigValue('CA_EcommDC'); // 1901
	US_EcommDC = SCRIPTCONFIG.getScriptConfigValue('US_EcommDC'); // 2901
	CA_Virtual = SCRIPTCONFIG.getScriptConfigValue('CA_Virtual'); // 1902
	US_Virtual = SCRIPTCONFIG.getScriptConfigValue('US_Virtual'); // 2902
	
	//Environment Sensitive Variables
	Ext_Payment	= SCRIPTCONFIG.getScriptConfigValue('External System Payment'); // External System Payment
	InvTrfURL 	= SCRIPTCONFIG.getScriptConfigValue('Hybris: End Point: Inventory Transfer'); 
	OrderURL 	= SCRIPTCONFIG.getScriptConfigValue('Hybris: End Point: Sales Order'); 
	RollbkURL	= SCRIPTCONFIG.getScriptConfigValue('Hybris: End Point: Rollback');
	Fedex_CA	= SCRIPTCONFIG.getScriptConfigValue('Fedex_CA');
	Fedex_US	= SCRIPTCONFIG.getScriptConfigValue('Fedex_US');
	UPS_CA		= SCRIPTCONFIG.getScriptConfigValue('UPS_CA');
	UPS_US		= SCRIPTCONFIG.getScriptConfigValue('UPS_US');
	Taxcode_US	= SCRIPTCONFIG.getScriptConfigValue('Taxcode_US'); // External System - US
}

//RT START

function lookupCustomer(dataIn)
{
	nlapiLogExecution('AUDIT','NEW CUSTOMER ADDRESS!', 'This is the new method'); 
	var customerId = 0;

	var customerResp = customerUpdate(dataIn, 'order');
	if(customerResp.status != 'undefined' && customerResp.status == "Success")
	{
		customerId = customerResp.messages[0].recordId;
	}
	else
	{
		ErrorObj	= customerResp;
		var message = '<br><b>Error Details: </b><br>' + JSON.stringify(ErrorObj);
		sendOrderMail('Order', message, dataIn);
	}
	return customerId;
}

function customerUpdate(dataIn, requestType) //requestType - order request
{
	var billingCustomer = dataIn.order.orderHeader.billingCustomer;
	
	try
	{
		var customer = 0, 		custNo, 	record;
		var existing = false;
		if(PrevalidationCustomerCheck(billingCustomer))
		{
			var locale  = billingCustomer.locale.split('/');
			var country = locale[locale.length-1].toUpperCase(); //Locale - kitandace.com/ca
			if(country != 'CA')	country = 'US';
			var subsid = FetchSubsidiaryId('country', country);
			
			if(subsid == -1)
			{
				ErrorObj.messages[0] = new Object();
				ErrorObj.messages[0].status   		= "Error";
				ErrorObj.messages[0].messagetype    = "Subsidiary error";
				ErrorObj.messages[0].message 	    = "Invalid Query. Error: Subsidiary for Country: " + country + " cannot be found.";
				nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
				if(requestType == null)
				{
					sendCustomerErrorEmail(billingCustomer, ErrorObj);
					var errorLogs = LogEntityCreation(1, 4, customer, billingCustomer, 2, ErrorObj);
				}
				return ErrorObj;
			}
			
			//Lookup Existing Customers using Email
			var cols 		 = new Array();
			cols.push(new nlobjSearchColumn('entityid'));
			cols.push(new nlobjSearchColumn('subsidiary'));
			var filters 	 = new Array();
			filters.push(new nlobjSearchFilter('isinactive',  null, 'is', 	'F'));
			filters.push(new nlobjSearchFilter('email',  	  null, 'is', 	billingCustomer.customerEmail));
			filters.push(new nlobjSearchFilter('subsidiary',  null, 'is', 	subsid.code));
			var searchResult = nlapiSearchRecord('customer',  null, filters, cols);
			if(searchResult != null)
			{
				customer 	= searchResult[0].getId();
				custNo	 	= searchResult[0].getValue('entityid');
				subsidiary	= searchResult[0].getValue('subsidiary');
				existing 	= true;
			}
			
			try
			{
				if(existing)
				{
					record = nlapiLoadRecord('customer', customer, {customform: 2});//Update
					var recStat = record.getFieldValue('entitystatus');
					changed = (recStat != 13);
				}
				else
				{
					record = nlapiCreateRecord('customer', {customform: 2}); //Create
					record.setFieldValue('subsidiary', subsid.code);
					record.setFieldValue('email', 		billingCustomer.customerEmail);
					changed = true;
				}
				
				/* Set Hybris Customer ID */
				if(dataIn.order.shipment[0].shipmentHeader.recipient){
					var stCustomerId = dataIn.order.shipment[0].shipmentHeader.recipient.customerId;
					if(stCustomerId && stCustomerId != record.getFieldValue('custentity_erp_hybris_customer_id')){
						record.setFieldValue('custentity_erp_hybris_customer_id', stCustomerId);
						changed = true;
					}
				}
				
				/* Set Names */
				var firstName = " ";
				var lastName  = " ";
				if(checkEmptyObject(billingCustomer.firstName, "FirstName"))
				{
					if(existing)
					{
						var recFirstName = record.getFieldValue('firstname');
						changed = (recFirstName != billingCustomer.firstName);
					}
					firstName = billingCustomer.firstName;
				}
				if(checkEmptyObject(billingCustomer.lastName, "LastName"))
				{
					if(existing)
					{
						var recLastName = record.getFieldValue('lastname');
						changed = (recLastName != billingCustomer.lastName);
					}
					lastName = billingCustomer.lastName;
				}
				record.setFieldValue('firstname', 	firstName);
				record.setFieldValue('lastname', 	lastName);
				
				// var concatName = (lastName != " ") ? firstName + " " + lastName : firstName;
				// record.setFieldValue('firstname', 	concatName);
				
				/* Set Other Values */
				record.setFieldValue('entitystatus', 				13); //Customer - Closed Won
				record.setFieldText('globalsubscriptionstatus', 	'Soft Opt-In');
				record.setFieldValue('custentity_newsletteroptin', 	'T');		
				
				/* Set Addresses */
				//Check if address should be set
				var setBillingAddressFlag = false, 	 setShippingAddressFlag = false;
				if(emptyObject(billingCustomer.billingAddress,									"BillingAddress"))
				{
					if(checkEmptyObject(billingCustomer.billingAddress.billingFirstName,			"BillingFirstName"))			setBillingAddressFlag  = true;
					if(checkEmptyObject(billingCustomer.billingAddress.billingLastName,			"BillingLastName"))				setBillingAddressFlag  = true;
					if(checkEmptyObject(billingCustomer.billingAddress.billingAddressLine1,		"BillingAddressLine1"))			setBillingAddressFlag  = true;
					if(checkEmptyObject(billingCustomer.billingAddress.cityName,					"BillingCity"))			 		setBillingAddressFlag  = true;
					if(checkEmptyObject(billingCustomer.billingAddress.provinceName,				"BillingProvinceName"))  		setBillingAddressFlag  = true;
					if(checkEmptyObject(billingCustomer.billingAddress.postalCode,				"BillingPostalCode")) 	 		setBillingAddressFlag  = true;
					if(checkEmptyObject(billingCustomer.billingAddress.countryName,				"BillingCountry")) 		 		setBillingAddressFlag  = true;
				}
				
				if(emptyObject(billingCustomer.shippingAddress,									"ShippingAddress"))
				{
					if(checkEmptyObject(billingCustomer.shippingAddress.recipientFirstName,		"ShippingFirstName")) 	 		setShippingAddressFlag  = true;
					if(checkEmptyObject(billingCustomer.shippingAddress.recipientLastName,		"ShippingLastName"))	 		setShippingAddressFlag  = true;
					if(checkEmptyObject(billingCustomer.shippingAddress.shippingAddressLine1,	"ShippingAddressLine1"))		setShippingAddressFlag  = true;
					if(checkEmptyObject(billingCustomer.shippingAddress.cityName,				"ShippingCity"))		 		setShippingAddressFlag  = true;
					if(checkEmptyObject(billingCustomer.shippingAddress.provinceName,			"ShippingProvinceName")) 		setShippingAddressFlag  = true;
					if(checkEmptyObject(billingCustomer.shippingAddress.postalCode,				"ShippingPostalCode"))	 		setShippingAddressFlag  = true;
					if(checkEmptyObject(billingCustomer.shippingAddress.countryName,				"ShippingCountry"))		 		setShippingAddressFlag  = true;
				}
				
				if(checkEmptyObject(billingCustomer.customerPhoneNumber,							"CustomerPhoneNumber"))
				{
					record.setFieldValue('phone', 		billingCustomer.customerPhoneNumber);
				}
				
				var sameAddressList = false;
				if(setBillingAddressFlag && setShippingAddressFlag && compareAddresses(billingCustomer))
				{
					sameAddressList = true;
				}
				
				if(setBillingAddressFlag)
				{
					var subrecord;
					var billingIndex = findNewAddress(record, billingCustomer, 'billingAddress');
					// nlapiLogExecution('debug', 'billingIndex', billingIndex);
					if(billingIndex > 0) // Do Nothing - Match
					{
						record.selectLineItem('addressbook', billingIndex);
						if(record.getCurrentLineItemValue('addressbook', 'defaultbilling') 		!= 'T')	changed = true;
						record.setCurrentLineItemValue('addressbook', 'defaultbilling',  'T');
						if(sameAddressList)
						{
							if(record.getCurrentLineItemValue('addressbook', 'defaultshipping') != 'T')	changed = true;
							record.setCurrentLineItemValue('addressbook', 'defaultshipping',  'T');
						}
						record.setCurrentLineItemValue('addressbook', 'isresidential',   'F'); 
						subrecord = record.editCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
					}
					else
					{
						changed = true;
						record.selectNewLineItem('addressbook');
						record.setCurrentLineItemValue('addressbook', 'defaultbilling',  'T');
						if(sameAddressList)	record.setCurrentLineItemValue('addressbook', 'defaultshipping',  'T');
						record.setCurrentLineItemValue('addressbook', 'isresidential',   'F'); 
						subrecord = record.createCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
					
						var firstName = " ";
						var lastName  = " ";
						if(checkEmptyObject(billingCustomer.billingAddress.billingFirstName,		"BillingFirstName"))	firstName 	 = billingCustomer.billingAddress.billingFirstName;
						if(checkEmptyObject(billingCustomer.billingAddress.billingLastName,	"BillingLastName"))			lastName = billingCustomer.billingAddress.billingLastName;
						subrecord.setFieldValue('addressee', firstName + ' ' + lastName);
						
						if(checkEmptyObject(billingCustomer.customerPhoneNumber,					"CustomerPhoneNumber"))		subrecord.setFieldValue('addrphone', 		billingCustomer.customerPhoneNumber);						
						if(checkEmptyObject(billingCustomer.billingAddress.billingAddressLine1,	"BillingAddressLine1"))		subrecord.setFieldValue('addr1', 			billingCustomer.billingAddress.billingAddressLine1);
						if(checkEmptyObject(billingCustomer.billingAddress.billingAddressLine2,	"BillingAddressLine2"))		subrecord.setFieldValue('addr2', 			billingCustomer.billingAddress.billingAddressLine2);
						if(checkEmptyObject(billingCustomer.billingAddress.cityName,				"BillingCity"))				subrecord.setFieldValue('city', 			billingCustomer.billingAddress.cityName);
						if(checkEmptyObject(billingCustomer.billingAddress.provinceName,			"BillingProvinceName"))		subrecord.setFieldValue('state', 			billingCustomer.billingAddress.provinceName);
						if(checkEmptyObject(billingCustomer.billingAddress.countryName,			"BillingCountry"))			subrecord.setFieldValue('country', 			billingCustomer.billingAddress.countryName);
						if(checkEmptyObject(billingCustomer.billingAddress.postalCode,			"BillingPostalCode"))		subrecord.setFieldValue('zip', 				billingCustomer.billingAddress.postalCode);
					}
					subrecord.commit();
					record.commitLineItem('addressbook');
				}
				if(setShippingAddressFlag && !sameAddressList)
				{
					var subrecord;
					var shippingIndex = findNewAddress(record, billingCustomer, 'shippingAddress');
					// nlapiLogExecution('debug', 'shippingIndex', shippingIndex);
					if(shippingIndex > 0) // Do Nothing - Match
					{
						record.selectLineItem('addressbook', shippingIndex);
						if(record.getCurrentLineItemValue('addressbook', 'defaultshipping') != 'T')	changed = true;
						record.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T');
						record.setCurrentLineItemValue('addressbook', 'isresidential',   'F'); 
						subrecord = record.editCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
					}
					else
					{
						changed = true;
						record.selectNewLineItem('addressbook');
						record.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T');
						record.setCurrentLineItemValue('addressbook', 'isresidential',   'F'); 
						subrecord = record.createCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
						
						var firstName = " ";
						var lastName  = " ";
						if(checkEmptyObject(billingCustomer.shippingAddress.recipientFirstName,		"ShippingFirstName"))	firstName 	 = billingCustomer.shippingAddress.recipientFirstName;
						if(checkEmptyObject(billingCustomer.shippingAddress.recipientLastName, 		"ShippingLastName"))	lastName = billingCustomer.shippingAddress.recipientLastName;
						subrecord.setFieldValue('addressee', firstName + ' ' + lastName);
						
						if(checkEmptyObject(billingCustomer.shippingAddress.recipientPhoneNumber,	"RecipientPhoneNumber"))	subrecord.setFieldValue('addrphone', 		billingCustomer.shippingAddress.recipientPhoneNumber);						
						if(checkEmptyObject(billingCustomer.shippingAddress.shippingAddressLine1,	"ShippingAddressLine1"))	subrecord.setFieldValue('addr1', 			billingCustomer.shippingAddress.shippingAddressLine1);
						if(checkEmptyObject(billingCustomer.shippingAddress.shippingAddressLine2,	"ShippingAddressLine2"))	subrecord.setFieldValue('addr2', 			billingCustomer.shippingAddress.shippingAddressLine2);
						if(checkEmptyObject(billingCustomer.shippingAddress.cityName,				"ShippingCity"))			subrecord.setFieldValue('city', 			billingCustomer.shippingAddress.cityName);
						if(checkEmptyObject(billingCustomer.shippingAddress.provinceName,			"ShippingProvinceName"))	subrecord.setFieldValue('state', 			billingCustomer.shippingAddress.provinceName);
						if(checkEmptyObject(billingCustomer.shippingAddress.postalCode,				"ShippingPostalCode"))		subrecord.setFieldValue('zip', 				billingCustomer.shippingAddress.postalCode);
						if(checkEmptyObject(billingCustomer.shippingAddress.countryName,				"ShippingCountry"))			subrecord.setFieldValue('country', 			billingCustomer.shippingAddress.countryName);
					}	
					subrecord.commit();
					record.commitLineItem('addressbook');
				}
				
				if(requestType != null)
				{
					/* Set tax code */
					/* var state = dataIn.shippingAddress.provinceName.split('-');
					var province = state[state.length-1].toUpperCase();
					taxCode  	 = FetchTaxCode(country, province);
					
					if(existing)
					{
						var recTax = record.getFieldValue('taxitem');
						changed = (recTax != taxCode);
					}
					// record.setFieldValue('taxable', 'F');
					record.setFieldValue('taxitem', taxCode); */
					
					/* Set Multi-currency */
					var cols 		 = new Array();
					cols.push(new nlobjSearchColumn('name'));
					var filters 	 = new Array();
					var searchResult = nlapiSearchRecord('currency', null, filters, cols);
					var currencyList = new Array();
					
					if(searchResult != null && searchResult.length != record.getLineItemCount('currency'))
					{
						for(var i=0; searchResult != null && i<searchResult.length; i++)
						{
							var flag = false;
							var listCurrency = searchResult[i].getValue('name');
							for(var j=0; j<record.getLineItemCount('currency'); j++)
							{
								var recCurrency  = record.getLineItemText('currency', 'currency', j+1);
								if(recCurrency == listCurrency)
								{
									flag = true;
									break;
								}
							}
							if(!flag)	currencyList.push(searchResult[i].getId());
						}
						if(currencyList.length > 0)
						{
							for(var i= 0; i<currencyList.length; i++)
							{
								record.selectNewLineItem('currency');
								record.setCurrentLineItemValue('currency', 'currency', currencyList[i]);
								record.commitLineItem('currency');
							}
							changed = true;
						}
					}
				}
								
				if(changed)	customer = nlapiSubmitRecord(record, true, true); //Submit only for changes
				
				if(!existing)
				{
					var customerRecord 	= nlapiLoadRecord('customer', customer);
					custNo	   	   		= customerRecord.getFieldValue('entityid');
					subsidiary	  		= customerRecord.getFieldValue('subsidiary');
				}
				
				var SuccessObj 						= new Object();
				SuccessObj.status 					= "Success";
				SuccessObj.messages 				= new Array();
				SuccessObj.messages[0] 				= new Object();
				SuccessObj.messages[0].recordId 	= customer;
				SuccessObj.messages[0].customerId 	= custNo;
				SuccessObj.messages[0].message 		= "Customer successfully ";
				SuccessObj.messages[0].message 		+= (existing) ? "updated." : "created.";
				nlapiLogExecution('debug', 'SuccessObj', JSON.stringify(SuccessObj));
				var customerLogs = LogEntityCreation(1, 4, customer, billingCustomer, 1, SuccessObj);
				return SuccessObj;
			}
			catch(err)
			{
				if(err.code == 'RCRD_HAS_BEEN_CHANGED') // Retry customer update
				{
					nlapiLogExecution('debug', 'Multiple Customer Update: ' + dataIn.order.orderHeader.orderId, 'Retrying customer master update');
					if(customerUpdateFail < 3) //Maximum re-try 2 times
					{
						customerUpdateFail++;
						return (customerUpdate(dataIn, requestType));
					}
					else
					{
						ErrorObj.status   				 = "Exception";
						ErrorObj.messages[0] 			 = new Object();
						ErrorObj.messages[0].message 	 = "Customer could not be created or updated";
						ErrorObj.messages[0].message 	+= " Details as below."; 
						ErrorObj.messages[0].details 	 = err;
						nlapiLogExecution('debug', 'ErrorObj: ', JSON.stringify(ErrorObj));
						if(requestType == null)
						{
							sendCustomerErrorEmail(billingCustomer, ErrorObj);
							var errorLogs = LogEntityCreation(1, 4, customer, billingCustomer, 2, ErrorObj);			
						}
						return ErrorObj;
					}
				}
				else
				{
					ErrorObj.status 	 = 'Exception'; 
					ErrorObj.messages[0] = new Object();
					ErrorObj.messages[0].messagetype 	= (existing) ? "Customer Update Failure" : "Customer Creation Failure";
					ErrorObj.messages[0].message 		= (existing) ? "Customer " + custNo + " could not be updated" : "Customer could not be created.";
					ErrorObj.messages[0].message 		+= " Details as below."; 
					ErrorObj.messages[0].details 		= err.getCode() + ' : ' + err.getDetails(); //err;
					nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
					if(requestType == null)
					{
						sendCustomerErrorEmail(billingCustomer, ErrorObj);
						var errorLogs = LogEntityCreation(1, 4, customer, billingCustomer, 2, ErrorObj);			
					}
					return ErrorObj;
				}
			}
		}
		else
		{
			nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
			if(requestType == null)
			{
				sendCustomerErrorEmail(billingCustomer, ErrorObj);
				var errorLogs = LogEntityCreation(1, 4, customer, billingCustomer, 2, ErrorObj);	
			}
			return ErrorObj;
		}
	}
	catch(any_err)
	{
		ErrorObj.status 	 = 'Exception'; 
		ErrorObj.messages[0] = new Object();
		ErrorObj.messages[0].messagetype 	= "Customer Update Failure";
		ErrorObj.messages[0].message 		= "Customer could not be created or updated";
		ErrorObj.messages[0].message 		+= " Details as below."; 
		ErrorObj.messages[0].details 		= any_err;
		nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
		if(requestType == null)
		{
			sendCustomerErrorEmail(billingCustomer, ErrorObj);
			var errorLogs = LogEntityCreation(1, 4, customer, billingCustomer, 2, ErrorObj);	
		}
		return ErrorObj;
	}
}

function PrevalidationCustomerCheck(dataIn)
{
	//Pre-Validation checks on DataIn
	
	//1. Check if dataIn is empty
	if(!emptyInbound(dataIn))												return false;

	//2. Check if dataIn has header level objects values set
	if(!checkEmptyObject(dataIn.customerEmail, 		"CustomerEmail"))		return false;
	if(!checkEmptyObject(dataIn.locale, 			"Locale"))				return false;
	
	return true;
}

function compareAddresses(dataIn)
{
	// if(checkEmptyObject(dataIn.shippingAddress.recipientPhoneNumber,		"RecipientPhoneNumber") && checkEmptyObject(dataIn.customerPhoneNumber,					"CustomerPhoneNumber"))	
	// {
		// if(dataIn.shippingAddress.recipientPhoneNumber != dataIn.customerPhoneNumber)						return false;
	// }
	if(checkEmptyObject(dataIn.shippingAddress.shippingAddressLine1,		"ShippingAddressLine1") && checkEmptyObject(dataIn.billingAddress.billingAddressLine1,	"BillingAddressLine1"))
	{
		if(dataIn.shippingAddress.shippingAddressLine1 != dataIn.billingAddress.billingAddressLine1)		return false;
	}
	if(checkEmptyObject(dataIn.shippingAddress.shippingAddressLine2,		"ShippingAddressLine2") && checkEmptyObject(dataIn.billingAddress.billingAddressLine2,	"BillingAddressLine2"))
	{
		if(dataIn.shippingAddress.shippingAddressLine2 != dataIn.billingAddress.billingAddressLine2)		return false;
	}
	if(checkEmptyObject(dataIn.shippingAddress.cityName,					"ShippingCity") 		&& checkEmptyObject(dataIn.billingAddress.cityName,				"BillingCity"))
	{
		if(dataIn.shippingAddress.cityName 				!= dataIn.billingAddress.cityName)					return false;
	}
	
	if(checkEmptyObject(dataIn.shippingAddress.provinceName,				"ShippingProvinceName")	&& checkEmptyObject(dataIn.billingAddress.provinceName,			"BillingProvinceName"))
	{
		if(dataIn.shippingAddress.provinceName != dataIn.billingAddress.provinceName)						return false;
	}
	if(checkEmptyObject(dataIn.shippingAddress.countryName,				"BillingCountry")			&& checkEmptyObject(dataIn.billingAddress.countryName,			"BillingCountry"))
	{
		if(dataIn.shippingAddress.countryName 			!= dataIn.billingAddress.countryName)				return false;
	}
	if(checkEmptyObject(dataIn.shippingAddress.postalCode,				"BillingPostalCode")		&& checkEmptyObject(dataIn.billingAddress.postalCode,			"BillingPostalCode"))
	{
		if(dataIn.shippingAddress.postalCode 			!= dataIn.billingAddress.postalCode)				return false;
	}
	// nlapiLogExecution('debug', 'Same Billing & Shipping Address');
	return true;
}

function findNewAddress(record, dataIn, type)
{
	var index 			= 0; //First line (new)
	var existingAddress = record.getLineItemCount('addressbook');
	// nlapiLogExecution('debug', 'existingAddress', existingAddress);
	var countDiff 		= 0;
	var i				= 1;
	for(; i<=existingAddress; i++) 
	{
		var change = false;
		// record.removeLineItem('addressbook', i);
		record.selectLineItem('addressbook', i);
		var subrecord = record.editCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
		
		if(checkDuplicate(dataIn, type, subrecord))
		{
			// nlapiLogExecution('debug', 'change: ' + i, countDiff); 
			countDiff++;
		}
		else
		{
			// nlapiLogExecution('debug', 'No change');
			index = i;
			break;
		}
	}
	return index;
}

function checkDuplicate(dataIn, type, subrecord)
{
	if(type == 'billingAddress')
	{
		// if(checkEmptyObject(dataIn.customerPhoneNumber,						"CustomerPhoneNumber"))	
		// {
			// if(dataIn.customerPhoneNumber 					!= subrecord.getFieldValue('addrphone'))		return true;
		// }
		if(checkEmptyObject(dataIn.billingAddress.billingAddressLine1,		"BillingAddressLine1"))
		{
			if(dataIn.billingAddress.billingAddressLine1 	!= subrecord.getFieldValue('addr1'))			return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.billingAddressLine2,		"BillingAddressLine2"))
		{
			if(dataIn.billingAddress.billingAddressLine2 	!= subrecord.getFieldValue('addr2'))			return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.cityName,					"BillingCity"))
		{
			if(dataIn.billingAddress.cityName 				!= subrecord.getFieldValue('city'))				return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.provinceName,				"BillingProvinceName"))
		{
			if(dataIn.billingAddress.provinceName 			!= subrecord.getFieldValue('state'))			return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.countryName,				"BillingCountry"))
		{
			if(dataIn.billingAddress.countryName 			!= subrecord.getFieldValue('country'))			return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.postalCode,				"BillingPostalCode"))
		{
			if(dataIn.billingAddress.postalCode 			!= subrecord.getFieldValue('zip'))				return true;
		}
	}
	else if(type == 'shippingAddress')
	{
		// if(checkEmptyObject(dataIn.shippingAddress.recipientPhoneNumber,		"RecipientPhoneNumber"))	
		// {
			// if(dataIn.shippingAddress.recipientPhoneNumber 	!= subrecord.getFieldValue('addrphone'))		return true;
		// }
		if(checkEmptyObject(dataIn.shippingAddress.shippingAddressLine1,		"ShippingAddressLine1"))
		{
			if(dataIn.shippingAddress.shippingAddressLine1 	!= subrecord.getFieldValue('addr1'))			return true;
		}
		if(checkEmptyObject(dataIn.shippingAddress.shippingAddressLine2,		"ShippingAddressLine2"))
		{
			if(dataIn.shippingAddress.shippingAddressLine2 	!= subrecord.getFieldValue('addr2'))			return true;
		}
		if(checkEmptyObject(dataIn.shippingAddress.cityName,					"ShippingCity"))
		{
			if(dataIn.shippingAddress.cityName 				!= subrecord.getFieldValue('city'))				return true;
		}
		
		if(checkEmptyObject(dataIn.shippingAddress.provinceName,				"ShippingProvinceName"))
		{
			if(dataIn.shippingAddress.provinceName 			!= subrecord.getFieldValue('state'))			return true;
		}
		if(checkEmptyObject(dataIn.shippingAddress.countryName,				"BillingCountry"))
		{
			if(dataIn.shippingAddress.countryName 			!= subrecord.getFieldValue('country'))			return true;
		}
		if(checkEmptyObject(dataIn.shippingAddress.postalCode,				"BillingPostalCode"))
		{
			if(dataIn.shippingAddress.postalCode 			!= subrecord.getFieldValue('zip'))				return true;
		}
	}
	return false;
}

function checkEmptyObject(objectVar, objectName) //additional check for unknown
{
	//Check if dataIn has header level objects values set
	if(!emptyObject(objectVar, objectName))
	{
		return false;
	}
	else if(objectVar.match(/unknown/gi) != null)
	{
		ErrorObj.messages[0] 			 = new Object();
		ErrorObj.messages[0].messagetype = "Bad Request";
		ErrorObj.messages[0].message 	 = objectName + " data is unknown";
		return false;
	}
	else	return true;
}



function sendCustomerErrorEmail(request, message)
{
	var subject = 'Customer Opt-In failure';
	var body 	= 'Hello,<br><br>';
	body 		+= 'Customer creation and Opt-In request failed in NetSuite due to below error.<br>';
	body 		+= JSON.stringify(message);
	body 		+= '<br><br><b>Data received in NetSuite: </b><br><br>' + JSON.stringify(request) + '<br><br><br>';
	body 		+= '<br><br>Process needs to be resubmitted or handled manually through user interface after corrections.';
	body 		+= '<br><br><br><br><br><br><br>Thanks';
	body 		+= '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
	nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
	if(sendEmail)	nlapiSendEmail(243423, toList, subject, body, ccList);
}

function LogEntityCreation(ediType, ediNumber, sourceEntity, dataIn, ediStatus, ediResponse)
{
	var parentId = 0;
	var cols 	 = new Array();
	cols[0]  	 = new nlobjSearchColumn('custrecord_wtka_entity');
	cols[1]  	 = new nlobjSearchColumn('custrecord_wtka_entity_editype');
	cols[2]  	 = new nlobjSearchColumn('custrecord_wtka_entity_edinumber');
	
	var filters  = new Array();
	filters[0]   = new nlobjSearchFilter('custrecord_wtka_entity', 				null, 'anyof', sourceEntity);
	filters[1]   = new nlobjSearchFilter('custrecord_wtka_entity_editype', 		null, 'anyof', ediType);
	filters[2]   = new nlobjSearchFilter('custrecord_wtka_entity_edinumber', 	null, 'anyof', ediNumber);

	var searchRecord = nlapiSearchRecord('customrecord_wtka_entitylogs', null, filters, cols);
	if(searchRecord != null)
	{
		for(var i=0;  i < searchRecord.length ; i++)
		{
			parentId = searchRecord[i].getId()
		}
	}
	else
	{
		var logRecord = nlapiCreateRecord('customrecord_wtka_entitylogs');
		logRecord.setFieldValue('custrecord_wtka_entity_editype', 	ediType);
		logRecord.setFieldValue('custrecord_wtka_entity_edinumber', ediNumber);
		if(sourceEntity > 0) logRecord.setFieldValue('custrecord_wtka_entity',  sourceEntity);
		parentId = nlapiSubmitRecord(logRecord);
	}

	var logRecordChild = nlapiCreateRecord('customrecord_wtka_entitylogs_child');

	logRecordChild.setFieldValue('custrecord_wtka_entity_parent', 	parentId);
	logRecordChild.setFieldValue('custrecord_wtka_entity_request', 	JSON.stringify(dataIn));
	logRecordChild.setFieldValue('custrecord_wtka_entity_response', JSON.stringify(ediResponse));
	logRecordChild.setFieldValue('custrecord_wtka_entity_status', 	ediStatus);
	
	var childId = nlapiSubmitRecord(logRecordChild);
	
	var logArray = new Array();
	logArray[0]  = parentId;
	logArray[1]  = childId;
	
	//Logging for WTKA_External_Orders
	var stLogStatus = '';
	switch(ediStatus){
		case 1: 
			stLogStatus = SYNC_STATUS.CREATED;
			break;
		case 2:
		case 3:
			stLogStatus = SYNC_STATUS.ERROR;
			break;
		default:
			break;
	}
	createTransactionLog(dataIn, null, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, null, stLogStatus, JSON.stringify(ediResponse), null, 88); //Library call to WTKA_Library.js 
	
	return logArray;
}


//RT END

/*
function lookupCustomer(dataIn)
{
	nlapiLogExecution('AUDIT','YEY HERE!', 'This is the method'); 
	
	var custNo 	 = 0, 		customer = 0;
	var leadFlag = false, 	existing = false;
	var customerId = 0, 	currency = '';

	var locale  = dataIn.order.orderHeader.billingCustomer.locale.split('/');
	var country = locale[locale.length-1].toUpperCase();
	if(country != 'CA')	country = 'US';
	var subsid  = FetchSubsidiaryId('country', country);
	if(subsid == -1)
	{
		ErrorObj.messages[0] 				= new Object();
		ErrorObj.messages[0].messagetype    = "Subsidiary error";
		ErrorObj.messages[0].message 	    = "Invalid Query. Error: Subsidiary for Country: " + country + " cannot be found.";
		return 0;
	}
	
	// Lookup Tax Code //
	var state  	 = dataIn.order.orderHeader.billingCustomer.shippingAddress.provinceName.split('-');
	var province = state[state.length-1].toUpperCase();
	taxCode  	 = FetchTaxCode(country, province);

	//Lookup Existing Leads using Email
	var cols 		 = new Array();
	cols.push(new nlobjSearchColumn('entityid'));
	cols.push(new nlobjSearchColumn('subsidiary'));
	var filters 	 = new Array();
	filters.push(new nlobjSearchFilter('email',  	  null, 'is', 	dataIn.order.orderHeader.billingCustomer.customerEmail));
	filters.push(new nlobjSearchFilter('subsidiary',  null, 'is', 	subsid.code));
	filters.push(new nlobjSearchFilter('isinactive',  null, 'is', 	'F'));
	var searchResult = nlapiSearchRecord('lead', null, filters, cols);
	if(searchResult != null)
	{
		customer 	= searchResult[0].getId();
		custNo	 	= searchResult[0].getValue('entityid');
		subsidiary	= searchResult[0].getValue('subsidiary');
		existing 	= true;
		leadFlag 	= true;
	}
	else
	{
		//Lookup Existing Customer using Email
		var cols 		 = new Array();
		cols.push(new nlobjSearchColumn('entityid'));
		cols.push(new nlobjSearchColumn('subsidiary'));
		var filters 	 = new Array();
		filters.push(new nlobjSearchFilter('email',  null, 'is', dataIn.order.orderHeader.billingCustomer.customerEmail));
		filters.push(new nlobjSearchFilter('subsidiary',  null, 'is', 	subsid.code));
		filters.push(new nlobjSearchFilter('isinactive',  null, 'is', 	'F'));
		var searchResult = nlapiSearchRecord('customer', null, filters, cols);
		if(searchResult != null)
		{
			customer 	= searchResult[0].getId();
			custNo	 	= searchResult[0].getValue('entityid');
			subsidiary	= searchResult[0].getValue('subsidiary');
			existing 	= true;
		}
	}

	try
	{
		if(existing)
		{
			var type   = (leadFlag) ? 'lead' : 'customer';
			customerUpdateFail = 0;
			customerId = updateCustomer(dataIn, customer, type);
		}
		else	customerId = createCustomer(dataIn);

		if(customerId < 0)	return ErrorObj;
		else if(!existing || leadFlag)
		{
			var cusRecord = nlapiLoadRecord('customer', customerId);
			custNo	   	  = cusRecord.getFieldValue('entityid');
			subsidiary	  = cusRecord.getFieldValue('subsidiary');
		}

		if(updatedCustomer)
		{
			var SuccessObj 						= new Object();
			SuccessObj.status 					= "Success";
			SuccessObj.messages 				= new Array();
			SuccessObj.messages[0] 				= new Object();
			SuccessObj.messages[0].recordId 	= customer;
			SuccessObj.messages[0].customerId 	= custNo;
			SuccessObj.messages[0].message 		= "Customer successfully ";
			SuccessObj.messages[0].message 		+= (existing) ? "updated." : "created.";
			nlapiLogExecution('debug', 'SuccessObj: ' + dataIn.order.orderHeader.orderId, JSON.stringify(SuccessObj));
		}
		updatedCustomer = false; //Reset Global variable
	}
	catch(err)
	{
		ErrorObj.status 	 				= 'Exception';
		ErrorObj.messages[0] 				= new Object();
		ErrorObj.messages[0].messagetype 	= (existing) ? "Customer Update Failure" : "Customer Creation Failure";
		ErrorObj.messages[0].message 		= (existing) ? "Customer " + custNo + " could not be updated" : "Customer could not be created.";
		ErrorObj.messages[0].message 		+= " Details as below.";
		ErrorObj.messages[0].details 		= err.getCode() + ' : ' + err.getDetails();
		nlapiLogExecution('debug', 'ErrorObj: ' + dataIn.order.orderHeader.orderId, JSON.stringify(ErrorObj));
		var message = '<br><b>Error Details: </b><br>' + JSON.stringify(ErrorObj);
		sendOrderMail('Order', message, dataIn);
		updatedCustomer = false; //Reset Global variable		
	}
	return customerId;
}



//IDS  START

function checkEmptyObject(objectVar, objectName) //additional check for unknown
{
	//Check if dataIn has header level objects values set
	if(!emptyObject(objectVar, objectName))
	{
		return false;
	}
	else if(objectVar.match(/unknown/gi) != null)
	{
		ErrorObj.messages[0] 			 = new Object();
		ErrorObj.messages[0].messagetype = "Bad Request";
		ErrorObj.messages[0].message 	 = objectName + " data is unknown";
		return false;
	}
	else	return true;
}


function compareAddresses(dataIn)
{
	// if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.recipientPhoneNumber,		"RecipientPhoneNumber") && checkEmptyObject(dataIn.order.orderHeader.billingCustomer.customerPhoneNumber,					"CustomerPhoneNumber"))	
	// {
		// if(dataIn.order.orderHeader.billingCustomer.shippingAddress.recipientPhoneNumber != dataIn.order.orderHeader.billingCustomer.customerPhoneNumber)						return false;
	// }
	if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.shippingAddressLine1,		"ShippingAddressLine1") && checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.billingAddressLine1,	"BillingAddressLine1"))
	{
		if(dataIn.order.orderHeader.billingCustomer.shippingAddress.shippingAddressLine1 != dataIn.order.orderHeader.billingCustomer.billingAddress.billingAddressLine1)		return false;
	}
	if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.shippingAddressLine2,		"ShippingAddressLine2") && checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.billingAddressLine2,	"BillingAddressLine2"))
	{
		if(dataIn.order.orderHeader.billingCustomer.shippingAddress.shippingAddressLine2 != dataIn.order.orderHeader.billingCustomer.billingAddress.billingAddressLine2)		return false;
	}
	if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.cityName,					"ShippingCity") 		&& checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.cityName,				"BillingCity"))
	{
		if(dataIn.order.orderHeader.billingCustomer.shippingAddress.cityName 				!= dataIn.order.orderHeader.billingCustomer.billingAddress.cityName)					return false;
	}
	
	if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.provinceName,				"ShippingProvinceName")	&& checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.provinceName,			"BillingProvinceName"))
	{
		if(dataIn.order.orderHeader.billingCustomer.shippingAddress.provinceName != dataIn.order.orderHeader.billingCustomer.billingAddress.provinceName)						return false;
	}
	if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.countryName,				"BillingCountry")			&& checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.countryName,			"BillingCountry"))
	{
		if(dataIn.order.orderHeader.billingCustomer.shippingAddress.countryName 			!= dataIn.order.orderHeader.billingCustomer.billingAddress.countryName)				return false;
	}
	if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.postalCode,				"BillingPostalCode")		&& checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.postalCode,			"BillingPostalCode"))
	{
		if(dataIn.order.orderHeader.billingCustomer.shippingAddress.postalCode 			!= dataIn.order.orderHeader.billingCustomer.billingAddress.postalCode)				return false;
	}
	// nlapiLogExecution('debug', 'Same Billing & Shipping Address');
	return true;
}


function checkDuplicate(dataIn, type, subrecord)
{
	if(type == 'billingAddress')
	{
		// if(checkEmptyObject(dataIn.customerPhoneNumber,						"CustomerPhoneNumber"))	
		// {
			// if(dataIn.customerPhoneNumber 					!= subrecord.getFieldValue('addrphone'))		return true;
		// }
		if(checkEmptyObject(dataIn.billingAddress.billingAddressLine1,		"BillingAddressLine1"))
		{
			if(dataIn.billingAddress.billingAddressLine1 	!= subrecord.getFieldValue('addr1'))			return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.billingAddressLine2,		"BillingAddressLine2"))
		{
			if(dataIn.billingAddress.billingAddressLine2 	!= subrecord.getFieldValue('addr2'))			return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.cityName,					"BillingCity"))
		{
			if(dataIn.billingAddress.cityName 				!= subrecord.getFieldValue('city'))				return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.provinceName,				"BillingProvinceName"))
		{
			if(dataIn.billingAddress.provinceName 			!= subrecord.getFieldValue('state'))			return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.countryName,				"BillingCountry"))
		{
			if(dataIn.billingAddress.countryName 			!= subrecord.getFieldValue('country'))			return true;
		}
		if(checkEmptyObject(dataIn.billingAddress.postalCode,				"BillingPostalCode"))
		{
			if(dataIn.billingAddress.postalCode 			!= subrecord.getFieldValue('zip'))				return true;
		}
	}
	else if(type == 'shippingAddress')
	{
		// if(checkEmptyObject(dataIn.shippingAddress.recipientPhoneNumber,		"RecipientPhoneNumber"))	
		// {
			// if(dataIn.shippingAddress.recipientPhoneNumber 	!= subrecord.getFieldValue('addrphone'))		return true;
		// }
		if(checkEmptyObject(dataIn.shippingAddress.shippingAddressLine1,		"ShippingAddressLine1"))
		{
			if(dataIn.shippingAddress.shippingAddressLine1 	!= subrecord.getFieldValue('addr1'))			return true;
		}
		if(checkEmptyObject(dataIn.shippingAddress.shippingAddressLine2,		"ShippingAddressLine2"))
		{
			if(dataIn.shippingAddress.shippingAddressLine2 	!= subrecord.getFieldValue('addr2'))			return true;
		}
		if(checkEmptyObject(dataIn.shippingAddress.cityName,					"ShippingCity"))
		{
			if(dataIn.shippingAddress.cityName 				!= subrecord.getFieldValue('city'))				return true;
		}
		
		if(checkEmptyObject(dataIn.shippingAddress.provinceName,				"ShippingProvinceName"))
		{
			if(dataIn.shippingAddress.provinceName 			!= subrecord.getFieldValue('state'))			return true;
		}
		if(checkEmptyObject(dataIn.shippingAddress.countryName,				"BillingCountry"))
		{
			if(dataIn.shippingAddress.countryName 			!= subrecord.getFieldValue('country'))			return true;
		}
		if(checkEmptyObject(dataIn.shippingAddress.postalCode,				"BillingPostalCode"))
		{
			if(dataIn.shippingAddress.postalCode 			!= subrecord.getFieldValue('zip'))				return true;
		}
	}
	return false;
}

function findNewAddress(record, dataIn, type)
{
	var index 			= 0; //First line (new)
	var existingAddress = record.getLineItemCount('addressbook');
	// nlapiLogExecution('debug', 'existingAddress', existingAddress);
	var countDiff 		= 0;
	var i				= 1;
	for(; i<=existingAddress; i++) 
	{
		var change = false;
		// record.removeLineItem('addressbook', i);
		record.selectLineItem('addressbook', i);
		var subrecord = record.editCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
		
		if(checkDuplicate(dataIn, type, subrecord))
		{
			// nlapiLogExecution('debug', 'change: ' + i, countDiff); 
			countDiff++;
		}
		else
		{
			// nlapiLogExecution('debug', 'No change');
			index = i;
			break;
		}
	}
	return index;
}

/*

function upsertCustomerAddress(record, dataIn){
	//Entering upsrt
	nlapiLogExecution('AUDIT', 'this upsertCustomerAddress', 'upsertCustomerAddress');
	
	try{
	
	var setBillingAddressFlag = false, 	 setShippingAddressFlag = false;
	if(emptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress,									"BillingAddress"))
	{
		if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.billingFirstName,			"BillingFirstName"))			setBillingAddressFlag  = true;
		if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.billingLastName,			"BillingLastName"))				setBillingAddressFlag  = true;
		if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.billingAddressLine1,		"BillingAddressLine1"))			setBillingAddressFlag  = true;
		if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.cityName,					"BillingCity"))			 		setBillingAddressFlag  = true;
		if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.provinceName,				"BillingProvinceName"))  		setBillingAddressFlag  = true;
		if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.postalCode,				"BillingPostalCode")) 	 		setBillingAddressFlag  = true;
		if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.countryName,				"BillingCountry")) 		 		setBillingAddressFlag  = true;
	}
	
	if(emptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress,									"ShippingAddress"))
	{
		if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.recipientFirstName,		"ShippingFirstName")) 	 		setShippingAddressFlag  = true;
		if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.recipientLastName,		"ShippingLastName"))	 		setShippingAddressFlag  = true;
		if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.shippingAddressLine1,	"ShippingAddressLine1"))		setShippingAddressFlag  = true;
		if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.cityName,				"ShippingCity"))		 		setShippingAddressFlag  = true;
		if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.provinceName,			"ShippingProvinceName")) 		setShippingAddressFlag  = true;
		if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.postalCode,				"ShippingPostalCode"))	 		setShippingAddressFlag  = true;
		if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.countryName,				"ShippingCountry"))		 		setShippingAddressFlag  = true;
	}
	
	var sameAddressList = false;
	if(setBillingAddressFlag && setShippingAddressFlag && compareAddresses(dataIn))
	{
		sameAddressList = true;
	}
	
	if(setBillingAddressFlag)
	{
		var subrecord;
		var billingIndex = findNewAddress(record, dataIn, 'billingAddress');
		// nlapiLogExecution('debug', 'billingIndex', billingIndex);
		if(billingIndex > 0) // Do Nothing - Match
		{
			record.selectLineItem('addressbook', billingIndex);
			if(record.getCurrentLineItemValue('addressbook', 'defaultbilling') 		!= 'T')	changed = true;
			record.setCurrentLineItemValue('addressbook', 'defaultbilling',  'T');
			if(sameAddressList)
			{
				if(record.getCurrentLineItemValue('addressbook', 'defaultshipping') != 'T')	changed = true;
				record.setCurrentLineItemValue('addressbook', 'defaultshipping',  'T');
			}
			record.setCurrentLineItemValue('addressbook', 'isresidential',   'F'); 
			subrecord = record.editCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
		}
		else
		{
			changed = true;
			record.selectNewLineItem('addressbook');
			record.setCurrentLineItemValue('addressbook', 'defaultbilling',  'T');
			if(sameAddressList)	record.setCurrentLineItemValue('addressbook', 'defaultshipping',  'T');
			record.setCurrentLineItemValue('addressbook', 'isresidential',   'F'); 
			subrecord = record.createCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
		
			var firstName = " ";
			var lastName  = " ";
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.billingFirstName,		"BillingFirstName"))	firstName 	 = dataIn.order.orderHeader.billingCustomer.billingAddress.billingFirstName;
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.billingLastName,	"BillingLastName"))			lastName = dataIn.order.orderHeader.billingCustomer.billingAddress.billingLastName;
			subrecord.setFieldValue('addressee', firstName + ' ' + lastName);
			
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.customerPhoneNumber,					"CustomerPhoneNumber"))		subrecord.setFieldValue('addrphone', 		dataIn.order.orderHeader.billingCustomer.customerPhoneNumber);
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.billingAddressLine1,	"BillingAddressLine1"))		subrecord.setFieldValue('addr1', 			dataIn.order.orderHeader.billingCustomer.billingAddress.billingAddressLine1);
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.billingAddressLine2,	"BillingAddressLine2"))		subrecord.setFieldValue('addr2', 			dataIn.order.orderHeader.billingCustomer.billingAddress.billingAddressLine2);
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.cityName,				"BillingCity"))				subrecord.setFieldValue('city', 			dataIn.order.orderHeader.billingCustomer.billingAddress.cityName);
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.provinceName,			"BillingProvinceName"))		subrecord.setFieldValue('state', 			dataIn.order.orderHeader.billingCustomer.billingAddress.provinceName);
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.countryName,			"BillingCountry"))			subrecord.setFieldValue('country', 			dataIn.order.orderHeader.billingCustomer.billingAddress.countryName);
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.billingAddress.postalCode,			"BillingPostalCode"))		subrecord.setFieldValue('zip', 				dataIn.order.orderHeader.billingCustomer.billingAddress.postalCode);
		}
		subrecord.commit();
		record.commitLineItem('addressbook');
	}
	if(setShippingAddressFlag && !sameAddressList)
	{
		var subrecord;
		var shippingIndex = findNewAddress(record, dataIn, 'shippingAddress');
		// nlapiLogExecution('debug', 'shippingIndex', shippingIndex);
		if(shippingIndex > 0) // Do Nothing - Match
		{
			record.selectLineItem('addressbook', shippingIndex);
			if(record.getCurrentLineItemValue('addressbook', 'defaultshipping') != 'T')	changed = true;
			record.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T');
			record.setCurrentLineItemValue('addressbook', 'isresidential',   'F'); 
			subrecord = record.editCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
		}
		else
		{
			changed = true;
			record.selectNewLineItem('addressbook');
			record.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T');
			record.setCurrentLineItemValue('addressbook', 'isresidential',   'F'); 
			subrecord = record.createCurrentLineItemSubrecord('addressbook', 'addressbookaddress');
			
			var firstName = " ";
			var lastName  = " ";
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.recipientFirstName,		"ShippingFirstName"))	firstName 	 = dataIn.order.orderHeader.billingCustomer.shippingAddress.recipientFirstName;
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.recipientLastName, 		"ShippingLastName"))	lastName = dataIn.order.orderHeader.billingCustomer.shippingAddress.recipientLastName;
			subrecord.setFieldValue('addressee', firstName + ' ' + lastName);
									
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.customerPhoneNumber,						"CustomerPhoneNumber"))		subrecord.setFieldValue('addrphone', 		dataIn.order.orderHeader.billingCustomer.shippingAddress.recipientPhoneNumber);
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.shippingAddressLine1,	"ShippingAddressLine1"))	subrecord.setFieldValue('addr1', 			dataIn.order.orderHeader.billingCustomer.shippingAddress.shippingAddressLine1);
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.shippingAddressLine2,	"ShippingAddressLine2"))	subrecord.setFieldValue('addr2', 			dataIn.order.orderHeader.billingCustomer.shippingAddress.shippingAddressLine2);
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.cityName,				"ShippingCity"))			subrecord.setFieldValue('city', 			dataIn.order.orderHeader.billingCustomer.shippingAddress.cityName);
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.provinceName,			"ShippingProvinceName"))	subrecord.setFieldValue('state', 			dataIn.order.orderHeader.billingCustomer.shippingAddress.provinceName);
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.postalCode,				"ShippingPostalCode"))		subrecord.setFieldValue('zip', 				dataIn.order.orderHeader.billingCustomer.shippingAddress.postalCode);
			if(checkEmptyObject(dataIn.order.orderHeader.billingCustomer.shippingAddress.countryName,				"ShippingCountry"))			subrecord.setFieldValue('country', 			dataIn.order.orderHeader.billingCustomer.shippingAddress.countryName);
		}	
		subrecord.commit();
		record.commitLineItem('addressbook');
	}
	
	
	

	}catch(ex){ 
		nlapiLogExecution('AUDIT', 'this error', ex.toString());
//		return record; 
	}
	
	return record;
	
}


// IDS END




function updateCustomer(dataIn, customer, type)
{
	var customerId = -1;
	try
	{
		var record 	= nlapiLoadRecord(type, customer); //Update
		
		nlapiLogExecution('AUDIT', 'Trying this new log', 'Entering'); 
		record = upsertCustomerAddress(record, dataIn) // IVAN's new method
		nlapiLogExecution('AUDIT', 'Trying this new log', 'Exiting');
		
		var locale  = dataIn.order.orderHeader.billingCustomer.locale.split('/');
		var country = locale[locale.length-1].toUpperCase();
		if(country != 'CA')	country = 'US';

		customerId = setOtherCustomerValues(dataIn, record, country);
	}
	catch(update_err)
	{
		if(update_err.code == 'RCRD_HAS_BEEN_CHANGED') // Retry customer update
		{
			nlapiLogExecution('debug', 'Multiple Customer Update: ' + dataIn.order.orderHeader.orderId, 'Retrying customer master update');
			if(customerUpdateFail < 3) //Maximum re-try 2 times
			{
				customerUpdateFail++;
				customerId = updateCustomer(dataIn, customer, type);
			}
			else
			{
				ErrorObj.status   				 = "Exception";
				ErrorObj.messages[0] 			 = new Object();
				ErrorObj.messages[0].messagetype = "Customer Update error";
				ErrorObj.messages[0].message 	 = "Error: " + update_err.getCode() + " Details: " + update_err.getDetails();
				nlapiLogExecution('debug', 'ErrorObj: ' + dataIn.order.orderHeader.orderId, JSON.stringify(ErrorObj));
			}
		}
		else
		{
			ErrorObj.status   				 = "Exception";
			ErrorObj.messages[0] 			 = new Object();
			ErrorObj.messages[0].messagetype = "Customer Update error";
			ErrorObj.messages[0].message 	 = "Error: " + update_err.getCode() + " Details: " + update_err.getDetails();
			nlapiLogExecution('debug', 'ErrorObj: ' + dataIn.order.orderHeader.orderId, JSON.stringify(ErrorObj));
		}
	}
	return customerId;
}

function setOtherCustomerValues(dataIn, record, country)
{
	var setBillingAddressFlag = false, setShippingAddressFlag = false;
//	var changed = false; //IDS TEMP
	var changed = true;
	// Set tax code //
	// var recTax = record.getFieldValue('taxitem');
	// if(recTax != taxCode)
	// {
		// // record.setFieldValue('taxable', 'F');
		// record.setFieldValue('taxitem', taxCode);
		// changed = true;
	// } //

	// Set as Customer Won //
	var recStat = record.getFieldValue('entitystatus');
	if(recStat != 13)
	{
		// Create Customer or Convert Lead to Customer //
		record.setFieldValue('entitystatus', 13); //CUSTOMER - Closed Won
		changed = true;
	}
	
	//IVAN
	record.setFieldText('globalsubscriptionstatus', 	'Soft Opt-In');
	record.setFieldValue('custentity_newsletteroptin', 	'T');		
	//IVAN
	
	// Set name //
	var firstName = record.getFieldValue('firstname');
	if(emptyObject(dataIn.order.orderHeader.billingCustomer.firstName))
	{
		if(firstName != dataIn.order.orderHeader.billingCustomer.firstName)
		{
			record.setFieldValue('firstname', dataIn.order.orderHeader.billingCustomer.firstName);
			var lastName = record.getFieldValue('lastname');
			if(emptyObject(dataIn.order.orderHeader.billingCustomer.lastName) && lastName != dataIn.order.orderHeader.billingCustomer.lastName)
			{
				record.setFieldValue('lastname', dataIn.order.orderHeader.billingCustomer.lastName);
			}
			changed = true;
		}
	}
	else
	{
		record.setFieldValue('firstname', 	'||NONAME||');
		changed = true;
	}

	// Set Multi-currency //
	var cols 		 = new Array();
	cols.push(new nlobjSearchColumn('name'));
	var filters 	 = new Array();
	var searchResult = nlapiSearchRecord('currency', null, filters, cols);
	var currencyList = new Array();
	
	if(searchResult != null && searchResult.length != record.getLineItemCount('currency'))
	{
		for(var i=0; searchResult != null && i<searchResult.length; i++)
		{
			var flag = false;
			var listCurrency = searchResult[i].getValue('name');
			for(var j=0; j<record.getLineItemCount('currency'); j++)
			{
				var recCurrency  = record.getLineItemText('currency', 'currency', j+1);
				if(recCurrency == listCurrency)
				{
					flag = true;
					break;
				}
			}
			if(!flag)	currencyList.push(searchResult[i].getId());
		}
		if(currencyList.length > 0)
		{
			for(var i= 0; i<currencyList.length; i++)
			{
				record.selectNewLineItem('currency');
				record.setCurrentLineItemValue('currency', 'currency', currencyList[i]);
				record.commitLineItem('currency');
			}
			changed = true;
		}
	}
	if(changed)
	{
		var custId = nlapiSubmitRecord(record, true, true);
		updatedCustomer = true;
		return custId;
	}
	updatedCustomer = false;
	return record.getId(); //Return original 
}

function createCustomer(dataIn)
{
	var customerId = -1;
	try
	{
		var record  = nlapiCreateRecord('customer'); //Create
		var locale  = dataIn.order.orderHeader.billingCustomer.locale.split('/');
		var country = locale[locale.length-1].toUpperCase();
		var origcountry = locale[locale.length-1].toUpperCase();																		//20160628 IDS
		if(country != 'CA')	country = 'US';											
		var subsid  = FetchSubsidiaryId('country', country);

		if(subsid == -1)
		{
			ErrorObj.messages[0] 			 = new Object();
			ErrorObj.messages[0].messagetype = "Subsidiary error";
			ErrorObj.messages[0].message 	 = "Invalid Query. Error: Subsidiary for Country: " + country + " cannot be found.";
			nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
			return -1;
		}
		record.setFieldValue('subsidiary', subsid.code);
		record.setFieldValue('email', 		dataIn.order.orderHeader.billingCustomer.customerEmail);
		
		//20160628 IDS START
		
		record.setFieldText('globalsubscriptionstatus', 	'Soft Opt-In');
		record.setFieldValue('custentity_newsletteroptin', 	'T');		
		
		if(origcountry == 'UK'){ origcountry == 'GB'}
		
		record.selectNewLineItem('addressbook');
		record.setCurrentLineItemValue('addressbook', 'defaultshipping', 'T'); 
		record.setCurrentLineItemValue('addressbook', 'defaultbilling',  'T'); 
		record.setCurrentLineItemValue('addressbook', 'isresidential',   'F'); 
		record.setCurrentLineItemValue('addressbook', 'country', origcountry);
		record.commitLineItem('addressbook');
		
		//20160628 IDS END

		customerId  = setOtherCustomerValues(dataIn, record, country);
		customer 	= customerId;
		subsidiary  = subsid.code;
		updatedCustomer = true;
	}
	catch(create_err)
	{
		ErrorObj.status   				 = "Exception";
		ErrorObj.messages[0] 			 = new Object();
		ErrorObj.messages[0].messagetype = "Customer Creation error";
		ErrorObj.messages[0].message 	 = "Error: " + create_err.getCode() + " Details: " + create_err.getDetails();
		nlapiLogExecution('debug', 'ErrorObj: ' + dataIn.order.orderHeader.orderId, JSON.stringify(ErrorObj));
	}
	return customerId;
}
*/

function LookupPayment(type, currency)
{
	var paymentMethod = 0, searchName = '';
	switch(type)
	{
		case "Cash":
		case "Cheque":
		case "CreditCard":
			searchName = '(External) ' + type + ' - ' + currency;
			if(type == "CreditCard")	creditCard = true;
			break;
		case "AccountsReceivable":
		case "GiftCard":
		case "Promise To Pay":
			searchName = 'External Payment - ' + currency;
			break;
	}
	if(searchName != '')
	{
		var cols 		 = new Array();
		var filters 	 = new Array();
		filters.push(new nlobjSearchFilter('name',  null, 'is', searchName));
		var searchResult = nlapiSearchRecord('paymentmethod', null, filters, cols);
		if(searchResult != null)	paymentMethod = searchResult[0].getId();
	}
	if(paymentMethod == 0)
	{
		nlapiLogExecution('debug', 'Unable to find Payment Method');
		var message = 'Payment Method Lookup failed.<br>';
		message += '<br><b>Error Details: </b><br> Unable to find Payment Method. Defaulting Payment Method to "External System Payment"';
		message += '<br><br><b>Data received in NetSuite: </b> <br><br>Payment - ' + type + ' currency - ' + currency + '<br><br><br>';
		sendOrderMail('Order', message, dataIn);
		paymentMethod = Ext_Payment;
	}
	return paymentMethod;
}

function fetchRequestFromFile(recordFileId, recordCounter)
{
	var extFile 	= nlapiLoadFile(recordFileId);
	var fileContent = JSON.parse(extFile.getValue());
	
	var requests 	= fileContent.records;
	// nlapiLogExecution('DEBUG', 'requests length', requests.length);

	if(Array.isArray(requests) && recordCounter <= requests.length)
	{
		return requests[recordCounter];
	}
	else
	{
		nlapiLogExecution('DEBUG', 'Unable to process request', 'Invalid File Contents');
		return 0;
	}
}

function sendOrderMail(type, message, request)
{
	var subject = (type == 'Order') ? 'Order Processing failure' : 'Inventory Transfer failure';
	var body 	= 'Hello,<br><br>';
	body 		+= 'Order request failed in NetSuite due to below error.<br>';
	body 		+= message;
	if(request != null && request != '')	
	{
		body 	+= '<br><br><b>Data received in NetSuite: </b><br><br>' + JSON.stringify(request) + '<br><br><br>';
	}
	body 		+= '<br><br>Process needs to be resubmitted or handled manually through user interface after corrections.';
	body 		+= '<br><br><br><br><br><br><br>Thanks';
	body 		+= '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
	nlapiLogExecution('debug', 'EMAIL NOTIFICATION', 'SUBJECT: ' + subject + ' BODY: ' + body);
	if(sendEmail)	nlapiSendEmail(emailAuthor, toList, subject, body, ccList);
}

/**********************************
 DYNAMIC LOGIC 
 Based on Script Config settings
***********************************/
function validatePOVendor(stSellingLoc, stFulfillingLoc){
	
	//eComm
	if(stSellingLoc == stFulfillingLoc){ 
		return 1;
	}
	
	return fetchPOVendor(stSellingLoc, stFulfillingLoc);
}

function fetchPOVendor(stSellingLoc, stFulfillingLoc){
	
	var stPOVendor = 0; //Default
	
	//Use Script Config setting
	var stPOVendorMap = SCRIPTCONFIG.getScriptConfigValue('Hybris: PO Vendor Map');	
	if(stPOVendorMap){
		
		var objPOVendorMap = JSON.parse(stPOVendorMap);
		
		//Find Selling country object
		if(objPOVendorMap[stSellingLoc]){
			//Find Fulfilling country object
			if(objPOVendorMap[stSellingLoc][stFulfillingLoc]){
				//Return PO vendor id
				return SCRIPTCONFIG.getScriptConfigValue(objPOVendorMap[stSellingLoc][stFulfillingLoc]);
			}
		}
		
	} else {
		
		//Use Default logic in case Script Config was not set up
		switch(stSellingLoc){
			case 'CA':
				switch(stFulfillingLoc.toUpperCase()){
					case 'US':
						poVendor	 =  CA_US_Vendor; //SW US Inc. - Inventory - Canada (Inventory)
						break;
				}
				break;
			case 'US':
				switch(stFulfillingLoc.toUpperCase()){
					case 'CA':
						poVendor	 =  US_CA_Vendor; //Kit and Ace Designs Inc. - USA (Inventory)
						break;
				}
				break;		
		}
	}
		
	return stPOVendor;
}

function fetchShippingMethod(stCarrierName, stSubsidiary){
	
	var objReturn = {shipcarrier: 'nonups', shipmethod: 0}; //Default
	
	//Use Script Config setting
	var stShipMethodMap = SCRIPTCONFIG.getScriptConfigValue('Hybris: Shipping Method Map');	
	if(stShipMethodMap){
		
		var objShipMethodMap = JSON.parse(stShipMethodMap);

		var bCarrierFound = false;
		for(var stCarrierKey in objShipMethodMap){
			
			//Find matching Carrier
			if(stCarrierName.match(new RegExp(stCarrierKey, 'gi'))){
				
				bCarrierFound = true;
				
				//If UPS, set shipcarrier
				if(stCarrierKey.toUpperCase() == 'UPS'){
					objReturn.shipcarrier = 'ups';
				}
				
				//Set shipmethod based on subsidiary
				if(objShipMethodMap[stCarrierKey][stSubsidiary]){
					objReturn.shipmethod = SCRIPTCONFIG.getScriptConfigValue(objShipMethodMap[stCarrierKey][stSubsidiary]);
				}
			}
			
			if(bCarrierFound){
				break;
			}
		}
		
	} else {
	
		//Use Default logic in case Script Config was not set up
		var carriercode = (stCarrierName.match(/FedEx/gi) != null) ? 'FedEx' : (stCarrierName.match(/UPS/gi) != null) ? 'UPS' : null;
		switch(carriercode){
			case 'FedEx':
				objReturn.shipcarrier = 'nonups';
				switch(stSubsidiary){
					case '3': //CA
						objReturn.shipmethod = Fedex_CA;
						break;
					case '4': //US
						objReturn.shipmethod = Fedex_US;
						break;
					default:
						break;
				}
				break;

			case 'UPS':
				objReturn.shipcarrier = 'ups';
				switch(stSubsidiary){
					case '3': //CA
						objReturn.shipmethod = UPS_CA;
						break;
					case '4': //US
						objReturn.shipmethod = UPS_US;
						break;
					default:
						break;
				}
				break;

			default:
				break;
		}
	}
	
	return objReturn;
}

function fetchITLocMultiDC(stShippingLocation){
	
	var objLoc = {fromloc: '', toloc: ''};
	
	//Get warehouse code
	var stWHCode = fetchWHCode(stShippingLocation);
	
	if(stWHCode){
		//Inventory Transfer Locations Map
		var stITLocMap = SCRIPTCONFIG.getScriptConfigValue('Hybris: Inventory Transfer: Location Map: Regular');
		
		if(stITLocMap){			
			var objITLocMap = JSON.parse(stITLocMap);
			
			//Find locations based on warehouse code
			var arITLocs = objITLocMap[stWHCode];				
			if(!isEmpty(arITLocs)){
				objLoc.fromloc = arITLocs[0];
				objLoc.toloc = arITLocs[1];
			}
		}
	}
	
	return objLoc;
}

function fetchSOLocMultiDC(stBillingCountry, stFulfillingCountry, stShippingLocation){
		
	var objLoc = {loc: '', ico: false};

	//Get warehouse code
	var stWHCode = fetchWHCode(stShippingLocation);
	
	//Get actual Country where Billing country is mapped into
	var stBillingCountry = fetchMappedCountry(stBillingCountry);
	
	if(stWHCode){
		//Sales Order Locations Map
		var stSOLocMap;
		
		if(stBillingCountry == stFulfillingCountry){
			stSOLocMap = SCRIPTCONFIG.getScriptConfigValue('Hybris: Sales Order: Location Map: Regular');
		} else {
			//Intercompany
			stSOLocMap = SCRIPTCONFIG.getScriptConfigValue('Hybris: Sales Order: Location Map: ICO');
			objLoc.ico = true;
		}
		
		if(stSOLocMap){			
			var objSOLocMap = JSON.parse(stSOLocMap);
			
			//Find locations based on warehouse code		
			if(objSOLocMap[stWHCode]){
				objLoc.loc = objSOLocMap[stWHCode];
			}
		}
	}
	
	return objLoc;
}

function fetchWHCode(stShippingLocation){
	
	//Location - Warehouse Map
	var stWHLocMap = SCRIPTCONFIG.getScriptConfigValue('Hybris: Fulfilling Location Map');
	
	if(stWHLocMap){
		return JSON.parse(stWHLocMap)[stShippingLocation];
	}
	
	return null;
}

function isMultiDCEnabled(){
	
	var stEnableMultiDC = SCRIPTCONFIG.getScriptConfigValue('Hybris: Enable Multi 3PL');
	
	if(stEnableMultiDC){
		return eval(stEnableMultiDC);
	}
	
	return false;
}

function fetchMappedCountry(stCountry){
	
	var stSellCountryMap = SCRIPTCONFIG.getScriptConfigValue('Hybris: Selling Country Map');
	
	if(stSellCountryMap){
		return JSON.parse(stSellCountryMap)[stCountry];
	}
	
	return stCountry;
}