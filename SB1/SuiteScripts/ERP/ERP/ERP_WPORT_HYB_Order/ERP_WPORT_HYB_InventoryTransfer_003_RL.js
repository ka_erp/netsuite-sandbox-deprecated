/**
 *	File Name		:	ERP_WPORT_HYB_InventoryTransfer_003_RL.js
 *	Function		:	Order Process - Step 3 (Inventory Reservation)
 * 	Remarks			:	Relevant scripts isolated from WTKA_NetSuite_ICTO_Process.js
 *	Prepared by		:	A.Villanueva
 *	Release Dates	:	01-Jun-2016
 * 	Current Version	:	1.0
**/

{
	var postingMessage = 'not within the date range of your accounting period';
}

//START : APPLE V. 2016/10/17 : Operational Issue : Moved 200 Response processing from 002_UE script to IT_003_RL script
//POST FUNCTION
function createInventoryTransfers(dataIn){
	
	var extOrderId = dataIn.order.orderHeader.orderId;
	var objExtRecord;
	
	try{
		//Create Inventory Transfers
		finalMessage = do_createInventoryTransfers(dataIn);
		
		//Get WTKA_External_Order object
		objExtRecord = findExternalRecord(extOrderId);
		
		//Process result based on status
		if(finalMessage.status == "Success"){
			
			//Inventory Transfer creation successful
			//Update staging record and send 940 request
			do_SuccessProcessing(finalMessage, objExtRecord);
			
		} else if(finalMessage.status == "Duplicate"){
			
			//Duplicate
			do_DuplicateProcessing(objExtRecord);
			
		} else {
			
			//Error
			do_ErrorProcessing(finalMessage, objExtRecord);
			
		}
		
	} catch(error){
		nlapiLogExecution('DEBUG', 'createInventoryTransfers try/catch', error);
		ErrorObj.messages.push(error);
		nlapiLogExecution('DEBUG', 'createInventoryTransfers failure-ErrorObj', JSON.stringify(ErrorObj));
		finalMessage.status = "Exception";
		finalMessage.records.push(ErrorObj);
		var message = '<br><b>Error Details: </b><br>' + JSON.stringify(ErrorObj);
		sendOrderMail('Inventory', message, dataIn);
		
		//Error
		do_ErrorProcessing(finalMessage, objExtRecord);

		//Logging
		createTransactionLog(dataIn, extOrderId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 12, SYNC_STATUS.ERROR, JSON.stringify(ErrorObj), null, 802); //Library call to WTKA_Library.js //Record Type: 12 is Inventory Transfer
	}
	
	return finalMessage;
}

//Searches for the WTKA_External_Order record associated with the current processing
//Returns an object
function findExternalRecord(extOrderId){
	
	var objExtRecord = {};
			
	//Find WTKA_External_Orders record
	var arResult = nlapiSearchRecord('customrecord_wtka_external_orders', null,
					[new nlobjSearchFilter('custrecord_wtka_orderid', null, 'is', extOrderId)],
					[new nlobjSearchColumn('internalid').setSort(true),
					 new nlobjSearchColumn('custrecord_wtka_final_response'),
					 new nlobjSearchColumn('custrecord_wtka_inventorytransfers')]);
	
	if(arResult && arResult.length > 0){
		objExtRecord.extOrderId = extOrderId;
		objExtRecord.internalId = arResult[0].getValue('internalid');
		objExtRecord.recordResp = arResult[0].getValue('custrecord_wtka_final_response');
		objExtRecord.invTrfValues = arResult[0].getValue('custrecord_wtka_inventorytransfers');
	}
	
	return objExtRecord;
}

//Reflect Duplicate Inventory Transfer processing info in staging record
function do_DuplicateProcessing(objExtRecord){
	
	if(objExtRecord && objExtRecord.internalId){							
		var ordStatus 	= 2; //Reset status to Inventory Transfer Processed
		nlapiLogExecution('debug', 'Duplicate Inventory Transfer Request');
		nlapiSubmitField('customrecord_wtka_external_orders', objExtRecord.internalId, 'custrecord_wtka_order_status', ordStatus, false);
	}	
}

//Reflect failed Inventory Transfer processing info in staging record
function do_ErrorProcessing(finalMessage, objExtRecord){
	
	if(objExtRecord){		
		var ordStatus = 6; //Error
		var recResp = {};
		if(objExtRecord.recordResp){
			recResp = JSON.parse(objExtRecord.recordResp);
			recResp.status 	= finalMessage.status;
			recResp.records.push(finalMessage);
		} else{
			recResp = finalMessage;
		}
		var resp = JSON.stringify(recResp);
		
		//Fields to update on the WTKA_External_Order record
		var fields = [];	
		fields.push('custrecord_wtka_final_response');
		fields.push('custrecord_wtka_order_status');
		
		//Set values into the fields
		var values = [];
		values.push(resp);
		values.push(ordStatus);
		
		nlapiSubmitField('customrecord_wtka_external_orders', objExtRecord.internalId, fields, values, false);	
	}
}

//Reflect successful Inventory Transfer processing info in staging record
function do_SuccessProcessing(finalMessage, objExtRecord){
	
	if(finalMessage.records && finalMessage.records.length > 0 && objExtRecord){
		
		try{
			//Order Status field
			var ordStatus = 2; //Inventory Transfer Processed
						
			//Final Response field
			var recResp = {};
			if(objExtRecord.recordResp){
				recResp = JSON.parse(objExtRecord.recordResp);
				nlapiLogExecution('DEBUG', 'recResp', JSON.stringify(recResp));
			} else {
				recResp.records = [];
			}
			recResp.status 	= 'Success';
						
			nlapiLogExecution('DEBUG',  'finalMessage.records', JSON.stringify(finalMessage.records));
			for(var r in finalMessage.records){				
				//Final Response field
				recResp.records.push(finalMessage.records[r]);
				
				//Inventory Transfers field
				//put together string for field: INVENTORY TRANSFERS
				if(objExtRecord.invTrfValues == ""){
					objExtRecord.invTrfValues = finalMessage.records[r].transactionId;
				} else {
					objExtRecord.invTrfValues += ',' + finalMessage.records[r].transactionId;
				} 
			}	
			var stFinalResp = JSON.stringify(recResp);
			
			//Fields to update on the WTKA_External_Order record
			var fields = [];
			fields.push('custrecord_wtka_final_response');
			fields.push('custrecord_wtka_inventorytransfers');
			fields.push('custrecord_wtka_order_status');
			
			//Set values into the fields
			var values = [];
			values.push(stFinalResp);
			values.push(objExtRecord.invTrfValues);						
			values.push(ordStatus); 
			
			nlapiSubmitField('customrecord_wtka_external_orders', objExtRecord.internalId, fields, values, false);	

			linkTransactionsToExternalWTKA(objExtRecord.internalId); //Call to WTKA_Library.js
			
		} catch(error){
			nlapiLogExecution('debug', 'do_SuccessProcessing error', error.toString());
		
			//Logging
			createTransactionLog(dataIn, objExtRecord.extOrderId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 12, SYNC_STATUS.ERROR, error.toString(), null, 803); //Library call to WTKA_Library.js //Record Type: 12 is Inventory Transfer
		}
		
		//Send 940 Request
		do_Send940Request(objExtRecord.extOrderId, objExtRecord.internalId, stFinalResp);
	}
}

//Sends 940 request to ESB
function do_Send940Request(extOrderId, recordId, recordResp){
				
	//Send request to ESB
	var ediStat = InvokeCloudhub('salesorder', 0, extOrderId, 'Released', 'inventoryFlow'); //call to WTKA_Library.js
	nlapiLogExecution('debug', 'ediStat', ediStat);
	
	//Log error response from ESB
	if(ediStat != 'T'){
		var ordStatus = 6; //Error
		
		//Final Response field
		var recResp = {};
		if(recordResp){
			recResp = JSON.parse(recordResp);
			nlapiLogExecution('DEBUG', 'recResp', JSON.stringify(recResp));
		} else {
			recResp.records = new Array();
		}
		
		if(recResp){
			var respObj = "940 ESB call post Inventory Reservation failed for order " + extOrderId;
			recResp.status 	= "Error";
			recResp.records.push(respObj);
		}
		
		//Fields to update on the WTKA_External_Order record
		var fields = [];
		fields.push('custrecord_wtka_final_response');
		fields.push('custrecord_wtka_order_status');
		
		//Set values into the fields
		var values = [];
		values.push(JSON.stringify(recResp));
		values.push(ordStatus);

		nlapiSubmitField('customrecord_wtka_external_orders', recordId, fields, values, false);	
	}
}
//END : APPLE V. 2016/10/17 : Operational Issue : Moved 200 Response processing from 002_UE script to IT_003_RL script

//Processing of POST request
//Renamed from 'createInventoryTransfers'
function do_createInventoryTransfers(dataIn)
{
	nlapiLogExecution('DEBUG', 'Inside Create Inventory Transfer', JSON.stringify(dataIn));
	//Get the Order ID from dataIn, if it exists
	var tranId;
	try{
		tranId = dataIn.order.orderHeader.orderId;
	} catch (tranExc) {
		tranId = '';
	}

	try
	{
		//1. Pre-Validations Checks
		if(preValidationCheckIT(dataIn))
		{
			finalMessage.orderNumber = dataIn.order.orderHeader.orderId; //get the external Order Number
			nlapiLogExecution('DEBUG', 'FinalInvTrfArray', JSON.stringify(FinalInvTrfArray));
			for(var i=0; i < FinalInvTrfArray.length; i++)
			{
				finalMessage.status = "Success";
				try
				{
					if(FinalInvTrfArray[i].invTrf == 0) // Process only new Reservation requests
					{
						var invTrf  = nlapiCreateRecord('inventorytransfer');
						invTrf.setFieldValue('subsidiary', 							FinalInvTrfArray[i].subsidiary);
						invTrf.setFieldValue('trandate', 							FinalInvTrfArray[i].orderDate);
						invTrf.setFieldValue('memo', 								(FinalInvTrfArray[i].memo != null) ? FinalInvTrfArray[i].memo : '');
						invTrf.setFieldValue('custbody_wtka_extsys_order_number', 	FinalInvTrfArray[i].extNumber);
						invTrf.setFieldValue('externalid', 							FinalInvTrfArray[i].extNumber + '_IT'); //IT External ID
						invTrf.setFieldValue('custbody_wtka_extsys_hybris_order', 	FinalInvTrfArray[i].ordNumber);
						invTrf.setFieldValue('location', 							FinalInvTrfArray[i].fromLocation);
						invTrf.setFieldValue('transferlocation', 					FinalInvTrfArray[i].toLocation);
						
						/* Set item quantities */
						for(var j=0; j < FinalInvTrfArray[i].items.length; j++)
						{
							invTrf.selectNewLineItem('inventory');
							invTrf.setCurrentLineItemValue('inventory', 'item',  		FinalInvTrfArray[i].items[j].SKUId);
							invTrf.setCurrentLineItemValue('inventory', 'adjustqtyby',  FinalInvTrfArray[i].items[j].orderQty);
							invTrf.commitLineItem('inventory');
						}

						//var invTrfRecord = nlapiSubmitRecord(invTrf, true); //old command, comment out for now
						var invTrfRecord = -1;
						try {
							invTrfRecord = nlapiSubmitRecord(invTrf, true);
						} catch (error1) {
							
							nlapiLogExecution('DEBUG', 'createInventoryTransfers Error1', error1.toString());
							createTransactionLog(dataIn, finalMessage.orderNumber, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 12, SYNC_STATUS.ERROR, error1.toString(), null, 800); //Library call to WTKA_Library.js //Record Type: 12 is Inventory Transfer

							if (error1.toString().indexOf(postingMessage) > 0){ //check if the error was because the transaction date is in a closed period

								var newDate = nlapiDateToString(new Date(), 'date');

								var dateMessage = 'Posting period error. Retrying with date: ' + newDate;
								createTransactionLog(dataIn, finalMessage.orderNumber, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 12, null, dateMessage, null, 801); //Library call to WTKA_Library.js //Record Type: 12 is Inventory Transfer								

								invTrf.setFieldValue('custbody_erp_failed_posting_date', FinalInvTrfArray[i].orderDate); 
								invTrf.setFieldValue('trandate', newDate); //set the transaction date to today

								invTrfRecord = nlapiSubmitRecord(invTrf, true); //try to submit the record again
							}
						}
						
						var processObject 				= new Object();
						processObject.status 			= "Success";
						processObject.transactionType 	= "Inventory Transfer";
						processObject.transactionId 	= invTrfRecord;
						processObject.transactionNumber = nlapiLookupField('inventorytransfer', invTrfRecord, 'tranid');
						finalMessage.records.push(processObject);
						ediStatus 			 			= 1;
						var logId  = LogCreation(1, 7, invTrfRecord, invTrfRecord, dataIn, ediStatus, finalMessage, 'F'); //7 - Inventory Transfer
				
						createTransactionLog(dataIn, finalMessage.orderNumber, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 12, SYNC_STATUS.CREATED, JSON.stringify(finalMessage), invTrfRecord, 30); //Library call to WTKA_Library.js //Record Type: 12 is Inventory Transfer
					}
					else
					{
						ErrorObj.status 					= "Duplicate";
						ErrorObj.messages[0]			 	= new Object();
						ErrorObj.messages[0].messagetype 	= "Duplicate request";
						ErrorObj.messages[0].message 	 	= "Request cannot be processed as the Inventory Transfer against Order " + orderShipID[i] + " already exists.";
						
						finalMessage.status = ErrorObj.status;
						finalMessage.records.push(ErrorObj.messages);

						createTransactionLog(dataIn, finalMessage.orderNumber, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 12, SYNC_STATUS.CREATED, JSON.stringify(finalMessage), null, 31); //Library call to WTKA_Library.js //Record Type: 12 is Inventory Transfer
					}
				}
				catch(err1)
				{
					ErrorObj.status  	 = "Exception";
					ErrorObj.messages[0] = "Unable to validate Inventory Transfer Request : " + err1;
					finalMessage.status  = ErrorObj.status;
					finalMessage.records.push(ErrorObj);
					nlapiLogExecution('DEBUG', 'createInventoryTransfers Error', JSON.stringify(finalMessage));
					createTransactionLog(dataIn, finalMessage.orderNumber, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 12, SYNC_STATUS.CREATED, JSON.stringify(finalMessage), null, 32); //Library call to WTKA_Library.js //Record Type: 12 is Inventory Transfer
					return finalMessage;
				}
			}
			nlapiLogExecution('DEBUG', 'processObject', JSON.stringify(finalMessage));			

			return finalMessage;
		}
		else
		{
			nlapiLogExecution('DEBUG', 'Pre-validation failure-ErrorObj', JSON.stringify(ErrorObj));
			finalMessage.status  	 = ErrorObj.status;
			finalMessage.records.push(ErrorObj);
			var message = '<br><b>Error Details: </b><br>' + JSON.stringify(ErrorObj);
			sendOrderMail('Inventory', message, dataIn);

			createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 12, SYNC_STATUS.ERROR, JSON.stringify(finalMessage), null, 33); //Library call to WTKA_Library.js //Record Type: 12 is Inventory Transfer

			return finalMessage;
		}
	}
	catch(err)
	{
		nlapiLogExecution('DEBUG', 'In createInventoryTransfers try/catch', err);
		ErrorObj.messages.push(err);
		nlapiLogExecution('DEBUG', 'Pre-validation failure-ErrorObj', JSON.stringify(ErrorObj));
		finalMessage.status  	 = ErrorObj.status;
		finalMessage.records.push(ErrorObj);
		var message = '<br><b>Error Details: </b><br>' + JSON.stringify(ErrorObj);
		sendOrderMail('Inventory', message, dataIn);

		createTransactionLog(dataIn, tranId, DIRECTION.IN, SYNC_SYSTEM.ESB, SYNC_SYSTEM.NETSUITE, 12, SYNC_STATUS.ERROR, JSON.stringify(ErrorObj), null, 34); //Library call to WTKA_Library.js //Record Type: 12 is Inventory Transfer

		return finalMessage;
	}
}

function preValidationCheckIT(dataIn)
{
	//Pre-Validation checks on DataIn

	if(dataIn.order.orderHeader.orderStatus == 'Created' || dataIn.order.orderHeader.orderStatus == 'Shipped' || dataIn.order.orderHeader.orderStatus == 'Completed')
	{
		ErrorObj.messages[0] = new Object();
		ErrorObj.messages[0].messagetype = "Invalid Order Status";
		ErrorObj.messages[0].message 	 = "Order cannot be created as the order "  + dataIn.order.orderHeader.orderId + " is in " + dataIn.order.orderHeader.orderStatus + " status and the Inventory Transfer is not complete.";
		return false;
	}	
	
	//Check for duplicates by reading through orderId and shipmentId combination
	var totalCount = 0;
	for(var i in dataIn.order.shipment)
	{
		var ordShipId 	=	dataIn.order.orderHeader.orderId + '_' + dataIn.order.shipment[i].shipmentHeader.shipmentId;
		nlapiLogExecution('DEBUG', 'ordShipId', ordShipId);
		var type 		= 'inventorytransfer';
		var InvTrfId	= FetchOrderId(type, ordShipId, 'ext');
		nlapiLogExecution('debug', 'InvTrfId', i + ') ' + InvTrfId);
		if(InvTrfId > 0)
		{
			totalCount++;
		}
		else
		{
			nlapiLogExecution('DEBUG', 'Inventory Transfer', 'Inventory Transfer to be processed for ' + ordShipId);
			var FinalUPCCodes = new Array();
			for(var ii in dataIn.order.orderDetail)
			{
				FinalUPCCodes[ii] = dataIn.order.orderDetail[ii].item.SKUId;
			}
			
			/* Validate dates */
			if(!validateISODate(dataIn.order.orderHeader.orderTransactionDt))	return false;
			
			var sellingLocation 	= dataIn.order.orderHeader.sellingLocation.locationCode;
			var fulfillingLocation  = dataIn.order.shipment[i].shipmentHeader.fulfillingLocation.country;
			var locale = (sellingLocation == fulfillingLocation) ? sellingLocation : fulfillingLocation;
			location = (locale == 'CA') ? CA_EcommDC : US_EcommDC;
			
			//This is original logic but taken out from a different line below 
			var toLoc = (locale == 'CA') ? CA_Virtual : US_Virtual;
			
			//Check Script Config for Multiple DC setting
			if(isMultiDCEnabled()){
				//Use Script Config mapping for the locations (based on Shipment Fulfilling Location Code)
				var objLoc = fetchITLocMultiDC(dataIn.order.shipment[i].shipmentHeader.fulfillingLocation.locationCode); 
				location = objLoc.fromloc;
				toLoc = objLoc.toloc;
			}
						
			var subsid  = FetchSubsidiaryId('country', locale);
			if(subsid == -1)
			{
				ErrorObj.messages[0] 				= new Object();
				ErrorObj.messages[0].messagetype    = "Subsidiary error";
				ErrorObj.messages[0].message 	    = "Invalid Query. Error: Subsidiary for Country: " + locale + " cannot be found.";
				nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
				return false;
			}

			/* Lookup Items */
			FinalItemIDs = FetchIdFromName('item', FinalUPCCodes);
			nlapiLogExecution('DEBUG', 'FinalItemIDs', JSON.stringify(FinalItemIDs));
			if(FinalItemIDs[0] == 'Error')	return false

			for(var j in dataIn.order.shipment[i].shipmentDetail)  
			{
				var flag 	  = false;
				var orderDate = '';
				for(var k=0; k < FinalInvTrfArray.length; k++)
				{
					if(FinalInvTrfArray[k].extNumber == ordShipId)
					{
						var lineArray 		  	= new Object();
						lineArray 				= FinalInvTrfArray[k];
						
						var itemDetails 	  	= new Object;
						var lineItemId 			= dataIn.order.shipment[i].shipmentDetail[j].orderDetailId;

						var lineSKUId;
						for(var kk in dataIn.order.orderDetail)
						{
							var flag = false;
							if(dataIn.order.orderDetail[kk].orderLineNumber === lineItemId)
							{
								var lineUPCCode = dataIn.order.orderDetail[kk].item.SKUId; //UPC Codes 
								for(var ii in FinalItemIDs)
								{
									if(FinalItemIDs[ii].UPCCode == lineUPCCode)
									{
										lineSKUId = FinalItemIDs[ii].Id;
										flag 	  = true;
										break;
									}
								}
							}
							if(flag) break;
						} 			
						itemDetails.SKUId 	  	= lineSKUId;
						itemDetails.orderQty 	= dataIn.order.shipment[i].shipmentDetail[j].quantityOrdered;
						lineArray.items.push(itemDetails);
						flag 					= true;
						break;
					}
				}
				
				if(!flag)
				{
					//Object doesn't exist
					var lineArray 			= new Object();
					var dateValue 			= dataIn.order.orderHeader.orderTransactionDt;
					orderDate 				= moment(dateValue).format('l LT'); 	
					orderDate 				= new Date(orderDate);
					orderDate 				= nlapiDateToString(orderDate, 'date');						
					
					lineArray.extNumber 	= dataIn.order.orderHeader.orderId + '_' + dataIn.order.shipment[i].shipmentHeader.shipmentId;
					lineArray.ordNumber 	= String(dataIn.order.orderHeader.orderId);
					lineArray.invTrf		= InvTrfId;
					lineArray.orderDate 	= orderDate; 
					lineArray.memo 			= dataIn.order.orderHeader.orderHeaderMemo;
					lineArray.subsidiary	= subsid.code;
					lineArray.fromLocation	= location;
					
					/* Comment Out Reason: will set 'To Location' value only Once above, to support multi-DC
					lineArray.toLocation	= (locale == 'CA') ? CA_Virtual : US_Virtual; */
					lineArray.toLocation	= toLoc;
					
					lineArray.items 		= new Array();
					var itemDetails 	  	= new Object;
					var lineItemId 			= dataIn.order.shipment[i].shipmentDetail[j].orderDetailId;

					var lineSKUId;
					for(var k in dataIn.order.orderDetail)
					{
						var flag = false;
						if(dataIn.order.orderDetail[k].orderLineNumber === lineItemId)
						{
							var lineUPCCode = dataIn.order.orderDetail[k].item.SKUId; //UPC Codes 
							for(var ii in FinalItemIDs)
							{
								if(FinalItemIDs[ii].UPCCode == lineUPCCode)
								{
									lineSKUId = FinalItemIDs[ii].Id;
									flag 	  = true;
									break;
								}
							}
						}
						if(flag) break;
					} 			
					itemDetails.SKUId 	  	= lineSKUId;
					itemDetails.orderQty 	= dataIn.order.shipment[i].shipmentDetail[j].quantityOrdered;
					lineArray.items.push(itemDetails);
					FinalInvTrfArray.push(lineArray);
				}
			}
		}
		
		if(totalCount == dataIn.order.shipment.length)
		{
			nlapiLogExecution('DEBUG', 'Duplicate Inventory Transfer Found',  'ordShipID: ' + ordShipId + ', InvTrfId:' + InvTrfId);

			ErrorObj.status 					= "Duplicate";
			ErrorObj.messages[0]			 	= new Object();
			ErrorObj.messages[0].messagetype 	= "Duplicate request";
			ErrorObj.messages[0].message 	 	= "Request cannot be processed as the Inventory Transfer against Order " + ordShipId + " already exists. (inventorytransfer ID: " + InvTrfId + ")";
			nlapiLogExecution('debug', 'ErrorObj', JSON.stringify(ErrorObj));
			return false;
		}
	}
	return true;
}

