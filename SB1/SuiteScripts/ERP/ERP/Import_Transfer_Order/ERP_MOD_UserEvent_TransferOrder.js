/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_UserEvent_TransferOrder
 * Version            1.0.0.0
 **/

// Custom List called Set Transfer Price Status
var TO_STATUS = {
    Pending: 1,
    Completed: 2,
    IssuesFound: 3
};

if ( nlapiGetContext().getExecutionContext() == "PRODUCTION" )
    var SendErrorEmailsFrom = 125247;
else
    var SendErrorEmailsFrom = -5;


function onBeforeLoad(type, form, request) {

}

function onBeforeSubmit(type) {

}

function onAfterSubmit(type) {
	
//	var if = nlapiLookupField(nlapiGetRecordId(), newIFtoPO)
//	nalpiSubmitFile(purchase, if)
	
    if ( type != 'delete' && nlapiGetRecordId() != 273617 ) EC.PopulateIntraCompanyTransferPricing_OnSave();


}

function removeTransferOrder(){}



/**
 * Function to validate From Location field on the Transfer Order form.  This field must always be populated before any
 * lines are added to the order.  If populated, then a Suitelet is called to get the Transfer Pricing for the line.
 * @constructor
 */
EC.PopulateIntraCompanyTransferPricing_OnSave = function(){

    try
    {

        var transferOrder = nsdal.loadObject(nlapiGetRecordType(), nlapiGetRecordId(), ["id", "location", "custbody_set_transfer_price_status", "orderstatus", "subsidiary"])
            .withSublist("item", ["item","description", "itemtype", "rate", "custcol_erp_override_rate", "custcol_erp_original_quantity", "commitinventory", "quantity", "custcol_erp_to_memo"]);  // ERP EDIT - IS20150824


        // ERP Codes  START
        nlapiLogExecution('DEBUG', ' ============================================== location ', transferOrder.location);

        //Get all the line item id
        // ERP START - IS20150821
    	var arrTOItemId = new Array();
    	_.each(transferOrder.item, function(line, index){
    		  var itemType = getItemType(line.itemtype);
    		  if ( itemType == "inventoryitem") {
    			  //nlapiLogExecution('DEBUG', ' ============================================== Items in TO: Index Of ',line.item + " in >> " + arrTOItemId.indexOf(line.item) );
    			  if(arrTOItemId.indexOf(line.item) < 0 ){
    				  arrTOItemId.push(line.item);
    			  }
    		  }
    	});
    	 // ERP END - IS20150821


    	//nlapiLogExecution('DEBUG', ' ============================================== Items in TO ', arrTOItemId.toString());


      var arSavedSearchResultsItm = null;

    	var arSaveSearchFiltersItm = new Array();
    	var arSavedSearchColumnsItm = new Array();

    	var resultsArray = new Array(); // important array
    	var idArray = new Array();  // important array


    	var strSavedSearchIDItm = null;

    	arSaveSearchFiltersItm.push(new nlobjSearchFilter('inventorylocation', null , 'is', transferOrder.location ));
    	arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null , 'anyof', arrTOItemId ));

    	arSavedSearchColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
    	arSavedSearchColumnsItm.push(new nlobjSearchColumn('name'));
    	arSavedSearchColumnsItm.push(new nlobjSearchColumn('inventorylocation'));
    	arSavedSearchColumnsItm.push(new nlobjSearchColumn('locationaveragecost'));


    	arSavedSearchResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchColumnsItm);

    	if (!isNullOrEmpty(arSavedSearchResultsItm)){

    		 nlapiLogExecution('DEBUG', ' ============================================== Item Search Location ', arSavedSearchResultsItm.length);

    			for(var i = 0 ;  i < arSavedSearchResultsItm.length; i++ ){

    				var obj = new Object();
    				obj.id = arSavedSearchResultsItm[i].getValue('internalid');
    				obj.name = arSavedSearchResultsItm[i].getValue('name');
    				obj.location = arSavedSearchResultsItm[i].getValue('inventorylocation');
    				obj.locationname = arSavedSearchResultsItm[i].getText('inventorylocation');
    				obj.locavecost = arSavedSearchResultsItm[i].getValue('locationaveragecost');

    				resultsArray.push(obj);
    				idArray.push(obj.id);

    			}
    	}


        // ERP Codes END


        var arrZeroAvgItemObj = new Array();
        var arrZeroAvgItemIds = new Array();

        var errorMsg = false;          // Error Message variable that will help us

        _.each(transferOrder.item, function(line, index){

            var lineNumber = index+1;
            Log.d("PopulateIntraCompanyTransferPricing_OnSave", "itemid:  " + line.item);
            Log.d("PopulateIntraCompanyTransferPricing_OnSave", "itemType:  " + line.itemtype);
            var itemType = getItemType(line.itemtype);
            Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Converted itemType:  " + itemType);

            Log.d("PopulateIntraCompanyTransferPricing_OnSave", "==== >> custcol_erp_override_rate:  " + line.custcol_erp_override_rate);  // ERP ADD- IS20150824

            if ( itemType == "inventoryitem" ) {

                var avgCost =  0.00;

                // ERP START - IS20150821
                //nlapiLogExecution('DEBUG', ' ============================================== line number ', line.item + " cost/rate: " + avgCost  );


            	var intArrayPosition = idArray.indexOf(line.item); var itemObject = resultsArray[intArrayPosition];

            	if(!isNullOrEmpty(itemObject)){
            		avgCost = itemObject.locavecost;
            	}else{}
            	
                // ERP END - IS20150821


            	//if(line.commitinventory > 0){
            	//	line.custcol_erp_original_quantity = line.quantity;
            	//	line.quantity = line.commitinventory;
            	//}

            	//var a = EC.ERPGetLocationItemAverageCost(line.item); //EC.GetAverageCost(transferOrder.location, line.item, itemType);

                Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Average Cost:  " + avgCost);
                if ( avgCost && avgCost != "NOTFOUND" )
                {
                    line.rate = avgCost;
                    line.custcol_erp_to_memo = "loc: " + itemObject.locationname + " line: " + lineNumber  + " avg: "+ avgCost; 
                    
                    Log.d("PopulateIntraCompanyTransferPricing_OnSave - LINE " + lineNumber + " SET", "Average Cost:  " + avgCost);
                    
                    Log.d("Found Items", "Found Item Id:  " + line.item + " " + line.description);

                }
                else
                {
                    var findItemObj = new Object();
                    findItemObj.linenum = lineNumber;
                    findItemObj.itemid = line.item;

                    arrZeroAvgItemIds.push(line.item);
                    arrZeroAvgItemObj.push(findItemObj);
                    
                    Log.a("Zero Items", "Zero Item Id:  " + line.item + " " +  line.description +  " override:"+ line.custcol_erp_override_rate); 
                                        
                    
                    if(line.custcol_erp_override_rate == false){  // ERP ADD- IS20150824
	                    errorMsg = true;
	                    Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Error Found on Line:  " + lineNumber);
                    }  // ERP ADD- IS20150824
                }
                
                //Log.a("Error Line", " Line:  " + lineNumber + " errorMsg:" + errorMsg);
                
                
            }
            else
                Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Line:  " + lineNumber + " is not an inventory item");
        });



        nlapiLogExecution('DEBUG', 'ZERO ITEM ID', arrZeroAvgItemIds.toString());
        nlapiLogExecution('DEBUG', 'ZERO ITEM OBJ', JSON.stringify(arrZeroAvgItemObj));


        
        // ----------------------------------------------------------------------------------------------------------------------------------------------- Zero Cost Catcher
//        if(false){
        if(!isNullOrEmpty(arrZeroAvgItemIds)){
        	

            //Saved search to identify the Main location per sub
        	
        	var idSubsidiary = transferOrder.subsidiary; 
           				
    	    var searchFiltersMainLocation = [];
    	    searchFiltersMainLocation[0] =  new nlobjSearchFilter('subsidiary', null, 'is', idSubsidiary);
    		searchFiltersMainLocation[1] =  new nlobjSearchFilter('custrecord_ka_main_warehouse', null, 'is', 'T');    	    
    	    var searchColumnsMainLocation = [new nlobjSearchColumn('internalid')];    	    
    	    var resultsMainLocation = nlapiSearchRecord('location', null, searchFiltersMainLocation, searchColumnsMainLocation);
    	    
    	    if (!isNullOrEmpty(resultsMainLocation)){
    	    	var idLocation = resultsMainLocation[0].getValue('internalid');
    	    	
    	    	nlapiLogExecution('DEBUG', 'MAIN LOCATION LOOKUP', "LOCATION: " + idLocation + " FOUND");
    		    			
            	var arSavedSearchItemMainResultsItm = null;
                var arSaveSearchFiltersItm = new Array();
                var arSavedSearchItemMainColumnsItm = new Array();
                var zeroIdCostArray = new Array(); // important array
                var zeroObjCostArray = new Array();  // important array
                var strSavedSearchIDItm = null;

                arSaveSearchFiltersItm.push(new nlobjSearchFilter('inventorylocation', null , 'is', idLocation ));
                arSaveSearchFiltersItm.push(new nlobjSearchFilter('internalid', null , 'anyof', arrZeroAvgItemIds ));
                arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('internalid').setSort());
                arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('name'));
                arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('inventorylocation'));
                arSavedSearchItemMainColumnsItm.push(new nlobjSearchColumn('locationaveragecost'));
                arSavedSearchItemMainResultsItm = nlapiSearchRecord('inventoryitem', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchItemMainColumnsItm);
                                                

                if (!isNullOrEmpty(arSavedSearchItemMainResultsItm)){

                   ///nlapiLogExecution('DEBUG', ' ============================================== Item Search Location ', arSavedSearchItemMainResultsItm.length);

                    for(var i = 0 ;  i < arSavedSearchItemMainResultsItm.length; i++ ){

                      var obj = new Object();
                      obj.id = arSavedSearchItemMainResultsItm[i].getValue('internalid');
                      obj.name = arSavedSearchItemMainResultsItm[i].getValue('name');
                      obj.location = arSavedSearchItemMainResultsItm[i].getValue('inventorylocation');
                      obj.locationname = arSavedSearchItemMainResultsItm[i].getText('inventorylocation');
                      obj.locavecost = arSavedSearchItemMainResultsItm[i].getValue('locationaveragecost');

                      zeroObjCostArray.push(obj);
                      zeroIdCostArray.push(obj.id);

                    }
                }

              //nlapiLogExecution('DEBUG', 'MAIN LOCATION LOOKUP', JSON.stringify(transferOrder.item));

              for(var m = 0; m < arrZeroAvgItemIds.length; m++ ){

                var lineid = arrZeroAvgItemObj[m].linenum; 
                var itemid = arrZeroAvgItemObj[m].itemid;
                
                var avgCost =  0.00;
                var intArrayPosition = zeroIdCostArray.indexOf(itemid); var zeroCostItemObject = zeroObjCostArray[intArrayPosition];
                
               
            	if(!isNullOrEmpty(zeroCostItemObject)){
            		//nlapiLogExecution('DEBUG', 'zeroCostItemObject', JSON.stringify(zeroCostItemObject));
            		if(!isNullOrEmpty(zeroCostItemObject.locavecost)){
            			avgCost = zeroCostItemObject.locavecost;
            		}            		            		
            	}
            	            	
            	nlapiLogExecution('AUDIT', "zeroObject/ to item: " ,  JSON.stringify(transferOrder.item[lineid-1])); 
            	transferOrder.item[lineid-1].rate = avgCost;
            	transferOrder.item[lineid-1].custcol_erp_to_memo = " loc: " + zeroCostItemObject.locationname + " line: " + lineid + " avg: "+ avgCost;
            	
            	var bOverride = transferOrder.item[lineid-1].custcol_erp_override_rate;
            	
            	if(bOverride == false && avgCost == 0.0){
            		 errorMsg = true; 
            	}
            	
//            	            	
//            	nlapiLogExecution('AUDIT', "condition: " + (item[lineid-1].custcol_erp_override_rate == false && avgCost == 0.0));
//            	if(item[lineid-1].custcol_erp_override_rate == false && avgCost == 0.0){  
//                    errorMsg = true;                    
//                } 
                                                                
                nlapiLogExecution('DEBUG', 'UPDATE TO ITEM [' + lineid-1 + ']', JSON.stringify( transferOrder.item[lineid-1]));

              }   			
    		}
        }



        // If an error was encountered for any line, set the custom status field to reflect this
        Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Show Error?  " + errorMsg);
        if (errorMsg) {
            transferOrder.custbody_set_transfer_price_status = TO_STATUS.IssuesFound;
            transferOrder.orderstatus = "A";        // A = Pending Approval
        } else {
            transferOrder.custbody_set_transfer_price_status = TO_STATUS.Completed;
            transferOrder.orderstatus = "B";        // B = Pending Fulfillment
        }

        var id = transferOrder.save(true, true);
        Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Transfer Order Re-saved:  " + id);
    }
    catch(e)
    {
        Log.d("PopulateIntraCompanyTransferPricing_OnSave", "Unexpected Error Occurred:  " + e);
        nlapiSendEmail(SendErrorEmailsFrom, "rhackey@exploreconsulting.com", "Kit & Ace:  Error in EC_UserEvent_TransferOrder script", "Error Details:  " + e)
    }
};

/**
 * Function to do lookup to Item Record to find the average cost for a specific location
 * @param loc
 * @param item
 * @param itemtype
 * @return itemcost - the average cost for the given location for this item
 */
EC.GetAverageCost = function(loc, item, itemtype) {

    var itemcost = 0;
    try
    {
        Log.d('getAverageCost', 'Location Received: ' + loc);
        Log.d('getAverageCost', 'Item Received: ' + item);
        Log.d('getAverageCost', 'Item Type: ' + itemtype);

        // Open the Item Record and Find the Average Cost
        var itemRecord = nsdal.loadObject(itemtype, item, ["id", "type"])
            .withSublist("locations", ["locationid", "averagecostmli"]);

        if ( itemRecord && loc )
        {
            // Get the Average Cost for this location
            itemcost = EC.GetItemAverageCost(itemRecord, loc);
            Log.d('getAverageCost', 'Cost found:  ' + itemcost);

            // If a Cost was found - return that to the client-side script
            if ( itemcost && itemcost != null )
                return itemcost;
            else
                itemcost = "NOTFOUND";
        }
    }
    catch(e)
    {
        Log.d('getAverageCost', 'Unexpected Error thrown from EC_Suitelet_Item_GetAverageCost: ' + e);
        nlapiSendEmail(SendErrorEmailsFrom, "rhackey@exploreconsulting.com,rmayo@exploreconsulting.com", "Kit & Ace - EC_Suitelet_GetAverageCost Error", "Error Details:  " + e)
    }
    return itemcost;
};

/**
 * Function that goes through location sublist on Item Record and finds the relevant location and then pulls the average cost from that line.
 * @param itemRecord
 * @param location
 * @returns averageCost {number} - average cost found in Locations sublist
 */
EC.GetItemAverageCost = function(itemRecord, location){

    var averageCost = 0.00;
    if ( itemRecord )
    {
        _.each(itemRecord.locations, function(locLine, index)
        {
            //Log.d('Processing Locations Line ' + index+1, 'Location:  ' + locLine.locationid);
            if ( locLine.locationid == location )
            {
                averageCost = locLine.averagecostmli;
                Log.d('Average Cost Found for Location ' + location, 'Avg Cost:  ' + averageCost);
                return;
            }
        });
    }
    return averageCost;
};

Log.AutoLogMethodEntryExit(null, true, true, true);


EC.ERPGetLocationItemAverageCost = function(itemid){

	var itemcost = 0;
	//	var loc = 4;

	var intArrayPosition = idArray.indexOf(itemid);

	var itemObject = [intArrayPosition];

	if(!isNullOrEmpty(itemObject)){
		itemcost = itemObject.locavecost;
	}


	nlapiLogExectuion("DEBUG", "Average Price", itemcost)

	return 0;
	//console.log(JSON.stringify(resultsArray));


};


function isNullOrEmpty(valueStr){
    return(valueStr == null || valueStr == "" || valueStr == undefined);
}
