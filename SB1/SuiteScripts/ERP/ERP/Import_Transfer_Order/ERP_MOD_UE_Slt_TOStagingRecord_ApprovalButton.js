/**
 * Company            Explore Consulting
 * Copyright            2015 Explore Consulting, LLC
 * Type                NetSuite EC_UE_Slt_TOStagingRecord_ApprovalButton
 * Version            1.0.0.0
 * Description          This script will add a custom button to the TO Header Staging Record form and then execute
 *                          functionality to go through all of it's TO Line Staging Records and approve them.
 **/

var ScriptName = "EC_UE_Slt_TOStagingRecord_ApprovalButton";

// Function to add the button to the record form
// Deployed as a User Event, on Before Load
function addButton(type, form, request)
{
    try
    {
        // Only show if the Record is in View Mode			// Change as needed
//        if ( type != 'view' ) return;

        // Add button to form
        var id = nlapiGetRecordId();
        var rectype = nlapiGetRecordType();

        if ( nlapiGetFieldValue('custrecord_tranfer_stage_status') != TO_HeaderStagingStatus.PendingApproval ) return;

        // The button script will be controlled via a suitelet.  See Suitelet code below.
        var url = nlapiResolveURL('SUITELET','customscript_ec_slt_to_staging_approval','customdeploy_ec_slt_to_staging_approval') + '&transactionRecord=' + id + '&transactionType=' + rectype;
//
//        form.addButton('custpage_approval','Approve All Transfers', "document.location='"+url+"'");
//        
//        form.addButton('custpage_approval2','Approve All Transfers2', "document.location='"+url+"'");
//        
//        form.addSubmitButton('custpage_approval_ids','Approve All Transfers - IDS'); 

    }
    catch(e)
    {
        Log.d("addButton", "Unexpected Error:  " + e);
    }
}



/////////////////////////////   SUITELET FUNCTIONALITY    /////////////////////////////////////
/////////////////////////////    TRIGGERED BY BUTTON      /////////////////////////////////////


/*
 *  Function triggered by custom Button on record form
 */
function buttonFunctionality(request, response)
{
    var transID = request.getParameter( 'transactionRecord' );
    var transType = request.getParameter( 'transactionType' );

    try
    {
        if ( !transID || !transType )
            throw "Parameters missing.";

        // Since this functionality could use a lot of governance - we will just send this directly off to a scheduled script
        Log.d('Scheduling Script to continue updating');
        var param = { custscript_to_header_staging_rec: transID };
        var result = nlapiScheduleScript('customscript_ec_sch_approve_tolines', null, param);                  // 20 units
        Log.d('Scheduling Script RESULT:  ' + result);
        if (result != 'QUEUED') {
            Log.d('scheduled script not queued',
                'expected QUEUED but scheduled script api returned ' + result);
            var emailBody5 = "Script: EC_UE_Slt_TOStagingRecord_ApprovalButton.js\nFunction: buttonFunctionality\nError: There was an issue re-scheduling the scheduled script in this script";
            EC.SendEmailAlert("Error: There was an issue re-scheduling the scheduled script in this script.\nHeader Staging Record:  " + transID, ScriptName);
        }
        else {
            Log.d('ProcessInventoryAdjustments -- Re-Scheduling Script Status', "Result:  " + result);
        }
    }
    catch(e)
    {
        Log.d("buttonFunctionality ", "Unexpected Error:  " + e);
    }

    // After the button's functionality is complete, we will want to route the user back to the original record's form
    nlapiSetRedirectURL("RECORD", transType, transID, null, null);
}

/////////////////////////////   SCHEDULED SCRIPT FUNCTIONALITY            /////////////////////////////////////
/////////////////////////////    USED TO ENSURE GOVERNANCE WILL BE OK      /////////////////////////////////////

function approveLineRecords(){

    // Pull the script parameters and validate them for processing
    var context = nlapiGetContext();
    var recordID = context.getSetting("SCRIPT", "custscript_to_header_staging_rec");
    Log.d('approveLineRecords', 'Script Parameter Check - TO Header Staging Record ID:  ' + recordID);

    try{

        if ( !recordID ) throw nlapiCreateError("INVALID_PARAMETER", "NO Record ID parameter found.\nRecord ID:  " + recordID);

        // Set the TO Header Staging Record status to Processing so we know this record is currently being processed.
        nlapiSubmitField("customrecord_tran_order_staging_header", recordID, "custrecord_tranfer_stage_status", TO_HeaderStagingStatus.Processing);

        // Get Detail Staging Records Connected to this Header Staging Record
        // This search will not check status - it is purely used as a MASS UPDATE of all of the Detail Staging Records
        //      connected to the provided Header Staging Record
        var filters = [new nlobjSearchFilter("isinactive", null, "is", "F"),
            new nlobjSearchFilter("custrecord_transfer_stage_header", null, "is", recordID),
            new nlobjSearchFilter("custrecord_transfer_stage_line_status", null, "anyof", [TO_LineStagingStatus.WaitingApproval])];
        var columns = [new nlobjSearchColumn("internalid")];
        var results = nlapiSearchRecord("customrecord_transfer_order_staging_line", null, filters, columns);
        var currentID;
        var rerun = false;
        var timesUp = false;

        if ( results ) {
            Log.d("approveLineRecords", "Number of TO Line Search Results:  " + results.length);

            if ( results.length == 1000 )
                rerun = true;

            for (var i=0; i < results.length && !timesUp; i++) {

                currentID = results[i].getId();
                Log.d("Processing TO Line Staging records - result #" + i, "Current Record ID:  " + currentID);

                // Set the status of the TO Line Staging Record to To Be Processed - meaning it is ready for adjustment in NetSuite
                nlapiSubmitField("customrecord_transfer_order_staging_line", currentID, "custrecord_transfer_stage_line_status", TO_LineStagingStatus.ToBeProcessed);

                timesUp = EC.RunTimeScriptCheckers(Script_GovernanceThreshold);
            }

            if ( timesUp || rerun)
            {
                Log.d('Scheduling Script to continue updating');
                var param = { custscript_to_header_staging_rec: recordID };
                var result = nlapiScheduleScript('customscript_ec_sch_approve_tolines', null, param);                  // 20 units
                Log.d('Scheduling Script RESULT:  ' + result);
                if (result != 'QUEUED') {
                    Log.d('scheduled script not queued',
                        'expected QUEUED but scheduled script api returned ' + result);
                    var emailBody5 = "Script: EC_UE_Slt_TOStagingRecord_ApprovalButton.js\nFunction: approveLineRecords\nError: There was an issue re-scheduling the scheduled script in this script";
                    EC.SendEmailAlert("Error: There was an issue re-scheduling the scheduled script in this script.", ScriptName);
                }
                else {
                    Log.d('approveLineRecords -- Re-Scheduling Script Status', "Result:  " + result);
                }
            }
            else
            {
                Log.d("approveLineRecords - " + recordID, "Processing Completed - All Detail Record Approved");
                var fields2 = ["custrecord_tranfer_stage_status", "custrecord_header_error_message"];
                var values2 = [TO_HeaderStagingStatus.Approved, ""];
                nlapiSubmitField("customrecord_tran_order_staging_header", recordID, fields2, values2);
            }
        }
        else
        {
            Log.d("approveLineRecords - " + recordID, "Approval Button - No Detail Staging records found to approve");
            var fields3 = ["custrecord_tranfer_stage_status", "custrecord_header_error_message"];
            var values3 = [TO_HeaderStagingStatus.Approved, "Approval Button - No TO Line Staging records found to approve.  Setting this Header record to Approved."];
            nlapiSubmitField("customrecord_tran_order_staging_header", recordID, fields3, values3);
        }
    }
    catch(e)
    {
        Log.d("approveLineRecords - " + recordID, "Error was throwing during processing:  " + e);
        var fields = ["custrecord_tranfer_stage_status", "custrecord_header_error_message"];
        var values = [TO_HeaderStagingStatus.Error_ToBeReviewed, "Error thrown while approving Detail Recs for Header Record ID " + recordID +  "\n" + e];
        nlapiSubmitField("customrecord_tran_order_staging_header", recordID, fields, values);
        EC.SendEmailAlert(e, ScriptName);
    }
}

Log.AutoLogMethodEntryExit(null, true, true, true);
