/**
 * Module Description
 *
 * Version    Date            Author           Remarks
 * 1.00       25 Oct 2015     ivan.sioson
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment.
 * @appliedtorecord recordType
 *
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function erpPurchaseOrderStaging_UE_BS(type){
}

function findListItem(listname,listfield, listtext){
	
	
	var listResultObject = new Object(); 
	
	var col = new Array(); 
	col[0] = new nlobjSearchColumn(listfield);
	col[1] = new nlobjSearchColumn('internalId');
	
	
	var filter = new Array();
	filter[0] = new nlobjSearchFilter(listfield, null, 'is', listtext); 	
	filter[1] = new nlobjSearchFilter('isinactive', null, 'is', 'F'); 
	
	var results = nlapiSearchRecord(listname, null, filter, col);
	

	if(!isNullOrEmpty(results)){
	
		if(results.length < 0 && results.length > 1 ){
			return null; 
		}
		else{
			
			var res = results[0]; 
			listResultObject.listValue = (res.getValue('name'));
			listResultObject.listID = (res.getValue('internalId'));
			
			nlapiLogExecution('audit', 'finding list item', JSON.stringify(listResultObject)); 
			
			return listResultObject;
		}
	
	}else{
		
		return null; 
	}

}


function enumerateListItems(){
	
	var listResultObject = new Object(); 
	
	var col = new Array(); 
	col[0] = new nlobjSearchColumn(listfield);
	col[1] = new nlobjSearchColumn('internalId');
	
	
	var filter = new Array();
	filter[0] = new nlobjSearchFilter(listfield, null, 'is', listtext); 		 
	
	var results = nlapiSearchRecord(listname, null, filter, col);
	
	
	for ( var i = 0; results != null && i < results.length; i++ )
	{
		var res = results[i]; 
		listResultObject.listValue = (res.getValue('name'));
		listResultObject.listID = (res.getValue('internalId'));
		
	} 	
	
}

function findFromGlobalSearch(objectname,operation,keyword){

	var operationstring = '';
	var globalSearch = '';

	try{
		if(operation == 'wildcard'){
			operationstring = '%';
		}

		globalSearch = objectname + ':' + operationstring + keyword;
		//nlapiLogExecution('AUDIT', 'global search', globalSearch);

		var foundObj = nlapiSearchGlobal(globalSearch);

		if(!isNullOrEmpty(foundObj)){

			if(foundObj.length > 0 ){
				var foundId = foundObj[0].getId();
				return foundId;
			}else{
				return null;
			}

		}else{
			return null;
		}

	}catch(ex){ nlapiLogExecution('ERROR', 'Find From Global', ex.toString()); }

}

function processStagingHead(stagingRecord){

	var returnObject = new Object();	
	var bStaginHead = true;  var strStagingHeadStatus = ''; 
	
	stagingRecord.setFieldValue('custrecord_erp_pohead_head_status', ''); 
	
	

	// Declarations
	var factoryId = ''; var channelId = ''; 
	var vendorId = ''; var purchaseTypeId = '';
	var seasonId = ''; var shippingPortId = '';  
	var sourcingManagerId = ''; var shippingMethodId = '';
	var locationId = ''; var shipToId = '';

	//All Texts
	var factoryTxt = stagingRecord.getFieldValue('custrecord_erp_pohead_factory');
	var factoryId = stagingRecord.getFieldValue('custrecord_erp_pohead_factory_list');	
	var channelTxt = stagingRecord.getFieldValue('custrecord_erp_pohead_channel');
	var purchaseTypeTxt = stagingRecord.getFieldValue('custrecord_erp_pohead_purchase_type');	
	var shippingPortTxt = stagingRecord.getFieldValue('custrecord_erp_pohead_shipping_port');
	var seasonTxt = stagingRecord.getFieldValue('custrecord_erp_pohead_season');	
	var sourcingManagerTxt = stagingRecord.getFieldValue('custrecord_erp_pohead_sourcingmanager');
	var shippingMethodTxt = stagingRecord.getFieldValue('custrecord_erp_pohead_shipmethod');
	
		
	//Basic Text Lookup
	
	if(!isNullOrEmpty(factoryTxt) && isNullOrEmpty(factoryId))					{ 
		//factoryId = findFromGlobalSearch('vendor','wildcard',factoryTxt); 										
		factoryId = findListItem('vendor', 'entityid', factoryTxt);
		
		if(isNullOrEmpty(factoryId)){ 	
			
			
			
			strStagingHeadStatus = strStagingHeadStatus + 'factory not found \n'; 			
			bStaginHead = false;
			
			
		}	else{
			
			factoryId = factoryId.listID; 
		}
	}
	
	
	if(!isNullOrEmpty(sourcingManagerTxt) && isNullOrEmpty(sourcingManagerId))	{ sourcingManagerId = findFromGlobalSearch('employee','wildcard',sourcingManagerTxt); 			}														
	if(!isNullOrEmpty(seasonTxt) && isNullOrEmpty(seasonId))					{ seasonId = findListItem('customrecord_wfx_season', 'name', seasonTxt);  						}		
	if(!isNullOrEmpty(purchaseTypeTxt) && isNullOrEmpty(purchaseTypeId) )		{ purchaseTypeId = findListItem('customlist_ka_po_type', 'name', purchaseTypeTxt); 				}	
	if(!isNullOrEmpty(shippingPortTxt) && isNullOrEmpty(shippingPortId) )		{ shippingPortId = findListItem('customlist_ka_shipping_port', 'name', shippingPortTxt); 		}	
	if(!isNullOrEmpty(shippingMethodTxt) && isNullOrEmpty(shippingMethodId))	{ shippingMethodId = findListItem('customlist_ka_shipping_method', 'name', shippingMethodTxt); 	}	
	if(!isNullOrEmpty(channelTxt) && isNullOrEmpty(channelId) )					{ channelId = findListItem('customrecord_ka_channel_list', 'name', channelTxt);  				}	
	
	if(isNullOrEmpty(sourcingManagerId)){ 							strStagingHeadStatus = strStagingHeadStatus + 'sourcing manager not found \n'; 	bStaginHead = false; 	}
    if(isNullOrEmpty(seasonId)){			seasonId = '';			strStagingHeadStatus = strStagingHeadStatus + 'season not found \n'; 			bStaginHead = false; 	} else { seasonId = seasonId.listID } 				
	if(isNullOrEmpty(purchaseTypeId)){		purchaseTypeId = '';	strStagingHeadStatus = strStagingHeadStatus + 'purchase type not found \n'; 	bStaginHead = false;	} else { purchaseTypeId = purchaseTypeId.listID } 	
	if(isNullOrEmpty(shippingPortId)){		shippingPortId = '';	strStagingHeadStatus = strStagingHeadStatus + 'shipping port not found \n'; 	bStaginHead = false;	} else { shippingPortId = shippingPortId.listID } 	
    if(isNullOrEmpty(shippingMethodId)){	shippingMethodId = '';	strStagingHeadStatus = strStagingHeadStatus + 'shipping method not found \n'; 	bStaginHead = false;	} else { shippingMethodId = shippingMethodId.listID } 
    if(isNullOrEmpty(channelId)){			channelId = '';			strStagingHeadStatus = strStagingHeadStatus + 'channel method not found \n'; 	bStaginHead = false;	} else { channelId = channelId.listID } 				
	
	//Related Records Lookup
	if(!isNullOrEmpty(factoryId)) { vendorId = nlapiLookupField('vendor',factoryId, 'custentity_ka_vendor_parent_vendor'); 																	if(isNullOrEmpty(vendorId)){ 									strStagingHeadStatus = strStagingHeadStatus + 'Vendor not found. Factory Parent is left blank \n'; 			bStaginHead = false; }}
	
	
	if(!isNullOrEmpty(channelId)) { 
		
		var recChannelList = nlapiLoadRecord('customrecord_ka_channel_list',channelId);
		
		if(!isNullOrEmpty(recChannelList)){ 									
			
			locationId = recChannelList.getFieldValue('custrecord_ka_channel_location'); 
			shipToId = recChannelList.getFieldValue('custrecord_ka_channel_shipto');
			
		}
	
	}
	

			
	//Set Variable IDs
	stagingRecord.setFieldValue('custrecord_erp_pohead_season_list', seasonId);
	stagingRecord.setFieldValue('custrecord_erp_pohead_factory_list', factoryId);
	stagingRecord.setFieldValue('custrecord_erp_pohead_vendor_list', vendorId);
	stagingRecord.setFieldValue('custrecord_erp_pohead_ship_method_list', shippingMethodId);
	stagingRecord.setFieldValue('custrecord_erp_pohead_sourcingman_list', sourcingManagerId);
	stagingRecord.setFieldValue('custrecord_erp_pohead_channel_list', channelId);
	stagingRecord.setFieldValue('custrecord_erp_pohead_location_list', locationId);
	stagingRecord.setFieldValue('custrecord_erp_pohead_shipto_list', shipToId);
	
	
	stagingRecord.setFieldValue('custrecord_erp_pohead_ordertype_list', purchaseTypeId);
	stagingRecord.setFieldValue('custrecord_erp_pohead_shipping_port_list', shippingPortId);
	
	
	if(bStaginHead){strStagingHeadStatus = 'No issues found'} 	
	stagingRecord.setFieldValue('custrecord_erp_pohead_head_status', 'Header Status: \n' + strStagingHeadStatus);

	
	returnObject.status = bStaginHead;
	returnObject.message = strStagingHeadStatus;
	returnObject.record = stagingRecord;
	
	return returnObject;
}

function processLineDetail(){

}


function getItemStyle(style){
	
	 
	if(!isNullOrEmpty(style)){
		
		var variantMarker = style.indexOf(".");
		
		if(variantMarker > 0){
			style = style.substring(0, variantMarker); 			
		}
	}else{
		
		style = ''; 
		
	}

	return style; 
	
	
}


function getItemQty(quantity){
	
	if(!isNullOrEmpty(quantity)){ quantity = Number(quantity); } else{ quantity = 0; }
	
	return quantity; 
}


function getItemRate(rate){
	
	if(isNullOrEmpty(rate)){
		rate = 0.0;	
	}else{
		rate = Number(rate);
	}	
	
	return rate;
}


function findItemMatrix(itemString){
	
	try{	
		var itemid = ''; 
		var searchObj = nlapiSearchGlobal('item:"' + itemString + '"');
		
		if(!isNullOrEmpty(searchObj)){			
								
			var itemname = searchObj[0].getValue('name');
			
			if(!isNullOrEmpty(itemname)){
				
				//nlapiLogExecution('AUDIT', 'Searching this Item2: ' + itemname, itemname);  			
				var intIndexFromString = itemname.search(itemString);			
				var intLength = itemname.length;
				var strSubstringOfTwo = itemname.substring(intIndexFromString, intLength);
				
				if(strSubstringOfTwo == itemString){					
					itemid = searchObj[0].getId();					
				}else{
					itemid = '';
				}			
			}			
		}else{
			itemid = '';
		}
		
		return itemid;
		
	}catch(ex){
		nlapiLogExecution("ERROR","func: findItemMatrix: Failure ",ex.toString()); return '';  	
	}		
}


function processStagingLines(stagingRecord){
	
	try{	
		
		var returnObject = new Object();	
		var bStagingLine = true;  var strStagingLineStatus = ''; 
		
	
		var arraySizeLoop = ['OS','00','0','2','4','6','8','10','12','XS/S','S/M','M/L','L/XL','XXS','XS','S','M','L','XL','2XL','26','28','30','32','34','36','38','40', '30/32','34/36','38/40' ];
		var arrayItemSizeLoop = ['os', '00','0','2','4','6','8','10','12','xss','sm','ml','lxl','xxs','xs','s','m','l','xl','xxl','26','28','30','32','34','36','38','40', '3032','3436', '3840'];
		
		
		//RESET PO MEMO	
		stagingRecord.setFieldValue('custrecord_erp_pohead_line_status', ''); 
		
		
		
		
		//GET ITEM STYLE FROM STAGING
		var strPOLine = ''; var bMatrixFound = false;
		
		var extId = 		stagingRecord.getFieldValue('externalid');
		var style = 		stagingRecord.getFieldValue('custrecord_erp_pohead_sku'); style = getItemStyle(style); 		
		var colorcode =		stagingRecord.getFieldValue('custrecord_erp_pohead_colorcode');				
		var fRate = 		stagingRecord.getFieldValue('custrecord_erp_pohead_rate');  fRate = getItemRate(fRate);
		var lineCount = 	stagingRecord.getLineItemCount('recmachcustrecord_erp_poline_parentlink');
	
		

		
		for (var i = 0 ; i < arraySizeLoop.length; i++){
		
			
			var itemstring = style + '-' + colorcode + '-' + arraySizeLoop[i]; var stringGlobal = 'item:"' + itemstring + '"';
			var strSize = stagingRecord.getFieldValue('custrecord_erp_pohead_size_'+ arrayItemSizeLoop[i]);	strSize = getItemQty(strSize);
			
			var itemid = findItemMatrix(itemstring); 
							
			//DECIDE IF IT NEEDS TO CREATE OR IF IT IS TO UPDATE A STAGING LINE		
			if(lineCount == 0 || ((i + 1) > lineCount) ){
				stagingRecord.selectNewLineItem('recmachcustrecord_erp_poline_parentlink');
			}
			else{
				stagingRecord.selectLineItem('recmachcustrecord_erp_poline_parentlink', i + 1);
			}
											
			stagingRecord.setCurrentLineItemValue('recmachcustrecord_erp_poline_parentlink', 'custrecord_erp_poline_itemtext', itemstring);
			stagingRecord.setCurrentLineItemValue('recmachcustrecord_erp_poline_parentlink', 'custrecord_erp_poline_item', itemid);
			stagingRecord.setCurrentLineItemValue('recmachcustrecord_erp_poline_parentlink', 'custrecord_erp_poline_qty', strSize);
			stagingRecord.setCurrentLineItemValue('recmachcustrecord_erp_poline_parentlink', 'custrecord_erp_poline_rate', fRate);
			stagingRecord.setCurrentLineItemValue('recmachcustrecord_erp_poline_parentlink', 'custrecord_erp_poline_line', extId+"_"+i);
			stagingRecord.commitLineItem('recmachcustrecord_erp_poline_parentlink');
			
			if(strSize > 0){			
				if(!isNullOrEmpty(itemid)){								
					strPOLine = strPOLine + itemstring +  " : Found\n"; 
				}else{				
					strPOLine = strPOLine + itemstring +  " : Not Found\n"; bStagingLine = false;
				}								
			} 
				
		}
		
		if(bStagingLine){strPOLine = 'No issues found'} 
					
		stagingRecord.setFieldValue('custrecord_erp_pohead_line_status', 'Line Status: \n' + strPOLine); 	
		
		returnObject.status = bStagingLine;
		returnObject.message = strStagingLineStatus;
		returnObject.record = stagingRecord;
		
		return returnObject;		
		
		
	}catch(ex){
		nlapiLogExecution("ERROR","func: processStagingLines: Failure ",ex.toString()); return null; 	
	}
	
}

function erpPurchaseOrderStaging_UE_AS(type){


	nlapiLogExecution('audit', 'erpPurchaseOrderStaging_UE_AS type', type);
		
	var recPOStaging = nlapiLoadRecord('customrecord_erp_po_staging', nlapiGetRecordId()); recPOStaging.setFieldValue('custrecord_erp_pohead_memo','');
	var runPOStaging = recPOStaging.getFieldValue('custrecord_erp_pohead_run'); // Check if Staging needs to be Executed
	
	
	var strPOLineMessage =	''; 
	
	if(runPOStaging == "T"){

		recPOStaging.setFieldValue('custrecord_erp_pohead_run', 'F');	//Revert to false
		
		var errorString = '';

		// PROCESS STAGING HEAD
		var objHead = processStagingHead(recPOStaging); var bProcessPurchaseOrderHead = true; var strPOHeadMessage = '';
		bProcessPurchaseOrderHead 	= 	objHead.status; 
		recPOStaging 			= 		objHead.record; 
		strPOHeadMessage 		= 		objHead.message;
		
		
		// PROCESS STAGING LINE
		var objLines = processStagingLines(recPOStaging); var bProcessPurchaseOrderLines = true; var strPOHeadMessage = '';
		bProcessPurchaseOrderLines 	= 	objLines.status; 
		recPOStaging 			= 		objLines.record; 
		strPOLineMessage 		= 		objLines.message;
		
		
		
		nlapiLogExecution('AUDIT', 'PROCESS HEAD', bProcessPurchaseOrderHead);
		nlapiLogExecution('AUDIT', 'PROCESS LINES', bProcessPurchaseOrderLines);
		
		
		
		if(bProcessPurchaseOrderHead == false){
			
			errorString = errorString + 'ERROR: Issues found on the header. Please see PO Head Details \n'
			recPOStaging.setFieldValue('custrecord_erp_pohead_memo', errorString);
		}
		
		if(bProcessPurchaseOrderLines == false){
			
			errorString = errorString + 'ERROR: Issues found on the lines. Please see PO Lines Details \n'
			recPOStaging.setFieldValue('custrecord_erp_pohead_memo', errorString);
		}
		
		// PROCESS ACTUAL PO		
		if(bProcessPurchaseOrderHead && bProcessPurchaseOrderLines ){
			recPOStaging = processPurchaseOrder(recPOStaging);
		}
		
		
		try{nlapiSubmitRecord(recPOStaging);}catch(ex){ nlapiLogExecution('ERROR', 'recPOStaging', 'recPOStaging submit ' + ex.toString()); }
		
	}
}


function processPurchaseOrder(stagingRecord){
	
	var newDate = nlapiDateToString(new Date(), "datetimetz");
	 
	var idPurchaseOrder = stagingRecord.getFieldValue('custrecord_erp_pohead_purchaseorder');
	
	nlapiLogExecution('audit', 'func: processPurchaseOrder: update or create',  idPurchaseOrder);

	if(!isNullOrEmpty(idPurchaseOrder)){

//		  nlapiLogExecution('audit', 'func: processPurchaseOrder: UPDATE: idPurchaseOrder',  idPurchaseOrder);
//		  updatePurchaseOrder(idPurchaseOrder, stagingRecord);
		  stagingRecord.setFieldValue('custrecord_erp_pohead_memo', 'This Purchase Order has already been processed. Updates are not allowed  ' + newDate);
		
	}else{

	      var poextid = stagingRecord.getFieldValue('name');
	      var poCheck = checkExistingPurchaseOrder(poextid);
	      
	      stagingRecord.setFieldValue('custrecord_erp_pohead_memo', 'Purchase Order Created  ' + newDate);
	      
	      nlapiLogExecution('audit', 'func: processPurchaseOrder: CREATE: poCheck',  poCheck);

		  if(isNullOrEmpty(poCheck)){
		        
		    	var poObject = createPurchaseOrder(stagingRecord);
		        
		    	nlapiLogExecution('audit', 'func: processPurchaseOrder: CREATE: poObject',  JSON.stringify(poObject));
		    	
		        if(poObject.status == 1){
		        			            	
		        	stagingRecord.setFieldValue('custrecord_erp_pohead_purchaseorder', poObject.id);
		        	stagingRecord.setFieldValue('custrecord_erp_pohead_memo', 'Purchase Order Created  ' + newDate);
		        	
		        }else if(poObject.status == 2){
		        	
		        	stagingRecord.setFieldValue('custrecord_erp_pohead_memo', 'ERROR: Purchase Order Creation Failed: ' + poObject.message );
		        	
		        }		        		
		  }	      	      
	      else{
	
	    	var Poid =  poCheck.internalid;	    	
	    	var Postatus = poCheck.status;
	    	var Pofactory = poCheck.factoryid;
	    	var bAddLines = true; 
	    	
	    	var errorMsg = '';
	    	
	    	
	    	var existingPOFactoryID = stagingRecord.getFieldValue('custrecord_erp_pohead_factory_list');
	    	var thisPOFactoryID = poCheck.factoryid;
	    	
	    	if(existingPOFactoryID != thisPOFactoryID){
	    		bAddLines = false;
	    		errorMsg = 'Purchase Order cannot be processed. Factory Details is different from the first Purchase Order created.';
	    	}
	    	
	    	if(!(Postatus == 'pendingReceipt' || Postatus == 'pendingApproval' || Postatus == 'pendingSupApproval'  )){
	    		bAddLines = false;
	    		errorMsg = 'Purchase Order cannot be modified when receipt/fulfilled';
	    	}
	    	
	    	
	    	if(bAddLines){
	    	
		        nlapiLogExecution('AUDIT', 'po checked', JSON.stringify(poCheck));
		        updatePurchaseOrder(Poid, stagingRecord);
		        
		        stagingRecord.setFieldValue('custrecord_erp_pohead_purchaseorder',  Poid);
		        stagingRecord.setFieldValue('custrecord_erp_pohead_memo', 'Purchase Order Appended  ' + newDate);
	        
	    	}else{	    		
	    		stagingRecord.setFieldValue('custrecord_erp_pohead_memo', errorMsg );	
	    	}
	        
	      }
      
	}
	
	return stagingRecord;
}


function isNullOrEmpty(valueStr) {
	return (valueStr == null || valueStr == "" || valueStr == undefined);
}


function checkExistingPurchaseOrder(externalid){

	
	try{
	
		if(!isNullOrEmpty(externalid)){
			
			
			var strSavedSearchIDItm = null;
		  	var arSaveSearchFiltersItm = new Array();
		  	var arSavedSearchColumnsItm = new Array();
		  	var arSavedSearchResultsItm = null;
	
		  	var arrResultObjectArray = new Array();
		  	var arrIdObjectArray = new Array();
	
		  	var objItemResult = new Object();
	
		  	arSaveSearchFiltersItm.push(new nlobjSearchFilter('externalid', null , 'is', externalid ));
	
		    arSavedSearchColumnsItm.push(new nlobjSearchColumn('internalid'));
		    arSavedSearchColumnsItm.push(new nlobjSearchColumn('externalid'));
		    arSavedSearchColumnsItm.push(new nlobjSearchColumn('tranid'));
		    arSavedSearchColumnsItm.push(new nlobjSearchColumn('status'));
		    arSavedSearchColumnsItm.push(new nlobjSearchColumn('custbody_ka_factory_id'));
		    
		    
	
		  	arSavedSearchResultsItm = nlapiSearchRecord('purchaseorder', strSavedSearchIDItm, arSaveSearchFiltersItm, arSavedSearchColumnsItm);
	
		  	if(!isNullOrEmpty(arSavedSearchResultsItm)){
	
		  		  var j = 0;
		  			var obj = new Object();
		  			obj.externalid = arSavedSearchResultsItm[j].getValue('externalid');
		  			obj.internalid = arSavedSearchResultsItm[j].getValue('internalid');
		  			obj.tranid = arSavedSearchResultsItm[j].getValue('tranid');
		  			obj.status = arSavedSearchResultsItm[j].getValue('status');
		  			obj.factoryid = arSavedSearchResultsItm[j].getValue('custbody_ka_factory_id');
	
		  			nlapiLogExecution('AUDIT','Check existing PO', obj.status); 
		  				
		  		  return obj;
	
		  	}else{
		  		return null;
		  	}
	
		}else{			
			return null; 
		}
		
	}catch(ex){		
		nlapiLogExecution('ERROR', 'func: checkExistingPurchaseOrder', ex.toString()); 	return null; 
	}
	
  	
}


function updatePurchaseOrder(poId, stageRecord){

	var updatePO = nlapiLoadRecord('purchaseorder', poId);
	
	var lineCount = stageRecord.getLineItemCount('recmachcustrecord_erp_poline_parentlink');
	var vendorId = stageRecord.getFieldValue('custrecord_erp_pohead_vendor_list');	

	//createPO.setFieldValue('customform', 100);

	if(false){ 

			    var stgPOName = stageRecord.getFieldValue('name');
			  	var stgVendorId = stageRecord.getFieldValue('custrecord_erp_pohead_vendor_list');
			  	var stgShipMethodId = stageRecord.getFieldValue('custrecord_erp_pohead_ship_method_list');
			  	var stgShippingPortId = stageRecord.getFieldValue('custrecord_erp_pohead_shipping_port_list');
			  	var stgFactoryId = stageRecord.getFieldValue('custrecord_erp_pohead_factory_list');
			  	var stgPOOrderTypeId = stageRecord.getFieldValue('custrecord_erp_pohead_ordertype_list');
			  	var stgChannelListId = stageRecord.getFieldValue('custrecord_erp_pohead_channel_list');
			  	var stgLocationListId = stageRecord.getFieldValue('custrecord_erp_pohead_location_list');
			  	var stgSeasonListId = stageRecord.getFieldValue('custrecord_erp_pohead_season_list');
			  	var stgSourcingManagerId = stageRecord.getFieldValue('custrecord_erp_pohead_sourcingman_list');
			
			  	var stgReqHod = stageRecord.getFieldValue('custrecord_erp_pohead_req_hod_date');
			  	var stgReqDc = stageRecord.getFieldValue('custrecord_erp_pohead_req_dc_date');
			  	var stgActualHod = stageRecord.getFieldValue('custrecord_erp_pohead_actual_hod');
			  	var stgActualDc = stageRecord.getFieldValue('custrecord_erp_pohead_actual_dc');
			  	var stgOrigHod = stageRecord.getFieldValue('custrecord_erp_pohead_original_hod');
			  	var stgOrigDc = stageRecord.getFieldValue('custrecord_erp_pohead_original_dc');
			  	var stgReqInStore = stageRecord.getFieldValue('custrecord_erp_pohead_req_instore');
			  	var stgOrigInStore = stageRecord.getFieldValue('custrecord_erp_pohead_original_instore');
			
			  	var stgSubId = nlapiLookupField('vendor',  stgVendorId, 'subsidiary');
			
			  	updatePO.setFieldValue('employee',nlapiGetUser());
			  	updatePO.setFieldValue('custbody_sourcing_manager',stgSourcingManagerId);
			  	updatePO.setFieldValue('custbody_ka_requested_hod',stgReqHod);
			  	updatePO.setFieldValue('custbody_ka_original_hod',stgOrigHod);
			  	updatePO.setFieldValue('custbody_ka_actual_hod',stgActualHod);
			  	updatePO.setFieldValue('custbody_ka_requested_in_dc',stgReqDc);
			  	updatePO.setFieldValue('custbody_ka_original_in_dc',stgOrigDc);
			  	updatePO.setFieldValue('custbody_ka_actual_in_dc',stgActualDc);
			  	updatePO.setFieldValue('custbody_ka_requested_in_store_date',stgReqInStore);
			  	updatePO.setFieldValue('custbody_ka_original_isd',stgOrigInStore);
			
			  	updatePO.setFieldValue('custbody_ka_season', stgSeasonListId);

	}

	if(true){

		for(var j = 1; j <= lineCount ; j++){

			var rate = stageRecord.getFieldValue('custrecord_erp_pohead_rate');

			stageRecord.selectLineItem('recmachcustrecord_erp_poline_parentlink', j);
			var item = stageRecord.getCurrentLineItemValue('recmachcustrecord_erp_poline_parentlink', 'custrecord_erp_poline_item');
			var qty = stageRecord.getCurrentLineItemValue('recmachcustrecord_erp_poline_parentlink', 'custrecord_erp_poline_qty');
			var line = stageRecord.getCurrentLineItemValue('recmachcustrecord_erp_poline_parentlink', 'custrecord_erp_poline_line');



			//nlapiLogExecution('debug', 'Update: Id: ' + item + 'qty 1', qty);
			qty  = Number(qty);
			//nlapiLogExecution('debug', 'Update: Id: ' + item + 'qty 2', qty);

			var findItem = updatePO.findLineItemValue('item', 'custcol_erp_itemline' , line);

			if(findItem > -1){
				if(!isNullOrEmpty(item) && qty > 0){

						updatePO.selectLineItem('item', findItem);
						updatePO.setCurrentLineItemValue('item', 'item', item);
						updatePO.setCurrentLineItemValue('item', 'quantity', qty);
						updatePO.setCurrentLineItemValue('item', 'rate', rate);
						updatePO.commitLineItem('item', true);

				}
				else{
					updatePO.removeLineItem('item', findItem);
				}




			}else{
				if(qty > 0){
					updatePO.selectNewLineItem('item');
					updatePO.setCurrentLineItemValue('item', 'item', item);
					updatePO.setCurrentLineItemValue('item', 'quantity', qty);
					updatePO.setCurrentLineItemValue('item', 'rate', rate);
					updatePO.setCurrentLineItemValue('item', 'custcol_erp_itemline', line);
					updatePO.commitLineItem('item', true);
				}
			}


		}

		try{

			var id = nlapiSubmitRecord(updatePO);
//			nlapiLogExecution('audit', 'updatePurchaseOrder', id);
			
			if(!isNullOrEmpty(id)){
				
				processRecordScripts(id);
			}
			

			return id;

		}catch(ex){nlapiLogExecution('ERROR', 'updatePurchaseOrder', ex.toString()); return null; }


	}

}

function processRecordScripts(internalid){
	
	
	if(!isNullOrEmpty(internalid)){
	
		var url = nlapiResolveURL('SUITELET',
			'customscript_erp_po_staging_sl',
			'customdeploy_erp_po_staging_sl', true);
	
		var params = new Array();
		params['transactionRecord'] = internalid;
		
		var slResponse = nlapiRequestURL(url, params);
	
	}
}


function createPurchaseOrder(stageRecord){

	try{
		
		var createPOObj = new Object(); 
		
		//validation goes here
		nlapiLogExecution('audit', 'createPurchaseOrder', 'START');
	
		var createPO = nlapiCreateRecord('purchaseorder'); //{recordmode: 'dynamic'});   , //initializeValues)
	
		var stgPOName = stageRecord.getFieldValue('name');
		var stgVendorId = stageRecord.getFieldValue('custrecord_erp_pohead_vendor_list');
		var stgShipMethodId = stageRecord.getFieldValue('custrecord_erp_pohead_ship_method_list');
		var stgShippingPortId = stageRecord.getFieldValue('custrecord_erp_pohead_shipping_port_list');
		var stgFactoryId = stageRecord.getFieldValue('custrecord_erp_pohead_factory_list');
		var stgPOOrderTypeId = stageRecord.getFieldValue('custrecord_erp_pohead_ordertype_list');
		var stgChannelListId = stageRecord.getFieldValue('custrecord_erp_pohead_channel_list');
		var stgLocationListId = stageRecord.getFieldValue('custrecord_erp_pohead_location_list');
		var stgSeasonListId = stageRecord.getFieldValue('custrecord_erp_pohead_season_list');
		var stgSourcingManagerId = stageRecord.getFieldValue('custrecord_erp_pohead_sourcingman_list');
		var stgLocationId = stageRecord.getFieldValue('custrecord_erp_pohead_location_list');
		var stgShipToId =stageRecord.getFieldValue('custrecord_erp_pohead_shipto_list');
		
		var stgPOMemoText = stageRecord.getFieldValue('custrecord_erp_pohead_po_memo');
		
	
		
		//OLD DATES - UNUSED
		var stgReqHod = stageRecord.getFieldValue('custrecord_erp_pohead_req_hod_date');
		var stgReqDc = stageRecord.getFieldValue('custrecord_erp_pohead_req_dc_date');
		var stgActualHod = stageRecord.getFieldValue('custrecord_erp_pohead_actual_hod');
		var stgActualDc = stageRecord.getFieldValue('custrecord_erp_pohead_actual_dc');
		var stgOrigHod = stageRecord.getFieldValue('custrecord_erp_pohead_original_hod');
		var stgOrigDc = stageRecord.getFieldValue('custrecord_erp_pohead_original_dc');
		var stgReqInStore = stageRecord.getFieldValue('custrecord_erp_pohead_req_instore');
		var stgOrigInStore = stageRecord.getFieldValue('custrecord_erp_pohead_original_instore');
	
		
		//NEW DATES 
		var stgHODOrig = stageRecord.getFieldValue('custrecord_erp_pohead_hod_orig');
		var stgHODRequest = stageRecord.getFieldValue('custrecord_erp_pohead_hod_request');
		var stgHODCurrent = stageRecord.getFieldValue('custrecord_erp_pohead_hod_current');
		var stgHODActual = stageRecord.getFieldValue('custrecord_erp_pohead_hod_actual');
		
		var stgINDCOrig = stageRecord.getFieldValue('custrecord_erp_pohead_indc_orig');
		var stgINDCRequest = stageRecord.getFieldValue('custrecord_erp_pohead_indc_request');
		var stgINDCCurrent = stageRecord.getFieldValue('custrecord_erp_pohead_indc_current');
		var stgINDCActual = stageRecord.getFieldValue('custrecord_erp_pohead_indc_actual');
		
		var stgINSTOREOrig = stageRecord.getFieldValue('custrecord_erp_pohead_instore_orig');
		var stgINSTORERequest = stageRecord.getFieldValue('custrecord_erp_pohead_instore_request');
		var stgINSTORECurrent = stageRecord.getFieldValue('custrecord_erp_pohead_instore_current');
		var stgINSTOREActual = stageRecord.getFieldValue('custrecord_erp_pohead_instore_actual');
		
		
		// GETTING PARAMETERIZED DEFAULT
		var formId = nlapiGetContext().getSetting('SCRIPT', 'custscript_erp_po_staging_formid'); 
		var intercoId = nlapiGetContext().getSetting('SCRIPT', 'custscript_erp_po_staging_incoterm');
		
		
	
		var stgSubId = nlapiLookupField('vendor',  stgVendorId, 'subsidiary');
	
		
		createPO.setFieldValue('customform', formId); // TODO: Put this in a parameter
		createPO.setFieldValue('entity', stgVendorId);
	
		createPO.setFieldValue('tranid', stgPOName);
		createPO.setFieldValue('externalid', stgPOName);
			
		createPO.setFieldValue('subsidiary', stgSubId);
		createPO.setFieldValue('location', stgLocationId);
		createPO.setFieldValue('shipto', stgShipToId);
		createPO.setFieldValue('employee', nlapiGetUser());
		createPO.setFieldValue('memo', stgPOMemoText);
		
		createPO.setFieldValue('custbody_ka_season', stgSeasonListId);		
		createPO.setFieldValue('custbody_ka_shipping_method', stgShipMethodId);
		createPO.setFieldValue('custbodyka_shipping_port', stgShippingPortId);
		createPO.setFieldValue('custbody_ka_factory_id', stgFactoryId);
		createPO.setFieldValue('custbody_ka_po_type', stgPOOrderTypeId);
		createPO.setFieldValue('custbody_ka_erp_channel_new', stgChannelListId);					
		createPO.setFieldValue('custbody_sourcing_manager',stgSourcingManagerId);
		createPO.setFieldValue('custbody_ka_incoterm',intercoId);
		
		
		
		//HOD DATES
		createPO.setFieldValue('custbody_ka_requested_hod',stgHODRequest);
		createPO.setFieldValue('custbody_ka_original_hod',stgHODOrig);
		createPO.setFieldValue('custbody_ka_current_hod',stgHODCurrent);
		createPO.setFieldValue('custbody_ka_actual_hod',stgHODActual);
		
		//IN DC DATES
		
		createPO.setFieldValue('custbody_ka_requested_in_dc',stgINDCRequest);
		createPO.setFieldValue('custbody_ka_original_in_dc',stgINDCOrig);
		createPO.setFieldValue('custbody_ka_actual_in_dc',stgINDCActual);
		createPO.setFieldValue('custbody_ka_current_in_dc',stgINDCCurrent);

		//INSTORE DATES			
		createPO.setFieldValue('custbody_ka_requested_in_store_date',stgINSTORERequest);
		createPO.setFieldValue('custbody_ka_original_isd',stgINSTOREOrig);
		createPO.setFieldValue('custbody_ka_current_in_store_date',stgINSTORECurrent);
		createPO.setFieldValue('custbody_ka_actual_isd',stgINSTOREActual);
	
			
	
		var lineCount = stageRecord.getLineItemCount('recmachcustrecord_erp_poline_parentlink');
		var rate = stageRecord.getFieldValue('custrecord_erp_pohead_rate');
		
		var intToProcess = 0; 
	
		for(var j = 1; j <= lineCount ; j++){
	
			stageRecord.selectLineItem('recmachcustrecord_erp_poline_parentlink', j);
			var item = stageRecord.getCurrentLineItemValue('recmachcustrecord_erp_poline_parentlink', 'custrecord_erp_poline_item');
			var qty = stageRecord.getCurrentLineItemValue('recmachcustrecord_erp_poline_parentlink', 'custrecord_erp_poline_qty');
			var line = stageRecord.getCurrentLineItemValue('recmachcustrecord_erp_poline_parentlink', 'custrecord_erp_poline_line');
	
	
			//nlapiLogExecution('debug', 'item id', item);
			nlapiLogExecution('debug', 'Create: id:' + item + ' qty 1', qty);
			qty = Number(qty);
			nlapiLogExecution('debug', 'Create: id:' + item + ' qty 2', qty);
	
			if((qty > 0) && !isNullOrEmpty(item)){
				createPO.selectNewLineItem('item');
				createPO.setCurrentLineItemValue('item', 'item', item);
				createPO.setCurrentLineItemValue('item', 'quantity', qty);
				createPO.setCurrentLineItemValue('item', 'rate', rate);
				createPO.setCurrentLineItemValue('item', 'custcol_erp_itemline', line);
				createPO.commitLineItem('item', true);
				intToProcess++; 
			}
		}

		if(intToProcess > 0 ){
			var id = nlapiSubmitRecord(createPO);

			createPOObj.id = id; 
			createPOObj.status = 1; 
			createPOObj.message = 'Purchase Order: Created ' + nlapiDateToString(new Date(), "datetimetz"); 
			
			processRecordScripts(id); 
							
			return createPOObj;
			
		}else{
			
			createPOObj.id = null; 
			createPOObj.status = 2; 
			createPOObj.message = 'ERRORL Purchase Order: No Lines Found'; 
			
			return createPOObj; 
		}		
			
	}catch(ex){

		createPOObj.id = null; 
		createPOObj.status = 2; 
		createPOObj.message = ex.toString(); 
		
		return createPOObj; 
	}
	


}
