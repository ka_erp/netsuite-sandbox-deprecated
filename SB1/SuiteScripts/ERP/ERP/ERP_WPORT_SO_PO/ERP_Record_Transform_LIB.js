/**
 *	File Name		:	ERP_Record_Transform_LIB.js
 *	Function		:
 * 	Remarks			:	Used for ESB/Netsuite integration
 *
 * 						Also contains functions that are shared across:
 *							-ERP_3PL_Outbound_SalesOrder_UE.js
 *							-ERP_3PL_Outbound_TransferOrder_UE.js
 *							-ERP_3PL_Inbound_SalesOrder_RS.js
 *							-ERP_3PL_Inbound_TransferOrder_RS.js
 *
 *	Prepared by		:	christopher.neal@kitandace.com, apple.villanueva@kitandace.com
 *	Release Dates	:
 * 	Current Version	:	1.0
**/

{
	/*************************************
	WTKA_MulesoftToNetSuite.js globals
	Scripts:ERP_TransferOrder_RS
			ERP_SalesOrder_RS
			ERP_3PL_Inbound_TransferOrder_RS
			ERP_3PL_Inbound_SalesOrder_RS
			etc.
	**************************************/
	var totalquantity 		= 0, 	totalQuantityReceived   = 0;
	var OrderId 			= 0, 	OrderNumber 			= 0;
	var ItemFulfillrecord 	= 0, 	InboundOrderId 			= 0;
	var invalid_orderid 	= 'F', 	DeliveryDate	  		= '';
	
	var postingMessage = '';

	var ErrorObj 			= new Object();
	ErrorObj.status 	 	= "Error";
	ErrorObj.editype 	 	= 0;
	ErrorObj.orderid 		= 0;
	ErrorObj.messages 	 	= new Array();

	var sendEmail 			= true; //false;

	var FinalShipArray, FinalOrderIdArray, 	 FinalShipQty , 	 FinalUPCCodes, 	InboundOrderNumber, 	ShipmentStatusCode;
	var shipmentLines, 	itemfulfillmentflag, finalTransformType, transformID, 		greturnreferencenumber, gtrackingnumber, 	Trantype945;
	var ccList = null; //['']; //Enter CC Addresses as array
	var toList = ['3PL_eComm_Integration@kitandace.com'];

	var emailAuthor = 243423;

	setGlobalVariables();
}

/**
 * Script Config global variables
 */
function setGlobalVariables(){
	var stToList = SCRIPTCONFIG.getScriptConfigValue('3PL: Email Notification: To List');
	if(stToList) toList = JSON.parse(stToList);

	var stCCList = SCRIPTCONFIG.getScriptConfigValue('3PL: Email Notification: CC List');
	if(stCCList) ccList = JSON.parse(stToList);

	var stEmailAuthor = SCRIPTCONFIG.getScriptConfigValue('3PL: Email Notification: Author');
	if(stEmailAuthor) emailAuthor = stEmailAuthor;
}


function FinalTransformProcess(dataIn) // Final routines for 945 process checks for SO & TO
{
	var FinalOrderNumber, FinalOrderId, SourceType, ReturnReqRefNumber, TrackingNumber, TranDate;
	var finalmessage 	= new Object();
	var fulfillmentId;
	finalmessage.status = "Success";
	//var dataIn = JSON.parse(dataIn);
	if(Trantype945 == 'salesorder_b2b' || Trantype945 == 'salesorder')
	{
		/* Comment Out Reason: this fails if ESB sends a date in UTC format
		TranDate 					= new Date(dataIn.order.shipment[0].shipmentHeader.shipDt);
		TranDate 					= nlapiDateToString(TranDate, 'date');*/
		TranDate					= convertToNSDate(dataIn.order.shipment[0].shipmentHeader.shipDt);
		finalmessage.orderid 		= transformID;
		finalmessage.ordernumber 	= dataIn.order.orderHeader.orderId;
		finalmessage.shipmentdate 	= TranDate;
		FinalOrderId 				= transformID;// dataIn.order.orderHeader.orderId;
		FinalOrderNumber 			= dataIn.order.orderHeader.orderId; //dataIn.order.orderHeader.parentOrderNumber;
		SourceType 					= 'salesorder';
		ReturnReqRefNumber 			= '';
		TrackingNumber 				= dataIn.order.shipment[0].shipmentHeader.trackingNumber;
	}
	else //Transfer Order
	{
		/* Comment Out Reason: this fails if ESB sends a date in UTC format
		TranDate 					= new Date (dataIn.transferOrder.shipment[0].shipmentHeader.shipDt);
		TranDate 					= nlapiDateToString(TranDate, 'date');*/
		TranDate 					= convertToNSDate(dataIn.transferOrder.shipment[0].shipmentHeader.shipDt);
		finalmessage.orderid 		= transformID;
		finalmessage.ordernumber 	= dataIn.transferOrder.transferHeader.transferId;

		finalmessage.shipmentdate 	= TranDate;
		FinalOrderId 				= transformID; //dataIn.transferOrder.transferHeader.transferId;
		FinalOrderNumber 			= dataIn.transferOrder.transferHeader.transferId; //dataIn.transferOrder.transferHeader.transferNumber;
		SourceType 					= 'transferOrder';
		ReturnReqRefNumber 			= '';
		TrackingNumber 				= dataIn.transferOrder.shipment[0].shipmentHeader.trackingNumber;
	}
	finalmessage.records = new Array();

	try
	{
		var itemFulfillment = nlapiTransformRecord(SourceType, FinalOrderId, 'itemfulfillment');

		if(Trantype945 == 'salesorder_b2b' || Trantype945 == 'transferOrder')
		{

			//IDS START
          if(Trantype945 == 'transferOrder'){

				  var shipId = dataIn.transferOrder.shipment[0].shipmentHeader.shipmentId ;
					if( shipId != null){
							itemFulfillment.setFieldValue('externalid', shipId); // New IDS
					}


			}

			//IDS END


			for(var i=0; i < itemFulfillment.getLineItemCount('item'); i++)
			{
				itemFulfillment.selectLineItem('item', i+1);
				var ItemCode = itemFulfillment.getLineItemValue('item', 'custcol_wtka_upccode', i+1);
				itemFulfillment.setCurrentLineItemValue('item', 'quantity', null);
				itemFulfillment.setCurrentLineItemValue('item', 'fulfill',  'F');

				for(var j=0; j <FinalUPCCodes.length; j++)
				{
					if(FinalUPCCodes[j] == ItemCode)
					{
						itemFulfillment.setCurrentLineItemValue('item', 'quantity', FinalShipQty[j]);
						itemFulfillment.setCurrentLineItemValue('item', 'fulfill',  'T');
						itemFulfillment.setCurrentLineItemValue('item', 'custcol_returnreqrefnumber', 		ReturnReqRefNumber);
						itemFulfillment.setCurrentLineItemValue('item', 'custcol_shipmenttrackingnumber', 	TrackingNumber);
						break;
					}
				}
				itemFulfillment.commitLineItem('item');
			}
		}
		else
		{
			for(var i=0; i < itemFulfillment.getLineItemCount('item'); i++)
			{
				itemFulfillment.setLineItemValue('item', 'fulfill', 						i+1, 'T');
				itemFulfillment.setLineItemValue('item', 'custcol_returnreqrefnumber', 		i+1, ReturnReqRefNumber);
				itemFulfillment.setLineItemValue('item', 'custcol_shipmenttrackingnumber', 	i+1, TrackingNumber);
			}
		}
		itemFulfillment.setLineItemValue('package', 'packageweight', 		 1, parseInt('1'));
		itemFulfillment.setLineItemValue('package', 'packagedescr', 		 1, ReturnReqRefNumber);
		itemFulfillment.setLineItemValue('package', 'packagetrackingnumber', 1, TrackingNumber);
		itemFulfillment.setFieldValue('trandate', 	TranDate);
		itemFulfillment.setFieldText('shipstatus', 	'shipped');

		try{
			fulfillmentId 	= nlapiSubmitRecord(itemFulfillment, true);
		} catch (error3) {
			//nlapiLogExecution('AUDIT', 'IVAN Final Transform Error error3', error3.toString());
			//nlapiLogExecution('AUDIT', 'IVAN Final Transform Error error3 TEST', error3.toString());
			
			
			
			
			if (error3.toString().indexOf('DUP_RCRD') > 0){
				
				//nlapiLogExecution('AUDIT', 'IVAN Final Transform Error error3 condition',(error3.toString().indexOf('DUP_RCRD') > 0));
				finalmessage.status = "Duplicate";
			}
						
			if (error3.toString().indexOf(postingMessage) > 0){
				var newDate = nlapiDateToString(new Date(), 'date');
				var dateMessage = 'Posting period error. Retrying with date: ' + newDate;
				nlapiLogExecution('DEBUG', 'dateMessage', dateMessage);
				itemFulfillment.setFieldValue('trandate', newDate); //set the transaction date to today
				itemFulfillment.setFieldValue('custbody_erp_failed_posting_date', TranDate); //set the original transaction date

				fulfillmentId = nlapiSubmitRecord(itemFulfillment); //try to submit the record again
			}
		}
		
		nlapiLogExecution('AUDIT', 'IVAN OUT', '4');

		if(Trantype945 == 'salesorder_b2b' || Trantype945 == 'salesorder')
		{
			//IS - Appended code - START
			if(fulfillmentId) nlapiSubmitField('salesorder', FinalOrderId ,'custbody_erp_inv_fulfilled', 'T');
			//IS - Appended code - END
		}

		finalmessage.records[0] 					= new Object();
		finalmessage.records[0].transactiontype 	= "Item Fulfillment";
		finalmessage.records[0].transactionid 		= fulfillmentId;
		finalmessage.records[0].transactiondate 	= TranDate;
		itemfulfillmentflag							= true;

		//Create entry into customrecord for tracking of all inbound/outbound calls
		var fulFillLogs = LogCreation(1, 2, FinalOrderId, fulfillmentId, dataIn, 1, finalmessage, 'T'); //2 - Outbound Call, 1 - Inbound call
	}
	catch(err)
	{
		var i = (itemfulfillmentflag) ? 1 : 0;
		finalmessage.records[i] 		= new Object();
		finalmessage.records[i].message = 'Item Fulfillment record could not be created. Details as below.';
		finalmessage.records[i].details = err; //err.getCode() + ' : ' + err.getDetails();

		var errorLogs = LogCreation(1, 2, FinalOrderId, 0, dataIn, 1, finalmessage, 'F'); //2 - Outbound Call, 1 - Inbound call

		var subject = 'Inbound 945 call processing failure';
		var body 	= 'Hello,<br><br>';
		body += 'Item fulfillment process against order <b>' + FinalOrderNumber + '</b> failed in NetSuite due to below error.<br>';
		body += '<br><b>Error Details: </b><br>' + err.getCode() + ' : ' + err.getDetails() ;
		body += '<br><br>Process needs to be resubmitted or handled manually through user interface after corrections.';
		body += '<br><br><br><br><br><br><br>Thanks';
		body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
		if(sendEmail)	nlapiSendEmail(emailAuthor, toList, subject, body, ccList);
	}

	try
	{
		if(Trantype945 == 'salesorder')
		{
			var finalRecord   = nlapiTransformRecord('salesorder', FinalOrderId, finalTransformType);
			//finalRecord.setFieldValue('trandate', TranDate);  //IDS 012916 Removed to default the date in the Cash Sale

			var finalRecordId = -1;
			try{
				var finalRecordId = nlapiSubmitRecord(finalRecord, true);
			} catch (error4) {
				nlapiLogExecution('DEBUG', 'Final Transform Error error4', error4.toString());

				if (error4.toString().indexOf(postingMessage) > 0){
					var newDate = nlapiDateToString(new Date(), 'date');
					var dateMessage = 'Posting period error. Retrying with date: ' + newDate;
					nlapiLogExecution('DEBUG', 'dateMessage', dateMessage);
					finalRecord.setFieldValue('trandate', newDate); //set the transaction date to today
					finalRecord.setFieldValue('custbody_erp_failed_posting_date', TranDate); //set the original transaction date

					finalRecordId = nlapiSubmitRecord(itemFulfillment); //try to submit the record again
				}
			}

			if(finalRecordId ) nlapiSubmitField('salesorder', FinalOrderId ,'custbody_erp_inv_cs_created', 'T');

			finalmessage.records[1] 				= new Object();
			finalmessage.records[1].transactiontype = (Trantype945 == 'salesorder') ? (finalTransformType == 'cashsale')? "Cash Sale" : "Invoice" : "Item Receipt";
			finalmessage.records[1].transactionid 	= finalRecordId;
			finalmessage.records[1].transactiondate = TranDate;
			var recordLogs = LogCreation(1, 2, FinalOrderId, finalRecordId, dataIn, 1, finalmessage, 'T'); //2 - Outbound Call, 1 - Inbound call
		}
	}
	catch(err1)
	{
		var subject = 'Inbound 945 call processing failure';
		var body 	= 'Hello,<br><br>';
		if(itemfulfillmentflag) body += 'Item fulfillment process executed successfully against <b>' + FinalOrderId + '</b>, but ';

		finalmessage.records[1] = new Object();
		if(Trantype945 == 'salesorder')
		{
			finalmessage.records[1].message = 'Cash Sale/Invoice could not be created. Details as below.';
			body += 'Invoice creation process against order <b>' + FinalOrderNumber + '</b> failed in NetSuite due to below error.<br>';
		}
		finalmessage.records[1].details = err1.getCode() + ' : ' + err1.getDetails();

		var finalLogs = LogCreation(1, 2, FinalOrderId, 0, dataIn, 1, finalmessage, 'F'); //0 - Outbound Call, 1 - Inbound call

		body += '<br><b>Error Details: </b><br>' + err1.getCode() + ' : ' + err1.getDetails() ;
		body += '<br><br>Transaction needs to be resubmitted or handled manually through user interface after corrections.';
		body += '<br><br><br><br><br><br><br>Thanks';
		body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
		if(sendEmail)	nlapiSendEmail(emailAuthor, toList, subject, body, ccList);
	}

	nlapiLogExecution('DEBUG', 'Final Message', JSON.stringify(finalmessage));
	return finalmessage;
}

function Validate945InBoundDataTOSO(dataIn) // Validation routines for 945 process checks for SO & TO
{
	//Pre-Validation checks on DataIn
	ErrorObj.editype 	  	= 945;
	ErrorObj.orderid 	  	= 0;
	ErrorObj.ordernumber  	= 0;
	ErrorObj.shipmentdate 	= '';
	ErrorObj.messages 	 	= new Array();
	ErrorObj.messages[0] 	= new Object();
	FinalShipArray 			= new Array();
	FinalOrderIdArray 		= new Array();
	FinalShipQty 			= new Array();
	FinalUPCCodes 			= new Array();
	var update_flag			= 'F';
	var shipmentStatus 		= '';

	//1. Check if dataIn is empty
	if(!emptyInbound(dataIn)) 	return false;

	if(Trantype945 == 'salesorder_b2b')
	{
		//2. Check if dataIn has header level objects values set
		if(!emptyObject(dataIn.order, 									"order"))			return false;
		if(!emptyObject(dataIn.order.orderHeader, 						"orderHeader"))		return false;

		transformID 		  = FetchOrderId('salesorder', dataIn.order.orderHeader.orderId);

		//For additional logging - START
		if(transformID <= 0){
			//Return invalid order
			return (invalidOrder());
		}
		//For additional logging - END

		ErrorObj.orderid 	  = transformID;
		ErrorObj.ordernumber  = dataIn.order.orderHeader.orderId;

		//Moved validation of orderDetail and shipment to here (so in case of Failure, the log will have Transaction Source)
		if(!emptyObject(dataIn.order.orderDetail, 						"orderDetail"))		return false;
		if(!emptyObject(dataIn.order.shipment, 							"shipment"))		return false;

		if(!emptyObject(dataIn.order.shipment[0].shipmentHeader.shipDt, "shipDt"))			return false;

		ErrorObj.shipmentdate = dataIn.order.shipment[0].shipmentHeader.shipDt;

		Trantype945			  = (dataIn.order.orderHeader.childSourceSystem == 'Hybris') ? 'salesorder' : 'salesorder_b2b';
		shipmentStatus = dataIn.order.shipment[0].shipmentHeader.shipmentStatus;
		if(Trantype945 == 'salesorder_b2b' && (shipmentStatus != 'CL' && shipmentStatus != 'PR'))	update_flag = 'SO';	 // Check for Updates

		//2.1 Check for Location based fulfillment
		if(Trantype945 == 'salesorder_b2b')
		{
			var POId 				= nlapiLookupField('salesorder', 	transformID, 'intercotransaction');
			var locationid 			= nlapiLookupField('purchaseorder', POId, 		 'location');

			/* Comment Out Reason: Remove validation
			//Check Send to 3PL flag in location record
			if(checkLocSendTo3plFlag(locationid) != 'T'){
				ErrorObj.messages[0].messagetype = "LocationId not configured";
				ErrorObj.messages[0].message 	 = "Order cannot be processed as Fulfillment Location " + locationid + " is not mapped for processing. Please contact Administrator";
				return false;
			}
			*/
		}

		if(update_flag == 'F')
		{
			//3. Check if dataIn has line level objects
			if(!containLines(dataIn.order.orderDetail, 		"orderDetail items present"))  	return false;
			if(!containLines(dataIn.order.shipment, 		"shipment items present")) 		return false;

			//4. Check if dataIn has LineNumber,UPCNumber and SKUId available for orderDetail and transferDetail object
			for(var i in dataIn.order.orderDetail)
			{
				if(!emptyObject(dataIn.order.orderDetail[i].orderLineNumber, 	"TransferLineNumber")) 	return false;
				if(!emptyObject(dataIn.order.orderDetail[i].item.SKUId, 		"SKUId")) 			return false;
			}

			//5. Check if dataIn has orderDetailId, quantityOrdered and quantityShipped in shipment.shipmentDetail object
			var LineDetails = dataIn.order.shipment[0];
			for (var j=0; j < LineDetails.shipmentDetail.length; j++)
			{
				var orderDetailId   = LineDetails.shipmentDetail[j].orderDetailId;
				if(!emptyObject(orderDetailId, 	 "OrderDetailId")) 	 return false;
				FinalShipArray[FinalShipArray.length] 		= orderDetailId;

				var quantityOrdered = LineDetails.shipmentDetail[j].quantityOrdered;
				if(!emptyObject(quantityOrdered, "QuantityOrdered")) return false;
				FinalOrderIdArray[FinalOrderIdArray.length] = quantityOrdered;

				var quantityShipped = LineDetails.shipmentDetail[j].quantityShipped;
				if(!emptyObject(quantityShipped, "QuantityShipped")) return false;
				FinalShipQty[FinalShipQty.length] 			= quantityShipped;

				//Check if dataIn has Quantity Ordered == Quantity Shipped
				nlapiLogExecution('DEBUG', 'TESTING orderDetailId:', orderDetailId);
				nlapiLogExecution('DEBUG', 'TESTING quantityOrdered:', quantityOrdered);
				nlapiLogExecution('DEBUG', 'TESTING quantityShipped:', quantityShipped);
				if(Trantype945 == 'salesorder' && quantityOrdered != quantityShipped)
				{
					ErrorObj.messages[0].messagetype = "Quantity Mismatch";
					ErrorObj.messages[0].message 	 = "Order cannot be processed as quantity ordered does not match quantity shipped";
					return false;
				}
			}

			//6. Fetch UPCCode from orderDetail object for final processing - in case of partial shipments
			for(var j=0; FinalShipArray != null && j < FinalShipArray.length; j++)
			{
				for(var i in dataIn.order.orderDetail)
				{
					if(dataIn.order.orderDetail[i].orderLineNumber === FinalShipArray[j])
					{
						FinalUPCCodes[FinalUPCCodes.length] = dataIn.order.orderDetail[i].item.SKUId;
					}
				}
			}
		}
	}
	else // Transfer Order Checks
	{
		//2. Check if dataIn has header level objects values set
		if(!emptyObject(dataIn.transferOrder, 					"transfer")) 			return false;
		if(!emptyObject(dataIn.transferOrder.transferHeader, 	"transferHeader"))		return false;
		if(!emptyObject(dataIn.transferOrder.transferDetail, 	"transferDetail"))		return false;
		if(!emptyObject(dataIn.transferOrder.shipment, 			"shipment"))			return false;

		shipmentStatus = dataIn.transferOrder.shipment[0].shipmentHeader.shipmentStatus;
		if(shipmentStatus != 'CL' && shipmentStatus != 'PR')	update_flag = 'TO';		 // Check for Updates
		transformID 		  = FetchOrderId('transferOrder', dataIn.transferOrder.transferHeader.transferId);

		//For additional logging - START
		if(transformID <= 0){
			//Return invalid order
			return (invalidOrder());
		}
		//For additional logging - END

		ErrorObj.orderid 	  = transformID;
		ErrorObj.ordernumber  = dataIn.transferOrder.transferHeader.transferId;

		//2.1 Check for Location based fulfillment
		var locationValue 		= FetchIdFromName('location', dataIn.transferOrder.transferHeader.toLocation.locationCode);
		if(locationValue[0] == 'Error')	return false;
		var locationid			= locationValue[0];

		/* Comment Out Reason: Remove validation
		//Check Send to 3PL flag in location record
		if(checkLocSendTo3plFlag(locationid) != 'T'){
			ErrorObj.messages[0].messagetype = "LocationId not configured";
			ErrorObj.messages[0].message = "Order cannot be processed as Fulfillment Location " + locationid + " is not mapped for processing. Please contact Administrator";
			return false;
		}
		*/

		if(update_flag == 'F')
		{
			//3. Check if dataIn has line level objects
			if(!containLines(dataIn.transferOrder.transferDetail, 	"transferOrderdetails items present")) 	return false;
			if(!containLines(dataIn.transferOrder.shipment, 		"shipment items present"))  			return false;

			ErrorObj.shipmentdate = dataIn.transferOrder.shipment[0].shipmentHeader.shipDt;

			//4. Check if dataIn has LineNumber,UPCNumber and SKUId available for orderDetail and transferDetail object
			for(var i in dataIn.transferOrder.transferDetail)
			{
				if(!emptyObject(dataIn.transferOrder.transferDetail[i].transferLineNumber, "TransferLineNumber")) 	return false;
				if(!emptyObject(dataIn.transferOrder.transferDetail[i].item.SKUId, 			"SKUId")) 				return false;
			}

			//5. Check if dataIn has orderDetailId, quantityOrdered and quantityShipped in shipment.shipmentDetail object
			var LineDetails = dataIn.transferOrder.shipment[0];

			for(var j=0; j < LineDetails.shipmentDetail.length; j++)
			{
				var orderDetailId = LineDetails.shipmentDetail[j].orderDetailId;
				if(!emptyObject(orderDetailId, 	 "OrderDetailId")) 	 return false;
				FinalShipArray[FinalShipArray.length] = orderDetailId;

				var quantityOrdered = LineDetails.shipmentDetail[j].quantityOrdered;
				if(!emptyObject(quantityOrdered, "QuantityOrdered")) return false;
				FinalOrderIdArray[FinalOrderIdArray.length] = quantityOrdered;

				var quantityShipped = LineDetails.shipmentDetail[j].quantityShipped;
				if(!emptyObject(quantityShipped, "QuantityShipped")) return false;
				FinalShipQty[FinalShipQty.length] = quantityShipped;
			}

			//6. Fetch UPCCode from transferDetail object for final processing - in case of partial shipments
			for(var j=0; j < FinalShipArray.length; j++)
			{
				for(var i in dataIn.transferOrder.transferDetail)
				{
					if(dataIn.transferOrder.transferDetail[i].transferLineNumber === FinalShipArray[j]) FinalUPCCodes[FinalUPCCodes.length] = dataIn.transferOrder.transferDetail[i].item.SKUId;
				}
			}
		}
	}

	if(update_flag == 'F')
	{
		//7. Check if Line IDs match
		if(FinalUPCCodes.length <= 0)
		{
			ErrorObj.messages[0].messagetype = "Lineids do not match";
			ErrorObj.messages[0].message = "Order cannot be processed as shipment object Lineids could not be found in Detail Object";
			return false;
		}
	}

	//8. Check if salesorder/transferOrder is Valid and in Pending Fulfillment status
	var finalmessage 							= new Object();
	finalmessage.status 						= "Success";
	finalmessage.records						= new Array();
	finalmessage.records[0] 					= new Object();
	finalmessage.records[0].status 				= "Success";
	finalmessage.records[0].transactiontype 	= "";
	finalmessage.records[0].transactionid 		= 0;
	finalmessage.records[0].transactionnumber	= 0;
	if(update_flag != 'F')	finalmessage.records[0].message	= "Order has been updated successfully"
	try
	{
		var orderInvalidFlag = 'F';
		var recType 		 = (Trantype945 == 'salesorder_b2b' || Trantype945 == 'salesorder') ? 'salesorder' : 'transferOrder';
		var record 		 	 = nlapiLoadRecord(recType, transformID);
		var recordStatus 	 = record.getFieldValue('statusRef');
		var recordStat 	 	 = record.getFieldValue('status');
		var salesItemCount 	 = record.getLineItemCount('item');

		if(Trantype945 == 'salesorder')
		{
			var recterms 		= record.getFieldValue('terms');
			var recpaymeth 		= record.getFieldValue('paymentmethod');
			salesItemCount 		= record.getLineItemCount('item');

			finalTransformType = ((recterms != null && recterms.length > 0) || (recpaymeth != null && recpaymeth.length > 0)) ? 'cashsale' : 'invoice';
		}
		if(update_flag !== 'F')	 // Check for Updates
		{
			finalmessage.records[0].transactiontype 	= recType;
			finalmessage.records[0].transactionid 		= transformID;
			finalmessage.records[0].transactionnumber	= record.getFieldValue('tranid');
		}

		if(update_flag == 'TO')	 		// Check for TO Updates
		{
			record.setFieldValue('trandate', convertToNSDate(dataIn.transferOrder.transferHeader.transactionDt));
			record.setFieldValue('memo', 	 dataIn.transferOrder.transferHeader.transferHeaderMemo);
			if(dataIn.transferOrder.transferHeader.transferStatus == 'Closed' || dataIn.transferOrder.transferHeader.transferStatus == 'Rejected')
			{
				for(var i=0; i<record.getLineItemCount('item'); i++)
				{
					record.selectLineItem('item', i+1);
					record.setCurrentLineItemValue('item', 'isclosed', 'T');
					record.commitLineItem('item');
				}
				finalmessage.records[0].message	= "Transfer order record has been closed";
			}

			try{
				nlapiSubmitRecord(record);
			} catch (error1) {
				nlapiLogExecution('DEBUG', 'TO Error error1', error1.toString());

				if (error1.toString().indexOf(postingMessage) > 0){
					var newDate = nlapiDateToString(new Date(), 'date');
					var dateMessage = 'Posting period error. Retrying with date: ' + newDate;
					nlapiLogExecution('DEBUG', 'dateMessage', dateMessage);
					record.setFieldValue('trandate', newDate); //set the transaction date to today
					record.setFieldValue('custbody_erp_failed_posting_date', convertToNSDate(dataIn.transferOrder.transferHeader.transactionDt)); //set the original transaction date

					nlapiSubmitRecord(record); //try to submit the record again
				}
			}

			return finalmessage;
		}
		else if(update_flag == 'SO')	// Check for SO Updates
		{
			record.setFieldValue('trandate', convertToNSDate(dataIn.order.orderHeader.orderTransactionDt));
			record.setFieldValue('memo', 	 dataIn.order.orderHeader.orderHeaderMemo);
			try
			{
				var customerRecord 	= nlapiLoadRecord('customer', dataIn.order.orderHeader.billingCustomer.customerId);
				customerRecord.setFieldValue('email', 		dataIn.order.orderHeader.billingCustomer.customerEmail);
				customerRecord.setFieldValue('salutation', 	dataIn.order.orderHeader.billingCustomer.honorific);
				customerRecord.setFieldValue('firstname', 	dataIn.order.orderHeader.billingCustomer.firstName);
				customerRecord.setFieldValue('lastname', 	dataIn.order.orderHeader.billingCustomer.lastName);
				customerRecord.setFieldValue('phone', 		dataIn.order.orderHeader.billingCustomer.customerPhoneNumber);
				//customerRecord.setFieldValue('gender', dataIn.order.orderHeader.billingCustomer.gender);
				nlapiSubmitRecord(customerRecord, false, true);
				finalmessage.records[1] 			= new Object();
				finalmessage.records[1].status		= "Success";
				finalmessage.records[1].recordtype 	= "customer";
				finalmessage.records[1].recordid 	= dataIn.order.orderHeader.billingCustomer.customerId;
			}
			catch(custErr)
			{
				finalmessage.status 			 	= "Exception";
				finalmessage.records[1] 			= new Object();
				finalmessage.records[1].status		= "Exception";
				finalmessage.records[1].recordtype 	= "customer";
				finalmessage.records[1].recordid 	= dataIn.order.orderHeader.billingCustomer.customerId;
				finalmessage.records[1].message 	= "Customer record cannot be updated. Details as below.";
				finalmessage.records[1].details 	= custErr; //custErr.getCode() + ' : ' + custErr.getDetails();
			}
			if(dataIn.order.orderHeader.orderStatus == 'Closed' || dataIn.order.orderHeader.orderStatus == 'Rejected')
			{
				for(var i=0; i<record.getLineItemCount('item'); i++)
				{
					record.selectLineItem('item', i+1);
					record.setCurrentLineItemValue('item', 'isclosed', 'T');
					record.commitLineItem('item');
				}
				finalmessage.records[0].message	= "Sales order record has been closed";
			}

			try{
				nlapiSubmitRecord(record, false, true);
			} catch (error2) {
				nlapiLogExecution('DEBUG', 'SO Error error2', error2.toString());

				if (error2.toString().indexOf(postingMessage) > 0){
					var newDate = nlapiDateToString(new Date(), 'date');
					var dateMessage = 'Posting period error. Retrying with date: ' + newDate;
					nlapiLogExecution('DEBUG', 'dateMessage', dateMessage);
					record.setFieldValue('trandate', newDate); //set the transaction date to today
					record.setFieldValue('custbody_erp_failed_posting_date', convertToNSDate(dataIn.order.orderHeader.orderTransactionDt)); //set the original transaction date

					nlapiSubmitRecord(record); //try to submit the record again
				}
			}

			return finalmessage;
		}
		else
		{
			if(recType == 'salesorder')
			{
				switch(recordStatus)
				{
					case 'pendingBilling':
					case 'cancelled':
					case 'fullyBilled':
					case 'closed':
					case 'partiallyFulfilled':
					case 'pendingBillingPartFulfilled':
						orderInvalidFlag = 'T';
						break;
				}
			}
			else
			{
				switch(recordStatus)
				{
					case 'pendingApproval':
					case 'pendingReceipt':
					case 'cancelled':
					case 'rejected':
					case 'closed':
						orderInvalidFlag = 'T';
						break;
				}
			}
			if(orderInvalidFlag == 'T')
			{
				ErrorObj.messages[0].messagetype = "Invalid order";
				ErrorObj.messages[0].message 	 = "Order cannot be fulfilled as it is in " + recordStat + " status.";
				return false;
			}

			//9. Check if fulfillment can be processed
			try
			{
				var itemFulfillment 	 = nlapiTransformRecord(recType, transformID, 'itemfulfillment');
				var itemFulfillItemCount = itemFulfillment.getLineItemCount('item');
				var inventoryItemFulfillmentCount = 0;

				for (var i=1; i<=salesItemCount; i++)
				{
					itemType = record.getLineItemValue('item','itemtype',i);
					if(itemType != 'GiftCert')
					{
						inventoryItemFulfillmentCount += 1;
					}
				}

				if(itemFulfillItemCount == 0)
				{
					ErrorObj.messages[0].messagetype = "Item Fulfillment Failure";
					ErrorObj.messages[0].message 	 = "Order cannot be processed as items are backordered and there are no items available to fulfill";
					return false;
				}
				else
				{
					if(Trantype945 == 'salesorder' && inventoryItemFulfillmentCount != itemFulfillItemCount)
					{
						ErrorObj.messages[0].messagetype = "Item Fulfillment Failure";
						ErrorObj.messages[0].message = "Order cannot be processed as item count on fulfillment process does not match item count on salesorder";
						return false;
					}
				}
			}
			catch(err1)
			{
				ErrorObj.status 				 = "Exception";
				ErrorObj.messages[0].messagetype = "Item Fulfillment Failure";
				ErrorObj.messages[0].message 	 = "Order cannot be fulfilled as items are backordered and there are no items available to fulfill.";
				nlapiLogExecution('DEBUG', 'Transform Error', err1);
				return false;
			}
		}
	}
	catch(err)
	{
		nlapiLogExecution('debug', 'Order load error', err);
		if(update_flag != 'F')	 // Check for Updates
		{
			finalmessage.status 					= "Exception";
			finalmessage.records[0].status			= "Exception";
			finalmessage.records[0].transactiontype = recType;
			finalmessage.records[0].transactionid 	= transformID;
			if(Trantype945 == 'transferOrder')	finalmessage.records[0].transactionnumber	= dataIn.transferOrder.transferHeader.transferId;
			else								finalmessage.records[0].transactionnumber	= dataIn.order.orderHeader.orderId;
			finalmessage.records[0].message 	 	= "Order cannot be updated. Details as below.";
			finalmessage.records[0].details 		= err; //err.getCode() + ' : ' + err.getDetails();
			return finalmessage;
		}
		else	return (invalidOrder());
	}
	return true;
}

function Validate940(type, OrderId) // Validations routines to check inbound request from REST API
{
	//Pre-Validation checks on DataIn
	ErrorObj.editype 	 	= 940;
	ErrorObj.orderid 		= OrderId;
	ErrorObj.messages[0] 	= new Object();

	//Check if Order is Valid and in Pending Fulfillment status
	try
	{
		var record = nlapiLoadRecord(type, OrderId);
		for(var i=0; i < record.getLineItemCount('item'); i++)
		{
			totalquantity 		  += parseInt(record.getLineItemValue('item',   'quantity',			  i+1));
			totalQuantityReceived += parseInt(record.getLineItemValue('item',   'quantityreceived',   i+1));
		}
	}
	catch(err)
	{
        nlapiLogExecution('DEBUG','error message',err.message);
		return (invalidOrder());
	}
	return true;
}

/**
 * Returns the Send To 3PL flag value of a location record
 */
function checkLocSendTo3plFlag(stLocId){

	return nlapiLookupField('location', stLocId, 'custrecord_perform_sendto3pl');

}

/**
 * Translates EDI Status to equivalent ID
 */
function getEDIStatus(stStatus){

	switch(stStatus){
		case 'Success':
			return 1;
		case 'Failure':
			return 2;
		case 'Exception':
			return 3;
		default:
			return '';
	}

}

/**
 * Inbound 945 Error Processing (instead of ErrorProcess in WTKA_Library.js)
 */
function ErrorProcess_Inbound945(dataIn){
	var errorStatus = (ErrorObj.status == "Error") ? 2 : 3; // 2- Error, 3-Exception
	if(invalid_orderid == 'F'){
		var orderid 	= ErrorObj.orderid;
		var ErrorLogs 	= LogCreation(1, 2, orderid, 0, dataIn, errorStatus, ErrorObj, 'F'); //0 - Outbound Call, 1 - Inbound call
		nlapiLogExecution('DEBUG', 'Error Object', JSON.stringify(ErrorObj));
	}

	send3PLIntegrationErrorMail(dataIn, 'Inbound 945 call processing failure', ErrorObj.ordernumber);
}

/**
 * Sends error mail
 */
function send3PLIntegrationErrorMail(dataIn, subject, orderNumber){
	var body = 'Hello,<br><br>';
	if(invalid_orderid == 'T')	body += 'Process failed in NetSuite. <br><b>Data Received from Mulesoft: </b><br>';
	else	body += 'Process against <b>' + orderNumber + '</b> failed in NetSuite. <br><b>Data Received from Mulesoft: </b><br>';
	body += '<br>' + JSON.stringify(dataIn) + '<br><br><br>';
	body += '<b>NetSuite Response: </b><br>';
	body += JSON.stringify(ErrorObj);
	body += '<br><br><br><br><br><br><br>Thanks';
	body += '<br><br><b>This is an auto-generated email. Please do not reply.</b>';
	if(sendEmail)	nlapiSendEmail(emailAuthor, toList, subject, body, ccList);
}

/**
 * Returns a NetSuite-acceptable date
 */
function convertToNSDate(stDate){

	if(isUTCFormat(stDate)){
		stDate = moment(stDate).format('l LT');
	}

	return nlapiDateToString(new Date(stDate), 'date');
}

/**
 * Determines if a date is in UTC format
 */
function isUTCFormat(stDate){

	var utcformat = false;

	if(stDate.match(/^\d{4}\-\d{1,2}\-\d{1,2}T\d{1,2}:\d{2}:\d{2}Z/) != null){
		utcformat = true;
	} else if(stDate.match(/^\d{4}\-\d{1,2}\-\d{1,2}T\d{1,2}:\d{2}:\d{2}[+-]\d{1,2}:\d{2}/) != null){
		utcformat = true;
	}

	return utcformat;
}

/**
 * Returns an object of shipping info
 */
function getShippingInfo(record){

	//Default
	var shippingMethod 	= 'FedEx';
	var shipmentCode 	= 'FedEx';
	var carrierName 	= 'FedEx';

	//Script Config Implementation
	var scShippingMethod 	= SCRIPTCONFIG.getScriptConfigValue('3PL: SO/TO: Shipping Method');
	var scShipmentCode 		= SCRIPTCONFIG.getScriptConfigValue('3PL: SO/TO: Shipment Code');
	var scCarrierName 		= SCRIPTCONFIG.getScriptConfigValue('3PL: SO/TO: Carrier Name');

	if(scShippingMethod){
		var objShippingMethod = JSON.parse(scShippingMethod);

		var stShipCarrier = record.getFieldValue('shipcarrier');

		if(stShipCarrier){
			if(objShippingMethod[stShipCarrier.toLowerCase()]){
				shippingMethod = objShippingMethod[stShipCarrier.toLowerCase()];
			}
		}
	}

	if(scShipmentCode){
		shipmentCode = scShipmentCode;
	}

	if(scCarrierName){
		carrierName = scCarrierName;
	}

	return {
		shippingMethod 	: shippingMethod,
		shipmentCode	: shipmentCode,
		carrierName		: carrierName
	};
}
