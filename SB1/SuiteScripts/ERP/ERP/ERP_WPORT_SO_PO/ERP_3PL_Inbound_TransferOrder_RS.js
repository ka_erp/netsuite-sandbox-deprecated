/**
 *	File Name		:	ERP_3PL_Inbound_TransferOrder_RS.js
 *	Function		:	
 * 	Remarks			:	
 *	Prepared by		:	christopher.neal@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

function putRESTlet945TO(dataIn) //945 call from Cloudhub for TO processing
{
	nlapiLogExecution('DEBUG', 'DATAIN', JSON.stringify(dataIn));
	Trantype945 			= 'transferOrder';
	var PreValidationChecks = Validate945InBoundDataTOSO(dataIn); //call to ERP_Record_Transform.js
	switch(PreValidationChecks)
	{
		case true:
			var transformProcess = FinalTransformProcess(dataIn); //call to ERP_Record_Transform.js
			nlapiLogExecution('DEBUG', 'transformProcess', JSON.stringify(transformProcess));
			return transformProcess;

		case false:
			var errorProcess = ErrorProcess_Inbound945(dataIn); //call to ERP_Record_Transform.js
			
			//For additional logging - START
			//Log invalid order also
			if(invalid_orderid == 'T'){
				LogCreation(1, 2, 0, 0, dataIn, getEDIStatus(ErrorObj.status), ErrorObj, 'F'); //2 - Outbound Call, 1 - Inbound call
			}
			//For additional logging - END
			
			return ErrorObj;

		default:

			//For additional logging - START
			//Log Record Update request
			LogCreation(1, 2, PreValidationChecks.records[0].transactionid, 0, dataIn, getEDIStatus(PreValidationChecks.status), PreValidationChecks, 'T'); //2 - Outbound Call, 1 - Inbound call
			//For additional logging - END
				
			nlapiLogExecution('DEBUG', 'updateProcess', JSON.stringify(PreValidationChecks));
			return PreValidationChecks;
	}
}