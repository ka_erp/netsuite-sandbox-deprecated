/**
 *	File Name		:	ERP_3PL_Outbound_SalesOrder_UE.js
 *	Function		:	
 * 	Remarks			:	
 *	Prepared by		:	christopher.neal@kitandace.com, apple.villanueva@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/



/*
*	name: beforeLoad_showButton
*	descr: 	1) Determine whether to show the button 'Send to 3PL'
*				-sales order must be 'pendingFulfillment'
*				-sales order must have an associated 'Paired Intercompany Transaction'
*				-the associated record *
*			2) Compile previous logs associated with this sales order
*
*	author: christopher.neal@kitandace.com
*	@param: {type} type of operation on the record
*   @param: {form} the form the scirpt is deployed on 
*   @param: {request} the request sent to the script
*
*/
function beforeLoad_showButton(type, form, request){

	var recType = nlapiGetRecordType();

	if(recType == 'salesorder' && nlapiGetContext().getExecutionContext() == 'userinterface' && type == 'view'){

		var UserRole = nlapiGetContext().getRole();		
		var formStatus = nlapiGetFieldValue('statusRef');
	
		var buttonDisplayCheck = 'F';
		
		//1) Determine whether to show the button 'Send to 3PL'
		if(formStatus == 'pendingFulfillment'){
			
			if(readValue(nlapiGetFieldValue('intercotransaction')) != ""){
				if(UserRole != 3){ // 3 = Administrator	
					var locationid = nlapiLookupField('purchaseorder', nlapiGetFieldValue('intercotransaction'), 'location');

					if(locationid){
						//Check if this location can display the 'Ship to 3PL' button
						buttonDisplayCheck = nlapiLookupField('location', locationid, 'custrecord_perform_sendto3pl');
					}
				} else{
					buttonDisplayCheck = 'T';
				}
			}
		}

		if(buttonDisplayCheck == 'T'){
			form.addButton('custpage_sendto3pl_btn', "Send to 3PL", 'callButtonSuitelet_sendTo3pl()'); //function is in ERP_3PL_Button_CL.js
			form.setScript('customscript_erp_3pl_button_cl');
		}

		/* Commented out: Logs will be shown using custom sublist under ERP Integration subtab 
		//2) Find and compile previous logs associated with this record
		var cols = new Array();
		cols[cols.length] = new nlobjSearchColumn('custrecord_editype');
		cols[cols.length] = new nlobjSearchColumn('custrecord_edinumber');
		cols[cols.length] = new nlobjSearchColumn('custrecord_transaction_source');
		cols[cols.length] = new nlobjSearchColumn('custrecord_edicallstatus');

		var filters = new Array();	
		filters[filters.length] = new nlobjSearchFilter('custrecord_transaction_source', null, 'anyof', nlapiGetRecordId());
		filters[filters.length] = new nlobjSearchFilter('custrecord_editype', 			 null, 'anyof', '1', '2'); //Inbound && Outbound call
	
		var searchResult = nlapiSearchRecord('customrecord_integrationlogs', null, filters, cols);
		if(searchResult !=  null){
			createLogs(form, searchResult);	//call to WTKA_Library.js
		}
		*/
	}
}
