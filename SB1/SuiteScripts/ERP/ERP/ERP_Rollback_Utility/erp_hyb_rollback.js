{
	FOLDER = 556120;
}

function scheduled_rollback(type) {

	nlapiLogExecution('Debug', 'scheduled_rollback', '::::::::START::::::::');	

	//Get orders to rollback from saved search
	var search = nlapiLoadSearch('', 1466); //1658
	var resultSet = search.runSearch();
	var arResults;
	if(resultSet){
		arResults = getAllSearchResults(resultSet);
	}

	//Process the results
	if(arResults){
			
		//Get Hybris Order numbers to rollback
		var stHybOrderNo;
		var stFilter = 'CASE';
		for(var i = 0; i < arResults.length; i++){
			stHybOrderNo = arResults[i].getValue('custbody_wtka_extsys_hybris_order');
			stFilter += ' WHEN {custbody_wtka_extsys_hybris_order} = \'' + stHybOrderNo + '\' THEN 1';
		}		
		stFilter += ' ELSE 0 END';
		
		//Search all SO with the Hybris Order numbers (because there might be multiple SO for a Hybris Order number)
		var arFilters = [//new nlobjSearchFilter('mainline', null, 'is', 'T'), //Main Line = true
		                 new nlobjSearchFilter('formulanumeric', null, 'equalto', 1).setFormula(stFilter)];
		
		var arColumns = [new nlobjSearchColumn('internalid'),				//Internal ID
						 new nlobjSearchColumn('tranid'),					//Tran ID
						 new nlobjSearchColumn('custbody_wtka_extsys_hybris_order'),
						 new nlobjSearchColumn('custbody_wtka_extsys_order_number'),
						 new nlobjSearchColumn('applyingtransaction'),
						 new nlobjSearchColumn('recordtype', 'applyingtransaction'),
						 new nlobjSearchColumn('tranid', 'applyingtransaction')];
		
		var soSearch = nlapiCreateSearch('salesorder', arFilters, arColumns);
		resultSet = soSearch.runSearch();
		if(resultSet){
			arResults = getAllSearchResults(resultSet);
		}
		
		//Store results
		var objRollback = {};
		var stInternalId, stTranId, stApplyTran, stApplyTranType, stApplyTranId;
		for(var i = 0; i < arResults.length; i++){
			stHybOrderNo = arResults[i].getValue('custbody_wtka_extsys_hybris_order');
			stInternalId = arResults[i].getValue('internalid');
			stTranId = arResults[i].getValue('tranid');
			stApplyTran = arResults[i].getValue('applyingtransaction');
			stApplyTranType = arResults[i].getValue('recordtype', 'applyingtransaction');
			stApplyTranId = arResults[i].getValue('tranid', 'applyingtransaction');
			
			if(!objRollback[stHybOrderNo]){
				objRollback[stHybOrderNo] = {};
			}
			
			if(!objRollback[stHybOrderNo][stInternalId]){
				objRollback[stHybOrderNo][stInternalId] = {};
				objRollback[stHybOrderNo][stInternalId].tranid = stTranId;
				objRollback[stHybOrderNo][stInternalId].relatedrecords = {};
			}
						
			if(stApplyTran){				
				if(!objRollback[stHybOrderNo][stInternalId].relatedrecords[stApplyTran]){
					objRollback[stHybOrderNo][stInternalId].relatedrecords[stApplyTran] = {};
				}
				objRollback[stHybOrderNo][stInternalId].relatedrecords[stApplyTran].type = stApplyTranType;
				objRollback[stHybOrderNo][stInternalId].relatedrecords[stApplyTran].tranid = stApplyTranId;
			}			
		}
		
		//Rollback
		var stDeletedLog = '';
		for(var hyborder in objRollback){
			
			stDeletedLog += hyborder + ',';
			
			for(var so in objRollback[hyborder]){
				var bHasCashSale = false;
				
				//check if cash sale has already been created
				for(var applytran in objRollback[hyborder][so].relatedrecords){
					if(objRollback[hyborder][so].relatedrecords[applytran].type.toUpperCase() == 'CASHSALE'
						/*|| objRollback[hyborder][so].relatedrecords[applytran].type.toUpperCase() == 'INVOICE'*/){
						bHasCashSale = true;
						break;
					}
				}
				
				//do not rollback if cash sale has already been created
				if(bHasCashSale){
					nlapiLogExecution('Debug', 'scheduled_rollback', 'bHasCashSale: ' + bHasCashSale);
					continue;
				}
				
				for(var applytran in objRollback[hyborder][so].relatedrecords){
					try{
						//Delete applying transaction
						nlapiDeleteRecord(objRollback[hyborder][so].relatedrecords[applytran].type, applytran, {deletionreason: 8}); //External System Rollback
						stDeletedLog += objRollback[hyborder][so].relatedrecords[applytran].type + ':' + objRollback[hyborder][so].relatedrecords[applytran].tranid + ',';
					} catch(ex){
						stDeletedLog += objRollback[hyborder][so].relatedrecords[applytran].type + ':' + objRollback[hyborder][so].relatedrecords[applytran].tranid + ':' + ex + ',';
					}
					
					checkGovernance();
				}
				
				try{
					//Delete SO
					nlapiDeleteRecord('salesorder', so, {deletionreason: 8}); //External System Rollback
					stDeletedLog += 'salesorder:' + objRollback[hyborder][so].tranid;
				} catch(ex){
					stDeletedLog += 'salesorder:' + objRollback[hyborder][so].tranid + ':' + ex;
				}
						
				checkGovernance();
			}
			
			stDeletedLog += '\r\n';
		}
		
		//Print logs into a file
		var objFile = nlapiCreateFile(moment() + '.txt','PLAINTEXT', stDeletedLog + '\r\n\r\n' + JSON.stringify(objRollback));
		objFile.setFolder(FOLDER);
		objFile.setEncoding('UTF-8');
		var fileid = nlapiSubmitFile(objFile); 
		
		nlapiLogExecution('Debug', 'scheduled_rollback', 'fileid: ' + fileid);
	}
	
	nlapiLogExecution('Debug', 'scheduled_rollback', '::::::::END::::::::');	
}

/*******************
 Governance Check
********************/
function checkGovernance(){
	if (!validUsageRemaining()) {
		yieldScript();
	} else {
		nlapiLogExecution('DEBUG', 'scheduled_rollback', 'Remaining usage = ' + nlapiGetContext().getRemainingUsage());
	}
}

function validUsageRemaining() {                                                 
	return nlapiGetContext().getRemainingUsage() >= 500;
}

function yieldScript() {
	var state = nlapiYieldScript();
	var message = 'Reason: ' + state.reason + ' Info: ' + state.information + ' Size: ' + state.size;

	if (state.status == 'FAILURE'){

		nlapiLogExecution('Debug', 'Sched Script', 'Failed to yield script; exiting. ' + message);
		nlapiLogExecution('Error', 'Sched Script', 'Failed to yield script; exiting. ' + message);

		throw nlapiCreateError('SCRIPT_ERROR', 'Failed to yield script; exiting. ' + message);

	} else if (state.status == 'RESUME'){

		nlapiLogExecution('Debug', 'Resuming script because of ' + message);
		nlapiLogExecution('Audit', 'Resuming script because of ' + message);

	}
}