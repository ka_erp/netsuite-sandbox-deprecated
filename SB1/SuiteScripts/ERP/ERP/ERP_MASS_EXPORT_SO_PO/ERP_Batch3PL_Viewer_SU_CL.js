/**
 *	File Name		:	ERP_Batch3PL_Viewer_SU_CL.js
 *	Function		:	Suitelet to show the user the progress of the order mass export to the 3PL.
 *						Also must be deployed as a client side script to allow for Button press events.
 *	Authors			:	christopher.neal@kitandace.com
 *	Release Dates	:	
 * 	Current Version	:	1.0
**/

{
    STATUS_SEND = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Send to 3PL');
    STATUS_EXPORTED = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Completed');
    STATUS_FAIL = SCRIPTCONFIG.getScriptConfigValue('Export to 3PL Status: Failed');
    
    MAX_RESULTS = 1000;
    KA_ALLOCATION_ROLE = 1008;    
}

/*
*	name: ERP_Batch3PL_Viewer_SU
*	descr: Entry point into script. Create the GUI form and rerturn it as a response.
*
*	author: chrisotpher.neal@kitandace.com
*	@param: {obj} request - request to the suitelet. Example URL resquest: https://system.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=563&deploy=1&time=1467744719793&recordtype=salesorder&transactions=1972957$2545778$2580851$2592311$2708219
*	@return: {obj} response - response returned from the suitelet
*	
*/
function ERP_Batch3PL_Viewer_SU(request, response){

    var method = request.getMethod();
    nlapiLogExecution('DEBUG', 'method', method);

    var form = nlapiCreateForm('Processing Records'); 
    form.setScript('customscript_erp_batch3pl_viewer_c'); //set client side script for button calls
    form = createForm(form, request);

    response.writePage(form); //return the GUI form
}


/*
*	name: createForm
*	descr: Entry point into script. Create the GUI form and rerturn it as a response.
*
*	author: chrisotpher.neal@kitandace.com
*   @param: {obj} form - the form which the user will see
*   @param: {obj} request - request to the suitelet, contains the 
*	@return: {obj} response - response returned from the suitelet
*	
*/
function createForm(form, request){

	form.addButton('custpage_refreshbutton', 'See Status', 'seestatus();'); //Add button and the associated function call on the button is pressed

	var transactionsArray = [];
	if(isNotBlank(request.getParameter('transactions'))){
    	nlapiLogExecution('DEBUG', 'transaction params', request.getParameter('transactions'));
    	transactionsArray = request.getParameter('transactions').split('$');
    }
    nlapiLogExecution('DEBUG', 'transactions', JSON.stringify(transactionsArray));

    var time = request.getParameter('time');
    var displayDate = nlapiDateToString(new Date(parseInt(time)), 'datetimetz');
    nlapiLogExecution('DEBUG', 'displayDate', displayDate);

	var recordType = request.getParameter('recordtype');
    nlapiLogExecution('DEBUG', 'recordType', recordType);

    form.addField('custpage_sendto3pllist', 'textarea', 'Transactions To Send').setDisplayType('hidden'); //Used to store an array of all the Orders to process. The script will automatically fill this in.
    form.addField('custpage_enochstart', 'integer', 'Processing start (enoch time)').setDisplayType('hidden'); //Used to store an array of all the Orders to process. The script will automatically fill this in.
    form.addField('custpage_recordtype', 'text', 'Record type').setDisplayType('hidden'); //Used to store an array of all the Orders to process. The script will automatically fill this in.

    form.addField('custpage_starttime', 'text', 'Process sent to QUEUE: ' + displayDate).setDisplayType('inline');
    form.addField('custpage_scheudled', 'text', 'See schedule status at Page: Scheduled Script Status').setDisplayType('inline'); 

    //add list to the form
    var subList = form.addSubList('tranlist', 'list', 'Transactions');

    subList.addField('id', 'integer', 'id').setDisplayType('hidden');
    subList.addField('recordtype', 'text', 'recordtype').setDisplayType('hidden');
    subList.addField('starttime', 'text', 'starttime').setDisplayType('hidden');
    subList.addField('transactionnumber', 'text', 'Tran#', 'transaction');
    subList.addField('custbody_export_3pl_status', 'select', '3PL Export Status', 'customlist_3pl_export_statis').setDisplayType('inline');
    subList.addField('lastmodifieddate','text', 'Time Processed');

    var cols = new Array();
	cols.push(new nlobjSearchColumn('custbody_export_3pl_status'));
	cols.push(new nlobjSearchColumn('tranid'));
	cols.push(new nlobjSearchColumn('lastmodifieddate'));

	var filters = new Array();
	filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));
	filters.push(new nlobjSearchFilter('internalid', null, 'anyof', transactionsArray));
	var transactions = nlapiSearchRecord(recordType, null, filters, cols);

    if (transactions != null){

    	for (var curTran = 0; curTran < transactions.length; curTran++){
	    	
	    	subList.setLineItemValue('id', curTran+1, transactions[curTran].getId());
	    	subList.setLineItemValue('transactionnumber', curTran+1, transactions[curTran].getValue('tranid'));

	    	var dateString = transactions[curTran].getValue('lastmodifieddate');
	    	var date = new Date(dateString).getTime(); //get current enoch time

	    	nlapiLogExecution('DEBUG', 'custbody_export_3pl_status', transactions[curTran].getValue('custbody_export_3pl_status'));
	    	var curStatus = transactions[curTran].getValue('custbody_export_3pl_status')
	    	

	    	var processingStartDate = new Date(parseInt(time));
	    	var lastmodifiedDate = new Date(transactions[curTran].getValue('lastmodifieddate'));


		  	var timeString = time;
		  	nlapiLogExecution('DEBUG', '1 timeString', timeString)

		  	timeString = timeString.substring(0, timeString.length-4);
		  	timeString += '0000';
		  	nlapiLogExecution('DEBUG', '2 timeString', timeString)
		  	
		  	var processingStartDate = new Date(parseInt(timeString));
		  	processingStartDate.setSeconds(0);

	    	var lastmodifiedDate = new Date(transactions[curTran].getValue('lastmodifieddate'));

	    	nlapiLogExecution('DEBUG', 'processingStartDate', processingStartDate);
	    	nlapiLogExecution('DEBUG', 'lastmodifiedDate', lastmodifiedDate);

		  	var processingStartDateInt = parseInt(processingStartDate.getTime());
			var lastmodifiedDateInt = parseInt(lastmodifiedDate.getTime());

	    	nlapiLogExecution('DEBUG', 'processingStartDateInt', processingStartDateInt);
	    	nlapiLogExecution('DEBUG', 'lastmodifiedDateInt', lastmodifiedDateInt);
	    	nlapiLogExecution('DEBUG', 'lastmodifiedDateInt >= processingStartDateInt', lastmodifiedDateInt >= processingStartDateInt);

	    	//only show the Export Status and the Last Modified Date if the Last Modified Date is after or equal to when the process started.
	    	if (lastmodifiedDateInt >= processingStartDateInt){
	    		subList.setLineItemValue('lastmodifieddate', curTran+1, transactions[curTran].getValue('lastmodifieddate'));
	    		subList.setLineItemValue('custbody_export_3pl_status', curTran+1, curStatus);	
	    	}

	    	subList.setLineItemValue('recordtype', curTran+1, recordType);
		    subList.setLineItemValue('starttime', curTran+1, time);		    	
    	}
    }

    return form;
}


/*
*	name: seestatus
*	descr: 	Create the URL to this suitelet, then refresh the page by calling itself. 
*			By refreshing this page the user will be able to see the status of the records that are 
*			being exported.
*
*	author: chrisotpher.neal@kitandace.com
*	
*/
function seestatus(){


	//see if there has been any transactions selected
    var count = nlapiGetLineItemCount('tranlist'); //number of transactions in the list

    var transactionType = nlapiGetFieldValue('custpage_recordtypes');


    //loop through all the lines
    var time;
    var recordType;
    var transactionList = ''; //create a string of transaction ids
    for (var i = 1; i <= count; i++){

    	if (i == 1){
    		time = nlapiGetLineItemValue('tranlist', 'starttime', i)
    		recordType = nlapiGetLineItemValue('tranlist', 'recordtype', i)
    	}

    	transactionList = transactionList + nlapiGetLineItemValue('tranlist', 'id', i) + '$';
    }
    transactionList = transactionList.slice(0, -1); //remove last character, there is an extra '$'

    var viewerUrl =  nlapiResolveURL('SUITELET','customscript_erp_batch3pl_viewer_su','customdeploy_erp_batch3pl_viewer_su',null) + '&time=' + time + '&recordtype=' + recordType + '&transactions=' + transactionList; //create the URL
    console.log(viewerUrl);
    window.open(viewerUrl, '_self', false); //refresh the page

}

/*
*   name: isBlank
*   descr: Determines whether a variable has been set
*
*   author: chrisotpher.neal@kitandace.com (copied from Purchase_OrderAggregate.js)
*/
function isBlank(s) {
    return s === undefined || s === null || s === '' || s.length < 1;
}

/*
*   name: isNotBlank
*   descr: Determines whether a variable has NOT been set
*
*   author: chrisotpher.neal@kitandace.com (copied from Purchase_OrderAggregate.js)
*/
function isNotBlank(s) {
    return !isBlank(s);
}