/**
 *	File Name		:	ERP_IntegrationRouter_RS.js
 *	Function		:	
 * 	Authors			:	christopher.neal@kitandace.com
 *	Company			:	Kit & Ace
 *	Release Dates	:	
 * 	Current Version	:	
 *
 *	Description: 	A RESTlet to act as a router of integration messages between Netsuite and other systems
 *				 	Receives a message and creates a new external sync object.
 *
**/

var returnObject = {};
var folderName = 'External Sync Dev';

var IN_OUT = {};
IN_OUT.IN = 1;
IN_OUT.OUT = 2;

var SYNC_SYSTEM = {};
SYNC_SYSTEM.NETSUITE = 1;
SYNC_SYSTEM.WFX = 2;
SYNC_SYSTEM.ESB = 3;

var SYNC_STATUS = {};
SYNC_STATUS.CREATED = 1;
SYNC_STATUS.PENDING = 2;
SYNC_STATUS.ERROR = 3;

var SYNC_RECORD_TYPE = {};
SYNC_RECORD_TYPE.PURCHASE = 1;

var SYNC_TYPE = {};
SYNC_TYPE.CREATE = 1;
SYNC_TYPE.READ = 2;
SYNC_TYPE.UPDATE = 3;
SYNC_TYPE.DELETE = 4;


/*
*	name: post_IntegrationMessage.
*	descr:  Receive an integration message.
*			Determine which type of integration is to be performed.
*			Validate the message then create the Router Object Record.
*
*	author: christopher.neal@kitandace.com
*	@param: {obj} dataIn - JSON object of record to integrate with Netsuite
*	@return: tbd
*
*/
function post_IntegrationMessage(dataIn)
{

	nlapiLogExecution('DEBUG', 'Inbound Request', JSON.stringify(dataIn));

	var messageType = determineMessageType(dataIn);

	nlapiLogExecution('DEBUG', 'Message Type', messageType);

	if (messageType == SYNC_RECORD_TYPE.PURCHASE){ //Purchase Order Sync

		nlapiLogExecution('DEBUG', 'Received Purchase Order Sync', 'Processing...');
		initiatePurchaseOrderSync(dataIn);	

	} else {
		nlapiLogExecution('DEBUG', 'Unknown request', JSON.stringify(dataIn));
		returnObject.message = 'Unknown message type received';
	} 

	return returnObject;
}


/*
*	name: determineMessageType 
*	descr: determine what type of message has been sent to the RESTlet
*
*	author: christopher.neal@kitandace.com
*	@param: {obj} dataIn - object sent to RESTlet
*	@return: {int} messageType
*
*/
function determineMessageType(dataIn)
{
	messageType = 0; //default
	
	//iterate over the properties of the object
	for (var prop in dataIn){

		if (prop == "purchase"){
			messageType = SYNC_RECORD_TYPE.PURCHASE; //Purchase Order
		}
	}

	return messageType;
}


/*
*	name: initiatePurchaseOrderSync 
*	descr: steps to perform Purchase Order Sync:
*			1)Validate the message
*			2)Find the Purchase Order in Netsuite
*			3)Create a Router Record 
*
*	author: christopher.neal@kitandace.com
*	@param: {obj} dataIn - object sent to RESTlet
*/
function initiatePurchaseOrderSync(dataIn){

	//1)Validate the message
	var isValid = validatePurchaseOrderMessage(dataIn);

	if (isValid){

		//2) Find the Get Purchase Order ID
		var tranid = dataIn.purchase.purchaseOrderName;
		var cols = new Array();
		cols.push(new nlobjSearchColumn('tranid'));
		var filters = new Array();
		filters.push(new nlobjSearchFilter('tranid', 	null, 'is', tranid));
		filters.push(new nlobjSearchFilter('mainline', null, 'is', 'T'));	
		var purchaseOrderRecord = nlapiSearchRecord('purchaseorder', null, filters, cols);

		if (purchaseOrderRecord != null){

			//3) Create the Router Record 
			
			// var routerLog = nlapiCreateRecord('customrecord_external_sync_obj');
			// routerLog.setFieldValue('custrecord_extsync_request', JSON.stringify(dataIn));
			// routerLog.setFieldValue('custrecord_extsync_resourceid', dataIn.resourceId);
			// routerLog.setFieldValue('custrecord_extsync_status_id', 0); //Product status id
			// routerLog.setFieldValue('custrecord_extsync_type', SYNC_TYPE.PURCHASE); //Sync Type
			// routerLog.setFieldValue('custrecord_extsync_from', SYNC_SYSTEM.NETSUITE); //From system
			// routerLog.setFieldValue('custrecord_extsync_to', SYNC_SYSTEM.TIGERS); //To system		
			// routerLog.setFieldValue('custrecord_erp_ir_transactionlink', purchaseOrderRecord[0].id);
			// var routerLogId = nlapiSubmitRecord(routerLog);

			var fileId = createPayloadFile(folderName, dataIn, dataIn.resourceId);

			var routerRecord = nlapiCreateRecord('customrecord_router_record');
			routerRecord.setFieldValue('custrecord_router_transaction_number', tranid);
			routerRecord.setFieldValue('custrecord_router_in_out', IN_OUT.OUT);
			routerRecord.setFieldValue('custrecord_router_from', SYNC_SYSTEM.NETSUITE);
			routerRecord.setFieldValue('custrecord_router_to', SYNC_SYSTEM.ESB);
			//routerRecord.setFieldValue('custrecord_router_type', );
			//routerRecord.setFieldValue('custrecord_routerlist_status', );
			routerRecord.setFieldValue('custrecord_router_record_type', SYNC_RECORD_TYPE.PURCHASE);
			routerRecord.setFieldValue('custrecord_router_transaction_id',  purchaseOrderRecord[0].id);
			routerRecord.setFieldValue('custrecord_router_payload', fileId);

			var routerRecordId = nlapiSubmitRecord(routerRecord);

			returnObject.message = 'Created Router Record: (ID: ' + routerRecordId + ', Type: Purchase Order, Transaction: ' + tranid + ')';

		} else {
			returnObject.message = 'Could not find Purchase Order: ' + tranid;
		}
	} else {
		returnObject.message = 'Not a valid \'Purchase Order\' message';
	}
}


/*
*	name: validatePurchaseOrderMessage 
*	descr: validate that the proper information has been sent to the RESTlet
*
*	author: christopher.neal@kitandace.com
*	@param: {obj} dataIn - object sent to RESTlet
*	@return: {boolean} true - valid message was sent, false - invalid message
*
*/
function validatePurchaseOrderMessage(dataIn){

	return true; //for now
}


function createPayloadFile(folderName, dataIn, crResourceId){

	var filters = new Array();
	filters.push(new nlobjSearchFilter('name', null, 'is', folderName));
	var fileCabinet = nlapiSearchRecord('folder', null, filters, null);
	var folderId = (fileCabinet != null) ? fileCabinet[0].getId() : 0;

	var newFile = nlapiCreateFile(dataIn.resourceId, 'PLAINTEXT', JSON.stringify(dataIn));
	newFile.setFolder(folderId);
	var fileId = nlapiSubmitFile(newFile);

	return fileId;
}