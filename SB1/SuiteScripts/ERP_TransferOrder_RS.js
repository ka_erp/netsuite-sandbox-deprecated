/**
 *	File Name		:	ERP_TransferOrder_RS.js
 *	Function		:	
 * 	Authors			:	christopher.neal@kitandace.com
 *	Company			:	Kit & Ace
 *	Release Dates	:	
 * 	Current Version	:	
 *
 *	Description: 	RESTlet to synchronize Transfer Orders with ESB/3PL. 
 *					
**/


function post_syncTransferOrder(dataIn)
{

	nlapiLogExecution('DEBUG', 'Received payload from suitelet', JSON.stringify(dataIn));


}