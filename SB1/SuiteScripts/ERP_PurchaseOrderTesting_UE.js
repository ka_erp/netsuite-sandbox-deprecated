/**
 *	File Name		:	ERP_PurchaseOrderTesting_UE.js
 *	Function		:	
 * 	Authors			:	christopher.neal@kitandace.com
 *	Company			:	Kit & Ace
 *	Release Dates	:	
 * 	Current Version	:	
 *
 *	Description: 	A testing script. Creates a Router Record to simulate the integration to ESB/Tigers.
 *
**/


//Router Record Constants
var returnObject = {};
var folderName = 'External Sync Dev';

var IN_OUT = {};
IN_OUT.IN = 1;
IN_OUT.OUT = 2;

var SYNC_SYSTEM = {};
SYNC_SYSTEM.NETSUITE = 1;
SYNC_SYSTEM.WFX = 2;
SYNC_SYSTEM.ESB = 3;

var SYNC_STATUS = {};
SYNC_STATUS.CREATED = 1;
SYNC_STATUS.PENDING = 2;
SYNC_STATUS.ERROR = 3;

var SYNC_RECORD_TYPE = {};
SYNC_RECORD_TYPE.PURCHASE = 1;

var SYNC_TYPE = {};
SYNC_TYPE.CREATE = 1;
SYNC_TYPE.READ = 2;
SYNC_TYPE.UPDATE = 3;
SYNC_TYPE.DELETE = 4;

function afterSubmit_createTestingRouterRecord(type){

	nlapiLogExecution('DEBUG', 'type', type);

	if (type == 'edit'){
		createRouterRecord();
	}

}


function createRouterRecord(){

	nlapiLogExecution('DEBUG', 'Testing', 'Creating router record');

	var poId = nlapiGetRecordId();
	var poObj = getPurchaseOrderModel(poId);

	var fileId = createPayloadFile(folderName, poObj);

	var routerRecord = nlapiCreateRecord('customrecord_router_record');
	routerRecord.setFieldValue('custrecord_router_transaction_number', poObj.tranid);
	routerRecord.setFieldValue('custrecord_router_in_out', IN_OUT.OUT);
	routerRecord.setFieldValue('custrecord_router_from', SYNC_SYSTEM.NETSUITE);
	routerRecord.setFieldValue('custrecord_router_to', SYNC_SYSTEM.ESB);
	routerRecord.setFieldValue('custrecord_router_type', SYNC_TYPE.CREATE);
	routerRecord.setFieldValue('custrecord_routerlist_status', SYNC_STATUS.CREATED);
	routerRecord.setFieldValue('custrecord_router_record_type', SYNC_RECORD_TYPE.PURCHASE);
	routerRecord.setFieldValue('custrecord_router_transaction_id',  poObj.netsuiteId);
	routerRecord.setFieldValue('custrecord_router_payload', fileId);
	
	var routerRecordId = nlapiSubmitRecord(routerRecord, true);
	nlapiLogExecution('DEBUG', 'Testing', 'Created Router Record: (ID: ' + routerRecordId + ', Type: Purchase Order, Transaction: ' + poObj.tranid + ')');

	var logRecord = nlapiCreateRecord('customrecord_integrations_log');	
	logRecord.setFieldValue('custrecord_intlog_router_link', routerRecordId)
	logRecord.setFieldValue('custrecord_intlog_transaction_number', poObj.tranid);
	logRecord.setFieldValue('custrecord_intlog_in_out', IN_OUT.OUT);
	logRecord.setFieldValue('custrecord_intlog_record_type', SYNC_RECORD_TYPE.PURCHASE);
	logRecord.setFieldValue('custrecord_intlog_index', 1);
	logRecord.setFieldValue('custrecord_intlog_log_message', 'Created log');
	// var startDate = nlapiDateToString(new Date, 'datetime');
	// var endDate = nlapiDateToString(new Date, 'datetime');
	//logRecord.setFieldValue('custrecord_intlog_start', new Date);
	//logRecord.setFieldValue('custrecord_intlog_end', new Date);

	var logRecordId = nlapiSubmitRecord(logRecord);
	nlapiLogExecution('DEBUG', 'Created Log', 'Router: ' + routerRecordId + ', Log: ' + logRecordId);
}


function getPurchaseOrderModel(poId){

	var curRecord = nlapiLoadRecord('purchaseorder', poId);

	var poObj = {};

	poObj.netsuiteId = curRecord.getFieldValue('id');
	poObj.tranid = curRecord.getFieldValue('tranid'); //PO#
	poObj.entity = curRecord.getFieldValue('entity'); //Vendor ID
	poObj.employee = curRecord.getFieldValue('employee'); //Employee ID
	poObj.trandate = curRecord.getFieldValue('trandate'); //transaction date.
	poObj.location = curRecord.getFieldValue('location'); //location id

	return poObj;
}


function createPayloadFile(folderName, dataIn){

	var filters = new Array();
	filters.push(new nlobjSearchFilter('name', null, 'is', folderName));
	var fileCabinet = nlapiSearchRecord('folder', null, filters, null);
	var folderId = (fileCabinet != null) ? fileCabinet[0].getId() : 0;

	var newFile = nlapiCreateFile(dataIn.tranid, 'PLAINTEXT', JSON.stringify(dataIn));
	newFile.setFolder(folderId);
	var fileId = nlapiSubmitFile(newFile);

	return fileId;
}







