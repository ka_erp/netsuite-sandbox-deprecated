/**
 *	File Name		:	ERP_PurchaseOrder_SU.js
 *	Function		:	
 * 	Authors			:	christopher.neal@kitandace.com
 *	Company			:	Kit & Ace
 *	Release Dates	:	
 * 	Current Version	:	
 *
 *	Description: 	Suitelet to synchronize Purchase Orders with ESB/3PL. 
 *					
**/

/*
https://forms.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=501&deploy=1&compid=3883338&h=9498516774600c413aba

/app/site/hosting/scriptlet.nl?script=501&deploy=1
*/

function syncPurchaseOrder(request, response){


	if (request.getMethod() == 'GET'){
		nlapiLogExecution('DEBUG', 'Entered Suitelet', 'GET');

		var routerLogId = request.getParameter('routerlogid');

		nlapiLogExecution('DEBUG', 'routerLogId', routerLogId);

		processPurchaseRecord(routerLogId);

		response.write('Processed ID');
	} else {
		nlapiLogExecution('DEBUG', 'here', 'post');

		response.write('test post');
	}
}


/*
*	name: processPurchaseRecord
*	descr: 
*
*	author: christopher.neal@kitandace.com
*	@param: {id} routerLogId - ID of the Custom Router Record to be processed
*	@return: tbd
*
*/
function processPurchaseRecord(routerLogId){

	//load the custom record
	var routerRecord = nlapiLoadRecord('customrecord_external_sync_obj', routerLogId);
	var routerRequest = routerRecord.getFieldValue('custrecord_extsync_request');

	//load the Transfer Order record
	var purchaseOrderId = routerRecord.getFieldValue('custrecord_erp_ir_transactionlink');
	var purchaseOrderRecord = nlapiLoadRecord('purchaseorder', purchaseOrderId);

	nlapiLogExecution('DEBUG', 'loaded purchase order record', 'id: ' + purchaseOrderId);
}

