/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope Public
 * @NScriptName Price ajustment scheduler script
 * @param {record} objRec
 * @param {search} objSearch
 * @param {error} objError
 * @param {email} objEmail
 * @param {runtime} objRuntime
 * 1) Schedule a search to look at custom records (search for item recs that are within the hour and not processing)
 * 2) If the custom record matches [time frame] & active then run map reduce
 * 3)
 */


/*
status list
1 pending
2 progress
3 completed
4 error
*/


define(['N/search',
        'N/record',
        'N/email',
        'N/runtime',
        'N/error',
        'N/file',
        'N/format'
        ],
    function(search, record, email, runtime, error, file,format) {

		function getInputData() {
			
			var rightNow =  new Date();
			rightNow = formatDate(rightNow);
			log.debug('datetime',rightNow);

			var headerSetSearch = search.load({
				id: 'customsearch_pa_headers'
			});

			headerSetSearch.filters.push(search.createFilter({
				name: 'custrecord_pa_start_time',
				operator: 'onorbefore',
				values: [rightNow]
			}));

			var headerIds =[];
			headerSetSearch.run().each(function(result) {
				headerIds.push(result.getValue({
					name: 'internalid'
				}));
				return true;
			});
			log.debug('header ids', JSON.stringify(headerIds));
			
			for (var i =0; i < headerIds.length;i++ ){
				record.submitFields({
					type:'customrecord_price_adjustment',
					id:headerIds[i],
					values: {
						custrecord_pa_status:2 // in progress
					}
				});
			}

			//Get all of the item records that need to be changed
			// the search contains the filters for the markdown
			var itmsToAdj = [];

			if(headerIds && headerIds.length > 0){
				
				var searchObj = search.load({
					id: 'customsearch_pa_items',
				});
				
				searchObj.filters.push(search.createFilter({
					name: 'internalid',
					operator: 'anyof',
					values: headerIds
				}));

				log.debug('search : ', searchObj);

				var pagedData = searchObj.runPaged({
					pageSize: 1000
				});

				pagedData.pageRanges.forEach(function(pageRange) {
					var page = pagedData.fetch({
						index: pageRange.index
					});
					itmsToAdj = itmsToAdj.concat(page.data);
				});
			}
			
			log.debug('search results', JSON.stringify(itmsToAdj));
			
			// send an array of items to discount to MAP
			return itmsToAdj;
  		}

      // as this is a 1-1 ratio a reduce is not rquired for doing the price adjustments

        function map(context) {

        	log.debug('map parameters',context.value);

        	var searchResult = JSON.parse(context.value);


          //adjustment record
          var itm = {
                    id: 0,
                    priceLevel:0,
                    currency:0,
                    recLine:0,
                    newRecPrice:0,
                    oldRecPrice:0
          };


          itm.id = searchResult.values.formulatext;

          var itemObj = record.load({ // 5 GOV
                        type: record.Type.INVENTORY_ITEM,
                        id: itm.id // internal ID of the item
                    });

          itm.newRecPrice = searchResult.values.formulacurrency;
          itm.currency = 'price'+searchResult.values.formulatext_2;
          itm.priceLevel =  searchResult.values.formulatext_1.toString();	
          itm.adjId =  searchResult.values.formulanumeric; //price record id
			log.debug('price levels', itm.priceLevel);
			
			var objOldPrices = {};
			var arPriceLevels = [];
			
			if(itm.priceLevel){
				arPriceLevels = itm.priceLevel.split(",");
			}
			
			for(var i = 0; i < arPriceLevels.length; i++){
				
				//find the row that has the internal ID of the price level
                itm.recLine = itemObj.findSublistLineWithValue({
                    sublistId:itm.currency,
                    fieldId: 'pricelevel',
                    value: arPriceLevels[i]
                });
				
				//get old price then store
				itm.oldRecPrice = itemObj.getMatrixSublistValue({
                    sublistId: itm.currency,
                    fieldId: 'price',
                    column:0, // always this as we don't do discounts on volume
                    line:itm.recLine
                });
				objOldPrices[arPriceLevels[i]] = itm.oldRecPrice;
				
				//set new price
				itemObj.setMatrixSublistValue({
					sublistId: itm.currency,
					fieldId: 'price',
					column: 0,
					line: itm.recLine,
					value: itm.newRecPrice
				});			
			}

          var itemSaved = itemObj.save();
		  
		//error checking if the line was set properly
          if (itemSaved){
            record.submitFields({
              //backup old price field 3 GOV
                   type:'customrecord_pa_child',
                   id:itm.adjId,
                   values: {
						custrecord_pa_price_backup: JSON.stringify(objOldPrices),
						custrecord_pa_adj_child_status:3 //completed
					}
                 });
          }
          else {

            record.submitFields({
                   type:'customrecord_pa_child',
                   id:itm.adjId,
                   values: {
                   custrecord_pa_child_log: JSON.stringify(itemSaved),
                   custrecord_pa_adj_child_status:4 //error
                 }
                 });

          }


			context.write(searchResult.id, 1);

        }


        //write out the parent ids that have been adjusted
        function reduce(context){
            context.write(context.key,context.values);
        }



        function summarize(summary)
        {
          timeStamp = new Date();
          var timeStamp = format.format({
             value : timeStamp,
             type : format.Type.DATETIME
           });

            var parentIds = [];
            summary.output.iterator().each(function(key, value){
                parentIds.push(key);
                return true;
            });

           parentIds.forEach (function(parendIds){
                record.submitFields({
                                type:'customrecord_price_adjustment',
                                id:parendIds,
                                values: {
                                custrecord_pa_status:'3', // completed
                                custrecord_pa_completed_time:timeStamp
                                }
                       });
                    });



          //Set the progress for the Price adjustment scheduler to done
            log.debug('summarizing...');
            var type = summary.toString();
            log.debug('parentIds', JSON.stringify(parentIds));
            log.debug('summary ',JSON.stringify(summary));
            log.debug(type + ' Usage Consumed', summary.usage);
            log.debug(type + ' Number of Queues', summary.concurrency);
            log.debug(type + ' Number of Yields', summary.yields);
        }
		
		function formatDate(date){
			var hours = date.getHours();
			var minutes = date.getMinutes();
			var ampm = hours >= 12 ? 'pm' : 'am';
			hours = hours % 12;
			hours = hours ? hours : 12; // the hour '0' should be '12'
			minutes = minutes < 10 ? '0'+minutes : minutes;
			var strTime = hours + ':' + minutes + ' ' + ampm;
			return date.getMonth()+1 + "/" + date.getDate() + "/" + date.getFullYear() + " " + strTime;
		}

        return {
            getInputData: getInputData,
            map: map,
            reduce: reduce,
            summarize: summarize
        };


    });
