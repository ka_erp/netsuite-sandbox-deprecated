/**
 *	File Name		:	ERP_ExternalSyncObj_UE.js
 *	Function		:	
 * 	Authors			:	christopher.neal@kitandace.com
 *	Company			:	Kit & Ace
 *	Release Dates	:	
 * 	Current Version	:	
 *
 *	Description: 	User event script to 
 *
**/

var SYNC_TYPE = {};
SYNC_TYPE.PRODUCT = 1;
SYNC_TYPE.TRANSFER = 4;
SYNC_TYPE.ITEMRECEIPT = 5;
SYNC_TYPE.PURCHASE = 6;

var SYNC_SYSTEM = {};
SYNC_SYSTEM.NETSUITE = 1;
SYNC_SYSTEM.TIGERS = 2;

var URLS = {};
//URLS.TRANSFER = 'https://forms/sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=500&deploy=1';

URLS.TRANSFER = 'https://forms.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=500&deploy=1&compid=3883338&h=8f28a41d3f9bf8a4a803';
URLS.PURCHASE = 'https://forms.sandbox.netsuite.com/app/site/hosting/scriptlet.nl?script=501&deploy=1&compid=3883338&h=9498516774600c413aba';





/*
*	name: beforeSubmit_updateStatus
*	descr:  Update the status on the External Sync Object record once
*			it has been created.
*
*	author: christopher.neal@kitandace.com
*	@param: {type} type of operation on record
*
*/
function beforeSubmit_updateStatus(type){

	nlapiLogExecution('debug', 'External Sync Obj created', 'running before submit logic');

	if(type != 'delete')
	{
		var syncType = nlapiGetFieldValue('custrecord_extsync_type'); //get sync type
		var syncFrom = nlapiGetFieldValue('custrecord_extsync_from'); //get sync from
		var syncTo = nlapiGetFieldValue('custrecord_extsync_to'); //get sync to

		//determine which type of sync to occur
		if (syncType == SYNC_TYPE.PRODUCT){
			setProductSyncStatusAndFile(); //set status for product sync

		} else if (syncType == SYNC_TYPE.ITEMRECEIPT){
			setTransferSyncStatus(); //set status for item receipt sync

		} else if (syncType == SYNC_TYPE.PURCHASE){
			setPurchaseSyncStatus();

		} else {
			//todo
		}
	}
}

/*
*	name: afterSubmit_updateRecord
*	descr: 
*
*	author: christopher.neal@kitandace.com
*	@param: {type} type of operation on the record
*
*/
function afterSubmit_updateRecord(type){

	if(type == 'create')
	{
		var syncType = nlapiGetFieldValue('custrecord_extsync_type'); //get sync type
		var syncFrom = nlapiGetFieldValue('custrecord_extsync_from'); //get sync from
		var syncTo = nlapiGetFieldValue('custrecord_extsync_to'); //get sync to

		nlapiLogExecution('debug', 'syncType', syncType);
		nlapiLogExecution('debug', 'syncFrom', syncFrom);
		nlapiLogExecution('debug', 'syncTo', syncTo);

		//Perform the proper sync
		if (syncType == SYNC_TYPE.PRODUCT){//Product sync
			nlapiLogExecution('debug', 'Product Sync', 'Starting call to Poduct Sync Scheduled Script');
			var status = nlapiScheduleScript('customscript_erp_productsync_ss', 'customdeploy_erp_productsync_ss');
			nlapiLogExecution('debug', 'Script', status);

		} else if  (syncType == SYNC_TYPE.TRANSFER){ //Item receipt sync

			nlapiLogExecution('debug', 'Transfer Sync', 'Starting call to Transfer Order suitelet');


			var payload = {};

			//var url = nlapiResolveURL('SUITELET', 'customscripterp_transferorder_su', 'customdeploy_erp_transferorder_su');			
			var url = URLS.TRANSFER;
   			url += '&routerlogid=' + encodeURIComponent(nlapiGetRecordId());
 			
			nlapiLogExecution('debug', 'url', url);
   			var response = nlapiRequestURL(url, null, null, null);

   			nlapiLogExecution('debug', 'response', response.getBody());

		} else if (syncType == SYNC_TYPE.PURCHASE){

			nlapiLogExecution('debug', 'Purchase Sync', 'Starting call to Purchase Order suitelet');


			var payload = {};

			//var url = nlapiResolveURL('SUITELET', 'customscripterp_transferorder_su', 'customdeploy_erp_transferorder_su');			
			var url = URLS.PURCHASE;
   			url += '&routerlogid=' + encodeURIComponent(nlapiGetRecordId());
 			
			nlapiLogExecution('debug', 'url', url);
   			var response = nlapiRequestURL(url, null, null, null);

   			nlapiLogExecution('debug', 'response', response.getBody());

		} else {
			//todo
		}
	}
}

/*
*	name: setProductSyncStatusAndFile.
*	descr:  
*
*	author: christopher.neal@kitandace.com
*
*/
function setProductSyncStatusAndFile(){

	var status_id = nlapiGetFieldValue('custrecord_extsync_status_id');
	var file = parseInt(nlapiGetFieldValue('custrecord_extsync_file'));
	var status;

	nlapiLogExecution('debug', 'status_id', status_id);
	nlapiLogExecution('debug', 'file', file);

	if (status_id == 3){
		status = parseInt(status_id) + 1; // Index starts from 1 in List
	} else if (status_id != 0 && status_id != 1 && status_id != 2) {
		status = 5;
	}

	if(status_id != null){
		nlapiSetFieldValue('custrecord_wtka_extsys_sync_status_name', status);
	}

	try
	{
		if(file > 0){
			nlapiSetFieldValue('custrecord_wtka_extsys_sync_file_link', file);
		}
	}
	catch(err)
	{
		nlapiLogExecution('debug', 'Unable to link file', err);
	}
}


/*
*	name: setTransferSyncStatus
*	descr:  
*
*	author: christopher.neal@kitandace.com
*
*/
function setTransferSyncStatus(){

	var status_id = nlapiGetFieldValue('custrecord_extsync_status_id');
	var status;

	nlapiLogExecution('debug', 'status_id', status_id);

	if (status_id == 3){
		status = parseInt(status_id) + 1; // Index starts from 1 in List
	} else if (status_id != 0 && status_id != 1 && status_id != 2) {
		status = 5;
	}

	if(status_id != null){
		nlapiSetFieldValue('custrecord_wtka_extsys_sync_status_name', status);
	}
}


/*
*	name: setPurchaseSyncStatus
*	descr:  
*
*	author: christopher.neal@kitandace.com
*
*/
function setPurchaseSyncStatus(){

	var status_id = nlapiGetFieldValue('custrecord_extsync_status_id');
	var status;

	nlapiLogExecution('debug', 'status_id', status_id);

	if (status_id == 3){
		status = parseInt(status_id) + 1; // Index starts from 1 in List
	} else if (status_id != 0 && status_id != 1 && status_id != 2) {
		status = 5;
	}

	if(status_id != null){
		nlapiSetFieldValue('custrecord_wtka_extsys_sync_status_name', status);
	}
}