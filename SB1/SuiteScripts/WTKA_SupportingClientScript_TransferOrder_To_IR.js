/**
 *	File Name		:	WTKA_SupportingClientScript_TransferOrder_To_IR.js
 *	Function		:	Supporting Client for Suitelet to submit transfer order to transform them in to Item Reciept.
 * 	Authors			:	Manish Prajapati
 *	Company			:	Wipro Limited
 *	Release Dates	:	22-July-2016
 * 	Current Version	:	1.0
**/

function filterTranferOrders()
{
	var from_date_value = nlapiGetFieldValue('custpage_fromdatefield');
	//alert("from_date_value in client::"+from_date_value);
	var to_date_value = nlapiGetFieldValue('custpage_todatefield');
	//alert("to_date_value in client::"+to_date_value);
	
	var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_transfer_order_to_ir', 'customdeploy_transfer_order_to_ir');
	//alert("suiteletURL::"+suiteletURL);
	var redirectURL = suiteletURL+'&from_date_param='+from_date_value+'&to_date_param='+to_date_value;
	//alert("redirectURL::"+redirectURL);
	
	window.location.href = redirectURL;
	//nlapiSetRedirectURL('SUITELET', 'customscript_transfer_order_to_ir', 'customdeploy_transfer_order_to_ir', false, filter_params);
}
function resetCall()
{
	nlapiSetFieldValue('custpage_datefield','');
	nlapiSetFieldValue('custpage_location','');
	var suiteletURL = nlapiResolveURL('SUITELET', 'customscript_transfer_order_to_ir', 'customdeploy_transfer_order_to_ir');
	window.location.href = suiteletURL;
}