/**
 *	File Name		:	ERP_PurchaseOrder_RS.js
 *	Function		:	
 * 	Authors			:	christopher.neal@kitandace.com
 *	Company			:	Kit & Ace
 *	Release Dates	:	
 * 	Current Version	:	
 *
 *	Description: 	RESTlet to synchronize Purchase Orders with ESB/3PL. 
 *					
**/

/*
/app/site/hosting/restlet.nl?script=502&deploy=1
https://rest.sandbox.netsuite.com/app/site/hosting/restlet.nl?script=502&deploy=1
*/


function post_syncPurchaseOrder(dataIn)
{

	nlapiLogExecution('DEBUG', 'Received payload from User Event Script', JSON.stringify(dataIn));


	return dataIn;

}