{
	var ErrorObj    		= new Object();
	ErrorObj.status    		= "Error";
	ErrorObj.messages    	= new Array();
	ErrorObj.messages[0]  	= new Object();
	var returnObject   		= new Object();
	returnObject.response  	= new Object();

	var folderName			= 'External System Synchronization';
	var lastProcessedId  	= 0;
	var counterFlag   		= 'F';
	var fileCounter   		= 0;
	var governanceMinUsage  = 200;
	var scriptMaxTimeout	= 3500; //3600 - is max
	var processLoopCount 	= 11;
	var FinalString;
	var crRequest, crResourceId, crFileId, crFileCounter, crLastProcessed, crStatus, crRequestId;  
	var processFromDate;
	var processLocationType = new Array();
	var processLocationCode = new Array();
	var scriptLimit			= false;
}	

function get_WTKA_NetSuiteInventoryData(dataIn)
{
	returnObject = getNetSuiteSyncData(dataIn, 1);
	return returnObject;
}

function ValidateRequest(dataIn)
{
	if(!emptyInbound(dataIn)) 															return false;
	if(!emptyObject(dataIn.resourceId,            			"ResourceId"))    			return false; 
	if(!emptyObject(dataIn.inventoryRequest,        		"InventoryRequest"))   		return false; 
	if(!emptyObject(dataIn.inventoryRequest.location,    	"Location"))     			return false;
	if(!containLines(dataIn.inventoryRequest.location,    	"location data present")) 	return false; 

	for(var i in dataIn.inventoryRequest.location)
	{
		if(!emptyObject(dataIn.inventoryRequest.location[i], "Location array"))  		return false;
	}
	
	//Validate Date
	if(dataIn.inventoryRequest.fromDate != null && dataIn.inventoryRequest.fromDate != '')
	{
		if(!validateISODate(dataIn.inventoryRequest.fromDate))	return false;
	}
	
	var resourceId  = dataIn.resourceId;
	var filters   	= new Array();
	filters.push(new nlobjSearchFilter('custrecord_wtka_extsys_sync_resourceid', null, 'is', resourceId));
	filters.push(new nlobjSearchFilter('custrecord_wtka_extsys_sync_type', 		 null, 'is', 1));
	
	var updateRecord   = nlapiSearchRecord('customrecord_wtka_extsys_sync', null, filters, null);
	if(updateRecord != null && updateRecord.length > 0)
	{
		ErrorObj.messages[0].messagetype = "Bad Request";
		ErrorObj.messages[0].message   = "ResourceId already exists. Please try again with a new ResourceId."
		return false;
	}
	return true;
}

function post_WTKA_NetSuite_InventorySync(dataIn)
{
	nlapiLogExecution('DEBUG', 'Inbound Request', JSON.stringify(dataIn));
	var filters 	= new Array();
	filters.push(new nlobjSearchFilter('name', null, 'is',		folderName));
	var fileCabinet = nlapiSearchRecord('folder', 	null, filters, 	null);
	var folderId   	= (fileCabinet != null) ? fileCabinet[0].getId() : 0;
	if(ValidateRequest(dataIn)) //Queued
	{
		try
		{
			var newFile = nlapiCreateFile(dataIn.resourceId, 'PLAINTEXT', '');
			newFile.setFolder(folderId);
			var fileId = nlapiSubmitFile(newFile);
			
			var InventoryUpdate = nlapiCreateRecord('customrecord_wtka_extsys_sync');
			InventoryUpdate.setFieldValue('custrecord_wtka_extsys_sync_request',  	 	JSON.stringify(dataIn));
			InventoryUpdate.setFieldValue('custrecord_wtka_extsys_sync_resourceid',  	dataIn.resourceId);
			InventoryUpdate.setFieldValue('custrecord_wtka_extsys_sync_status',  	 	0); // 0 = not started / In Queue, // 1 = In Progress, // 2 = Completed, // 3 = Error 
			InventoryUpdate.setFieldValue('custrecord_wtka_extsys_sync_type',  		 	1); // Inventory Sync Type
			InventoryUpdate.setFieldValue('custrecord_wtka_extsys_sync_file',     	 	fileId); // File ID
			InventoryUpdate.setFieldValue('custrecord_wtka_extsys_sync_timeoutcount', 	0);
			var updateRecordId  = nlapiSubmitRecord(InventoryUpdate);
			
			/ Response "PENDING" /
			returnObject.response.status  	= "PENDING";
			returnObject.response.eta   	= "10 mins";
			nlapiLogExecution('DEBUG', 'responseObject', JSON.stringify(returnObject));
		}
		catch(err)
		{
			returnObject.response.message = "ERROR: " + err;
		}
		return returnObject;
	}
	else
	{
		nlapiLogExecution('DEBUG', 'ErrorObj ', JSON.stringify(ErrorObj));
		return ErrorObj;
	}
}

function FetchInventoryRecords()
{
	/*** Batch Statuses:
	0 = not started / In Queue
	1 = In Progress
	2 = Completed
	3 = Error ***/

	script_entry = new Date(); //Script Start Time
	
	//1. Fetch data from custom record based on status 0 and 1
	var filters = new Array();
	filters.push(new nlobjSearchFilter('custrecord_wtka_extsys_sync_type', 	null,  'is', 		1)); //Inventory sync
	filters.push(new nlobjSearchFilter('custrecord_wtka_extsys_sync_status', null, 'lessthan', 	2));

	//Fetch records which are in Progress first and then by Internal id
	var cols = new Array();
	cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_request'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_resourceid'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_file'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_filecounter'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_lastprocess'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_timeoutcount'));
	cols.push(new nlobjSearchColumn('custrecord_wtka_extsys_sync_status').setSort(true)); //Fetch records which are in Progress first and then by Internal id
	cols.push(new nlobjSearchColumn('internalid').setSort());
	
	var searchRecords = nlapiSearchRecord('customrecord_wtka_extsys_sync', null, filters, cols);
	if(searchRecords != null)
	{
		nlapiLogExecution('debug', 'searchRecords-Fetch', searchRecords.length);
		for(var i = 0; getScriptTimeConsumed() < scriptMaxTimeout && nlapiGetContext().getRemainingUsage() > governanceMinUsage && searchRecords != null && i < searchRecords.length ; i++)
		{
			// Fetch record and split parameters to be processed
			crRequest 		= searchRecords[i].getValue('custrecord_wtka_extsys_sync_request');
			crResourceId 	= searchRecords[i].getValue('custrecord_wtka_extsys_sync_resourceid');
			crFileId 		= searchRecords[i].getValue('custrecord_wtka_extsys_sync_file');
			crFileCounter 	= searchRecords[i].getValue('custrecord_wtka_extsys_sync_filecounter');
			crLastProcessed = searchRecords[i].getValue('custrecord_wtka_extsys_sync_lastprocess');
			crStatus 		= searchRecords[i].getValue('custrecord_wtka_extsys_sync_status');
			crTimeOutCount	= searchRecords[i].getValue('custrecord_wtka_extsys_sync_timeoutcount');
			crRequestId 	= searchRecords[i].getId();

			crTimeOutCount  = (crTimeOutCount != null && crTimeOutCount.length > 0) ? crTimeOutCount : 0;
			splitParameterValues(crRequest);
			fileCounter = crFileCounter;
			while(crStatus == 0 || crStatus == 1)
			{
				if(getScriptTimeConsumed() < scriptMaxTimeout && nlapiGetContext().getRemainingUsage() > governanceMinUsage)
				{
					// Update record status to 1 to indicate 'In Progress'
					if (crStatus == 0)
					{
						nlapiSubmitField('customrecord_wtka_extsys_sync', crRequestId, 'custrecord_wtka_extsys_sync_status', 1, false);
						crStatus = 1; //Set the field to In progress
					}

					// Get Total count of records based on the request
					var totalLoops = 0;
					try
					{
						//nlapiLogExecution('debug', 'crLastProcessed', crLastProcessed);
						var searchResult = performSearch(processLocationCode, processLocationType, processFromDate, crLastProcessed, 'T');
						if(searchResult != null)
						{
							//Extract total count and divide by 1000, which provides 'n' number of loopings to be done.
							nlapiLogExecution('DEBUG', 'total Results', searchResult.length);
							totalLoops = searchResult[0].getValue(FinalString);
							nlapiLogExecution('DEBUG', 'totalCount-Records', totalLoops);
							totalLoops = parseInt(totalLoops/1000) + 2; 
							nlapiLogExecution('DEBUG', 'totalLoops', totalLoops);
							
							//3. PayLoad function to append to array and submit to file.
							BuildInventoryPayLoad(totalLoops);
							if(scriptLimit)
							{
								SwitchScheduleScript(1);
								break;
							}
						}
						else
						{
							nlapiLogExecution('DEBUG', 'LENGTH', '0');
							crStatus = 2; //Completed state
							updateCustomRecord();
							fileCounter 	= 0;
							lastProcessedId = 0;
						}
					}
					catch(err)
					{
						var errorRef = err;
						if(errorRef.code == 'SSS_SEARCH_TIMEOUT')
						{
							nlapiLogExecution('DEBUG', 'Error in Processing request', crRequestId);
							crTimeOutCount++;
							if(crTimeOutCount > 3)
							{
								nlapiLogExecution('DEBUG', 'Process Aborted', err);
								crStatus = 3;
								updateCustomRecord();
								fileCounter 	= 0;
								lastProcessedId = 0;
								break;
							}
							else
							{
								crStatus = 1; //In-progress
								updateCustomRecord();
								nlapiLogExecution('DEBUG', 'Rescheduling script', 'Process timed out and rescheduling script');
								SwitchScheduleScript(1); // 1 - Inventory Sync
							}
						}
						else
						{
							nlapiLogExecution('debug', 'Error in Processing request', crRequestId);
							nlapiLogExecution('debug', 'Error Details', err);
							crStatus = 3;
							updateCustomRecord();
							fileCounter 	= 0;
							lastProcessedId = 0;
							break;
						}
					}
				}
				else
				{
					//Update customrecord values based on  current state
					updateCustomRecord();
					SwitchScheduleScript(1); // 1 - Inventory Sync
				}
			}
			if(scriptLimit)
			{
				scriptLimit = false;
				break;
			}
		}
	}
	else
	{
		nlapiLogExecution('DEBUG', 'PROCESS', 'No records to process. All requests have been processed');
	}
}
	
function splitParameterValues(dataIn)
{
	//Split the request parameters into respective parameters for processing
	var crRequestObj 	 = JSON.parse(dataIn);
	processFromDate 	 = crRequestObj.inventoryRequest.fromDate;
	var processLocationCodeArray = new Array();
	
	for(var i in crRequestObj.inventoryRequest.location)
	{
		switch(crRequestObj.inventoryRequest.location[i].locationType)
		{
			case 'Retail Store':
				processLocationType[i] = 1;
				break;
			case 'Administration Center':
				processLocationType[i] = 2;
				break;
			case 'Distribution Center':
				processLocationType[i] = 3;
				break;
			case 'Headquarters':
				processLocationType[i] = 4;
				break;
			case 'Region':
				processLocationType[i] = 5;
				break;
			case 'Country':
				processLocationType[i] = 6;
				break;
		}
		var locationCode = crRequestObj.inventoryRequest.location[i].locationCode;
		if(locationCode != "" && locationCode.length > 0) processLocationCodeArray[i] = locationCode; // crRequestObj.inventoryRequest.location[i].locationCode;
	}
	
	if(processLocationCodeArray.length > 0)
	{
		processLocationCode  = FetchIdFromName('location', processLocationCodeArray);
		if(processLocationCode[0] == "Error")	processLocationCode = null;
	}
	else
	{
		processLocationCode = null
	}
}

function performSearch(locationId, locationType, dateValue, lastProcessed, initCall)
{
	//Base filters
	var filters = new Array();
	filters.push(['type',   'anyof',  'InvtPart']); 													// Inventory Item
	filters.push("AND");
	filters.push(['matrix', 'is', 'F']); 																// Inventory without subitem and matrix child items
	filters.push("AND");																				
	filters.push(['locationquantityonhand', 'isnotempty', null]); 			
	filters.push("AND");
	filters.push(['inventorylocation.isinactive', 'is', 'F']);
	filters.push("AND");
	filters.push(['inventorylocation.makeinventoryavailable', 'is', 'T']);
	filters.push("AND");
	if(locationId == null || locationId.length <=0)			filters.push(['inventorylocation', 'anyof', '@ALL@']); // All Locations
	else
	{
		filters.push(generateMultiValueFilterExpression(locationId, 'inventorylocation'));
	}
	filters.push("AND");
	
	if(locationType == null || locationType.length <=0)		filters.push(['inventorylocation.custrecord_ra_loctype', 'anyof', 1, 3, 4]); //3 - Distribution Center, 1-Retail Store, 4-Headquarters;
	else 
	{
		filters.push(generateMultiValueFilterExpression(locationType, 'inventorylocation.custrecord_ra_loctype'));
	}
	
	if(dateValue != null && dateValue != '')
	{
		if(dateValue.match(/^\d{4}\-\d{1,2}\-\d{1,2}T\d{1,2}:\d{2}:\d{2}Z/) != null) 				 dateValue = convertDate_UTC(dateValue);
		if(dateValue.match(/^\d{4}\-\d{1,2}\-\d{1,2}T\d{1,2}:\d{2}:\d{2}[+-]\d{1,2}:\d{2}/) != null) dateValue = convertDate_extended(dateValue);
		filters.push("AND");
		filters.push(['lastquantityavailablechange', 'after', dateValue]); 
	}
	//else Initial Inventory Sync-no date filter
	
	if(lastProcessed > 0)
	{
		filters.push("AND");
		filters.push(['formulanumeric: ABS(CONCAT({internalid}, {inventorylocation.internalid}))', "greaterthan", lastProcessed, null]); 
	}
	
	var cols = new Array();
	if(initCall == 'F')
	{
		cols.push(new nlobjSearchColumn('custrecord_ra_loctype', 				'inventoryLocation'));
		cols.push(new nlobjSearchColumn('externalid', 							'inventoryLocation'));
		cols.push(new nlobjSearchColumn('created'));
		cols.push(new nlobjSearchColumn('modified'));
		cols.push(new nlobjSearchColumn('upccode'));
		cols.push(new nlobjSearchColumn('averagecost')); 							//IDS Added to include average cost
		cols.push(new nlobjSearchColumn('locationquantityonhand')); 
		cols.push(new nlobjSearchColumn('locationquantityonorder')); 
		cols.push(new nlobjSearchColumn('locationquantitybackordered')); 
		cols.push(new nlobjSearchColumn('locationquantityintransit')); 
		cols.push(new nlobjSearchColumn('locationquantityavailable')); 
		cols.push(new nlobjSearchColumn('locationquantitycommitted')); 
		cols.push(new nlobjSearchColumn('inventorylocation'));
		cols.push(new nlobjSearchColumn('baseprice'));
		FinalString = new nlobjSearchColumn('formulanumeric').setFormula('ABS(CONCAT({internalid}, {inventorylocation.internalid}))');
	}
	else
	{
		FinalString = new nlobjSearchColumn('formulanumeric', null, 'count').setFormula('ABS(CONCAT({internalid}, {inventorylocation.internalid}))');
	}
	cols.push(FinalString.setSort()); 

	var searchResult = nlapiSearchRecord('item', null, filters, cols);
	return searchResult;
}

function BuildInventoryPayLoad(loopCounter)
{
	var finalFileContents 	= '';
	var lastProcessed 		= 0;
	var processedLoop 		= 0;
	var itemObject			= new Array();

	for(var i=0; scriptLimit != true && i < loopCounter; i++)
	{
		nlapiLogExecution('DEBUG', 'loopCounter', parseInt(i+1));
		var finalResult = performSearch(processLocationCode, processLocationType, processFromDate, crLastProcessed, 'F');

		if(finalResult != null)
		{
			//nlapiLogExecution('DEBUG', 'Total Results - subresults', finalResult.length);
			for(var j=0; j < finalResult.length; j++)
			{
				var qtyOnHand			= quantityParse(finalResult[j].getValue('locationquantityonhand'), 		'int'); 
				var qtyBackOrder		= quantityParse(finalResult[j].getValue('locationquantitybackordered'), 'int'); 
				var qtyInTransit		= quantityParse(finalResult[j].getValue('locationquantityintransit'), 	'int'); 
				var qtyAvailableToSell	= quantityParse(finalResult[j].getValue('locationquantityavailable'), 	'int'); 
				var qtyCommitted		= quantityParse(finalResult[j].getValue('locationquantitycommitted'), 	'int'); 
				var amtAverageCost		= quantityParse(finalResult[j].getValue('averagecost'), 				'float');  // IDS 020116 Changed locationaveragecost to Average Cost
				var amtCurrentPrice		= quantityParse(finalResult[j].getValue('baseprice'), 					'float'); 
				var amtPromoPrice		= quantityParse(finalResult[j].getValue('baseprice'), 					'float'); 
				
				var locationType		= readValue(finalResult[j].getText('custrecord_ra_loctype', 'inventoryLocation'));
				switch(locationType)
				{
					case 'Retail Store':
						locationType = 'Retail Shops';
						break;
					case 'Distribution Center':
						locationType = 'Distribution Centres';
						break;
					case 'Headquarters':
						locationType = 'Head Office';
						break;
				}
				
				var newLine 									= new Object();
				newLine.inventory 								= new Object();
				newLine.inventory.inventoryId 					= finalResult[j].getId();
				newLine.inventory.qtyOnHand 					= qtyOnHand;
				newLine.inventory.qtyBackOrder 					= qtyBackOrder;
				newLine.inventory.qtyInTransit 					= qtyInTransit;
				newLine.inventory.qtyAvailableToSell			= qtyAvailableToSell;
				newLine.inventory.qtyCommitted 					= qtyCommitted;
				newLine.inventory.qtyPreOrder 					= null; //Set to null as field is for future-proofing
				newLine.inventory.amtAverageCost				= amtAverageCost;
				newLine.inventory.createdTimeStamp				= readValue(finalResult[j].getValue('created'));
				newLine.inventory.lastUpdatedTimeStamp			= readValue(finalResult[j].getValue('modified'));
				
				newLine.inventory.product 						= new Object();
				newLine.inventory.product.sku 					= new Object();
				newLine.inventory.product.sku.SKUId				= readValue(finalResult[j].getValue('upccode'));
				
				newLine.inventory.location 						= new Object();
				newLine.inventory.location.locationId			= readValue(finalResult[j].getValue('inventoryLocation'));
				newLine.inventory.location.locationType 		= locationType;
				newLine.inventory.location.locationCode 		= readValue(finalResult[j].getValue('externalid', 'inventoryLocation'));
				
				itemObject.push(newLine);
				
				lastProcessedId = finalResult[j].getValue(FinalString); 
				lastProcessed 	= lastProcessedId;
				crLastProcessed = lastProcessedId;
				if(finalResult.length < 1000) counterFlag = 'T';
				if(getScriptTimeConsumed() > scriptMaxTimeout || nlapiGetContext().getRemainingUsage() < governanceMinUsage)
				{
					//nlapiLogExecution('debug', 'BUILD - Time Consumed', getScriptTimeConsumed());
					counterFlag = 'T';
					scriptLimit = true;
					break;
				}
			}
			processedLoop++;
		}		
		else
		{
			nlapiLogExecution('DEBUG', 'No results', 'No contents');
			crStatus = 2;
			updateCustomRecord();
			fileCounter 	= 0;
			lastProcessedId = 0;
			//Reset globals before breaking
			crRequest = 0, crResourceId = '', crFileId = 0, crFileCounter = 0, crLastProcessed = 0, crRequestId = '', crTimeOutCount = 0;
			break;
		}
		
		if(processedLoop == processLoopCount || counterFlag == 'T')
		{
			if(itemObject.length > 0)
			{
				//nlapiLogExecution('DEBUG', 'Inside file creation loop', 'Inside file creation loop');
				//nlapiLogExecution('DEBUG', 'Governance Limit-Before File Creation', nlapiGetContext().getRemainingUsage());
				var fileUpdate 	= nlapiLoadFile(folderName + '/' + crResourceId);
				var fileContent = fileUpdate.getValue();

				var newFileName = fileUpdate.getName();	
				if(fileCounter > 0) newFileName += '_' + parseInt(fileCounter);
				
				var updatedFile = nlapiCreateFile(newFileName, 'PLAINTEXT', JSON.stringify(itemObject));

				updatedFile.setFolder(fileUpdate.getFolder());
				var idupdate = nlapiSubmitFile(updatedFile);
				nlapiLogExecution('DEBUG', 'Governance Limit-After File Creation', nlapiGetContext().getRemainingUsage());
				fileCounter++;

				//Update CustomRecord
				updateCustomRecord();
				
				// Reset Values
				itemObject 		= new Array();
				processedLoop 	= 0;
				counterFlag 	= 'F';
			}
		}
		if(scriptLimit)	break;
	}
}

function getScriptTimeConsumed()
{
	var currentTime = new Date();
	var timeDiff 	= (currentTime - script_entry)/1000; //in seconds
	return timeDiff;
}